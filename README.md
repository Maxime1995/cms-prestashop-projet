Instructions d'installation

1. Récupérer les fichiers sur git lab en faisant la commande "git clone https://gitlab.com/Maxime1995/cms-prestashop-projet.git" dans le dossier localhost de votre serveur.
2. Récupérer la base de données "prestashop.sql" se trouvant sur le dossier racine du projet et l'importer sur votre serveur mysql
3. Rendez-vous sur l'adresse du site http://localhost/prestashop

