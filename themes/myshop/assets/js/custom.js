/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */

$(document).on('click', '.link-grid-view', function(){
    $(".link-grid-view").addClass("active");
    $(".link-list-view").removeClass("active");
    $(".product-listing").removeClass("list");
    setCookie('productshow',"grid",1);
});

$(document).on('click', '.link-list-view', function(){
    $(".link-list-view").addClass("active");
    $(".link-grid-view").removeClass("active");
    $(".product-listing").addClass("list");
    setCookie('productshow',"list",1);
});

$(document).ready(function(){
    setTimeout(function(){
        $(".sub-popup-overlay").fadeIn("slow");
        $(".subscription-modal").addClass("active");
    }, 1000);
    $(".sub-popup-overlay").click(function(){
        $(".sub-popup-overlay").fadeOut("slow");
        $(".subscription-modal").removeClass("active");
        setTimeout(function(){
            $(".subscription-modal").hide();
        }, 500);
    });
    $(".subscription-modal .close").click(function(){
        $(".sub-popup-overlay").fadeOut("slow");
        $(".subscription-modal").removeClass("active");
        setTimeout(function(){
            $(".subscription-modal").hide();
        }, 500);
    });

    $('.popup-dont input[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
            setCookie('mark_check',"checked",1);
            $(".box").addClass("checked");
        }
        else {
            setCookie('mark_check',"",1);
            $(".box").removeClass("checked");
        }
    });

    $(".search-toggle-on").click(function(){
        $(".sidebar__single").fadeIn();
        $(".search-toggle-off").show();
        $(".search-toggle-on").hide();
    });
    $(".search-toggle-off").click(function(){
        $(".sidebar__single").fadeOut();
        $(".search-toggle-on").show();
        $(".search-toggle-off").hide();
    });

});

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

$(document).on('click', function(){
    $('#product_cart_btn').load(document.URL +  ' #product_cart_btn');
});

// Load the product section after sorting

$(document).on('click', '.dropdown-menu', function(){
    $("#products").css("visibility", "hidden");
    $("#products").slideUp();
    $(document).ready(function() {
        setTimeout(function(){
            $("#products").css("visibility", "visible");
            $('#js-product-list').load(document.URL +  ' #js-product-list');
        }, 400);
        setTimeout(function(){
            $("#products").slideDown(); 
        }, 800);
    });

});

// Load the product section after sorting