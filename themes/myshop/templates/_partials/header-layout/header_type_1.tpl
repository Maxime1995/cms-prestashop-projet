{if !isset($myshop_conf)}
  {$myshop_conf = ''}
{/if}
{block name='header_banner'}
  <div class="header-banner">
    {hook h='displayBanner'}
  </div>
{/block}
{if isset($myshop_conf['enable_header_top']) && $myshop_conf['enable_header_top'] == '1'}
  {block name='header_nav'}
    <nav class="header-section">
      <div class="container">
        <div class="row header-nav-sec">
          <div class="hidden-sm-down col-md-6 left-header-nav">
              {hook h='displayNav1'}
          </div>
          <div class="hidden-sm-down col-md-6 right-header-nav">
            {if $myshop_conf['c_e_email']}
              <div class="email-addrs">
                  <a href="mailto:{$myshop_conf['c_e_email']}"><i class="fa fa-envalop" aria-hidden="true"></i>{$myshop_conf['c_e_email']}</a>
              </div>
            {/if}
            {if $myshop_conf['fb_link'] || $myshop_conf['tw_link'] || $myshop_conf['ig_link'] || $myshop_conf['li_link']}
              <div class="social-links">
                {if $myshop_conf['fb_link']}
                  <a href="{$myshop_conf['fb_link']}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                {/if}
                {if $myshop_conf['tw_link']}
                  <a href="{$myshop_conf['tw_link']}"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                {/if}
                {if $myshop_conf['ig_link']}
                  <a href="{$myshop_conf['ig_link']}"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                {/if}
                {if $myshop_conf['li_link']}
                  <a href="{$myshop_conf['li_link']}"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                {/if}
              </div>
            {/if}
            {hook h='displayNav2'}
          </div>

          <div class="hidden-md-up text-sm-center mobile">
              <div class="col-md-4">
                  <div class="float-xs-right" id="_mobile_user_info"></div>
              </div>
              <div class="col-md-4">
                  <div class="top-logo" id="_mobile_logo"></div>
              </div>
              <div class="col-md-4">
                  <div class="float-xs-right" id="_mobile_cart"></div>
              </div>
          </div>
        </div>
      </div>
    </nav>
  {/block}
{/if}

{if isset($myshop_conf['enable_header_nav']) && $myshop_conf['enable_header_nav'] == '1'}
  {block name='header_top'}
    <div class="header-top">
      <div class="container">
        <div class="row header-menu header-nav-row">
          <div class="col-md-4" id="_desktop_logo">
            {if $shop.logo_details}
              {if $page.page_name == 'index'}
                <h1><a href="{$urls.pages.index}"><img class="logo img-fluid" src="{$shop.logo_details.src}" alt="{$shop.name}" width="{$shop.logo_details.width}" height="{$shop.logo_details.height}"></a></h1>
              {else}
                <a href="{$urls.pages.index}"><img class="logo img-fluid" src="{$shop.logo_details.src}" alt="{$shop.name}" width="{$shop.logo_details.width}" height="{$shop.logo_details.height}"></a>
              {/if}
            {/if}
          </div>
          <div class="col-md-8 header-top-right">
            {hook h='displayNav3'}
            {if isset($myshop_conf['cont_info_content']) && $myshop_conf['cont_info_content']}
              <div class="cont_info_content">
                {$myshop_conf['cont_info_content'] nofilter}
              </div>
            {/if}
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row header-menu">
        <div class="header-top-right col-md-12 col-sm-12 col-xs-12 position-static">
          <div class="hidden-md-up mobile-toggle" id="menu-icon">
            <i class="material-icons">&#xE5D2;</i> <b>{l s='MENU' d='Shop.Header.Menu'}</b>
          </div>
          {hook h='displayTop'}
          <div class="search-toggle">
            <i class="search-toggle-on material-icons search"></i>
            <i class="search-toggle-off material-icons">{l s='close' d='Shop.Header.Menu'}</i>
          </div>
        </div>
      </div>
      <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
        <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
        <div class="js-top-menu-bottom">
          <div id="_mobile_currency_selector"></div>
          <div id="_mobile_language_selector"></div>
          <div id="_mobile_contact_link"></div>
        </div>
      </div>
    </div>
    {hook h='displayNavFullWidth'}
  {/block}
{/if}