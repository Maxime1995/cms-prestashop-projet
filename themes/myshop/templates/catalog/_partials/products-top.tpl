{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div id="js-product-list-top" class="row products-selection">
  <div class="col-xs-12 col-sm-6 col-lg-6">
    <div class="row sort-by-row">

      {block name='sort_by'}
        {include file='catalog/_partials/sort-orders.tpl' sort_orders=$listing.sort_orders}
      {/block}

      {if !empty($listing.rendered_facets)}
        <div class="col-xs-12 hidden-md-up filter-button">
          <button id="search_filter_toggler" class="btn btn-secondary">
            {l s='Filter' d='Shop.Theme.Actions'}
          </button>
        </div>
      {/if}
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 col-lg-6 total-products">
    {if $listing.pagination.total_items > 1}
      <p class="hidden-sm-down">{l s='%product_count% items.' d='Shop.Theme.Catalog' sprintf=['%product_count%' => $listing.pagination.total_items]}</p>
    {elseif $listing.pagination.total_items > 0}
      <p class="hidden-sm-down">{l s='1 item.' d='Shop.Theme.Catalog'}</p>
    {/if}

    {if isset($smarty.cookies.productshow)}
      {if ($smarty.cookies.productshow == list)}
        {$list_act_class = 'active'}
        {$grid_act_class = ''}
      {else}
        {$list_act_class = ''}
        {$grid_act_class = 'active'}
      {/if}
    {else}
      {$list_act_class = ''}
      {$grid_act_class = 'active'}
    {/if}
    <div class="layout-switcher">
      <button class="link-mode link-grid-view {$grid_act_class}"><i class="fa fa-th-large"></i></button>
      <button class="link-mode link-list-view {$list_act_class}"><i class="fa fa-th-list"></i></button>
    </div>
    
  </div>
  {*
  <div class="col-sm-12 hidden-md-up text-sm-center showing"> {l s='Showing %from%-%to% of %total% item(s)' d='Shop.Theme.Catalog' sprintf=[ '%from%' => $listing.pagination.items_shown_from , '%to%' => $listing.pagination.items_shown_to, '%total%' => $listing.pagination.total_items ]}
  </div>
  *}
</div>
