{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div id="_desktop_user_info">
  <div class="user-info">
    {if $logged}

      <a class="account hidden-md-up" href="{$urls.pages.my_account}" title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}" rel="nofollow" >
        <i class="material-icons">&#xE853;</i>
        <span class="hidden-sm-down">{$customerName}</span>
      </a>

      <div class="dropdown js-dropdown hidden-sm-down">
        <button data-target="#" data-toggle="dropdown" class="btn-unstyle">
          <span class="expand-more _gray-darker"><i class="material-icons"></i> My Account</span>
          <i class="material-icons expand-more"></i>
        </button>
        <ul class="dropdown-menu" style="display: none;">
          <li>
            <a class="account" href="{$urls.pages.my_account}" title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}" rel="nofollow" >
              <i class="material-icons">&#xE853;</i>
              <span class="hidden-sm-down">{$customerName}</span>
            </a>
          </li>
          
          {if $customer.addresses|count}
            <li>
              <a id="addresses-link" href="{$urls.pages.addresses}">
                <span class="link-item">
                  <i class="material-icons">&#xE56A;</i>
                  {l s='Addresses' d='Shop.Theme.Customeraccount'}
                </span>
              </a>
            </li>
          {else}
            <li>
              <a id="address-link" href="{$urls.pages.address}">
                <span class="link-item">
                  <i class="material-icons">&#xE567;</i>
                  {l s='Add first address' d='Shop.Theme.Customeraccount'}
                </span>
              </a>
            </li>
          {/if}
        
          {if !$configuration.is_catalog}
            <li>
              <a id="history-link" href="{$urls.pages.history}">
                <span class="link-item">
                  <i class="material-icons">&#xE916;</i>
                  {l s='Order history and details' d='Shop.Theme.Customeraccount'}
                </span>
              </a>
            </li>
          {/if}
        
          {if !$configuration.is_catalog}
            <li>
              <a id="order-slips-link" href="{$urls.pages.order_slip}">
                <span class="link-item">
                  <i class="material-icons">&#xE8B0;</i>
                  {l s='Credit slips' d='Shop.Theme.Customeraccount'}
                </span>
              </a>
            </li>
          {/if}

          <li>
            <a class="logout hidden-sm-down" href="{$urls.actions.logout}" rel="nofollow">
              <i class="material-icons">&#xE7FF;</i>
              {l s='Sign out' d='Shop.Theme.Actions'}
            </a>
          </li>
      </div>
      
    {else}
      <a href="{$urls.pages.my_account}" title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}" rel="nofollow" >
        <i class="material-icons">&#xE7FF;</i>
        <span class="hidden-sm-down">{l s='Sign in' d='Shop.Theme.Actions'}</span>
      </a>
    {/if}
  </div>
</div>
