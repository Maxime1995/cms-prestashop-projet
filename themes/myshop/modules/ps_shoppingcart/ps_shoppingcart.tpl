{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div id="_desktop_cart">
  <div class="blockcart cart-preview {if $cart.products_count > 0}active{else}inactive{/if}" data-refresh-url="{$refresh_url}">
    <div class="dropdown js-dropdown">
      <div class="header">
        <button data-toggle="collapse" data-target="#cart-pop-panel" id="product_cart_btn" class="btn-unstyle">
          <i class="material-icons shopping-cart">shopping_cart</i>
          <span class="cart-products-count">{$cart.products_count}</span>
          <span class="hidden-sm-down">{l s='Cart' d='Shop.Theme.Checkout'}</span>
        </button>
      </div>
      <div id="cart-pop-panel" class="collapse cart-popup">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="container">
              <div class="row cart-popup-top">
                <div class="col-xs-10">
                  <h2>{l s='RECENTLY ADDED ITEM(S)' d='Shop.Theme.Checkout'}</h2>
                </div>
                <div class="col-xs-2 cart-popup-close-button">
                  <button data-toggle="collapse" data-target="#cart-pop-panel" class="btn btn-unstyle"> <i class="material-icons">close</i></button>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="cart-pop-content">
                    {block name='cart_overview'}
                      {include file='checkout/_partials/cart-detailed.tpl' cart=$cart}
                    {/block}
                  </div>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="row cart-popup-bottom">
                <div class="col-md-6">
                 <a href="{$cart_url}"><button class="btn"><i class="material-icons shopping-cart">shopping_cart</i> {l s='View Cart' d='Shop.Theme.Checkout'}</button></a>
                </div>
                <div class="col-md-6 cart-popup-checkout-total">
                  <h2>{l s='TOTAL : ' d='Shop.Theme.Checkout'}</h2>
                  <section class="cart-popup-total-value">
                    {include file='checkout/_partials/cart-summary-items-subtotal.tpl' cart=$cart}
                  </section>
                </div>
              </div>
            </div>
           
          </div>
        </div>
      </div>
        
      </div>
    </div>
  </div>
</div>
