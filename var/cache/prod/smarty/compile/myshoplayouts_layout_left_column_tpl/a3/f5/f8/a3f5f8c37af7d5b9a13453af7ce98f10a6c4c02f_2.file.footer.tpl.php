<?php
/* Smarty version 3.1.43, created on 2022-12-11 16:55:15
  from 'C:\wamp64\www\prestashop\themes\myshop\templates\_partials\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395fd634fc253_80343929',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a3f5f8c37af7d5b9a13453af7ce98f10a6c4c02f' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\myshop\\templates\\_partials\\footer.tpl',
      1 => 1670238248,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6395fd634fc253_80343929 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

  <?php $_smarty_tpl->_assignInScope('footer_widget_section', 0);?>
  <?php $_smarty_tpl->_assignInScope('footer_bottom_section', 0);?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3517730376395fd634f4377_45095111', 'hook_footer_before');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15601399046395fd634f6708_59461241', 'hook_footer_top');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12862600786395fd634f85f6_54105432', 'hook_footer_bottom');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20462440296395fd634fa762_28480348', 'hook_footer_after');
}
/* {block 'hook_footer_before'} */
class Block_3517730376395fd634f4377_45095111 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_before' => 
  array (
    0 => 'Block_3517730376395fd634f4377_45095111',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterBefore'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'hook_footer_before'} */
/* {block 'hook_footer_top'} */
class Block_15601399046395fd634f6708_59461241 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_top' => 
  array (
    0 => 'Block_15601399046395fd634f6708_59461241',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterTop'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'hook_footer_top'} */
/* {block 'hook_footer_bottom'} */
class Block_12862600786395fd634f85f6_54105432 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_bottom' => 
  array (
    0 => 'Block_12862600786395fd634f85f6_54105432',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterBottom'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'hook_footer_bottom'} */
/* {block 'hook_footer_after'} */
class Block_20462440296395fd634fa762_28480348 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_after' => 
  array (
    0 => 'Block_20462440296395fd634fa762_28480348',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterAfter'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'hook_footer_after'} */
}
