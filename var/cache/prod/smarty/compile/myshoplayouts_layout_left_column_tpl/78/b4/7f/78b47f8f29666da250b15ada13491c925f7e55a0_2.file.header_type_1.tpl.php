<?php
/* Smarty version 3.1.43, created on 2022-12-11 16:55:15
  from 'C:\wamp64\www\prestashop\themes\myshop\templates\_partials\header-layout\header_type_1.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395fd63110940_70149411',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '78b47f8f29666da250b15ada13491c925f7e55a0' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\myshop\\templates\\_partials\\header-layout\\header_type_1.tpl',
      1 => 1670238248,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6395fd63110940_70149411 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
if (!(isset($_smarty_tpl->tpl_vars['myshop_conf']->value))) {?>
  <?php $_smarty_tpl->_assignInScope('myshop_conf', '');
}
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_151215946395fd630d85e5_44398617', 'header_banner');
?>

<?php if ((isset($_smarty_tpl->tpl_vars['myshop_conf']->value['enable_header_top'])) && $_smarty_tpl->tpl_vars['myshop_conf']->value['enable_header_top'] == '1') {?>
  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20009959406395fd630dd4a9_98596904', 'header_nav');
?>

<?php }?>

<?php if ((isset($_smarty_tpl->tpl_vars['myshop_conf']->value['enable_header_nav'])) && $_smarty_tpl->tpl_vars['myshop_conf']->value['enable_header_nav'] == '1') {?>
  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9258382896395fd630f65f2_53287972', 'header_top');
?>

<?php }
}
/* {block 'header_banner'} */
class Block_151215946395fd630d85e5_44398617 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header_banner' => 
  array (
    0 => 'Block_151215946395fd630d85e5_44398617',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div class="header-banner">
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayBanner'),$_smarty_tpl ) );?>

  </div>
<?php
}
}
/* {/block 'header_banner'} */
/* {block 'header_nav'} */
class Block_20009959406395fd630dd4a9_98596904 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header_nav' => 
  array (
    0 => 'Block_20009959406395fd630dd4a9_98596904',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <nav class="header-section">
      <div class="container">
        <div class="row header-nav-sec">
          <div class="hidden-sm-down col-md-6 left-header-nav">
              <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNav1'),$_smarty_tpl ) );?>

          </div>
          <div class="hidden-sm-down col-md-6 right-header-nav">
            <?php if ($_smarty_tpl->tpl_vars['myshop_conf']->value['c_e_email']) {?>
              <div class="email-addrs">
                  <a href="mailto:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['myshop_conf']->value['c_e_email'], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-envalop" aria-hidden="true"></i><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['myshop_conf']->value['c_e_email'], ENT_QUOTES, 'UTF-8');?>
</a>
              </div>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['myshop_conf']->value['fb_link'] || $_smarty_tpl->tpl_vars['myshop_conf']->value['tw_link'] || $_smarty_tpl->tpl_vars['myshop_conf']->value['ig_link'] || $_smarty_tpl->tpl_vars['myshop_conf']->value['li_link']) {?>
              <div class="social-links">
                <?php if ($_smarty_tpl->tpl_vars['myshop_conf']->value['fb_link']) {?>
                  <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['myshop_conf']->value['fb_link'], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['myshop_conf']->value['tw_link']) {?>
                  <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['myshop_conf']->value['tw_link'], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['myshop_conf']->value['ig_link']) {?>
                  <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['myshop_conf']->value['ig_link'], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['myshop_conf']->value['li_link']) {?>
                  <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['myshop_conf']->value['li_link'], ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                <?php }?>
              </div>
            <?php }?>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNav2'),$_smarty_tpl ) );?>

          </div>

          <div class="hidden-md-up text-sm-center mobile">
              <div class="col-md-4">
                  <div class="float-xs-right" id="_mobile_user_info"></div>
              </div>
              <div class="col-md-4">
                  <div class="top-logo" id="_mobile_logo"></div>
              </div>
              <div class="col-md-4">
                  <div class="float-xs-right" id="_mobile_cart"></div>
              </div>
          </div>
        </div>
      </div>
    </nav>
  <?php
}
}
/* {/block 'header_nav'} */
/* {block 'header_top'} */
class Block_9258382896395fd630f65f2_53287972 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header_top' => 
  array (
    0 => 'Block_9258382896395fd630f65f2_53287972',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="header-top">
      <div class="container">
        <div class="row header-menu header-nav-row">
          <div class="col-md-4" id="_desktop_logo">
            <?php if ($_smarty_tpl->tpl_vars['shop']->value['logo_details']) {?>
              <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'index') {?>
                <h1><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
"><img class="logo img-fluid" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['src'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
" width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['width'], ENT_QUOTES, 'UTF-8');?>
" height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['height'], ENT_QUOTES, 'UTF-8');?>
"></a></h1>
              <?php } else { ?>
                <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
"><img class="logo img-fluid" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['src'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
" width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['width'], ENT_QUOTES, 'UTF-8');?>
" height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['height'], ENT_QUOTES, 'UTF-8');?>
"></a>
              <?php }?>
            <?php }?>
          </div>
          <div class="col-md-8 header-top-right">
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNav3'),$_smarty_tpl ) );?>

            <?php if ((isset($_smarty_tpl->tpl_vars['myshop_conf']->value['cont_info_content'])) && $_smarty_tpl->tpl_vars['myshop_conf']->value['cont_info_content']) {?>
              <div class="cont_info_content">
                <?php echo $_smarty_tpl->tpl_vars['myshop_conf']->value['cont_info_content'];?>

              </div>
            <?php }?>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row header-menu">
        <div class="header-top-right col-md-12 col-sm-12 col-xs-12 position-static">
          <div class="hidden-md-up mobile-toggle" id="menu-icon">
            <i class="material-icons">&#xE5D2;</i> <b><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'MENU','d'=>'Shop.Header.Menu'),$_smarty_tpl ) );?>
</b>
          </div>
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayTop'),$_smarty_tpl ) );?>

          <div class="search-toggle">
            <i class="search-toggle-on material-icons search"></i>
            <i class="search-toggle-off material-icons"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'close','d'=>'Shop.Header.Menu'),$_smarty_tpl ) );?>
</i>
          </div>
        </div>
      </div>
      <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
        <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
        <div class="js-top-menu-bottom">
          <div id="_mobile_currency_selector"></div>
          <div id="_mobile_language_selector"></div>
          <div id="_mobile_contact_link"></div>
        </div>
      </div>
    </div>
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNavFullWidth'),$_smarty_tpl ) );?>

  <?php
}
}
/* {/block 'header_top'} */
}
