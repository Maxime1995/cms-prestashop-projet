<?php
/* Smarty version 3.1.43, created on 2022-12-11 16:55:25
  from 'C:\wamp64\www\prestashop\themes\myshop\templates\_partials\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395fd6de601b7_09366533',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a3f5f8c37af7d5b9a13453af7ce98f10a6c4c02f' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\myshop\\templates\\_partials\\footer.tpl',
      1 => 1670238248,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6395fd6de601b7_09366533 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

  <?php $_smarty_tpl->_assignInScope('footer_widget_section', 0);?>
  <?php $_smarty_tpl->_assignInScope('footer_bottom_section', 0);?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21405426646395fd6de58c19_79107395', 'hook_footer_before');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2270288016395fd6de5abf2_56308779', 'hook_footer_top');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20415356576395fd6de5ca47_63445386', 'hook_footer_bottom');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20121088926395fd6de5e828_97000304', 'hook_footer_after');
}
/* {block 'hook_footer_before'} */
class Block_21405426646395fd6de58c19_79107395 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_before' => 
  array (
    0 => 'Block_21405426646395fd6de58c19_79107395',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterBefore'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'hook_footer_before'} */
/* {block 'hook_footer_top'} */
class Block_2270288016395fd6de5abf2_56308779 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_top' => 
  array (
    0 => 'Block_2270288016395fd6de5abf2_56308779',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterTop'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'hook_footer_top'} */
/* {block 'hook_footer_bottom'} */
class Block_20415356576395fd6de5ca47_63445386 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_bottom' => 
  array (
    0 => 'Block_20415356576395fd6de5ca47_63445386',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterBottom'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'hook_footer_bottom'} */
/* {block 'hook_footer_after'} */
class Block_20121088926395fd6de5e828_97000304 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_after' => 
  array (
    0 => 'Block_20121088926395fd6de5e828_97000304',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterAfter'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'hook_footer_after'} */
}
