<?php
/* Smarty version 3.1.43, created on 2022-12-11 16:55:25
  from 'C:\wamp64\www\prestashop\themes\myshop\templates\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395fd6dbcd475_90140552',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '233197cf48379b780bc907a2d5b69dad9f00122f' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\myshop\\templates\\page.tpl',
      1 => 1670238248,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6395fd6dbcd475_90140552 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10735803296395fd6dbc1947_30753473', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_4721226156395fd6dbc3397_54144665 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_19451358856395fd6dbc2534_68463378 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4721226156395fd6dbc3397_54144665', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_13161526006395fd6dbc8076_04617824 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_7181030196395fd6dbc90f1_75417718 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_16911364396395fd6dbc7507_27250331 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13161526006395fd6dbc8076_04617824', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7181030196395fd6dbc90f1_75417718', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_7725568536395fd6dbcb612_51644821 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_6247409366395fd6dbcaac4_68077183 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7725568536395fd6dbcb612_51644821', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_10735803296395fd6dbc1947_30753473 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_10735803296395fd6dbc1947_30753473',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_19451358856395fd6dbc2534_68463378',
  ),
  'page_title' => 
  array (
    0 => 'Block_4721226156395fd6dbc3397_54144665',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_16911364396395fd6dbc7507_27250331',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_13161526006395fd6dbc8076_04617824',
  ),
  'page_content' => 
  array (
    0 => 'Block_7181030196395fd6dbc90f1_75417718',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_6247409366395fd6dbcaac4_68077183',
  ),
  'page_footer' => 
  array (
    0 => 'Block_7725568536395fd6dbcb612_51644821',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19451358856395fd6dbc2534_68463378', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16911364396395fd6dbc7507_27250331', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6247409366395fd6dbcaac4_68077183', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
