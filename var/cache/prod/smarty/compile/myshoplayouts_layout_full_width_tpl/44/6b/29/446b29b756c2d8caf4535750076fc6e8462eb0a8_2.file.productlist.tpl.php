<?php
/* Smarty version 3.1.43, created on 2022-12-11 17:03:19
  from 'C:\wamp64\www\prestashop\themes\myshop\templates\catalog\_partials\productlist.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395ff47c57990_49501696',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '446b29b756c2d8caf4535750076fc6e8462eb0a8' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\myshop\\templates\\catalog\\_partials\\productlist.tpl',
      1 => 1670238247,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/miniatures/product.tpl' => 1,
  ),
),false)) {
function content_6395ff47c57990_49501696 (Smarty_Internal_Template $_smarty_tpl) {
if ((isset($_COOKIE['productshow'])) && (isset($_smarty_tpl->tpl_vars['is_product_page']->value))) {?>
    <?php if (($_COOKIE['productshow'] == 'list')) {?>
        <?php $_smarty_tpl->_assignInScope('list_product_class', 'list');?>
    <?php } else { ?>
        <?php $_smarty_tpl->_assignInScope('list_product_class', '');?>
    <?php }
} else { ?>
    <?php $_smarty_tpl->_assignInScope('list_product_class', '');
}?>
<div class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_product_class']->value, ENT_QUOTES, 'UTF-8');?>
 product-listing products<?php if (!empty($_smarty_tpl->tpl_vars['cssClass']->value)) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cssClass']->value, ENT_QUOTES, 'UTF-8');
}?>" itemscope itemtype="http://schema.org/ItemList">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products']->value, 'product', false, 'position');
$_smarty_tpl->tpl_vars['product']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['position']->value => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->do_else = false;
?>
        <?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value,'position'=>$_smarty_tpl->tpl_vars['position']->value), 0, true);
?>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div><?php }
}
