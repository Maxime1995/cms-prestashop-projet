<?php
/* Smarty version 3.1.43, created on 2022-12-11 17:03:19
  from 'C:\wamp64\www\prestashop\themes\myshop\templates\catalog\_partials\products-top.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395ff47be2e76_71588387',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '27ed6178db4da07562c948af29b12b534c447823' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\myshop\\templates\\catalog\\_partials\\products-top.tpl',
      1 => 1670238247,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/sort-orders.tpl' => 1,
  ),
),false)) {
function content_6395ff47be2e76_71588387 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<div id="js-product-list-top" class="row products-selection">
  <div class="col-xs-12 col-sm-6 col-lg-6">
    <div class="row sort-by-row">

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10828649366395ff47bc75e0_99950260', 'sort_by');
?>


      <?php if (!empty($_smarty_tpl->tpl_vars['listing']->value['rendered_facets'])) {?>
        <div class="col-xs-12 hidden-md-up filter-button">
          <button id="search_filter_toggler" class="btn btn-secondary">
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Filter','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

          </button>
        </div>
      <?php }?>
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 col-lg-6 total-products">
    <?php if ($_smarty_tpl->tpl_vars['listing']->value['pagination']['total_items'] > 1) {?>
      <p class="hidden-sm-down"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'%product_count% items.','d'=>'Shop.Theme.Catalog','sprintf'=>array('%product_count%'=>$_smarty_tpl->tpl_vars['listing']->value['pagination']['total_items'])),$_smarty_tpl ) );?>
</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['listing']->value['pagination']['total_items'] > 0) {?>
      <p class="hidden-sm-down"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'1 item.','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</p>
    <?php }?>

    <?php if ((isset($_COOKIE['productshow']))) {?>
      <?php if (($_COOKIE['productshow'] == 'list')) {?>
        <?php $_smarty_tpl->_assignInScope('list_act_class', 'active');?>
        <?php $_smarty_tpl->_assignInScope('grid_act_class', '');?>
      <?php } else { ?>
        <?php $_smarty_tpl->_assignInScope('list_act_class', '');?>
        <?php $_smarty_tpl->_assignInScope('grid_act_class', 'active');?>
      <?php }?>
    <?php } else { ?>
      <?php $_smarty_tpl->_assignInScope('list_act_class', '');?>
      <?php $_smarty_tpl->_assignInScope('grid_act_class', 'active');?>
    <?php }?>
    <div class="layout-switcher">
      <button class="link-mode link-grid-view <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['grid_act_class']->value, ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-th-large"></i></button>
      <button class="link-mode link-list-view <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_act_class']->value, ENT_QUOTES, 'UTF-8');?>
"><i class="fa fa-th-list"></i></button>
    </div>
    
  </div>
  </div>
<?php }
/* {block 'sort_by'} */
class Block_10828649366395ff47bc75e0_99950260 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'sort_by' => 
  array (
    0 => 'Block_10828649366395ff47bc75e0_99950260',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/sort-orders.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('sort_orders'=>$_smarty_tpl->tpl_vars['listing']->value['sort_orders']), 0, false);
?>
      <?php
}
}
/* {/block 'sort_by'} */
}
