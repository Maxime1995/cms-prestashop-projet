<?php
/* Smarty version 3.1.43, created on 2022-12-11 16:55:15
  from 'module:psshoppingcartpsshoppingc' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395fd63241cd3_35147624',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '35655e6409b6198f29dd6e732ef9598dec599880' => 
    array (
      0 => 'module:psshoppingcartpsshoppingc',
      1 => 1670238247,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'file:checkout/_partials/cart-detailed.tpl' => 1,
    'file:checkout/_partials/cart-summary-items-subtotal.tpl' => 1,
  ),
),false)) {
function content_6395fd63241cd3_35147624 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<div id="_desktop_cart">
  <div class="blockcart cart-preview <?php if ($_smarty_tpl->tpl_vars['cart']->value['products_count'] > 0) {?>active<?php } else { ?>inactive<?php }?>" data-refresh-url="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['refresh_url']->value, ENT_QUOTES, 'UTF-8');?>
">
    <div class="dropdown js-dropdown">
      <div class="header">
        <button data-toggle="collapse" data-target="#cart-pop-panel" id="product_cart_btn" class="btn-unstyle">
          <i class="material-icons shopping-cart">shopping_cart</i>
          <span class="cart-products-count"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['products_count'], ENT_QUOTES, 'UTF-8');?>
</span>
          <span class="hidden-sm-down"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Cart','d'=>'Shop.Theme.Checkout'),$_smarty_tpl ) );?>
</span>
        </button>
      </div>
      <div id="cart-pop-panel" class="collapse cart-popup">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="container">
              <div class="row cart-popup-top">
                <div class="col-xs-10">
                  <h2><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'RECENTLY ADDED ITEM(S)','d'=>'Shop.Theme.Checkout'),$_smarty_tpl ) );?>
</h2>
                </div>
                <div class="col-xs-2 cart-popup-close-button">
                  <button data-toggle="collapse" data-target="#cart-pop-panel" class="btn btn-unstyle"> <i class="material-icons">close</i></button>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="cart-pop-content">
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7958429836395fd6323bcb3_53046652', 'cart_overview');
?>

                  </div>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="row cart-popup-bottom">
                <div class="col-md-6">
                 <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart_url']->value, ENT_QUOTES, 'UTF-8');?>
"><button class="btn"><i class="material-icons shopping-cart">shopping_cart</i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View Cart','d'=>'Shop.Theme.Checkout'),$_smarty_tpl ) );?>
</button></a>
                </div>
                <div class="col-md-6 cart-popup-checkout-total">
                  <h2><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'TOTAL : ','d'=>'Shop.Theme.Checkout'),$_smarty_tpl ) );?>
</h2>
                  <section class="cart-popup-total-value">
                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/_partials/cart-summary-items-subtotal.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cart'=>$_smarty_tpl->tpl_vars['cart']->value), 0, false);
?>
                  </section>
                </div>
              </div>
            </div>
           
          </div>
        </div>
      </div>
        
      </div>
    </div>
  </div>
</div>
<?php }
/* {block 'cart_overview'} */
class Block_7958429836395fd6323bcb3_53046652 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'cart_overview' => 
  array (
    0 => 'Block_7958429836395fd6323bcb3_53046652',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                      <?php $_smarty_tpl->_subTemplateRender('file:checkout/_partials/cart-detailed.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cart'=>$_smarty_tpl->tpl_vars['cart']->value), 0, false);
?>
                    <?php
}
}
/* {/block 'cart_overview'} */
}
