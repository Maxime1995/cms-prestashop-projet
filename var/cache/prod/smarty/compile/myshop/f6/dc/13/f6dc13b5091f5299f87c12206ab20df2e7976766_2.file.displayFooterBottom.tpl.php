<?php
/* Smarty version 3.1.43, created on 2022-12-11 16:55:15
  from 'C:\wamp64\www\prestashop\modules\myshophelper\views\templates\hook\displayFooterBottom.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395fd635bccf5_87904831',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f6dc13b5091f5299f87c12206ab20df2e7976766' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\modules\\myshophelper\\views\\templates\\hook\\displayFooterBottom.tpl',
      1 => 1670238246,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6395fd635bccf5_87904831 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
if ($_smarty_tpl->tpl_vars['c_e_en_n_p']->value != '0') {?>
    <?php if ((isset($_smarty_tpl->tpl_vars['popup_content']->value)) && $_smarty_tpl->tpl_vars['popup_content']->value) {?>
        <?php if (!(isset($_COOKIE['mark_check'])) || $_COOKIE['mark_check'] != 'checked') {?>
            <!-- Subscription Popup -->
                <div class="subscription-modal">
                    <button type="button" class="close">
                        <i class="material-icons">close</i>
                    </button>
                    <div class="html-popup-content">
                        <?php echo $_smarty_tpl->tpl_vars['popup_content']->value;?>

                    </div>
                    <div class="popup-dont">
                        <span class="box"></span><label><input type="checkbox" value="" id="checkbox">Don’t show this popup again</label>
                    </div>
                </div>
                <div class="sub-popup-overlay">
                </div>
            <!-- Subscription Popup -->
        <?php }?>
    <?php } else { ?>
        <?php if (!(isset($_COOKIE['mark_check'])) || $_COOKIE['mark_check'] != 'checked') {?>
            <!-- Subscription Popup -->
                <div class="subscription-modal">
                    <button type="button" class="close">
                        <i class="material-icons">close</i>
                    </button>
                    <div class="popup-content">
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['widget'][0], array( array('name'=>"ps_emailsubscription"),$_smarty_tpl ) );?>

                    </div>
                    <div class="popup-dont">
                        <span class="box"></span><label><input type="checkbox" value="" id="checkbox">Don’t show this popup again</label>
                    </div>
                </div>
                <div class="sub-popup-overlay">
                </div>
            <!-- Subscription Popup -->
        <?php }?>
    <?php }
}?>

<?php if ($_smarty_tpl->tpl_vars['c_e_en_f_w']->value != '0') {?>
    <!-- Footer Widget Section -->
    <div class="foooter-widget-section">
        <div class="container">
        <div class="row">
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14535697846395fd635acf02_19771953', 'hook_footer');
?>

        </div>
        </div>
    </div>
    <!-- Footer Widget Section -->
    <?php } else { ?>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20135012706395fd635af581_96284321', 'hook_footer');
?>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['c_e_en_f_b']->value != 'off') {?>
    <!-- Footer Bottom Section -->
    <div class="foooter-bottom-section">
        <div class="container">
            <div class="row">
            <div class="col-md-12">
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3216711506395fd635b2c57_45696007', 'copyright_link');
?>

            </div>
            </div>
        </div>
    </div>
    <!-- Footer Bottom Section -->
<?php }
}
/* {block 'hook_footer'} */
class Block_14535697846395fd635acf02_19771953 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer' => 
  array (
    0 => 'Block_14535697846395fd635acf02_19771953',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooter'),$_smarty_tpl ) );?>

            <?php
}
}
/* {/block 'hook_footer'} */
/* {block 'hook_footer'} */
class Block_20135012706395fd635af581_96284321 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer' => 
  array (
    0 => 'Block_20135012706395fd635af581_96284321',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooter'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_footer'} */
/* {block 'copyright_link'} */
class Block_3216711506395fd635b2c57_45696007 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'copyright_link' => 
  array (
    0 => 'Block_3216711506395fd635b2c57_45696007',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php if ($_smarty_tpl->tpl_vars['c_e_copyright_text']->value) {?>
                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c_e_copyright_link']->value, ENT_QUOTES, 'UTF-8');?>
" target="_blank" rel="noopener noreferrer nofollow">
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'%c_e_copyright_text%','sprintf'=>array('%c_e_copyright_text%'=>$_smarty_tpl->tpl_vars['c_e_copyright_text']->value),'d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>

                        </a>
                    <?php } else { ?>
                        <a href="https://classydevs.com/?utm_source=myshop_theme&utm_medium=myshop_footer_copyright&utm_campaign=myshop_footer_copyright&utm_id=myshop_copyright&utm_term=myshop_footer_copyright" target="_blank" rel="noopener noreferrer nofollow">
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'%copyright% ClassyDevs %year% | All Rights Reserved','sprintf'=>array('%prestashop%'=>'PrestaShop™','%year%'=>date('Y'),'%copyright%'=>'©'),'d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>

                        </a>
                    <?php }?>
                <?php
}
}
/* {/block 'copyright_link'} */
}
