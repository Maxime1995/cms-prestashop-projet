<?php
/* Smarty version 3.1.43, created on 2022-12-11 17:00:16
  from 'module:psimagesliderviewstemplat' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395fe90332f57_56107469',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6c2108a17c7103b6e203f4f0621d4645b56b0114' => 
    array (
      0 => 'module:psimagesliderviewstemplat',
      1 => 1670238247,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6395fe90332f57_56107469 (Smarty_Internal_Template $_smarty_tpl) {
?>
  <div id="carousel" data-ride="carousel" class="carousel slide" data-interval="5000" data-wrap="true" data-pause="hover" data-touch="true">
    <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
          </ol>
    <ul class="carousel-inner" role="listbox">
              <li class="carousel-item active" role="option" aria-hidden="false">
          <a href="http://localhost/prestashop/fr/jeux-de-role/20-dungeons-dragons-kit-d-initiation.html">
            <figure>
              <img src="http://localhost/prestashop/modules/ps_imageslider/images/64fb3e6c9e9d3bce5e55248e534a088d522506bb_tiamat-email.jpg" alt="" loading="lazy">
              <div class="carousel-overlay"></div>
                              <figcaption class="caption">
                  <h2 class="display-1 text-uppercase">Donjons &amp; Dragons</h2>
                  <div class="caption-description"></div>
                </figcaption>
                          </figure>
          </a>
        </li>
              <li class="carousel-item " role="option" aria-hidden="true">
          <a href="http://localhost/prestashop/fr/accueil/24-le-seigneur-des-anneaux-jce-edition-revisee.html">
            <figure>
              <img src="http://localhost/prestashop/modules/ps_imageslider/images/f8aa402c729017af408b51046f948084cc671346_LeSeigneurDesAnneaux-retourduroi_Header.jpg" alt="" loading="lazy">
              <div class="carousel-overlay"></div>
                              <figcaption class="caption">
                  <h2 class="display-1 text-uppercase">Le seigneur des anneaux</h2>
                  <div class="caption-description"></div>
                </figcaption>
                          </figure>
          </a>
        </li>
          </ul>
    <div class="direction" aria-label="Carousel buttons">
      <a class="left carousel-control" href="#carousel" role="button" data-slide="prev" aria-label="Previous">
        <span class="icon-prev hidden-xs" aria-hidden="true">
          <i class="material-icons">&#xE5CB;</i>
        </span>
      </a>
      <a class="right carousel-control" href="#carousel" role="button" data-slide="next" aria-label="Next">
        <span class="icon-next" aria-hidden="true">
          <i class="material-icons">&#xE5CC;</i>
        </span>
      </a>
    </div>
  </div>
<?php }
}
