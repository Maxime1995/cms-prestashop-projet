<?php
/* Smarty version 3.1.43, created on 2022-12-11 17:00:16
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395fe904b9cd3_63312638',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1670238247,
      2 => 'module',
    ),
    '446b29b756c2d8caf4535750076fc6e8462eb0a8' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\myshop\\templates\\catalog\\_partials\\productlist.tpl',
      1 => 1670238247,
      2 => 'file',
    ),
    'a33c0e267f8231c08eb2e611298eb2a211a4993a' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\myshop\\templates\\catalog\\_partials\\miniatures\\product.tpl',
      1 => 1670238247,
      2 => 'file',
    ),
    'f8095d2f6accade89ed95224b6bcae25aa46c76d' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\myshop\\templates\\catalog\\_partials\\product-flags.tpl',
      1 => 1670238247,
      2 => 'file',
    ),
    'a935379d4302adf96a753e6456f5de7867cf1eee' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\myshop\\templates\\catalog\\_partials\\product-add-to-cart.tpl',
      1 => 1670238247,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6395fe904b9cd3_63312638 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Popular Products
  </h2>
  
    <div class=" product-listing products row" itemscope itemtype="http://schema.org/ItemList">
            


<!-- shop-section start -->
    <div class="col-lg-3 col-md-4 shop-block-one product-miniature js-product-miniature" data-id-product="20" data-id-product-attribute="0">
        <div class="inner-box product">
          <div class="product_inside thumbnail-container">

            
                              <figure class="image-box">
                    <a href="http://localhost/prestashop/fr/jeux-de-role/20-dungeons-dragons-kit-d-initiation.html" class="thumbnail product-thumbnail">
                      <img data-src="http://localhost/prestashop/24-home_default/dungeons-dragons-kit-d-initiation.jpg" alt="Dungeons &amp; Dragons : Kit..." class="lazy" data-full-size-image-url="http://localhost/prestashop/24-large_default/dungeons-dragons-kit-d-initiation.jpg"/>
                    </a>
                    
    <ul class="product-flags">
                    <span class="category green-bg new">New</span>
            </ul>

                    <ul class="info-list clearfix">
                        <li>
                          
                            <a class="quick-view" href="#" data-link-action="quickview">
                              <i class="material-icons">remove_red_eye</i> Quick view
                            </a>
                          
                        </li>
                    </ul>
                </figure>
                          


            <div class="lower-content">
              
                                    <h2 class="title"><a href="http://localhost/prestashop/fr/jeux-de-role/20-dungeons-dragons-kit-d-initiation.html" itemprop="url" content="http://localhost/prestashop/fr/jeux-de-role/20-dungeons-dragons-kit-d-initiation.html">Dungeons &amp; Dragons : Kit...</a></h2>
                              
    
              
                                  <div class="theme-price-and-shipping">
                    
                    

                    <span class="price" aria-label="Price">
                                                                    23,29 €
                                          </span>

                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                      <meta itemprop="priceCurrency" content="EUR" />
                      <meta itemprop="price" content="23.29" />
                    </div>

                    

                    
                  </div>
                              

              
                <div id="product-description-short-20" class="product-content" itemprop="description"></div>
              

              <div class="product_inside_hover">

                
                                  

                
                  <div class="product-addition">
                    <form action="http://localhost/prestashop/fr/panier" method="post" id="add-to-cart-or-refresh">
                      <input type="hidden" name="token" value="c7e6fe3de5456c4dc8ec099f5e192d16">
                      <input type="hidden" name="id_product" value="20" id="product_page_product_id">
                      
                        <div class="product-add-to-cart">
      <span class="control-label">Quantity</span>
    
      <div class="product-quantity clearfix">
        <div class="qty">
          <input
            type="number"
            name="qty"
            id="quantity_wanted"
                          value="1"
              min="1"
                        class="input-group"
            aria-label="Quantity"
          >
        </div>

        <div class="add">
          <button
            class="btn btn-primary add-to-cart"
            data-button-action="add-to-cart"
            type="submit"
                      >
            <i class="material-icons shopping-cart">&#xE547;</i>
            Add to cart
          </button>
        </div>

        
      </div>
    

    
         

    
        
  </div>
                      
                    </form>
                  </div>
                

              </div>

              
                
<div class="product-list-reviews" data-id="20" data-url="http://localhost/prestashop/fr/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


              
            </div>


          </div>
        </div>
    </div>
<!-- shop-section end -->
            


<!-- shop-section start -->
    <div class="col-lg-3 col-md-4 shop-block-one product-miniature js-product-miniature" data-id-product="24" data-id-product-attribute="0">
        <div class="inner-box product">
          <div class="product_inside thumbnail-container">

            
                              <figure class="image-box">
                    <a href="http://localhost/prestashop/fr/accueil/24-le-seigneur-des-anneaux-jce-edition-revisee.html" class="thumbnail product-thumbnail">
                      <img data-src="http://localhost/prestashop/27-home_default/le-seigneur-des-anneaux-jce-edition-revisee.jpg" alt="Le Seigneur des Anneaux JCE..." class="lazy" data-full-size-image-url="http://localhost/prestashop/27-large_default/le-seigneur-des-anneaux-jce-edition-revisee.jpg"/>
                    </a>
                    
    <ul class="product-flags">
                    <span class="category green-bg new">New</span>
            </ul>

                    <ul class="info-list clearfix">
                        <li>
                          
                            <a class="quick-view" href="#" data-link-action="quickview">
                              <i class="material-icons">remove_red_eye</i> Quick view
                            </a>
                          
                        </li>
                    </ul>
                </figure>
                          


            <div class="lower-content">
              
                                    <h2 class="title"><a href="http://localhost/prestashop/fr/accueil/24-le-seigneur-des-anneaux-jce-edition-revisee.html" itemprop="url" content="http://localhost/prestashop/fr/accueil/24-le-seigneur-des-anneaux-jce-edition-revisee.html">Le Seigneur des Anneaux JCE...</a></h2>
                              
    
              
                                  <div class="theme-price-and-shipping">
                    
                    

                    <span class="price" aria-label="Price">
                                                                    59,90 €
                                          </span>

                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                      <meta itemprop="priceCurrency" content="EUR" />
                      <meta itemprop="price" content="59.9" />
                    </div>

                    

                    
                  </div>
                              

              
                <div id="product-description-short-24" class="product-content" itemprop="description"></div>
              

              <div class="product_inside_hover">

                
                                  

                
                  <div class="product-addition">
                    <form action="http://localhost/prestashop/fr/panier" method="post" id="add-to-cart-or-refresh">
                      <input type="hidden" name="token" value="c7e6fe3de5456c4dc8ec099f5e192d16">
                      <input type="hidden" name="id_product" value="24" id="product_page_product_id">
                      
                        <div class="product-add-to-cart">
      <span class="control-label">Quantity</span>
    
      <div class="product-quantity clearfix">
        <div class="qty">
          <input
            type="number"
            name="qty"
            id="quantity_wanted"
                          value="1"
              min="1"
                        class="input-group"
            aria-label="Quantity"
          >
        </div>

        <div class="add">
          <button
            class="btn btn-primary add-to-cart"
            data-button-action="add-to-cart"
            type="submit"
                      >
            <i class="material-icons shopping-cart">&#xE547;</i>
            Add to cart
          </button>
        </div>

        
      </div>
    

    
         

    
        
  </div>
                      
                    </form>
                  </div>
                

              </div>

              
                
<div class="product-list-reviews" data-id="24" data-url="http://localhost/prestashop/fr/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


              
            </div>


          </div>
        </div>
    </div>
<!-- shop-section end -->
            


<!-- shop-section start -->
    <div class="col-lg-3 col-md-4 shop-block-one product-miniature js-product-miniature" data-id-product="23" data-id-product-attribute="0">
        <div class="inner-box product">
          <div class="product_inside thumbnail-container">

            
                              <figure class="image-box">
                    <a href="http://localhost/prestashop/fr/jeux-de-societe/23-dungeons-dragons-les-tyrans-de-l-ombreterre.html" class="thumbnail product-thumbnail">
                      <img data-src="http://localhost/prestashop/26-home_default/dungeons-dragons-les-tyrans-de-l-ombreterre.jpg" alt="Dungeons &amp; Dragons : Les..." class="lazy" data-full-size-image-url="http://localhost/prestashop/26-large_default/dungeons-dragons-les-tyrans-de-l-ombreterre.jpg"/>
                    </a>
                    
    <ul class="product-flags">
                    <span class="category green-bg new">New</span>
            </ul>

                    <ul class="info-list clearfix">
                        <li>
                          
                            <a class="quick-view" href="#" data-link-action="quickview">
                              <i class="material-icons">remove_red_eye</i> Quick view
                            </a>
                          
                        </li>
                    </ul>
                </figure>
                          


            <div class="lower-content">
              
                                    <h2 class="title"><a href="http://localhost/prestashop/fr/jeux-de-societe/23-dungeons-dragons-les-tyrans-de-l-ombreterre.html" itemprop="url" content="http://localhost/prestashop/fr/jeux-de-societe/23-dungeons-dragons-les-tyrans-de-l-ombreterre.html">Dungeons &amp; Dragons : Les...</a></h2>
                              
    
              
                                  <div class="theme-price-and-shipping">
                    
                    

                    <span class="price" aria-label="Price">
                                                                    62,92 €
                                          </span>

                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                      <meta itemprop="priceCurrency" content="EUR" />
                      <meta itemprop="price" content="62.92" />
                    </div>

                    

                    
                  </div>
                              

              
                <div id="product-description-short-23" class="product-content" itemprop="description"></div>
              

              <div class="product_inside_hover">

                
                                  

                
                  <div class="product-addition">
                    <form action="http://localhost/prestashop/fr/panier" method="post" id="add-to-cart-or-refresh">
                      <input type="hidden" name="token" value="c7e6fe3de5456c4dc8ec099f5e192d16">
                      <input type="hidden" name="id_product" value="23" id="product_page_product_id">
                      
                        <div class="product-add-to-cart">
      <span class="control-label">Quantity</span>
    
      <div class="product-quantity clearfix">
        <div class="qty">
          <input
            type="number"
            name="qty"
            id="quantity_wanted"
                          value="1"
              min="1"
                        class="input-group"
            aria-label="Quantity"
          >
        </div>

        <div class="add">
          <button
            class="btn btn-primary add-to-cart"
            data-button-action="add-to-cart"
            type="submit"
                      >
            <i class="material-icons shopping-cart">&#xE547;</i>
            Add to cart
          </button>
        </div>

        
      </div>
    

    
         

    
        
  </div>
                      
                    </form>
                  </div>
                

              </div>

              
                
<div class="product-list-reviews" data-id="23" data-url="http://localhost/prestashop/fr/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


              
            </div>


          </div>
        </div>
    </div>
<!-- shop-section end -->
    </div>  <div class="align-centered">
    <a class="all-product-link btn h4" href="http://localhost/prestashop/fr/2-accueil">
      All products<i class="material-icons">&#xE315;</i>
    </a>
  </div>
</section>
<?php }
}
