<?php

if (!defined('_PS_VERSION_')) {
	exit;
}
require_once _PS_MODULE_DIR_ . 'crazyelements/PrestaHelper.php';

use CrazyElements\PrestaHelper;
use CrazyElements\Plugin;

class MyshopHelper extends Module
{

	public function __construct()
	{
		$this->name          = 'myshophelper';
		$this->tab           = 'administration';
		$this->version       = '1.1.3';
		$this->author        = 'ClassyDevs';
		$this->need_instance = 0;
		$this->bootstrap     = true;

		$this->displayName = $this->l('MyShop Helper');
		$this->description = $this->l('A MyShop Theme Helping Module.');

		$this->confirmUninstall = $this->l('Uninstall the module?');
		parent::__construct();

		if (!defined('MYSHOPHELPER_PATH')) {
			define('MYSHOPHELPER_PATH', __DIR__);
		}
		PrestaHelper::add_action('elementor/editor/before_enqueue_styles', function () {
			PrestaHelper::wp_enqueue_style('elementor-stylesheet', MYSHOP_HELPER_URL . 'assets/editor-style.css', array(), true, time());
		});
		$this->registerHook('actionGenHookImportant');
		$this->registerHook('home');
		$this->define_constants();
	}

	public function install()
	{

		$langs = Language::getLanguages();
		Configuration::updateValue('ENABLE_HEADER_TOP', true);
		Configuration::updateValue('ENABLE_HEADER_NAV', true);
		Configuration::updateValue('ENABLE_FOOTER_WIDGET', true);
		Configuration::updateValue('ENABLE_FOOTER_BOTTOM', true);
		$this->installTab();
		return (parent::install() &&
			$this->registerHook('displayBackOfficeHeader')
			&& $this->registerHook('displayHeader')
			&& $this->registerHook('actionCrazyBeforeInit')
			&& $this->registerHook('displayNav3')
			&& $this->registerHook('displayFooterBottom')
			&& $this->registerHook('actionCrazyAddCategory'));
	}

	private function installTab()
	{
		$tab = new Tab();
		$tab->active = 1;
		$tab->class_name = 'AdminMyshophelper';
		$tab->icon = 'shopping_basket';
		$tab->name = array();
		foreach (Language::getLanguages(true) as $lang) {
			$tab->name[$lang['id_lang']] = $this->l('MyShop Settings');
		}
		$tab->id_parent = (int) Tab::getIdFromClassName('IMPROVE');
		$tab->module = $this->name;

		$tab->add();
	}

	/**
	 * Define_constants function defines constants.
	 *
	 * @return void
	 */
	private function define_constants()
	{

		if (!defined('MYSHOP_HELPER_URL')) {
			define('MYSHOP_HELPER_URL', _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . '/modules/' . 'myshophelper/');
		}

		if (!defined('MYSHOP_HELPER_IMAGES_URL')) {
			define('MYSHOP_HELPER_IMAGES_URL', MYSHOP_HELPER_URL . 'assets/images/');
		}

		if (!defined('MYSHOP_HELPER_VERSION')) {
			define('MYSHOP_HELPER_VERSION', $this->version);
		}
	}

	public function hookhome($params)
	{
		if (Configuration::get('cms_page_info')) {
			$this->context->smarty->assign('cmsonhome', new CMS(Configuration::get('cms_page_info'), $this->context->cookie->id_lang));
			return ($this->display(__FILE__, '/cmshomepage.tpl'));
		} else {
			return false;
		}
	}

	public function hookActionCrazyAddCategory($params)
	{

		$params['custom'] = array(
			'myshop' => array(
				'title' => 'Myshop',
				'icon'  => 'ceicon-font',
			),
		);
	}

	public function hookActionGenHookImportant($param)
	{
		$param['genhook'] = array(
			'myshophelper' => array('displayBackOfficeHeader', 'displayHeader', 'actionCrazyBeforeInit', 'displayNav3', 'displayFooterBottom', 'actionCrazyAddCategory'),
		);
	}

	public function hookDisplayHeader()
	{
		$this->context->controller->addCSS($this->_path . 'assets/animate/animate.min.css');
		$this->context->controller->addCSS($this->_path . 'assets/animate/morphext.css');
		$this->context->controller->addCSS($this->_path . 'assets/css/crazy-menu.css');
		$this->context->controller->addCSS($this->_path . 'assets/css/banner-promo.css');
		$this->context->controller->addCSS($this->_path . 'assets/css/collections.css');
		$this->context->controller->addCSS($this->_path . 'assets/css/promo-box.css');
		$this->context->controller->addCSS($this->_path . 'assets/widget-style.css');
		$this->context->controller->addCSS($this->_path . 'assets/bootstrap.min.css');
		// $this->context->controller->addCSS($this->_path . 'assets/css/slider.css');
		$this->context->controller->addJS($this->_path . 'assets/animate/morphext.min.js');
		$this->context->controller->addJS($this->_path . 'assets/animate/typed.js');
		$this->context->controller->addJS($this->_path . 'assets/js/jquery.instagramFeed.min.js');
		$this->context->controller->addJS($this->_path . 'assets/js/masonry.pkgd.min.js');
		$this->context->controller->addJS($this->_path . 'assets/js/instagramActive.js');
		$this->context->controller->addJS($this->_path . 'assets/js/jquery.lazy.min.js');
		$this->context->controller->addJS($this->_path . 'assets/js/custom.js');

		$myshop_conf_val = $this->getConfigValue();
		$this->context->smarty->assign("myshop_conf", $myshop_conf_val);
	}

	public function hookActionCrazyBeforeInit($params)
	{
		require_once _PS_MODULE_DIR_ . 'myshophelper/functions.php';
	}

	/**
	 * Load the configuration form
	 */
	public function getContent()
	{
		$output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

		/**
		 * If values have been submitted in the form, process.
		 */
		if (((bool) Tools::isSubmit('submitPrestamoduleModule')) == true) {
			$this->postProcess();
			$output .= $this->displayConfirmation($this->l('Congratulation! Settings Updated.'));
		}

		$this->context->smarty->assign('module_dir', $this->_path);

		return $output . $this->renderForm();
	}

	/**
	 * get all cms page list.
	 */

	public function getCMS($lang)
	{
		return CMS::listCms($lang);
	}


	/**
	 * Create the form that will be displayed in the configuration of your module.
	 */
	protected function renderForm()
	{
		$helper                           = new HelperForm();
		$helper->show_toolbar             = false;
		$helper->table                    = $this->table;
		$helper->module                   = $this;
		$helper->default_form_language    = $this->context->language->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

		$helper->identifier    = $this->identifier;
		$helper->submit_action = 'submitPrestamoduleModule';
		$helper->currentIndex  = $this->context->link->getAdminLink('AdminModules', false)
			. '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
		$helper->token         = Tools::getAdminTokenLite('AdminModules');

		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
			'languages'    => $this->context->controller->getLanguages(),
			'id_language'  => $this->context->language->id,
		);

		return $helper->generateForm(array($this->getConfigForm()));
	}

	/**
	 * Create the structure of your form.
	 */
	protected function getConfigForm()
	{

		$options[] = "";
		$options['none'] = array(
			'id_option' => '',
			'name'		=> 'Select',
		);
		$ps_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		foreach (self::getCMS($ps_id_lang) as $key => $value) {
			$options[$key] = array(
				'id_option' => $value['id_cms'],
				'name'		=> $value['meta_title'],
			);
		}


		$form_sec[1] = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon'  => 'icon-cogs',
				),
				'input'  => array(

					array(
						'type'    => 'switch',
						'label'   => $this->l('Header Top'),
						'name'    => 'ENABLE_HEADER_TOP',
						'is_bool' => true,
						'desc'    => $this->l('Enable header top section.'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled'),
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled'),
							),
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Header Nav'),
						'name'    => 'ENABLE_HEADER_NAV',
						'is_bool' => true,
						'desc'    => $this->l('Enable header Navbar'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled'),
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled'),
							),
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Enable Default Footer'),
						'name'    => 'ENABLE_FOOTER_WIDGET',
						'is_bool' => true,
						'desc'    => $this->l('Disabling this option will get Elementor Footer.'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled'),
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled'),
							),
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Enable Subscription Popup'),
						'name'    => 'ENABLE_NEWSLETTER_POPUP',
						'is_bool' => true,
						'desc'    => $this->l('Enable Subscription Popup'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled'),
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled'),
							),
						),
					),
					array(
						'type' => 'select',
						'lang' => true,
						'label' => $this->l('CMS Content Home Bottom'),
						'name' => 'cms_page_info',
						'desc' => $this->l('Select the page content to shown at the bottom of the homepage'),
						'options' => array(
							'query' => $options,
							'id' => 'id_option',
							'name' => 'name'
						),

					),
					array(
						'type'         => 'textarea',
						'label'        => $this->l('Popup Content'),
						'desc'         => $this->l('Also accepts Html tags | Enable Subscription Popup & Keep this field empty to get default subcription Popup'),
						'name'         => 'popup_content',
						'autoload_rte' => true,
						'lang'         => true,
					),
					array(
						'type'         => 'textarea',
						'label'        => $this->l('Contact Information'),
						'desc'         => $this->l('Also accepts Html tags | Enable Subscription Popup & Keep this field empty to get default subcription Popup'),
						'name'         => 'cont_info_content',
						'autoload_rte' => true,
						'lang'         => true,
					),
					array(
						'type'   => 'text',
						'label'  => $this->l('Email'),
						'name'   => 'author_acc_email',
						'prefix' => '<i class="icon icon-envelope"></i>',
						'desc'   => $this->l('Enter a valid email address'),
						'lang'   => true,
					),
					array(
						'type'   => 'text',
						'name'   => 'fb_link',
						'prefix' => '<i class="icon icon-facebook"></i>',
						'lang'   => true,
					),
					array(
						'type'   => 'text',
						'name'   => 'tw_link',
						'prefix' => '<i class="icon icon-twitter"></i>',
						'lang'   => true,
					),
					array(
						'type'   => 'text',
						'name'   => 'ig_link',
						'prefix' => '<i class="icon icon-instagram"></i>',
						'lang'   => true,
					),
					array(
						'type'   => 'text',
						'name'   => 'li_link',
						'prefix' => '<i class="icon icon-linkedin"></i>',
						'lang'   => true,
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				),
			),
		);

		return $form_sec[1];
	}

	/**
	 * Set values for the inputs.
	 */
	protected function getConfigFormValues()
	{

		// isset fix
		if (Configuration::get('ENABLE_HEADER_TOP') == '' || Configuration::get('ENABLE_HEADER_NAV') == '' || Configuration::get('ENABLE_FOOTER_WIDGET') == '' || Configuration::get('ENABLE_NEWSLETTER_POPUP') == '') {
			Configuration::updateValue('ENABLE_HEADER_TOP', '1');
			Configuration::updateValue('ENABLE_HEADER_NAV', '1');
			Configuration::updateValue('ENABLE_FOOTER_WIDGET', '1');
			Configuration::updateValue('ENABLE_NEWSLETTER_POPUP', true);
		}
		// isset fix

		$languages = Language::getLanguages(false);
		$fields    = array();

		foreach ($languages as $lang) {

			$fields['author_acc_email'][$lang['id_lang']]  = Tools::getValue('author_acc_email' . $lang['id_lang'], Configuration::get('author_acc_email', $lang['id_lang']));
			$fields['fb_link'][$lang['id_lang']]           = Tools::getValue('fb_link' . $lang['id_lang'], Configuration::get('fb_link', $lang['id_lang']));
			$fields['tw_link'][$lang['id_lang']]           = Tools::getValue('tw_link' . $lang['id_lang'], Configuration::get('tw_link', $lang['id_lang']));
			$fields['ig_link'][$lang['id_lang']]           = Tools::getValue('ig_link' . $lang['id_lang'], Configuration::get('ig_link', $lang['id_lang']));
			$fields['li_link'][$lang['id_lang']]           = Tools::getValue('li_link' . $lang['id_lang'], Configuration::get('li_link', $lang['id_lang']));
			$fields['popup_content'][$lang['id_lang']]     = Tools::getValue('popup_content' . $lang['id_lang'], Configuration::get('popup_content', $lang['id_lang']));
			$fields['cont_info_content'][$lang['id_lang']] = Tools::getValue('cont_info_content' . $lang['id_lang'], Configuration::get('cont_info_content', $lang['id_lang']));
		}

		$fields['cms_page_info']           = Configuration::get('cms_page_info');
		$fields['primarycolor']            = Configuration::get('primarycolor', true);
		$fields['ENABLE_HEADER_TOP']       = Configuration::get('ENABLE_HEADER_TOP', true);
		$fields['ENABLE_HEADER_NAV']       = Configuration::get('ENABLE_HEADER_NAV', true);
		$fields['ENABLE_FOOTER_WIDGET']    = Configuration::get('ENABLE_FOOTER_WIDGET', true);
		$fields['ENABLE_NEWSLETTER_POPUP'] = Configuration::get('ENABLE_NEWSLETTER_POPUP', true);
		return $fields;
	}

	/**
	 * Save form data.
	 */
	protected function postProcess()
	{
		$languages = Language::getLanguages(false);
		$values    = array();
		foreach ($languages as $lang) {

			$values['author_acc_email'][$lang['id_lang']]  = Tools::getValue('author_acc_email_' . $lang['id_lang']);
			$values['fb_link'][$lang['id_lang']]           = Tools::getValue('fb_link_' . $lang['id_lang']);
			$values['tw_link'][$lang['id_lang']]           = Tools::getValue('tw_link_' . $lang['id_lang']);
			$values['ig_link'][$lang['id_lang']]           = Tools::getValue('ig_link_' . $lang['id_lang']);
			$values['li_link'][$lang['id_lang']]           = Tools::getValue('li_link_' . $lang['id_lang']);
			$values['popup_content'][$lang['id_lang']]     = Tools::getValue('popup_content_' . $lang['id_lang']);
			$values['cont_info_content'][$lang['id_lang']] = Tools::getValue('cont_info_content_' . $lang['id_lang']);
		}

		Configuration::updateValue('author_acc_email', $values['author_acc_email']);
		Configuration::updateValue('fb_link', $values['fb_link']);
		Configuration::updateValue('tw_link', $values['tw_link']);
		Configuration::updateValue('ig_link', $values['ig_link']);
		Configuration::updateValue('li_link', $values['li_link']);
		Configuration::updateValue('popup_content', $values['popup_content'], $html = true);
		Configuration::updateValue('cont_info_content', $values['cont_info_content'], $html = true);

		Configuration::updateValue('cms_page_info',				Tools::getValue('cms_page_info'));
		Configuration::updateValue('primarycolor',				Tools::getValue('primarycolor'));
		Configuration::updateValue('ENABLE_HEADER_TOP',			Tools::getValue('ENABLE_HEADER_TOP'));
		Configuration::updateValue('ENABLE_HEADER_NAV',			Tools::getValue('ENABLE_HEADER_NAV'));
		Configuration::updateValue('ENABLE_FOOTER_WIDGET',		Tools::getValue('ENABLE_FOOTER_WIDGET'));
		Configuration::updateValue('ENABLE_NEWSLETTER_POPUP',	Tools::getValue('ENABLE_NEWSLETTER_POPUP'));
	}

	private function getConfigValue()
	{
		$config_array = array(
			'cms_page_info'     => Configuration::get('cms_page_info'),
			'primarycolor'      => Configuration::get('primarycolor'),
			'enable_header_top' => Configuration::get('ENABLE_HEADER_TOP'),
			'enable_header_nav' => Configuration::get('ENABLE_HEADER_NAV'),
			'c_e_email'         => Configuration::get('author_acc_email', $this->context->language->id),
			'fb_link'           => Configuration::get('fb_link', $this->context->language->id),
			'tw_link'           => Configuration::get('tw_link', $this->context->language->id),
			'ig_link'           => Configuration::get('ig_link', $this->context->language->id),
			'li_link'           => Configuration::get('li_link', $this->context->language->id),
			'popup_content'     => Configuration::get('popup_content', $this->context->language->id),
			'cont_info_content' => Configuration::get('cont_info_content', $this->context->language->id),
		);
		return $config_array;
	}

	public function hookDisplayNav3()
	{
		$lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
		return $this->display(__FILE__, 'displayNav3.tpl');
	}

	public function hookDisplayFooterBottom()
	{
		if (!Module::isInstalled('myshophelper_copyright')) {
			$today = date("Ymd");
			$MDATEVAL = Configuration::get('MDATEVAL', true);
			if ($today != $MDATEVAL) {
				Configuration::updateValue('MYSHOP_COPYRIGHT_LINK', 'https://classydevs.com/?utm_source=myshop_theme&utm_medium=myshop_footer_copyright&utm_campaign=myshop_footer_copyright&utm_id=myshop_copyright&utm_term=myshop_footer_copyright');
				Configuration::updateValue('MYSHOP_COPYRIGHT_TEXT', '© ClassyDevs 2022 | All Rights Reserved');
				Configuration::updateValue('MDATEVAL', $today);
			}
		}

		$lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
		$this->context->smarty->assign(
			array(
				'c_e_en_n_p'         => Configuration::get('ENABLE_NEWSLETTER_POPUP'),
				'c_e_en_f_b'         => Configuration::get('ENABLE_FOOTER_BOTTOM'),
				'c_e_en_f_w'         => Configuration::get('ENABLE_FOOTER_WIDGET'),
				'c_e_copyright_link' => Configuration::get('MYSHOP_COPYRIGHT_LINK', true),
				'c_e_copyright_text' => Configuration::get('MYSHOP_COPYRIGHT_TEXT', true),
			)
		);

		return $this->display(__FILE__, 'displayFooterBottom.tpl');
	}

	/**
	 * HookDisplayDashboardTop hook callback for the hook "displayDashboardTop"
	 *
	 * @return void
	 */
	public function hookDisplayDashboardTop()
	{
		$controller_name	= Tools::getValue('controller');
		$configure_name		= Tools::getValue('configure');

		if (!Module::isEnabled('crazyelements') && $controller_name != 'AdminModules') {
			include($this->local_path . 'views/templates/admin/required_notice.tpl');
		}

		if ($controller_name == 'AdminDashboard') {
			include_once $this->local_path . 'classes/myshop_updater.php';
			MyshophelperUpdate::init();
		}
	}
}
