<?php

if (!defined('_PS_VERSION_')) {
	exit;
}

/**
 * MyshophelperUpdate updater class for the module.
 */
class MyshophelperUpdate
{

	private $product_id = 40068;
	private $store_url  = 'https://classydevs.com/';

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct()
	{

		$purchase_code	= Configuration::get('MYSHOP_LICENSE');
		$todate			= Configuration::get('MYSHOP_DATE');
		$today			= date('Y-m-d');

		$stable = Configuration::get('MYSHOP_STABLE');
		$d_link = Configuration::get('MYSHOP_DLINK');
		$description	= Configuration::get('MYSHOP_DESC');

		if (isset($stable) && isset($d_link)) {
			if ($today != $todate || $stable == '' && $d_link == '') {
				Configuration::updateValue('MYSHOP_DATE', $today);
				$this->myshop_get_update($purchase_code);
			} elseif (version_compare($stable, MYSHOP_HELPER_VERSION, '>')) {
				$this->show_notification($stable, $d_link, $description);
			}
		}
	}

	/**
	 * Classypetab_get_update checks the api if the module has update available.
	 */
	public function myshop_get_update($key)
	{
		$api_params = array(
			'edd_action' => 'get_version',
			'item_id'    => $this->product_id,
			'license'    => $key,
			'version'    => MYSHOP_HELPER_VERSION,
			'url'        => _PS_BASE_URL_SSL_,
		);
		$url        = $this->store_url . '?' . http_build_query($api_params);

		$response = $this->wp_remote_get(
			$url,
			array(
				'timeout' => 20,
				'headers' => '',
				'header'  => false,
				'json'    => true,
			)
		);

		$responsearray = Tools::jsonDecode($response, true);



		if (version_compare($responsearray['stable_version'], MYSHOP_HELPER_VERSION, '>')) {
			$d_link = $responsearray['download_link'];
			$description = unserialize($responsearray['sections']);
			$description = trim($description['changelog']);
			Configuration::updateValue('MYSHOP_STABLE', $responsearray['stable_version']);
			Configuration::updateValue('MYSHOP_DLINK', $d_link);
			Configuration::updateValue('MYSHOP_DESC', $description);

			$this->show_notification($responsearray['stable_version'], $d_link, $description);
		}
	}

	public function classypetab_activate_license($key)
	{
		$api_params = array(
			'edd_action' => 'activate_license',
			'item_id'    => $this->product_id,
			'license'    => $key,
			'url'        => _PS_BASE_URL_SSL_,
		);

		$url = $this->store_url . '?' . http_build_query($api_params);

		$response = $this->wp_remote_get(
			$url,
			array(
				'timeout' => 20,
				'headers' => '',
				'header'  => false,
				'json'    => true,
			)
		);

		$responsearray = Tools::jsonDecode($response, true);

		if ($responsearray['success'] && $responsearray['license'] == 'valid') {
			Configuration::updateValue('MYSHOP_LICENSE_STATUS', 'true');
			return true;
		} else {
			Configuration::updateValue('MYSHOP_LICENSE_STATUS', 'false');
			return false;
		}
	}

	private function wp_remote_get($url, $args = array())
	{
		return $this->getHttpCurl($url, $args);
	}

	private function getHttpCurl($url, $args)
	{
		global $wp_version;
		if (function_exists('curl_init')) {
			$defaults = array(
				'method'      => 'GET',
				'timeout'     => 30,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking'    => true,
				'headers'     => array(
					'Authorization'   => 'Basic ',
					'Content-Type'    => 'application/x-www-form-urlencoded;charset=UTF-8',
					'Accept-Encoding' => 'x-gzip,gzip,deflate',
				),
				'body'        => array(),
				'cookies'     => array(),
				'user-agent'  => 'Prestashop' . $wp_version,
				'header'      => true,
				'sslverify'   => false,
				'json'        => false,
			);

			$args         = array_merge($defaults, $args);
			$curl_timeout = ceil($args['timeout']);
			$curl         = curl_init();
			if ($args['httpversion'] == '1.0') {
				curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
			} else {
				curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			}
			curl_setopt($curl, CURLOPT_USERAGENT, $args['user-agent']);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $curl_timeout);
			curl_setopt($curl, CURLOPT_TIMEOUT, $curl_timeout);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, 'api=true');
			$ssl_verify = $args['sslverify'];
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $ssl_verify);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, ($ssl_verify === true) ? 2 : false);
			$http_headers = array();
			if ($args['header']) {
				curl_setopt($curl, CURLOPT_HEADER, $args['header']);
				foreach ($args['headers'] as $key => $value) {
					$http_headers[] = "{$key}: {$value}";
				}
			}
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
			if (defined('CURLOPT_PROTOCOLS')) { // PHP 5.2.10 / cURL 7.19.4
				curl_setopt($curl, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
			}
			if (is_array($args['body']) || is_object($args['body'])) {
				$args['body'] = http_build_query($args['body']);
			}
			$http_headers[] = 'Content-Length: ' . strlen($args['body']);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($curl);
			if ($args['json']) {
				return $response;
			}
			$header_size    = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
			$responseHeader = substr($response, 0, $header_size);
			$responseBody   = substr($response, $header_size);
			$error          = curl_error($curl);
			$errorcode      = curl_errno($curl);
			$info           = curl_getinfo($curl);
			curl_close($curl);
			$info_as_response            = $info;
			$info_as_response['code']    = $info['http_code'];
			$info_as_response['message'] = 'OK';
			$response                    = array(
				'body'     => $responseBody,
				'headers'  => $responseHeader,
				'info'     => $info,
				'response' => $info_as_response,
				'error'    => $error,
				'errno'    => $errorcode,
			);
			return $response;
		}
		return false;
	}

	private function show_notification($v, $d, $ds = "")
	{
		$msg = 'There is a new version of MyShop is available.';

		$controller_name	= Tools::getValue('controller');
		if ($controller_name != 'AdminModules') {
?>
			<div class="bootstrap row">
				<div class="col-lg-12">
					<div class="update-content-area">
						<div class="update-logo-and-text">
							<img src="<?php echo MYSHOP_HELPER_IMAGES_URL . 'logo.png'; ?>" width="90" height="90">
							<div class="update-header-text-and-version">
								<h4 class="update_msg"><?php echo $msg; ?></h4>
								<h6 class="update_vsn"><?php echo 'Version: ' . $v; ?></h6>
								<?php if ($ds != "") {
									echo $ds;
								} ?>
							</div>
						</div>
						<div>
							<a target="_blank" href="https://classydevs.com/docs/myshop-multipurpose-prestashop-theme/getting-startted/how-to-install-2/?utm_source=dashboardtop-hook&utm_medium=upgrade-notification&utm_campaign=upgrade-notice&utm_id=myshop-upgrade&utm_term=free-upgrade&utm_content=upgrade-to-new-version" class="btn btn-success"><?php echo 'How to Update?'; ?></a>
							<a target="_blank" href="https://classydevs.com/myshop-multipurpose-prestashop-free-theme-download/?utm_source=classydevs&utm_medium=upgrade+notification&utm_campaign=api&utm_id=Myshop+update&utm_term=upgrade+notification&utm_content=get+update" class="btn btn-primary"><?php echo 'Update To <strong>Version ' . $v . '</strong>'; ?></a>
						</div>

					</div>
				</div>
			</div>
<?php
		}
	}

	public static function init()
	{
		new MyshophelperUpdate();
	}
}
