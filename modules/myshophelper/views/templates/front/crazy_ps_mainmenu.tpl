{assign var=_counter value=0}

{function name="menu" nodes=[] depth=0 parent=null}
    {if $nodes|count}
      <ul class="nav navbar-nav">
        {foreach from=$nodes item=node}
            <li class="{$node.type}{if $node.current} active {/if}" id="{$node.page_identifier}">
            {assign var=_counter value=$_counter+1}
              <a class="{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown{/if}" href="{$node.url}" data-depth="{$depth}" {if $node.open_in_new_window} target="_blank" {/if}>
                {if $node.children|count}
                  {* Cannot use page identifier as we can have the same page several times *}
                  {assign var=_expand_id value=10|mt_rand:100000}
                  <span class="float-xs-right">
                    <span data-target="#top_sub_menu_{$_expand_id}" data-toggle="collapse" class="navbar-toggler collapse-icons">
                      <span class="caret"></span>
                    </span>
                  </span>
                {/if}
                {$node.label}
              </a>
              {if $node.children|count}
              <div {if $depth === 0} class="menu-dropdown sub-menu js-sub-menu collapse"{else} class="collapse"{/if} id="top_sub_menu_{$_expand_id}">
                {menu nodes=$node.children depth=$node.depth parent=$node}
              </div>
              {/if}
            </li>
        {/foreach}
      </ul>
    {/if}
{/function}

  {menu nodes=$menu.children}
  <div class="clearfix"></div>