{if $c_e_en_n_p != '0'}
    {if isset($popup_content) && $popup_content}
        {if !isset($smarty.cookies.mark_check) || $smarty.cookies.mark_check != 'checked'}
            <!-- Subscription Popup -->
                <div class="subscription-modal">
                    <button type="button" class="close">
                        <i class="material-icons">close</i>
                    </button>
                    <div class="html-popup-content">
                        {$popup_content nofilter}
                    </div>
                    <div class="popup-dont">
                        <span class="box"></span><label><input type="checkbox" value="" id="checkbox">Don’t show this popup again</label>
                    </div>
                </div>
                <div class="sub-popup-overlay">
                </div>
            <!-- Subscription Popup -->
        {/if}
    {else}
        {if !isset($smarty.cookies.mark_check) || $smarty.cookies.mark_check != 'checked'}
            <!-- Subscription Popup -->
                <div class="subscription-modal">
                    <button type="button" class="close">
                        <i class="material-icons">close</i>
                    </button>
                    <div class="popup-content">
                        {widget name="ps_emailsubscription"}
                    </div>
                    <div class="popup-dont">
                        <span class="box"></span><label><input type="checkbox" value="" id="checkbox">Don’t show this popup again</label>
                    </div>
                </div>
                <div class="sub-popup-overlay">
                </div>
            <!-- Subscription Popup -->
        {/if}
    {/if}
{/if}

{if $c_e_en_f_w != '0'}
    <!-- Footer Widget Section -->
    <div class="foooter-widget-section">
        <div class="container">
        <div class="row">
            {block name='hook_footer'}
                {hook h='displayFooter'}
            {/block}
        </div>
        </div>
    </div>
    <!-- Footer Widget Section -->
    {else}
    {block name='hook_footer'}
        {hook h='displayFooter'}
    {/block}
{/if}

{if $c_e_en_f_b != off}
    <!-- Footer Bottom Section -->
    <div class="foooter-bottom-section">
        <div class="container">
            <div class="row">
            <div class="col-md-12">
                {block name='copyright_link'}
                    {if $c_e_copyright_text}
                        <a href="{$c_e_copyright_link}" target="_blank" rel="noopener noreferrer nofollow">
                            {l s='%c_e_copyright_text%' sprintf=['%c_e_copyright_text%' => $c_e_copyright_text] d='Shop.Theme.Global'}
                        </a>
                    {else}
                        <a href="https://classydevs.com/?utm_source=myshop_theme&utm_medium=myshop_footer_copyright&utm_campaign=myshop_footer_copyright&utm_id=myshop_copyright&utm_term=myshop_footer_copyright" target="_blank" rel="noopener noreferrer nofollow">
                            {l s='%copyright% ClassyDevs %year% | All Rights Reserved' sprintf=['%prestashop%' => 'PrestaShop™', '%year%' => 'Y'|date, '%copyright%' => '©'] d='Shop.Theme.Global'}
                        </a>
                    {/if}
                {/block}
            </div>
            </div>
        </div>
    </div>
    <!-- Footer Bottom Section -->
{/if}