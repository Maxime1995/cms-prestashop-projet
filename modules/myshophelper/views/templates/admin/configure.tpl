{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="row">
	<div class="col-md-12">
		<div class="panel">
			<h1 style="color:#5fd179; font-family:'arial'; font-weight:bold;"><span class="icon icon-gears"></span> {l s='MyShop Helper' mod='prestamodule'}</h1>
			<span style="font-size:12px; color:#5fd179; margin:10px 0px;">{l s='A Classydevs Product' mod='prestamodule'}</span>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-4 col-lg-3">
		<div class="panel">
			<h2 class="panel-heading"><i class="icon-eye-open"></i> Documentation</h2>
			<p>Thanks for downloading MyShop Multipurpose PrestaShop Theme.</p>
			<p>Let’s go and see how to customize the theme.</p>
			<a class="btn btn-success" target="_blank" href="https://classydevs.com/docs/myshop-multipurpose-prestashop-theme/?utm_source=myshop-theme-backdoc&utm_medium=myshop-theme-backdoc&utm_campaign=myshop-theme-backdoc&utm_id=myshop-theme&utm_term=myshop-theme-backdoc&utm_content=myshop-theme-backdoc">
				<i class="icon-link"></i> Read Documentation
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3">
		<div class="panel">
			<h2 class="panel-heading"><i class="icon-link"></i> Support</h2>
			<p>We are confident that we can solve any kind of issues related to our module.</p>
			<p>Have no fear!!! Our support team is always ready to help you out!!!</p>
			<a class="btn btn-success" target="_blank" href="https://support.classydevs.com/">
				<i class="icon-link"></i> Open Support Ticket
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3">
		<div class="panel">
			<h2 class="panel-heading"><i class="icon-puzzle-piece"></i> Recommended Modules</h2>
			<p>MyShop is compatible with <b>SmartBlog</b></p>
			<p>One of the Most Powerful Blogging Tools For Prestashop that will help you to create beautiful blog page in your website.</p>
			<a class="btn btn-info" target="_blank" href="https://classydevs.com/free-modules/smartblog/?utm_source=smartblog_myshop&utm_medium=smartblog_myshop&utm_campaign=smartblog_myshop&utm_id=smartblog_myshop&utm_term=smartblog_myshop&utm_content=smartblog_myshop">Get SmartBlog</a>
			<a class="btn btn-info" target="_blank" href="https://classydevs.com/free-modules/smartblog/?utm_source=smartblog_myshop&utm_medium=smartblog_myshop&utm_campaign=smartblog_myshop&utm_id=smartblog_myshop&utm_term=smartblog_myshop&utm_content=smartblog_myshop#addonsone">Get Free Extensions</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3">
		<div class="panel">
			<h2 class="panel-heading"><i class="icon-fire"></i> Premium Addons</h2>
			<p>We have used some premium SmartBlog addons on our site to give it a professional look</p>
			<p>Get all addons as a bundle and save <b>35%!</b></p>
			<a class="btn btn-info" target="_blank" href="https://classydevs.com/free-modules/smartblog/?utm_source=smartblog_myshop&utm_medium=smartblog_myshop&utm_campaign=smartblog_myshop&utm_id=smartblog_myshop&utm_term=smartblog_myshop&utm_content=smartblog_myshop#DISCOUNT">Get Now</a>
		</div>
	</div>
</div>
