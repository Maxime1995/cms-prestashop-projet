<div class="row crazy_notice">
	<div class="col-md-12">
		<div class="panel">
			<h2 class="panel-heading"><i class="icon-key"></i> Activate Crazy Elements</h2>
            <div style="display: flex; align-items: center; gap: 20px;">
                <img style="border-radius:20px;" src="<?php echo MYSHOP_HELPER_IMAGES_URL . 'crazy_logo.png'; ?>" width="90" height="90">
                <div>
                    <p>You Need <b>Crazy Elements</b> to use MyShop theme Properly. Crazy Elements is currently inactive. Please activate Crazy Elements</p>
                    <a style="background:#5d408b;" class="btn btn-primary" target="_blank" href="https://classydevs.com/crazy-elements/?utm_source=dashboard_top_notice&utm_medium=myshop_notice&utm_campaign=activation_notice&utm_id=crazy_inactive_notice&utm_term=activation_notice&utm_content=activation_notice"><b>Crazy Elements</b></a>
                </div>
            </div>
		</div>
	</div>
</div>