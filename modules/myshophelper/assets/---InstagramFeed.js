/*
 * InstagramFeed
 *
 * @version 1.4.0
 *
 * @author Javier Sanahuja Liebana <bannss1@gmail.com>
 * @contributor csanahuja <csanahuja@gmail.com>
 *
 * https://github.com/jsanahuja/InstagramFeed
 *
 */

(function ($) { 

    "use strict";

    var blocks = {
        instagramFeed: $(".js-instagram-feed"),
    }

	function instagramFeed(el){
      //  $(window).on('load', function(){   
		$(el).each(function () { 

			var $el = $(this),
				tag = $el.data('tag'),
                username = $el.data('username'),
				limit = $el.data('limit'),
                id = "#" + $el.attr('id');
                
                console.log(username);
             //   console.log('text');
                
                new InstagramFeed({
                    'username': username,
                    'container': document.getElementById('#instagram'),
                    'display_profile': false,
                    'display_biography': true,
                    'display_gallery': true,
                    'callback': null,
                    'styling': true,
                    'items': 6,
                   'items_per_row': 6,
                    'margin': 0.5,
                    'lazy_load': true,
                    'on_error': console.error
                });
        
                (function(root, factory) {
                    if (typeof define === "function" && define.amd) {
                        define([], factory);
                    } else if (typeof exports === "object") {
                        module.exports = factory();
                    } else {
                        root.InstagramFeed = factory();
                    }
                });
                 /*
                setTimeout(function(){ 
                   
                    $('.owl-carousel').owlCarousel({
                        loop: true,
                        margin:10,
                        nav:true,
                        smartSpeed: 1000,
			            autoplay: 6000,
                        navText: '',
                        dots: false,
                        responsive:{
                            0:{
                                items:1
                            },
                            600:{
                                items:3
                            },
                            1000:{
                                items:per_column
                            }
                        }
                    })
                    
                }, 3000);
        */
			
            })
        //   })
        }
    
        if (blocks.instagramFeed.length) {
            instagramFeed(blocks.instagramFeed)
        }
    

})(window.jQuery);