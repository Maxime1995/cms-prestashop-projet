/*
 * InstagramFeed
 *
 * @version 1.4.0
 *
 * @author Javier Sanahuja Liebana <bannss1@gmail.com>
 * @contributor csanahuja <csanahuja@gmail.com>
 *
 * https://github.com/jsanahuja/InstagramFeed
 *
 */

(function ($) { 

    "use strict";

    var blocks = {
        instagramFeed: $(".js-instagram-feed"),
    }

	function instagramFeed(el){
        
		$(el).each(function () { 

			var $el = $(this),
				tag = $el.data('tag'),
                username = $el.data('username'),
				limit = $el.data('limit'),
				id = "#" + $el.attr('id');

				console.log(username)

			$.instagramFeed({
				'tag': tag,
				'username': username,
				'container': id,
				'display_profile': false,
				'display_biography': false,
				'display_gallery': true,
				'callback': null,
				'styling': false,
				'items': limit
			});

			function buildInstGrid(t) {
				if ($el.find('img').length) {
					clearInterval(t);
					$el.find('.instagram_gallery').addClass('instagram-grid')
					$el.find('img').each(function () {
						$(this).removeAttr('style');
					});
					$el.find('a').each(function () {
						$(this).append('<span class="icn"><i class="fas fa-eye"></i></span>');
					});
				}
			}
			
			var t = setInterval(function () {
				buildInstGrid(t);
			}, 100);
			
		})
	}

    if (blocks.instagramFeed.length) {
        instagramFeed(blocks.instagramFeed)
    }


})(window.jQuery);