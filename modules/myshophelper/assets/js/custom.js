(function ($) {
    "use strict";

    $(document).ready(function(){

      $(function() {
        $('.lazy').lazy();
      });

      $('body').on('DOMSubtreeModified', function(){
        $(function() {
          $('.lazy').lazy();
        });
      });

      var $scope = $('.ce-animated-text-area');

        $scope.find('.moving-animation-loop').each(function() {
          console.log( $(this));
          if ($('.moving_animation').length) {
            var animationName = $(this).data('optionce');
            console.log(animationName.nameAnimi);
            $(this).find(".ce-animation-text").Morphext({
              animation: animationName.nameAnimi,
              separator: ",",
              speed: animationName.speed,
              complete: function() {
              }
            });
          }
        });

        $scope.find('.typing-animation-loop').each(function() {
          if ($('.typing_animation').length) {
            var typing_animation = $(this).data('optionce');
            var typed = new Typed('#' + typing_animation.animiList_id_echo, {
              stringsElement: '#' + typing_animation.animiList_id,
              loop: true,
              showCursor: typing_animation.arow_off,
              typeSpeed: typing_animation.speed
            });
          }
        });

        $('#tab-content').on('DOMSubtreeModified', function(){
          $('.products-masonry').masonry({
            itemSelector: '.shop-block-one',
          });
        });

        $('.nav-link').on('click', function(){
          setTimeout(function(){
            $('.products-masonry').masonry({
              itemSelector: '.shop-block-one',
            });
          }, 200);
        });
    });

    $( document ).ready(function() {


      if ($('.carousel-products-mobile').length) {
        $('.carousel-products-mobile').not('.slick-initialized').slick({
            responsive: [
              {
                  breakpoint: 9999,
                  settings: "unslick"
              },
              {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    autoplay: true,
                    arrows : true,
                    autoplaySpeed: 5000,
                  }
              }
          ]
        });
      }

    });
      
  })(jQuery);

(function($) {
    "use strict";
    // slider section
    var slider = function($scope, $) {
      if ($('.banner-carousel').length) {
        $('.banner-carousel').not('.slick-initialized').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            autoplay: true,
            arrows : true,
            autoplaySpeed: 5000,
        });
      }
    }

    // brand slider section
    var carousel_brands = function($scope, $) {
      if ($('.carousel-brands').length) {
        $('.carousel-brands').not('.slick-initialized').slick({
            slidesToShow: 5,
            slidesToScroll: 2,
            dots: false,
            autoplay: true, 
            autoplaySpeed: 5000,
        });
      }
    }

    // brand slider section
    var carousel_look_book = function($scope, $) {
      if ($('.carousel-look-book').length) {
        $('.carousel-look-book').not('.slick-initialized').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            autoplay: true,
            arrows : false,
            autoplaySpeed: 5000,
        });
      }
    }
    
    // testimonial slider section
    var carousel_testimonial = function($scope, $) {
      if ($('.slider-testimonial').length) {
        $('.slider-testimonial').not('.slick-initialized').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            autoplay: true,
            arrows : false,
            autoplaySpeed: 5000,
        });
      }
    }
    
    $(window).on('elementor/frontend/init', function() {
        elementorFrontend.hooks.addAction('frontend/element_ready/slider.default', slider);
        elementorFrontend.hooks.addAction('frontend/element_ready/brandslider.default', carousel_brands);
        elementorFrontend.hooks.addAction('frontend/element_ready/lookbook.default', carousel_look_book);
        elementorFrontend.hooks.addAction('frontend/element_ready/testimonial.default', carousel_testimonial);
    });
})(window.jQuery);