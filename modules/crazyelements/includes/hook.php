<?php
$GetAlldisplayHooks = array(
	array(
		'id'   => 'displayTop',
		'name' => 'displayTop',
	),
	array(
		'id'   => 'displayTopColumn',
		'name' => 'displayTopColumn',
	),
	array(
		'id'   => 'displayHome',
		'name' => 'displayHome',
	),
	array(
		'id'   => 'displayNavFullWidth',
		'name' => 'displayNavFullWidth',
	),
	array(
		'id'   => 'displayFooterBefore',
		'name' => 'displayFooterBefore',
	),
	array(
		'id'   => 'displayFooter',
		'name' => 'displayFooter',
	)
);