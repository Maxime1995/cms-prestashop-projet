-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : dim. 11 déc. 2022 à 15:58
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `prestashop`
--

-- --------------------------------------------------------

--
-- Structure de la table `ps_access`
--

DROP TABLE IF EXISTS `ps_access`;
CREATE TABLE IF NOT EXISTS `ps_access` (
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_authorization_role` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_profile`,`id_authorization_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_access`
--

INSERT INTO `ps_access` (`id_profile`, `id_authorization_role`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 54),
(1, 55),
(1, 56),
(1, 57),
(1, 58),
(1, 59),
(1, 60),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(1, 67),
(1, 68),
(1, 69),
(1, 70),
(1, 71),
(1, 72),
(1, 73),
(1, 74),
(1, 75),
(1, 76),
(1, 77),
(1, 78),
(1, 79),
(1, 80),
(1, 81),
(1, 82),
(1, 83),
(1, 84),
(1, 85),
(1, 86),
(1, 87),
(1, 88),
(1, 89),
(1, 90),
(1, 91),
(1, 92),
(1, 93),
(1, 94),
(1, 95),
(1, 96),
(1, 97),
(1, 98),
(1, 99),
(1, 100),
(1, 101),
(1, 102),
(1, 103),
(1, 104),
(1, 105),
(1, 106),
(1, 107),
(1, 108),
(1, 109),
(1, 110),
(1, 111),
(1, 112),
(1, 113),
(1, 114),
(1, 115),
(1, 116),
(1, 117),
(1, 118),
(1, 119),
(1, 120),
(1, 121),
(1, 122),
(1, 123),
(1, 124),
(1, 125),
(1, 126),
(1, 127),
(1, 128),
(1, 129),
(1, 130),
(1, 131),
(1, 132),
(1, 133),
(1, 134),
(1, 135),
(1, 136),
(1, 137),
(1, 138),
(1, 139),
(1, 140),
(1, 141),
(1, 142),
(1, 143),
(1, 144),
(1, 145),
(1, 146),
(1, 147),
(1, 148),
(1, 149),
(1, 150),
(1, 151),
(1, 152),
(1, 153),
(1, 154),
(1, 155),
(1, 156),
(1, 157),
(1, 158),
(1, 159),
(1, 160),
(1, 161),
(1, 162),
(1, 163),
(1, 164),
(1, 165),
(1, 166),
(1, 167),
(1, 168),
(1, 169),
(1, 170),
(1, 171),
(1, 172),
(1, 173),
(1, 174),
(1, 175),
(1, 176),
(1, 177),
(1, 178),
(1, 179),
(1, 180),
(1, 181),
(1, 182),
(1, 183),
(1, 184),
(1, 185),
(1, 186),
(1, 187),
(1, 188),
(1, 189),
(1, 190),
(1, 191),
(1, 192),
(1, 193),
(1, 194),
(1, 195),
(1, 196),
(1, 197),
(1, 198),
(1, 199),
(1, 200),
(1, 201),
(1, 202),
(1, 203),
(1, 204),
(1, 205),
(1, 206),
(1, 207),
(1, 208),
(1, 209),
(1, 210),
(1, 211),
(1, 212),
(1, 213),
(1, 214),
(1, 215),
(1, 216),
(1, 217),
(1, 218),
(1, 219),
(1, 220),
(1, 221),
(1, 222),
(1, 223),
(1, 224),
(1, 225),
(1, 226),
(1, 227),
(1, 228),
(1, 229),
(1, 230),
(1, 231),
(1, 232),
(1, 233),
(1, 234),
(1, 235),
(1, 236),
(1, 237),
(1, 238),
(1, 239),
(1, 240),
(1, 241),
(1, 242),
(1, 243),
(1, 244),
(1, 245),
(1, 246),
(1, 247),
(1, 248),
(1, 249),
(1, 250),
(1, 251),
(1, 252),
(1, 253),
(1, 254),
(1, 255),
(1, 256),
(1, 257),
(1, 258),
(1, 259),
(1, 260),
(1, 261),
(1, 262),
(1, 263),
(1, 264),
(1, 265),
(1, 266),
(1, 267),
(1, 268),
(1, 269),
(1, 270),
(1, 271),
(1, 272),
(1, 273),
(1, 274),
(1, 275),
(1, 276),
(1, 277),
(1, 278),
(1, 279),
(1, 280),
(1, 281),
(1, 282),
(1, 283),
(1, 284),
(1, 285),
(1, 286),
(1, 287),
(1, 288),
(1, 289),
(1, 290),
(1, 291),
(1, 292),
(1, 293),
(1, 294),
(1, 295),
(1, 296),
(1, 297),
(1, 298),
(1, 299),
(1, 300),
(1, 301),
(1, 302),
(1, 303),
(1, 304),
(1, 305),
(1, 306),
(1, 307),
(1, 308),
(1, 309),
(1, 310),
(1, 311),
(1, 312),
(1, 313),
(1, 314),
(1, 315),
(1, 316),
(1, 317),
(1, 318),
(1, 319),
(1, 320),
(1, 321),
(1, 322),
(1, 323),
(1, 324),
(1, 325),
(1, 326),
(1, 327),
(1, 328),
(1, 329),
(1, 330),
(1, 331),
(1, 332),
(1, 333),
(1, 334),
(1, 335),
(1, 336),
(1, 337),
(1, 338),
(1, 339),
(1, 340),
(1, 341),
(1, 342),
(1, 343),
(1, 344),
(1, 345),
(1, 346),
(1, 347),
(1, 348),
(1, 349),
(1, 350),
(1, 351),
(1, 352),
(1, 353),
(1, 354),
(1, 355),
(1, 356),
(1, 357),
(1, 358),
(1, 359),
(1, 360),
(1, 361),
(1, 362),
(1, 363),
(1, 364),
(1, 365),
(1, 366),
(1, 367),
(1, 368),
(1, 369),
(1, 370),
(1, 371),
(1, 372),
(1, 373),
(1, 374),
(1, 375),
(1, 376),
(1, 377),
(1, 378),
(1, 379),
(1, 380),
(1, 381),
(1, 382),
(1, 383),
(1, 384),
(1, 385),
(1, 386),
(1, 387),
(1, 388),
(1, 389),
(1, 390),
(1, 391),
(1, 392),
(1, 393),
(1, 394),
(1, 395),
(1, 396),
(1, 397),
(1, 398),
(1, 399),
(1, 400),
(1, 401),
(1, 402),
(1, 403),
(1, 404),
(1, 405),
(1, 406),
(1, 407),
(1, 408),
(1, 409),
(1, 410),
(1, 411),
(1, 412),
(1, 413),
(1, 414),
(1, 415),
(1, 416),
(1, 417),
(1, 418),
(1, 419),
(1, 420),
(1, 421),
(1, 422),
(1, 423),
(1, 424),
(1, 425),
(1, 426),
(1, 427),
(1, 428),
(1, 429),
(1, 430),
(1, 431),
(1, 432),
(1, 433),
(1, 434),
(1, 435),
(1, 436),
(1, 437),
(1, 438),
(1, 439),
(1, 440),
(1, 441),
(1, 442),
(1, 443),
(1, 444),
(1, 445),
(1, 446),
(1, 447),
(1, 448),
(1, 449),
(1, 450),
(1, 451),
(1, 452),
(1, 453),
(1, 454),
(1, 455),
(1, 456),
(1, 457),
(1, 458),
(1, 459),
(1, 460),
(1, 461),
(1, 462),
(1, 463),
(1, 464),
(1, 465),
(1, 466),
(1, 467),
(1, 468),
(1, 481),
(1, 482),
(1, 483),
(1, 484),
(1, 485),
(1, 486),
(1, 487),
(1, 488),
(1, 489),
(1, 490),
(1, 491),
(1, 492),
(1, 493),
(1, 494),
(1, 495),
(1, 496),
(1, 513),
(1, 514),
(1, 515),
(1, 516),
(1, 597),
(1, 598),
(1, 599),
(1, 600),
(1, 641),
(1, 642),
(1, 643),
(1, 644),
(1, 645),
(1, 646),
(1, 647),
(1, 648),
(1, 649),
(1, 650),
(1, 651),
(1, 652),
(1, 729),
(1, 730),
(1, 731),
(1, 732),
(1, 733),
(1, 734),
(1, 735),
(1, 736),
(1, 749),
(1, 750),
(1, 751),
(1, 752),
(1, 753),
(1, 754),
(1, 755),
(1, 756),
(1, 761),
(1, 762),
(1, 763),
(1, 764),
(1, 765),
(1, 766),
(1, 767),
(1, 768),
(1, 769),
(1, 770),
(1, 771),
(1, 772),
(1, 773),
(1, 774),
(1, 775),
(1, 776),
(1, 781),
(1, 782),
(1, 783),
(1, 784),
(1, 789),
(1, 790),
(1, 791),
(1, 792),
(1, 793),
(1, 794),
(1, 795),
(1, 796),
(1, 801),
(1, 802),
(1, 803),
(1, 804),
(1, 805),
(1, 806),
(1, 807),
(1, 808),
(1, 809),
(1, 810),
(1, 811),
(1, 812),
(1, 813),
(1, 814),
(1, 815),
(1, 816),
(1, 817),
(1, 818),
(1, 819),
(1, 820),
(1, 825),
(1, 826),
(1, 827),
(1, 828),
(1, 829),
(1, 830),
(1, 831),
(1, 832),
(1, 841),
(1, 842),
(1, 843),
(1, 844),
(1, 853),
(1, 854),
(1, 855),
(1, 856),
(1, 861),
(1, 862),
(1, 863),
(1, 864),
(1, 865),
(1, 866),
(1, 867),
(1, 868),
(1, 869),
(1, 870),
(1, 871),
(1, 872),
(1, 873),
(1, 874),
(1, 875),
(1, 876),
(1, 877),
(1, 878),
(1, 879),
(1, 880),
(1, 881),
(1, 882),
(1, 883),
(1, 884),
(1, 885),
(1, 886),
(1, 887),
(1, 888),
(1, 889),
(1, 890),
(1, 891),
(1, 892),
(1, 893),
(1, 894),
(1, 895),
(1, 896),
(1, 897),
(1, 898),
(1, 899),
(1, 900),
(1, 901),
(1, 902),
(1, 903),
(1, 904),
(1, 905),
(1, 906),
(1, 907),
(1, 908),
(1, 909),
(1, 910),
(1, 911),
(1, 912),
(1, 913),
(1, 914),
(1, 915),
(1, 916),
(1, 917),
(1, 918),
(1, 919),
(1, 920),
(1, 925),
(1, 926),
(1, 927),
(1, 928),
(1, 929),
(1, 930),
(1, 931),
(1, 932),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 45),
(2, 46),
(2, 47),
(2, 48),
(2, 49),
(2, 50),
(2, 51),
(2, 52),
(2, 85),
(2, 86),
(2, 87),
(2, 88),
(2, 129),
(2, 130),
(2, 131),
(2, 132),
(2, 189),
(2, 190),
(2, 191),
(2, 192),
(2, 209),
(2, 210),
(2, 211),
(2, 212),
(2, 217),
(2, 218),
(2, 219),
(2, 220),
(2, 229),
(2, 230),
(2, 231),
(2, 232),
(2, 242),
(2, 243),
(2, 249),
(2, 250),
(2, 251),
(2, 252),
(2, 269),
(2, 270),
(2, 271),
(2, 272),
(2, 273),
(2, 274),
(2, 275),
(2, 276),
(2, 309),
(2, 310),
(2, 311),
(2, 312),
(2, 325),
(2, 326),
(2, 327),
(2, 328),
(2, 337),
(2, 338),
(2, 339),
(2, 340),
(2, 349),
(2, 350),
(2, 351),
(2, 352),
(2, 373),
(2, 374),
(2, 375),
(2, 376),
(2, 389),
(2, 390),
(2, 391),
(2, 392),
(2, 397),
(2, 398),
(2, 399),
(2, 400),
(2, 401),
(2, 402),
(2, 403),
(2, 404),
(2, 425),
(2, 426),
(2, 427),
(2, 428),
(2, 433),
(2, 434),
(2, 435),
(2, 436),
(2, 449),
(2, 450),
(2, 451),
(2, 452),
(2, 453),
(2, 454),
(2, 455),
(2, 456),
(3, 45),
(3, 46),
(3, 47),
(3, 48),
(3, 49),
(3, 50),
(3, 51),
(3, 52),
(3, 125),
(3, 126),
(3, 127),
(3, 128),
(3, 141),
(3, 142),
(3, 143),
(3, 144),
(3, 225),
(3, 226),
(3, 227),
(3, 228),
(3, 265),
(3, 266),
(3, 267),
(3, 268),
(3, 309),
(3, 310),
(3, 311),
(3, 312),
(3, 329),
(3, 330),
(3, 331),
(3, 332),
(3, 429),
(3, 430),
(3, 431),
(3, 432),
(3, 445),
(3, 446),
(3, 447),
(3, 448),
(3, 449),
(3, 450),
(3, 451),
(3, 452),
(3, 453),
(3, 454),
(3, 455),
(3, 456),
(3, 457),
(3, 458),
(3, 459),
(3, 460),
(4, 0),
(4, 9),
(4, 10),
(4, 11),
(4, 12),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(4, 41),
(4, 42),
(4, 43),
(4, 44),
(4, 45),
(4, 46),
(4, 47),
(4, 48),
(4, 49),
(4, 50),
(4, 51),
(4, 52),
(4, 129),
(4, 130),
(4, 131),
(4, 132),
(4, 154),
(4, 181),
(4, 182),
(4, 183),
(4, 184),
(4, 189),
(4, 190),
(4, 191),
(4, 192),
(4, 209),
(4, 210),
(4, 211),
(4, 212),
(4, 217),
(4, 218),
(4, 219),
(4, 220),
(4, 229),
(4, 230),
(4, 231),
(4, 232),
(4, 237),
(4, 238),
(4, 239),
(4, 240),
(4, 242),
(4, 243),
(4, 249),
(4, 250),
(4, 251),
(4, 252),
(4, 266),
(4, 309),
(4, 310),
(4, 311),
(4, 312),
(4, 317),
(4, 318),
(4, 319),
(4, 320),
(4, 330),
(4, 349),
(4, 350),
(4, 351),
(4, 352),
(4, 401),
(4, 402),
(4, 403),
(4, 404),
(4, 437),
(4, 438),
(4, 439),
(4, 440),
(4, 445),
(4, 446),
(4, 447),
(4, 448),
(4, 453),
(4, 454),
(4, 455),
(4, 456),
(4, 457),
(4, 458),
(4, 459),
(4, 460);

-- --------------------------------------------------------

--
-- Structure de la table `ps_accessory`
--

DROP TABLE IF EXISTS `ps_accessory`;
CREATE TABLE IF NOT EXISTS `ps_accessory` (
  `id_product_1` int(10) UNSIGNED NOT NULL,
  `id_product_2` int(10) UNSIGNED NOT NULL,
  KEY `accessory_product` (`id_product_1`,`id_product_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_address`
--

DROP TABLE IF EXISTS `ps_address`;
CREATE TABLE IF NOT EXISTS `ps_address` (
  `id_address` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_state` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_manufacturer` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_supplier` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_warehouse` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `alias` varchar(32) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `other` text,
  `phone` varchar(32) DEFAULT NULL,
  `phone_mobile` varchar(32) DEFAULT NULL,
  `vat_number` varchar(32) DEFAULT NULL,
  `dni` varchar(16) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_address`),
  KEY `address_customer` (`id_customer`),
  KEY `id_country` (`id_country`),
  KEY `id_state` (`id_state`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_warehouse` (`id_warehouse`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_address`
--

INSERT INTO `ps_address` (`id_address`, `id_country`, `id_state`, `id_customer`, `id_manufacturer`, `id_supplier`, `id_warehouse`, `alias`, `company`, `lastname`, `firstname`, `address1`, `address2`, `postcode`, `city`, `other`, `phone`, `phone_mobile`, `vat_number`, `dni`, `date_add`, `date_upd`, `active`, `deleted`) VALUES
(1, 3, 0, 1, 0, 0, 0, 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', 'Anonymous', '', '00000', 'Anonymous', '', '0000000000', '0000000000', '0000', '0000', '2022-11-21 11:05:59', '2022-11-21 11:05:59', 1, 0),
(2, 8, 0, 2, 0, 0, 0, 'Mon adresse', 'My Company', 'DOE', 'John', '16, Main street', '2nd floor', '75002', 'Paris ', '', '0102030405', '', '', '', '2022-11-21 11:07:24', '2022-11-21 11:07:24', 1, 0),
(3, 21, 35, 0, 0, 1, 0, 'supplier', 'Fashion', 'supplier', 'supplier', '767 Fifth Ave.', '', '10153', 'New York', '', '(212) 336-1440', '', '', '', '2022-11-21 11:07:24', '2022-11-21 11:07:24', 1, 0),
(4, 21, 35, 0, 1, 0, 0, 'manufacturer', 'Fashion', 'manufacturer', 'manufacturer', '767 Fifth Ave.', '', '10154', 'New York', '', '(212) 336-1666', '', '', '', '2022-11-21 11:07:24', '2022-11-21 11:07:24', 1, 0),
(5, 21, 12, 2, 0, 0, 0, 'My address', 'My Company', 'DOE', 'John', '16, Main street', '2nd floor', '33133', 'Miami', '', '0102030405', '', '', '', '2022-11-21 11:07:24', '2022-11-21 11:07:24', 1, 0),
(6, 8, 0, 0, 0, 2, 0, 'accessories_supplier', 'Accessories and Co', 'accessories', 'accessories', '42 Avenue Maréchal Soult', '', '64990', 'Bayonne', '', '0102030405', '', '', '', '2022-11-21 11:07:24', '2022-11-21 11:07:24', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_address_format`
--

DROP TABLE IF EXISTS `ps_address_format`;
CREATE TABLE IF NOT EXISTS `ps_address_format` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `format` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_address_format`
--

INSERT INTO `ps_address_format` (`id_country`, `format`) VALUES
(1, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(2, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(3, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(4, 'firstname lastname\ncompany\naddress1\naddress2\ncity State:name postcode\nCountry:name\nphone'),
(5, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(6, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(7, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(8, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(9, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(10, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone'),
(11, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone'),
(12, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(13, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(14, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(15, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(16, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(17, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\ncity\npostcode\nCountry:name\nphone'),
(18, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(19, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(20, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(21, 'firstname lastname\ncompany\naddress1 address2\ncity, State:name postcode\nCountry:name\nphone'),
(22, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(23, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(24, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\ncity State:iso_code postcode\nCountry:name\nphone'),
(25, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(26, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(27, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(28, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(29, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(30, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(31, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(32, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(33, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(34, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(35, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(36, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(37, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(38, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(39, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(40, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(41, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(42, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(43, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(44, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone'),
(45, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(46, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(47, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(48, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(49, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(50, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(51, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(52, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(53, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(54, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(55, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(56, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(57, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(58, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(59, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(60, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(61, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(62, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(63, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(64, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(65, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(66, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(67, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(68, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(69, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(70, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(71, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(72, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(73, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(74, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(75, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(76, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(77, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(78, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(79, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(80, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(81, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(82, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(83, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(84, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(85, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(86, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(87, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(88, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(89, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(90, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(91, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(92, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(93, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(94, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(95, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(96, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(97, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(98, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(99, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(100, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(101, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(102, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(103, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(104, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(105, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(106, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(107, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(108, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(109, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\ncity\npostcode\nState:name\nCountry:name\nphone'),
(110, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone'),
(111, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(112, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(113, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(114, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(115, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(116, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(117, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(118, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(119, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(120, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(121, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(122, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(123, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(124, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(125, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(126, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(127, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(128, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(129, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(130, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(131, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(132, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(133, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(134, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(135, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(136, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(137, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(138, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(139, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(140, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(141, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(142, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(143, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(144, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone'),
(145, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(146, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(147, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(148, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(149, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(150, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(151, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(152, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(153, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(154, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(155, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(156, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(157, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(158, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(159, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(160, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(161, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(162, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(163, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(164, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(165, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(166, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(167, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(168, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(169, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(170, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(171, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(172, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(173, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(174, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(175, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(176, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(177, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(178, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(179, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(180, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(181, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(182, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(183, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(184, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(185, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(186, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(187, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(188, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(189, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(190, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(191, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(192, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(193, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(194, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(195, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(196, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(197, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(198, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(199, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(200, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(201, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(202, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(203, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(204, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(205, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(206, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(207, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(208, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(209, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(210, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(211, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(212, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(213, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(214, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(215, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(216, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(217, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(218, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(219, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(220, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(221, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(222, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(223, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(224, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(225, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(226, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(227, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(228, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(229, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(230, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(231, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(232, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(233, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(234, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(235, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(236, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(237, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(238, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(239, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(240, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),
(241, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone');

-- --------------------------------------------------------

--
-- Structure de la table `ps_admin_filter`
--

DROP TABLE IF EXISTS `ps_admin_filter`;
CREATE TABLE IF NOT EXISTS `ps_admin_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee` int(11) NOT NULL,
  `shop` int(11) NOT NULL,
  `controller` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filter` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `filter_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_filter_search_id_idx` (`employee`,`shop`,`controller`,`action`,`filter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_admin_filter`
--

INSERT INTO `ps_admin_filter` (`id`, `employee`, `shop`, `controller`, `action`, `filter`, `filter_id`) VALUES
(1, 1, 1, 'contacts', 'index', '{\"limit\":10,\"orderBy\":\"id_contact\",\"sortOrder\":\"asc\",\"filters\":[]}', ''),
(2, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"id_meta\",\"sortOrder\":\"asc\",\"filters\":[]}', 'meta'),
(3, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"id_lang\",\"sortOrder\":\"ASC\",\"filters\":[]}', 'language'),
(4, 1, 1, 'employee', 'index', '{\"limit\":50,\"orderBy\":\"id_employee\",\"sortOrder\":\"asc\",\"filters\":[]}', ''),
(5, 1, 1, 'profile', 'index', '{\"limit\":10,\"orderBy\":\"id_profile\",\"sortOrder\":\"asc\",\"filters\":[]}', ''),
(6, 1, 1, 'ProductController', 'catalogAction', '{\"filter_category\":\"11\",\"filter_column_id_product\":\"\",\"filter_column_name\":\"\",\"filter_column_reference\":\"\",\"filter_column_name_category\":\"\",\"filter_column_price\":\"\",\"filter_column_sav_quantity\":\"\",\"filter_column_active\":\"\",\"last_offset\":\"0\",\"last_limit\":\"20\",\"last_orderBy\":\"id_product\",\"last_sortOrder\":\"desc\"}', ''),
(7, 1, 1, '', '', '{\"orderBy\":\"position\",\"sortOrder\":\"asc\",\"limit\":50,\"filters\":{\"id_category_parent\":\"2\"}}', 'category'),
(8, 1, 1, '', '', '{\"orderBy\":\"position\",\"sortOrder\":\"asc\",\"limit\":50,\"filters\":{\"id_cms_category_parent\":1}}', 'cms_page_category'),
(9, 1, 1, '', '', '{\"orderBy\":\"position\",\"sortOrder\":\"asc\",\"limit\":50,\"filters\":{\"id_cms_category_parent\":1}}', 'cms_page'),
(10, 1, 1, '', '', '{\"limit\":20,\"orderBy\":\"name\",\"sortOrder\":\"asc\",\"filters\":[]}', 'empty_category'),
(11, 1, 1, '', '', '{\"limit\":20,\"orderBy\":\"name\",\"sortOrder\":\"asc\",\"filters\":[]}', 'no_qty_product_with_combination'),
(12, 1, 1, '', '', '{\"limit\":20,\"orderBy\":\"name\",\"sortOrder\":\"asc\",\"filters\":[]}', 'no_qty_product_without_combination'),
(13, 1, 1, '', '', '{\"limit\":20,\"orderBy\":\"name\",\"sortOrder\":\"asc\",\"filters\":[]}', 'disabled_product'),
(14, 1, 1, '', '', '{\"limit\":20,\"orderBy\":\"name\",\"sortOrder\":\"asc\",\"filters\":[]}', 'product_without_image'),
(15, 1, 1, '', '', '{\"limit\":20,\"orderBy\":\"name\",\"sortOrder\":\"asc\",\"filters\":[]}', 'product_without_description'),
(16, 1, 1, '', '', '{\"limit\":20,\"orderBy\":\"name\",\"sortOrder\":\"asc\",\"filters\":[]}', 'product_without_price'),
(17, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"id_attachment\",\"sortOrder\":\"asc\",\"filters\":[]}', 'attachment'),
(18, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"id_order\",\"sortOrder\":\"DESC\",\"filters\":[]}', 'order'),
(19, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"id_currency\",\"sortOrder\":\"asc\",\"filters\":[]}', 'currency'),
(20, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"id_order_slip\",\"sortOrder\":\"asc\",\"filters\":[]}', 'credit_slip'),
(21, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"id_zone\",\"sortOrder\":\"asc\",\"filters\":[]}', 'zone'),
(22, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"id_tax\",\"sortOrder\":\"asc\",\"filters\":[]}', 'tax'),
(23, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"id_address\",\"sortOrder\":\"asc\",\"filters\":[]}', 'address'),
(24, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"date_add\",\"sortOrder\":\"DESC\",\"filters\":[]}', 'customer'),
(25, 1, 1, '', '', '{\"limit\":10,\"orderBy\":\"name\",\"sortOrder\":\"asc\",\"filters\":[]}', 'manufacturer'),
(26, 1, 1, '', '', '{\"limit\":10,\"orderBy\":\"id_address\",\"sortOrder\":\"desc\",\"filters\":[]}', 'manufacturer_address'),
(27, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"name\",\"sortOrder\":\"asc\",\"filters\":[]}', 'supplier'),
(28, 1, 1, '', '', '{\"limit\":50,\"orderBy\":\"id_order_message\",\"sortOrder\":\"asc\",\"filters\":[]}', 'order_message');

-- --------------------------------------------------------

--
-- Structure de la table `ps_advice`
--

DROP TABLE IF EXISTS `ps_advice`;
CREATE TABLE IF NOT EXISTS `ps_advice` (
  `id_advice` int(11) NOT NULL AUTO_INCREMENT,
  `id_ps_advice` int(11) NOT NULL,
  `id_tab` int(11) NOT NULL,
  `ids_tab` text,
  `validated` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `location` enum('after','before') NOT NULL,
  `selector` varchar(255) DEFAULT NULL,
  `start_day` int(11) NOT NULL DEFAULT '0',
  `stop_day` int(11) NOT NULL DEFAULT '0',
  `weight` int(11) DEFAULT '1',
  PRIMARY KEY (`id_advice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_advice_lang`
--

DROP TABLE IF EXISTS `ps_advice_lang`;
CREATE TABLE IF NOT EXISTS `ps_advice_lang` (
  `id_advice` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `html` text,
  PRIMARY KEY (`id_advice`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_alias`
--

DROP TABLE IF EXISTS `ps_alias`;
CREATE TABLE IF NOT EXISTS `ps_alias` (
  `id_alias` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `alias` varchar(191) NOT NULL,
  `search` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_alias`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_alias`
--

INSERT INTO `ps_alias` (`id_alias`, `alias`, `search`, `active`) VALUES
(1, 'bloose', 'blouse', 1),
(2, 'blues', 'blouse', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_attachment`
--

DROP TABLE IF EXISTS `ps_attachment`;
CREATE TABLE IF NOT EXISTS `ps_attachment` (
  `id_attachment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `file` varchar(40) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `file_size` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `mime` varchar(128) NOT NULL,
  PRIMARY KEY (`id_attachment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_attachment_lang`
--

DROP TABLE IF EXISTS `ps_attachment_lang`;
CREATE TABLE IF NOT EXISTS `ps_attachment_lang` (
  `id_attachment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id_attachment`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_attribute`
--

DROP TABLE IF EXISTS `ps_attribute`;
CREATE TABLE IF NOT EXISTS `ps_attribute` (
  `id_attribute` int(11) NOT NULL AUTO_INCREMENT,
  `id_attribute_group` int(11) NOT NULL,
  `color` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id_attribute`),
  KEY `attribute_group` (`id_attribute_group`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_attribute`
--

INSERT INTO `ps_attribute` (`id_attribute`, `id_attribute_group`, `color`, `position`) VALUES
(1, 1, '', 0),
(2, 1, '', 1),
(3, 1, '', 2),
(4, 1, '', 3),
(5, 2, '#AAB2BD', 0),
(6, 2, '#CFC4A6', 1),
(7, 2, '#f5f5dc', 2),
(8, 2, '#ffffff', 3),
(9, 2, '#faebd7', 4),
(10, 2, '#E84C3D', 5),
(11, 2, '#434A54', 6),
(12, 2, '#C19A6B', 7),
(13, 2, '#F39C11', 8),
(14, 2, '#5D9CEC', 9),
(15, 2, '#A0D468', 10),
(16, 2, '#F1C40F', 11),
(17, 2, '#964B00', 12),
(18, 2, '#FCCACD', 13),
(19, 3, '', 0),
(20, 3, '', 1),
(21, 3, '', 2),
(22, 4, '', 0),
(23, 4, '', 1),
(24, 4, '', 2),
(25, 4, '', 3);

-- --------------------------------------------------------

--
-- Structure de la table `ps_attribute_group`
--

DROP TABLE IF EXISTS `ps_attribute_group`;
CREATE TABLE IF NOT EXISTS `ps_attribute_group` (
  `id_attribute_group` int(11) NOT NULL AUTO_INCREMENT,
  `is_color_group` tinyint(1) NOT NULL,
  `group_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id_attribute_group`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_attribute_group`
--

INSERT INTO `ps_attribute_group` (`id_attribute_group`, `is_color_group`, `group_type`, `position`) VALUES
(1, 0, 'select', 0),
(2, 1, 'color', 1),
(3, 0, 'select', 2),
(4, 0, 'select', 3);

-- --------------------------------------------------------

--
-- Structure de la table `ps_attribute_group_lang`
--

DROP TABLE IF EXISTS `ps_attribute_group_lang`;
CREATE TABLE IF NOT EXISTS `ps_attribute_group_lang` (
  `id_attribute_group` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `public_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_attribute_group`,`id_lang`),
  KEY `IDX_4653726C67A664FB` (`id_attribute_group`),
  KEY `IDX_4653726CBA299860` (`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_attribute_group_lang`
--

INSERT INTO `ps_attribute_group_lang` (`id_attribute_group`, `id_lang`, `name`, `public_name`) VALUES
(1, 1, 'Size', 'Size'),
(1, 2, 'Taille', 'Taille'),
(1, 3, 'Größe', 'Größe'),
(1, 4, 'Grootte', 'Grootte'),
(1, 5, 'Dimensione', 'Dimensione'),
(1, 6, 'Tamaño', 'Tamaño'),
(2, 1, 'Color', 'Color'),
(2, 2, 'Couleur', 'Couleur'),
(2, 3, 'Farbe', 'Farbe'),
(2, 4, 'Kleur', 'Kleur'),
(2, 5, 'Colore', 'Colore'),
(2, 6, 'Color', 'Color'),
(3, 1, 'Dimension', 'Dimension'),
(3, 2, 'Dimension', 'Dimension'),
(3, 3, 'Dimension', 'Dimension'),
(3, 4, 'Dimension', 'Dimension'),
(3, 5, 'Dimension', 'Dimension'),
(3, 6, 'Dimension', 'Dimension'),
(4, 1, 'Paper Type', 'Paper Type'),
(4, 2, 'Type de papier', 'Type de papier'),
(4, 3, 'Paper Type', 'Paper Type'),
(4, 4, 'Paper Type', 'Paper Type'),
(4, 5, 'Paper Type', 'Paper Type'),
(4, 6, 'Paper Type', 'Paper Type');

-- --------------------------------------------------------

--
-- Structure de la table `ps_attribute_group_shop`
--

DROP TABLE IF EXISTS `ps_attribute_group_shop`;
CREATE TABLE IF NOT EXISTS `ps_attribute_group_shop` (
  `id_attribute_group` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_attribute_group`,`id_shop`),
  KEY `IDX_DB30BAAC67A664FB` (`id_attribute_group`),
  KEY `IDX_DB30BAAC274A50A0` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_attribute_group_shop`
--

INSERT INTO `ps_attribute_group_shop` (`id_attribute_group`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_attribute_impact`
--

DROP TABLE IF EXISTS `ps_attribute_impact`;
CREATE TABLE IF NOT EXISTS `ps_attribute_impact` (
  `id_attribute_impact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_attribute` int(11) UNSIGNED NOT NULL,
  `weight` decimal(20,6) NOT NULL,
  `price` decimal(20,6) NOT NULL,
  PRIMARY KEY (`id_attribute_impact`),
  UNIQUE KEY `id_product` (`id_product`,`id_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_attribute_lang`
--

DROP TABLE IF EXISTS `ps_attribute_lang`;
CREATE TABLE IF NOT EXISTS `ps_attribute_lang` (
  `id_attribute` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_attribute`,`id_lang`),
  KEY `IDX_3ABE46A77A4F53DC` (`id_attribute`),
  KEY `IDX_3ABE46A7BA299860` (`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_attribute_lang`
--

INSERT INTO `ps_attribute_lang` (`id_attribute`, `id_lang`, `name`) VALUES
(1, 1, 'S'),
(1, 2, 'S'),
(1, 3, 'S'),
(1, 4, 'S'),
(1, 5, 'S'),
(1, 6, 'S'),
(2, 1, 'M'),
(2, 2, 'M'),
(2, 3, 'M'),
(2, 4, 'M'),
(2, 5, 'M'),
(2, 6, 'M'),
(3, 1, 'L'),
(3, 2, 'L'),
(3, 3, 'L'),
(3, 4, 'L'),
(3, 5, 'L'),
(3, 6, 'L'),
(4, 1, 'XL'),
(4, 2, 'XL'),
(4, 3, 'XL'),
(4, 4, 'XL'),
(4, 5, 'XL'),
(4, 6, 'XL'),
(5, 1, 'Grey'),
(5, 2, 'Gris'),
(5, 3, 'Grau'),
(5, 4, 'Grijs'),
(5, 5, 'Grigio'),
(5, 6, 'Gris'),
(6, 1, 'Taupe'),
(6, 2, 'Taupe'),
(6, 3, 'Taupe'),
(6, 4, 'Taupe'),
(6, 5, 'Talpa'),
(6, 6, 'Gris pardo'),
(7, 1, 'Beige'),
(7, 2, 'Beige'),
(7, 3, 'Beige'),
(7, 4, 'Beige'),
(7, 5, 'Beige'),
(7, 6, 'Beige'),
(8, 1, 'White'),
(8, 2, 'Blanc'),
(8, 3, 'Weiß'),
(8, 4, 'Wit'),
(8, 5, 'Bianco'),
(8, 6, 'Blanco'),
(9, 1, 'Off White'),
(9, 2, 'Blanc cassé'),
(9, 3, 'Wollweiß'),
(9, 4, 'Gebroken wit'),
(9, 5, 'Avorio'),
(9, 6, 'Blanco roto'),
(10, 1, 'Red'),
(10, 2, 'Rouge'),
(10, 3, 'Rot'),
(10, 4, 'Rood'),
(10, 5, 'Rosso'),
(10, 6, 'Rojo'),
(11, 1, 'Black'),
(11, 2, 'Noir'),
(11, 3, 'Schwarz'),
(11, 4, 'Zwart'),
(11, 5, 'Nero'),
(11, 6, 'Negro'),
(12, 1, 'Camel'),
(12, 2, 'Camel'),
(12, 3, 'Camel'),
(12, 4, 'Camel'),
(12, 5, 'Cammello'),
(12, 6, 'Camel'),
(13, 1, 'Orange'),
(13, 2, 'Orange'),
(13, 3, 'Orange'),
(13, 4, 'Oranje'),
(13, 5, 'Arancione'),
(13, 6, 'Naranja'),
(14, 1, 'Blue'),
(14, 2, 'Bleu'),
(14, 3, 'Blau'),
(14, 4, 'Blauw'),
(14, 5, 'Blu'),
(14, 6, 'Azul'),
(15, 1, 'Green'),
(15, 2, 'Vert'),
(15, 3, 'Grün'),
(15, 4, 'Groen'),
(15, 5, 'Verde'),
(15, 6, 'Verde'),
(16, 1, 'Yellow'),
(16, 2, 'Jaune'),
(16, 3, 'Gelb'),
(16, 4, 'Geel'),
(16, 5, 'Giallo'),
(16, 6, 'Amarillo'),
(17, 1, 'Brown'),
(17, 2, 'Marron'),
(17, 3, 'Braun'),
(17, 4, 'Bruin'),
(17, 5, 'Marrone'),
(17, 6, 'Marrón'),
(18, 1, 'Pink'),
(18, 2, 'Rose'),
(18, 3, 'Pink'),
(18, 4, 'Roze'),
(18, 5, 'Rosa'),
(18, 6, 'Rosa'),
(19, 1, '40x60cm'),
(19, 2, '40x60cm'),
(19, 3, '40x60cm'),
(19, 4, '40x60cm'),
(19, 5, '40x60cm'),
(19, 6, '40x60cm'),
(20, 1, '60x90cm'),
(20, 2, '60x90cm'),
(20, 3, '60x90cm'),
(20, 4, '60x90cm'),
(20, 5, '60x90cm'),
(20, 6, '60x90cm'),
(21, 1, '80x120cm'),
(21, 2, '80x120cm'),
(21, 3, '80x120cm'),
(21, 4, '80x120cm'),
(21, 5, '80x120cm'),
(21, 6, '80x120cm'),
(22, 1, 'Ruled'),
(22, 2, 'Ligné'),
(22, 3, 'Ruled'),
(22, 4, 'Ruled'),
(22, 5, 'Ruled'),
(22, 6, 'Ruled'),
(23, 1, 'Plain'),
(23, 2, 'Vierge'),
(23, 3, 'Plain'),
(23, 4, 'Plain'),
(23, 5, 'Plain'),
(23, 6, 'Plain'),
(24, 1, 'Squarred'),
(24, 2, 'Quadrillé'),
(24, 3, 'Squarred'),
(24, 4, 'Squarred'),
(24, 5, 'Squarred'),
(24, 6, 'Squarred'),
(25, 1, 'Doted'),
(25, 2, 'Pointillés'),
(25, 3, 'Doted'),
(25, 4, 'Doted'),
(25, 5, 'Doted'),
(25, 6, 'Doted');

-- --------------------------------------------------------

--
-- Structure de la table `ps_attribute_shop`
--

DROP TABLE IF EXISTS `ps_attribute_shop`;
CREATE TABLE IF NOT EXISTS `ps_attribute_shop` (
  `id_attribute` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_attribute`,`id_shop`),
  KEY `IDX_A7DD8E677A4F53DC` (`id_attribute`),
  KEY `IDX_A7DD8E67274A50A0` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_attribute_shop`
--

INSERT INTO `ps_attribute_shop` (`id_attribute`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_authorization_role`
--

DROP TABLE IF EXISTS `ps_authorization_role`;
CREATE TABLE IF NOT EXISTS `ps_authorization_role` (
  `id_authorization_role` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) NOT NULL,
  PRIMARY KEY (`id_authorization_role`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=937 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_authorization_role`
--

INSERT INTO `ps_authorization_role` (`id_authorization_role`, `slug`) VALUES
(837, 'ROLE_MOD_MODULE_BLOCKREASSURANCE_CREATE'),
(840, 'ROLE_MOD_MODULE_BLOCKREASSURANCE_DELETE'),
(838, 'ROLE_MOD_MODULE_BLOCKREASSURANCE_READ'),
(839, 'ROLE_MOD_MODULE_BLOCKREASSURANCE_UPDATE'),
(497, 'ROLE_MOD_MODULE_BLOCKWISHLIST_CREATE'),
(500, 'ROLE_MOD_MODULE_BLOCKWISHLIST_DELETE'),
(498, 'ROLE_MOD_MODULE_BLOCKWISHLIST_READ'),
(499, 'ROLE_MOD_MODULE_BLOCKWISHLIST_UPDATE'),
(501, 'ROLE_MOD_MODULE_CONTACTFORM_CREATE'),
(504, 'ROLE_MOD_MODULE_CONTACTFORM_DELETE'),
(502, 'ROLE_MOD_MODULE_CONTACTFORM_READ'),
(503, 'ROLE_MOD_MODULE_CONTACTFORM_UPDATE'),
(921, 'ROLE_MOD_MODULE_CRAZYELEMENTS_CREATE'),
(924, 'ROLE_MOD_MODULE_CRAZYELEMENTS_DELETE'),
(922, 'ROLE_MOD_MODULE_CRAZYELEMENTS_READ'),
(923, 'ROLE_MOD_MODULE_CRAZYELEMENTS_UPDATE'),
(505, 'ROLE_MOD_MODULE_DASHACTIVITY_CREATE'),
(508, 'ROLE_MOD_MODULE_DASHACTIVITY_DELETE'),
(506, 'ROLE_MOD_MODULE_DASHACTIVITY_READ'),
(507, 'ROLE_MOD_MODULE_DASHACTIVITY_UPDATE'),
(517, 'ROLE_MOD_MODULE_DASHGOALS_CREATE'),
(520, 'ROLE_MOD_MODULE_DASHGOALS_DELETE'),
(518, 'ROLE_MOD_MODULE_DASHGOALS_READ'),
(519, 'ROLE_MOD_MODULE_DASHGOALS_UPDATE'),
(521, 'ROLE_MOD_MODULE_DASHPRODUCTS_CREATE'),
(524, 'ROLE_MOD_MODULE_DASHPRODUCTS_DELETE'),
(522, 'ROLE_MOD_MODULE_DASHPRODUCTS_READ'),
(523, 'ROLE_MOD_MODULE_DASHPRODUCTS_UPDATE'),
(509, 'ROLE_MOD_MODULE_DASHTRENDS_CREATE'),
(512, 'ROLE_MOD_MODULE_DASHTRENDS_DELETE'),
(510, 'ROLE_MOD_MODULE_DASHTRENDS_READ'),
(511, 'ROLE_MOD_MODULE_DASHTRENDS_UPDATE'),
(737, 'ROLE_MOD_MODULE_GAMIFICATION_CREATE'),
(740, 'ROLE_MOD_MODULE_GAMIFICATION_DELETE'),
(738, 'ROLE_MOD_MODULE_GAMIFICATION_READ'),
(739, 'ROLE_MOD_MODULE_GAMIFICATION_UPDATE'),
(525, 'ROLE_MOD_MODULE_GRAPHNVD3_CREATE'),
(528, 'ROLE_MOD_MODULE_GRAPHNVD3_DELETE'),
(526, 'ROLE_MOD_MODULE_GRAPHNVD3_READ'),
(527, 'ROLE_MOD_MODULE_GRAPHNVD3_UPDATE'),
(529, 'ROLE_MOD_MODULE_GRIDHTML_CREATE'),
(532, 'ROLE_MOD_MODULE_GRIDHTML_DELETE'),
(530, 'ROLE_MOD_MODULE_GRIDHTML_READ'),
(531, 'ROLE_MOD_MODULE_GRIDHTML_UPDATE'),
(533, 'ROLE_MOD_MODULE_GSITEMAP_CREATE'),
(536, 'ROLE_MOD_MODULE_GSITEMAP_DELETE'),
(534, 'ROLE_MOD_MODULE_GSITEMAP_READ'),
(535, 'ROLE_MOD_MODULE_GSITEMAP_UPDATE'),
(857, 'ROLE_MOD_MODULE_MYSHOPHELPER_CREATE'),
(860, 'ROLE_MOD_MODULE_MYSHOPHELPER_DELETE'),
(858, 'ROLE_MOD_MODULE_MYSHOPHELPER_READ'),
(859, 'ROLE_MOD_MODULE_MYSHOPHELPER_UPDATE'),
(537, 'ROLE_MOD_MODULE_PAGESNOTFOUND_CREATE'),
(540, 'ROLE_MOD_MODULE_PAGESNOTFOUND_DELETE'),
(538, 'ROLE_MOD_MODULE_PAGESNOTFOUND_READ'),
(539, 'ROLE_MOD_MODULE_PAGESNOTFOUND_UPDATE'),
(541, 'ROLE_MOD_MODULE_PRODUCTCOMMENTS_CREATE'),
(544, 'ROLE_MOD_MODULE_PRODUCTCOMMENTS_DELETE'),
(542, 'ROLE_MOD_MODULE_PRODUCTCOMMENTS_READ'),
(543, 'ROLE_MOD_MODULE_PRODUCTCOMMENTS_UPDATE'),
(741, 'ROLE_MOD_MODULE_PSADDONSCONNECT_CREATE'),
(744, 'ROLE_MOD_MODULE_PSADDONSCONNECT_DELETE'),
(742, 'ROLE_MOD_MODULE_PSADDONSCONNECT_READ'),
(743, 'ROLE_MOD_MODULE_PSADDONSCONNECT_UPDATE'),
(745, 'ROLE_MOD_MODULE_PSGDPR_CREATE'),
(748, 'ROLE_MOD_MODULE_PSGDPR_DELETE'),
(746, 'ROLE_MOD_MODULE_PSGDPR_READ'),
(747, 'ROLE_MOD_MODULE_PSGDPR_UPDATE'),
(833, 'ROLE_MOD_MODULE_PSXMARKETINGWITHGOOGLE_CREATE'),
(836, 'ROLE_MOD_MODULE_PSXMARKETINGWITHGOOGLE_DELETE'),
(834, 'ROLE_MOD_MODULE_PSXMARKETINGWITHGOOGLE_READ'),
(835, 'ROLE_MOD_MODULE_PSXMARKETINGWITHGOOGLE_UPDATE'),
(545, 'ROLE_MOD_MODULE_PS_BANNER_CREATE'),
(548, 'ROLE_MOD_MODULE_PS_BANNER_DELETE'),
(546, 'ROLE_MOD_MODULE_PS_BANNER_READ'),
(547, 'ROLE_MOD_MODULE_PS_BANNER_UPDATE'),
(777, 'ROLE_MOD_MODULE_PS_BUYBUTTONLITE_CREATE'),
(780, 'ROLE_MOD_MODULE_PS_BUYBUTTONLITE_DELETE'),
(778, 'ROLE_MOD_MODULE_PS_BUYBUTTONLITE_READ'),
(779, 'ROLE_MOD_MODULE_PS_BUYBUTTONLITE_UPDATE'),
(549, 'ROLE_MOD_MODULE_PS_CATEGORYTREE_CREATE'),
(552, 'ROLE_MOD_MODULE_PS_CATEGORYTREE_DELETE'),
(550, 'ROLE_MOD_MODULE_PS_CATEGORYTREE_READ'),
(551, 'ROLE_MOD_MODULE_PS_CATEGORYTREE_UPDATE'),
(785, 'ROLE_MOD_MODULE_PS_CHECKOUT_CREATE'),
(788, 'ROLE_MOD_MODULE_PS_CHECKOUT_DELETE'),
(786, 'ROLE_MOD_MODULE_PS_CHECKOUT_READ'),
(787, 'ROLE_MOD_MODULE_PS_CHECKOUT_UPDATE'),
(553, 'ROLE_MOD_MODULE_PS_CHECKPAYMENT_CREATE'),
(556, 'ROLE_MOD_MODULE_PS_CHECKPAYMENT_DELETE'),
(554, 'ROLE_MOD_MODULE_PS_CHECKPAYMENT_READ'),
(555, 'ROLE_MOD_MODULE_PS_CHECKPAYMENT_UPDATE'),
(557, 'ROLE_MOD_MODULE_PS_CONTACTINFO_CREATE'),
(560, 'ROLE_MOD_MODULE_PS_CONTACTINFO_DELETE'),
(558, 'ROLE_MOD_MODULE_PS_CONTACTINFO_READ'),
(559, 'ROLE_MOD_MODULE_PS_CONTACTINFO_UPDATE'),
(561, 'ROLE_MOD_MODULE_PS_CROSSSELLING_CREATE'),
(564, 'ROLE_MOD_MODULE_PS_CROSSSELLING_DELETE'),
(562, 'ROLE_MOD_MODULE_PS_CROSSSELLING_READ'),
(563, 'ROLE_MOD_MODULE_PS_CROSSSELLING_UPDATE'),
(565, 'ROLE_MOD_MODULE_PS_CURRENCYSELECTOR_CREATE'),
(568, 'ROLE_MOD_MODULE_PS_CURRENCYSELECTOR_DELETE'),
(566, 'ROLE_MOD_MODULE_PS_CURRENCYSELECTOR_READ'),
(567, 'ROLE_MOD_MODULE_PS_CURRENCYSELECTOR_UPDATE'),
(569, 'ROLE_MOD_MODULE_PS_CUSTOMERACCOUNTLINKS_CREATE'),
(572, 'ROLE_MOD_MODULE_PS_CUSTOMERACCOUNTLINKS_DELETE'),
(570, 'ROLE_MOD_MODULE_PS_CUSTOMERACCOUNTLINKS_READ'),
(571, 'ROLE_MOD_MODULE_PS_CUSTOMERACCOUNTLINKS_UPDATE'),
(573, 'ROLE_MOD_MODULE_PS_CUSTOMERSIGNIN_CREATE'),
(576, 'ROLE_MOD_MODULE_PS_CUSTOMERSIGNIN_DELETE'),
(574, 'ROLE_MOD_MODULE_PS_CUSTOMERSIGNIN_READ'),
(575, 'ROLE_MOD_MODULE_PS_CUSTOMERSIGNIN_UPDATE'),
(577, 'ROLE_MOD_MODULE_PS_CUSTOMTEXT_CREATE'),
(580, 'ROLE_MOD_MODULE_PS_CUSTOMTEXT_DELETE'),
(578, 'ROLE_MOD_MODULE_PS_CUSTOMTEXT_READ'),
(579, 'ROLE_MOD_MODULE_PS_CUSTOMTEXT_UPDATE'),
(581, 'ROLE_MOD_MODULE_PS_DATAPRIVACY_CREATE'),
(584, 'ROLE_MOD_MODULE_PS_DATAPRIVACY_DELETE'),
(582, 'ROLE_MOD_MODULE_PS_DATAPRIVACY_READ'),
(583, 'ROLE_MOD_MODULE_PS_DATAPRIVACY_UPDATE'),
(585, 'ROLE_MOD_MODULE_PS_EMAILSUBSCRIPTION_CREATE'),
(588, 'ROLE_MOD_MODULE_PS_EMAILSUBSCRIPTION_DELETE'),
(586, 'ROLE_MOD_MODULE_PS_EMAILSUBSCRIPTION_READ'),
(587, 'ROLE_MOD_MODULE_PS_EMAILSUBSCRIPTION_UPDATE'),
(821, 'ROLE_MOD_MODULE_PS_FACEBOOK_CREATE'),
(824, 'ROLE_MOD_MODULE_PS_FACEBOOK_DELETE'),
(822, 'ROLE_MOD_MODULE_PS_FACEBOOK_READ'),
(823, 'ROLE_MOD_MODULE_PS_FACEBOOK_UPDATE'),
(845, 'ROLE_MOD_MODULE_PS_FACETEDSEARCH_CREATE'),
(848, 'ROLE_MOD_MODULE_PS_FACETEDSEARCH_DELETE'),
(846, 'ROLE_MOD_MODULE_PS_FACETEDSEARCH_READ'),
(847, 'ROLE_MOD_MODULE_PS_FACETEDSEARCH_UPDATE'),
(593, 'ROLE_MOD_MODULE_PS_FAVICONNOTIFICATIONBO_CREATE'),
(596, 'ROLE_MOD_MODULE_PS_FAVICONNOTIFICATIONBO_DELETE'),
(594, 'ROLE_MOD_MODULE_PS_FAVICONNOTIFICATIONBO_READ'),
(595, 'ROLE_MOD_MODULE_PS_FAVICONNOTIFICATIONBO_UPDATE'),
(601, 'ROLE_MOD_MODULE_PS_FEATUREDPRODUCTS_CREATE'),
(604, 'ROLE_MOD_MODULE_PS_FEATUREDPRODUCTS_DELETE'),
(602, 'ROLE_MOD_MODULE_PS_FEATUREDPRODUCTS_READ'),
(603, 'ROLE_MOD_MODULE_PS_FEATUREDPRODUCTS_UPDATE'),
(605, 'ROLE_MOD_MODULE_PS_IMAGESLIDER_CREATE'),
(608, 'ROLE_MOD_MODULE_PS_IMAGESLIDER_DELETE'),
(606, 'ROLE_MOD_MODULE_PS_IMAGESLIDER_READ'),
(607, 'ROLE_MOD_MODULE_PS_IMAGESLIDER_UPDATE'),
(609, 'ROLE_MOD_MODULE_PS_LANGUAGESELECTOR_CREATE'),
(612, 'ROLE_MOD_MODULE_PS_LANGUAGESELECTOR_DELETE'),
(610, 'ROLE_MOD_MODULE_PS_LANGUAGESELECTOR_READ'),
(611, 'ROLE_MOD_MODULE_PS_LANGUAGESELECTOR_UPDATE'),
(613, 'ROLE_MOD_MODULE_PS_LINKLIST_CREATE'),
(616, 'ROLE_MOD_MODULE_PS_LINKLIST_DELETE'),
(614, 'ROLE_MOD_MODULE_PS_LINKLIST_READ'),
(615, 'ROLE_MOD_MODULE_PS_LINKLIST_UPDATE'),
(617, 'ROLE_MOD_MODULE_PS_MAINMENU_CREATE'),
(620, 'ROLE_MOD_MODULE_PS_MAINMENU_DELETE'),
(618, 'ROLE_MOD_MODULE_PS_MAINMENU_READ'),
(619, 'ROLE_MOD_MODULE_PS_MAINMENU_UPDATE'),
(757, 'ROLE_MOD_MODULE_PS_MBO_CREATE'),
(760, 'ROLE_MOD_MODULE_PS_MBO_DELETE'),
(758, 'ROLE_MOD_MODULE_PS_MBO_READ'),
(759, 'ROLE_MOD_MODULE_PS_MBO_UPDATE'),
(797, 'ROLE_MOD_MODULE_PS_METRICS_CREATE'),
(800, 'ROLE_MOD_MODULE_PS_METRICS_DELETE'),
(798, 'ROLE_MOD_MODULE_PS_METRICS_READ'),
(799, 'ROLE_MOD_MODULE_PS_METRICS_UPDATE'),
(621, 'ROLE_MOD_MODULE_PS_SEARCHBAR_CREATE'),
(624, 'ROLE_MOD_MODULE_PS_SEARCHBAR_DELETE'),
(622, 'ROLE_MOD_MODULE_PS_SEARCHBAR_READ'),
(623, 'ROLE_MOD_MODULE_PS_SEARCHBAR_UPDATE'),
(625, 'ROLE_MOD_MODULE_PS_SHAREBUTTONS_CREATE'),
(628, 'ROLE_MOD_MODULE_PS_SHAREBUTTONS_DELETE'),
(626, 'ROLE_MOD_MODULE_PS_SHAREBUTTONS_READ'),
(627, 'ROLE_MOD_MODULE_PS_SHAREBUTTONS_UPDATE'),
(629, 'ROLE_MOD_MODULE_PS_SHOPPINGCART_CREATE'),
(632, 'ROLE_MOD_MODULE_PS_SHOPPINGCART_DELETE'),
(630, 'ROLE_MOD_MODULE_PS_SHOPPINGCART_READ'),
(631, 'ROLE_MOD_MODULE_PS_SHOPPINGCART_UPDATE'),
(633, 'ROLE_MOD_MODULE_PS_SOCIALFOLLOW_CREATE'),
(636, 'ROLE_MOD_MODULE_PS_SOCIALFOLLOW_DELETE'),
(634, 'ROLE_MOD_MODULE_PS_SOCIALFOLLOW_READ'),
(635, 'ROLE_MOD_MODULE_PS_SOCIALFOLLOW_UPDATE'),
(637, 'ROLE_MOD_MODULE_PS_THEMECUSTO_CREATE'),
(640, 'ROLE_MOD_MODULE_PS_THEMECUSTO_DELETE'),
(638, 'ROLE_MOD_MODULE_PS_THEMECUSTO_READ'),
(639, 'ROLE_MOD_MODULE_PS_THEMECUSTO_UPDATE'),
(849, 'ROLE_MOD_MODULE_PS_VIEWEDPRODUCT_CREATE'),
(852, 'ROLE_MOD_MODULE_PS_VIEWEDPRODUCT_DELETE'),
(850, 'ROLE_MOD_MODULE_PS_VIEWEDPRODUCT_READ'),
(851, 'ROLE_MOD_MODULE_PS_VIEWEDPRODUCT_UPDATE'),
(653, 'ROLE_MOD_MODULE_PS_WIREPAYMENT_CREATE'),
(656, 'ROLE_MOD_MODULE_PS_WIREPAYMENT_DELETE'),
(654, 'ROLE_MOD_MODULE_PS_WIREPAYMENT_READ'),
(655, 'ROLE_MOD_MODULE_PS_WIREPAYMENT_UPDATE'),
(933, 'ROLE_MOD_MODULE_SMARTSUPP_CREATE'),
(936, 'ROLE_MOD_MODULE_SMARTSUPP_DELETE'),
(934, 'ROLE_MOD_MODULE_SMARTSUPP_READ'),
(935, 'ROLE_MOD_MODULE_SMARTSUPP_UPDATE'),
(657, 'ROLE_MOD_MODULE_STATSBESTCATEGORIES_CREATE'),
(660, 'ROLE_MOD_MODULE_STATSBESTCATEGORIES_DELETE'),
(658, 'ROLE_MOD_MODULE_STATSBESTCATEGORIES_READ'),
(659, 'ROLE_MOD_MODULE_STATSBESTCATEGORIES_UPDATE'),
(661, 'ROLE_MOD_MODULE_STATSBESTCUSTOMERS_CREATE'),
(664, 'ROLE_MOD_MODULE_STATSBESTCUSTOMERS_DELETE'),
(662, 'ROLE_MOD_MODULE_STATSBESTCUSTOMERS_READ'),
(663, 'ROLE_MOD_MODULE_STATSBESTCUSTOMERS_UPDATE'),
(665, 'ROLE_MOD_MODULE_STATSBESTPRODUCTS_CREATE'),
(668, 'ROLE_MOD_MODULE_STATSBESTPRODUCTS_DELETE'),
(666, 'ROLE_MOD_MODULE_STATSBESTPRODUCTS_READ'),
(667, 'ROLE_MOD_MODULE_STATSBESTPRODUCTS_UPDATE'),
(669, 'ROLE_MOD_MODULE_STATSBESTSUPPLIERS_CREATE'),
(672, 'ROLE_MOD_MODULE_STATSBESTSUPPLIERS_DELETE'),
(670, 'ROLE_MOD_MODULE_STATSBESTSUPPLIERS_READ'),
(671, 'ROLE_MOD_MODULE_STATSBESTSUPPLIERS_UPDATE'),
(673, 'ROLE_MOD_MODULE_STATSBESTVOUCHERS_CREATE'),
(676, 'ROLE_MOD_MODULE_STATSBESTVOUCHERS_DELETE'),
(674, 'ROLE_MOD_MODULE_STATSBESTVOUCHERS_READ'),
(675, 'ROLE_MOD_MODULE_STATSBESTVOUCHERS_UPDATE'),
(677, 'ROLE_MOD_MODULE_STATSCARRIER_CREATE'),
(680, 'ROLE_MOD_MODULE_STATSCARRIER_DELETE'),
(678, 'ROLE_MOD_MODULE_STATSCARRIER_READ'),
(679, 'ROLE_MOD_MODULE_STATSCARRIER_UPDATE'),
(681, 'ROLE_MOD_MODULE_STATSCATALOG_CREATE'),
(684, 'ROLE_MOD_MODULE_STATSCATALOG_DELETE'),
(682, 'ROLE_MOD_MODULE_STATSCATALOG_READ'),
(683, 'ROLE_MOD_MODULE_STATSCATALOG_UPDATE'),
(685, 'ROLE_MOD_MODULE_STATSCHECKUP_CREATE'),
(688, 'ROLE_MOD_MODULE_STATSCHECKUP_DELETE'),
(686, 'ROLE_MOD_MODULE_STATSCHECKUP_READ'),
(687, 'ROLE_MOD_MODULE_STATSCHECKUP_UPDATE'),
(689, 'ROLE_MOD_MODULE_STATSDATA_CREATE'),
(692, 'ROLE_MOD_MODULE_STATSDATA_DELETE'),
(690, 'ROLE_MOD_MODULE_STATSDATA_READ'),
(691, 'ROLE_MOD_MODULE_STATSDATA_UPDATE'),
(693, 'ROLE_MOD_MODULE_STATSFORECAST_CREATE'),
(696, 'ROLE_MOD_MODULE_STATSFORECAST_DELETE'),
(694, 'ROLE_MOD_MODULE_STATSFORECAST_READ'),
(695, 'ROLE_MOD_MODULE_STATSFORECAST_UPDATE'),
(697, 'ROLE_MOD_MODULE_STATSNEWSLETTER_CREATE'),
(700, 'ROLE_MOD_MODULE_STATSNEWSLETTER_DELETE'),
(698, 'ROLE_MOD_MODULE_STATSNEWSLETTER_READ'),
(699, 'ROLE_MOD_MODULE_STATSNEWSLETTER_UPDATE'),
(701, 'ROLE_MOD_MODULE_STATSPERSONALINFOS_CREATE'),
(704, 'ROLE_MOD_MODULE_STATSPERSONALINFOS_DELETE'),
(702, 'ROLE_MOD_MODULE_STATSPERSONALINFOS_READ'),
(703, 'ROLE_MOD_MODULE_STATSPERSONALINFOS_UPDATE'),
(705, 'ROLE_MOD_MODULE_STATSPRODUCT_CREATE'),
(708, 'ROLE_MOD_MODULE_STATSPRODUCT_DELETE'),
(706, 'ROLE_MOD_MODULE_STATSPRODUCT_READ'),
(707, 'ROLE_MOD_MODULE_STATSPRODUCT_UPDATE'),
(709, 'ROLE_MOD_MODULE_STATSREGISTRATIONS_CREATE'),
(712, 'ROLE_MOD_MODULE_STATSREGISTRATIONS_DELETE'),
(710, 'ROLE_MOD_MODULE_STATSREGISTRATIONS_READ'),
(711, 'ROLE_MOD_MODULE_STATSREGISTRATIONS_UPDATE'),
(713, 'ROLE_MOD_MODULE_STATSSALES_CREATE'),
(716, 'ROLE_MOD_MODULE_STATSSALES_DELETE'),
(714, 'ROLE_MOD_MODULE_STATSSALES_READ'),
(715, 'ROLE_MOD_MODULE_STATSSALES_UPDATE'),
(717, 'ROLE_MOD_MODULE_STATSSEARCH_CREATE'),
(720, 'ROLE_MOD_MODULE_STATSSEARCH_DELETE'),
(718, 'ROLE_MOD_MODULE_STATSSEARCH_READ'),
(719, 'ROLE_MOD_MODULE_STATSSEARCH_UPDATE'),
(721, 'ROLE_MOD_MODULE_STATSSTOCK_CREATE'),
(724, 'ROLE_MOD_MODULE_STATSSTOCK_DELETE'),
(722, 'ROLE_MOD_MODULE_STATSSTOCK_READ'),
(723, 'ROLE_MOD_MODULE_STATSSTOCK_UPDATE'),
(725, 'ROLE_MOD_MODULE_WELCOME_CREATE'),
(728, 'ROLE_MOD_MODULE_WELCOME_DELETE'),
(726, 'ROLE_MOD_MODULE_WELCOME_READ'),
(727, 'ROLE_MOD_MODULE_WELCOME_UPDATE'),
(1, 'ROLE_MOD_TAB_ADMINACCESS_CREATE'),
(4, 'ROLE_MOD_TAB_ADMINACCESS_DELETE'),
(2, 'ROLE_MOD_TAB_ADMINACCESS_READ'),
(3, 'ROLE_MOD_TAB_ADMINACCESS_UPDATE'),
(5, 'ROLE_MOD_TAB_ADMINADDONSCATALOG_CREATE'),
(8, 'ROLE_MOD_TAB_ADMINADDONSCATALOG_DELETE'),
(6, 'ROLE_MOD_TAB_ADMINADDONSCATALOG_READ'),
(7, 'ROLE_MOD_TAB_ADMINADDONSCATALOG_UPDATE'),
(9, 'ROLE_MOD_TAB_ADMINADDRESSES_CREATE'),
(12, 'ROLE_MOD_TAB_ADMINADDRESSES_DELETE'),
(10, 'ROLE_MOD_TAB_ADMINADDRESSES_READ'),
(11, 'ROLE_MOD_TAB_ADMINADDRESSES_UPDATE'),
(13, 'ROLE_MOD_TAB_ADMINADMINPREFERENCES_CREATE'),
(16, 'ROLE_MOD_TAB_ADMINADMINPREFERENCES_DELETE'),
(14, 'ROLE_MOD_TAB_ADMINADMINPREFERENCES_READ'),
(15, 'ROLE_MOD_TAB_ADMINADMINPREFERENCES_UPDATE'),
(17, 'ROLE_MOD_TAB_ADMINADVANCEDPARAMETERS_CREATE'),
(20, 'ROLE_MOD_TAB_ADMINADVANCEDPARAMETERS_DELETE'),
(18, 'ROLE_MOD_TAB_ADMINADVANCEDPARAMETERS_READ'),
(19, 'ROLE_MOD_TAB_ADMINADVANCEDPARAMETERS_UPDATE'),
(789, 'ROLE_MOD_TAB_ADMINAJAXPRESTASHOPCHECKOUT_CREATE'),
(792, 'ROLE_MOD_TAB_ADMINAJAXPRESTASHOPCHECKOUT_DELETE'),
(790, 'ROLE_MOD_TAB_ADMINAJAXPRESTASHOPCHECKOUT_READ'),
(791, 'ROLE_MOD_TAB_ADMINAJAXPRESTASHOPCHECKOUT_UPDATE'),
(817, 'ROLE_MOD_TAB_ADMINAJAXPSFACEBOOK_CREATE'),
(820, 'ROLE_MOD_TAB_ADMINAJAXPSFACEBOOK_DELETE'),
(818, 'ROLE_MOD_TAB_ADMINAJAXPSFACEBOOK_READ'),
(819, 'ROLE_MOD_TAB_ADMINAJAXPSFACEBOOK_UPDATE'),
(749, 'ROLE_MOD_TAB_ADMINAJAXPSGDPR_CREATE'),
(752, 'ROLE_MOD_TAB_ADMINAJAXPSGDPR_DELETE'),
(750, 'ROLE_MOD_TAB_ADMINAJAXPSGDPR_READ'),
(751, 'ROLE_MOD_TAB_ADMINAJAXPSGDPR_UPDATE'),
(829, 'ROLE_MOD_TAB_ADMINAJAXPSXMKTGWITHGOOGLE_CREATE'),
(832, 'ROLE_MOD_TAB_ADMINAJAXPSXMKTGWITHGOOGLE_DELETE'),
(830, 'ROLE_MOD_TAB_ADMINAJAXPSXMKTGWITHGOOGLE_READ'),
(831, 'ROLE_MOD_TAB_ADMINAJAXPSXMKTGWITHGOOGLE_UPDATE'),
(781, 'ROLE_MOD_TAB_ADMINAJAXPS_BUYBUTTONLITE_CREATE'),
(784, 'ROLE_MOD_TAB_ADMINAJAXPS_BUYBUTTONLITE_DELETE'),
(782, 'ROLE_MOD_TAB_ADMINAJAXPS_BUYBUTTONLITE_READ'),
(783, 'ROLE_MOD_TAB_ADMINAJAXPS_BUYBUTTONLITE_UPDATE'),
(21, 'ROLE_MOD_TAB_ADMINATTACHMENTS_CREATE'),
(24, 'ROLE_MOD_TAB_ADMINATTACHMENTS_DELETE'),
(22, 'ROLE_MOD_TAB_ADMINATTACHMENTS_READ'),
(23, 'ROLE_MOD_TAB_ADMINATTACHMENTS_UPDATE'),
(25, 'ROLE_MOD_TAB_ADMINATTRIBUTESGROUPS_CREATE'),
(28, 'ROLE_MOD_TAB_ADMINATTRIBUTESGROUPS_DELETE'),
(26, 'ROLE_MOD_TAB_ADMINATTRIBUTESGROUPS_READ'),
(27, 'ROLE_MOD_TAB_ADMINATTRIBUTESGROUPS_UPDATE'),
(29, 'ROLE_MOD_TAB_ADMINBACKUP_CREATE'),
(32, 'ROLE_MOD_TAB_ADMINBACKUP_DELETE'),
(30, 'ROLE_MOD_TAB_ADMINBACKUP_READ'),
(31, 'ROLE_MOD_TAB_ADMINBACKUP_UPDATE'),
(841, 'ROLE_MOD_TAB_ADMINBLOCKLISTING_CREATE'),
(844, 'ROLE_MOD_TAB_ADMINBLOCKLISTING_DELETE'),
(842, 'ROLE_MOD_TAB_ADMINBLOCKLISTING_READ'),
(843, 'ROLE_MOD_TAB_ADMINBLOCKLISTING_UPDATE'),
(33, 'ROLE_MOD_TAB_ADMINCARRIERS_CREATE'),
(36, 'ROLE_MOD_TAB_ADMINCARRIERS_DELETE'),
(34, 'ROLE_MOD_TAB_ADMINCARRIERS_READ'),
(35, 'ROLE_MOD_TAB_ADMINCARRIERS_UPDATE'),
(37, 'ROLE_MOD_TAB_ADMINCARTRULES_CREATE'),
(40, 'ROLE_MOD_TAB_ADMINCARTRULES_DELETE'),
(38, 'ROLE_MOD_TAB_ADMINCARTRULES_READ'),
(39, 'ROLE_MOD_TAB_ADMINCARTRULES_UPDATE'),
(41, 'ROLE_MOD_TAB_ADMINCARTS_CREATE'),
(44, 'ROLE_MOD_TAB_ADMINCARTS_DELETE'),
(42, 'ROLE_MOD_TAB_ADMINCARTS_READ'),
(43, 'ROLE_MOD_TAB_ADMINCARTS_UPDATE'),
(45, 'ROLE_MOD_TAB_ADMINCATALOG_CREATE'),
(48, 'ROLE_MOD_TAB_ADMINCATALOG_DELETE'),
(46, 'ROLE_MOD_TAB_ADMINCATALOG_READ'),
(47, 'ROLE_MOD_TAB_ADMINCATALOG_UPDATE'),
(49, 'ROLE_MOD_TAB_ADMINCATEGORIES_CREATE'),
(52, 'ROLE_MOD_TAB_ADMINCATEGORIES_DELETE'),
(50, 'ROLE_MOD_TAB_ADMINCATEGORIES_READ'),
(51, 'ROLE_MOD_TAB_ADMINCATEGORIES_UPDATE'),
(53, 'ROLE_MOD_TAB_ADMINCMSCONTENT_CREATE'),
(56, 'ROLE_MOD_TAB_ADMINCMSCONTENT_DELETE'),
(54, 'ROLE_MOD_TAB_ADMINCMSCONTENT_READ'),
(55, 'ROLE_MOD_TAB_ADMINCMSCONTENT_UPDATE'),
(597, 'ROLE_MOD_TAB_ADMINCONFIGUREFAVICONBO_CREATE'),
(600, 'ROLE_MOD_TAB_ADMINCONFIGUREFAVICONBO_DELETE'),
(598, 'ROLE_MOD_TAB_ADMINCONFIGUREFAVICONBO_READ'),
(599, 'ROLE_MOD_TAB_ADMINCONFIGUREFAVICONBO_UPDATE'),
(57, 'ROLE_MOD_TAB_ADMINCONTACTS_CREATE'),
(60, 'ROLE_MOD_TAB_ADMINCONTACTS_DELETE'),
(58, 'ROLE_MOD_TAB_ADMINCONTACTS_READ'),
(59, 'ROLE_MOD_TAB_ADMINCONTACTS_UPDATE'),
(61, 'ROLE_MOD_TAB_ADMINCOUNTRIES_CREATE'),
(64, 'ROLE_MOD_TAB_ADMINCOUNTRIES_DELETE'),
(62, 'ROLE_MOD_TAB_ADMINCOUNTRIES_READ'),
(63, 'ROLE_MOD_TAB_ADMINCOUNTRIES_UPDATE'),
(889, 'ROLE_MOD_TAB_ADMINCRAZYAJAXURL_CREATE'),
(892, 'ROLE_MOD_TAB_ADMINCRAZYAJAXURL_DELETE'),
(890, 'ROLE_MOD_TAB_ADMINCRAZYAJAXURL_READ'),
(891, 'ROLE_MOD_TAB_ADMINCRAZYAJAXURL_UPDATE'),
(917, 'ROLE_MOD_TAB_ADMINCRAZYBRANDS_CREATE'),
(920, 'ROLE_MOD_TAB_ADMINCRAZYBRANDS_DELETE'),
(918, 'ROLE_MOD_TAB_ADMINCRAZYBRANDS_READ'),
(919, 'ROLE_MOD_TAB_ADMINCRAZYBRANDS_UPDATE'),
(909, 'ROLE_MOD_TAB_ADMINCRAZYCATEGORIES_CREATE'),
(912, 'ROLE_MOD_TAB_ADMINCRAZYCATEGORIES_DELETE'),
(910, 'ROLE_MOD_TAB_ADMINCRAZYCATEGORIES_READ'),
(911, 'ROLE_MOD_TAB_ADMINCRAZYCATEGORIES_UPDATE'),
(897, 'ROLE_MOD_TAB_ADMINCRAZYCONTENT_CREATE'),
(900, 'ROLE_MOD_TAB_ADMINCRAZYCONTENT_DELETE'),
(898, 'ROLE_MOD_TAB_ADMINCRAZYCONTENT_READ'),
(899, 'ROLE_MOD_TAB_ADMINCRAZYCONTENT_UPDATE'),
(865, 'ROLE_MOD_TAB_ADMINCRAZYEDITOR_CREATE'),
(868, 'ROLE_MOD_TAB_ADMINCRAZYEDITOR_DELETE'),
(866, 'ROLE_MOD_TAB_ADMINCRAZYEDITOR_READ'),
(867, 'ROLE_MOD_TAB_ADMINCRAZYEDITOR_UPDATE'),
(881, 'ROLE_MOD_TAB_ADMINCRAZYEXTENDEDMODULES_CREATE'),
(884, 'ROLE_MOD_TAB_ADMINCRAZYEXTENDEDMODULES_DELETE'),
(882, 'ROLE_MOD_TAB_ADMINCRAZYEXTENDEDMODULES_READ'),
(883, 'ROLE_MOD_TAB_ADMINCRAZYEXTENDEDMODULES_UPDATE'),
(869, 'ROLE_MOD_TAB_ADMINCRAZYFONTS_CREATE'),
(872, 'ROLE_MOD_TAB_ADMINCRAZYFONTS_DELETE'),
(870, 'ROLE_MOD_TAB_ADMINCRAZYFONTS_READ'),
(871, 'ROLE_MOD_TAB_ADMINCRAZYFONTS_UPDATE'),
(885, 'ROLE_MOD_TAB_ADMINCRAZYFRONTENDEDITOR_CREATE'),
(888, 'ROLE_MOD_TAB_ADMINCRAZYFRONTENDEDITOR_DELETE'),
(886, 'ROLE_MOD_TAB_ADMINCRAZYFRONTENDEDITOR_READ'),
(887, 'ROLE_MOD_TAB_ADMINCRAZYFRONTENDEDITOR_UPDATE'),
(893, 'ROLE_MOD_TAB_ADMINCRAZYIMAGES_CREATE'),
(896, 'ROLE_MOD_TAB_ADMINCRAZYIMAGES_DELETE'),
(894, 'ROLE_MOD_TAB_ADMINCRAZYIMAGES_READ'),
(895, 'ROLE_MOD_TAB_ADMINCRAZYIMAGES_UPDATE'),
(861, 'ROLE_MOD_TAB_ADMINCRAZYMAIN_CREATE'),
(864, 'ROLE_MOD_TAB_ADMINCRAZYMAIN_DELETE'),
(862, 'ROLE_MOD_TAB_ADMINCRAZYMAIN_READ'),
(863, 'ROLE_MOD_TAB_ADMINCRAZYMAIN_UPDATE'),
(901, 'ROLE_MOD_TAB_ADMINCRAZYPAGES_CREATE'),
(904, 'ROLE_MOD_TAB_ADMINCRAZYPAGES_DELETE'),
(902, 'ROLE_MOD_TAB_ADMINCRAZYPAGES_READ'),
(903, 'ROLE_MOD_TAB_ADMINCRAZYPAGES_UPDATE'),
(925, 'ROLE_MOD_TAB_ADMINCRAZYPRDLAYOUTS_CREATE'),
(928, 'ROLE_MOD_TAB_ADMINCRAZYPRDLAYOUTS_DELETE'),
(926, 'ROLE_MOD_TAB_ADMINCRAZYPRDLAYOUTS_READ'),
(927, 'ROLE_MOD_TAB_ADMINCRAZYPRDLAYOUTS_UPDATE'),
(905, 'ROLE_MOD_TAB_ADMINCRAZYPRODUCTS_CREATE'),
(908, 'ROLE_MOD_TAB_ADMINCRAZYPRODUCTS_DELETE'),
(906, 'ROLE_MOD_TAB_ADMINCRAZYPRODUCTS_READ'),
(907, 'ROLE_MOD_TAB_ADMINCRAZYPRODUCTS_UPDATE'),
(873, 'ROLE_MOD_TAB_ADMINCRAZYPSEICON_CREATE'),
(876, 'ROLE_MOD_TAB_ADMINCRAZYPSEICON_DELETE'),
(874, 'ROLE_MOD_TAB_ADMINCRAZYPSEICON_READ'),
(875, 'ROLE_MOD_TAB_ADMINCRAZYPSEICON_UPDATE'),
(877, 'ROLE_MOD_TAB_ADMINCRAZYSETTING_CREATE'),
(880, 'ROLE_MOD_TAB_ADMINCRAZYSETTING_DELETE'),
(878, 'ROLE_MOD_TAB_ADMINCRAZYSETTING_READ'),
(879, 'ROLE_MOD_TAB_ADMINCRAZYSETTING_UPDATE'),
(913, 'ROLE_MOD_TAB_ADMINCRAZYSUPPLIERS_CREATE'),
(916, 'ROLE_MOD_TAB_ADMINCRAZYSUPPLIERS_DELETE'),
(914, 'ROLE_MOD_TAB_ADMINCRAZYSUPPLIERS_READ'),
(915, 'ROLE_MOD_TAB_ADMINCRAZYSUPPLIERS_UPDATE'),
(65, 'ROLE_MOD_TAB_ADMINCURRENCIES_CREATE'),
(68, 'ROLE_MOD_TAB_ADMINCURRENCIES_DELETE'),
(66, 'ROLE_MOD_TAB_ADMINCURRENCIES_READ'),
(67, 'ROLE_MOD_TAB_ADMINCURRENCIES_UPDATE'),
(69, 'ROLE_MOD_TAB_ADMINCUSTOMERPREFERENCES_CREATE'),
(72, 'ROLE_MOD_TAB_ADMINCUSTOMERPREFERENCES_DELETE'),
(70, 'ROLE_MOD_TAB_ADMINCUSTOMERPREFERENCES_READ'),
(71, 'ROLE_MOD_TAB_ADMINCUSTOMERPREFERENCES_UPDATE'),
(73, 'ROLE_MOD_TAB_ADMINCUSTOMERS_CREATE'),
(76, 'ROLE_MOD_TAB_ADMINCUSTOMERS_DELETE'),
(74, 'ROLE_MOD_TAB_ADMINCUSTOMERS_READ'),
(75, 'ROLE_MOD_TAB_ADMINCUSTOMERS_UPDATE'),
(77, 'ROLE_MOD_TAB_ADMINCUSTOMERTHREADS_CREATE'),
(80, 'ROLE_MOD_TAB_ADMINCUSTOMERTHREADS_DELETE'),
(78, 'ROLE_MOD_TAB_ADMINCUSTOMERTHREADS_READ'),
(79, 'ROLE_MOD_TAB_ADMINCUSTOMERTHREADS_UPDATE'),
(81, 'ROLE_MOD_TAB_ADMINDASHBOARD_CREATE'),
(84, 'ROLE_MOD_TAB_ADMINDASHBOARD_DELETE'),
(82, 'ROLE_MOD_TAB_ADMINDASHBOARD_READ'),
(83, 'ROLE_MOD_TAB_ADMINDASHBOARD_UPDATE'),
(513, 'ROLE_MOD_TAB_ADMINDASHGOALS_CREATE'),
(516, 'ROLE_MOD_TAB_ADMINDASHGOALS_DELETE'),
(514, 'ROLE_MOD_TAB_ADMINDASHGOALS_READ'),
(515, 'ROLE_MOD_TAB_ADMINDASHGOALS_UPDATE'),
(85, 'ROLE_MOD_TAB_ADMINDELIVERYSLIP_CREATE'),
(88, 'ROLE_MOD_TAB_ADMINDELIVERYSLIP_DELETE'),
(86, 'ROLE_MOD_TAB_ADMINDELIVERYSLIP_READ'),
(87, 'ROLE_MOD_TAB_ADMINDELIVERYSLIP_UPDATE'),
(753, 'ROLE_MOD_TAB_ADMINDOWNLOADINVOICESPSGDPR_CREATE'),
(756, 'ROLE_MOD_TAB_ADMINDOWNLOADINVOICESPSGDPR_DELETE'),
(754, 'ROLE_MOD_TAB_ADMINDOWNLOADINVOICESPSGDPR_READ'),
(755, 'ROLE_MOD_TAB_ADMINDOWNLOADINVOICESPSGDPR_UPDATE'),
(89, 'ROLE_MOD_TAB_ADMINEMAILS_CREATE'),
(92, 'ROLE_MOD_TAB_ADMINEMAILS_DELETE'),
(90, 'ROLE_MOD_TAB_ADMINEMAILS_READ'),
(91, 'ROLE_MOD_TAB_ADMINEMAILS_UPDATE'),
(93, 'ROLE_MOD_TAB_ADMINEMPLOYEES_CREATE'),
(96, 'ROLE_MOD_TAB_ADMINEMPLOYEES_DELETE'),
(94, 'ROLE_MOD_TAB_ADMINEMPLOYEES_READ'),
(95, 'ROLE_MOD_TAB_ADMINEMPLOYEES_UPDATE'),
(481, 'ROLE_MOD_TAB_ADMINFEATUREFLAG_CREATE'),
(484, 'ROLE_MOD_TAB_ADMINFEATUREFLAG_DELETE'),
(482, 'ROLE_MOD_TAB_ADMINFEATUREFLAG_READ'),
(483, 'ROLE_MOD_TAB_ADMINFEATUREFLAG_UPDATE'),
(97, 'ROLE_MOD_TAB_ADMINFEATURES_CREATE'),
(100, 'ROLE_MOD_TAB_ADMINFEATURES_DELETE'),
(98, 'ROLE_MOD_TAB_ADMINFEATURES_READ'),
(99, 'ROLE_MOD_TAB_ADMINFEATURES_UPDATE'),
(733, 'ROLE_MOD_TAB_ADMINGAMIFICATION_CREATE'),
(736, 'ROLE_MOD_TAB_ADMINGAMIFICATION_DELETE'),
(734, 'ROLE_MOD_TAB_ADMINGAMIFICATION_READ'),
(735, 'ROLE_MOD_TAB_ADMINGAMIFICATION_UPDATE'),
(101, 'ROLE_MOD_TAB_ADMINGENDERS_CREATE'),
(104, 'ROLE_MOD_TAB_ADMINGENDERS_DELETE'),
(102, 'ROLE_MOD_TAB_ADMINGENDERS_READ'),
(103, 'ROLE_MOD_TAB_ADMINGENDERS_UPDATE'),
(105, 'ROLE_MOD_TAB_ADMINGEOLOCATION_CREATE'),
(108, 'ROLE_MOD_TAB_ADMINGEOLOCATION_DELETE'),
(106, 'ROLE_MOD_TAB_ADMINGEOLOCATION_READ'),
(107, 'ROLE_MOD_TAB_ADMINGEOLOCATION_UPDATE'),
(109, 'ROLE_MOD_TAB_ADMINGROUPS_CREATE'),
(112, 'ROLE_MOD_TAB_ADMINGROUPS_DELETE'),
(110, 'ROLE_MOD_TAB_ADMINGROUPS_READ'),
(111, 'ROLE_MOD_TAB_ADMINGROUPS_UPDATE'),
(113, 'ROLE_MOD_TAB_ADMINIMAGES_CREATE'),
(116, 'ROLE_MOD_TAB_ADMINIMAGES_DELETE'),
(114, 'ROLE_MOD_TAB_ADMINIMAGES_READ'),
(115, 'ROLE_MOD_TAB_ADMINIMAGES_UPDATE'),
(117, 'ROLE_MOD_TAB_ADMINIMPORT_CREATE'),
(120, 'ROLE_MOD_TAB_ADMINIMPORT_DELETE'),
(118, 'ROLE_MOD_TAB_ADMINIMPORT_READ'),
(119, 'ROLE_MOD_TAB_ADMINIMPORT_UPDATE'),
(121, 'ROLE_MOD_TAB_ADMININFORMATION_CREATE'),
(124, 'ROLE_MOD_TAB_ADMININFORMATION_DELETE'),
(122, 'ROLE_MOD_TAB_ADMININFORMATION_READ'),
(123, 'ROLE_MOD_TAB_ADMININFORMATION_UPDATE'),
(125, 'ROLE_MOD_TAB_ADMININTERNATIONAL_CREATE'),
(128, 'ROLE_MOD_TAB_ADMININTERNATIONAL_DELETE'),
(126, 'ROLE_MOD_TAB_ADMININTERNATIONAL_READ'),
(127, 'ROLE_MOD_TAB_ADMININTERNATIONAL_UPDATE'),
(129, 'ROLE_MOD_TAB_ADMININVOICES_CREATE'),
(132, 'ROLE_MOD_TAB_ADMININVOICES_DELETE'),
(130, 'ROLE_MOD_TAB_ADMININVOICES_READ'),
(131, 'ROLE_MOD_TAB_ADMININVOICES_UPDATE'),
(133, 'ROLE_MOD_TAB_ADMINLANGUAGES_CREATE'),
(136, 'ROLE_MOD_TAB_ADMINLANGUAGES_DELETE'),
(134, 'ROLE_MOD_TAB_ADMINLANGUAGES_READ'),
(135, 'ROLE_MOD_TAB_ADMINLANGUAGES_UPDATE'),
(137, 'ROLE_MOD_TAB_ADMINLINKWIDGET_CREATE'),
(140, 'ROLE_MOD_TAB_ADMINLINKWIDGET_DELETE'),
(138, 'ROLE_MOD_TAB_ADMINLINKWIDGET_READ'),
(139, 'ROLE_MOD_TAB_ADMINLINKWIDGET_UPDATE'),
(141, 'ROLE_MOD_TAB_ADMINLOCALIZATION_CREATE'),
(144, 'ROLE_MOD_TAB_ADMINLOCALIZATION_DELETE'),
(142, 'ROLE_MOD_TAB_ADMINLOCALIZATION_READ'),
(143, 'ROLE_MOD_TAB_ADMINLOCALIZATION_UPDATE'),
(145, 'ROLE_MOD_TAB_ADMINLOGS_CREATE'),
(148, 'ROLE_MOD_TAB_ADMINLOGS_DELETE'),
(146, 'ROLE_MOD_TAB_ADMINLOGS_READ'),
(147, 'ROLE_MOD_TAB_ADMINLOGS_UPDATE'),
(465, 'ROLE_MOD_TAB_ADMINMAILTHEME_CREATE'),
(468, 'ROLE_MOD_TAB_ADMINMAILTHEME_DELETE'),
(466, 'ROLE_MOD_TAB_ADMINMAILTHEME_READ'),
(467, 'ROLE_MOD_TAB_ADMINMAILTHEME_UPDATE'),
(149, 'ROLE_MOD_TAB_ADMINMAINTENANCE_CREATE'),
(152, 'ROLE_MOD_TAB_ADMINMAINTENANCE_DELETE'),
(150, 'ROLE_MOD_TAB_ADMINMAINTENANCE_READ'),
(151, 'ROLE_MOD_TAB_ADMINMAINTENANCE_UPDATE'),
(153, 'ROLE_MOD_TAB_ADMINMANUFACTURERS_CREATE'),
(156, 'ROLE_MOD_TAB_ADMINMANUFACTURERS_DELETE'),
(154, 'ROLE_MOD_TAB_ADMINMANUFACTURERS_READ'),
(155, 'ROLE_MOD_TAB_ADMINMANUFACTURERS_UPDATE'),
(157, 'ROLE_MOD_TAB_ADMINMETA_CREATE'),
(160, 'ROLE_MOD_TAB_ADMINMETA_DELETE'),
(158, 'ROLE_MOD_TAB_ADMINMETA_READ'),
(159, 'ROLE_MOD_TAB_ADMINMETA_UPDATE'),
(805, 'ROLE_MOD_TAB_ADMINMETRICSCONTROLLER_CREATE'),
(808, 'ROLE_MOD_TAB_ADMINMETRICSCONTROLLER_DELETE'),
(806, 'ROLE_MOD_TAB_ADMINMETRICSCONTROLLER_READ'),
(807, 'ROLE_MOD_TAB_ADMINMETRICSCONTROLLER_UPDATE'),
(801, 'ROLE_MOD_TAB_ADMINMETRICSLEGACYSTATSCONTROLLER_CREATE'),
(804, 'ROLE_MOD_TAB_ADMINMETRICSLEGACYSTATSCONTROLLER_DELETE'),
(802, 'ROLE_MOD_TAB_ADMINMETRICSLEGACYSTATSCONTROLLER_READ'),
(803, 'ROLE_MOD_TAB_ADMINMETRICSLEGACYSTATSCONTROLLER_UPDATE'),
(473, 'ROLE_MOD_TAB_ADMINMODULESCATALOG_CREATE'),
(476, 'ROLE_MOD_TAB_ADMINMODULESCATALOG_DELETE'),
(474, 'ROLE_MOD_TAB_ADMINMODULESCATALOG_READ'),
(475, 'ROLE_MOD_TAB_ADMINMODULESCATALOG_UPDATE'),
(469, 'ROLE_MOD_TAB_ADMINMODULESMANAGE_CREATE'),
(472, 'ROLE_MOD_TAB_ADMINMODULESMANAGE_DELETE'),
(470, 'ROLE_MOD_TAB_ADMINMODULESMANAGE_READ'),
(471, 'ROLE_MOD_TAB_ADMINMODULESMANAGE_UPDATE'),
(173, 'ROLE_MOD_TAB_ADMINMODULESNOTIFICATIONS_CREATE'),
(176, 'ROLE_MOD_TAB_ADMINMODULESNOTIFICATIONS_DELETE'),
(174, 'ROLE_MOD_TAB_ADMINMODULESNOTIFICATIONS_READ'),
(175, 'ROLE_MOD_TAB_ADMINMODULESNOTIFICATIONS_UPDATE'),
(165, 'ROLE_MOD_TAB_ADMINMODULESPOSITIONS_CREATE'),
(168, 'ROLE_MOD_TAB_ADMINMODULESPOSITIONS_DELETE'),
(166, 'ROLE_MOD_TAB_ADMINMODULESPOSITIONS_READ'),
(167, 'ROLE_MOD_TAB_ADMINMODULESPOSITIONS_UPDATE'),
(177, 'ROLE_MOD_TAB_ADMINMODULESSF_CREATE'),
(180, 'ROLE_MOD_TAB_ADMINMODULESSF_DELETE'),
(178, 'ROLE_MOD_TAB_ADMINMODULESSF_READ'),
(179, 'ROLE_MOD_TAB_ADMINMODULESSF_UPDATE'),
(169, 'ROLE_MOD_TAB_ADMINMODULESUPDATES_CREATE'),
(172, 'ROLE_MOD_TAB_ADMINMODULESUPDATES_DELETE'),
(170, 'ROLE_MOD_TAB_ADMINMODULESUPDATES_READ'),
(171, 'ROLE_MOD_TAB_ADMINMODULESUPDATES_UPDATE'),
(161, 'ROLE_MOD_TAB_ADMINMODULES_CREATE'),
(164, 'ROLE_MOD_TAB_ADMINMODULES_DELETE'),
(162, 'ROLE_MOD_TAB_ADMINMODULES_READ'),
(163, 'ROLE_MOD_TAB_ADMINMODULES_UPDATE'),
(853, 'ROLE_MOD_TAB_ADMINMYSHOPHELPER_CREATE'),
(856, 'ROLE_MOD_TAB_ADMINMYSHOPHELPER_DELETE'),
(854, 'ROLE_MOD_TAB_ADMINMYSHOPHELPER_READ'),
(855, 'ROLE_MOD_TAB_ADMINMYSHOPHELPER_UPDATE'),
(181, 'ROLE_MOD_TAB_ADMINORDERMESSAGE_CREATE'),
(184, 'ROLE_MOD_TAB_ADMINORDERMESSAGE_DELETE'),
(182, 'ROLE_MOD_TAB_ADMINORDERMESSAGE_READ'),
(183, 'ROLE_MOD_TAB_ADMINORDERMESSAGE_UPDATE'),
(185, 'ROLE_MOD_TAB_ADMINORDERPREFERENCES_CREATE'),
(188, 'ROLE_MOD_TAB_ADMINORDERPREFERENCES_DELETE'),
(186, 'ROLE_MOD_TAB_ADMINORDERPREFERENCES_READ'),
(187, 'ROLE_MOD_TAB_ADMINORDERPREFERENCES_UPDATE'),
(189, 'ROLE_MOD_TAB_ADMINORDERS_CREATE'),
(192, 'ROLE_MOD_TAB_ADMINORDERS_DELETE'),
(190, 'ROLE_MOD_TAB_ADMINORDERS_READ'),
(191, 'ROLE_MOD_TAB_ADMINORDERS_UPDATE'),
(193, 'ROLE_MOD_TAB_ADMINOUTSTANDING_CREATE'),
(196, 'ROLE_MOD_TAB_ADMINOUTSTANDING_DELETE'),
(194, 'ROLE_MOD_TAB_ADMINOUTSTANDING_READ'),
(195, 'ROLE_MOD_TAB_ADMINOUTSTANDING_UPDATE'),
(197, 'ROLE_MOD_TAB_ADMINPARENTATTRIBUTESGROUPS_CREATE'),
(200, 'ROLE_MOD_TAB_ADMINPARENTATTRIBUTESGROUPS_DELETE'),
(198, 'ROLE_MOD_TAB_ADMINPARENTATTRIBUTESGROUPS_READ'),
(199, 'ROLE_MOD_TAB_ADMINPARENTATTRIBUTESGROUPS_UPDATE'),
(201, 'ROLE_MOD_TAB_ADMINPARENTCARTRULES_CREATE'),
(204, 'ROLE_MOD_TAB_ADMINPARENTCARTRULES_DELETE'),
(202, 'ROLE_MOD_TAB_ADMINPARENTCARTRULES_READ'),
(203, 'ROLE_MOD_TAB_ADMINPARENTCARTRULES_UPDATE'),
(205, 'ROLE_MOD_TAB_ADMINPARENTCOUNTRIES_CREATE'),
(208, 'ROLE_MOD_TAB_ADMINPARENTCOUNTRIES_DELETE'),
(206, 'ROLE_MOD_TAB_ADMINPARENTCOUNTRIES_READ'),
(207, 'ROLE_MOD_TAB_ADMINPARENTCOUNTRIES_UPDATE'),
(213, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMERPREFERENCES_CREATE'),
(216, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMERPREFERENCES_DELETE'),
(214, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMERPREFERENCES_READ'),
(215, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMERPREFERENCES_UPDATE'),
(217, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMERTHREADS_CREATE'),
(220, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMERTHREADS_DELETE'),
(218, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMERTHREADS_READ'),
(219, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMERTHREADS_UPDATE'),
(209, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMER_CREATE'),
(212, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMER_DELETE'),
(210, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMER_READ'),
(211, 'ROLE_MOD_TAB_ADMINPARENTCUSTOMER_UPDATE'),
(221, 'ROLE_MOD_TAB_ADMINPARENTEMPLOYEES_CREATE'),
(224, 'ROLE_MOD_TAB_ADMINPARENTEMPLOYEES_DELETE'),
(222, 'ROLE_MOD_TAB_ADMINPARENTEMPLOYEES_READ'),
(223, 'ROLE_MOD_TAB_ADMINPARENTEMPLOYEES_UPDATE'),
(225, 'ROLE_MOD_TAB_ADMINPARENTLOCALIZATION_CREATE'),
(228, 'ROLE_MOD_TAB_ADMINPARENTLOCALIZATION_DELETE'),
(226, 'ROLE_MOD_TAB_ADMINPARENTLOCALIZATION_READ'),
(227, 'ROLE_MOD_TAB_ADMINPARENTLOCALIZATION_UPDATE'),
(461, 'ROLE_MOD_TAB_ADMINPARENTMAILTHEME_CREATE'),
(464, 'ROLE_MOD_TAB_ADMINPARENTMAILTHEME_DELETE'),
(462, 'ROLE_MOD_TAB_ADMINPARENTMAILTHEME_READ'),
(463, 'ROLE_MOD_TAB_ADMINPARENTMAILTHEME_UPDATE'),
(229, 'ROLE_MOD_TAB_ADMINPARENTMANUFACTURERS_CREATE'),
(232, 'ROLE_MOD_TAB_ADMINPARENTMANUFACTURERS_DELETE'),
(230, 'ROLE_MOD_TAB_ADMINPARENTMANUFACTURERS_READ'),
(231, 'ROLE_MOD_TAB_ADMINPARENTMANUFACTURERS_UPDATE'),
(237, 'ROLE_MOD_TAB_ADMINPARENTMETA_CREATE'),
(240, 'ROLE_MOD_TAB_ADMINPARENTMETA_DELETE'),
(238, 'ROLE_MOD_TAB_ADMINPARENTMETA_READ'),
(239, 'ROLE_MOD_TAB_ADMINPARENTMETA_UPDATE'),
(477, 'ROLE_MOD_TAB_ADMINPARENTMODULESCATALOG_CREATE'),
(480, 'ROLE_MOD_TAB_ADMINPARENTMODULESCATALOG_DELETE'),
(478, 'ROLE_MOD_TAB_ADMINPARENTMODULESCATALOG_READ'),
(479, 'ROLE_MOD_TAB_ADMINPARENTMODULESCATALOG_UPDATE'),
(233, 'ROLE_MOD_TAB_ADMINPARENTMODULESSF_CREATE'),
(236, 'ROLE_MOD_TAB_ADMINPARENTMODULESSF_DELETE'),
(234, 'ROLE_MOD_TAB_ADMINPARENTMODULESSF_READ'),
(235, 'ROLE_MOD_TAB_ADMINPARENTMODULESSF_UPDATE'),
(241, 'ROLE_MOD_TAB_ADMINPARENTMODULES_CREATE'),
(244, 'ROLE_MOD_TAB_ADMINPARENTMODULES_DELETE'),
(242, 'ROLE_MOD_TAB_ADMINPARENTMODULES_READ'),
(243, 'ROLE_MOD_TAB_ADMINPARENTMODULES_UPDATE'),
(245, 'ROLE_MOD_TAB_ADMINPARENTORDERPREFERENCES_CREATE'),
(248, 'ROLE_MOD_TAB_ADMINPARENTORDERPREFERENCES_DELETE'),
(246, 'ROLE_MOD_TAB_ADMINPARENTORDERPREFERENCES_READ'),
(247, 'ROLE_MOD_TAB_ADMINPARENTORDERPREFERENCES_UPDATE'),
(249, 'ROLE_MOD_TAB_ADMINPARENTORDERS_CREATE'),
(252, 'ROLE_MOD_TAB_ADMINPARENTORDERS_DELETE'),
(250, 'ROLE_MOD_TAB_ADMINPARENTORDERS_READ'),
(251, 'ROLE_MOD_TAB_ADMINPARENTORDERS_UPDATE'),
(253, 'ROLE_MOD_TAB_ADMINPARENTPAYMENT_CREATE'),
(256, 'ROLE_MOD_TAB_ADMINPARENTPAYMENT_DELETE'),
(254, 'ROLE_MOD_TAB_ADMINPARENTPAYMENT_READ'),
(255, 'ROLE_MOD_TAB_ADMINPARENTPAYMENT_UPDATE'),
(257, 'ROLE_MOD_TAB_ADMINPARENTPREFERENCES_CREATE'),
(260, 'ROLE_MOD_TAB_ADMINPARENTPREFERENCES_DELETE'),
(258, 'ROLE_MOD_TAB_ADMINPARENTPREFERENCES_READ'),
(259, 'ROLE_MOD_TAB_ADMINPARENTPREFERENCES_UPDATE'),
(261, 'ROLE_MOD_TAB_ADMINPARENTREQUESTSQL_CREATE'),
(264, 'ROLE_MOD_TAB_ADMINPARENTREQUESTSQL_DELETE'),
(262, 'ROLE_MOD_TAB_ADMINPARENTREQUESTSQL_READ'),
(263, 'ROLE_MOD_TAB_ADMINPARENTREQUESTSQL_UPDATE'),
(265, 'ROLE_MOD_TAB_ADMINPARENTSEARCHCONF_CREATE'),
(268, 'ROLE_MOD_TAB_ADMINPARENTSEARCHCONF_DELETE'),
(266, 'ROLE_MOD_TAB_ADMINPARENTSEARCHCONF_READ'),
(267, 'ROLE_MOD_TAB_ADMINPARENTSEARCHCONF_UPDATE'),
(269, 'ROLE_MOD_TAB_ADMINPARENTSHIPPING_CREATE'),
(272, 'ROLE_MOD_TAB_ADMINPARENTSHIPPING_DELETE'),
(270, 'ROLE_MOD_TAB_ADMINPARENTSHIPPING_READ'),
(271, 'ROLE_MOD_TAB_ADMINPARENTSHIPPING_UPDATE'),
(273, 'ROLE_MOD_TAB_ADMINPARENTSTOCKMANAGEMENT_CREATE'),
(276, 'ROLE_MOD_TAB_ADMINPARENTSTOCKMANAGEMENT_DELETE'),
(274, 'ROLE_MOD_TAB_ADMINPARENTSTOCKMANAGEMENT_READ'),
(275, 'ROLE_MOD_TAB_ADMINPARENTSTOCKMANAGEMENT_UPDATE'),
(277, 'ROLE_MOD_TAB_ADMINPARENTSTORES_CREATE'),
(280, 'ROLE_MOD_TAB_ADMINPARENTSTORES_DELETE'),
(278, 'ROLE_MOD_TAB_ADMINPARENTSTORES_READ'),
(279, 'ROLE_MOD_TAB_ADMINPARENTSTORES_UPDATE'),
(281, 'ROLE_MOD_TAB_ADMINPARENTTAXES_CREATE'),
(284, 'ROLE_MOD_TAB_ADMINPARENTTAXES_DELETE'),
(282, 'ROLE_MOD_TAB_ADMINPARENTTAXES_READ'),
(283, 'ROLE_MOD_TAB_ADMINPARENTTAXES_UPDATE'),
(285, 'ROLE_MOD_TAB_ADMINPARENTTHEMES_CREATE'),
(288, 'ROLE_MOD_TAB_ADMINPARENTTHEMES_DELETE'),
(286, 'ROLE_MOD_TAB_ADMINPARENTTHEMES_READ'),
(287, 'ROLE_MOD_TAB_ADMINPARENTTHEMES_UPDATE'),
(293, 'ROLE_MOD_TAB_ADMINPAYMENTPREFERENCES_CREATE'),
(296, 'ROLE_MOD_TAB_ADMINPAYMENTPREFERENCES_DELETE'),
(294, 'ROLE_MOD_TAB_ADMINPAYMENTPREFERENCES_READ'),
(295, 'ROLE_MOD_TAB_ADMINPAYMENTPREFERENCES_UPDATE'),
(289, 'ROLE_MOD_TAB_ADMINPAYMENT_CREATE'),
(292, 'ROLE_MOD_TAB_ADMINPAYMENT_DELETE'),
(290, 'ROLE_MOD_TAB_ADMINPAYMENT_READ'),
(291, 'ROLE_MOD_TAB_ADMINPAYMENT_UPDATE'),
(793, 'ROLE_MOD_TAB_ADMINPAYPALONBOARDINGPRESTASHOPCHECKOUT_CREATE'),
(796, 'ROLE_MOD_TAB_ADMINPAYPALONBOARDINGPRESTASHOPCHECKOUT_DELETE'),
(794, 'ROLE_MOD_TAB_ADMINPAYPALONBOARDINGPRESTASHOPCHECKOUT_READ'),
(795, 'ROLE_MOD_TAB_ADMINPAYPALONBOARDINGPRESTASHOPCHECKOUT_UPDATE'),
(297, 'ROLE_MOD_TAB_ADMINPERFORMANCE_CREATE'),
(300, 'ROLE_MOD_TAB_ADMINPERFORMANCE_DELETE'),
(298, 'ROLE_MOD_TAB_ADMINPERFORMANCE_READ'),
(299, 'ROLE_MOD_TAB_ADMINPERFORMANCE_UPDATE'),
(301, 'ROLE_MOD_TAB_ADMINPPREFERENCES_CREATE'),
(304, 'ROLE_MOD_TAB_ADMINPPREFERENCES_DELETE'),
(302, 'ROLE_MOD_TAB_ADMINPPREFERENCES_READ'),
(303, 'ROLE_MOD_TAB_ADMINPPREFERENCES_UPDATE'),
(305, 'ROLE_MOD_TAB_ADMINPREFERENCES_CREATE'),
(308, 'ROLE_MOD_TAB_ADMINPREFERENCES_DELETE'),
(306, 'ROLE_MOD_TAB_ADMINPREFERENCES_READ'),
(307, 'ROLE_MOD_TAB_ADMINPREFERENCES_UPDATE'),
(309, 'ROLE_MOD_TAB_ADMINPRODUCTS_CREATE'),
(312, 'ROLE_MOD_TAB_ADMINPRODUCTS_DELETE'),
(310, 'ROLE_MOD_TAB_ADMINPRODUCTS_READ'),
(311, 'ROLE_MOD_TAB_ADMINPRODUCTS_UPDATE'),
(313, 'ROLE_MOD_TAB_ADMINPROFILES_CREATE'),
(316, 'ROLE_MOD_TAB_ADMINPROFILES_DELETE'),
(314, 'ROLE_MOD_TAB_ADMINPROFILES_READ'),
(315, 'ROLE_MOD_TAB_ADMINPROFILES_UPDATE'),
(813, 'ROLE_MOD_TAB_ADMINPSFACEBOOKMODULE_CREATE'),
(816, 'ROLE_MOD_TAB_ADMINPSFACEBOOKMODULE_DELETE'),
(814, 'ROLE_MOD_TAB_ADMINPSFACEBOOKMODULE_READ'),
(815, 'ROLE_MOD_TAB_ADMINPSFACEBOOKMODULE_UPDATE'),
(765, 'ROLE_MOD_TAB_ADMINPSMBOADDONS_CREATE'),
(768, 'ROLE_MOD_TAB_ADMINPSMBOADDONS_DELETE'),
(766, 'ROLE_MOD_TAB_ADMINPSMBOADDONS_READ'),
(767, 'ROLE_MOD_TAB_ADMINPSMBOADDONS_UPDATE'),
(761, 'ROLE_MOD_TAB_ADMINPSMBOMODULE_CREATE'),
(764, 'ROLE_MOD_TAB_ADMINPSMBOMODULE_DELETE'),
(762, 'ROLE_MOD_TAB_ADMINPSMBOMODULE_READ'),
(763, 'ROLE_MOD_TAB_ADMINPSMBOMODULE_UPDATE'),
(769, 'ROLE_MOD_TAB_ADMINPSMBORECOMMENDED_CREATE'),
(772, 'ROLE_MOD_TAB_ADMINPSMBORECOMMENDED_DELETE'),
(770, 'ROLE_MOD_TAB_ADMINPSMBORECOMMENDED_READ'),
(771, 'ROLE_MOD_TAB_ADMINPSMBORECOMMENDED_UPDATE'),
(773, 'ROLE_MOD_TAB_ADMINPSMBOTHEME_CREATE'),
(776, 'ROLE_MOD_TAB_ADMINPSMBOTHEME_DELETE'),
(774, 'ROLE_MOD_TAB_ADMINPSMBOTHEME_READ'),
(775, 'ROLE_MOD_TAB_ADMINPSMBOTHEME_UPDATE'),
(649, 'ROLE_MOD_TAB_ADMINPSTHEMECUSTOADVANCED_CREATE'),
(652, 'ROLE_MOD_TAB_ADMINPSTHEMECUSTOADVANCED_DELETE'),
(650, 'ROLE_MOD_TAB_ADMINPSTHEMECUSTOADVANCED_READ'),
(651, 'ROLE_MOD_TAB_ADMINPSTHEMECUSTOADVANCED_UPDATE'),
(645, 'ROLE_MOD_TAB_ADMINPSTHEMECUSTOCONFIGURATION_CREATE'),
(648, 'ROLE_MOD_TAB_ADMINPSTHEMECUSTOCONFIGURATION_DELETE'),
(646, 'ROLE_MOD_TAB_ADMINPSTHEMECUSTOCONFIGURATION_READ'),
(647, 'ROLE_MOD_TAB_ADMINPSTHEMECUSTOCONFIGURATION_UPDATE'),
(825, 'ROLE_MOD_TAB_ADMINPSXMKTGWITHGOOGLEMODULE_CREATE'),
(828, 'ROLE_MOD_TAB_ADMINPSXMKTGWITHGOOGLEMODULE_DELETE'),
(826, 'ROLE_MOD_TAB_ADMINPSXMKTGWITHGOOGLEMODULE_READ'),
(827, 'ROLE_MOD_TAB_ADMINPSXMKTGWITHGOOGLEMODULE_UPDATE'),
(317, 'ROLE_MOD_TAB_ADMINREFERRERS_CREATE'),
(320, 'ROLE_MOD_TAB_ADMINREFERRERS_DELETE'),
(318, 'ROLE_MOD_TAB_ADMINREFERRERS_READ'),
(319, 'ROLE_MOD_TAB_ADMINREFERRERS_UPDATE'),
(321, 'ROLE_MOD_TAB_ADMINREQUESTSQL_CREATE'),
(324, 'ROLE_MOD_TAB_ADMINREQUESTSQL_DELETE'),
(322, 'ROLE_MOD_TAB_ADMINREQUESTSQL_READ'),
(323, 'ROLE_MOD_TAB_ADMINREQUESTSQL_UPDATE'),
(325, 'ROLE_MOD_TAB_ADMINRETURN_CREATE'),
(328, 'ROLE_MOD_TAB_ADMINRETURN_DELETE'),
(326, 'ROLE_MOD_TAB_ADMINRETURN_READ'),
(327, 'ROLE_MOD_TAB_ADMINRETURN_UPDATE'),
(329, 'ROLE_MOD_TAB_ADMINSEARCHCONF_CREATE'),
(332, 'ROLE_MOD_TAB_ADMINSEARCHCONF_DELETE'),
(330, 'ROLE_MOD_TAB_ADMINSEARCHCONF_READ'),
(331, 'ROLE_MOD_TAB_ADMINSEARCHCONF_UPDATE'),
(333, 'ROLE_MOD_TAB_ADMINSEARCHENGINES_CREATE'),
(336, 'ROLE_MOD_TAB_ADMINSEARCHENGINES_DELETE'),
(334, 'ROLE_MOD_TAB_ADMINSEARCHENGINES_READ'),
(335, 'ROLE_MOD_TAB_ADMINSEARCHENGINES_UPDATE'),
(337, 'ROLE_MOD_TAB_ADMINSHIPPING_CREATE'),
(340, 'ROLE_MOD_TAB_ADMINSHIPPING_DELETE'),
(338, 'ROLE_MOD_TAB_ADMINSHIPPING_READ'),
(339, 'ROLE_MOD_TAB_ADMINSHIPPING_UPDATE'),
(341, 'ROLE_MOD_TAB_ADMINSHOPGROUP_CREATE'),
(344, 'ROLE_MOD_TAB_ADMINSHOPGROUP_DELETE'),
(342, 'ROLE_MOD_TAB_ADMINSHOPGROUP_READ'),
(343, 'ROLE_MOD_TAB_ADMINSHOPGROUP_UPDATE'),
(345, 'ROLE_MOD_TAB_ADMINSHOPURL_CREATE'),
(348, 'ROLE_MOD_TAB_ADMINSHOPURL_DELETE'),
(346, 'ROLE_MOD_TAB_ADMINSHOPURL_READ'),
(347, 'ROLE_MOD_TAB_ADMINSHOPURL_UPDATE'),
(349, 'ROLE_MOD_TAB_ADMINSLIP_CREATE'),
(352, 'ROLE_MOD_TAB_ADMINSLIP_DELETE'),
(350, 'ROLE_MOD_TAB_ADMINSLIP_READ'),
(351, 'ROLE_MOD_TAB_ADMINSLIP_UPDATE'),
(929, 'ROLE_MOD_TAB_ADMINSMARTSUPPAJAX_CREATE'),
(932, 'ROLE_MOD_TAB_ADMINSMARTSUPPAJAX_DELETE'),
(930, 'ROLE_MOD_TAB_ADMINSMARTSUPPAJAX_READ'),
(931, 'ROLE_MOD_TAB_ADMINSMARTSUPPAJAX_UPDATE'),
(353, 'ROLE_MOD_TAB_ADMINSPECIFICPRICERULE_CREATE'),
(356, 'ROLE_MOD_TAB_ADMINSPECIFICPRICERULE_DELETE'),
(354, 'ROLE_MOD_TAB_ADMINSPECIFICPRICERULE_READ'),
(355, 'ROLE_MOD_TAB_ADMINSPECIFICPRICERULE_UPDATE'),
(357, 'ROLE_MOD_TAB_ADMINSTATES_CREATE'),
(360, 'ROLE_MOD_TAB_ADMINSTATES_DELETE'),
(358, 'ROLE_MOD_TAB_ADMINSTATES_READ'),
(359, 'ROLE_MOD_TAB_ADMINSTATES_UPDATE'),
(361, 'ROLE_MOD_TAB_ADMINSTATS_CREATE'),
(364, 'ROLE_MOD_TAB_ADMINSTATS_DELETE'),
(362, 'ROLE_MOD_TAB_ADMINSTATS_READ'),
(363, 'ROLE_MOD_TAB_ADMINSTATS_UPDATE'),
(365, 'ROLE_MOD_TAB_ADMINSTATUSES_CREATE'),
(368, 'ROLE_MOD_TAB_ADMINSTATUSES_DELETE'),
(366, 'ROLE_MOD_TAB_ADMINSTATUSES_READ'),
(367, 'ROLE_MOD_TAB_ADMINSTATUSES_UPDATE'),
(373, 'ROLE_MOD_TAB_ADMINSTOCKCONFIGURATION_CREATE'),
(376, 'ROLE_MOD_TAB_ADMINSTOCKCONFIGURATION_DELETE'),
(374, 'ROLE_MOD_TAB_ADMINSTOCKCONFIGURATION_READ'),
(375, 'ROLE_MOD_TAB_ADMINSTOCKCONFIGURATION_UPDATE'),
(377, 'ROLE_MOD_TAB_ADMINSTOCKCOVER_CREATE'),
(380, 'ROLE_MOD_TAB_ADMINSTOCKCOVER_DELETE'),
(378, 'ROLE_MOD_TAB_ADMINSTOCKCOVER_READ'),
(379, 'ROLE_MOD_TAB_ADMINSTOCKCOVER_UPDATE'),
(381, 'ROLE_MOD_TAB_ADMINSTOCKINSTANTSTATE_CREATE'),
(384, 'ROLE_MOD_TAB_ADMINSTOCKINSTANTSTATE_DELETE'),
(382, 'ROLE_MOD_TAB_ADMINSTOCKINSTANTSTATE_READ'),
(383, 'ROLE_MOD_TAB_ADMINSTOCKINSTANTSTATE_UPDATE'),
(385, 'ROLE_MOD_TAB_ADMINSTOCKMANAGEMENT_CREATE'),
(388, 'ROLE_MOD_TAB_ADMINSTOCKMANAGEMENT_DELETE'),
(386, 'ROLE_MOD_TAB_ADMINSTOCKMANAGEMENT_READ'),
(387, 'ROLE_MOD_TAB_ADMINSTOCKMANAGEMENT_UPDATE'),
(389, 'ROLE_MOD_TAB_ADMINSTOCKMVT_CREATE'),
(392, 'ROLE_MOD_TAB_ADMINSTOCKMVT_DELETE'),
(390, 'ROLE_MOD_TAB_ADMINSTOCKMVT_READ'),
(391, 'ROLE_MOD_TAB_ADMINSTOCKMVT_UPDATE'),
(369, 'ROLE_MOD_TAB_ADMINSTOCK_CREATE'),
(372, 'ROLE_MOD_TAB_ADMINSTOCK_DELETE'),
(370, 'ROLE_MOD_TAB_ADMINSTOCK_READ'),
(371, 'ROLE_MOD_TAB_ADMINSTOCK_UPDATE'),
(393, 'ROLE_MOD_TAB_ADMINSTORES_CREATE'),
(396, 'ROLE_MOD_TAB_ADMINSTORES_DELETE'),
(394, 'ROLE_MOD_TAB_ADMINSTORES_READ'),
(395, 'ROLE_MOD_TAB_ADMINSTORES_UPDATE'),
(397, 'ROLE_MOD_TAB_ADMINSUPPLIERS_CREATE'),
(400, 'ROLE_MOD_TAB_ADMINSUPPLIERS_DELETE'),
(398, 'ROLE_MOD_TAB_ADMINSUPPLIERS_READ'),
(399, 'ROLE_MOD_TAB_ADMINSUPPLIERS_UPDATE'),
(401, 'ROLE_MOD_TAB_ADMINSUPPLYORDERS_CREATE'),
(404, 'ROLE_MOD_TAB_ADMINSUPPLYORDERS_DELETE'),
(402, 'ROLE_MOD_TAB_ADMINSUPPLYORDERS_READ'),
(403, 'ROLE_MOD_TAB_ADMINSUPPLYORDERS_UPDATE'),
(405, 'ROLE_MOD_TAB_ADMINTAGS_CREATE'),
(408, 'ROLE_MOD_TAB_ADMINTAGS_DELETE'),
(406, 'ROLE_MOD_TAB_ADMINTAGS_READ'),
(407, 'ROLE_MOD_TAB_ADMINTAGS_UPDATE'),
(409, 'ROLE_MOD_TAB_ADMINTAXES_CREATE'),
(412, 'ROLE_MOD_TAB_ADMINTAXES_DELETE'),
(410, 'ROLE_MOD_TAB_ADMINTAXES_READ'),
(411, 'ROLE_MOD_TAB_ADMINTAXES_UPDATE'),
(413, 'ROLE_MOD_TAB_ADMINTAXRULESGROUP_CREATE'),
(416, 'ROLE_MOD_TAB_ADMINTAXRULESGROUP_DELETE'),
(414, 'ROLE_MOD_TAB_ADMINTAXRULESGROUP_READ'),
(415, 'ROLE_MOD_TAB_ADMINTAXRULESGROUP_UPDATE'),
(421, 'ROLE_MOD_TAB_ADMINTHEMESCATALOG_CREATE'),
(424, 'ROLE_MOD_TAB_ADMINTHEMESCATALOG_DELETE'),
(422, 'ROLE_MOD_TAB_ADMINTHEMESCATALOG_READ'),
(423, 'ROLE_MOD_TAB_ADMINTHEMESCATALOG_UPDATE'),
(641, 'ROLE_MOD_TAB_ADMINTHEMESPARENT_CREATE'),
(644, 'ROLE_MOD_TAB_ADMINTHEMESPARENT_DELETE'),
(642, 'ROLE_MOD_TAB_ADMINTHEMESPARENT_READ'),
(643, 'ROLE_MOD_TAB_ADMINTHEMESPARENT_UPDATE'),
(417, 'ROLE_MOD_TAB_ADMINTHEMES_CREATE'),
(420, 'ROLE_MOD_TAB_ADMINTHEMES_DELETE'),
(418, 'ROLE_MOD_TAB_ADMINTHEMES_READ'),
(419, 'ROLE_MOD_TAB_ADMINTHEMES_UPDATE'),
(425, 'ROLE_MOD_TAB_ADMINTRACKING_CREATE'),
(428, 'ROLE_MOD_TAB_ADMINTRACKING_DELETE'),
(426, 'ROLE_MOD_TAB_ADMINTRACKING_READ'),
(427, 'ROLE_MOD_TAB_ADMINTRACKING_UPDATE'),
(429, 'ROLE_MOD_TAB_ADMINTRANSLATIONS_CREATE'),
(432, 'ROLE_MOD_TAB_ADMINTRANSLATIONS_DELETE'),
(430, 'ROLE_MOD_TAB_ADMINTRANSLATIONS_READ'),
(431, 'ROLE_MOD_TAB_ADMINTRANSLATIONS_UPDATE'),
(433, 'ROLE_MOD_TAB_ADMINWAREHOUSES_CREATE'),
(436, 'ROLE_MOD_TAB_ADMINWAREHOUSES_DELETE'),
(434, 'ROLE_MOD_TAB_ADMINWAREHOUSES_READ'),
(435, 'ROLE_MOD_TAB_ADMINWAREHOUSES_UPDATE'),
(437, 'ROLE_MOD_TAB_ADMINWEBSERVICE_CREATE'),
(440, 'ROLE_MOD_TAB_ADMINWEBSERVICE_DELETE'),
(438, 'ROLE_MOD_TAB_ADMINWEBSERVICE_READ'),
(439, 'ROLE_MOD_TAB_ADMINWEBSERVICE_UPDATE'),
(729, 'ROLE_MOD_TAB_ADMINWELCOME_CREATE'),
(732, 'ROLE_MOD_TAB_ADMINWELCOME_DELETE'),
(730, 'ROLE_MOD_TAB_ADMINWELCOME_READ'),
(731, 'ROLE_MOD_TAB_ADMINWELCOME_UPDATE'),
(441, 'ROLE_MOD_TAB_ADMINZONES_CREATE'),
(444, 'ROLE_MOD_TAB_ADMINZONES_DELETE'),
(442, 'ROLE_MOD_TAB_ADMINZONES_READ'),
(443, 'ROLE_MOD_TAB_ADMINZONES_UPDATE'),
(445, 'ROLE_MOD_TAB_CONFIGURE_CREATE'),
(448, 'ROLE_MOD_TAB_CONFIGURE_DELETE'),
(446, 'ROLE_MOD_TAB_CONFIGURE_READ'),
(447, 'ROLE_MOD_TAB_CONFIGURE_UPDATE'),
(449, 'ROLE_MOD_TAB_IMPROVE_CREATE'),
(452, 'ROLE_MOD_TAB_IMPROVE_DELETE'),
(450, 'ROLE_MOD_TAB_IMPROVE_READ'),
(451, 'ROLE_MOD_TAB_IMPROVE_UPDATE'),
(809, 'ROLE_MOD_TAB_MARKETING_CREATE'),
(812, 'ROLE_MOD_TAB_MARKETING_DELETE'),
(810, 'ROLE_MOD_TAB_MARKETING_READ'),
(811, 'ROLE_MOD_TAB_MARKETING_UPDATE'),
(453, 'ROLE_MOD_TAB_SELL_CREATE'),
(456, 'ROLE_MOD_TAB_SELL_DELETE'),
(454, 'ROLE_MOD_TAB_SELL_READ'),
(455, 'ROLE_MOD_TAB_SELL_UPDATE'),
(457, 'ROLE_MOD_TAB_SHOPPARAMETERS_CREATE'),
(460, 'ROLE_MOD_TAB_SHOPPARAMETERS_DELETE'),
(458, 'ROLE_MOD_TAB_SHOPPARAMETERS_READ'),
(459, 'ROLE_MOD_TAB_SHOPPARAMETERS_UPDATE'),
(489, 'ROLE_MOD_TAB_WISHLISTCONFIGURATIONADMINCONTROLLER_CREATE'),
(492, 'ROLE_MOD_TAB_WISHLISTCONFIGURATIONADMINCONTROLLER_DELETE'),
(490, 'ROLE_MOD_TAB_WISHLISTCONFIGURATIONADMINCONTROLLER_READ'),
(491, 'ROLE_MOD_TAB_WISHLISTCONFIGURATIONADMINCONTROLLER_UPDATE'),
(485, 'ROLE_MOD_TAB_WISHLISTCONFIGURATIONADMINPARENTCONTROLLER_CREATE'),
(488, 'ROLE_MOD_TAB_WISHLISTCONFIGURATIONADMINPARENTCONTROLLER_DELETE'),
(486, 'ROLE_MOD_TAB_WISHLISTCONFIGURATIONADMINPARENTCONTROLLER_READ'),
(487, 'ROLE_MOD_TAB_WISHLISTCONFIGURATIONADMINPARENTCONTROLLER_UPDATE'),
(493, 'ROLE_MOD_TAB_WISHLISTSTATISTICSADMINCONTROLLER_CREATE'),
(496, 'ROLE_MOD_TAB_WISHLISTSTATISTICSADMINCONTROLLER_DELETE'),
(494, 'ROLE_MOD_TAB_WISHLISTSTATISTICSADMINCONTROLLER_READ'),
(495, 'ROLE_MOD_TAB_WISHLISTSTATISTICSADMINCONTROLLER_UPDATE');

-- --------------------------------------------------------

--
-- Structure de la table `ps_badge`
--

DROP TABLE IF EXISTS `ps_badge`;
CREATE TABLE IF NOT EXISTS `ps_badge` (
  `id_badge` int(11) NOT NULL AUTO_INCREMENT,
  `id_ps_badge` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `id_group` int(11) NOT NULL,
  `group_position` int(11) NOT NULL,
  `scoring` int(11) NOT NULL,
  `awb` int(11) DEFAULT '0',
  `validated` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_badge`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_badge_lang`
--

DROP TABLE IF EXISTS `ps_badge_lang`;
CREATE TABLE IF NOT EXISTS `ps_badge_lang` (
  `id_badge` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_badge`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_blockwishlist_statistics`
--

DROP TABLE IF EXISTS `ps_blockwishlist_statistics`;
CREATE TABLE IF NOT EXISTS `ps_blockwishlist_statistics` (
  `id_statistics` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cart` int(10) UNSIGNED DEFAULT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  `id_shop` int(10) UNSIGNED DEFAULT '1',
  PRIMARY KEY (`id_statistics`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_carrier`
--

DROP TABLE IF EXISTS `ps_carrier`;
CREATE TABLE IF NOT EXISTS `ps_carrier` (
  `id_carrier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_reference` int(10) UNSIGNED NOT NULL,
  `id_tax_rules_group` int(10) UNSIGNED DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipping_handling` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `range_behavior` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_module` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_free` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipping_external` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `need_range` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `external_module_name` varchar(64) DEFAULT NULL,
  `shipping_method` int(2) NOT NULL DEFAULT '0',
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `max_width` int(10) DEFAULT '0',
  `max_height` int(10) DEFAULT '0',
  `max_depth` int(10) DEFAULT '0',
  `max_weight` decimal(20,6) DEFAULT '0.000000',
  `grade` int(10) DEFAULT '0',
  PRIMARY KEY (`id_carrier`),
  KEY `deleted` (`deleted`,`active`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `reference` (`id_reference`,`deleted`,`active`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_carrier`
--

INSERT INTO `ps_carrier` (`id_carrier`, `id_reference`, `id_tax_rules_group`, `name`, `url`, `active`, `deleted`, `shipping_handling`, `range_behavior`, `is_module`, `is_free`, `shipping_external`, `need_range`, `external_module_name`, `shipping_method`, `position`, `max_width`, `max_height`, `max_depth`, `max_weight`, `grade`) VALUES
(1, 1, 0, '0', '', 1, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, 0, 0, 0, '0.000000', 0),
(2, 2, 0, 'My carrier', '', 1, 0, 1, 0, 0, 0, 0, 0, '', 0, 1, 0, 0, 0, '0.000000', 0),
(3, 3, 0, 'My cheap carrier', '', 0, 0, 1, 0, 0, 0, 0, 0, '', 2, 2, 0, 0, 0, '0.000000', 0),
(4, 4, 0, 'My light carrier', '', 0, 0, 1, 0, 0, 0, 0, 0, '', 1, 3, 0, 0, 0, '0.000000', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_carrier_group`
--

DROP TABLE IF EXISTS `ps_carrier_group`;
CREATE TABLE IF NOT EXISTS `ps_carrier_group` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_carrier_group`
--

INSERT INTO `ps_carrier_group` (`id_carrier`, `id_group`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3);

-- --------------------------------------------------------

--
-- Structure de la table `ps_carrier_lang`
--

DROP TABLE IF EXISTS `ps_carrier_lang`;
CREATE TABLE IF NOT EXISTS `ps_carrier_lang` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `delay` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id_lang`,`id_shop`,`id_carrier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_carrier_lang`
--

INSERT INTO `ps_carrier_lang` (`id_carrier`, `id_shop`, `id_lang`, `delay`) VALUES
(1, 1, 1, 'Pick up in-store'),
(2, 1, 1, 'Delivery next day!'),
(3, 1, 1, 'Buy more to pay less!'),
(4, 1, 1, 'The lighter the cheaper!'),
(1, 1, 2, 'Retrait en magasin'),
(2, 1, 2, 'Livraison le lendemain !'),
(3, 1, 2, 'Achetez plus vous paierez moins!'),
(4, 1, 2, 'Panier léger, prix allégé!'),
(1, 1, 3, 'Abholung im Geschäft'),
(2, 1, 3, 'Lieferung am nächsten Tag!'),
(3, 1, 3, 'Buy more to pay less!'),
(4, 1, 3, 'The lighter the cheaper!'),
(1, 1, 4, 'Afhalen in de winkel'),
(2, 1, 4, 'De volgende dag in huis!'),
(3, 1, 4, 'Buy more to pay less!'),
(4, 1, 4, 'The lighter the cheaper!'),
(1, 1, 5, 'Ritiro in negozio'),
(2, 1, 5, 'Consegna il giorno successivo!'),
(3, 1, 5, 'Buy more to pay less!'),
(4, 1, 5, 'The lighter the cheaper!'),
(1, 1, 6, 'Recoger en tienda'),
(2, 1, 6, '¡Envío en 24h!'),
(3, 1, 6, 'Buy more to pay less!'),
(4, 1, 6, 'The lighter the cheaper!');

-- --------------------------------------------------------

--
-- Structure de la table `ps_carrier_shop`
--

DROP TABLE IF EXISTS `ps_carrier_shop`;
CREATE TABLE IF NOT EXISTS `ps_carrier_shop` (
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_carrier_shop`
--

INSERT INTO `ps_carrier_shop` (`id_carrier`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_carrier_tax_rules_group_shop`
--

DROP TABLE IF EXISTS `ps_carrier_tax_rules_group_shop`;
CREATE TABLE IF NOT EXISTS `ps_carrier_tax_rules_group_shop` (
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_tax_rules_group`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_carrier_tax_rules_group_shop`
--

INSERT INTO `ps_carrier_tax_rules_group_shop` (`id_carrier`, `id_tax_rules_group`, `id_shop`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 1),
(4, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_carrier_zone`
--

DROP TABLE IF EXISTS `ps_carrier_zone`;
CREATE TABLE IF NOT EXISTS `ps_carrier_zone` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_zone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_carrier_zone`
--

INSERT INTO `ps_carrier_zone` (`id_carrier`, `id_zone`) VALUES
(1, 1),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart`
--

DROP TABLE IF EXISTS `ps_cart`;
CREATE TABLE IF NOT EXISTS `ps_cart` (
  `id_cart` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delivery_option` text NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL,
  `id_address_invoice` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_guest` int(10) UNSIGNED NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `recyclable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `gift` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_message` text,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  `allow_seperated_package` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `checkout_session_data` mediumtext,
  PRIMARY KEY (`id_cart`),
  KEY `cart_customer` (`id_customer`),
  KEY `id_address_delivery` (`id_address_delivery`),
  KEY `id_address_invoice` (`id_address_invoice`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_lang` (`id_lang`),
  KEY `id_currency` (`id_currency`),
  KEY `id_guest` (`id_guest`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_shop_2` (`id_shop`,`date_upd`),
  KEY `id_shop` (`id_shop`,`date_add`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_cart`
--

INSERT INTO `ps_cart` (`id_cart`, `id_shop_group`, `id_shop`, `id_carrier`, `delivery_option`, `id_lang`, `id_address_delivery`, `id_address_invoice`, `id_currency`, `id_customer`, `id_guest`, `secure_key`, `recyclable`, `gift`, `gift_message`, `mobile_theme`, `allow_seperated_package`, `date_add`, `date_upd`, `checkout_session_data`) VALUES
(1, 1, 1, 2, '{\"3\":\"2,\"}', 1, 5, 5, 1, 2, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2022-11-21 11:07:25', '2022-11-21 11:07:25', NULL),
(2, 1, 1, 2, '{\"3\":\"2,\"}', 1, 5, 5, 1, 2, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2022-11-21 11:07:25', '2022-11-21 11:07:25', NULL),
(3, 1, 1, 2, '{\"3\":\"2,\"}', 1, 5, 5, 1, 2, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2022-11-21 11:07:25', '2022-11-21 11:07:25', NULL),
(4, 1, 1, 2, '{\"3\":\"2,\"}', 1, 5, 5, 1, 2, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2022-11-21 11:07:25', '2022-11-21 11:07:25', NULL),
(5, 1, 1, 2, '{\"3\":\"2,\"}', 1, 5, 5, 1, 2, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2022-11-21 11:07:25', '2022-11-21 11:07:25', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_cart_rule`
--

DROP TABLE IF EXISTS `ps_cart_cart_rule`;
CREATE TABLE IF NOT EXISTS `ps_cart_cart_rule` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart`,`id_cart_rule`),
  KEY `id_cart_rule` (`id_cart_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_product`
--

DROP TABLE IF EXISTS `ps_cart_product`;
CREATE TABLE IF NOT EXISTS `ps_cart_product` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_customization` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_cart`,`id_product`,`id_product_attribute`,`id_customization`,`id_address_delivery`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_cart_order` (`id_cart`,`date_add`,`id_product`,`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_rule`
--

DROP TABLE IF EXISTS `ps_cart_rule`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL,
  `description` text,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity_per_user` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `priority` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `partial_use` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `code` varchar(254) NOT NULL,
  `minimum_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `minimum_amount_tax` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_amount_currency` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `minimum_amount_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `country_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `carrier_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `group_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `cart_rule_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `product_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shop_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `reduction_percent` decimal(5,2) NOT NULL DEFAULT '0.00',
  `reduction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_tax` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `reduction_currency` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `reduction_product` int(10) NOT NULL DEFAULT '0',
  `reduction_exclude_special` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_product` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gift_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `highlight` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_cart_rule`),
  KEY `id_customer` (`id_customer`,`active`,`date_to`),
  KEY `group_restriction` (`group_restriction`,`active`,`date_to`),
  KEY `id_customer_2` (`id_customer`,`active`,`highlight`,`date_to`),
  KEY `group_restriction_2` (`group_restriction`,`active`,`highlight`,`date_to`),
  KEY `date_from` (`date_from`),
  KEY `date_to` (`date_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_rule_carrier`
--

DROP TABLE IF EXISTS `ps_cart_rule_carrier`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_carrier` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_carrier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_rule_combination`
--

DROP TABLE IF EXISTS `ps_cart_rule_combination`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_combination` (
  `id_cart_rule_1` int(10) UNSIGNED NOT NULL,
  `id_cart_rule_2` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart_rule_1`,`id_cart_rule_2`),
  KEY `id_cart_rule_1` (`id_cart_rule_1`),
  KEY `id_cart_rule_2` (`id_cart_rule_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_rule_country`
--

DROP TABLE IF EXISTS `ps_cart_rule_country`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_country` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_rule_group`
--

DROP TABLE IF EXISTS `ps_cart_rule_group`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_group` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_rule_lang`
--

DROP TABLE IF EXISTS `ps_cart_rule_lang`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_lang` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(254) NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_rule_product_rule`
--

DROP TABLE IF EXISTS `ps_cart_rule_product_rule`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_product_rule` (
  `id_product_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product_rule_group` int(10) UNSIGNED NOT NULL,
  `type` enum('products','categories','attributes','manufacturers','suppliers') NOT NULL,
  PRIMARY KEY (`id_product_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_rule_product_rule_group`
--

DROP TABLE IF EXISTS `ps_cart_rule_product_rule_group`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_product_rule_group` (
  `id_product_rule_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_product_rule_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_rule_product_rule_value`
--

DROP TABLE IF EXISTS `ps_cart_rule_product_rule_value`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_product_rule_value` (
  `id_product_rule` int(10) UNSIGNED NOT NULL,
  `id_item` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product_rule`,`id_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cart_rule_shop`
--

DROP TABLE IF EXISTS `ps_cart_rule_shop`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_shop` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_category`
--

DROP TABLE IF EXISTS `ps_category`;
CREATE TABLE IF NOT EXISTS `ps_category` (
  `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) UNSIGNED NOT NULL,
  `id_shop_default` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `level_depth` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `nleft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `nright` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_root_category` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`),
  KEY `category_parent` (`id_parent`),
  KEY `nleftrightactive` (`nleft`,`nright`,`active`),
  KEY `level_depth` (`level_depth`),
  KEY `nright` (`nright`),
  KEY `activenleft` (`active`,`nleft`),
  KEY `activenright` (`active`,`nright`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_category`
--

INSERT INTO `ps_category` (`id_category`, `id_parent`, `id_shop_default`, `level_depth`, `nleft`, `nright`, `active`, `date_add`, `date_upd`, `position`, `is_root_category`) VALUES
(1, 0, 1, 0, 1, 22, 1, '2022-11-21 11:03:35', '2022-11-21 11:03:35', 0, 0),
(2, 1, 1, 1, 2, 21, 1, '2022-11-21 11:03:35', '2022-11-21 11:03:35', 0, 1),
(10, 11, 1, 3, 4, 5, 1, '2022-12-05 09:34:38', '2022-12-05 10:00:23', 0, 0),
(11, 2, 1, 2, 3, 10, 1, '2022-12-05 10:00:00', '2022-12-05 10:00:00', 1, 0),
(12, 11, 1, 3, 6, 7, 1, '2022-12-05 10:10:10', '2022-12-05 10:10:10', 1, 0),
(13, 11, 1, 3, 8, 9, 1, '2022-12-05 12:12:43', '2022-12-05 12:12:43', 2, 0),
(14, 2, 1, 2, 11, 20, 1, '2022-12-05 12:35:55', '2022-12-05 12:35:55', 2, 0),
(15, 14, 1, 3, 12, 19, 1, '2022-12-05 12:36:17', '2022-12-05 12:36:17', 0, 0),
(16, 15, 1, 4, 13, 18, 1, '2022-12-05 12:37:10', '2022-12-05 12:37:10', 0, 0),
(17, 16, 1, 5, 14, 17, 1, '2022-12-05 12:37:39', '2022-12-05 12:37:39', 0, 0),
(18, 17, 1, 6, 15, 16, 1, '2022-12-05 12:38:00', '2022-12-05 12:38:00', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_category_group`
--

DROP TABLE IF EXISTS `ps_category_group`;
CREATE TABLE IF NOT EXISTS `ps_category_group` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_category`,`id_group`),
  KEY `id_category` (`id_category`),
  KEY `id_group` (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_category_group`
--

INSERT INTO `ps_category_group` (`id_category`, `id_group`) VALUES
(2, 0),
(2, 1),
(2, 2),
(2, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(11, 3),
(12, 1),
(12, 2),
(12, 3),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(14, 2),
(14, 3),
(15, 1),
(15, 2),
(15, 3),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3);

-- --------------------------------------------------------

--
-- Structure de la table `ps_category_lang`
--

DROP TABLE IF EXISTS `ps_category_lang`;
CREATE TABLE IF NOT EXISTS `ps_category_lang` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id_category`,`id_shop`,`id_lang`),
  KEY `category_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_category_lang`
--

INSERT INTO `ps_category_lang` (`id_category`, `id_shop`, `id_lang`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 1, 'Root', '', 'root', '', '', ''),
(1, 1, 2, 'Racine', '', 'racine', '', '', ''),
(1, 1, 3, 'Stammverzeichnis', '', 'stammverzeichnis', '', '', ''),
(1, 1, 4, 'Root', '', 'root', '', '', ''),
(1, 1, 5, 'Root', '', 'root', '', '', ''),
(1, 1, 6, 'Raíz', '', 'raiz', '', '', ''),
(2, 1, 1, 'Home', '', 'home', '', '', ''),
(2, 1, 2, 'Accueil', '', 'accueil', '', '', ''),
(2, 1, 3, 'Startseite', '', 'startseite', '', '', ''),
(2, 1, 4, 'Home', '', 'home', '', '', ''),
(2, 1, 5, 'Home', '', 'home', '', '', ''),
(2, 1, 6, 'Inicio', '', 'inicio', '', '', ''),
(10, 1, 1, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(10, 1, 2, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(10, 1, 3, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(10, 1, 4, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(10, 1, 5, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(10, 1, 6, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(11, 1, 1, 'Produits', '', 'produits', '', '', ''),
(11, 1, 2, 'Produits', '', 'produits', '', '', ''),
(11, 1, 3, 'Produits', '', 'produits', '', '', ''),
(11, 1, 4, 'Produits', '', 'produits', '', '', ''),
(11, 1, 5, 'Produits', '', 'produits', '', '', ''),
(11, 1, 6, 'Produits', '', 'produits', '', '', ''),
(12, 1, 1, 'Jeux de société', '', 'jeux-de-societe', '', '', ''),
(12, 1, 2, 'Jeux de société', '', 'jeux-de-societe', '', '', ''),
(12, 1, 3, 'Jeux de société', '', 'jeux-de-societe', '', '', ''),
(12, 1, 4, 'Jeux de société', '', 'jeux-de-societe', '', '', ''),
(12, 1, 5, 'Jeux de société', '', 'jeux-de-societe', '', '', ''),
(12, 1, 6, 'Jeux de société', '', 'jeux-de-societe', '', '', ''),
(13, 1, 1, 'Jeux de cartes', '', 'jeux-de-cartes', '', '', ''),
(13, 1, 2, 'Jeux de cartes', '', 'jeux-de-cartes', '', '', ''),
(13, 1, 3, 'Jeux de cartes', '', 'jeux-de-cartes', '', '', ''),
(13, 1, 4, 'Jeux de cartes', '', 'jeux-de-cartes', '', '', ''),
(13, 1, 5, 'Jeux de cartes', '', 'jeux-de-cartes', '', '', ''),
(13, 1, 6, 'Jeux de cartes', '', 'jeux-de-cartes', '', '', ''),
(14, 1, 1, 'Catégorie 1', '', 'categorie-1', '', '', ''),
(14, 1, 2, 'Catégorie 1', '', 'categorie-1', '', '', ''),
(14, 1, 3, 'Catégorie 1', '', 'categorie-1', '', '', ''),
(14, 1, 4, 'Catégorie 1', '', 'categorie-1', '', '', ''),
(14, 1, 5, 'Catégorie 1', '', 'categorie-1', '', '', ''),
(14, 1, 6, 'Catégorie 1', '', 'categorie-1', '', '', ''),
(15, 1, 1, 'Catégorie 2', '', 'categorie-2', '', '', ''),
(15, 1, 2, 'Catégorie 2', '', 'categorie-2', '', '', ''),
(15, 1, 3, 'Catégorie 2', '', 'categorie-2', '', '', ''),
(15, 1, 4, 'Catégorie 2', '', 'categorie-2', '', '', ''),
(15, 1, 5, 'Catégorie 2', '', 'categorie-2', '', '', ''),
(15, 1, 6, 'Catégorie 2', '', 'categorie-2', '', '', ''),
(16, 1, 1, 'Catégorie 3', '', 'categorie-3', '', '', ''),
(16, 1, 2, 'Catégorie 3', '', 'categorie-3', '', '', ''),
(16, 1, 3, 'Catégorie 3', '', 'categorie-3', '', '', ''),
(16, 1, 4, 'Catégorie 3', '', 'categorie-3', '', '', ''),
(16, 1, 5, 'Catégorie 3', '', 'categorie-3', '', '', ''),
(16, 1, 6, 'Catégorie 3', '', 'categorie-3', '', '', ''),
(17, 1, 1, 'Catégorie 4', '', 'categorie-4', '', '', ''),
(17, 1, 2, 'Catégorie 4', '', 'categorie-4', '', '', ''),
(17, 1, 3, 'Catégorie 4', '', 'categorie-4', '', '', ''),
(17, 1, 4, 'Catégorie 4', '', 'categorie-4', '', '', ''),
(17, 1, 5, 'Catégorie 4', '', 'categorie-4', '', '', ''),
(17, 1, 6, 'Catégorie 4', '', 'categorie-4', '', '', ''),
(18, 1, 1, 'Catégorie 5', '', 'categorie-5', '', '', ''),
(18, 1, 2, 'Catégorie 5', '', 'categorie-5', '', '', ''),
(18, 1, 3, 'Catégorie 5', '', 'categorie-5', '', '', ''),
(18, 1, 4, 'Catégorie 5', '', 'categorie-5', '', '', ''),
(18, 1, 5, 'Catégorie 5', '', 'categorie-5', '', '', ''),
(18, 1, 6, 'Catégorie 5', '', 'categorie-5', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_category_product`
--

DROP TABLE IF EXISTS `ps_category_product`;
CREATE TABLE IF NOT EXISTS `ps_category_product` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`,`id_product`),
  KEY `id_product` (`id_product`),
  KEY `id_category` (`id_category`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_category_product`
--

INSERT INTO `ps_category_product` (`id_category`, `id_product`, `position`) VALUES
(2, 20, 1),
(2, 24, 2),
(2, 23, 3),
(2, 25, 4),
(10, 20, 1),
(11, 23, 1),
(12, 23, 1),
(13, 24, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_category_shop`
--

DROP TABLE IF EXISTS `ps_category_shop`;
CREATE TABLE IF NOT EXISTS `ps_category_shop` (
  `id_category` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_category_shop`
--

INSERT INTO `ps_category_shop` (`id_category`, `id_shop`, `position`) VALUES
(1, 1, 0),
(2, 1, 0),
(10, 1, 0),
(11, 1, 1),
(12, 1, 1),
(13, 1, 2),
(14, 1, 2),
(15, 1, 0),
(16, 1, 0),
(17, 1, 0),
(18, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_cms`
--

DROP TABLE IF EXISTS `ps_cms`;
CREATE TABLE IF NOT EXISTS `ps_cms` (
  `id_cms` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `indexation` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_cms`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_cms`
--

INSERT INTO `ps_cms` (`id_cms`, `id_cms_category`, `position`, `active`, `indexation`) VALUES
(1, 1, 0, 1, 0),
(2, 1, 1, 1, 0),
(3, 1, 2, 1, 0),
(4, 1, 3, 1, 0),
(5, 1, 4, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_cms_category`
--

DROP TABLE IF EXISTS `ps_cms_category`;
CREATE TABLE IF NOT EXISTS `ps_cms_category` (
  `id_cms_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) UNSIGNED NOT NULL,
  `level_depth` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_cms_category`),
  KEY `category_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_cms_category`
--

INSERT INTO `ps_cms_category` (`id_cms_category`, `id_parent`, `level_depth`, `active`, `date_add`, `date_upd`, `position`) VALUES
(1, 0, 1, 1, '2022-11-21 11:03:35', '2022-11-21 11:03:35', 0),
(3, 1, 2, 0, '2022-12-05 10:46:43', '2022-12-05 14:12:45', 0),
(4, 3, 3, 0, '2022-12-05 10:51:31', '2022-12-05 14:12:57', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_cms_category_lang`
--

DROP TABLE IF EXISTS `ps_cms_category_lang`;
CREATE TABLE IF NOT EXISTS `ps_cms_category_lang` (
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(128) NOT NULL,
  `description` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id_cms_category`,`id_shop`,`id_lang`),
  KEY `category_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_cms_category_lang`
--

INSERT INTO `ps_cms_category_lang` (`id_cms_category`, `id_lang`, `id_shop`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 1, 'Home', '', 'home', '', '', ''),
(1, 2, 1, 'Accueil', '', 'accueil', '', '', ''),
(1, 3, 1, 'Startseite', '', 'startseite', '', '', ''),
(1, 4, 1, 'Home', '', 'home', '', '', ''),
(1, 5, 1, 'Home', '', 'home', '', '', ''),
(1, 6, 1, 'Inicio', '', 'inicio', '', '', ''),
(3, 1, 1, 'Produits', '', 'produits', '', '', ''),
(3, 2, 1, 'Produits', '', 'produits', '', '', ''),
(3, 3, 1, 'Produits', '', 'produits', '', '', ''),
(3, 4, 1, 'Produits', '', 'produits', '', '', ''),
(3, 5, 1, 'Produits', '', 'produits', '', '', ''),
(3, 6, 1, 'Produits', '', 'produits', '', '', ''),
(4, 1, 1, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(4, 2, 1, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(4, 3, 1, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(4, 4, 1, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(4, 5, 1, 'Jeux de rôle', '', 'jeux-de-role', '', '', ''),
(4, 6, 1, 'Jeux de rôle', '', 'jeux-de-role', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_cms_category_shop`
--

DROP TABLE IF EXISTS `ps_cms_category_shop`;
CREATE TABLE IF NOT EXISTS `ps_cms_category_shop` (
  `id_cms_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cms_category`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_cms_category_shop`
--

INSERT INTO `ps_cms_category_shop` (`id_cms_category`, `id_shop`) VALUES
(1, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_cms_lang`
--

DROP TABLE IF EXISTS `ps_cms_lang`;
CREATE TABLE IF NOT EXISTS `ps_cms_lang` (
  `id_cms` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `meta_title` varchar(255) NOT NULL,
  `head_seo_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(512) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `content` longtext,
  `link_rewrite` varchar(128) NOT NULL,
  PRIMARY KEY (`id_cms`,`id_shop`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_cms_lang`
--

INSERT INTO `ps_cms_lang` (`id_cms`, `id_lang`, `id_shop`, `meta_title`, `head_seo_title`, `meta_description`, `meta_keywords`, `content`, `link_rewrite`) VALUES
(1, 1, 1, 'Delivery', '', 'Our terms and conditions of delivery', 'conditions, delivery, delay, shipment, pack', '<h2>Shipments and returns</h2><h3>Your pack shipment</h3><p>Packages are generally dispatched within 2 days after receipt of payment and are shipped via UPS with tracking and drop-off without signature. If you prefer delivery by UPS Extra with required signature, an additional cost will be applied, so please contact us before choosing this method. Whichever shipment choice you make, we will provide you with a link to track your package online.</p><p>Shipping fees include handling and packing fees as well as postage costs. Handling fees are fixed, whereas transport fees vary according to total weight of the shipment. We advise you to group your items in one order. We cannot group two distinct orders placed separately, and shipping fees will apply to each of them. Your package will be dispatched at your own risk, but special care is taken to protect fragile objects.<br /><br />Boxes are amply sized and your items are well-protected.</p>', 'delivery'),
(1, 2, 1, 'Livraison', '', 'Nos conditions de livraison', 'conditions, livraison, délais, expédition, colis', '<h2>Expéditions et retours</h2><h3>Expédition de votre colis</h3><p>Les colis sont généralement expédiés dans un délai de 2 jours après réception du paiement. Ils sont expédiés via UPS avec un numéro de suivi et remis sans signature. Les colis peuvent également être expédiés via UPS Extra et remis contre signature. Veuillez nous contacter avant de choisir ce mode de livraison, car il induit des frais supplémentaires. Quel que soit le mode de livraison choisi, nous vous envoyons un lien pour suivre votre colis en ligne.</p><p>Les frais d\'expédition incluent les frais de préparation et d\'emballage ainsi que les frais de port. Les frais de préparation sont fixes, tandis que les frais de transport varient selon le poids total du colis. Nous vous recommandons de regrouper tous vos articles dans une seule commande. Nous ne pouvons regrouper deux commandes placées séparément et des frais d\'expédition s\'appliquent à chacune d\'entre elles. Votre colis est expédié à vos propres risques, mais une attention particulière est portée aux objets fragiles.<br /><br />Les dimensions des boîtes sont appropriées et vos articles sont correctement protégés.</p>', 'livraison'),
(1, 3, 1, 'Lieferung', '', 'Unsere Lieferbedingungen', 'Bedingungen, Lieferung, Frist, Versand, Verpackung', '<h2>Versand und Rücknahme</h2><h3>Ihre Versandverpackung</h3><p>Pakete werden normalerweise 2 Tage nach Zahlungseingang mit UPS mit Bestellverfolgemöglichkeit und Ablieferung ohne Unterschrift geliefert. Wenn Sie lieber eine UPS-Sendung per Einschreiben erhalten möchten, entstehen zusätzliche Kosten. Bitte kontaktieren Sie uns, bevor Sie dieses Liefermethode wählen. Wir senden Ihnen einen Link für die Bestellverfolgung unabhängig davon, welche Liefermethode Sie wählen.</p><p>Die Versandkosten beinhalten Lade- und Verpackungsgebühren sowie die Portokosten. Die Verladegebühren stehen fest, wobei Transportkosten schwanken, je nach Gesamtgewicht des Pakets. Wir raten Ihnen, mehrere Artikel in einer Bestellung zusammenzufassen. Wir können zwei verschiedene Bestellungen nicht zusammenlegen, und die Versandkosten werden separat für jede Bestellung gerechnet. Ihr Paket wird auf Ihr Risiko versandt, aber zerbrechliche Ware wird besonders sorgsam behandelt.<br /><br />Die Versandschachteln sind weit geschnitten und ihre Ware wird gut geschützt verpackt.</p>', 'Lieferung'),
(1, 4, 1, 'Levering', '', 'Onze leveringsvoorwaarden', 'voorwaarden, levering, vertraging, verzending, pakket', '<h2>Zendingen en retourzendingen</h2><h3>Verzending van uw pakket</h3><p>Pakketten worden over het algemeen binnen 2 dagen na ontvangst van uw betaling verzonden via UPS met tracking en aflevering zonder handtekening. Als u de voorkeur geeft aan verzending via UPS Extra met vereiste handtekening, zullen er extra kosten in rekening worden gebracht. Neem contact met ons op voordat u deze bezorgwijze kiest. Welke verzending u ook kiest, u krijgt van ons een trackingnummer waarmee u uw pakket online kunt volgen.</p><p>Verzendkosten zijn inclusief behandeling, verpakking en frankering. Behandelingskosten zijn vaste bedragen, terwijl vervoerskosten afhankelijk zijn van het totaalgewicht. We raden u aan uw artikelen onder te brengen in één bestelling. We kunnen twee apart geplaatste bestellingen niet samenvoegen, voor elke bestelling zullen dus verzendkosten in rekening worden gebracht. Uw pakket wordt op eigen risico verzonden, maar er wordt bijzondere zorg besteed aan breekbare voorwerpen.<br /><br />Onze dozen zijn groot genoeg om uw artikelen goed beschermd te kunnen verzenden.</p>', 'levering'),
(1, 5, 1, 'Delivery', '', 'Our terms and conditions of delivery', 'conditions, delivery, delay, shipment, pack', '<h2>Shipments and returns</h2><h3>Your pack shipment</h3><p>Packages are generally dispatched within 2 days after receipt of payment and are shipped via UPS with tracking and drop-off without signature. If you prefer delivery by UPS Extra with required signature, an additional cost will be applied, so please contact us before choosing this method. Whichever shipment choice you make, we will provide you with a link to track your package online.</p><p>Shipping fees include handling and packing fees as well as postage costs. Handling fees are fixed, whereas transport fees vary according to total weight of the shipment. We advise you to group your items in one order. We cannot group two distinct orders placed separately, and shipping fees will apply to each of them. Your package will be dispatched at your own risk, but special care is taken to protect fragile objects.<br /><br />Boxes are amply sized and your items are well-protected.</p>', 'delivery'),
(1, 6, 1, 'Delivery', '', 'Our terms and conditions of delivery', 'conditions, delivery, delay, shipment, pack', '<h2>Shipments and returns</h2><h3>Your pack shipment</h3><p>Packages are generally dispatched within 2 days after receipt of payment and are shipped via UPS with tracking and drop-off without signature. If you prefer delivery by UPS Extra with required signature, an additional cost will be applied, so please contact us before choosing this method. Whichever shipment choice you make, we will provide you with a link to track your package online.</p><p>Shipping fees include handling and packing fees as well as postage costs. Handling fees are fixed, whereas transport fees vary according to total weight of the shipment. We advise you to group your items in one order. We cannot group two distinct orders placed separately, and shipping fees will apply to each of them. Your package will be dispatched at your own risk, but special care is taken to protect fragile objects.<br /><br />Boxes are amply sized and your items are well-protected.</p>', 'delivery'),
(2, 1, 1, 'Legal Notice', '', 'Legal notice', 'notice, legal, credits', '<h2>Legal</h2><h3>Credits</h3><p>Concept and production:</p><p>This Online store was created using <a href=\"http://www.prestashop.com\">Prestashop Shopping Cart Software</a>,check out PrestaShop\'s <a href=\"http://www.prestashop.com/blog/en/\">ecommerce blog</a> for news and advices about selling online and running your ecommerce website.</p>', 'legal-notice'),
(2, 2, 1, 'Mentions légales', '', 'Mentions légales', 'mentions, légales, crédits', '<h2>Mentions légales</h2><h3>Crédits</h3><p>Conception et production :</p><p>cette boutique en ligne a été créée à l\'aide du <a href=\"http://www.prestashop.com\">logiciel PrestaShop. </a>Rendez-vous sur le <a href=\"http://www.prestashop.com/blog/en/\">blog e-commerce de PrestaShop</a> pour vous tenir au courant des dernières actualités et obtenir des conseils sur la vente en ligne et la gestion d\'un site d\'e-commerce.</p>', 'mentions-legales'),
(2, 3, 1, 'Rechtliche Hinweise', '', 'Rechtliche Hinweise', 'Hinweise, rechtlich, Gutscheine', '<h2>Legal</h2><h3>Credits</h3><p>Konzept und Gestaltung:</p><p>Diese Webseite wurde hergestellt unter Verwendung von <a href=\"http://www.prestashop.com\">PrestaShop</a>&trade; open-source software.</p>', 'rechtliche-hinweise'),
(2, 4, 1, 'Wettelijke Mededeling', '', 'Wettelijke mededeling', 'mededeling, wettelijk, kredieten', '<h2>Wettelijk</h2><h3>Kredieten</h3><p>Concept en productie:</p><p>Deze Webwinkel is opgezet met <a href=\"http://www.prestashop.com\">Prestashop Webwinkel Software</a>, op Prestashop\'s <a href=\"http://www.prestashop.com/blog/en/\">e-commerce blog</a> vindt u nieuws en advies over online verkopen en het runnen van uw e-commerce website.</p>', 'wettelijke-mededeling'),
(2, 5, 1, 'Legal Notice', '', 'Legal notice', 'notice, legal, credits', '<h2>Legal</h2><h3>Credits</h3><p>Concept and production:</p><p>This Online store was created using <a href=\"http://www.prestashop.com\">Prestashop Shopping Cart Software</a>,check out PrestaShop\'s <a href=\"http://www.prestashop.com/blog/en/\">ecommerce blog</a> for news and advices about selling online and running your ecommerce website.</p>', 'legal-notice'),
(2, 6, 1, 'Legal Notice', '', 'Legal notice', 'notice, legal, credits', '<h2>Legal</h2><h3>Credits</h3><p>Concept and production:</p><p>This Online store was created using <a href=\"http://www.prestashop.com\">Prestashop Shopping Cart Software</a>,check out PrestaShop\'s <a href=\"http://www.prestashop.com/blog/en/\">ecommerce blog</a> for news and advices about selling online and running your ecommerce website.</p>', 'legal-notice'),
(3, 1, 1, 'Terms and conditions of use', '', 'Our terms and conditions of use', 'conditions, terms, use, sell', '<h1 class=\"page-heading\">Terms and conditions of use</h1>\n<h3 class=\"page-subheading\">Rule 1</h3>\n<p class=\"bottom-indent\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<h3 class=\"page-subheading\">Rule 2</h3>\n<p class=\"bottom-indent\">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>\n<h3 class=\"page-subheading\">Rule 3</h3>\n<p class=\"bottom-indent\">Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>', 'terms-and-conditions-of-use'),
(3, 2, 1, 'Conditions d\'utilisation', '', 'Nos conditions d\'utilisation', 'conditions, utilisation, vente', '<h1 class=\"page-heading\">Conditions d\'utilisation</h1>\n<h3 class=\"page-subheading\">Règle n° 1</h3>\n<p class=\"bottom-indent\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<h3 class=\"page-subheading\">Règle n° 2</h3>\n<p class=\"bottom-indent\">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>\n<h3 class=\"page-subheading\">Règle n° 3</h3>\n<p class=\"bottom-indent\">Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>', 'conditions-utilisation'),
(3, 3, 1, 'Allgemeine Geschäftsbedingungen', '', 'Unsere AGB', 'AGB, Bedingungen, Nutzung, Verkauf', '<h2>Ihre AGB</h2><h3>Rule 1</h3><p>Here is the rule 1 content</p>\r\n<h3>Rule 2</h3><p>Here is the rule 2 content</p>\r\n<h3>Rule 3</h3><p>Here is the rule 3 content</p>', 'allgemeine-geschaeftsbedingungen'),
(3, 4, 1, 'Gebruiksvoorwaarden', '', 'Onze gebruiksvoorwaarden', 'voorwaarden, gebruik, verkopen', '<h1 class=\"page-heading\">Gebruiksvoorwaarden</h1><h3 class=\"page-subheading\">Regel 1</h3><p class=\"bottom-indent\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><h3 class=\"page-subheading\">Rule 2</h3><p class=\"bottom-indent\">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p><h3 class=\"page-subheading\">Rule 3</h3><p class=\"bottom-indent\">Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>', 'gebruiks-voorwaarden'),
(3, 5, 1, 'Terms and conditions of use', '', 'Our terms and conditions of use', 'conditions, terms, use, sell', '<h1 class=\"page-heading\">Terms and conditions of use</h1>\n<h3 class=\"page-subheading\">Rule 1</h3>\n<p class=\"bottom-indent\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<h3 class=\"page-subheading\">Rule 2</h3>\n<p class=\"bottom-indent\">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>\n<h3 class=\"page-subheading\">Rule 3</h3>\n<p class=\"bottom-indent\">Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>', 'terms-and-conditions-of-use'),
(3, 6, 1, 'Terms and conditions of use', '', 'Our terms and conditions of use', 'conditions, terms, use, sell', '<h1 class=\"page-heading\">Terms and conditions of use</h1>\n<h3 class=\"page-subheading\">Rule 1</h3>\n<p class=\"bottom-indent\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<h3 class=\"page-subheading\">Rule 2</h3>\n<p class=\"bottom-indent\">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>\n<h3 class=\"page-subheading\">Rule 3</h3>\n<p class=\"bottom-indent\">Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>', 'terms-and-conditions-of-use'),
(4, 1, 1, 'About us', '', 'Learn more about us', 'about us, informations', '<h1 class=\"page-heading bottom-indent\">About us</h1>\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-block\">\n<h3 class=\"page-subheading\">Our company</h3>\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididun.</strong></p>\n<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet conse ctetur adipisicing elit.</p>\n<ul class=\"list-1\">\n<li><em class=\"icon-ok\"></em>Top quality products</li>\n<li><em class=\"icon-ok\"></em>Best customer service</li>\n<li><em class=\"icon-ok\"></em>30-days money back guarantee</li>\n</ul>\n</div>\n</div>\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-box\">\n<h3 class=\"page-subheading\">Our team</h3>\n<p><strong class=\"dark\">Lorem set sint occaecat cupidatat non </strong></p>\n<p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>\n</div>\n</div>\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-box\">\n<h3 class=\"page-subheading\">Testimonials</h3>\n<div class=\"testimonials\">\n<div class=\"inner\"><span class=\"before\">“</span>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.<span class=\"after\">”</span></div>\n</div>\n<p><strong class=\"dark\">Lorem ipsum dolor sit</strong></p>\n<div class=\"testimonials\">\n<div class=\"inner\"><span class=\"before\">“</span>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod.<span class=\"after\">”</span></div>\n</div>\n<p><strong class=\"dark\">Ipsum dolor sit</strong></p>\n</div>\n</div>\n</div>', 'about-us'),
(4, 2, 1, 'A propos', '', 'En savoir plus sur notre entreprise', 'à propos, informations', '<h1 class=\"page-heading bottom-indent\">A propos</h1>\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-block\">\n<h3 class=\"page-subheading\">Notre entreprise</h3>\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididun.</strong></p>\n<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet conse ctetur adipisicing elit.</p>\n<ul class=\"list-1\">\n<li><em class=\"icon-ok\"></em>Produits haute qualité</li>\n<li><em class=\"icon-ok\"></em>Service client inégalé</li>\n<li><em class=\"icon-ok\"></em>Remboursement garanti pendant 30 jours</li>\n</ul>\n</div>\n</div>\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-box\">\n<h3 class=\"page-subheading\">Notre équipe</h3>\n<p><strong class=\"dark\">Lorem set sint occaecat cupidatat non </strong></p>\n<p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>\n</div>\n</div>\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-box\">\n<h3 class=\"page-subheading\">Témoignages</h3>\n<div class=\"testimonials\">\n<div class=\"inner\"><span class=\"before\">“</span>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.<span class=\"after\">”</span></div>\n</div>\n<p><strong class=\"dark\">Lorem ipsum dolor sit</strong></p>\n<div class=\"testimonials\">\n<div class=\"inner\"><span class=\"before\">“</span>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod.<span class=\"after\">”</span></div>\n</div>\n<p><strong class=\"dark\">Ipsum dolor sit</strong></p>\n</div>\n</div>\n</div>', 'a-propos'),
(4, 3, 1, 'Über uns', '', 'Learn more about us', 'über uns, Informationen', '<h2>About us</h2>\r\n<h3>Our company</h3><p>Our company</p>\r\n<h3>Our team</h3><p>Our team</p>\r\n<h3>Informations</h3><p>Informations</p>', 'uber-uns'),
(4, 4, 1, 'Over ons', '', 'Meer over ons weten', 'over ons, informatie', '<h1 class=\"page-heading bottom-indent\">Over ons</h1><div class=\"row\"><div class=\"col-xs-12 col-sm-4\"><div class=\"cms-block\"><h3 class=\"page-subheading\">Ons bedrijf</h3><p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididun.</strong></p><p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet conse ctetur adipisicing elit.</p><ul class=\"list-1\"><li><em class=\"icon-ok\"></em>Producten van topkwaliteit</li><li><em class=\"icon-ok\"></em>Beste klantenservice</li><li><em class=\"icon-ok\"></em>30-dagen-geld-terug garantie</li></ul></div></div><div class=\"col-xs-12 col-sm-4\"><div class=\"cms-box\"><h3 class=\"page-subheading\">Ons team</h3><strong class=\"dark\">Lorem set sint occaecat cupidatat non </strong></p><p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p></div></div><div class=\"col-xs-12 col-sm-4\"><div class=\"cms-box\"><h3 class=\"page-subheading\">Getuigenissen</h3><div class=\"testimonials\"><div class=\"inner\"><span class=\"before\">“</span>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.<span class=\"after\">”</span></div></div><p><strong class=\"dark\">Lorem ipsum dolor sit</strong></p><div class=\"testimonials\"><div class=\"inner\"><span class=\"before\">“</span>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod.<span class=\"after\">”</span></div></div><p><strong class=\"dark\">Ipsum dolor sit</strong></p></div></div></div>', 'over-ons'),
(4, 5, 1, 'About us', '', 'Learn more about us', 'about us, informations', '<h1 class=\"page-heading bottom-indent\">About us</h1>\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-block\">\n<h3 class=\"page-subheading\">Our company</h3>\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididun.</strong></p>\n<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet conse ctetur adipisicing elit.</p>\n<ul class=\"list-1\">\n<li><em class=\"icon-ok\"></em>Top quality products</li>\n<li><em class=\"icon-ok\"></em>Best customer service</li>\n<li><em class=\"icon-ok\"></em>30-days money back guarantee</li>\n</ul>\n</div>\n</div>\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-box\">\n<h3 class=\"page-subheading\">Our team</h3>\n<p><strong class=\"dark\">Lorem set sint occaecat cupidatat non </strong></p>\n<p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>\n</div>\n</div>\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-box\">\n<h3 class=\"page-subheading\">Testimonials</h3>\n<div class=\"testimonials\">\n<div class=\"inner\"><span class=\"before\">“</span>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.<span class=\"after\">”</span></div>\n</div>\n<p><strong class=\"dark\">Lorem ipsum dolor sit</strong></p>\n<div class=\"testimonials\">\n<div class=\"inner\"><span class=\"before\">“</span>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod.<span class=\"after\">”</span></div>\n</div>\n<p><strong class=\"dark\">Ipsum dolor sit</strong></p>\n</div>\n</div>\n</div>', 'about-us'),
(4, 6, 1, 'About us', '', 'Learn more about us', 'about us, informations', '<h1 class=\"page-heading bottom-indent\">About us</h1>\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-block\">\n<h3 class=\"page-subheading\">Our company</h3>\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididun.</strong></p>\n<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet conse ctetur adipisicing elit.</p>\n<ul class=\"list-1\">\n<li><em class=\"icon-ok\"></em>Top quality products</li>\n<li><em class=\"icon-ok\"></em>Best customer service</li>\n<li><em class=\"icon-ok\"></em>30-days money back guarantee</li>\n</ul>\n</div>\n</div>\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-box\">\n<h3 class=\"page-subheading\">Our team</h3>\n<p><strong class=\"dark\">Lorem set sint occaecat cupidatat non </strong></p>\n<p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>\n</div>\n</div>\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-box\">\n<h3 class=\"page-subheading\">Testimonials</h3>\n<div class=\"testimonials\">\n<div class=\"inner\"><span class=\"before\">“</span>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.<span class=\"after\">”</span></div>\n</div>\n<p><strong class=\"dark\">Lorem ipsum dolor sit</strong></p>\n<div class=\"testimonials\">\n<div class=\"inner\"><span class=\"before\">“</span>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod.<span class=\"after\">”</span></div>\n</div>\n<p><strong class=\"dark\">Ipsum dolor sit</strong></p>\n</div>\n</div>\n</div>', 'about-us'),
(5, 1, 1, 'Secure payment', '', 'Our secure payment method', 'secure payment, ssl, visa, mastercard, paypal', '<h2>Secure payment</h2>\r\n<h3>Our secure payment</h3><p>With SSL</p>\r\n<h3>Using Visa/Mastercard/Paypal</h3><p>About this service</p>', 'secure-payment'),
(5, 2, 1, 'Paiement sécurisé', '', 'Notre méthode de paiement sécurisé', 'paiement sécurisé, ssl, visa, mastercard, paypal', '<h2>Paiement sécurisé</h2>\n<h3>Notre paiement sécurisé</h3><p>Avec SSL</p>\n<h3>Avec Visa/Mastercard/Paypal</h3><p>A propos de ce service</p>', 'paiement-securise'),
(5, 3, 1, 'Sichere Zahlung', '', 'Unsere sicheren Zahlungsarten', 'Sichere Zahlung, SSL, Visa, MasterCard, PayPal', '<h2>Secure payment</h2>\r\n<h3>Our secure payment</h3><p>With SSL</p>\r\n<h3>Using Visa/Mastercard/Paypal</h3><p>About this services</p>', 'sichere-zahlung'),
(5, 4, 1, 'Veilige betaling', '', 'Onze veilige betaalmethode', 'veilige betaling, ssl, visa, mastercard, paypal', '<h2>Veilige betaling</h2><h3>Onze veilige betaling</h3><p>Met SSL</p><h3>Aanvaardt Visa/Mastercard/Paypal</h3><p>Over deze dienst</p>', 'veilige-betaling'),
(5, 5, 1, 'Secure payment', '', 'Our secure payment method', 'secure payment, ssl, visa, mastercard, paypal', '<h2>Secure payment</h2>\r\n<h3>Our secure payment</h3><p>With SSL</p>\r\n<h3>Using Visa/Mastercard/Paypal</h3><p>About this service</p>', 'secure-payment'),
(5, 6, 1, 'Secure payment', '', 'Our secure payment method', 'secure payment, ssl, visa, mastercard, paypal', '<h2>Secure payment</h2>\r\n<h3>Our secure payment</h3><p>With SSL</p>\r\n<h3>Using Visa/Mastercard/Paypal</h3><p>About this service</p>', 'secure-payment');

-- --------------------------------------------------------

--
-- Structure de la table `ps_cms_role`
--

DROP TABLE IF EXISTS `ps_cms_role`;
CREATE TABLE IF NOT EXISTS `ps_cms_role` (
  `id_cms_role` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `id_cms` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cms_role`,`id_cms`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_cms_role`
--

INSERT INTO `ps_cms_role` (`id_cms_role`, `name`, `id_cms`) VALUES
(1, 'LEGAL_CONDITIONS', 3),
(2, 'LEGAL_NOTICE', 2);

-- --------------------------------------------------------

--
-- Structure de la table `ps_cms_role_lang`
--

DROP TABLE IF EXISTS `ps_cms_role_lang`;
CREATE TABLE IF NOT EXISTS `ps_cms_role_lang` (
  `id_cms_role` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_cms_role`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_cms_shop`
--

DROP TABLE IF EXISTS `ps_cms_shop`;
CREATE TABLE IF NOT EXISTS `ps_cms_shop` (
  `id_cms` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cms`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_cms_shop`
--

INSERT INTO `ps_cms_shop` (`id_cms`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_condition`
--

DROP TABLE IF EXISTS `ps_condition`;
CREATE TABLE IF NOT EXISTS `ps_condition` (
  `id_condition` int(11) NOT NULL AUTO_INCREMENT,
  `id_ps_condition` int(11) NOT NULL,
  `type` enum('configuration','install','sql') NOT NULL,
  `request` text,
  `operator` varchar(32) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  `result` varchar(64) DEFAULT NULL,
  `calculation_type` enum('hook','time') DEFAULT NULL,
  `calculation_detail` varchar(64) DEFAULT NULL,
  `validated` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_condition`,`id_ps_condition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_condition_advice`
--

DROP TABLE IF EXISTS `ps_condition_advice`;
CREATE TABLE IF NOT EXISTS `ps_condition_advice` (
  `id_condition` int(11) NOT NULL,
  `id_advice` int(11) NOT NULL,
  `display` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_condition`,`id_advice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_condition_badge`
--

DROP TABLE IF EXISTS `ps_condition_badge`;
CREATE TABLE IF NOT EXISTS `ps_condition_badge` (
  `id_condition` int(11) NOT NULL,
  `id_badge` int(11) NOT NULL,
  PRIMARY KEY (`id_condition`,`id_badge`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_configuration`
--

DROP TABLE IF EXISTS `ps_configuration`;
CREATE TABLE IF NOT EXISTS `ps_configuration` (
  `id_configuration` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) UNSIGNED DEFAULT NULL,
  `id_shop` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(254) NOT NULL,
  `value` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_configuration`),
  KEY `name` (`name`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=465 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_configuration`
--

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(1, NULL, NULL, 'PS_LANG_DEFAULT', '1', '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(2, NULL, NULL, 'PS_VERSION_DB', '1.7.8.7', '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(3, NULL, NULL, 'PS_INSTALL_VERSION', '1.7.8.7', '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(4, NULL, NULL, 'PS_CARRIER_DEFAULT', '1', '2022-11-21 11:03:35', '2022-11-21 11:03:35'),
(5, NULL, NULL, 'PS_GROUP_FEATURE_ACTIVE', '1', '2022-11-21 11:03:35', '2022-11-21 11:03:35'),
(6, NULL, NULL, 'PS_CURRENCY_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, NULL, NULL, 'PS_COUNTRY_DEFAULT', '3', '0000-00-00 00:00:00', '2022-11-21 11:03:36'),
(8, NULL, NULL, 'PS_REWRITING_SETTINGS', '1', '0000-00-00 00:00:00', '2022-11-21 11:03:36'),
(9, NULL, NULL, 'PS_ORDER_OUT_OF_STOCK', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, NULL, NULL, 'PS_LAST_QTIES', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, NULL, NULL, 'PS_CONDITIONS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, NULL, NULL, 'PS_RECYCLABLE_PACK', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, NULL, NULL, 'PS_GIFT_WRAPPING', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, NULL, NULL, 'PS_GIFT_WRAPPING_PRICE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, NULL, NULL, 'PS_STOCK_MANAGEMENT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, NULL, NULL, 'PS_NAVIGATION_PIPE', '>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, NULL, NULL, 'PS_PRODUCTS_PER_PAGE', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, NULL, NULL, 'PS_PURCHASE_MINIMUM', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, NULL, NULL, 'PS_PRODUCTS_ORDER_WAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, NULL, NULL, 'PS_PRODUCTS_ORDER_BY', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, NULL, NULL, 'PS_DISPLAY_QTIES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, NULL, NULL, 'PS_SHIPPING_HANDLING', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, NULL, NULL, 'PS_SHIPPING_FREE_PRICE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, NULL, NULL, 'PS_SHIPPING_FREE_WEIGHT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, NULL, NULL, 'PS_SHIPPING_METHOD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, NULL, NULL, 'PS_TAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, NULL, NULL, 'PS_SHOP_ENABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, NULL, NULL, 'PS_NB_DAYS_NEW_PRODUCT', '20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, NULL, NULL, 'PS_SSL_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, NULL, NULL, 'PS_WEIGHT_UNIT', 'kg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, NULL, NULL, 'PS_BLOCK_CART_AJAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, NULL, NULL, 'PS_ORDER_RETURN', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, NULL, NULL, 'PS_ORDER_RETURN_NB_DAYS', '14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, NULL, NULL, 'PS_MAIL_TYPE', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, NULL, NULL, 'PS_PRODUCT_PICTURE_MAX_SIZE', '8388608', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, NULL, NULL, 'PS_PRODUCT_PICTURE_WIDTH', '64', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, NULL, NULL, 'PS_PRODUCT_PICTURE_HEIGHT', '64', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, NULL, NULL, 'PS_INVOICE_PREFIX', '#IN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, NULL, NULL, 'PS_INVCE_INVOICE_ADDR_RULES', '{\"avoid\":[]}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, NULL, NULL, 'PS_INVCE_DELIVERY_ADDR_RULES', '{\"avoid\":[]}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, NULL, NULL, 'PS_DELIVERY_PREFIX', '#DE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, NULL, NULL, 'PS_DELIVERY_NUMBER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, NULL, NULL, 'PS_RETURN_PREFIX', '#RE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, NULL, NULL, 'PS_INVOICE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, NULL, NULL, 'PS_PASSWD_TIME_BACK', '360', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, NULL, NULL, 'PS_PASSWD_TIME_FRONT', '360', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, NULL, NULL, 'PS_PASSWD_RESET_VALIDITY', '1440', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, NULL, NULL, 'PS_DISP_UNAVAILABLE_ATTR', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, NULL, NULL, 'PS_SEARCH_INDEXATION', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, NULL, NULL, 'PS_SEARCH_FUZZY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, NULL, NULL, 'PS_SEARCH_FUZZY_MAX_LOOP', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, NULL, NULL, 'PS_SEARCH_MAX_WORD_LENGTH', '15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, NULL, NULL, 'PS_SEARCH_MINWORDLEN', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, NULL, NULL, 'PS_SEARCH_BLACKLIST', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, NULL, NULL, 'PS_SEARCH_WEIGHT_PNAME', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, NULL, NULL, 'PS_SEARCH_WEIGHT_REF', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, NULL, NULL, 'PS_SEARCH_WEIGHT_SHORTDESC', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, NULL, NULL, 'PS_SEARCH_WEIGHT_DESC', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, NULL, NULL, 'PS_SEARCH_WEIGHT_CNAME', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, NULL, NULL, 'PS_SEARCH_WEIGHT_MNAME', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, NULL, NULL, 'PS_SEARCH_WEIGHT_TAG', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, NULL, NULL, 'PS_SEARCH_WEIGHT_ATTRIBUTE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, NULL, NULL, 'PS_SEARCH_WEIGHT_FEATURE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, NULL, NULL, 'PS_SEARCH_AJAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, NULL, NULL, 'PS_TIMEZONE', 'Europe/Brussels', '0000-00-00 00:00:00', '2022-11-21 11:03:36'),
(66, NULL, NULL, 'PS_THEME_V11', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, NULL, NULL, 'PRESTASTORE_LIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, NULL, NULL, 'PS_TIN_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, NULL, NULL, 'PS_SHOW_ALL_MODULES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, NULL, NULL, 'PS_BACKUP_ALL', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, NULL, NULL, 'PS_1_3_UPDATE_DATE', '2011-12-27 10:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, NULL, NULL, 'PS_PRICE_ROUND_MODE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, NULL, NULL, 'PS_1_3_2_UPDATE_DATE', '2011-12-27 10:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, NULL, NULL, 'PS_CONDITIONS_CMS_ID', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, NULL, NULL, 'TRACKING_DIRECT_TRAFFIC', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, NULL, NULL, 'PS_VOLUME_UNIT', 'L', '0000-00-00 00:00:00', '2022-12-05 11:00:13'),
(77, NULL, NULL, 'PS_CIPHER_ALGORITHM', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, NULL, NULL, 'PS_ATTRIBUTE_CATEGORY_DISPLAY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, NULL, NULL, 'PS_CUSTOMER_SERVICE_FILE_UPLOAD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, NULL, NULL, 'PS_CUSTOMER_SERVICE_SIGNATURE', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, NULL, NULL, 'PS_BLOCK_BESTSELLERS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, NULL, NULL, 'PS_BLOCK_NEWPRODUCTS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, NULL, NULL, 'PS_BLOCK_SPECIALS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, NULL, NULL, 'PS_STOCK_MVT_REASON_DEFAULT', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, NULL, NULL, 'PS_SPECIFIC_PRICE_PRIORITIES', 'id_shop;id_currency;id_country;id_group', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, NULL, NULL, 'PS_TAX_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, NULL, NULL, 'PS_SMARTY_FORCE_COMPILE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, NULL, NULL, 'PS_DISTANCE_UNIT', 'km', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, NULL, NULL, 'PS_STORES_DISPLAY_CMS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, NULL, NULL, 'SHOP_LOGO_WIDTH', '152', '0000-00-00 00:00:00', '2022-12-08 16:15:09'),
(91, NULL, NULL, 'SHOP_LOGO_HEIGHT', '178', '0000-00-00 00:00:00', '2022-12-08 16:15:09'),
(92, NULL, NULL, 'EDITORIAL_IMAGE_WIDTH', '530', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, NULL, NULL, 'EDITORIAL_IMAGE_HEIGHT', '228', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, NULL, NULL, 'PS_STATSDATA_CUSTOMER_PAGESVIEWS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, NULL, NULL, 'PS_STATSDATA_PAGESVIEWS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, NULL, NULL, 'PS_STATSDATA_PLUGINS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, NULL, NULL, 'PS_GEOLOCATION_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, NULL, NULL, 'PS_ALLOWED_COUNTRIES', 'AF;ZA;AX;AL;DZ;DE;AD;AO;AI;AQ;AG;AN;SA;AR;AM;AW;AU;AT;AZ;BS;BH;BD;BB;BY;BE;BZ;BJ;BM;BT;BO;BA;BW;BV;BR;BN;BG;BF;MM;BI;KY;KH;CM;CA;CV;CF;CL;CN;CX;CY;CC;CO;KM;CG;CD;CK;KR;KP;CR;CI;HR;CU;DK;DJ;DM;EG;IE;SV;AE;EC;ER;ES;EE;ET;FK;FO;FJ;FI;FR;GA;GM;GE;GS;GH;GI;GR;GD;GL;GP;GU;GT;GG;GN;GQ;GW;GY;GF;HT;HM;HN;HK;HU;IM;MU;VG;VI;IN;ID;IR;IQ;IS;IL;IT;JM;JP;JE;JO;KZ;KE;KG;KI;KW;LA;LS;LV;LB;LR;LY;LI;LT;LU;MO;MK;MG;MY;MW;MV;ML;MT;MP;MA;MH;MQ;MR;YT;MX;FM;MD;MC;MN;ME;MS;MZ;NA;NR;NP;NI;NE;NG;NU;NF;NO;NC;NZ;IO;OM;UG;UZ;PK;PW;PS;PA;PG;PY;NL;PE;PH;PN;PL;PF;PR;PT;QA;DO;CZ;RE;RO;GB;RU;RW;EH;BL;KN;SM;MF;PM;VA;VC;LC;SB;WS;AS;ST;SN;RS;SC;SL;SG;SK;SI;SO;SD;LK;SE;CH;SR;SJ;SZ;SY;TJ;TW;TZ;TD;TF;TH;TL;TG;TK;TO;TT;TN;TM;TC;TR;TV;UA;UY;US;VU;VE;VN;WF;YE;ZM;ZW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, NULL, NULL, 'PS_GEOLOCATION_BEHAVIOR', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, NULL, NULL, 'PS_LOCALE_LANGUAGE', 'en', '0000-00-00 00:00:00', '2022-11-21 11:03:36'),
(101, NULL, NULL, 'PS_LOCALE_COUNTRY', 'be', '0000-00-00 00:00:00', '2022-11-21 11:03:36'),
(102, NULL, NULL, 'PS_ATTACHMENT_MAXIMUM_SIZE', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, NULL, NULL, 'PS_SMARTY_CACHE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, NULL, NULL, 'PS_DIMENSION_UNIT', 'cm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, NULL, NULL, 'PS_GUEST_CHECKOUT_ENABLED', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, NULL, NULL, 'PS_DISPLAY_SUPPLIERS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, NULL, NULL, 'PS_DISPLAY_MANUFACTURERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, NULL, NULL, 'PS_DISPLAY_BEST_SELLERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, NULL, NULL, 'PS_CATALOG_MODE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, NULL, NULL, 'PS_GEOLOCATION_WHITELIST', '127;::1;188.165.122;209.185.108;209.185.253;209.85.238;209.85.238.11;209.85.238.4;216.239.33.96;216.239.33.97;216.239.33.98;216.239.33.99;216.239.37.98;216.239.37.99;216.239.39.98;216.239.39.99;216.239.41.96;216.239.41.97;216.239.41.98;216.239.41.99;216.239.45.4;216.239.46;216.239.51.96;216.239.51.97;216.239.51.98;216.239.51.99;216.239.53.98;216.239.53.99;216.239.57.96;91.240.109;216.239.57.97;216.239.57.98;216.239.57.99;216.239.59.98;216.239.59.99;216.33.229.163;64.233.173.193;64.233.173.194;64.233.173.195;64.233.173.196;64.233.173.197;64.233.173.198;64.233.173.199;64.233.173.200;64.233.173.201;64.233.173.202;64.233.173.203;64.233.173.204;64.233.173.205;64.233.173.206;64.233.173.207;64.233.173.208;64.233.173.209;64.233.173.210;64.233.173.211;64.233.173.212;64.233.173.213;64.233.173.214;64.233.173.215;64.233.173.216;64.233.173.217;64.233.173.218;64.233.173.219;64.233.173.220;64.233.173.221;64.233.173.222;64.233.173.223;64.233.173.224;64.233.173.225;64.233.173.226;64.233.173.227;64.233.173.228;64.233.173.229;64.233.173.230;64.233.173.231;64.233.173.232;64.233.173.233;64.233.173.234;64.233.173.235;64.233.173.236;64.233.173.237;64.233.173.238;64.233.173.239;64.233.173.240;64.233.173.241;64.233.173.242;64.233.173.243;64.233.173.244;64.233.173.245;64.233.173.246;64.233.173.247;64.233.173.248;64.233.173.249;64.233.173.250;64.233.173.251;64.233.173.252;64.233.173.253;64.233.173.254;64.233.173.255;64.68.80;64.68.81;64.68.82;64.68.83;64.68.84;64.68.85;64.68.86;64.68.87;64.68.88;64.68.89;64.68.90.1;64.68.90.10;64.68.90.11;64.68.90.12;64.68.90.129;64.68.90.13;64.68.90.130;64.68.90.131;64.68.90.132;64.68.90.133;64.68.90.134;64.68.90.135;64.68.90.136;64.68.90.137;64.68.90.138;64.68.90.139;64.68.90.14;64.68.90.140;64.68.90.141;64.68.90.142;64.68.90.143;64.68.90.144;64.68.90.145;64.68.90.146;64.68.90.147;64.68.90.148;64.68.90.149;64.68.90.15;64.68.90.150;64.68.90.151;64.68.90.152;64.68.90.153;64.68.90.154;64.68.90.155;64.68.90.156;64.68.90.157;64.68.90.158;64.68.90.159;64.68.90.16;64.68.90.160;64.68.90.161;64.68.90.162;64.68.90.163;64.68.90.164;64.68.90.165;64.68.90.166;64.68.90.167;64.68.90.168;64.68.90.169;64.68.90.17;64.68.90.170;64.68.90.171;64.68.90.172;64.68.90.173;64.68.90.174;64.68.90.175;64.68.90.176;64.68.90.177;64.68.90.178;64.68.90.179;64.68.90.18;64.68.90.180;64.68.90.181;64.68.90.182;64.68.90.183;64.68.90.184;64.68.90.185;64.68.90.186;64.68.90.187;64.68.90.188;64.68.90.189;64.68.90.19;64.68.90.190;64.68.90.191;64.68.90.192;64.68.90.193;64.68.90.194;64.68.90.195;64.68.90.196;64.68.90.197;64.68.90.198;64.68.90.199;64.68.90.2;64.68.90.20;64.68.90.200;64.68.90.201;64.68.90.202;64.68.90.203;64.68.90.204;64.68.90.205;64.68.90.206;64.68.90.207;64.68.90.208;64.68.90.21;64.68.90.22;64.68.90.23;64.68.90.24;64.68.90.25;64.68.90.26;64.68.90.27;64.68.90.28;64.68.90.29;64.68.90.3;64.68.90.30;64.68.90.31;64.68.90.32;64.68.90.33;64.68.90.34;64.68.90.35;64.68.90.36;64.68.90.37;64.68.90.38;64.68.90.39;64.68.90.4;64.68.90.40;64.68.90.41;64.68.90.42;64.68.90.43;64.68.90.44;64.68.90.45;64.68.90.46;64.68.90.47;64.68.90.48;64.68.90.49;64.68.90.5;64.68.90.50;64.68.90.51;64.68.90.52;64.68.90.53;64.68.90.54;64.68.90.55;64.68.90.56;64.68.90.57;64.68.90.58;64.68.90.59;64.68.90.6;64.68.90.60;64.68.90.61;64.68.90.62;64.68.90.63;64.68.90.64;64.68.90.65;64.68.90.66;64.68.90.67;64.68.90.68;64.68.90.69;64.68.90.7;64.68.90.70;64.68.90.71;64.68.90.72;64.68.90.73;64.68.90.74;64.68.90.75;64.68.90.76;64.68.90.77;64.68.90.78;64.68.90.79;64.68.90.8;64.68.90.80;64.68.90.9;64.68.91;64.68.92;66.249.64;66.249.65;66.249.66;66.249.67;66.249.68;66.249.69;66.249.70;66.249.71;66.249.72;66.249.73;66.249.78;66.249.79;72.14.199;8.6.48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, NULL, NULL, 'PS_LOGS_BY_EMAIL', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, NULL, NULL, 'PS_COOKIE_CHECKIP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, NULL, NULL, 'PS_COOKIE_SAMESITE', 'Lax', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, NULL, NULL, 'PS_USE_ECOTAX', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, NULL, NULL, 'PS_CANONICAL_REDIRECT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, NULL, NULL, 'PS_IMG_UPDATE_TIME', '1670512509', '0000-00-00 00:00:00', '2022-12-08 16:15:09'),
(117, NULL, NULL, 'PS_BACKUP_DROP_TABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, NULL, NULL, 'PS_OS_CHEQUE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, NULL, NULL, 'PS_OS_PAYMENT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, NULL, NULL, 'PS_OS_PREPARATION', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, NULL, NULL, 'PS_OS_SHIPPING', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, NULL, NULL, 'PS_OS_DELIVERED', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, NULL, NULL, 'PS_OS_CANCELED', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, NULL, NULL, 'PS_OS_REFUND', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, NULL, NULL, 'PS_OS_ERROR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, NULL, NULL, 'PS_OS_OUTOFSTOCK', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, NULL, NULL, 'PS_OS_BANKWIRE', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, NULL, NULL, 'PS_OS_WS_PAYMENT', '11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, NULL, NULL, 'PS_OS_OUTOFSTOCK_PAID', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, NULL, NULL, 'PS_OS_OUTOFSTOCK_UNPAID', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, NULL, NULL, 'PS_OS_COD_VALIDATION', '13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, NULL, NULL, 'PS_LEGACY_IMAGES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, NULL, NULL, 'PS_IMAGE_QUALITY', 'png', '0000-00-00 00:00:00', '2022-11-21 11:06:42'),
(134, NULL, NULL, 'PS_PNG_QUALITY', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, NULL, NULL, 'PS_JPEG_QUALITY', '90', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, NULL, NULL, 'PS_COOKIE_LIFETIME_FO', '480', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, NULL, NULL, 'PS_COOKIE_LIFETIME_BO', '480', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, NULL, NULL, 'PS_RESTRICT_DELIVERED_COUNTRIES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, NULL, NULL, 'PS_SHOW_NEW_ORDERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, NULL, NULL, 'PS_SHOW_NEW_CUSTOMERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, NULL, NULL, 'PS_SHOW_NEW_MESSAGES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, NULL, NULL, 'PS_FEATURE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, NULL, NULL, 'PS_COMBINATION_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, NULL, NULL, 'PS_SPECIFIC_PRICE_FEATURE_ACTIVE', NULL, '0000-00-00 00:00:00', '2022-12-11 15:49:05'),
(145, NULL, NULL, 'PS_VIRTUAL_PROD_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '2022-11-21 11:07:25'),
(146, NULL, NULL, 'PS_CUSTOMIZATION_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, NULL, NULL, 'PS_CART_RULE_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, NULL, NULL, 'PS_PACK_FEATURE_ACTIVE', NULL, '0000-00-00 00:00:00', '2022-12-11 16:39:06'),
(149, NULL, NULL, 'PS_ALIAS_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, NULL, NULL, 'PS_TAX_ADDRESS_TYPE', 'id_address_delivery', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, NULL, NULL, 'PS_SHOP_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, NULL, NULL, 'PS_CARRIER_DEFAULT_SORT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, NULL, NULL, 'PS_STOCK_MVT_INC_REASON_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, NULL, NULL, 'PS_STOCK_MVT_DEC_REASON_DEFAULT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, NULL, NULL, 'PS_ADVANCED_STOCK_MANAGEMENT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, NULL, NULL, 'PS_STOCK_MVT_TRANSFER_TO', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, NULL, NULL, 'PS_STOCK_MVT_TRANSFER_FROM', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, NULL, NULL, 'PS_CARRIER_DEFAULT_ORDER', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, NULL, NULL, 'PS_STOCK_MVT_SUPPLY_ORDER', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, NULL, NULL, 'PS_STOCK_CUSTOMER_ORDER_CANCEL_REASON', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, NULL, NULL, 'PS_STOCK_CUSTOMER_RETURN_REASON', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, NULL, NULL, 'PS_STOCK_MVT_INC_EMPLOYEE_EDITION', '11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, NULL, NULL, 'PS_STOCK_MVT_DEC_EMPLOYEE_EDITION', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, NULL, NULL, 'PS_STOCK_CUSTOMER_ORDER_REASON', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, NULL, NULL, 'PS_UNIDENTIFIED_GROUP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, NULL, NULL, 'PS_GUEST_GROUP', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, NULL, NULL, 'PS_CUSTOMER_GROUP', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, NULL, NULL, 'PS_SMARTY_CONSOLE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, NULL, NULL, 'PS_INVOICE_MODEL', 'invoice', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, NULL, NULL, 'PS_LIMIT_UPLOAD_IMAGE_VALUE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, NULL, NULL, 'PS_LIMIT_UPLOAD_FILE_VALUE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, NULL, NULL, 'MB_PAY_TO_EMAIL', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, NULL, NULL, 'MB_SECRET_WORD', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, NULL, NULL, 'MB_HIDE_LOGIN', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, NULL, NULL, 'MB_ID_LOGO', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, NULL, NULL, 'MB_ID_LOGO_WALLET', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, NULL, NULL, 'MB_PARAMETERS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, NULL, NULL, 'MB_PARAMETERS_2', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, NULL, NULL, 'MB_DISPLAY_MODE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, NULL, NULL, 'MB_CANCEL_URL', 'http://www.yoursite.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, NULL, NULL, 'MB_LOCAL_METHODS', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, NULL, NULL, 'MB_INTER_METHODS', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, NULL, NULL, 'BANK_WIRE_CURRENCIES', '2,1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, NULL, NULL, 'CHEQUE_CURRENCIES', '2,1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, NULL, NULL, 'PRODUCTS_VIEWED_NBR', '8', '0000-00-00 00:00:00', '2022-12-11 16:01:44'),
(186, NULL, NULL, 'BLOCK_CATEG_DHTML', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, NULL, NULL, 'BLOCK_CATEG_MAX_DEPTH', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, NULL, NULL, 'MANUFACTURER_DISPLAY_FORM', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, NULL, NULL, 'MANUFACTURER_DISPLAY_TEXT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, NULL, NULL, 'MANUFACTURER_DISPLAY_TEXT_NB', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, NULL, NULL, 'NEW_PRODUCTS_NBR', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, NULL, NULL, 'PS_TOKEN_ENABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, NULL, NULL, 'PS_STATS_RENDER', 'graphnvd3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, NULL, NULL, 'PS_STATS_OLD_CONNECT_AUTO_CLEAN', 'never', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, NULL, NULL, 'PS_STATS_GRID_RENDER', 'gridhtml', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, NULL, NULL, 'BLOCKTAGS_NBR', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, NULL, NULL, 'CHECKUP_DESCRIPTIONS_LT', '100', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, NULL, NULL, 'CHECKUP_DESCRIPTIONS_GT', '400', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, NULL, NULL, 'CHECKUP_IMAGES_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, NULL, NULL, 'CHECKUP_IMAGES_GT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, NULL, NULL, 'CHECKUP_SALES_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, NULL, NULL, 'CHECKUP_SALES_GT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, NULL, NULL, 'CHECKUP_STOCK_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, NULL, NULL, 'CHECKUP_STOCK_GT', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, NULL, NULL, 'FOOTER_CMS', '0_3|0_4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, NULL, NULL, 'FOOTER_BLOCK_ACTIVATION', '0_3|0_4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, NULL, NULL, 'FOOTER_POWEREDBY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, NULL, NULL, 'BLOCKADVERT_LINK', 'https://www.prestashop.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, NULL, NULL, 'BLOCKSTORE_IMG', 'store.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(210, NULL, NULL, 'BLOCKADVERT_IMG_EXT', 'jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(211, NULL, NULL, 'MOD_BLOCKTOPMENU_ITEMS', 'CAT2,CAT10,CAT12,CAT13', '0000-00-00 00:00:00', '2022-12-08 16:18:32'),
(212, NULL, NULL, 'MOD_BLOCKTOPMENU_SEARCH', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, NULL, NULL, 'BLOCKSOCIAL_FACEBOOK', NULL, '0000-00-00 00:00:00', '2022-11-21 11:03:48'),
(214, NULL, NULL, 'BLOCKSOCIAL_TWITTER', NULL, '0000-00-00 00:00:00', '2022-11-21 11:03:48'),
(215, NULL, NULL, 'BLOCKSOCIAL_RSS', NULL, '0000-00-00 00:00:00', '2022-11-21 11:03:48'),
(216, NULL, NULL, 'BLOCKCONTACTINFOS_COMPANY', 'Your company', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(217, NULL, NULL, 'BLOCKCONTACTINFOS_ADDRESS', 'Address line 1\nCity\nCountry', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(218, NULL, NULL, 'BLOCKCONTACTINFOS_PHONE', '0123-456-789', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(219, NULL, NULL, 'BLOCKCONTACTINFOS_EMAIL', 'pub@prestashop.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(220, NULL, NULL, 'BLOCKCONTACT_TELNUMBER', '0123-456-789', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(221, NULL, NULL, 'BLOCKCONTACT_EMAIL', 'pub@prestashop.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(222, NULL, NULL, 'SUPPLIER_DISPLAY_TEXT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(223, NULL, NULL, 'SUPPLIER_DISPLAY_TEXT_NB', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(224, NULL, NULL, 'SUPPLIER_DISPLAY_FORM', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(225, NULL, NULL, 'BLOCK_CATEG_NBR_COLUMN_FOOTER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(226, NULL, NULL, 'UPGRADER_BACKUPDB_FILENAME', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, NULL, NULL, 'UPGRADER_BACKUPFILES_FILENAME', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(228, NULL, NULL, 'BLOCKREINSURANCE_NBBLOCKS', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, NULL, NULL, 'HOMESLIDER_WIDTH', '535', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, NULL, NULL, 'HOMESLIDER_SPEED', '5000', '0000-00-00 00:00:00', '2022-11-21 11:03:47'),
(231, NULL, NULL, 'HOMESLIDER_PAUSE', '7700', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, NULL, NULL, 'HOMESLIDER_LOOP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(233, NULL, NULL, 'PS_BASE_DISTANCE_UNIT', 'm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(234, NULL, NULL, 'PS_SHOP_DOMAIN', 'localhost', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(235, NULL, NULL, 'PS_SHOP_DOMAIN_SSL', 'localhost', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, NULL, NULL, 'PS_SHOP_NAME', 'Le Dédale', '0000-00-00 00:00:00', '2022-12-11 16:08:57'),
(237, NULL, NULL, 'PS_SHOP_EMAIL', 'test@test.com', '0000-00-00 00:00:00', '2022-11-21 11:03:40'),
(238, NULL, NULL, 'PS_MAIL_METHOD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(239, NULL, NULL, 'PS_SHOP_ACTIVITY', '7', '0000-00-00 00:00:00', '2022-11-21 11:03:36'),
(240, NULL, NULL, 'PS_LOGO', 'logo-1670512509.jpg', '0000-00-00 00:00:00', '2022-12-08 16:15:09'),
(241, NULL, NULL, 'PS_FAVICON', 'favicon.ico', '0000-00-00 00:00:00', '2022-12-05 09:28:23'),
(242, NULL, NULL, 'PS_STORES_ICON', 'logo_stores.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, NULL, NULL, 'PS_ROOT_CATEGORY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, NULL, NULL, 'PS_HOME_CATEGORY', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, NULL, NULL, 'PS_CONFIGURATION_AGREMENT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, NULL, NULL, 'PS_MAIL_SERVER', 'smtp.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, NULL, NULL, 'PS_MAIL_USER', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, NULL, NULL, 'PS_MAIL_PASSWD', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, NULL, NULL, 'PS_MAIL_SMTP_ENCRYPTION', 'off', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, NULL, NULL, 'PS_MAIL_SMTP_PORT', '25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, NULL, NULL, 'PS_MAIL_COLOR', '#db3484', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, NULL, NULL, 'NW_SALT', 'nbpRBcdc86Md8t8D', '0000-00-00 00:00:00', '2022-11-21 11:03:46'),
(253, NULL, NULL, 'PS_PAYMENT_LOGO_CMS_ID', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, NULL, NULL, 'HOME_FEATURED_NBR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, NULL, NULL, 'SEK_MIN_OCCURENCES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, NULL, NULL, 'SEK_FILTER_KW', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, NULL, NULL, 'PS_ALLOW_MOBILE_DEVICE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, NULL, NULL, 'PS_CUSTOMER_CREATION_EMAIL', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, NULL, NULL, 'PS_SMARTY_CONSOLE_KEY', 'SMARTY_DEBUG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, NULL, NULL, 'PS_DASHBOARD_USE_PUSH', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, NULL, NULL, 'PS_ATTRIBUTE_ANCHOR_SEPARATOR', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, NULL, NULL, 'CONF_AVERAGE_PRODUCT_MARGIN', '40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, NULL, NULL, 'PS_DASHBOARD_SIMULATION', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, NULL, NULL, 'PS_USE_HTMLPURIFIER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, NULL, NULL, 'PS_SMARTY_CACHING_TYPE', 'filesystem', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, NULL, NULL, 'PS_SMARTY_LOCAL', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, NULL, NULL, 'PS_SMARTY_CLEAR_CACHE', 'everytime', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, NULL, NULL, 'PS_DETECT_LANG', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, NULL, NULL, 'PS_DETECT_COUNTRY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, NULL, NULL, 'PS_ROUND_TYPE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, NULL, NULL, 'PS_LOG_EMAILS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, NULL, NULL, 'PS_CUSTOMER_OPTIN', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, NULL, NULL, 'PS_CUSTOMER_BIRTHDATE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, NULL, NULL, 'PS_PACK_STOCK_TYPE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, NULL, NULL, 'PS_LOG_MODULE_PERFS_MODULO', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, NULL, NULL, 'PS_DISALLOW_HISTORY_REORDERING', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, NULL, NULL, 'PS_DISPLAY_PRODUCT_WEIGHT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, NULL, NULL, 'PS_PRODUCT_WEIGHT_PRECISION', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, NULL, NULL, 'PS_ACTIVE_CRONJOB_EXCHANGE_RATE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, NULL, NULL, 'PS_ORDER_RECALCULATE_SHIPPING', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, NULL, NULL, 'PS_MAINTENANCE_TEXT', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, NULL, NULL, 'PS_PRODUCT_SHORT_DESC_LIMIT', '800', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, NULL, NULL, 'PS_LABEL_IN_STOCK_PRODUCTS', 'In Stock', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, NULL, NULL, 'PS_LABEL_OOS_PRODUCTS_BOA', 'Product available for orders', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(285, NULL, NULL, 'PS_LABEL_OOS_PRODUCTS_BOD', 'Out-of-Stock', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, NULL, NULL, 'PS_CATALOG_MODE_WITH_PRICES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, NULL, NULL, 'PS_MAIL_THEME', 'modern', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, NULL, NULL, 'PS_ORDER_PRODUCTS_NB_PER_PAGE', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, NULL, NULL, 'PS_LOGS_EMAIL_RECEIVERS', 'test@test.com', '0000-00-00 00:00:00', '2022-11-21 11:03:40'),
(290, NULL, NULL, 'PS_SHOW_LABEL_OOS_LISTING_PAGES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, NULL, NULL, 'ADDONS_API_MODULE_CHANNEL', 'stable', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(292, NULL, NULL, 'PS_SSL_ENABLED_EVERYWHERE', '0', '2022-11-21 11:03:36', '2022-11-21 11:03:36'),
(293, NULL, NULL, 'blockwishlist_WishlistPageName', NULL, '2022-11-21 11:03:44', '2022-11-21 11:03:44'),
(294, NULL, NULL, 'blockwishlist_WishlistDefaultTitle', NULL, '2022-11-21 11:03:44', '2022-11-21 11:03:44'),
(295, NULL, NULL, 'blockwishlist_CreateButtonLabel', NULL, '2022-11-21 11:03:44', '2022-11-21 11:03:44'),
(296, NULL, NULL, 'DASHACTIVITY_CART_ACTIVE', '30', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(297, NULL, NULL, 'DASHACTIVITY_CART_ABANDONED_MIN', '24', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(298, NULL, NULL, 'DASHACTIVITY_CART_ABANDONED_MAX', '48', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(299, NULL, NULL, 'DASHACTIVITY_VISITOR_ONLINE', '30', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(300, NULL, NULL, 'PS_DASHGOALS_CURRENT_YEAR', '2022', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(301, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_LAST_ORDER', '10', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(302, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_BEST_SELLER', '10', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(303, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_MOST_VIEWED', '10', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(304, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_TOP_SEARCH', '10', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(305, NULL, NULL, 'GSITEMAP_PRIORITY_HOME', '1', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(306, NULL, NULL, 'GSITEMAP_PRIORITY_PRODUCT', '0.9', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(307, NULL, NULL, 'GSITEMAP_PRIORITY_CATEGORY', '0.8', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(308, NULL, NULL, 'GSITEMAP_PRIORITY_CMS', '0.7', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(309, NULL, NULL, 'GSITEMAP_FREQUENCY', 'weekly', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(310, NULL, NULL, 'PRODUCT_COMMENTS_MINIMAL_TIME', '30', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(311, NULL, NULL, 'PRODUCT_COMMENTS_ALLOW_GUESTS', '0', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(312, NULL, NULL, 'PRODUCT_COMMENTS_USEFULNESS', '1', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(313, NULL, NULL, 'PRODUCT_COMMENTS_COMMENTS_PER_PAGE', '5', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(314, NULL, NULL, 'PRODUCT_COMMENTS_ANONYMISATION', '0', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(315, NULL, NULL, 'PRODUCT_COMMENTS_MODERATE', '1', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(316, NULL, NULL, 'BANNER_IMG', NULL, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(317, NULL, NULL, 'BANNER_LINK', NULL, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(318, NULL, NULL, 'BANNER_DESC', NULL, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(319, NULL, NULL, 'BLOCK_CATEG_ROOT_CATEGORY', '1', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(320, NULL, NULL, 'CONF_PS_CHECKPAYMENT_FIXED', '0.2', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(321, NULL, NULL, 'CONF_PS_CHECKPAYMENT_VAR', '2', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(322, NULL, NULL, 'CONF_PS_CHECKPAYMENT_FIXED_FOREIGN', '0.2', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(323, NULL, NULL, 'CONF_PS_CHECKPAYMENT_VAR_FOREIGN', '2', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(324, NULL, NULL, 'PS_CONTACT_INFO_DISPLAY_EMAIL', '1', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(325, NULL, NULL, 'CROSSSELLING_DISPLAY_PRICE', '1', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(326, NULL, NULL, 'CROSSSELLING_NBR', '8', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(327, NULL, NULL, 'CUSTPRIV_MSG_AUTH', NULL, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(328, NULL, NULL, 'PS_NEWSLETTER_RAND', '6709980111036998558', '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(329, NULL, NULL, 'NW_CONDITIONS', NULL, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(339, NULL, NULL, 'CHECKBOX_ORDER', '1', '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(340, NULL, NULL, 'CHECKBOX_CUSTOMER', '1', '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(341, NULL, NULL, 'CHECKBOX_MESSAGE', '1', '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(342, NULL, NULL, 'BACKGROUND_COLOR_FAVICONBO', '#DF0067', '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(343, NULL, NULL, 'TEXT_COLOR_FAVICONBO', '#FFFFFF', '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(344, NULL, NULL, 'HOME_FEATURED_CAT', '2', '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(345, NULL, NULL, 'HOMESLIDER_PAUSE_ON_HOVER', '1', '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(346, NULL, NULL, 'HOMESLIDER_WRAP', '1', '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(347, NULL, NULL, 'PS_SC_TWITTER', '1', '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(348, NULL, NULL, 'PS_SC_FACEBOOK', '1', '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(349, NULL, NULL, 'PS_SC_PINTEREST', '1', '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(350, NULL, NULL, 'BLOCKSOCIAL_YOUTUBE', NULL, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(351, NULL, NULL, 'BLOCKSOCIAL_PINTEREST', NULL, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(352, NULL, NULL, 'BLOCKSOCIAL_VIMEO', NULL, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(353, NULL, NULL, 'BLOCKSOCIAL_INSTAGRAM', NULL, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(354, NULL, NULL, 'BLOCKSOCIAL_LINKEDIN', NULL, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(355, NULL, NULL, 'BANK_WIRE_PAYMENT_INVITE', '1', '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(356, NULL, NULL, 'CONF_PS_WIREPAYMENT_FIXED', '0.2', '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(357, NULL, NULL, 'CONF_PS_WIREPAYMENT_VAR', '2', '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(358, NULL, NULL, 'CONF_PS_WIREPAYMENT_FIXED_FOREIGN', '0.2', '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(359, NULL, NULL, 'CONF_PS_WIREPAYMENT_VAR_FOREIGN', '2', '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(360, NULL, NULL, 'GF_INSTALL_CALC', '1', '2022-11-21 11:05:58', '2022-11-21 11:17:09'),
(361, NULL, NULL, 'GF_CURRENT_LEVEL', '1', '2022-11-21 11:05:58', '2022-11-21 11:05:58'),
(362, NULL, NULL, 'GF_CURRENT_LEVEL_PERCENT', '0', '2022-11-21 11:05:58', '2022-11-21 11:05:58'),
(363, NULL, NULL, 'GF_NOTIFICATION', '0', '2022-11-21 11:05:58', '2022-11-21 11:05:58'),
(364, NULL, NULL, 'PSGDPR_CREATION_FORM_SWITCH', '1', '2022-11-21 11:05:58', '2022-11-21 11:05:58'),
(365, NULL, NULL, 'PSGDPR_CREATION_FORM', NULL, '2022-11-21 11:05:58', '2022-11-21 11:05:58'),
(366, NULL, NULL, 'PSGDPR_CUSTOMER_FORM_SWITCH', '1', '2022-11-21 11:05:58', '2022-11-21 11:05:58'),
(367, NULL, NULL, 'PSGDPR_CUSTOMER_FORM', NULL, '2022-11-21 11:05:58', '2022-11-21 11:05:58'),
(368, NULL, NULL, 'PSGDPR_ANONYMOUS_CUSTOMER', '1', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(369, NULL, NULL, 'PSGDPR_ANONYMOUS_ADDRESS', '1', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(370, NULL, NULL, 'CONF_PS_CHECKOUT_FIXED', '0.2', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(371, NULL, NULL, 'CONF_PS_CHECKOUT_VAR', '2', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(372, NULL, NULL, 'CONF_PS_CHECKOUT_FIXED_FOREIGN', '0.2', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(373, NULL, NULL, 'CONF_PS_CHECKOUT_VAR_FOREIGN', '2', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(374, NULL, NULL, 'PS_CHECKOUT_INTENT', 'CAPTURE', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(375, NULL, NULL, 'PS_CHECKOUT_MODE', 'LIVE', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(376, NULL, NULL, 'PS_CHECKOUT_PAYMENT_METHODS_ORDER', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(377, NULL, NULL, 'PS_CHECKOUT_PAYPAL_ID_MERCHANT', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(378, NULL, NULL, 'PS_CHECKOUT_PAYPAL_EMAIL_MERCHANT', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(379, NULL, NULL, 'PS_CHECKOUT_PAYPAL_EMAIL_STATUS', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(380, NULL, NULL, 'PS_CHECKOUT_PAYPAL_PAYMENT_STATUS', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(381, NULL, NULL, 'PS_CHECKOUT_CARD_PAYMENT_STATUS', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(382, NULL, NULL, 'PS_CHECKOUT_CARD_PAYMENT_ENABLED', '1', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(383, NULL, NULL, 'PS_PSX_FIREBASE_EMAIL', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(384, NULL, NULL, 'PS_PSX_FIREBASE_ID_TOKEN', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(385, NULL, NULL, 'PS_PSX_FIREBASE_LOCAL_ID', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(386, NULL, NULL, 'PS_PSX_FIREBASE_REFRESH_TOKEN', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(387, NULL, NULL, 'PS_PSX_FIREBASE_REFRESH_DATE', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(388, NULL, NULL, 'PS_CHECKOUT_PSX_FORM', NULL, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(389, NULL, NULL, 'PS_CHECKOUT_LOGGER_MAX_FILES', '15', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(390, NULL, NULL, 'PS_CHECKOUT_LOGGER_LEVEL', '400', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(391, NULL, NULL, 'PS_CHECKOUT_LOGGER_HTTP', '0', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(392, NULL, NULL, 'PS_CHECKOUT_LOGGER_HTTP_FORMAT', 'DEBUG', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(393, NULL, NULL, 'PS_CHECKOUT_INTEGRATION_DATE', '2022-14-06', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(394, NULL, NULL, 'PS_CHECKOUT_SHOP_UUID_V4', 'f2185f34-e6e7-4278-830a-61b0202d2b54', '2022-11-21 11:05:59', '2022-11-21 11:06:00'),
(395, NULL, NULL, 'PS_CHECKOUT_STATE_WAITING_PAYPAL_PAYMENT', '14', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(396, NULL, NULL, 'PS_CHECKOUT_STATE_WAITING_CREDIT_CARD_PAYMENT', '15', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(397, NULL, NULL, 'PS_CHECKOUT_STATE_WAITING_LOCAL_PAYMENT', '16', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(398, NULL, NULL, 'PS_CHECKOUT_STATE_AUTHORIZED', '17', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(399, NULL, NULL, 'PS_CHECKOUT_STATE_PARTIAL_REFUND', '18', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(400, NULL, NULL, 'PS_CHECKOUT_STATE_WAITING_CAPTURE', '19', '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(401, NULL, NULL, '0', 'PS_FACEBOOK_PIXEL_ID', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(402, NULL, NULL, '1', 'PS_FACEBOOK_ACCESS_TOKEN', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(403, NULL, NULL, '2', 'PS_FACEBOOK_PROFILES', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(404, NULL, NULL, '3', 'PS_FACEBOOK_PAGES', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(405, NULL, NULL, '4', 'PS_FACEBOOK_BUSINESS_MANAGER_ID', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(406, NULL, NULL, '5', 'PS_FACEBOOK_AD_ACCOUNT_ID', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(407, NULL, NULL, '6', 'PS_FACEBOOK_CATALOG_ID', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(408, NULL, NULL, '7', 'PS_FACEBOOK_EXTERNAL_BUSINESS_ID', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(409, NULL, NULL, '8', 'PS_FACEBOOK_PIXEL_ENABLED', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(410, NULL, NULL, '9', 'PS_FACEBOOK_PRODUCT_SYNC_FIRST_START', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(411, NULL, NULL, '10', 'PS_FACEBOOK_PRODUCT_SYNC_ON', '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(412, NULL, NULL, 'PSR_HOOK_HEADER', '0', '2022-11-21 11:06:43', '2022-11-21 11:06:43'),
(413, NULL, NULL, 'PSR_HOOK_FOOTER', '0', '2022-11-21 11:06:43', '2022-11-21 11:06:43'),
(414, NULL, NULL, 'PSR_HOOK_PRODUCT', '1', '2022-11-21 11:06:43', '2022-11-21 11:06:43'),
(415, NULL, NULL, 'PSR_HOOK_CHECKOUT', '1', '2022-11-21 11:06:43', '2022-11-21 11:06:43'),
(416, NULL, NULL, 'PSR_ICON_COLOR', '#F19D76', '2022-11-21 11:06:43', '2022-11-21 11:06:43'),
(417, NULL, NULL, 'PSR_TEXT_COLOR', '#000000', '2022-11-21 11:06:43', '2022-11-21 11:06:43'),
(418, NULL, NULL, 'PS_LAYERED_CACHE_ENABLED', '1', '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(419, NULL, NULL, 'PS_LAYERED_SHOW_QTIES', '1', '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(420, NULL, NULL, 'PS_LAYERED_FULL_TREE', '1', '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(421, NULL, NULL, 'PS_LAYERED_FILTER_PRICE_USETAX', '1', '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(422, NULL, NULL, 'PS_LAYERED_FILTER_CATEGORY_DEPTH', '1', '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(423, NULL, NULL, 'PS_LAYERED_FILTER_PRICE_ROUNDING', '1', '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(424, NULL, NULL, 'PS_LAYERED_FILTER_SHOW_OUT_OF_STOCK_LAST', '0', '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(425, NULL, NULL, 'PS_LAYERED_FILTER_BY_DEFAULT_CATEGORY', '0', '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(426, NULL, NULL, 'PS_LAYERED_INDEXED', '1', '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(427, NULL, NULL, 'GF_NOT_VIEWED_BADGE', NULL, '2022-11-21 11:17:09', '2022-11-21 11:17:09'),
(428, NULL, NULL, 'ONBOARDINGV2_SHUT_DOWN', '1', '2022-11-21 11:18:22', '2022-12-11 16:02:26'),
(429, NULL, NULL, 'PS_SHOWCASECARD_SEO_URLS_CLOSED', '1', '2022-11-21 13:29:54', '2022-11-21 13:29:54'),
(430, NULL, NULL, 'PS_LOGO_MAIL', 'logo_mail-1670512509.jpg', '2022-12-05 09:25:49', '2022-12-08 16:15:09'),
(431, NULL, NULL, 'PS_SHOWCASECARD_CATEGORIES_CLOSED', '1', '2022-12-05 09:30:08', '2022-12-05 09:30:08'),
(432, NULL, NULL, 'PS_SHOWCASECARD_CMS_PAGES_CLOSED', '1', '2022-12-05 10:21:48', '2022-12-05 10:21:48'),
(433, NULL, NULL, 'PS_LOGO_INVOICE', 'logo_invoice-1670512509.jpg', '2022-12-05 13:18:21', '2022-12-08 16:15:09'),
(434, NULL, NULL, 'PS_SHOWCASECARD_MONITORING_CLOSED', '1', '2022-12-05 13:21:31', '2022-12-05 13:21:31'),
(435, NULL, NULL, 'ONBOARDINGV2_CURRENT_STEP', '1', '2022-12-05 13:22:52', '2022-12-11 16:02:02'),
(436, NULL, NULL, 'PS_SHOWCASECARD_CUSTOMERS_CLOSED', '1', '2022-12-05 14:44:52', '2022-12-05 14:44:52'),
(437, NULL, NULL, 'ENABLE_HEADER_TOP', '1', '2022-12-11 16:04:15', '2022-12-11 16:04:15'),
(438, NULL, NULL, 'ENABLE_HEADER_NAV', '1', '2022-12-11 16:04:15', '2022-12-11 16:04:15'),
(439, NULL, NULL, 'ENABLE_FOOTER_WIDGET', '1', '2022-12-11 16:04:15', '2022-12-11 16:04:15'),
(440, NULL, NULL, 'ENABLE_FOOTER_BOTTOM', '1', '2022-12-11 16:04:15', '2022-12-11 16:04:15'),
(441, NULL, NULL, 'MYSHOP_COPYRIGHT_LINK', 'https://classydevs.com/?utm_source=myshop_theme&utm_medium=myshop_footer_copyright&utm_campaign=myshop_footer_copyright&utm_id=myshop_copyright&utm_term=myshop_footer_copyright', '2022-12-11 16:04:51', '2022-12-11 16:04:51'),
(442, NULL, NULL, 'MYSHOP_COPYRIGHT_TEXT', '© ClassyDevs 2022 | All Rights Reserved', '2022-12-11 16:04:51', '2022-12-11 16:04:51'),
(443, NULL, NULL, 'MDATEVAL', '20221211', '2022-12-11 16:04:51', '2022-12-11 16:04:51'),
(444, NULL, NULL, 'PS_SHOP_DETAILS', NULL, '2022-12-11 16:08:57', '2022-12-11 16:08:57'),
(445, NULL, NULL, 'PS_SHOP_ADDR1', NULL, '2022-12-11 16:08:57', '2022-12-11 16:08:57'),
(446, NULL, NULL, 'PS_SHOP_ADDR2', NULL, '2022-12-11 16:08:57', '2022-12-11 16:08:57'),
(447, NULL, NULL, 'PS_SHOP_CODE', NULL, '2022-12-11 16:08:57', '2022-12-11 16:08:57'),
(448, NULL, NULL, 'PS_SHOP_CITY', NULL, '2022-12-11 16:08:57', '2022-12-11 16:08:57'),
(449, NULL, NULL, 'PS_SHOP_COUNTRY_ID', '3', '2022-12-11 16:08:57', '2022-12-11 16:08:57'),
(450, NULL, NULL, 'PS_SHOP_COUNTRY', 'Belgium', '2022-12-11 16:08:57', '2022-12-11 16:08:57'),
(451, NULL, NULL, 'PS_SHOP_PHONE', NULL, '2022-12-11 16:08:57', '2022-12-11 16:08:57'),
(452, NULL, NULL, 'PS_SHOP_FAX', NULL, '2022-12-11 16:08:57', '2022-12-11 16:08:57'),
(453, NULL, NULL, 'SMARTSUPP_KEY', NULL, '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(454, NULL, NULL, 'SMARTSUPP_EMAIL', NULL, '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(455, NULL, NULL, 'SMARTSUPP_CUSTOMER_ID', '1', '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(456, NULL, NULL, 'SMARTSUPP_CUSTOMER_NAME', '1', '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(457, NULL, NULL, 'SMARTSUPP_CUSTOMER_EMAIL', '1', '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(458, NULL, NULL, 'SMARTSUPP_CUSTOMER_PHONE', '1', '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(459, NULL, NULL, 'SMARTSUPP_CUSTOMER_ROLE', '1', '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(460, NULL, NULL, 'SMARTSUPP_CUSTOMER_SPENDINGS', '1', '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(461, NULL, NULL, 'SMARTSUPP_CUSTOMER_ORDERS', '1', '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(462, NULL, NULL, 'SMARTSUPP_OPTIONAL_API', NULL, '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(463, NULL, NULL, 'MYSHOP_DATE', '2022-12-11', '2022-12-11 16:41:06', '2022-12-11 16:41:06'),
(464, NULL, NULL, 'PS_SHOWCASECARD_EMPLOYEES_CLOSED', '1', '2022-12-11 16:41:49', '2022-12-11 16:41:49');

-- --------------------------------------------------------

--
-- Structure de la table `ps_configuration_kpi`
--

DROP TABLE IF EXISTS `ps_configuration_kpi`;
CREATE TABLE IF NOT EXISTS `ps_configuration_kpi` (
  `id_configuration_kpi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) UNSIGNED DEFAULT NULL,
  `id_shop` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `value` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_configuration_kpi`),
  KEY `name` (`name`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_configuration_kpi`
--

INSERT INTO `ps_configuration_kpi` (`id_configuration_kpi`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(1, NULL, NULL, 'DASHGOALS_TRAFFIC_01_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(2, NULL, NULL, 'DASHGOALS_CONVERSION_01_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(3, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_01_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(4, NULL, NULL, 'DASHGOALS_TRAFFIC_02_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(5, NULL, NULL, 'DASHGOALS_CONVERSION_02_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(6, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_02_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(7, NULL, NULL, 'DASHGOALS_TRAFFIC_03_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(8, NULL, NULL, 'DASHGOALS_CONVERSION_03_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(9, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_03_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(10, NULL, NULL, 'DASHGOALS_TRAFFIC_04_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(11, NULL, NULL, 'DASHGOALS_CONVERSION_04_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(12, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_04_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(13, NULL, NULL, 'DASHGOALS_TRAFFIC_05_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(14, NULL, NULL, 'DASHGOALS_CONVERSION_05_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(15, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_05_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(16, NULL, NULL, 'DASHGOALS_TRAFFIC_06_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(17, NULL, NULL, 'DASHGOALS_CONVERSION_06_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(18, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_06_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(19, NULL, NULL, 'DASHGOALS_TRAFFIC_07_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(20, NULL, NULL, 'DASHGOALS_CONVERSION_07_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(21, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_07_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(22, NULL, NULL, 'DASHGOALS_TRAFFIC_08_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(23, NULL, NULL, 'DASHGOALS_CONVERSION_08_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(24, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_08_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(25, NULL, NULL, 'DASHGOALS_TRAFFIC_09_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(26, NULL, NULL, 'DASHGOALS_CONVERSION_09_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(27, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_09_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(28, NULL, NULL, 'DASHGOALS_TRAFFIC_10_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(29, NULL, NULL, 'DASHGOALS_CONVERSION_10_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(30, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_10_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(31, NULL, NULL, 'DASHGOALS_TRAFFIC_11_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(32, NULL, NULL, 'DASHGOALS_CONVERSION_11_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(33, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_11_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(34, NULL, NULL, 'DASHGOALS_TRAFFIC_12_2022', '600', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(35, NULL, NULL, 'DASHGOALS_CONVERSION_12_2022', '2', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(36, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_12_2022', '80', '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(37, NULL, NULL, 'TOP_CATEGORY', NULL, '2022-12-05 09:30:02', '2022-12-05 09:30:02'),
(38, NULL, NULL, 'TOP_CATEGORY_EXPIRE', NULL, '2022-12-05 09:30:02', '2022-12-05 09:30:02'),
(39, NULL, NULL, 'PRODUCTS_PER_CATEGORY', '2', '2022-12-05 09:30:02', '2022-12-05 09:30:02'),
(40, NULL, NULL, 'PRODUCTS_PER_CATEGORY_EXPIRE', '1670232602', '2022-12-05 09:30:02', '2022-12-05 09:30:02'),
(41, NULL, NULL, 'DISABLED_CATEGORIES', '0', '2022-12-05 09:30:02', '2022-12-05 09:30:02'),
(42, NULL, NULL, 'DISABLED_CATEGORIES_EXPIRE', '1670236202', '2022-12-05 09:30:02', '2022-12-05 09:30:02'),
(43, NULL, NULL, 'EMPTY_CATEGORIES', '0', '2022-12-05 09:30:03', '2022-12-05 09:30:03'),
(44, NULL, NULL, 'EMPTY_CATEGORIES_EXPIRE', '1670236203', '2022-12-05 09:30:03', '2022-12-05 09:30:03'),
(45, NULL, NULL, 'NETPROFIT_VISIT', '€0.00', '2022-12-05 10:23:19', '2022-12-05 10:23:19'),
(46, NULL, NULL, 'NETPROFIT_VISIT_EXPIRE', '1670281200', '2022-12-05 10:23:19', '2022-12-05 10:23:19'),
(47, NULL, NULL, 'ABANDONED_CARTS', '0', '2022-12-05 10:23:19', '2022-12-05 10:23:19'),
(48, NULL, NULL, 'ABANDONED_CARTS_EXPIRE', '1670235799', '2022-12-05 10:23:19', '2022-12-05 10:23:19'),
(49, NULL, NULL, 'AVG_ORDER_VALUE', '€0.00', '2022-12-05 10:23:19', '2022-12-05 10:23:19'),
(50, NULL, NULL, 'AVG_ORDER_VALUE_EXPIRE', '1670281200', '2022-12-05 10:23:19', '2022-12-05 10:23:19'),
(51, NULL, NULL, 'CONVERSION_RATE', '0%', '2022-12-05 10:23:19', '2022-12-05 10:23:19'),
(52, NULL, NULL, 'CONVERSION_RATE_EXPIRE', '1670281200', '2022-12-05 10:23:19', '2022-12-05 10:23:19'),
(53, NULL, NULL, 'NEWSLETTER_REGISTRATIONS', '1', '2022-12-05 14:44:37', '2022-12-05 14:44:37'),
(54, NULL, NULL, 'NEWSLETTER_REGISTRATIONS_EXPIRE', '1670269477', '2022-12-05 14:44:37', '2022-12-05 14:44:37'),
(55, NULL, NULL, 'CUSTOMER_MAIN_GENDER', NULL, '2022-12-05 14:44:37', '2022-12-05 14:44:37'),
(56, NULL, NULL, 'CUSTOMER_MAIN_GENDER_EXPIRE', NULL, '2022-12-05 14:44:37', '2022-12-05 14:44:37'),
(57, NULL, NULL, 'ORDERS_PER_CUSTOMER', '0', '2022-12-05 14:44:37', '2022-12-05 14:44:37'),
(58, NULL, NULL, 'ORDERS_PER_CUSTOMER_EXPIRE', '1670334277', '2022-12-05 14:44:37', '2022-12-05 14:44:37'),
(59, NULL, NULL, 'AVG_CUSTOMER_AGE', NULL, '2022-12-05 14:44:37', '2022-12-05 14:44:37'),
(60, NULL, NULL, 'AVG_CUSTOMER_AGE_EXPIRE', NULL, '2022-12-05 14:44:37', '2022-12-05 14:44:37'),
(61, NULL, NULL, 'PENDING_MESSAGES', '0', '2022-12-11 16:03:38', '2022-12-11 16:03:38'),
(62, NULL, NULL, 'PENDING_MESSAGES_EXPIRE', '1670771318', '2022-12-11 16:03:38', '2022-12-11 16:03:38'),
(63, NULL, NULL, 'MESSAGES_PER_THREAD', '0', '2022-12-11 16:03:38', '2022-12-11 16:03:38'),
(64, NULL, NULL, 'MESSAGES_PER_THREAD_EXPIRE', '1670814218', '2022-12-11 16:03:38', '2022-12-11 16:03:38'),
(65, NULL, NULL, 'AVG_MSG_RESPONSE_TIME', '0 hours', '2022-12-11 16:03:38', '2022-12-11 16:03:38'),
(66, NULL, NULL, 'AVG_MSG_RESPONSE_TIME_EXPIRE', '1670785418', '2022-12-11 16:03:38', '2022-12-11 16:03:38');

-- --------------------------------------------------------

--
-- Structure de la table `ps_configuration_kpi_lang`
--

DROP TABLE IF EXISTS `ps_configuration_kpi_lang`;
CREATE TABLE IF NOT EXISTS `ps_configuration_kpi_lang` (
  `id_configuration_kpi` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` text,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_configuration_kpi`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_configuration_kpi_lang`
--

INSERT INTO `ps_configuration_kpi_lang` (`id_configuration_kpi`, `id_lang`, `value`, `date_upd`) VALUES
(37, 1, 'Stationery', '2022-12-05 09:30:02'),
(37, 2, 'Stationery', '2022-12-05 09:30:02'),
(37, 3, 'Stationery', '2022-12-05 09:30:02'),
(37, 4, 'Stationery', '2022-12-05 09:30:02'),
(37, 5, 'Stationery', '2022-12-05 09:30:02'),
(37, 6, 'Stationery', '2022-12-05 09:30:02'),
(38, 1, '1670315402', '2022-12-05 09:30:02'),
(38, 2, '1670315402', '2022-12-05 09:30:02'),
(38, 3, '1670315402', '2022-12-05 09:30:02'),
(38, 4, '1670315402', '2022-12-05 09:30:02'),
(38, 5, '1670315402', '2022-12-05 09:30:02'),
(38, 6, '1670315402', '2022-12-05 09:30:02'),
(55, 1, '100% Male Customers', '2022-12-05 14:44:37'),
(56, 1, '1670334277', '2022-12-05 14:44:37'),
(59, 1, '53 years', '2022-12-05 14:44:37'),
(60, 1, '1670334277', '2022-12-05 14:44:37');

-- --------------------------------------------------------

--
-- Structure de la table `ps_configuration_lang`
--

DROP TABLE IF EXISTS `ps_configuration_lang`;
CREATE TABLE IF NOT EXISTS `ps_configuration_lang` (
  `id_configuration` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` text,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_configuration`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_configuration_lang`
--

INSERT INTO `ps_configuration_lang` (`id_configuration`, `id_lang`, `value`, `date_upd`) VALUES
(38, 1, '#IN', NULL),
(38, 2, '#FA', NULL),
(38, 3, 'RE', NULL),
(38, 4, '#IN', NULL),
(38, 5, '#FA', NULL),
(38, 6, '#FA', NULL),
(41, 1, '#DE', NULL),
(41, 2, '#LI', NULL),
(41, 3, 'LI', NULL),
(41, 4, '#DE', NULL),
(41, 5, '#SP', NULL),
(41, 6, '#EN', NULL),
(43, 1, '#RE', NULL),
(43, 2, '#RE', NULL),
(43, 3, 'RET', NULL),
(43, 4, '#RE', NULL),
(43, 5, '#RE', NULL),
(43, 6, '#DE', NULL),
(54, 1, 'a|about|above|after|again|against|all|am|an|and|any|are|aren|as|at|be|because|been|before|being|below|between|both|but|by|can|cannot|could|couldn|did|didn|do|does|doesn|doing|don|down|during|each|few|for|from|further|had|hadn|has|hasn|have|haven|having|he|ll|her|here|hers|herself|him|himself|his|how|ve|if|in|into|is|isn|it|its|itself|let|me|more|most|mustn|my|myself|no|nor|not|of|off|on|once|only|or|other|ought|our|ours|ourselves|out|over|own|same|shan|she|should|shouldn|so|some|such|than|that|the|their|theirs|them|themselves|then|there|these|they|re|this|those|through|to|too|under|until|up|very|was|wasn|we|were|weren|what|when|where|which|while|who|whom|why|with|won|would|wouldn|you|your|yours|yourself|yourselves', NULL),
(54, 2, 'alors|au|aucuns|aussi|autre|avant|avec|avoir|bon|car|ce|cela|ces|ceux|chaque|ci|comme|comment|dans|des|du|dedans|dehors|depuis|deux|devrait|doit|donc|dos|droite|début|elle|elles|en|encore|essai|est|et|eu|fait|faites|fois|font|force|haut|hors|ici|il|ils|je|juste|la|le|les|leur|là|ma|maintenant|mais|mes|mine|moins|mon|mot|même|ni|nommés|notre|nous|nouveaux|ou|où|par|parce|parole|pas|personnes|peut|peu|pièce|plupart|pour|pourquoi|quand|que|quel|quelle|quelles|quels|qui|sa|sans|ses|seulement|si|sien|son|sont|sous|soyez|sujet|sur|ta|tandis|tellement|tels|tes|ton|tous|tout|trop|très|tu|valeur|voie|voient|vont|votre|vous|vu|ça|étaient|état|étions|été|être', NULL),
(54, 3, 'aber|als|auch|auf|aus|bei|bin|bis|bist|dadurch|daher|darum|das|daß|dass|dein|deine|dem|den|der|des|dessen|deshalb|die|dies|dieser|dieses|doch|dort|durch|ein|eine|einem|einen|einer|eines|euer|eure|für|hatte|hatten|hattest|hattet|hier|hinter|ich|ihr|ihre|im|in|ist|ja|jede|jedem|jeden|jeder|jedes|jener|jenes|jetzt|kann|kannst|können|könnt|machen|mein|meine|muß|mußt|musst|müssen|müßt|nach|nachdem|nein|nicht|oder|seid|sein|seine|sich|sind|soll|sollen|sollst|sollt|sonst|soweit|sowie|und|unser|unsere|unter|vom|von|vor|wann|warum|was|weiter|weitere|wenn|wer|werde|werden|werdet|weshalb|wie|wieder|wieso|wir|wird|wirst|woher|wohin|zum|zur|über', NULL),
(54, 4, 'een|over|boven|na|weer|tegen|alles|ben|een|en|elke|zijn|zijn niet|als|op|zijn|omdat|geweest|voor|zijn|onder|tussen|beide|maar|door|kan|kan niet|kon|kon niet|deed|deed niet|doet|doet|doet niet|doen|doet niet|neer|tijdens|elk|weinig|voor|van|verder|had|had niet|heeft|heeft niet|hebben|hebben niet|hebben|hij|ll|haar|hier|haar|haarzelf|hem|hemzelf|zijn|hoe|ve|als|in|in|is|is niet|het|het|het zelf|laat|mij|meer|meest|moet niet|mijn|mijzelf|nee|noch|niet|van|uit|aan|eens|alleen|of|ander|zou moeten|ons|onze|ons zelf|out|over|eigen|zelfde|zal niet|zij|zou moeten|zou niet moeten|zo|iets|dergelijke|dan|dat|de|hun|hun|zij|zij zelf|dan|daar|deze|zij|onderwerp|dit|die|door|om|ook|onder|tot|omhoog|zeer|was|was niet|wij|waren|waren niet|wat|wanneer|waar|welke|terwijl|wie|van wie|waarom|met|gewonnen|zou|zou niet|jij|jouw|jouw|jij zelf|jullie zelf', NULL),
(54, 5, 'a|adesso|ai|al|alla|allo|allora|altre|altri|altro|anche|ancora|avere|aveva|avevano|ben|buono|che|chi|cinque|comprare|con|consecutivi|consecutivo|cosa|cui|da|del|della|dello|dentro|deve|devo|di|doppio|due|e|ecco|fare|fine|fino|fra|gente|giu|ha|hai|hanno|ho|il|indietro|invece|io|la|lavoro|le|lei|lo|loro|lui|lungo|ma|me|meglio|molta|molti|molto|nei|nella|no|noi|nome|nostro|nove|nuovi|nuovo|o|oltre|ora|otto|peggio|pero|persone|piu|poco|primo|promesso|qua|quarto|quasi|quattro|quello|questo|qui|quindi|quinto|rispetto|sara|secondo|sei|sembra|sembrava|senza|sette|sia|siamo|siete|solo|sono|sopra|soprattutto|sotto|stati|stato|stesso|su|subito|sul|sulla|tanto|te|tempo|terzo|tra|tre|triplo|ultimo|un|una|uno|va|vai|voi|volte|vostro', NULL),
(54, 6, 'a|sobre|encima|después|nuevamente|contra|todo|soy|un|y|ninguno|son|no|cuando|en|estar|porque|sido|antes|siendo|debajo|entre|ambos|pero|por|puede|podía|hizo|hacer|hace|haciendo|bajo|durante|cada|alguno|para|desde|más|tuvo|tiene|haber|habiendo|él|aquí|suyo|misma|su|mismo|cómo|si|en|dentro|es|eso|dejar|me|mayoría|mi|mismo|ni|desactivado|activado|solo|o|otro|nuestro|nuestros|mismos|fuera|propio|mismo|ella|debería|tal|que|el|sus|entonces|allí|estos|ellos|esos|aquellos|través|demasiado|hasta|arriba|muy|era|éramos|qué|cuándo|dónde|mientras|quién|con', NULL),
(80, 1, 'Dear Customer,\r\n\r\nRegards,\r\nCustomer service', NULL),
(80, 2, 'Dear Customer,\r\n\r\nRegards,\r\nCustomer service', NULL),
(80, 3, 'Dear Customer,\r\n\r\nRegards,\r\nCustomer service', NULL),
(80, 4, 'Dear Customer,\r\n\r\nRegards,\r\nCustomer service', NULL),
(80, 5, 'Dear Customer,\r\n\r\nRegards,\r\nCustomer service', NULL),
(80, 6, 'Dear Customer,\r\n\r\nRegards,\r\nCustomer service', NULL),
(281, 1, 'We are currently updating our shop and will be back really soon.\r\nThanks for your patience.', NULL),
(281, 2, 'We are currently updating our shop and will be back really soon.\r\nThanks for your patience.', NULL),
(281, 3, 'We are currently updating our shop and will be back really soon.\r\nThanks for your patience.', NULL),
(281, 4, 'We are currently updating our shop and will be back really soon.\r\nThanks for your patience.', NULL),
(281, 5, 'We are currently updating our shop and will be back really soon.\r\nThanks for your patience.', NULL),
(281, 6, 'We are currently updating our shop and will be back really soon.\r\nThanks for your patience.', NULL),
(283, 1, '', NULL),
(283, 2, '', NULL),
(283, 3, '', NULL),
(283, 4, '', NULL),
(283, 5, '', NULL),
(283, 6, '', NULL),
(284, 1, '', NULL),
(284, 2, '', NULL),
(284, 3, '', NULL),
(284, 4, '', NULL),
(284, 5, '', NULL),
(284, 6, '', NULL),
(285, 1, 'Out-of-Stock', NULL),
(285, 2, 'Rupture de stock', NULL),
(285, 3, 'Nicht auf Lager', NULL),
(285, 4, 'Niet op voorraad', NULL),
(285, 5, 'Non disponibile', NULL),
(285, 6, 'Fuera de stock', NULL),
(293, 1, 'My wishlists', '2022-11-21 11:03:44'),
(293, 2, 'My wishlists', '2022-11-21 11:03:44'),
(293, 3, 'My wishlists', '2022-11-21 11:03:44'),
(293, 4, 'My wishlists', '2022-11-21 11:03:44'),
(293, 5, 'My wishlists', '2022-11-21 11:03:44'),
(293, 6, 'My wishlists', '2022-11-21 11:03:44'),
(294, 1, 'My wishlist', '2022-11-21 11:03:44'),
(294, 2, 'My wishlist', '2022-11-21 11:03:44'),
(294, 3, 'My wishlist', '2022-11-21 11:03:44'),
(294, 4, 'My wishlist', '2022-11-21 11:03:44'),
(294, 5, 'My wishlist', '2022-11-21 11:03:44'),
(294, 6, 'My wishlist', '2022-11-21 11:03:44'),
(295, 1, 'Create new list', '2022-11-21 11:03:44'),
(295, 2, 'Create new list', '2022-11-21 11:03:44'),
(295, 3, 'Create new list', '2022-11-21 11:03:44'),
(295, 4, 'Create new list', '2022-11-21 11:03:44'),
(295, 5, 'Create new list', '2022-11-21 11:03:44'),
(295, 6, 'Create new list', '2022-11-21 11:03:44'),
(316, 1, 'a9bd10ff112ae78770070b428d83962b.jpg', '2022-12-05 11:11:28'),
(316, 2, 'sale70.png', '2022-11-21 11:03:46'),
(316, 3, 'sale70.png', '2022-11-21 11:03:46'),
(316, 4, 'sale70.png', '2022-11-21 11:03:46'),
(316, 5, 'sale70.png', '2022-12-05 11:00:04'),
(316, 6, 'a9bd10ff112ae78770070b428d83962b.jpg', '2022-12-05 13:24:50'),
(317, 1, '', '2022-11-21 11:03:46'),
(317, 2, '', '2022-11-21 11:03:46'),
(317, 3, '', '2022-11-21 11:03:46'),
(317, 4, '', '2022-11-21 11:03:46'),
(317, 5, '', '2022-12-05 11:00:04'),
(317, 6, '', '2022-12-05 13:24:50'),
(318, 1, '', '2022-11-21 11:03:46'),
(318, 2, '', '2022-11-21 11:03:46'),
(318, 3, '', '2022-11-21 11:03:46'),
(318, 4, '', '2022-11-21 11:03:46'),
(318, 5, '', '2022-12-05 11:00:04'),
(318, 6, '', '2022-12-05 13:24:50'),
(327, 1, 'The personal data you provide is used to answer queries, process orders or allow access to specific information. You have the right to modify and delete all the personal information found in the \"My Account\" page.', '2022-11-21 11:03:46'),
(327, 2, 'The personal data you provide is used to answer queries, process orders or allow access to specific information. You have the right to modify and delete all the personal information found in the \"My Account\" page.', '2022-11-21 11:03:46'),
(327, 3, 'The personal data you provide is used to answer queries, process orders or allow access to specific information. You have the right to modify and delete all the personal information found in the \"My Account\" page.', '2022-11-21 11:03:46'),
(327, 4, 'The personal data you provide is used to answer queries, process orders or allow access to specific information. You have the right to modify and delete all the personal information found in the \"My Account\" page.', '2022-11-21 11:03:46'),
(327, 5, 'The personal data you provide is used to answer queries, process orders or allow access to specific information. You have the right to modify and delete all the personal information found in the \"My Account\" page.', '2022-11-21 11:03:46'),
(327, 6, 'The personal data you provide is used to answer queries, process orders or allow access to specific information. You have the right to modify and delete all the personal information found in the \"My Account\" page.', '2022-11-21 11:03:46'),
(329, 1, 'You may unsubscribe at any moment. For that purpose, please find our contact info in the legal notice.', '2022-11-21 11:03:46'),
(329, 2, 'Vous pouvez vous désinscrire à tout moment. Vous trouverez pour cela nos informations de contact dans les conditions d\'utilisation du site.', '2022-11-21 11:03:46'),
(329, 3, 'Sie können Ihr Einverständnis jederzeit widerrufen. Unsere Kontaktinformationen finden Sie u. a. in der Datenschutzerklärung.', '2022-11-21 11:03:46'),
(329, 4, 'U kunt op elk gewenst moment weer uitschrijven. Hiervoor kunt u de contactgegevens gebruiken uit de algemene voorwaarden.', '2022-11-21 11:03:46'),
(329, 5, 'Puoi annullare l\'iscrizione in ogni momenti. A questo scopo, cerca le info di contatto nelle note legali.', '2022-11-21 11:03:46'),
(329, 6, 'Puede darse de baja en cualquier momento. Para ello, consulte nuestra información de contacto en el aviso legal.', '2022-11-21 11:03:46'),
(365, 1, 'I agree to the terms and conditions and the privacy policy', '2022-11-21 11:05:58'),
(365, 2, 'J\'accepte les conditions générales et la politique de confidentialité', '2022-11-21 11:05:58'),
(365, 3, 'Ich akzeptiere die Allgemeinen Geschäftsbedingungen und die Datenschutzrichtlinie', '2022-11-21 11:05:58'),
(365, 4, 'Ik accepteer de Algemene voorwaarden en het vertrouwelijkheidsbeleid', '2022-11-21 11:05:58'),
(365, 5, 'I agree to the terms and conditions and the privacy policy', '2022-11-21 11:05:58'),
(365, 6, 'I agree to the terms and conditions and the privacy policy', '2022-11-21 11:05:58'),
(367, 1, 'I agree to the terms and conditions and the privacy policy', '2022-11-21 11:05:59'),
(367, 2, 'J\'accepte les conditions générales et la politique de confidentialité', '2022-11-21 11:05:59'),
(367, 3, 'Ich akzeptiere die Allgemeinen Geschäftsbedingungen und die Datenschutzrichtlinie', '2022-11-21 11:05:59'),
(367, 4, 'Ik accepteer de Algemene voorwaarden en het vertrouwelijkheidsbeleid', '2022-11-21 11:05:59'),
(367, 5, 'I agree to the terms and conditions and the privacy policy', '2022-11-21 11:05:59'),
(367, 6, 'I agree to the terms and conditions and the privacy policy', '2022-11-21 11:05:59');

-- --------------------------------------------------------

--
-- Structure de la table `ps_connections`
--

DROP TABLE IF EXISTS `ps_connections`;
CREATE TABLE IF NOT EXISTS `ps_connections` (
  `id_connections` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_guest` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `ip_address` bigint(20) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_connections`),
  KEY `id_guest` (`id_guest`),
  KEY `date_add` (`date_add`),
  KEY `id_page` (`id_page`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_connections`
--

INSERT INTO `ps_connections` (`id_connections`, `id_shop_group`, `id_shop`, `id_guest`, `id_page`, `ip_address`, `date_add`, `http_referer`) VALUES
(1, 1, 1, 1, 1, 2130706433, '2022-11-21 11:07:25', 'https://www.prestashop.com'),
(2, 1, 1, 3, 1, 0, '2022-11-21 11:12:12', ''),
(3, 1, 1, 3, 2, 0, '2022-11-21 14:12:18', ''),
(4, 1, 1, 3, 1, 0, '2022-12-05 09:02:55', ''),
(5, 1, 1, 3, 1, 0, '2022-12-05 09:46:34', ''),
(6, 1, 1, 3, 1, 0, '2022-12-05 11:30:50', ''),
(7, 1, 1, 3, 2, 0, '2022-12-05 14:22:29', ''),
(8, 1, 1, 3, 1, 0, '2022-12-11 15:46:44', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_connections_page`
--

DROP TABLE IF EXISTS `ps_connections_page`;
CREATE TABLE IF NOT EXISTS `ps_connections_page` (
  `id_connections` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id_connections`,`id_page`,`time_start`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_connections_source`
--

DROP TABLE IF EXISTS `ps_connections_source`;
CREATE TABLE IF NOT EXISTS `ps_connections_source` (
  `id_connections_source` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_connections` int(10) UNSIGNED NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `request_uri` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_connections_source`),
  KEY `connections` (`id_connections`),
  KEY `orderby` (`date_add`),
  KEY `http_referer` (`http_referer`),
  KEY `request_uri` (`request_uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_contact`
--

DROP TABLE IF EXISTS `ps_contact`;
CREATE TABLE IF NOT EXISTS `ps_contact` (
  `id_contact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `customer_service` tinyint(1) NOT NULL DEFAULT '0',
  `position` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_contact`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_contact`
--

INSERT INTO `ps_contact` (`id_contact`, `email`, `customer_service`, `position`) VALUES
(1, 'test@test.com', 1, 0),
(2, 'test@test.com', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_contact_lang`
--

DROP TABLE IF EXISTS `ps_contact_lang`;
CREATE TABLE IF NOT EXISTS `ps_contact_lang` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`id_contact`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_contact_lang`
--

INSERT INTO `ps_contact_lang` (`id_contact`, `id_lang`, `name`, `description`) VALUES
(1, 1, 'Webmaster', 'If a technical problem occurs on this website'),
(1, 2, 'Webmaster', 'En cas de problème technique sur ce site'),
(1, 3, 'Webmaster', 'Falls ein technisches Problem auf der Webseite auftritt'),
(1, 4, 'Webmaster', 'Als er zich een technisch probleem voordoet op deze website'),
(1, 5, 'Webmaster', 'Se si verifica un problema tecnico su questo sito web'),
(1, 6, 'Webmaster', 'Si se produce un problema técnico en este sitio web'),
(2, 1, 'Customer service', 'For any question about a product, an order'),
(2, 2, 'Service client', 'Pour toute question sur un produit ou une commande'),
(2, 3, 'Kundenservice', 'Bei Fragen zu einem Artikel oder einer Bestellung'),
(2, 4, 'Klantenservice', 'Voor vragen over een product, een bestelling'),
(2, 5, 'Servizio clienti', 'Per qualsiasi domanda su un prodotto, un ordine'),
(2, 6, 'Servicio al cliente', 'Para cualquier pregunta sobre un artículo o un pedido');

-- --------------------------------------------------------

--
-- Structure de la table `ps_contact_shop`
--

DROP TABLE IF EXISTS `ps_contact_shop`;
CREATE TABLE IF NOT EXISTS `ps_contact_shop` (
  `id_contact` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_contact`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_contact_shop`
--

INSERT INTO `ps_contact_shop` (`id_contact`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_country`
--

DROP TABLE IF EXISTS `ps_country`;
CREATE TABLE IF NOT EXISTS `ps_country` (
  `id_country` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_zone` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `iso_code` varchar(3) NOT NULL,
  `call_prefix` int(10) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `contains_states` tinyint(1) NOT NULL DEFAULT '0',
  `need_identification_number` tinyint(1) NOT NULL DEFAULT '0',
  `need_zip_code` tinyint(1) NOT NULL DEFAULT '1',
  `zip_code_format` varchar(12) NOT NULL DEFAULT '',
  `display_tax_label` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_country`),
  KEY `country_iso_code` (`iso_code`),
  KEY `country_` (`id_zone`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_country`
--

INSERT INTO `ps_country` (`id_country`, `id_zone`, `id_currency`, `iso_code`, `call_prefix`, `active`, `contains_states`, `need_identification_number`, `need_zip_code`, `zip_code_format`, `display_tax_label`) VALUES
(1, 1, 0, 'DE', 49, 0, 0, 0, 1, 'NNNNN', 1),
(2, 1, 0, 'AT', 43, 0, 0, 0, 1, 'NNNN', 1),
(3, 1, 0, 'BE', 32, 1, 0, 0, 1, 'NNNN', 1),
(4, 2, 0, 'CA', 1, 0, 1, 0, 1, 'LNL NLN', 0),
(5, 3, 0, 'CN', 86, 0, 0, 0, 1, 'NNNNNN', 1),
(6, 1, 0, 'ES', 34, 1, 1, 1, 1, 'NNNNN', 1),
(7, 1, 0, 'FI', 358, 0, 0, 0, 1, 'NNNNN', 1),
(8, 1, 0, 'FR', 33, 0, 0, 0, 1, 'NNNNN', 1),
(9, 1, 0, 'GR', 30, 0, 0, 0, 1, 'NNNNN', 1),
(10, 1, 0, 'IT', 39, 1, 1, 0, 1, 'NNNNN', 1),
(11, 3, 0, 'JP', 81, 0, 1, 0, 1, 'NNN-NNNN', 1),
(12, 1, 0, 'LU', 352, 0, 0, 0, 1, 'NNNN', 1),
(13, 1, 0, 'NL', 31, 0, 0, 0, 1, 'NNNN LL', 1),
(14, 1, 0, 'PL', 48, 0, 0, 0, 1, 'NN-NNN', 1),
(15, 1, 0, 'PT', 351, 0, 0, 0, 1, 'NNNN-NNN', 1),
(16, 1, 0, 'CZ', 420, 0, 0, 0, 1, 'NNN NN', 1),
(17, 7, 0, 'GB', 44, 0, 0, 0, 1, '', 1),
(18, 1, 0, 'SE', 46, 0, 0, 0, 1, 'NNN NN', 1),
(19, 7, 0, 'CH', 41, 0, 0, 0, 1, 'NNNN', 1),
(20, 1, 0, 'DK', 45, 0, 0, 0, 1, 'NNNN', 1),
(21, 2, 0, 'US', 1, 0, 1, 0, 1, 'NNNNN', 0),
(22, 3, 0, 'HK', 852, 0, 0, 0, 0, '', 1),
(23, 7, 0, 'NO', 47, 0, 0, 0, 1, 'NNNN', 1),
(24, 5, 0, 'AU', 61, 0, 1, 0, 1, 'NNNN', 1),
(25, 3, 0, 'SG', 65, 0, 0, 0, 1, 'NNNNNN', 1),
(26, 1, 0, 'IE', 353, 0, 0, 0, 0, '', 1),
(27, 5, 0, 'NZ', 64, 0, 0, 0, 1, 'NNNN', 1),
(28, 3, 0, 'KR', 82, 0, 0, 0, 1, 'NNNNN', 1),
(29, 3, 0, 'IL', 972, 0, 0, 0, 1, 'NNNNNNN', 1),
(30, 4, 0, 'ZA', 27, 0, 0, 0, 1, 'NNNN', 1),
(31, 4, 0, 'NG', 234, 0, 0, 0, 1, '', 1),
(32, 4, 0, 'CI', 225, 0, 0, 0, 1, '', 1),
(33, 4, 0, 'TG', 228, 0, 0, 0, 1, '', 1),
(34, 6, 0, 'BO', 591, 0, 0, 0, 1, '', 1),
(35, 4, 0, 'MU', 230, 0, 0, 0, 1, '', 1),
(36, 1, 0, 'RO', 40, 0, 0, 0, 1, 'NNNNNN', 1),
(37, 1, 0, 'SK', 421, 0, 0, 0, 1, 'NNN NN', 1),
(38, 4, 0, 'DZ', 213, 0, 0, 0, 1, 'NNNNN', 1),
(39, 2, 0, 'AS', 0, 0, 0, 0, 1, '', 1),
(40, 7, 0, 'AD', 376, 0, 0, 0, 1, 'CNNN', 1),
(41, 4, 0, 'AO', 244, 0, 0, 0, 0, '', 1),
(42, 8, 0, 'AI', 0, 0, 0, 0, 1, '', 1),
(43, 2, 0, 'AG', 0, 0, 0, 0, 1, '', 1),
(44, 6, 0, 'AR', 54, 0, 1, 0, 1, 'LNNNNLLL', 1),
(45, 3, 0, 'AM', 374, 0, 0, 0, 1, 'NNNN', 1),
(46, 8, 0, 'AW', 297, 0, 0, 0, 1, '', 1),
(47, 3, 0, 'AZ', 994, 0, 0, 0, 1, 'CNNNN', 1),
(48, 2, 0, 'BS', 0, 0, 0, 0, 1, '', 1),
(49, 3, 0, 'BH', 973, 0, 0, 0, 1, '', 1),
(50, 3, 0, 'BD', 880, 0, 0, 0, 1, 'NNNN', 1),
(51, 2, 0, 'BB', 0, 0, 0, 0, 1, 'CNNNNN', 1),
(52, 7, 0, 'BY', 0, 0, 0, 0, 1, 'NNNNNN', 1),
(53, 8, 0, 'BZ', 501, 0, 0, 0, 0, '', 1),
(54, 4, 0, 'BJ', 229, 0, 0, 0, 0, '', 1),
(55, 2, 0, 'BM', 0, 0, 0, 0, 1, '', 1),
(56, 3, 0, 'BT', 975, 0, 0, 0, 1, '', 1),
(57, 4, 0, 'BW', 267, 0, 0, 0, 1, '', 1),
(58, 6, 0, 'BR', 55, 0, 0, 0, 1, 'NNNNN-NNN', 1),
(59, 3, 0, 'BN', 673, 0, 0, 0, 1, 'LLNNNN', 1),
(60, 4, 0, 'BF', 226, 0, 0, 0, 1, '', 1),
(61, 3, 0, 'MM', 95, 0, 0, 0, 1, '', 1),
(62, 4, 0, 'BI', 257, 0, 0, 0, 1, '', 1),
(63, 3, 0, 'KH', 855, 0, 0, 0, 1, 'NNNNN', 1),
(64, 4, 0, 'CM', 237, 0, 0, 0, 1, '', 1),
(65, 4, 0, 'CV', 238, 0, 0, 0, 1, 'NNNN', 1),
(66, 4, 0, 'CF', 236, 0, 0, 0, 1, '', 1),
(67, 4, 0, 'TD', 235, 0, 0, 0, 1, '', 1),
(68, 6, 0, 'CL', 56, 0, 0, 0, 1, 'NNN-NNNN', 1),
(69, 6, 0, 'CO', 57, 0, 0, 0, 1, 'NNNNNN', 1),
(70, 4, 0, 'KM', 269, 0, 0, 0, 1, '', 1),
(71, 4, 0, 'CD', 243, 0, 0, 0, 1, '', 1),
(72, 4, 0, 'CG', 242, 0, 0, 0, 1, '', 1),
(73, 8, 0, 'CR', 506, 0, 0, 0, 1, 'NNNNN', 1),
(74, 1, 0, 'HR', 385, 0, 0, 0, 1, 'NNNNN', 1),
(75, 8, 0, 'CU', 53, 0, 0, 0, 1, '', 1),
(76, 1, 0, 'CY', 357, 0, 0, 0, 1, 'NNNN', 1),
(77, 4, 0, 'DJ', 253, 0, 0, 0, 1, '', 1),
(78, 8, 0, 'DM', 0, 0, 0, 0, 1, '', 1),
(79, 8, 0, 'DO', 0, 0, 0, 0, 1, '', 1),
(80, 3, 0, 'TL', 670, 0, 0, 0, 1, '', 1),
(81, 6, 0, 'EC', 593, 0, 0, 0, 1, 'CNNNNNN', 1),
(82, 4, 0, 'EG', 20, 0, 0, 0, 1, 'NNNNN', 1),
(83, 8, 0, 'SV', 503, 0, 0, 0, 1, '', 1),
(84, 4, 0, 'GQ', 240, 0, 0, 0, 1, '', 1),
(85, 4, 0, 'ER', 291, 0, 0, 0, 1, '', 1),
(86, 1, 0, 'EE', 372, 0, 0, 0, 1, 'NNNNN', 1),
(87, 4, 0, 'ET', 251, 0, 0, 0, 1, '', 1),
(88, 8, 0, 'FK', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(89, 7, 0, 'FO', 298, 0, 0, 0, 1, '', 1),
(90, 5, 0, 'FJ', 679, 0, 0, 0, 1, '', 1),
(91, 4, 0, 'GA', 241, 0, 0, 0, 1, '', 1),
(92, 4, 0, 'GM', 220, 0, 0, 0, 1, '', 1),
(93, 3, 0, 'GE', 995, 0, 0, 0, 1, 'NNNN', 1),
(94, 4, 0, 'GH', 233, 0, 0, 0, 1, '', 1),
(95, 8, 0, 'GD', 0, 0, 0, 0, 1, '', 1),
(96, 7, 0, 'GL', 299, 0, 0, 0, 1, '', 1),
(97, 7, 0, 'GI', 350, 0, 0, 0, 1, '', 1),
(98, 8, 0, 'GP', 590, 0, 0, 0, 1, '', 1),
(99, 5, 0, 'GU', 0, 0, 0, 0, 1, '', 1),
(100, 8, 0, 'GT', 502, 0, 0, 0, 1, '', 1),
(101, 7, 0, 'GG', 0, 0, 0, 0, 1, 'LLN NLL', 1),
(102, 4, 0, 'GN', 224, 0, 0, 0, 1, '', 1),
(103, 4, 0, 'GW', 245, 0, 0, 0, 1, '', 1),
(104, 6, 0, 'GY', 592, 0, 0, 0, 1, '', 1),
(105, 8, 0, 'HT', 509, 0, 0, 0, 1, '', 1),
(106, 7, 0, 'VA', 379, 0, 0, 0, 1, 'NNNNN', 1),
(107, 8, 0, 'HN', 504, 0, 0, 0, 1, '', 1),
(108, 7, 0, 'IS', 354, 0, 0, 0, 1, 'NNN', 1),
(109, 3, 0, 'IN', 91, 0, 1, 0, 1, 'NNN NNN', 1),
(110, 3, 0, 'ID', 62, 0, 1, 0, 1, 'NNNNN', 1),
(111, 3, 0, 'IR', 98, 0, 0, 0, 1, 'NNNNN-NNNNN', 1),
(112, 3, 0, 'IQ', 964, 0, 0, 0, 1, 'NNNNN', 1),
(113, 7, 0, 'IM', 0, 0, 0, 0, 1, 'CN NLL', 1),
(114, 8, 0, 'JM', 0, 0, 0, 0, 1, '', 1),
(115, 7, 0, 'JE', 0, 0, 0, 0, 1, 'CN NLL', 1),
(116, 3, 0, 'JO', 962, 0, 0, 0, 1, '', 1),
(117, 3, 0, 'KZ', 7, 0, 0, 0, 1, 'NNNNNN', 1),
(118, 4, 0, 'KE', 254, 0, 0, 0, 1, '', 1),
(119, 5, 0, 'KI', 686, 0, 0, 0, 1, '', 1),
(120, 3, 0, 'KP', 850, 0, 0, 0, 1, '', 1),
(121, 3, 0, 'KW', 965, 0, 0, 0, 1, '', 1),
(122, 3, 0, 'KG', 996, 0, 0, 0, 1, '', 1),
(123, 3, 0, 'LA', 856, 0, 0, 0, 1, '', 1),
(124, 1, 0, 'LV', 371, 0, 0, 0, 1, 'C-NNNN', 1),
(125, 3, 0, 'LB', 961, 0, 0, 0, 1, '', 1),
(126, 4, 0, 'LS', 266, 0, 0, 0, 1, '', 1),
(127, 4, 0, 'LR', 231, 0, 0, 0, 1, '', 1),
(128, 4, 0, 'LY', 218, 0, 0, 0, 1, '', 1),
(129, 7, 0, 'LI', 423, 0, 0, 0, 1, 'NNNN', 1),
(130, 1, 0, 'LT', 370, 0, 0, 0, 1, 'NNNNN', 1),
(131, 3, 0, 'MO', 853, 0, 0, 0, 0, '', 1),
(132, 7, 0, 'MK', 389, 0, 0, 0, 1, '', 1),
(133, 4, 0, 'MG', 261, 0, 0, 0, 1, '', 1),
(134, 4, 0, 'MW', 265, 0, 0, 0, 1, '', 1),
(135, 3, 0, 'MY', 60, 0, 0, 0, 1, 'NNNNN', 1),
(136, 3, 0, 'MV', 960, 0, 0, 0, 1, '', 1),
(137, 4, 0, 'ML', 223, 0, 0, 0, 1, '', 1),
(138, 1, 0, 'MT', 356, 0, 0, 0, 1, 'LLL NNNN', 1),
(139, 5, 0, 'MH', 692, 0, 0, 0, 1, '', 1),
(140, 8, 0, 'MQ', 596, 0, 0, 0, 1, '', 1),
(141, 4, 0, 'MR', 222, 0, 0, 0, 1, '', 1),
(142, 1, 0, 'HU', 36, 0, 0, 0, 1, 'NNNN', 1),
(143, 4, 0, 'YT', 262, 0, 0, 0, 1, '', 1),
(144, 2, 0, 'MX', 52, 0, 1, 1, 1, 'NNNNN', 1),
(145, 5, 0, 'FM', 691, 0, 0, 0, 1, '', 1),
(146, 7, 0, 'MD', 373, 0, 0, 0, 1, 'C-NNNN', 1),
(147, 7, 0, 'MC', 377, 0, 0, 0, 1, '980NN', 1),
(148, 3, 0, 'MN', 976, 0, 0, 0, 1, '', 1),
(149, 7, 0, 'ME', 382, 0, 0, 0, 1, 'NNNNN', 1),
(150, 8, 0, 'MS', 0, 0, 0, 0, 1, '', 1),
(151, 4, 0, 'MA', 212, 0, 0, 0, 1, 'NNNNN', 1),
(152, 4, 0, 'MZ', 258, 0, 0, 0, 1, '', 1),
(153, 4, 0, 'NA', 264, 0, 0, 0, 1, '', 1),
(154, 5, 0, 'NR', 674, 0, 0, 0, 1, '', 1),
(155, 3, 0, 'NP', 977, 0, 0, 0, 1, '', 1),
(156, 5, 0, 'NC', 687, 0, 0, 0, 1, '', 1),
(157, 8, 0, 'NI', 505, 0, 0, 0, 1, 'NNNNNN', 1),
(158, 4, 0, 'NE', 227, 0, 0, 0, 1, '', 1),
(159, 5, 0, 'NU', 683, 0, 0, 0, 1, '', 1),
(160, 5, 0, 'NF', 0, 0, 0, 0, 1, '', 1),
(161, 5, 0, 'MP', 0, 0, 0, 0, 1, '', 1),
(162, 3, 0, 'OM', 968, 0, 0, 0, 1, '', 1),
(163, 3, 0, 'PK', 92, 0, 0, 0, 1, '', 1),
(164, 5, 0, 'PW', 680, 0, 0, 0, 1, '', 1),
(165, 3, 0, 'PS', 0, 0, 0, 0, 1, '', 1),
(166, 8, 0, 'PA', 507, 0, 0, 0, 1, 'NNNNNN', 1),
(167, 5, 0, 'PG', 675, 0, 0, 0, 1, '', 1),
(168, 6, 0, 'PY', 595, 0, 0, 0, 1, '', 1),
(169, 6, 0, 'PE', 51, 0, 0, 0, 1, '', 1),
(170, 3, 0, 'PH', 63, 0, 0, 0, 1, 'NNNN', 1),
(171, 5, 0, 'PN', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(172, 8, 0, 'PR', 0, 0, 0, 0, 1, 'NNNNN', 1),
(173, 3, 0, 'QA', 974, 0, 0, 0, 1, '', 1),
(174, 4, 0, 'RE', 262, 0, 0, 0, 1, '', 1),
(175, 7, 0, 'RU', 7, 0, 0, 0, 1, 'NNNNNN', 1),
(176, 4, 0, 'RW', 250, 0, 0, 0, 1, '', 1),
(177, 8, 0, 'BL', 0, 0, 0, 0, 1, '', 1),
(178, 8, 0, 'KN', 0, 0, 0, 0, 1, '', 1),
(179, 8, 0, 'LC', 0, 0, 0, 0, 1, '', 1),
(180, 8, 0, 'MF', 0, 0, 0, 0, 1, '', 1),
(181, 8, 0, 'PM', 508, 0, 0, 0, 1, '', 1),
(182, 8, 0, 'VC', 0, 0, 0, 0, 1, '', 1),
(183, 5, 0, 'WS', 685, 0, 0, 0, 1, '', 1),
(184, 7, 0, 'SM', 378, 0, 0, 0, 1, 'NNNNN', 1),
(185, 4, 0, 'ST', 239, 0, 0, 0, 1, '', 1),
(186, 3, 0, 'SA', 966, 0, 0, 0, 1, '', 1),
(187, 4, 0, 'SN', 221, 0, 0, 0, 1, '', 1),
(188, 7, 0, 'RS', 381, 0, 0, 0, 1, 'NNNNN', 1),
(189, 4, 0, 'SC', 248, 0, 0, 0, 1, '', 1),
(190, 4, 0, 'SL', 232, 0, 0, 0, 1, '', 1),
(191, 1, 0, 'SI', 386, 0, 0, 0, 1, 'C-NNNN', 1),
(192, 5, 0, 'SB', 677, 0, 0, 0, 1, '', 1),
(193, 4, 0, 'SO', 252, 0, 0, 0, 1, '', 1),
(194, 8, 0, 'GS', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(195, 3, 0, 'LK', 94, 0, 0, 0, 1, 'NNNNN', 1),
(196, 4, 0, 'SD', 249, 0, 0, 0, 1, '', 1),
(197, 8, 0, 'SR', 597, 0, 0, 0, 1, '', 1),
(198, 7, 0, 'SJ', 0, 0, 0, 0, 1, '', 1),
(199, 4, 0, 'SZ', 268, 0, 0, 0, 1, '', 1),
(200, 3, 0, 'SY', 963, 0, 0, 0, 1, '', 1),
(201, 3, 0, 'TW', 886, 0, 0, 0, 1, 'NNNNN', 1),
(202, 3, 0, 'TJ', 992, 0, 0, 0, 1, '', 1),
(203, 4, 0, 'TZ', 255, 0, 0, 0, 1, '', 1),
(204, 3, 0, 'TH', 66, 0, 0, 0, 1, 'NNNNN', 1),
(205, 5, 0, 'TK', 690, 0, 0, 0, 1, '', 1),
(206, 5, 0, 'TO', 676, 0, 0, 0, 1, '', 1),
(207, 6, 0, 'TT', 0, 0, 0, 0, 1, '', 1),
(208, 4, 0, 'TN', 216, 0, 0, 0, 1, '', 1),
(209, 7, 0, 'TR', 90, 0, 0, 0, 1, 'NNNNN', 1),
(210, 3, 0, 'TM', 993, 0, 0, 0, 1, '', 1),
(211, 8, 0, 'TC', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(212, 5, 0, 'TV', 688, 0, 0, 0, 1, '', 1),
(213, 4, 0, 'UG', 256, 0, 0, 0, 1, '', 1),
(214, 7, 0, 'UA', 380, 0, 0, 0, 1, 'NNNNN', 1),
(215, 3, 0, 'AE', 971, 0, 0, 0, 1, '', 1),
(216, 6, 0, 'UY', 598, 0, 0, 0, 1, '', 1),
(217, 3, 0, 'UZ', 998, 0, 0, 0, 1, '', 1),
(218, 5, 0, 'VU', 678, 0, 0, 0, 1, '', 1),
(219, 6, 0, 'VE', 58, 0, 0, 0, 1, '', 1),
(220, 3, 0, 'VN', 84, 0, 0, 0, 1, 'NNNNNN', 1),
(221, 2, 0, 'VG', 0, 0, 0, 0, 1, 'CNNNN', 1),
(222, 2, 0, 'VI', 0, 0, 0, 0, 1, '', 1),
(223, 5, 0, 'WF', 681, 0, 0, 0, 1, '', 1),
(224, 4, 0, 'EH', 0, 0, 0, 0, 1, '', 1),
(225, 3, 0, 'YE', 967, 0, 0, 0, 1, '', 1),
(226, 4, 0, 'ZM', 260, 0, 0, 0, 1, '', 1),
(227, 4, 0, 'ZW', 263, 0, 0, 0, 1, '', 1),
(228, 7, 0, 'AL', 355, 0, 0, 0, 1, 'NNNN', 1),
(229, 3, 0, 'AF', 93, 0, 0, 0, 1, 'NNNN', 1),
(230, 5, 0, 'AQ', 0, 0, 0, 0, 1, '', 1),
(231, 7, 0, 'BA', 387, 0, 0, 0, 1, '', 1),
(232, 5, 0, 'IO', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(233, 1, 0, 'BG', 359, 0, 0, 0, 1, 'NNNN', 1),
(234, 8, 0, 'KY', 0, 0, 0, 0, 1, '', 1),
(235, 3, 0, 'CX', 0, 0, 0, 0, 1, '', 1),
(236, 3, 0, 'CC', 0, 0, 0, 0, 1, '', 1),
(237, 5, 0, 'CK', 682, 0, 0, 0, 1, '', 1),
(238, 6, 0, 'GF', 594, 0, 0, 0, 1, '', 1),
(239, 5, 0, 'PF', 689, 0, 0, 0, 1, '', 1),
(240, 5, 0, 'TF', 0, 0, 0, 0, 1, '', 1),
(241, 7, 0, 'AX', 0, 0, 0, 0, 1, 'NNNNN', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_country_lang`
--

DROP TABLE IF EXISTS `ps_country_lang`;
CREATE TABLE IF NOT EXISTS `ps_country_lang` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_country`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_country_lang`
--

INSERT INTO `ps_country_lang` (`id_country`, `id_lang`, `name`) VALUES
(1, 1, 'Germany'),
(1, 2, 'Allemagne'),
(1, 3, 'Deutschland'),
(1, 4, 'Duitsland'),
(1, 5, 'Germania'),
(1, 6, 'Alemania'),
(2, 1, 'Austria'),
(2, 2, 'Autriche'),
(2, 3, 'Österreich'),
(2, 4, 'Oostenrijk'),
(2, 5, 'Austria'),
(2, 6, 'Austria'),
(3, 1, 'Belgium'),
(3, 2, 'Belgique'),
(3, 3, 'Belgien'),
(3, 4, 'België'),
(3, 5, 'Belgio'),
(3, 6, 'Bélgica'),
(4, 1, 'Canada'),
(4, 2, 'Canada'),
(4, 3, 'Kanada'),
(4, 4, 'Canada'),
(4, 5, 'Canada'),
(4, 6, 'Canadá'),
(5, 1, 'China'),
(5, 2, 'Chine'),
(5, 3, 'China'),
(5, 4, 'China'),
(5, 5, 'Cina'),
(5, 6, 'China'),
(6, 1, 'Spain'),
(6, 2, 'Espagne'),
(6, 3, 'Spanien'),
(6, 4, 'Spanje'),
(6, 5, 'Spagna'),
(6, 6, 'España'),
(7, 1, 'Finland'),
(7, 2, 'Finlande'),
(7, 3, 'Finnland'),
(7, 4, 'Finland'),
(7, 5, 'Finlandia'),
(7, 6, 'Finlandia'),
(8, 1, 'France'),
(8, 2, 'France'),
(8, 3, 'Frankreich'),
(8, 4, 'Frankrijk'),
(8, 5, 'Francia'),
(8, 6, 'Francia'),
(9, 1, 'Greece'),
(9, 2, 'Grèce'),
(9, 3, 'Griechenland'),
(9, 4, 'Griekenland'),
(9, 5, 'Grecia'),
(9, 6, 'Grecia'),
(10, 1, 'Italy'),
(10, 2, 'Italie'),
(10, 3, 'Italien'),
(10, 4, 'Italië'),
(10, 5, 'Italia'),
(10, 6, 'Italia'),
(11, 1, 'Japan'),
(11, 2, 'Japon'),
(11, 3, 'Japan'),
(11, 4, 'Japan'),
(11, 5, 'Giappone'),
(11, 6, 'Japón'),
(12, 1, 'Luxembourg'),
(12, 2, 'Luxembourg'),
(12, 3, 'Luxemburg'),
(12, 4, 'Luxemburg'),
(12, 5, 'Lussemburgo'),
(12, 6, 'Luxemburgo'),
(13, 1, 'Netherlands'),
(13, 2, 'Pays-Bas'),
(13, 3, 'Niederlande'),
(13, 4, 'Nederland'),
(13, 5, 'Paesi Bassi'),
(13, 6, 'Países Bajos'),
(14, 1, 'Poland'),
(14, 2, 'Pologne'),
(14, 3, 'Polen'),
(14, 4, 'Polen'),
(14, 5, 'Polonia'),
(14, 6, 'Polonia'),
(15, 1, 'Portugal'),
(15, 2, 'Portugal'),
(15, 3, 'Portugal'),
(15, 4, 'Portugal'),
(15, 5, 'Portogallo'),
(15, 6, 'Portugal'),
(16, 1, 'Czechia'),
(16, 2, 'Tchéquie'),
(16, 3, 'Tschechien'),
(16, 4, 'Tsjechië'),
(16, 5, 'Cechia'),
(16, 6, 'Chequia'),
(17, 1, 'United Kingdom'),
(17, 2, 'Royaume-Uni'),
(17, 3, 'Vereinigtes Königreich'),
(17, 4, 'Verenigd Koninkrijk'),
(17, 5, 'Regno Unito'),
(17, 6, 'Reino Unido'),
(18, 1, 'Sweden'),
(18, 2, 'Suède'),
(18, 3, 'Schweden'),
(18, 4, 'Zweden'),
(18, 5, 'Svezia'),
(18, 6, 'Suecia'),
(19, 1, 'Switzerland'),
(19, 2, 'Suisse'),
(19, 3, 'Schweiz'),
(19, 4, 'Zwitserland'),
(19, 5, 'Svizzera'),
(19, 6, 'Suiza'),
(20, 1, 'Denmark'),
(20, 2, 'Danemark'),
(20, 3, 'Dänemark'),
(20, 4, 'Denemarken'),
(20, 5, 'Danimarca'),
(20, 6, 'Dinamarca'),
(21, 1, 'United States'),
(21, 2, 'États-Unis'),
(21, 3, 'Vereinigte Staaten'),
(21, 4, 'Verenigde Staten'),
(21, 5, 'Stati Uniti'),
(21, 6, 'Estados Unidos'),
(22, 1, 'Hong Kong SAR China'),
(22, 2, 'R.A.S. chinoise de Hong Kong'),
(22, 3, 'Sonderverwaltungsregion Hongkong'),
(22, 4, 'Hongkong SAR van China'),
(22, 5, 'RAS di Hong Kong'),
(22, 6, 'RAE de Hong Kong (China)'),
(23, 1, 'Norway'),
(23, 2, 'Norvège'),
(23, 3, 'Norwegen'),
(23, 4, 'Noorwegen'),
(23, 5, 'Norvegia'),
(23, 6, 'Noruega'),
(24, 1, 'Australia'),
(24, 2, 'Australie'),
(24, 3, 'Australien'),
(24, 4, 'Australië'),
(24, 5, 'Australia'),
(24, 6, 'Australia'),
(25, 1, 'Singapore'),
(25, 2, 'Singapour'),
(25, 3, 'Singapur'),
(25, 4, 'Singapore'),
(25, 5, 'Singapore'),
(25, 6, 'Singapur'),
(26, 1, 'Ireland'),
(26, 2, 'Irlande'),
(26, 3, 'Irland'),
(26, 4, 'Ierland'),
(26, 5, 'Irlanda'),
(26, 6, 'Irlanda'),
(27, 1, 'New Zealand'),
(27, 2, 'Nouvelle-Zélande'),
(27, 3, 'Neuseeland'),
(27, 4, 'Nieuw-Zeeland'),
(27, 5, 'Nuova Zelanda'),
(27, 6, 'Nueva Zelanda'),
(28, 1, 'South Korea'),
(28, 2, 'Corée du Sud'),
(28, 3, 'Südkorea'),
(28, 4, 'Zuid-Korea'),
(28, 5, 'Corea del Sud'),
(28, 6, 'Corea del Sur'),
(29, 1, 'Israel'),
(29, 2, 'Israël'),
(29, 3, 'Israel'),
(29, 4, 'Israël'),
(29, 5, 'Israele'),
(29, 6, 'Israel'),
(30, 1, 'South Africa'),
(30, 2, 'Afrique du Sud'),
(30, 3, 'Südafrika'),
(30, 4, 'Zuid-Afrika'),
(30, 5, 'Sudafrica'),
(30, 6, 'Sudáfrica'),
(31, 1, 'Nigeria'),
(31, 2, 'Nigéria'),
(31, 3, 'Nigeria'),
(31, 4, 'Nigeria'),
(31, 5, 'Nigeria'),
(31, 6, 'Nigeria'),
(32, 1, 'Côte d’Ivoire'),
(32, 2, 'Côte d’Ivoire'),
(32, 3, 'Côte d’Ivoire'),
(32, 4, 'Ivoorkust'),
(32, 5, 'Costa d’Avorio'),
(32, 6, 'Côte d’Ivoire'),
(33, 1, 'Togo'),
(33, 2, 'Togo'),
(33, 3, 'Togo'),
(33, 4, 'Togo'),
(33, 5, 'Togo'),
(33, 6, 'Togo'),
(34, 1, 'Bolivia'),
(34, 2, 'Bolivie'),
(34, 3, 'Bolivien'),
(34, 4, 'Bolivia'),
(34, 5, 'Bolivia'),
(34, 6, 'Bolivia'),
(35, 1, 'Mauritius'),
(35, 2, 'Maurice'),
(35, 3, 'Mauritius'),
(35, 4, 'Mauritius'),
(35, 5, 'Mauritius'),
(35, 6, 'Mauricio'),
(36, 1, 'Romania'),
(36, 2, 'Roumanie'),
(36, 3, 'Rumänien'),
(36, 4, 'Roemenië'),
(36, 5, 'Romania'),
(36, 6, 'Rumanía'),
(37, 1, 'Slovakia'),
(37, 2, 'Slovaquie'),
(37, 3, 'Slowakei'),
(37, 4, 'Slowakije'),
(37, 5, 'Slovacchia'),
(37, 6, 'Eslovaquia'),
(38, 1, 'Algeria'),
(38, 2, 'Algérie'),
(38, 3, 'Algerien'),
(38, 4, 'Algerije'),
(38, 5, 'Algeria'),
(38, 6, 'Argelia'),
(39, 1, 'American Samoa'),
(39, 2, 'Samoa américaines'),
(39, 3, 'Amerikanisch-Samoa'),
(39, 4, 'Amerikaans-Samoa'),
(39, 5, 'Samoa americane'),
(39, 6, 'Samoa Americana'),
(40, 1, 'Andorra'),
(40, 2, 'Andorre'),
(40, 3, 'Andorra'),
(40, 4, 'Andorra'),
(40, 5, 'Andorra'),
(40, 6, 'Andorra'),
(41, 1, 'Angola'),
(41, 2, 'Angola'),
(41, 3, 'Angola'),
(41, 4, 'Angola'),
(41, 5, 'Angola'),
(41, 6, 'Angola'),
(42, 1, 'Anguilla'),
(42, 2, 'Anguilla'),
(42, 3, 'Anguilla'),
(42, 4, 'Anguilla'),
(42, 5, 'Anguilla'),
(42, 6, 'Anguila'),
(43, 1, 'Antigua & Barbuda'),
(43, 2, 'Antigua-et-Barbuda'),
(43, 3, 'Antigua und Barbuda'),
(43, 4, 'Antigua en Barbuda'),
(43, 5, 'Antigua e Barbuda'),
(43, 6, 'Antigua y Barbuda'),
(44, 1, 'Argentina'),
(44, 2, 'Argentine'),
(44, 3, 'Argentinien'),
(44, 4, 'Argentinië'),
(44, 5, 'Argentina'),
(44, 6, 'Argentina'),
(45, 1, 'Armenia'),
(45, 2, 'Arménie'),
(45, 3, 'Armenien'),
(45, 4, 'Armenië'),
(45, 5, 'Armenia'),
(45, 6, 'Armenia'),
(46, 1, 'Aruba'),
(46, 2, 'Aruba'),
(46, 3, 'Aruba'),
(46, 4, 'Aruba'),
(46, 5, 'Aruba'),
(46, 6, 'Aruba'),
(47, 1, 'Azerbaijan'),
(47, 2, 'Azerbaïdjan'),
(47, 3, 'Aserbaidschan'),
(47, 4, 'Azerbeidzjan'),
(47, 5, 'Azerbaigian'),
(47, 6, 'Azerbaiyán'),
(48, 1, 'Bahamas'),
(48, 2, 'Bahamas'),
(48, 3, 'Bahamas'),
(48, 4, 'Bahama’s'),
(48, 5, 'Bahamas'),
(48, 6, 'Bahamas'),
(49, 1, 'Bahrain'),
(49, 2, 'Bahreïn'),
(49, 3, 'Bahrain'),
(49, 4, 'Bahrein'),
(49, 5, 'Bahrein'),
(49, 6, 'Baréin'),
(50, 1, 'Bangladesh'),
(50, 2, 'Bangladesh'),
(50, 3, 'Bangladesch'),
(50, 4, 'Bangladesh'),
(50, 5, 'Bangladesh'),
(50, 6, 'Bangladés'),
(51, 1, 'Barbados'),
(51, 2, 'Barbade'),
(51, 3, 'Barbados'),
(51, 4, 'Barbados'),
(51, 5, 'Barbados'),
(51, 6, 'Barbados'),
(52, 1, 'Belarus'),
(52, 2, 'Biélorussie'),
(52, 3, 'Belarus'),
(52, 4, 'Belarus'),
(52, 5, 'Bielorussia'),
(52, 6, 'Bielorrusia'),
(53, 1, 'Belize'),
(53, 2, 'Belize'),
(53, 3, 'Belize'),
(53, 4, 'Belize'),
(53, 5, 'Belize'),
(53, 6, 'Belice'),
(54, 1, 'Benin'),
(54, 2, 'Bénin'),
(54, 3, 'Benin'),
(54, 4, 'Benin'),
(54, 5, 'Benin'),
(54, 6, 'Benín'),
(55, 1, 'Bermuda'),
(55, 2, 'Bermudes'),
(55, 3, 'Bermuda'),
(55, 4, 'Bermuda'),
(55, 5, 'Bermuda'),
(55, 6, 'Bermudas'),
(56, 1, 'Bhutan'),
(56, 2, 'Bhoutan'),
(56, 3, 'Bhutan'),
(56, 4, 'Bhutan'),
(56, 5, 'Bhutan'),
(56, 6, 'Bután'),
(57, 1, 'Botswana'),
(57, 2, 'Botswana'),
(57, 3, 'Botsuana'),
(57, 4, 'Botswana'),
(57, 5, 'Botswana'),
(57, 6, 'Botsuana'),
(58, 1, 'Brazil'),
(58, 2, 'Brésil'),
(58, 3, 'Brasilien'),
(58, 4, 'Brazilië'),
(58, 5, 'Brasile'),
(58, 6, 'Brasil'),
(59, 1, 'Brunei'),
(59, 2, 'Brunéi Darussalam'),
(59, 3, 'Brunei Darussalam'),
(59, 4, 'Brunei'),
(59, 5, 'Brunei'),
(59, 6, 'Brunéi'),
(60, 1, 'Burkina Faso'),
(60, 2, 'Burkina Faso'),
(60, 3, 'Burkina Faso'),
(60, 4, 'Burkina Faso'),
(60, 5, 'Burkina Faso'),
(60, 6, 'Burkina Faso'),
(61, 1, 'Myanmar (Burma)'),
(61, 2, 'Myanmar (Birmanie)'),
(61, 3, 'Myanmar'),
(61, 4, 'Myanmar (Birma)'),
(61, 5, 'Myanmar (Birmania)'),
(61, 6, 'Myanmar (Birmania)'),
(62, 1, 'Burundi'),
(62, 2, 'Burundi'),
(62, 3, 'Burundi'),
(62, 4, 'Burundi'),
(62, 5, 'Burundi'),
(62, 6, 'Burundi'),
(63, 1, 'Cambodia'),
(63, 2, 'Cambodge'),
(63, 3, 'Kambodscha'),
(63, 4, 'Cambodja'),
(63, 5, 'Cambogia'),
(63, 6, 'Camboya'),
(64, 1, 'Cameroon'),
(64, 2, 'Cameroun'),
(64, 3, 'Kamerun'),
(64, 4, 'Kameroen'),
(64, 5, 'Camerun'),
(64, 6, 'Camerún'),
(65, 1, 'Cape Verde'),
(65, 2, 'Cap-Vert'),
(65, 3, 'Cabo Verde'),
(65, 4, 'Kaapverdië'),
(65, 5, 'Capo Verde'),
(65, 6, 'Cabo Verde'),
(66, 1, 'Central African Republic'),
(66, 2, 'République centrafricaine'),
(66, 3, 'Zentralafrikanische Republik'),
(66, 4, 'Centraal-Afrikaanse Republiek'),
(66, 5, 'Repubblica Centrafricana'),
(66, 6, 'República Centroafricana'),
(67, 1, 'Chad'),
(67, 2, 'Tchad'),
(67, 3, 'Tschad'),
(67, 4, 'Tsjaad'),
(67, 5, 'Ciad'),
(67, 6, 'Chad'),
(68, 1, 'Chile'),
(68, 2, 'Chili'),
(68, 3, 'Chile'),
(68, 4, 'Chili'),
(68, 5, 'Cile'),
(68, 6, 'Chile'),
(69, 1, 'Colombia'),
(69, 2, 'Colombie'),
(69, 3, 'Kolumbien'),
(69, 4, 'Colombia'),
(69, 5, 'Colombia'),
(69, 6, 'Colombia'),
(70, 1, 'Comoros'),
(70, 2, 'Comores'),
(70, 3, 'Komoren'),
(70, 4, 'Comoren'),
(70, 5, 'Comore'),
(70, 6, 'Comoras'),
(71, 1, 'Congo - Kinshasa'),
(71, 2, 'Congo-Kinshasa'),
(71, 3, 'Kongo-Kinshasa'),
(71, 4, 'Congo-Kinshasa'),
(71, 5, 'Congo - Kinshasa'),
(71, 6, 'República Democrática del Congo'),
(72, 1, 'Congo - Brazzaville'),
(72, 2, 'Congo-Brazzaville'),
(72, 3, 'Kongo-Brazzaville'),
(72, 4, 'Congo-Brazzaville'),
(72, 5, 'Congo-Brazzaville'),
(72, 6, 'Congo'),
(73, 1, 'Costa Rica'),
(73, 2, 'Costa Rica'),
(73, 3, 'Costa Rica'),
(73, 4, 'Costa Rica'),
(73, 5, 'Costa Rica'),
(73, 6, 'Costa Rica'),
(74, 1, 'Croatia'),
(74, 2, 'Croatie'),
(74, 3, 'Kroatien'),
(74, 4, 'Kroatië'),
(74, 5, 'Croazia'),
(74, 6, 'Croacia'),
(75, 1, 'Cuba'),
(75, 2, 'Cuba'),
(75, 3, 'Kuba'),
(75, 4, 'Cuba'),
(75, 5, 'Cuba'),
(75, 6, 'Cuba'),
(76, 1, 'Cyprus'),
(76, 2, 'Chypre'),
(76, 3, 'Zypern'),
(76, 4, 'Cyprus'),
(76, 5, 'Cipro'),
(76, 6, 'Chipre'),
(77, 1, 'Djibouti'),
(77, 2, 'Djibouti'),
(77, 3, 'Dschibuti'),
(77, 4, 'Djibouti'),
(77, 5, 'Gibuti'),
(77, 6, 'Yibuti'),
(78, 1, 'Dominica'),
(78, 2, 'Dominique'),
(78, 3, 'Dominica'),
(78, 4, 'Dominica'),
(78, 5, 'Dominica'),
(78, 6, 'Dominica'),
(79, 1, 'Dominican Republic'),
(79, 2, 'République dominicaine'),
(79, 3, 'Dominikanische Republik'),
(79, 4, 'Dominicaanse Republiek'),
(79, 5, 'Repubblica Dominicana'),
(79, 6, 'República Dominicana'),
(80, 1, 'Timor-Leste'),
(80, 2, 'Timor oriental'),
(80, 3, 'Timor-Leste'),
(80, 4, 'Oost-Timor'),
(80, 5, 'Timor Est'),
(80, 6, 'Timor-Leste'),
(81, 1, 'Ecuador'),
(81, 2, 'Équateur'),
(81, 3, 'Ecuador'),
(81, 4, 'Ecuador'),
(81, 5, 'Ecuador'),
(81, 6, 'Ecuador'),
(82, 1, 'Egypt'),
(82, 2, 'Égypte'),
(82, 3, 'Ägypten'),
(82, 4, 'Egypte'),
(82, 5, 'Egitto'),
(82, 6, 'Egipto'),
(83, 1, 'El Salvador'),
(83, 2, 'Salvador'),
(83, 3, 'El Salvador'),
(83, 4, 'El Salvador'),
(83, 5, 'El Salvador'),
(83, 6, 'El Salvador'),
(84, 1, 'Equatorial Guinea'),
(84, 2, 'Guinée équatoriale'),
(84, 3, 'Äquatorialguinea'),
(84, 4, 'Equatoriaal-Guinea'),
(84, 5, 'Guinea Equatoriale'),
(84, 6, 'Guinea Ecuatorial'),
(85, 1, 'Eritrea'),
(85, 2, 'Érythrée'),
(85, 3, 'Eritrea'),
(85, 4, 'Eritrea'),
(85, 5, 'Eritrea'),
(85, 6, 'Eritrea'),
(86, 1, 'Estonia'),
(86, 2, 'Estonie'),
(86, 3, 'Estland'),
(86, 4, 'Estland'),
(86, 5, 'Estonia'),
(86, 6, 'Estonia'),
(87, 1, 'Ethiopia'),
(87, 2, 'Éthiopie'),
(87, 3, 'Äthiopien'),
(87, 4, 'Ethiopië'),
(87, 5, 'Etiopia'),
(87, 6, 'Etiopía'),
(88, 1, 'Falkland Islands'),
(88, 2, 'Îles Malouines'),
(88, 3, 'Falklandinseln'),
(88, 4, 'Falklandeilanden'),
(88, 5, 'Isole Falkland'),
(88, 6, 'Islas Malvinas'),
(89, 1, 'Faroe Islands'),
(89, 2, 'Îles Féroé'),
(89, 3, 'Färöer'),
(89, 4, 'Faeröer'),
(89, 5, 'Isole Fær Øer'),
(89, 6, 'Islas Feroe'),
(90, 1, 'Fiji'),
(90, 2, 'Fidji'),
(90, 3, 'Fidschi'),
(90, 4, 'Fiji'),
(90, 5, 'Figi'),
(90, 6, 'Fiyi'),
(91, 1, 'Gabon'),
(91, 2, 'Gabon'),
(91, 3, 'Gabun'),
(91, 4, 'Gabon'),
(91, 5, 'Gabon'),
(91, 6, 'Gabón'),
(92, 1, 'Gambia'),
(92, 2, 'Gambie'),
(92, 3, 'Gambia'),
(92, 4, 'Gambia'),
(92, 5, 'Gambia'),
(92, 6, 'Gambia'),
(93, 1, 'Georgia'),
(93, 2, 'Géorgie'),
(93, 3, 'Georgien'),
(93, 4, 'Georgië'),
(93, 5, 'Georgia'),
(93, 6, 'Georgia'),
(94, 1, 'Ghana'),
(94, 2, 'Ghana'),
(94, 3, 'Ghana'),
(94, 4, 'Ghana'),
(94, 5, 'Ghana'),
(94, 6, 'Ghana'),
(95, 1, 'Grenada'),
(95, 2, 'Grenade'),
(95, 3, 'Grenada'),
(95, 4, 'Grenada'),
(95, 5, 'Grenada'),
(95, 6, 'Granada'),
(96, 1, 'Greenland'),
(96, 2, 'Groenland'),
(96, 3, 'Grönland'),
(96, 4, 'Groenland'),
(96, 5, 'Groenlandia'),
(96, 6, 'Groenlandia'),
(97, 1, 'Gibraltar'),
(97, 2, 'Gibraltar'),
(97, 3, 'Gibraltar'),
(97, 4, 'Gibraltar'),
(97, 5, 'Gibilterra'),
(97, 6, 'Gibraltar'),
(98, 1, 'Guadeloupe'),
(98, 2, 'Guadeloupe'),
(98, 3, 'Guadeloupe'),
(98, 4, 'Guadeloupe'),
(98, 5, 'Guadalupa'),
(98, 6, 'Guadalupe'),
(99, 1, 'Guam'),
(99, 2, 'Guam'),
(99, 3, 'Guam'),
(99, 4, 'Guam'),
(99, 5, 'Guam'),
(99, 6, 'Guam'),
(100, 1, 'Guatemala'),
(100, 2, 'Guatemala'),
(100, 3, 'Guatemala'),
(100, 4, 'Guatemala'),
(100, 5, 'Guatemala'),
(100, 6, 'Guatemala'),
(101, 1, 'Guernsey'),
(101, 2, 'Guernesey'),
(101, 3, 'Guernsey'),
(101, 4, 'Guernsey'),
(101, 5, 'Guernsey'),
(101, 6, 'Guernsey'),
(102, 1, 'Guinea'),
(102, 2, 'Guinée'),
(102, 3, 'Guinea'),
(102, 4, 'Guinee'),
(102, 5, 'Guinea'),
(102, 6, 'Guinea'),
(103, 1, 'Guinea-Bissau'),
(103, 2, 'Guinée-Bissau'),
(103, 3, 'Guinea-Bissau'),
(103, 4, 'Guinee-Bissau'),
(103, 5, 'Guinea-Bissau'),
(103, 6, 'Guinea-Bisáu'),
(104, 1, 'Guyana'),
(104, 2, 'Guyana'),
(104, 3, 'Guyana'),
(104, 4, 'Guyana'),
(104, 5, 'Guyana'),
(104, 6, 'Guyana'),
(105, 1, 'Haiti'),
(105, 2, 'Haïti'),
(105, 3, 'Haiti'),
(105, 4, 'Haïti'),
(105, 5, 'Haiti'),
(105, 6, 'Haití'),
(106, 1, 'Vatican City'),
(106, 2, 'État de la Cité du Vatican'),
(106, 3, 'Vatikanstadt'),
(106, 4, 'Vaticaanstad'),
(106, 5, 'Città del Vaticano'),
(106, 6, 'Ciudad del Vaticano'),
(107, 1, 'Honduras'),
(107, 2, 'Honduras'),
(107, 3, 'Honduras'),
(107, 4, 'Honduras'),
(107, 5, 'Honduras'),
(107, 6, 'Honduras'),
(108, 1, 'Iceland'),
(108, 2, 'Islande'),
(108, 3, 'Island'),
(108, 4, 'IJsland'),
(108, 5, 'Islanda'),
(108, 6, 'Islandia'),
(109, 1, 'India'),
(109, 2, 'Inde'),
(109, 3, 'Indien'),
(109, 4, 'India'),
(109, 5, 'India'),
(109, 6, 'India'),
(110, 1, 'Indonesia'),
(110, 2, 'Indonésie'),
(110, 3, 'Indonesien'),
(110, 4, 'Indonesië'),
(110, 5, 'Indonesia'),
(110, 6, 'Indonesia'),
(111, 1, 'Iran'),
(111, 2, 'Iran'),
(111, 3, 'Iran'),
(111, 4, 'Iran'),
(111, 5, 'Iran'),
(111, 6, 'Irán'),
(112, 1, 'Iraq'),
(112, 2, 'Irak'),
(112, 3, 'Irak'),
(112, 4, 'Irak'),
(112, 5, 'Iraq'),
(112, 6, 'Irak'),
(113, 1, 'Isle of Man'),
(113, 2, 'Île de Man'),
(113, 3, 'Isle of Man'),
(113, 4, 'Isle of Man'),
(113, 5, 'Isola di Man'),
(113, 6, 'Isla de Man'),
(114, 1, 'Jamaica'),
(114, 2, 'Jamaïque'),
(114, 3, 'Jamaika'),
(114, 4, 'Jamaica'),
(114, 5, 'Giamaica'),
(114, 6, 'Jamaica'),
(115, 1, 'Jersey'),
(115, 2, 'Jersey'),
(115, 3, 'Jersey'),
(115, 4, 'Jersey'),
(115, 5, 'Jersey'),
(115, 6, 'Jersey'),
(116, 1, 'Jordan'),
(116, 2, 'Jordanie'),
(116, 3, 'Jordanien'),
(116, 4, 'Jordanië'),
(116, 5, 'Giordania'),
(116, 6, 'Jordania'),
(117, 1, 'Kazakhstan'),
(117, 2, 'Kazakhstan'),
(117, 3, 'Kasachstan'),
(117, 4, 'Kazachstan'),
(117, 5, 'Kazakistan'),
(117, 6, 'Kazajistán'),
(118, 1, 'Kenya'),
(118, 2, 'Kenya'),
(118, 3, 'Kenia'),
(118, 4, 'Kenia'),
(118, 5, 'Kenya'),
(118, 6, 'Kenia'),
(119, 1, 'Kiribati'),
(119, 2, 'Kiribati'),
(119, 3, 'Kiribati'),
(119, 4, 'Kiribati'),
(119, 5, 'Kiribati'),
(119, 6, 'Kiribati'),
(120, 1, 'North Korea'),
(120, 2, 'Corée du Nord'),
(120, 3, 'Nordkorea'),
(120, 4, 'Noord-Korea'),
(120, 5, 'Corea del Nord'),
(120, 6, 'Corea del Norte'),
(121, 1, 'Kuwait'),
(121, 2, 'Koweït'),
(121, 3, 'Kuwait'),
(121, 4, 'Koeweit'),
(121, 5, 'Kuwait'),
(121, 6, 'Kuwait'),
(122, 1, 'Kyrgyzstan'),
(122, 2, 'Kirghizistan'),
(122, 3, 'Kirgisistan'),
(122, 4, 'Kirgizië'),
(122, 5, 'Kirghizistan'),
(122, 6, 'Kirguistán'),
(123, 1, 'Laos'),
(123, 2, 'Laos'),
(123, 3, 'Laos'),
(123, 4, 'Laos'),
(123, 5, 'Laos'),
(123, 6, 'Laos'),
(124, 1, 'Latvia'),
(124, 2, 'Lettonie'),
(124, 3, 'Lettland'),
(124, 4, 'Letland'),
(124, 5, 'Lettonia'),
(124, 6, 'Letonia'),
(125, 1, 'Lebanon'),
(125, 2, 'Liban'),
(125, 3, 'Libanon'),
(125, 4, 'Libanon'),
(125, 5, 'Libano'),
(125, 6, 'Líbano'),
(126, 1, 'Lesotho'),
(126, 2, 'Lesotho'),
(126, 3, 'Lesotho'),
(126, 4, 'Lesotho'),
(126, 5, 'Lesotho'),
(126, 6, 'Lesoto'),
(127, 1, 'Liberia'),
(127, 2, 'Libéria'),
(127, 3, 'Liberia'),
(127, 4, 'Liberia'),
(127, 5, 'Liberia'),
(127, 6, 'Liberia'),
(128, 1, 'Libya'),
(128, 2, 'Libye'),
(128, 3, 'Libyen'),
(128, 4, 'Libië'),
(128, 5, 'Libia'),
(128, 6, 'Libia'),
(129, 1, 'Liechtenstein'),
(129, 2, 'Liechtenstein'),
(129, 3, 'Liechtenstein'),
(129, 4, 'Liechtenstein'),
(129, 5, 'Liechtenstein'),
(129, 6, 'Liechtenstein'),
(130, 1, 'Lithuania'),
(130, 2, 'Lituanie'),
(130, 3, 'Litauen'),
(130, 4, 'Litouwen'),
(130, 5, 'Lituania'),
(130, 6, 'Lituania'),
(131, 1, 'Macao SAR China'),
(131, 2, 'R.A.S. chinoise de Macao'),
(131, 3, 'Sonderverwaltungsregion Macau'),
(131, 4, 'Macau SAR van China'),
(131, 5, 'RAS di Macao'),
(131, 6, 'RAE de Macao (China)'),
(132, 1, 'North Macedonia'),
(132, 2, 'Macédoine du Nord'),
(132, 3, 'Nordmazedonien'),
(132, 4, 'Noord-Macedonië'),
(132, 5, 'Macedonia del Nord'),
(132, 6, 'Macedonia del Norte'),
(133, 1, 'Madagascar'),
(133, 2, 'Madagascar'),
(133, 3, 'Madagaskar'),
(133, 4, 'Madagaskar'),
(133, 5, 'Madagascar'),
(133, 6, 'Madagascar'),
(134, 1, 'Malawi'),
(134, 2, 'Malawi'),
(134, 3, 'Malawi'),
(134, 4, 'Malawi'),
(134, 5, 'Malawi'),
(134, 6, 'Malaui'),
(135, 1, 'Malaysia'),
(135, 2, 'Malaisie'),
(135, 3, 'Malaysia'),
(135, 4, 'Maleisië'),
(135, 5, 'Malaysia'),
(135, 6, 'Malasia'),
(136, 1, 'Maldives'),
(136, 2, 'Maldives'),
(136, 3, 'Malediven'),
(136, 4, 'Maldiven'),
(136, 5, 'Maldive'),
(136, 6, 'Maldivas'),
(137, 1, 'Mali'),
(137, 2, 'Mali'),
(137, 3, 'Mali'),
(137, 4, 'Mali'),
(137, 5, 'Mali'),
(137, 6, 'Mali'),
(138, 1, 'Malta'),
(138, 2, 'Malte'),
(138, 3, 'Malta'),
(138, 4, 'Malta'),
(138, 5, 'Malta'),
(138, 6, 'Malta'),
(139, 1, 'Marshall Islands'),
(139, 2, 'Îles Marshall'),
(139, 3, 'Marshallinseln'),
(139, 4, 'Marshalleilanden'),
(139, 5, 'Isole Marshall'),
(139, 6, 'Islas Marshall'),
(140, 1, 'Martinique'),
(140, 2, 'Martinique'),
(140, 3, 'Martinique'),
(140, 4, 'Martinique'),
(140, 5, 'Martinica'),
(140, 6, 'Martinica'),
(141, 1, 'Mauritania'),
(141, 2, 'Mauritanie'),
(141, 3, 'Mauretanien'),
(141, 4, 'Mauritanië'),
(141, 5, 'Mauritania'),
(141, 6, 'Mauritania'),
(142, 1, 'Hungary'),
(142, 2, 'Hongrie'),
(142, 3, 'Ungarn'),
(142, 4, 'Hongarije'),
(142, 5, 'Ungheria'),
(142, 6, 'Hungría'),
(143, 1, 'Mayotte'),
(143, 2, 'Mayotte'),
(143, 3, 'Mayotte'),
(143, 4, 'Mayotte'),
(143, 5, 'Mayotte'),
(143, 6, 'Mayotte'),
(144, 1, 'Mexico'),
(144, 2, 'Mexique'),
(144, 3, 'Mexiko'),
(144, 4, 'Mexico'),
(144, 5, 'Messico'),
(144, 6, 'México'),
(145, 1, 'Micronesia'),
(145, 2, 'États fédérés de Micronésie'),
(145, 3, 'Mikronesien'),
(145, 4, 'Micronesia'),
(145, 5, 'Micronesia'),
(145, 6, 'Micronesia'),
(146, 1, 'Moldova'),
(146, 2, 'Moldavie'),
(146, 3, 'Republik Moldau'),
(146, 4, 'Moldavië'),
(146, 5, 'Moldavia'),
(146, 6, 'Moldavia'),
(147, 1, 'Monaco'),
(147, 2, 'Monaco'),
(147, 3, 'Monaco'),
(147, 4, 'Monaco'),
(147, 5, 'Monaco'),
(147, 6, 'Mónaco'),
(148, 1, 'Mongolia'),
(148, 2, 'Mongolie'),
(148, 3, 'Mongolei'),
(148, 4, 'Mongolië'),
(148, 5, 'Mongolia'),
(148, 6, 'Mongolia'),
(149, 1, 'Montenegro'),
(149, 2, 'Monténégro'),
(149, 3, 'Montenegro'),
(149, 4, 'Montenegro'),
(149, 5, 'Montenegro'),
(149, 6, 'Montenegro'),
(150, 1, 'Montserrat'),
(150, 2, 'Montserrat'),
(150, 3, 'Montserrat'),
(150, 4, 'Montserrat'),
(150, 5, 'Montserrat'),
(150, 6, 'Montserrat'),
(151, 1, 'Morocco'),
(151, 2, 'Maroc'),
(151, 3, 'Marokko'),
(151, 4, 'Marokko'),
(151, 5, 'Marocco'),
(151, 6, 'Marruecos'),
(152, 1, 'Mozambique'),
(152, 2, 'Mozambique'),
(152, 3, 'Mosambik'),
(152, 4, 'Mozambique'),
(152, 5, 'Mozambico'),
(152, 6, 'Mozambique'),
(153, 1, 'Namibia'),
(153, 2, 'Namibie'),
(153, 3, 'Namibia'),
(153, 4, 'Namibië'),
(153, 5, 'Namibia'),
(153, 6, 'Namibia'),
(154, 1, 'Nauru'),
(154, 2, 'Nauru'),
(154, 3, 'Nauru'),
(154, 4, 'Nauru'),
(154, 5, 'Nauru'),
(154, 6, 'Nauru'),
(155, 1, 'Nepal'),
(155, 2, 'Népal'),
(155, 3, 'Nepal'),
(155, 4, 'Nepal'),
(155, 5, 'Nepal'),
(155, 6, 'Nepal'),
(156, 1, 'New Caledonia'),
(156, 2, 'Nouvelle-Calédonie'),
(156, 3, 'Neukaledonien'),
(156, 4, 'Nieuw-Caledonië'),
(156, 5, 'Nuova Caledonia'),
(156, 6, 'Nueva Caledonia'),
(157, 1, 'Nicaragua'),
(157, 2, 'Nicaragua'),
(157, 3, 'Nicaragua'),
(157, 4, 'Nicaragua'),
(157, 5, 'Nicaragua'),
(157, 6, 'Nicaragua'),
(158, 1, 'Niger'),
(158, 2, 'Niger'),
(158, 3, 'Niger'),
(158, 4, 'Niger'),
(158, 5, 'Niger'),
(158, 6, 'Níger'),
(159, 1, 'Niue'),
(159, 2, 'Niue'),
(159, 3, 'Niue'),
(159, 4, 'Niue'),
(159, 5, 'Niue'),
(159, 6, 'Niue'),
(160, 1, 'Norfolk Island'),
(160, 2, 'Île Norfolk'),
(160, 3, 'Norfolkinsel'),
(160, 4, 'Norfolk'),
(160, 5, 'Isola Norfolk'),
(160, 6, 'Isla Norfolk'),
(161, 1, 'Northern Mariana Islands'),
(161, 2, 'Îles Mariannes du Nord'),
(161, 3, 'Nördliche Marianen'),
(161, 4, 'Noordelijke Marianen'),
(161, 5, 'Isole Marianne settentrionali'),
(161, 6, 'Islas Marianas del Norte'),
(162, 1, 'Oman'),
(162, 2, 'Oman'),
(162, 3, 'Oman'),
(162, 4, 'Oman'),
(162, 5, 'Oman'),
(162, 6, 'Omán'),
(163, 1, 'Pakistan'),
(163, 2, 'Pakistan'),
(163, 3, 'Pakistan'),
(163, 4, 'Pakistan'),
(163, 5, 'Pakistan'),
(163, 6, 'Pakistán'),
(164, 1, 'Palau'),
(164, 2, 'Palaos'),
(164, 3, 'Palau'),
(164, 4, 'Palau'),
(164, 5, 'Palau'),
(164, 6, 'Palaos'),
(165, 1, 'Palestinian Territories'),
(165, 2, 'Territoires palestiniens'),
(165, 3, 'Palästinensische Autonomiegebiete'),
(165, 4, 'Palestijnse gebieden'),
(165, 5, 'Territori palestinesi'),
(165, 6, 'Territorios Palestinos'),
(166, 1, 'Panama'),
(166, 2, 'Panama'),
(166, 3, 'Panama'),
(166, 4, 'Panama'),
(166, 5, 'Panamá'),
(166, 6, 'Panamá'),
(167, 1, 'Papua New Guinea'),
(167, 2, 'Papouasie-Nouvelle-Guinée'),
(167, 3, 'Papua-Neuguinea'),
(167, 4, 'Papoea-Nieuw-Guinea'),
(167, 5, 'Papua Nuova Guinea'),
(167, 6, 'Papúa Nueva Guinea'),
(168, 1, 'Paraguay'),
(168, 2, 'Paraguay'),
(168, 3, 'Paraguay'),
(168, 4, 'Paraguay'),
(168, 5, 'Paraguay'),
(168, 6, 'Paraguay'),
(169, 1, 'Peru'),
(169, 2, 'Pérou'),
(169, 3, 'Peru'),
(169, 4, 'Peru'),
(169, 5, 'Perù'),
(169, 6, 'Perú'),
(170, 1, 'Philippines'),
(170, 2, 'Philippines'),
(170, 3, 'Philippinen'),
(170, 4, 'Filipijnen'),
(170, 5, 'Filippine'),
(170, 6, 'Filipinas'),
(171, 1, 'Pitcairn Islands'),
(171, 2, 'Îles Pitcairn'),
(171, 3, 'Pitcairninseln'),
(171, 4, 'Pitcairneilanden'),
(171, 5, 'Isole Pitcairn'),
(171, 6, 'Islas Pitcairn'),
(172, 1, 'Puerto Rico'),
(172, 2, 'Porto Rico'),
(172, 3, 'Puerto Rico'),
(172, 4, 'Puerto Rico'),
(172, 5, 'Portorico'),
(172, 6, 'Puerto Rico'),
(173, 1, 'Qatar'),
(173, 2, 'Qatar'),
(173, 3, 'Katar'),
(173, 4, 'Qatar'),
(173, 5, 'Qatar'),
(173, 6, 'Catar'),
(174, 1, 'Réunion'),
(174, 2, 'La Réunion'),
(174, 3, 'Réunion'),
(174, 4, 'Réunion'),
(174, 5, 'Riunione'),
(174, 6, 'Reunión'),
(175, 1, 'Russia'),
(175, 2, 'Russie'),
(175, 3, 'Russland'),
(175, 4, 'Rusland'),
(175, 5, 'Russia'),
(175, 6, 'Rusia'),
(176, 1, 'Rwanda'),
(176, 2, 'Rwanda'),
(176, 3, 'Ruanda'),
(176, 4, 'Rwanda'),
(176, 5, 'Ruanda'),
(176, 6, 'Ruanda'),
(177, 1, 'St. Barthélemy'),
(177, 2, 'Saint-Barthélemy'),
(177, 3, 'St. Barthélemy'),
(177, 4, 'Saint-Barthélemy'),
(177, 5, 'Saint-Barthélemy'),
(177, 6, 'San Bartolomé'),
(178, 1, 'St. Kitts & Nevis'),
(178, 2, 'Saint-Christophe-et-Niévès'),
(178, 3, 'St. Kitts und Nevis'),
(178, 4, 'Saint Kitts en Nevis'),
(178, 5, 'Saint Kitts e Nevis'),
(178, 6, 'San Cristóbal y Nieves'),
(179, 1, 'St. Lucia'),
(179, 2, 'Sainte-Lucie'),
(179, 3, 'St. Lucia'),
(179, 4, 'Saint Lucia'),
(179, 5, 'Saint Lucia'),
(179, 6, 'Santa Lucía'),
(180, 1, 'St. Martin'),
(180, 2, 'Saint-Martin'),
(180, 3, 'St. Martin'),
(180, 4, 'Saint-Martin'),
(180, 5, 'Saint Martin'),
(180, 6, 'San Martín'),
(181, 1, 'St. Pierre & Miquelon'),
(181, 2, 'Saint-Pierre-et-Miquelon'),
(181, 3, 'St. Pierre und Miquelon'),
(181, 4, 'Saint-Pierre en Miquelon'),
(181, 5, 'Saint-Pierre e Miquelon'),
(181, 6, 'San Pedro y Miquelón'),
(182, 1, 'St. Vincent & Grenadines'),
(182, 2, 'Saint-Vincent-et-les-Grenadines'),
(182, 3, 'St. Vincent und die Grenadinen'),
(182, 4, 'Saint Vincent en de Grenadines'),
(182, 5, 'Saint Vincent e Grenadine'),
(182, 6, 'San Vicente y las Granadinas'),
(183, 1, 'Samoa'),
(183, 2, 'Samoa'),
(183, 3, 'Samoa'),
(183, 4, 'Samoa'),
(183, 5, 'Samoa'),
(183, 6, 'Samoa'),
(184, 1, 'San Marino'),
(184, 2, 'Saint-Marin'),
(184, 3, 'San Marino'),
(184, 4, 'San Marino'),
(184, 5, 'San Marino'),
(184, 6, 'San Marino'),
(185, 1, 'São Tomé & Príncipe'),
(185, 2, 'Sao Tomé-et-Principe'),
(185, 3, 'São Tomé und Príncipe'),
(185, 4, 'Sao Tomé en Principe'),
(185, 5, 'São Tomé e Príncipe'),
(185, 6, 'Santo Tomé y Príncipe'),
(186, 1, 'Saudi Arabia'),
(186, 2, 'Arabie saoudite'),
(186, 3, 'Saudi-Arabien'),
(186, 4, 'Saoedi-Arabië'),
(186, 5, 'Arabia Saudita'),
(186, 6, 'Arabia Saudí'),
(187, 1, 'Senegal'),
(187, 2, 'Sénégal'),
(187, 3, 'Senegal'),
(187, 4, 'Senegal'),
(187, 5, 'Senegal'),
(187, 6, 'Senegal'),
(188, 1, 'Serbia'),
(188, 2, 'Serbie'),
(188, 3, 'Serbien'),
(188, 4, 'Servië'),
(188, 5, 'Serbia'),
(188, 6, 'Serbia'),
(189, 1, 'Seychelles'),
(189, 2, 'Seychelles'),
(189, 3, 'Seychellen'),
(189, 4, 'Seychellen'),
(189, 5, 'Seychelles'),
(189, 6, 'Seychelles'),
(190, 1, 'Sierra Leone'),
(190, 2, 'Sierra Leone'),
(190, 3, 'Sierra Leone'),
(190, 4, 'Sierra Leone'),
(190, 5, 'Sierra Leone'),
(190, 6, 'Sierra Leona'),
(191, 1, 'Slovenia'),
(191, 2, 'Slovénie'),
(191, 3, 'Slowenien'),
(191, 4, 'Slovenië'),
(191, 5, 'Slovenia'),
(191, 6, 'Eslovenia'),
(192, 1, 'Solomon Islands'),
(192, 2, 'Îles Salomon'),
(192, 3, 'Salomonen'),
(192, 4, 'Salomonseilanden'),
(192, 5, 'Isole Salomone'),
(192, 6, 'Islas Salomón'),
(193, 1, 'Somalia'),
(193, 2, 'Somalie'),
(193, 3, 'Somalia'),
(193, 4, 'Somalië'),
(193, 5, 'Somalia'),
(193, 6, 'Somalia'),
(194, 1, 'South Georgia & South Sandwich Islands'),
(194, 2, 'Géorgie du Sud et îles Sandwich du Sud'),
(194, 3, 'Südgeorgien und die Südlichen Sandwichinseln'),
(194, 4, 'Zuid-Georgia en Zuidelijke Sandwicheilanden'),
(194, 5, 'Georgia del Sud e Sandwich australi'),
(194, 6, 'Islas Georgia del Sur y Sandwich del Sur'),
(195, 1, 'Sri Lanka'),
(195, 2, 'Sri Lanka'),
(195, 3, 'Sri Lanka'),
(195, 4, 'Sri Lanka'),
(195, 5, 'Sri Lanka'),
(195, 6, 'Sri Lanka'),
(196, 1, 'Sudan'),
(196, 2, 'Soudan'),
(196, 3, 'Sudan'),
(196, 4, 'Soedan'),
(196, 5, 'Sudan'),
(196, 6, 'Sudán'),
(197, 1, 'Suriname'),
(197, 2, 'Suriname'),
(197, 3, 'Suriname'),
(197, 4, 'Suriname'),
(197, 5, 'Suriname'),
(197, 6, 'Surinam'),
(198, 1, 'Svalbard & Jan Mayen'),
(198, 2, 'Svalbard et Jan Mayen'),
(198, 3, 'Spitzbergen und Jan Mayen'),
(198, 4, 'Spitsbergen en Jan Mayen'),
(198, 5, 'Svalbard e Jan Mayen'),
(198, 6, 'Svalbard y Jan Mayen'),
(199, 1, 'Eswatini'),
(199, 2, 'Eswatini'),
(199, 3, 'Eswatini'),
(199, 4, 'eSwatini'),
(199, 5, 'Swaziland'),
(199, 6, 'Esuatini'),
(200, 1, 'Syria'),
(200, 2, 'Syrie'),
(200, 3, 'Syrien'),
(200, 4, 'Syrië'),
(200, 5, 'Siria'),
(200, 6, 'Siria'),
(201, 1, 'Taiwan'),
(201, 2, 'Taïwan'),
(201, 3, 'Taiwan'),
(201, 4, 'Taiwan'),
(201, 5, 'Taiwan'),
(201, 6, 'Taiwán'),
(202, 1, 'Tajikistan'),
(202, 2, 'Tadjikistan'),
(202, 3, 'Tadschikistan'),
(202, 4, 'Tadzjikistan'),
(202, 5, 'Tagikistan'),
(202, 6, 'Tayikistán'),
(203, 1, 'Tanzania'),
(203, 2, 'Tanzanie'),
(203, 3, 'Tansania'),
(203, 4, 'Tanzania'),
(203, 5, 'Tanzania'),
(203, 6, 'Tanzania'),
(204, 1, 'Thailand'),
(204, 2, 'Thaïlande'),
(204, 3, 'Thailand'),
(204, 4, 'Thailand'),
(204, 5, 'Thailandia'),
(204, 6, 'Tailandia'),
(205, 1, 'Tokelau'),
(205, 2, 'Tokelau'),
(205, 3, 'Tokelau'),
(205, 4, 'Tokelau'),
(205, 5, 'Tokelau'),
(205, 6, 'Tokelau'),
(206, 1, 'Tonga'),
(206, 2, 'Tonga'),
(206, 3, 'Tonga'),
(206, 4, 'Tonga'),
(206, 5, 'Tonga'),
(206, 6, 'Tonga'),
(207, 1, 'Trinidad & Tobago'),
(207, 2, 'Trinité-et-Tobago'),
(207, 3, 'Trinidad und Tobago'),
(207, 4, 'Trinidad en Tobago'),
(207, 5, 'Trinidad e Tobago'),
(207, 6, 'Trinidad y Tobago'),
(208, 1, 'Tunisia'),
(208, 2, 'Tunisie'),
(208, 3, 'Tunesien'),
(208, 4, 'Tunesië'),
(208, 5, 'Tunisia'),
(208, 6, 'Túnez'),
(209, 1, 'Turkey'),
(209, 2, 'Turquie'),
(209, 3, 'Türkei'),
(209, 4, 'Turkije'),
(209, 5, 'Turchia'),
(209, 6, 'Turquía'),
(210, 1, 'Turkmenistan'),
(210, 2, 'Turkménistan'),
(210, 3, 'Turkmenistan'),
(210, 4, 'Turkmenistan'),
(210, 5, 'Turkmenistan'),
(210, 6, 'Turkmenistán'),
(211, 1, 'Turks & Caicos Islands'),
(211, 2, 'Îles Turques-et-Caïques'),
(211, 3, 'Turks- und Caicosinseln'),
(211, 4, 'Turks- en Caicoseilanden'),
(211, 5, 'Isole Turks e Caicos'),
(211, 6, 'Islas Turcas y Caicos'),
(212, 1, 'Tuvalu'),
(212, 2, 'Tuvalu'),
(212, 3, 'Tuvalu'),
(212, 4, 'Tuvalu'),
(212, 5, 'Tuvalu'),
(212, 6, 'Tuvalu'),
(213, 1, 'Uganda'),
(213, 2, 'Ouganda'),
(213, 3, 'Uganda'),
(213, 4, 'Oeganda'),
(213, 5, 'Uganda'),
(213, 6, 'Uganda'),
(214, 1, 'Ukraine'),
(214, 2, 'Ukraine'),
(214, 3, 'Ukraine'),
(214, 4, 'Oekraïne'),
(214, 5, 'Ucraina'),
(214, 6, 'Ucrania'),
(215, 1, 'United Arab Emirates'),
(215, 2, 'Émirats arabes unis'),
(215, 3, 'Vereinigte Arabische Emirate'),
(215, 4, 'Verenigde Arabische Emiraten'),
(215, 5, 'Emirati Arabi Uniti'),
(215, 6, 'Emiratos Árabes Unidos'),
(216, 1, 'Uruguay'),
(216, 2, 'Uruguay'),
(216, 3, 'Uruguay'),
(216, 4, 'Uruguay'),
(216, 5, 'Uruguay'),
(216, 6, 'Uruguay'),
(217, 1, 'Uzbekistan'),
(217, 2, 'Ouzbékistan'),
(217, 3, 'Usbekistan'),
(217, 4, 'Oezbekistan'),
(217, 5, 'Uzbekistan'),
(217, 6, 'Uzbekistán'),
(218, 1, 'Vanuatu'),
(218, 2, 'Vanuatu'),
(218, 3, 'Vanuatu'),
(218, 4, 'Vanuatu'),
(218, 5, 'Vanuatu'),
(218, 6, 'Vanuatu'),
(219, 1, 'Venezuela'),
(219, 2, 'Venezuela'),
(219, 3, 'Venezuela'),
(219, 4, 'Venezuela'),
(219, 5, 'Venezuela'),
(219, 6, 'Venezuela'),
(220, 1, 'Vietnam'),
(220, 2, 'Vietnam'),
(220, 3, 'Vietnam'),
(220, 4, 'Vietnam'),
(220, 5, 'Vietnam'),
(220, 6, 'Vietnam'),
(221, 1, 'British Virgin Islands'),
(221, 2, 'Îles Vierges britanniques'),
(221, 3, 'Britische Jungferninseln'),
(221, 4, 'Britse Maagdeneilanden'),
(221, 5, 'Isole Vergini Britanniche'),
(221, 6, 'Islas Vírgenes Británicas'),
(222, 1, 'U.S. Virgin Islands'),
(222, 2, 'Îles Vierges des États-Unis'),
(222, 3, 'Amerikanische Jungferninseln'),
(222, 4, 'Amerikaanse Maagdeneilanden'),
(222, 5, 'Isole Vergini Americane'),
(222, 6, 'Islas Vírgenes de EE. UU.'),
(223, 1, 'Wallis & Futuna'),
(223, 2, 'Wallis-et-Futuna'),
(223, 3, 'Wallis und Futuna'),
(223, 4, 'Wallis en Futuna'),
(223, 5, 'Wallis e Futuna'),
(223, 6, 'Wallis y Futuna'),
(224, 1, 'Western Sahara'),
(224, 2, 'Sahara occidental'),
(224, 3, 'Westsahara'),
(224, 4, 'Westelijke Sahara'),
(224, 5, 'Sahara occidentale'),
(224, 6, 'Sáhara Occidental'),
(225, 1, 'Yemen'),
(225, 2, 'Yémen'),
(225, 3, 'Jemen'),
(225, 4, 'Jemen'),
(225, 5, 'Yemen'),
(225, 6, 'Yemen'),
(226, 1, 'Zambia'),
(226, 2, 'Zambie'),
(226, 3, 'Sambia'),
(226, 4, 'Zambia'),
(226, 5, 'Zambia'),
(226, 6, 'Zambia'),
(227, 1, 'Zimbabwe'),
(227, 2, 'Zimbabwe'),
(227, 3, 'Simbabwe'),
(227, 4, 'Zimbabwe'),
(227, 5, 'Zimbabwe'),
(227, 6, 'Zimbabue'),
(228, 1, 'Albania'),
(228, 2, 'Albanie'),
(228, 3, 'Albanien'),
(228, 4, 'Albanië'),
(228, 5, 'Albania'),
(228, 6, 'Albania'),
(229, 1, 'Afghanistan'),
(229, 2, 'Afghanistan'),
(229, 3, 'Afghanistan'),
(229, 4, 'Afghanistan'),
(229, 5, 'Afghanistan'),
(229, 6, 'Afganistán'),
(230, 1, 'Antarctica'),
(230, 2, 'Antarctique'),
(230, 3, 'Antarktis'),
(230, 4, 'Antarctica'),
(230, 5, 'Antartide'),
(230, 6, 'Antártida'),
(231, 1, 'Bosnia & Herzegovina'),
(231, 2, 'Bosnie-Herzégovine'),
(231, 3, 'Bosnien und Herzegowina'),
(231, 4, 'Bosnië en Herzegovina'),
(231, 5, 'Bosnia ed Erzegovina'),
(231, 6, 'Bosnia y Herzegovina'),
(232, 1, 'British Indian Ocean Territory'),
(232, 2, 'Territoire britannique de l’océan Indien'),
(232, 3, 'Britisches Territorium im Indischen Ozean'),
(232, 4, 'Brits Indische Oceaanterritorium'),
(232, 5, 'Territorio britannico dell’Oceano Indiano'),
(232, 6, 'Territorio Británico del Océano Índico'),
(233, 1, 'Bulgaria'),
(233, 2, 'Bulgarie'),
(233, 3, 'Bulgarien'),
(233, 4, 'Bulgarije'),
(233, 5, 'Bulgaria'),
(233, 6, 'Bulgaria'),
(234, 1, 'Cayman Islands'),
(234, 2, 'Îles Caïmans'),
(234, 3, 'Kaimaninseln'),
(234, 4, 'Kaaimaneilanden'),
(234, 5, 'Isole Cayman'),
(234, 6, 'Islas Caimán'),
(235, 1, 'Christmas Island'),
(235, 2, 'Île Christmas'),
(235, 3, 'Weihnachtsinsel'),
(235, 4, 'Christmaseiland'),
(235, 5, 'Isola Christmas'),
(235, 6, 'Isla de Navidad'),
(236, 1, 'Cocos (Keeling) Islands'),
(236, 2, 'Îles Cocos'),
(236, 3, 'Kokosinseln'),
(236, 4, 'Cocoseilanden'),
(236, 5, 'Isole Cocos (Keeling)'),
(236, 6, 'Islas Cocos'),
(237, 1, 'Cook Islands'),
(237, 2, 'Îles Cook'),
(237, 3, 'Cookinseln'),
(237, 4, 'Cookeilanden'),
(237, 5, 'Isole Cook'),
(237, 6, 'Islas Cook'),
(238, 1, 'French Guiana'),
(238, 2, 'Guyane française'),
(238, 3, 'Französisch-Guayana'),
(238, 4, 'Frans-Guyana'),
(238, 5, 'Guyana francese'),
(238, 6, 'Guayana Francesa'),
(239, 1, 'French Polynesia'),
(239, 2, 'Polynésie française'),
(239, 3, 'Französisch-Polynesien'),
(239, 4, 'Frans-Polynesië'),
(239, 5, 'Polinesia francese'),
(239, 6, 'Polinesia Francesa'),
(240, 1, 'French Southern Territories'),
(240, 2, 'Terres australes françaises'),
(240, 3, 'Französische Süd- und Antarktisgebiete'),
(240, 4, 'Franse Gebieden in de zuidelijke Indische Oceaan'),
(240, 5, 'Terre australi francesi'),
(240, 6, 'Territorios Australes Franceses'),
(241, 1, 'Åland Islands'),
(241, 2, 'Îles Åland'),
(241, 3, 'Ålandinseln'),
(241, 4, 'Åland'),
(241, 5, 'Isole Åland'),
(241, 6, 'Islas Åland');

-- --------------------------------------------------------

--
-- Structure de la table `ps_country_shop`
--

DROP TABLE IF EXISTS `ps_country_shop`;
CREATE TABLE IF NOT EXISTS `ps_country_shop` (
  `id_country` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_country`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_country_shop`
--

INSERT INTO `ps_country_shop` (`id_country`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(157, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1),
(162, 1),
(163, 1),
(164, 1),
(165, 1),
(166, 1),
(167, 1),
(168, 1),
(169, 1),
(170, 1),
(171, 1),
(172, 1),
(173, 1),
(174, 1),
(175, 1),
(176, 1),
(177, 1),
(178, 1),
(179, 1),
(180, 1),
(181, 1),
(182, 1),
(183, 1),
(184, 1),
(185, 1),
(186, 1),
(187, 1),
(188, 1),
(189, 1),
(190, 1),
(191, 1),
(192, 1),
(193, 1),
(194, 1),
(195, 1),
(196, 1),
(197, 1),
(198, 1),
(199, 1),
(200, 1),
(201, 1),
(202, 1),
(203, 1),
(204, 1),
(205, 1),
(206, 1),
(207, 1),
(208, 1),
(209, 1),
(210, 1),
(211, 1),
(212, 1),
(213, 1),
(214, 1),
(215, 1),
(216, 1),
(217, 1),
(218, 1),
(219, 1),
(220, 1),
(221, 1),
(222, 1),
(223, 1),
(224, 1),
(225, 1),
(226, 1),
(227, 1),
(228, 1),
(229, 1),
(230, 1),
(231, 1),
(232, 1),
(233, 1),
(234, 1),
(235, 1),
(236, 1),
(237, 1),
(238, 1),
(239, 1),
(240, 1),
(241, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_crazy_autocomplete_products`
--

DROP TABLE IF EXISTS `ps_crazy_autocomplete_products`;
CREATE TABLE IF NOT EXISTS `ps_crazy_autocomplete_products` (
  `id_autocomplete_products` int(11) NOT NULL AUTO_INCREMENT,
  `prd_specify` longtext,
  `prd_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id_autocomplete_products`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_crazy_content`
--

DROP TABLE IF EXISTS `ps_crazy_content`;
CREATE TABLE IF NOT EXISTS `ps_crazy_content` (
  `id_crazy_content` int(11) NOT NULL AUTO_INCREMENT,
  `id_content_type` int(11) NOT NULL,
  `hook` varchar(100) NOT NULL,
  `active` int(1) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crazy_content`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ps_crazy_content`
--

INSERT INTO `ps_crazy_content` (`id_crazy_content`, `id_content_type`, `hook`, `active`, `date_created`) VALUES
(1, 0, 'displayTopColumn', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `ps_crazy_content_lang`
--

DROP TABLE IF EXISTS `ps_crazy_content_lang`;
CREATE TABLE IF NOT EXISTS `ps_crazy_content_lang` (
  `id_crazy_content` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `resource` longtext NOT NULL,
  `id_shop` int(11) NOT NULL,
  UNIQUE KEY `id_crazy_content` (`id_crazy_content`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ps_crazy_content_lang`
--

INSERT INTO `ps_crazy_content_lang` (`id_crazy_content`, `id_lang`, `title`, `resource`, `id_shop`) VALUES
(1, 1, 'la Boutique de référence en matière de jeux de sociétés', '', 1),
(1, 2, 'la Boutique de référence en matière de jeux de sociétés', '', 1),
(1, 3, 'la Boutique de référence en matière de jeux de sociétés', '', 1),
(1, 4, 'la Boutique de référence en matière de jeux de sociétés', '', 1),
(1, 5, 'la Boutique de référence en matière de jeux de sociétés', '', 1),
(1, 6, 'la Boutique de référence en matière de jeux de sociétés', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_crazy_content_shop`
--

DROP TABLE IF EXISTS `ps_crazy_content_shop`;
CREATE TABLE IF NOT EXISTS `ps_crazy_content_shop` (
  `id_crazy_content` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  UNIQUE KEY `id_crazy_content` (`id_crazy_content`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ps_crazy_content_shop`
--

INSERT INTO `ps_crazy_content_shop` (`id_crazy_content`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_crazy_extended_modules`
--

DROP TABLE IF EXISTS `ps_crazy_extended_modules`;
CREATE TABLE IF NOT EXISTS `ps_crazy_extended_modules` (
  `id_crazy_extended_modules` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `controller_name` varchar(100) NOT NULL,
  `front_controller_name` varchar(255) NOT NULL,
  `field_name` varchar(100) NOT NULL,
  `extended_item_key` varchar(300) NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id_crazy_extended_modules`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ps_crazy_fonts`
--

DROP TABLE IF EXISTS `ps_crazy_fonts`;
CREATE TABLE IF NOT EXISTS `ps_crazy_fonts` (
  `id_crazy_fonts` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `font_weight` varchar(255) NOT NULL,
  `font_style` varchar(256) NOT NULL,
  `woff` varchar(256) NOT NULL,
  `woff2` varchar(256) NOT NULL,
  `ttf` varchar(256) NOT NULL,
  `svg` varchar(256) NOT NULL,
  `eot` varchar(256) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `fontname` varchar(100) NOT NULL,
  PRIMARY KEY (`id_crazy_fonts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_crazy_library`
--

DROP TABLE IF EXISTS `ps_crazy_library`;
CREATE TABLE IF NOT EXISTS `ps_crazy_library` (
  `id_crazy_library` int(100) NOT NULL AUTO_INCREMENT,
  `data` longtext NOT NULL,
  `elements` longtext NOT NULL,
  `settings` longtext NOT NULL,
  `title` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `post_type` varchar(100) NOT NULL,
  `source` varchar(100) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `human_date` datetime DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `hasPageSettings` varchar(100) DEFAULT NULL,
  `tags` varchar(100) DEFAULT NULL,
  `export_link` text NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`id_crazy_library`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_crazy_options`
--

DROP TABLE IF EXISTS `ps_crazy_options`;
CREATE TABLE IF NOT EXISTS `ps_crazy_options` (
  `id_options` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL DEFAULT '0',
  `id_lang` int(11) NOT NULL DEFAULT '0',
  `id_shop` int(11) NOT NULL DEFAULT '0',
  `option_name` varchar(100) NOT NULL,
  `option_value` longtext NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_options`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ps_crazy_options`
--

INSERT INTO `ps_crazy_options` (`id_options`, `id`, `id_lang`, `id_shop`, `option_name`, `option_value`, `date_created`) VALUES
(1, 0, 0, 0, 'ce_new_changelog', 'null', '2022-12-11 15:04:49'),
(2, 0, 0, 0, 'ce_new_v', '', '2022-12-11 15:04:49'),
(3, 0, 0, 0, 'ce_licence', '3399cfed136962c6f6f3901d5eb21e8f', '2022-12-11 15:31:43'),
(4, 0, 0, 0, 'ce_licence_date', '2022-12-11', '2022-12-11 15:31:52'),
(5, 0, 0, 0, 'ce_licence_status', 'valid', '2022-12-11 15:31:54'),
(6, 0, 0, 0, 'ce_licence_dataset', '{\"success\":true,\"license\":\"valid\",\"item_id\":38390,\"item_name\":\"crazyelements free\",\"is_local\":true,\"license_limit\":\"0\",\"site_count\":2642,\"expires\":\"lifetime\",\"activations_left\":\"unlimited\",\"checksum\":\"cc00c49c1b7a15ed375b36272850ec7a\",\"payment_id\":38392,\"customer_name\":\"classydevs docker\",\"customer_email\":\"enamul.hassan@smartdatasoft.net\",\"price_id\":false}', '2022-12-11 15:31:54'),
(7, 0, 0, 0, 'ce_licence_expires', 'lifetime', '2022-12-11 15:31:54'),
(8, 0, 0, 0, 'elementor_scheme_color', '{\"1\":\"#6ec1e4\",\"2\":\"#54595f\",\"3\":\"#7a7a7a\",\"4\":\"#61ce70\"}', '2022-12-11 15:46:19'),
(9, 0, 0, 0, 'elementor_scheme_typography', '{\"1\":{\"font_family\":\"Roboto\",\"font_weight\":\"600\"},\"2\":{\"font_family\":\"Roboto Slab\",\"font_weight\":\"400\"},\"3\":{\"font_family\":\"Roboto\",\"font_weight\":\"400\"},\"4\":{\"font_family\":\"Roboto\",\"font_weight\":\"500\"}}', '2022-12-11 15:46:19'),
(10, 0, 0, 0, 'elementor_scheme_color-picker', '{\"1\":\"#6ec1e4\",\"2\":\"#54595f\",\"3\":\"#7a7a7a\",\"4\":\"#61ce70\",\"5\":\"#4054b2\",\"6\":\"#23a455\",\"7\":\"#000\",\"8\":\"#fff\"}', '2022-12-11 15:46:19');

-- --------------------------------------------------------

--
-- Structure de la table `ps_crazy_revision`
--

DROP TABLE IF EXISTS `ps_crazy_revision`;
CREATE TABLE IF NOT EXISTS `ps_crazy_revision` (
  `id_crazy_revision` int(11) NOT NULL AUTO_INCREMENT,
  `id_lang` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `resource` longtext NOT NULL,
  `type` varchar(100) NOT NULL,
  `post_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `revision_type` varchar(100) DEFAULT NULL,
  `settings` longtext NOT NULL,
  PRIMARY KEY (`id_crazy_revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_crazy_setting`
--

DROP TABLE IF EXISTS `ps_crazy_setting`;
CREATE TABLE IF NOT EXISTS `ps_crazy_setting` (
  `ps_crazy_id` int(100) NOT NULL AUTO_INCREMENT,
  `post_id` int(100) NOT NULL,
  `settings` text NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`ps_crazy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_currency`
--

DROP TABLE IF EXISTS `ps_currency`;
CREATE TABLE IF NOT EXISTS `ps_currency` (
  `id_currency` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `iso_code` varchar(3) NOT NULL DEFAULT '0',
  `numeric_iso_code` varchar(3) DEFAULT NULL,
  `precision` int(2) NOT NULL DEFAULT '6',
  `conversion_rate` decimal(13,6) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `unofficial` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `modified` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_currency`),
  KEY `currency_iso_code` (`iso_code`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_currency`
--

INSERT INTO `ps_currency` (`id_currency`, `name`, `iso_code`, `numeric_iso_code`, `precision`, `conversion_rate`, `deleted`, `active`, `unofficial`, `modified`) VALUES
(1, '', 'EUR', '978', 2, '1.000000', 0, 1, 0, 0),
(2, '', 'GBP', '826', 2, '0.850000', 0, 1, 0, 0),
(3, '', 'CHF', '756', 2, '0.980000', 0, 1, 0, 0),
(4, '', 'DKK', '208', 2, '7.430000', 0, 1, 0, 0),
(5, '', 'SEK', '752', 2, '10.870000', 0, 1, 0, 0),
(6, '', 'CZK', '203', 2, '24.370000', 0, 1, 0, 0),
(7, '', 'PLN', '985', 2, '4.670000', 0, 1, 0, 0),
(8, '', 'HUF', '348', 2, '409.590000', 0, 1, 0, 0),
(9, '', 'RON', '946', 2, '4.920000', 0, 1, 0, 0),
(10, '', 'HRK', '191', 2, '7.550000', 0, 1, 0, 0),
(11, '', 'BGN', '975', 2, '1.970000', 0, 1, 0, 0),
(12, '', 'NOK', '578', 2, '10.280000', 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_currency_lang`
--

DROP TABLE IF EXISTS `ps_currency_lang`;
CREATE TABLE IF NOT EXISTS `ps_currency_lang` (
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `symbol` varchar(255) NOT NULL,
  `pattern` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_currency`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_currency_lang`
--

INSERT INTO `ps_currency_lang` (`id_currency`, `id_lang`, `name`, `symbol`, `pattern`) VALUES
(1, 1, 'Euro', '€', ''),
(1, 2, 'euro', '€', ''),
(1, 3, 'Euro', '€', ''),
(1, 4, 'Euro', '€', ''),
(1, 5, 'euro', '€', ''),
(1, 6, 'euro', '€', ''),
(2, 1, 'British Pound', '£', ''),
(2, 2, 'livre sterling', '£', ''),
(2, 3, 'Britisches Pfund', '£', ''),
(2, 4, 'Brits pond', '£', ''),
(2, 5, 'sterlina britannica', '£', ''),
(2, 6, 'libra esterlina', '£', ''),
(3, 1, 'Swiss Franc', 'CHF', ''),
(3, 2, 'franc suisse', 'CHF', ''),
(3, 3, 'Schweizer Franken', 'CHF', ''),
(3, 4, 'Zwitserse frank', 'CHF', ''),
(3, 5, 'franco svizzero', 'CHF', ''),
(3, 6, 'franco suizo', 'CHF', ''),
(4, 1, 'Danish Krone', 'kr', ''),
(4, 2, 'couronne danoise', 'kr', ''),
(4, 3, 'Dänische Krone', 'kr', ''),
(4, 4, 'Deense kroon', 'kr', ''),
(4, 5, 'corona danese', 'kr', ''),
(4, 6, 'corona danesa', 'kr', ''),
(5, 1, 'Swedish Krona', 'kr', ''),
(5, 2, 'couronne suédoise', 'kr', ''),
(5, 3, 'Schwedische Krone', 'kr', ''),
(5, 4, 'Zweedse kroon', 'kr', ''),
(5, 5, 'corona svedese', 'kr', ''),
(5, 6, 'corona sueca', 'kr', ''),
(6, 1, 'Czech Koruna', 'Kč', ''),
(6, 2, 'couronne tchèque', 'Kč', ''),
(6, 3, 'Tschechische Krone', 'Kč', ''),
(6, 4, 'Tsjechische kroon', 'Kč', ''),
(6, 5, 'corona ceca', 'Kč', ''),
(6, 6, 'corona checa', 'Kč', ''),
(7, 1, 'Polish Zloty', 'zł', ''),
(7, 2, 'zloty polonais', 'zł', ''),
(7, 3, 'Polnischer Złoty', 'zł', ''),
(7, 4, 'Poolse zloty', 'zł', ''),
(7, 5, 'złoty polacco', 'zł', ''),
(7, 6, 'esloti', 'zł', ''),
(8, 1, 'Hungarian Forint', 'Ft', ''),
(8, 2, 'forint hongrois', 'Ft', ''),
(8, 3, 'Ungarischer Forint', 'Ft', ''),
(8, 4, 'Hongaarse forint', 'Ft', ''),
(8, 5, 'fiorino ungherese', 'Ft', ''),
(8, 6, 'forinto húngaro', 'Ft', ''),
(9, 1, 'Romanian Leu', 'lei', ''),
(9, 2, 'leu roumain', 'L', ''),
(9, 3, 'Rumänischer Leu', 'L', ''),
(9, 4, 'Roemeense leu', 'lei', ''),
(9, 5, 'leu rumeno', 'lei', ''),
(9, 6, 'leu rumano', 'L', ''),
(10, 1, 'Croatian Kuna', 'kn', ''),
(10, 2, 'kuna croate', 'kn', ''),
(10, 3, 'Kroatischer Kuna', 'kn', ''),
(10, 4, 'Kroatische kuna', 'kn', ''),
(10, 5, 'kuna croata', 'kn', ''),
(10, 6, 'kuna', 'kn', ''),
(11, 1, 'Bulgarian Lev', 'BGN', ''),
(11, 2, 'lev bulgare', 'BGN', ''),
(11, 3, 'Bulgarischer Lew', 'BGN', ''),
(11, 4, 'Bulgaarse lev', 'BGN', ''),
(11, 5, 'lev bulgaro', 'BGN', ''),
(11, 6, 'lev búlgaro', 'BGN', ''),
(12, 1, 'Norwegian Krone', 'kr', ''),
(12, 2, 'couronne norvégienne', 'kr', ''),
(12, 3, 'Norwegische Krone', 'kr', ''),
(12, 4, 'Noorse kroon', 'kr', ''),
(12, 5, 'corona norvegese', 'NKr', ''),
(12, 6, 'corona noruega', 'kr', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_currency_shop`
--

DROP TABLE IF EXISTS `ps_currency_shop`;
CREATE TABLE IF NOT EXISTS `ps_currency_shop` (
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL,
  PRIMARY KEY (`id_currency`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_currency_shop`
--

INSERT INTO `ps_currency_shop` (`id_currency`, `id_shop`, `conversion_rate`) VALUES
(1, 1, '1.000000'),
(2, 1, '0.850000'),
(3, 1, '0.980000'),
(4, 1, '7.430000'),
(5, 1, '10.870000'),
(6, 1, '24.370000'),
(7, 1, '4.670000'),
(8, 1, '409.590000'),
(9, 1, '4.920000'),
(10, 1, '7.550000'),
(11, 1, '1.970000'),
(12, 1, '10.280000');

-- --------------------------------------------------------

--
-- Structure de la table `ps_customer`
--

DROP TABLE IF EXISTS `ps_customer`;
CREATE TABLE IF NOT EXISTS `ps_customer` (
  `id_customer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_gender` int(10) UNSIGNED NOT NULL,
  `id_default_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED DEFAULT NULL,
  `id_risk` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `company` varchar(255) DEFAULT NULL,
  `siret` varchar(14) DEFAULT NULL,
  `ape` varchar(5) DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `birthday` date DEFAULT NULL,
  `newsletter` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ip_registration_newsletter` varchar(15) DEFAULT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `optin` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `website` varchar(128) DEFAULT NULL,
  `outstanding_allow_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `show_public_prices` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `max_payment_days` int(10) UNSIGNED NOT NULL DEFAULT '60',
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `note` text,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_guest` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `reset_password_token` varchar(40) DEFAULT NULL,
  `reset_password_validity` datetime DEFAULT NULL,
  PRIMARY KEY (`id_customer`),
  KEY `customer_email` (`email`),
  KEY `customer_login` (`email`,`passwd`),
  KEY `id_customer_passwd` (`id_customer`,`passwd`),
  KEY `id_gender` (`id_gender`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_shop` (`id_shop`,`date_add`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_customer`
--

INSERT INTO `ps_customer` (`id_customer`, `id_shop_group`, `id_shop`, `id_gender`, `id_default_group`, `id_lang`, `id_risk`, `company`, `siret`, `ape`, `firstname`, `lastname`, `email`, `passwd`, `last_passwd_gen`, `birthday`, `newsletter`, `ip_registration_newsletter`, `newsletter_date_add`, `optin`, `website`, `outstanding_allow_amount`, `show_public_prices`, `max_payment_days`, `secure_key`, `note`, `active`, `is_guest`, `deleted`, `date_add`, `date_upd`, `reset_password_token`, `reset_password_validity`) VALUES
(1, 1, 1, 1, 3, 1, 0, '', '', '', 'Anonymous', 'Anonymous', 'anonymous@psgdpr.com', '$2y$10$A/My3MmAGa2YXwOFz2PmXOOlWMAlehAzRGCEZNLq3swDq4O1P2m/y', '2022-11-21 04:05:59', '0000-00-00', 0, '', '0000-00-00 00:00:00', 0, '', '0.000000', 0, 0, '37d83e614b2045baaec966a9082ff0a9', '', 0, 0, 0, '2022-11-21 11:05:59', '2022-11-21 11:05:59', '', '0000-00-00 00:00:00'),
(2, 1, 1, 1, 3, 1, 0, '', '', '', 'John', 'DOE', 'pub@prestashop.com', '0219be7a146e07fc1863edd0720ffef8', '2022-11-21 04:07:24', '1970-01-15', 1, '', '2013-12-13 08:19:15', 1, '', '0.000000', 0, 0, '14bc4659d63fd66729618ae856ae40fb', '', 1, 0, 0, '2022-11-21 11:07:24', '2022-11-21 11:07:24', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `ps_customer_group`
--

DROP TABLE IF EXISTS `ps_customer_group`;
CREATE TABLE IF NOT EXISTS `ps_customer_group` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_customer`,`id_group`),
  KEY `customer_login` (`id_group`),
  KEY `id_customer` (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_customer_group`
--

INSERT INTO `ps_customer_group` (`id_customer`, `id_group`) VALUES
(1, 3),
(2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `ps_customer_message`
--

DROP TABLE IF EXISTS `ps_customer_message`;
CREATE TABLE IF NOT EXISTS `ps_customer_message` (
  `id_customer_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer_thread` int(11) DEFAULT NULL,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `message` mediumtext NOT NULL,
  `file_name` varchar(18) DEFAULT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `user_agent` varchar(128) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_customer_message`),
  KEY `id_customer_thread` (`id_customer_thread`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_customer_message_sync_imap`
--

DROP TABLE IF EXISTS `ps_customer_message_sync_imap`;
CREATE TABLE IF NOT EXISTS `ps_customer_message_sync_imap` (
  `md5_header` varbinary(32) NOT NULL,
  KEY `md5_header_index` (`md5_header`(4))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_customer_session`
--

DROP TABLE IF EXISTS `ps_customer_session`;
CREATE TABLE IF NOT EXISTS `ps_customer_session` (
  `id_customer_session` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) UNSIGNED DEFAULT NULL,
  `token` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_customer_session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_customer_thread`
--

DROP TABLE IF EXISTS `ps_customer_thread`;
CREATE TABLE IF NOT EXISTS `ps_customer_thread` (
  `id_customer_thread` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_contact` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED DEFAULT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `id_product` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('open','closed','pending1','pending2') NOT NULL DEFAULT 'open',
  `email` varchar(255) NOT NULL,
  `token` varchar(12) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_customer_thread`),
  KEY `id_shop` (`id_shop`),
  KEY `id_lang` (`id_lang`),
  KEY `id_contact` (`id_contact`),
  KEY `id_customer` (`id_customer`),
  KEY `id_order` (`id_order`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_customization`
--

DROP TABLE IF EXISTS `ps_customization`;
CREATE TABLE IF NOT EXISTS `ps_customization` (
  `id_customization` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_address_delivery` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `quantity_refunded` int(11) NOT NULL DEFAULT '0',
  `quantity_returned` int(11) NOT NULL DEFAULT '0',
  `in_cart` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_customization`,`id_cart`,`id_product`,`id_address_delivery`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_cart_product` (`id_cart`,`id_product`,`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_customization_field`
--

DROP TABLE IF EXISTS `ps_customization_field`;
CREATE TABLE IF NOT EXISTS `ps_customization_field` (
  `id_customization_field` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `is_module` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_customization_field`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_customization_field_lang`
--

DROP TABLE IF EXISTS `ps_customization_field_lang`;
CREATE TABLE IF NOT EXISTS `ps_customization_field_lang` (
  `id_customization_field` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_customization_field`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_customized_data`
--

DROP TABLE IF EXISTS `ps_customized_data`;
CREATE TABLE IF NOT EXISTS `ps_customized_data` (
  `id_customization` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL,
  `index` int(3) NOT NULL,
  `value` varchar(255) NOT NULL,
  `id_module` int(10) NOT NULL DEFAULT '0',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id_customization`,`type`,`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_date_range`
--

DROP TABLE IF EXISTS `ps_date_range`;
CREATE TABLE IF NOT EXISTS `ps_date_range` (
  `id_date_range` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL,
  PRIMARY KEY (`id_date_range`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_delivery`
--

DROP TABLE IF EXISTS `ps_delivery`;
CREATE TABLE IF NOT EXISTS `ps_delivery` (
  `id_delivery` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) UNSIGNED DEFAULT NULL,
  `id_shop_group` int(10) UNSIGNED DEFAULT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_range_price` int(10) UNSIGNED DEFAULT NULL,
  `id_range_weight` int(10) UNSIGNED DEFAULT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL,
  `price` decimal(20,6) NOT NULL,
  PRIMARY KEY (`id_delivery`),
  KEY `id_zone` (`id_zone`),
  KEY `id_carrier` (`id_carrier`,`id_zone`),
  KEY `id_range_price` (`id_range_price`),
  KEY `id_range_weight` (`id_range_weight`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_delivery`
--

INSERT INTO `ps_delivery` (`id_delivery`, `id_shop`, `id_shop_group`, `id_carrier`, `id_range_price`, `id_range_weight`, `id_zone`, `price`) VALUES
(1, NULL, NULL, 2, 0, 1, 1, '5.000000'),
(2, NULL, NULL, 2, 0, 1, 2, '5.000000'),
(3, NULL, NULL, 2, 1, 0, 1, '5.000000'),
(4, NULL, NULL, 2, 1, 0, 2, '5.000000'),
(5, NULL, NULL, 3, 2, 0, 1, '3.000000'),
(6, NULL, NULL, 3, 2, 0, 2, '4.000000'),
(7, NULL, NULL, 3, 3, 0, 1, '1.000000'),
(8, NULL, NULL, 3, 3, 0, 2, '2.000000'),
(9, NULL, NULL, 3, 4, 0, 1, '0.000000'),
(10, NULL, NULL, 3, 4, 0, 2, '0.000000'),
(11, NULL, NULL, 4, 0, 2, 1, '0.000000'),
(12, NULL, NULL, 4, 0, 2, 2, '0.000000'),
(13, NULL, NULL, 4, 0, 3, 1, '2.000000'),
(14, NULL, NULL, 4, 0, 3, 2, '3.000000'),
(15, NULL, NULL, 4, 0, 4, 1, '5.000000'),
(16, NULL, NULL, 4, 0, 4, 2, '6.000000');

-- --------------------------------------------------------

--
-- Structure de la table `ps_emailsubscription`
--

DROP TABLE IF EXISTS `ps_emailsubscription`;
CREATE TABLE IF NOT EXISTS `ps_emailsubscription` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `email` varchar(255) NOT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `ip_registration_newsletter` varchar(15) NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `id_lang` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_employee`
--

DROP TABLE IF EXISTS `ps_employee`;
CREATE TABLE IF NOT EXISTS `ps_employee` (
  `id_employee` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stats_date_from` date DEFAULT NULL,
  `stats_date_to` date DEFAULT NULL,
  `stats_compare_from` date DEFAULT NULL,
  `stats_compare_to` date DEFAULT NULL,
  `stats_compare_option` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `preselect_date_range` varchar(32) DEFAULT NULL,
  `bo_color` varchar(32) DEFAULT NULL,
  `bo_theme` varchar(32) DEFAULT NULL,
  `bo_css` varchar(64) DEFAULT NULL,
  `default_tab` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bo_width` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bo_menu` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `optin` tinyint(1) UNSIGNED DEFAULT NULL,
  `id_last_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_last_customer_message` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_last_customer` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_connection_date` date DEFAULT NULL,
  `reset_password_token` varchar(40) DEFAULT NULL,
  `reset_password_validity` datetime DEFAULT NULL,
  `has_enabled_gravatar` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_employee`),
  KEY `employee_login` (`email`,`passwd`),
  KEY `id_employee_passwd` (`id_employee`,`passwd`),
  KEY `id_profile` (`id_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_employee`
--

INSERT INTO `ps_employee` (`id_employee`, `id_profile`, `id_lang`, `lastname`, `firstname`, `email`, `passwd`, `last_passwd_gen`, `stats_date_from`, `stats_date_to`, `stats_compare_from`, `stats_compare_to`, `stats_compare_option`, `preselect_date_range`, `bo_color`, `bo_theme`, `bo_css`, `default_tab`, `bo_width`, `bo_menu`, `active`, `optin`, `id_last_order`, `id_last_customer_message`, `id_last_customer`, `last_connection_date`, `reset_password_token`, `reset_password_validity`, `has_enabled_gravatar`) VALUES
(1, 1, 1, 'Noel', 'Maxime', 'test@test.com', '$2y$10$SLNFZc/8T0P6kvF6oglAiu/5gRD4BJc/m5PXsOkzos9Fx4Dw.ZZh6', '2022-11-21 04:03:40', '2022-10-21', '2022-11-21', '0000-00-00', '0000-00-00', 1, NULL, NULL, 'default', 'theme.css', 1, 0, 1, 1, NULL, 5, 0, 2, '2022-12-11', NULL, '0000-00-00 00:00:00', 0),
(2, 2, 1, 'Stark', 'Ned', 'test2@test.com', '$2y$10$BALdnZydJSj9cAHypzcIteYmfCmJxhqX86FtkSs9ZaOp2oiMKtuxu', '2022-12-11 09:43:02', '2022-11-11', '2022-12-11', '0000-00-00', '0000-00-00', 1, NULL, NULL, NULL, 'theme.css', 4, 0, 1, 1, NULL, 5, 0, 2, NULL, NULL, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_employee_session`
--

DROP TABLE IF EXISTS `ps_employee_session`;
CREATE TABLE IF NOT EXISTS `ps_employee_session` (
  `id_employee_session` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `token` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_employee_session`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_employee_session`
--

INSERT INTO `ps_employee_session` (`id_employee_session`, `id_employee`, `token`) VALUES
(5, 1, 'b9f41a3e8ab49cc8692ccfd1f60025f525e9a2a0');

-- --------------------------------------------------------

--
-- Structure de la table `ps_employee_shop`
--

DROP TABLE IF EXISTS `ps_employee_shop`;
CREATE TABLE IF NOT EXISTS `ps_employee_shop` (
  `id_employee` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_employee`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_employee_shop`
--

INSERT INTO `ps_employee_shop` (`id_employee`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_fb_category_match`
--

DROP TABLE IF EXISTS `ps_fb_category_match`;
CREATE TABLE IF NOT EXISTS `ps_fb_category_match` (
  `id_category` int(11) NOT NULL,
  `google_category_id` int(64) NOT NULL,
  `google_category_name` varchar(255) NOT NULL,
  `google_category_parent_id` int(64) NOT NULL,
  `google_category_parent_name` varchar(255) NOT NULL,
  `is_parent_category` tinyint(1) DEFAULT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_category`,`id_shop`),
  KEY `id_category` (`id_category`,`google_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_feature`
--

DROP TABLE IF EXISTS `ps_feature`;
CREATE TABLE IF NOT EXISTS `ps_feature` (
  `id_feature` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_feature`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_feature`
--

INSERT INTO `ps_feature` (`id_feature`, `position`) VALUES
(1, 0),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_feature_flag`
--

DROP TABLE IF EXISTS `ps_feature_flag`;
CREATE TABLE IF NOT EXISTS `ps_feature_flag` (
  `id_feature_flag` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `label_wording` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `label_domain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description_wording` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description_domain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id_feature_flag`),
  UNIQUE KEY `UNIQ_91700F175E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_feature_flag`
--

INSERT INTO `ps_feature_flag` (`id_feature_flag`, `name`, `state`, `label_wording`, `label_domain`, `description_wording`, `description_domain`) VALUES
(1, 'product_page_v2', 0, 'Experimental product page', 'Admin.Advparameters.Feature', 'This page benefits from increased performance and includes new features such as a new combination management system. Please note this is a work in progress and some features are not available', 'Admin.Advparameters.Help');

-- --------------------------------------------------------

--
-- Structure de la table `ps_feature_lang`
--

DROP TABLE IF EXISTS `ps_feature_lang`;
CREATE TABLE IF NOT EXISTS `ps_feature_lang` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_feature`,`id_lang`),
  KEY `id_lang` (`id_lang`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_feature_lang`
--

INSERT INTO `ps_feature_lang` (`id_feature`, `id_lang`, `name`) VALUES
(1, 1, 'Composition'),
(2, 1, 'Property'),
(1, 2, 'Composition'),
(2, 2, 'Propriété'),
(1, 3, 'Composition'),
(2, 3, 'Property'),
(1, 4, 'Composition'),
(2, 4, 'Property'),
(1, 5, 'Composition'),
(2, 5, 'Property'),
(1, 6, 'Composition'),
(2, 6, 'Property');

-- --------------------------------------------------------

--
-- Structure de la table `ps_feature_product`
--

DROP TABLE IF EXISTS `ps_feature_product`;
CREATE TABLE IF NOT EXISTS `ps_feature_product` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_feature_value` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_feature`,`id_product`,`id_feature_value`),
  KEY `id_feature_value` (`id_feature_value`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_feature_shop`
--

DROP TABLE IF EXISTS `ps_feature_shop`;
CREATE TABLE IF NOT EXISTS `ps_feature_shop` (
  `id_feature` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_feature`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_feature_shop`
--

INSERT INTO `ps_feature_shop` (`id_feature`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_feature_value`
--

DROP TABLE IF EXISTS `ps_feature_value`;
CREATE TABLE IF NOT EXISTS `ps_feature_value` (
  `id_feature_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_feature` int(10) UNSIGNED NOT NULL,
  `custom` tinyint(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`),
  KEY `feature` (`id_feature`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_feature_value`
--

INSERT INTO `ps_feature_value` (`id_feature_value`, `id_feature`, `custom`) VALUES
(1, 1, 0),
(2, 1, 0),
(3, 1, 0),
(4, 1, 0),
(5, 1, 0),
(6, 1, 0),
(7, 2, 0),
(8, 2, 0),
(9, 2, 0),
(10, 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_feature_value_lang`
--

DROP TABLE IF EXISTS `ps_feature_value_lang`;
CREATE TABLE IF NOT EXISTS `ps_feature_value_lang` (
  `id_feature_value` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_feature_value_lang`
--

INSERT INTO `ps_feature_value_lang` (`id_feature_value`, `id_lang`, `value`) VALUES
(1, 1, 'Polyester'),
(1, 2, 'Polyester'),
(1, 3, 'Polyester'),
(1, 4, 'Polyester'),
(1, 5, 'Poliestere'),
(1, 6, 'Poliéster'),
(2, 1, 'Wool'),
(2, 2, 'Laine'),
(2, 3, 'Wolle'),
(2, 4, 'Wol'),
(2, 5, 'Lana'),
(2, 6, 'Lana'),
(3, 1, 'Ceramic'),
(3, 2, 'Céramique'),
(3, 3, 'Ceramic'),
(3, 4, 'Ceramic'),
(3, 5, 'Ceramic'),
(3, 6, 'Ceramic'),
(4, 1, 'Cotton'),
(4, 2, 'Coton'),
(4, 3, 'Baumwolle'),
(4, 4, 'Katoen'),
(4, 5, 'Cotone'),
(4, 6, 'Algodón'),
(5, 1, 'Recycled cardboard'),
(5, 2, 'Carton recyclé'),
(5, 3, 'Recycled cardboard'),
(5, 4, 'Recycled cardboard'),
(5, 5, 'Recycled cardboard'),
(5, 6, 'Recycled cardboard'),
(6, 1, 'Matt paper'),
(6, 2, 'Papier mat'),
(6, 3, 'Matt paper'),
(6, 4, 'Matt paper'),
(6, 5, 'Matt paper'),
(6, 6, 'Matt paper'),
(7, 1, 'Long sleeves'),
(7, 2, 'Manches longues'),
(7, 3, 'Long sleeves'),
(7, 4, 'Long sleeves'),
(7, 5, 'Long sleeves'),
(7, 6, 'Long sleeves'),
(8, 1, 'Short sleeves'),
(8, 2, 'Manches courtes'),
(8, 3, 'Short sleeves'),
(8, 4, 'Short sleeves'),
(8, 5, 'Short sleeves'),
(8, 6, 'Short sleeves'),
(9, 1, 'Removable cover'),
(9, 2, 'Housse amovible'),
(9, 3, 'Removable cover'),
(9, 4, 'Removable cover'),
(9, 5, 'Removable cover'),
(9, 6, 'Removable cover'),
(10, 1, '120 pages'),
(10, 2, '120 pages'),
(10, 3, '120 pages'),
(10, 4, '120 pages'),
(10, 5, '120 pages'),
(10, 6, '120 pages');

-- --------------------------------------------------------

--
-- Structure de la table `ps_gender`
--

DROP TABLE IF EXISTS `ps_gender`;
CREATE TABLE IF NOT EXISTS `ps_gender` (
  `id_gender` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_gender`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_gender`
--

INSERT INTO `ps_gender` (`id_gender`, `type`) VALUES
(1, 0),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_gender_lang`
--

DROP TABLE IF EXISTS `ps_gender_lang`;
CREATE TABLE IF NOT EXISTS `ps_gender_lang` (
  `id_gender` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_gender`,`id_lang`),
  KEY `id_gender` (`id_gender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_gender_lang`
--

INSERT INTO `ps_gender_lang` (`id_gender`, `id_lang`, `name`) VALUES
(1, 1, 'Mr.'),
(1, 2, 'M'),
(1, 3, 'Herr'),
(1, 4, 'Dhr.'),
(1, 5, 'Sig.'),
(1, 6, 'Sr.'),
(2, 1, 'Mrs.'),
(2, 2, 'Mme'),
(2, 3, 'Frau'),
(2, 4, 'Mw.'),
(2, 5, 'Sig.ra'),
(2, 6, 'Sra.');

-- --------------------------------------------------------

--
-- Structure de la table `ps_group`
--

DROP TABLE IF EXISTS `ps_group`;
CREATE TABLE IF NOT EXISTS `ps_group` (
  `id_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `reduction` decimal(5,2) NOT NULL DEFAULT '0.00',
  `price_display_method` tinyint(4) NOT NULL DEFAULT '0',
  `show_prices` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_group`
--

INSERT INTO `ps_group` (`id_group`, `reduction`, `price_display_method`, `show_prices`, `date_add`, `date_upd`) VALUES
(1, '0.00', 0, 1, '2022-11-21 11:03:35', '2022-11-21 11:03:35'),
(2, '0.00', 0, 1, '2022-11-21 11:03:35', '2022-11-21 11:03:35'),
(3, '0.00', 0, 1, '2022-11-21 11:03:35', '2022-11-21 11:03:35');

-- --------------------------------------------------------

--
-- Structure de la table `ps_group_lang`
--

DROP TABLE IF EXISTS `ps_group_lang`;
CREATE TABLE IF NOT EXISTS `ps_group_lang` (
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_group`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_group_lang`
--

INSERT INTO `ps_group_lang` (`id_group`, `id_lang`, `name`) VALUES
(1, 1, 'Visitor'),
(1, 2, 'Visiteur'),
(1, 3, 'Besucher'),
(1, 4, 'Bezoeker'),
(1, 5, 'Visitatore'),
(1, 6, 'Visitante'),
(2, 1, 'Guest'),
(2, 2, 'Invité'),
(2, 3, 'Gast'),
(2, 4, 'Gast'),
(2, 5, 'Ospite'),
(2, 6, 'Invitado'),
(3, 1, 'Customer'),
(3, 2, 'Client'),
(3, 3, 'Kunde'),
(3, 4, 'Klant'),
(3, 5, 'Cliente'),
(3, 6, 'Cliente');

-- --------------------------------------------------------

--
-- Structure de la table `ps_group_reduction`
--

DROP TABLE IF EXISTS `ps_group_reduction`;
CREATE TABLE IF NOT EXISTS `ps_group_reduction` (
  `id_group_reduction` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(5,4) NOT NULL,
  PRIMARY KEY (`id_group_reduction`),
  UNIQUE KEY `id_group` (`id_group`,`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_group_shop`
--

DROP TABLE IF EXISTS `ps_group_shop`;
CREATE TABLE IF NOT EXISTS `ps_group_shop` (
  `id_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_group`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_group_shop`
--

INSERT INTO `ps_group_shop` (`id_group`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_gsitemap_sitemap`
--

DROP TABLE IF EXISTS `ps_gsitemap_sitemap`;
CREATE TABLE IF NOT EXISTS `ps_gsitemap_sitemap` (
  `link` varchar(255) DEFAULT NULL,
  `id_shop` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_guest`
--

DROP TABLE IF EXISTS `ps_guest`;
CREATE TABLE IF NOT EXISTS `ps_guest` (
  `id_guest` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operating_system` int(10) UNSIGNED DEFAULT NULL,
  `id_web_browser` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED DEFAULT NULL,
  `javascript` tinyint(1) DEFAULT '0',
  `screen_resolution_x` smallint(5) UNSIGNED DEFAULT NULL,
  `screen_resolution_y` smallint(5) UNSIGNED DEFAULT NULL,
  `screen_color` tinyint(3) UNSIGNED DEFAULT NULL,
  `sun_java` tinyint(1) DEFAULT NULL,
  `adobe_flash` tinyint(1) DEFAULT NULL,
  `adobe_director` tinyint(1) DEFAULT NULL,
  `apple_quicktime` tinyint(1) DEFAULT NULL,
  `real_player` tinyint(1) DEFAULT NULL,
  `windows_media` tinyint(1) DEFAULT NULL,
  `accept_language` varchar(8) DEFAULT NULL,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_guest`),
  KEY `id_customer` (`id_customer`),
  KEY `id_operating_system` (`id_operating_system`),
  KEY `id_web_browser` (`id_web_browser`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_guest`
--

INSERT INTO `ps_guest` (`id_guest`, `id_operating_system`, `id_web_browser`, `id_customer`, `javascript`, `screen_resolution_x`, `screen_resolution_y`, `screen_color`, `sun_java`, `adobe_flash`, `adobe_director`, `apple_quicktime`, `real_player`, `windows_media`, `accept_language`, `mobile_theme`) VALUES
(1, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(3, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_homeslider`
--

DROP TABLE IF EXISTS `ps_homeslider`;
CREATE TABLE IF NOT EXISTS `ps_homeslider` (
  `id_homeslider_slides` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_homeslider_slides`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_homeslider`
--

INSERT INTO `ps_homeslider` (`id_homeslider_slides`, `id_shop`) VALUES
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_homeslider_slides`
--

DROP TABLE IF EXISTS `ps_homeslider_slides`;
CREATE TABLE IF NOT EXISTS `ps_homeslider_slides` (
  `id_homeslider_slides` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_homeslider_slides`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_homeslider_slides`
--

INSERT INTO `ps_homeslider_slides` (`id_homeslider_slides`, `position`, `active`) VALUES
(4, 2, 1),
(5, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_homeslider_slides_lang`
--

DROP TABLE IF EXISTS `ps_homeslider_slides_lang`;
CREATE TABLE IF NOT EXISTS `ps_homeslider_slides_lang` (
  `id_homeslider_slides` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `legend` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id_homeslider_slides`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_homeslider_slides_lang`
--

INSERT INTO `ps_homeslider_slides_lang` (`id_homeslider_slides`, `id_lang`, `title`, `description`, `legend`, `url`, `image`) VALUES
(4, 1, 'Dungeons & dragons', '', '', 'http://localhost/prestashop/fr/jeux-de-role/20-dungeons-dragons-kit-d-initiation.html', '3cd7e0b8aa0c83d5973fe889961ce0a21cbb594a_tiamat-email.jpg'),
(4, 2, 'Donjons & Dragons', '', '', 'http://localhost/prestashop/fr/jeux-de-role/20-dungeons-dragons-kit-d-initiation.html', '64fb3e6c9e9d3bce5e55248e534a088d522506bb_tiamat-email.jpg'),
(4, 3, '', '', '', '/prestashop/', ''),
(4, 4, '', '', '', '/prestashop/', ''),
(4, 5, '', '', '', '/prestashop/', ''),
(4, 6, 'Dungeons & dragons', '', '', 'http://localhost/prestashop/fr/jeux-de-role/20-dungeons-dragons-kit-d-initiation.html', '3cd7e0b8aa0c83d5973fe889961ce0a21cbb594a_tiamat-email.jpg'),
(5, 1, 'The Lord of The Rings', '', '', 'http://localhost/prestashop/fr/accueil/24-le-seigneur-des-anneaux-jce-edition-revisee.html', '501e86d1ce213f277a6df167f2b7aa69c6a71530_LeSeigneurDesAnneaux-retourduroi_Header.jpg'),
(5, 2, 'Le seigneur des anneaux', '', '', 'http://localhost/prestashop/fr/accueil/24-le-seigneur-des-anneaux-jce-edition-revisee.html', 'f8aa402c729017af408b51046f948084cc671346_LeSeigneurDesAnneaux-retourduroi_Header.jpg'),
(5, 3, '', '', '', '/prestashop/', ''),
(5, 4, '', '', '', '/prestashop/', ''),
(5, 5, '', '', '', '/prestashop/', ''),
(5, 6, 'Le seigneur des anneaux', '', '', 'http://localhost/prestashop/fr/accueil/24-le-seigneur-des-anneaux-jce-edition-revisee.html', '7ed5f3ded2b9aeff3fe5f3891aa5e597303f2b6a_LeSeigneurDesAnneaux-retourduroi_Header.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `ps_hook`
--

DROP TABLE IF EXISTS `ps_hook`;
CREATE TABLE IF NOT EXISTS `ps_hook` (
  `id_hook` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `position` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_hook`),
  UNIQUE KEY `hook_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=769 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_hook`
--

INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `active`, `position`) VALUES
(1, 'actionValidateOrder', 'New orders', '', 1, 1),
(2, 'displayMaintenance', 'Maintenance Page', 'This hook displays new elements on the maintenance page', 1, 1),
(3, 'displayCartModalContent', 'Content of Add-to-cart modal', 'This hook displays content in the middle of the window that appears after adding product to cart', 1, 1),
(4, 'displayCartModalFooter', 'Bottom of Add-to-cart modal', 'This hook displays content in the bottom of window that appears after adding product to cart', 1, 1),
(5, 'displayProductPageDrawer', 'Product Page Drawer', 'This hook displays content in the right sidebar of the product page', 1, 1),
(6, 'actionPaymentConfirmation', 'Payment confirmation', 'This hook displays new elements after the payment is validated', 1, 1),
(7, 'displayPaymentReturn', 'Payment return', '', 1, 1),
(8, 'actionUpdateQuantity', 'Quantity update', 'Quantity is updated only when a customer effectively places their order', 1, 1),
(9, 'displayRightColumn', 'Right column blocks', 'This hook displays new elements in the right-hand column', 1, 1),
(10, 'displayWrapperTop', 'Main wrapper section (top)', 'This hook displays new elements in the top of the main wrapper', 1, 1),
(11, 'displayWrapperBottom', 'Main wrapper section (bottom)', 'This hook displays new elements in the bottom of the main wrapper', 1, 1),
(12, 'displayContentWrapperTop', 'Content wrapper section (top)', 'This hook displays new elements in the top of the content wrapper', 1, 1),
(13, 'displayContentWrapperBottom', 'Content wrapper section (bottom)', 'This hook displays new elements in the bottom of the content wrapper', 1, 1),
(14, 'displayLeftColumn', 'Left column blocks', 'This hook displays new elements in the left-hand column', 1, 1),
(15, 'displayHome', 'Homepage content', 'This hook displays new elements on the homepage', 1, 1),
(16, 'displayHeader', 'Pages html head section', 'This hook adds additional elements in the head section of your pages (head section of html)', 1, 1),
(17, 'actionCartSave', 'Cart creation and update', 'This hook is displayed when a product is added to the cart or if the cart\'s content is modified', 1, 1),
(18, 'actionAuthentication', 'Successful customer authentication', 'This hook is displayed after a customer successfully signs in', 1, 1),
(19, 'actionProductAdd', 'Product creation', 'This hook is displayed after a product is created', 1, 1),
(20, 'actionProductUpdate', 'Product update', 'This hook is displayed after a product has been updated', 1, 1),
(21, 'displayAfterTitleTag', 'After title tag', 'Use this hook to add content after title tag', 1, 1),
(22, 'displayAfterBodyOpeningTag', 'Very top of pages', 'Use this hook for advertisement or modals you want to load first', 1, 1),
(23, 'displayBanner', 'Very top of pages', 'Use this hook for banners on top of every pages', 1, 1),
(24, 'displayBeforeBodyClosingTag', 'Very bottom of pages', 'Use this hook for your modals or any content you want to load at the very end', 1, 1),
(25, 'displayTop', 'Top of pages', 'This hook displays additional elements at the top of your pages', 1, 1),
(26, 'displayNavFullWidth', 'Navigation', 'This hook displays full width navigation menu at the top of your pages', 1, 1),
(27, 'displayRightColumnProduct', 'New elements on the product page (right column)', 'This hook displays new elements in the right-hand column of the product page', 1, 1),
(28, 'actionProductDelete', 'Product deletion', 'This hook is called when a product is deleted', 1, 1),
(29, 'actionObjectProductInCartDeleteBefore', 'Cart product removal', 'This hook is called before a product is removed from a cart', 1, 1),
(30, 'actionObjectProductInCartDeleteAfter', 'Cart product removal', 'This hook is called after a product is removed from a cart', 1, 1),
(31, 'displayFooterProduct', 'Product footer', 'This hook adds new blocks under the product\'s description', 1, 1),
(32, 'displayInvoice', 'Invoice', 'This hook displays new blocks on the invoice (order)', 1, 1),
(33, 'actionOrderStatusUpdate', 'Order status update - Event', 'This hook launches modules when the status of an order changes', 1, 1),
(34, 'displayAdminGridTableBefore', 'Display before Grid table', 'This hook adds new blocks before Grid component table', 1, 1),
(35, 'displayAdminGridTableAfter', 'Display after Grid table', 'This hook adds new blocks after Grid component table', 1, 1),
(36, 'displayAdminOrder', 'Display new elements in the Back Office, tab AdminOrder', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office', 1, 1),
(37, 'displayAdminOrderTabOrder', 'Display new elements in Back Office, AdminOrder, panel Order', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel tabs', 1, 1),
(38, 'displayAdminOrderTabShip', 'Display new elements in Back Office, AdminOrder, panel Shipping', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel tabs', 1, 1),
(39, 'displayAdminOrderContentOrder', 'Display new elements in Back Office, AdminOrder, panel Order', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel content', 1, 1),
(40, 'displayAdminOrderContentShip', 'Display new elements in Back Office, AdminOrder, panel Shipping', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel content', 1, 1),
(41, 'displayFooter', 'Footer', 'This hook displays new blocks in the footer', 1, 1),
(42, 'displayPDFInvoice', 'PDF Invoice', 'This hook allows you to display additional information on PDF invoices', 1, 1),
(43, 'displayInvoiceLegalFreeText', 'PDF Invoice - Legal Free Text', 'This hook allows you to modify the legal free text on PDF invoices', 1, 1),
(44, 'displayAdminCustomers', 'Display new elements in the Back Office, tab AdminCustomers', 'This hook launches modules when the AdminCustomers tab is displayed in the Back Office', 1, 1),
(45, 'displayAdminCustomersAddressesItemAction', 'Display new elements in the Back Office, tab AdminCustomers, Addresses actions', 'This hook launches modules when the Addresses list into the AdminCustomers tab is displayed in the Back Office', 1, 1),
(46, 'displayOrderConfirmation', 'Order confirmation page', 'This hook is called within an order\'s confirmation page', 1, 1),
(47, 'actionCustomerAccountAdd', 'Successful customer account creation', 'This hook is called when a new customer creates an account successfully', 1, 1),
(48, 'actionCustomerAccountUpdate', 'Successful customer account update', 'This hook is called when a customer updates its account successfully', 1, 1),
(49, 'displayCustomerAccount', 'Customer account displayed in Front Office', 'This hook displays new elements on the customer account page', 1, 1),
(50, 'actionOrderSlipAdd', 'Order slip creation', 'This hook is called when a new credit slip is added regarding client order', 1, 1),
(51, 'displayShoppingCartFooter', 'Shopping cart footer', 'This hook displays some specific information on the shopping cart\'s page', 1, 1),
(52, 'displayCreateAccountEmailFormBottom', 'Customer authentication form', 'This hook displays some information on the bottom of the email form', 1, 1),
(53, 'displayAuthenticateFormBottom', 'Customer authentication form', 'This hook displays some information on the bottom of the authentication form', 1, 1),
(54, 'displayCustomerAccountForm', 'Customer account creation form', 'This hook displays some information on the form to create a customer account', 1, 1),
(55, 'displayAdminStatsModules', 'Stats - Modules', '', 1, 1),
(56, 'displayAdminStatsGraphEngine', 'Graph engines', '', 1, 1),
(57, 'actionOrderReturn', 'Returned product', 'This hook is displayed when a customer returns a product ', 1, 1),
(58, 'displayProductAdditionalInfo', 'Product page additional info', 'This hook adds additional information on the product page', 1, 1),
(59, 'displayBackOfficeHome', 'Administration panel homepage', 'This hook is displayed on the admin panel\'s homepage', 1, 1),
(60, 'displayAdminStatsGridEngine', 'Grid engines', '', 1, 1),
(61, 'actionWatermark', 'Watermark', '', 1, 1),
(62, 'actionProductCancel', 'Product cancelled', 'This hook is called when you cancel a product in an order', 1, 1),
(63, 'displayLeftColumnProduct', 'New elements on the product page (left column)', 'This hook displays new elements in the left-hand column of the product page', 1, 1),
(64, 'actionProductOutOfStock', 'Out-of-stock product', 'This hook displays new action buttons if a product is out of stock', 1, 1),
(65, 'actionProductAttributeUpdate', 'Product attribute update', 'This hook is displayed when a product\'s attribute is updated', 1, 1),
(66, 'displayCarrierList', 'Extra carrier (module mode)', '', 1, 1),
(67, 'displayShoppingCart', 'Shopping cart - Additional button', 'This hook displays new action buttons within the shopping cart', 1, 1),
(68, 'actionCarrierUpdate', 'Carrier Update', 'This hook is called when a carrier is updated', 1, 1),
(69, 'actionOrderStatusPostUpdate', 'Post update of order status', '', 1, 1),
(70, 'displayCustomerAccountFormTop', 'Block above the form for create an account', 'This hook is displayed above the customer\'s account creation form', 1, 1),
(71, 'displayBackOfficeHeader', 'Administration panel header', 'This hook is displayed in the header of the admin panel', 1, 1),
(72, 'displayBackOfficeTop', 'Administration panel hover the tabs', 'This hook is displayed on the roll hover of the tabs within the admin panel', 1, 1),
(73, 'displayAdminEndContent', 'Administration end of content', 'This hook is displayed at the end of the main content, before the footer', 1, 1),
(74, 'displayBackOfficeFooter', 'Administration panel footer', 'This hook is displayed within the admin panel\'s footer', 1, 1),
(75, 'actionProductAttributeDelete', 'Product attribute deletion', 'This hook is displayed when a product\'s attribute is deleted', 1, 1),
(76, 'actionCarrierProcess', 'Carrier process', '', 1, 1),
(77, 'displayBeforeCarrier', 'Before carriers list', 'This hook is displayed before the carrier list in Front Office', 1, 1),
(78, 'displayAfterCarrier', 'After carriers list', 'This hook is displayed after the carrier list in Front Office', 1, 1),
(79, 'displayOrderDetail', 'Order detail', 'This hook is displayed within the order\'s details in Front Office', 1, 1),
(80, 'actionPaymentCCAdd', 'Payment CC added', '', 1, 1),
(81, 'actionCategoryAdd', 'Category creation', 'This hook is displayed when a category is created', 1, 1),
(82, 'actionCategoryUpdate', 'Category modification', 'This hook is displayed when a category is modified', 1, 1),
(83, 'actionCategoryDelete', 'Category deletion', 'This hook is displayed when a category is deleted', 1, 1),
(84, 'displayPaymentTop', 'Top of payment page', 'This hook is displayed at the top of the payment page', 1, 1),
(85, 'actionHtaccessCreate', 'After htaccess creation', 'This hook is displayed after the htaccess creation', 1, 1),
(86, 'actionAdminMetaSave', 'After saving the configuration in AdminMeta', 'This hook is displayed after saving the configuration in AdminMeta', 1, 1),
(87, 'displayAttributeGroupForm', 'Add fields to the form \'attribute group\'', 'This hook adds fields to the form \'attribute group\'', 1, 1),
(88, 'actionAttributeGroupSave', 'Saving an attribute group', 'This hook is called while saving an attributes group', 1, 1),
(89, 'actionAttributeGroupDelete', 'Deleting attribute group', 'This hook is called while deleting an attributes  group', 1, 1),
(90, 'displayFeatureForm', 'Add fields to the form \'feature\'', 'This hook adds fields to the form \'feature\'', 1, 1),
(91, 'actionFeatureSave', 'Saving attributes\' features', 'This hook is called while saving an attributes features', 1, 1),
(92, 'actionFeatureDelete', 'Deleting attributes\' features', 'This hook is called while deleting an attributes features', 1, 1),
(93, 'actionProductSave', 'Saving products', 'This hook is called while saving products', 1, 1),
(94, 'displayAttributeGroupPostProcess', 'On post-process in admin attribute group', 'This hook is called on post-process in admin attribute group', 1, 1),
(95, 'displayFeaturePostProcess', 'On post-process in admin feature', 'This hook is called on post-process in admin feature', 1, 1),
(96, 'displayFeatureValueForm', 'Add fields to the form \'feature value\'', 'This hook adds fields to the form \'feature value\'', 1, 1),
(97, 'displayFeatureValuePostProcess', 'On post-process in admin feature value', 'This hook is called on post-process in admin feature value', 1, 1),
(98, 'actionFeatureValueDelete', 'Deleting attributes\' features\' values', 'This hook is called while deleting an attributes features value', 1, 1),
(99, 'actionFeatureValueSave', 'Saving an attributes features value', 'This hook is called while saving an attributes features value', 1, 1),
(100, 'displayAttributeForm', 'Add fields to the form \'attribute value\'', 'This hook adds fields to the form \'attribute value\'', 1, 1),
(101, 'actionAttributePostProcess', 'On post-process in admin feature value', 'This hook is called on post-process in admin feature value', 1, 1),
(102, 'actionAttributeDelete', 'Deleting an attributes features value', 'This hook is called while deleting an attributes features value', 1, 1),
(103, 'actionAttributeSave', 'Saving an attributes features value', 'This hook is called while saving an attributes features value', 1, 1),
(104, 'actionTaxManager', 'Tax Manager Factory', '', 1, 1),
(105, 'displayMyAccountBlock', 'My account block', 'This hook displays extra information within the \'my account\' block\"', 1, 1),
(106, 'actionModuleInstallBefore', 'actionModuleInstallBefore', '', 1, 1),
(107, 'actionModuleInstallAfter', 'actionModuleInstallAfter', '', 1, 1),
(108, 'actionModuleUninstallBefore', 'actionModuleUninstallBefore', '', 1, 1),
(109, 'actionModuleUninstallAfter', 'actionModuleUninstallAfter', '', 1, 1),
(110, 'displayTopColumn', 'Top column blocks', 'This hook displays new elements in the top of columns', 1, 1),
(111, 'displayBackOfficeCategory', 'Display new elements in the Back Office, tab AdminCategories', 'This hook launches modules when the AdminCategories tab is displayed in the Back Office', 1, 1),
(112, 'displayProductListFunctionalButtons', 'Display new elements in the Front Office, products list', 'This hook launches modules when the products list is displayed in the Front Office', 1, 1),
(113, 'displayNav', 'Navigation', '', 1, 1),
(114, 'displayOverrideTemplate', 'Change the default template of current controller', '', 1, 1),
(115, 'actionAdminLoginControllerSetMedia', 'Set media on admin login page header', 'This hook is called after adding media to admin login page header', 1, 1),
(116, 'actionOrderEdited', 'Order edited', 'This hook is called when an order is edited', 1, 1),
(117, 'actionEmailAddBeforeContent', 'Add extra content before mail content', 'This hook is called just before fetching mail template', 1, 1),
(118, 'actionEmailAddAfterContent', 'Add extra content after mail content', 'This hook is called just after fetching mail template', 1, 1),
(119, 'sendMailAlterTemplateVars', 'Alter template vars on the fly', 'This hook is called when Mail::send() is called', 1, 1),
(120, 'displayCartExtraProductActions', 'Extra buttons in shopping cart', 'This hook adds extra buttons to the product lines, in the shopping cart', 1, 1),
(121, 'displayPaymentByBinaries', 'Payment form generated by binaries', 'This hook displays form generated by binaries during the checkout', 1, 1),
(122, 'additionalCustomerFormFields', 'Add fields to the Customer form', 'This hook returns an array of FormFields to add them to the customer registration form', 1, 1),
(123, 'additionalCustomerAddressFields', 'Add fields to the Customer address form', 'This hook returns an array of FormFields to add them to the customer address registration form', 1, 1),
(124, 'addWebserviceResources', 'Add extra webservice resource', 'This hook is called when webservice resources list in webservice controller', 1, 1),
(125, 'displayCustomerLoginFormAfter', 'Display elements after login form', 'This hook displays new elements after the login form', 1, 1),
(126, 'actionClearCache', 'Clear smarty cache', 'This hook is called when smarty\'s cache is cleared', 1, 1),
(127, 'actionClearCompileCache', 'Clear smarty compile cache', 'This hook is called when smarty\'s compile cache is cleared', 1, 1),
(128, 'actionClearSf2Cache', 'Clear Sf2 cache', 'This hook is called when the Symfony cache is cleared', 1, 1),
(129, 'actionValidateCustomerAddressForm', 'Customer address form validation', 'This hook is called when a customer submit its address form', 1, 1),
(130, 'displayCarrierExtraContent', 'Display additional content for a carrier (e.g pickup points)', 'This hook calls only the module related to the carrier, in order to add options when needed', 1, 1),
(131, 'validateCustomerFormFields', 'Customer registration form validation', 'This hook is called to a module when it has sent additional fields with additionalCustomerFormFields', 1, 1),
(132, 'displayProductExtraContent', 'Display extra content on the product page', 'This hook expects ProductExtraContent instances, which will be properly displayed by the template on the product page', 1, 1),
(133, 'filterCmsContent', 'Filter the content page', 'This hook is called just before fetching content page', 1, 1),
(134, 'filterCmsCategoryContent', 'Filter the content page category', 'This hook is called just before fetching content page category', 1, 1),
(135, 'filterProductContent', 'Filter the content page product', 'This hook is called just before fetching content page product', 1, 1),
(136, 'filterCategoryContent', 'Filter the content page category', 'This hook is called just before fetching content page category', 1, 1),
(137, 'filterManufacturerContent', 'Filter the content page manufacturer', 'This hook is called just before fetching content page manufacturer', 1, 1),
(138, 'filterSupplierContent', 'Filter the content page supplier', 'This hook is called just before fetching content page supplier', 1, 1),
(139, 'filterHtmlContent', 'Filter HTML field before rending a page', 'This hook is called just before fetching a page on HTML field', 1, 1),
(140, 'displayDashboardTop', 'Dashboard Top', 'Displays the content in the dashboard\'s top area', 1, 1),
(141, 'actionUpdateLangAfter', 'Update \"lang\" tables', 'Update \"lang\" tables after adding or updating a language', 1, 1),
(142, 'actionOutputHTMLBefore', 'Before HTML output', 'This hook is used to filter the whole HTML page before it is rendered (only front)', 1, 1),
(143, 'displayAfterProductThumbs', 'Display extra content below product thumbs', 'This hook displays new elements below product images ex. additional media', 1, 1),
(144, 'actionDispatcherBefore', 'Before dispatch', 'This hook is called at the beginning of the dispatch method of the Dispatcher', 1, 1),
(145, 'actionDispatcherAfter', 'After dispatch', 'This hook is called at the end of the dispatch method of the Dispatcher', 1, 1),
(146, 'filterProductSearch', 'Filter search products result', 'This hook is called in order to allow to modify search product result', 1, 1),
(147, 'actionProductSearchAfter', 'Event triggered after search product completed', 'This hook is called after the product search. Parameters are already filter', 1, 1),
(148, 'actionEmailSendBefore', 'Before sending an email', 'This hook is used to filter the content or the metadata of an email before sending it or even prevent its sending', 1, 1),
(149, 'displayAdminProductsMainStepLeftColumnMiddle', 'Display new elements in back office product page, left column of the Basic settings tab', 'This hook launches modules when the back office product page is displayed', 1, 1),
(150, 'displayAdminProductsMainStepLeftColumnBottom', 'Display new elements in back office product page, left column of the Basic settings tab', 'This hook launches modules when the back office product page is displayed', 1, 1),
(151, 'displayAdminProductsMainStepRightColumnBottom', 'Display new elements in back office product page, right column of the Basic settings tab', 'This hook launches modules when the back office product page is displayed', 1, 1),
(152, 'displayAdminProductsQuantitiesStepBottom', 'Display new elements in back office product page, Quantities/Combinations tab', 'This hook launches modules when the back office product page is displayed', 1, 1),
(153, 'displayAdminProductsPriceStepBottom', 'Display new elements in back office product page, Price tab', 'This hook launches modules when the back office product page is displayed', 1, 1),
(154, 'displayAdminProductsOptionsStepTop', 'Display new elements in back office product page, Options tab', 'This hook launches modules when the back office product page is displayed', 1, 1),
(155, 'displayAdminProductsOptionsStepBottom', 'Display new elements in back office product page, Options tab', 'This hook launches modules when the back office product page is displayed', 1, 1),
(156, 'displayAdminProductsSeoStepBottom', 'Display new elements in back office product page, SEO tab', 'This hook launches modules when the back office product page is displayed', 1, 1),
(157, 'displayAdminProductsShippingStepBottom', 'Display new elements in back office product page, Shipping tab', 'This hook launches modules when the back office product page is displayed', 1, 1),
(158, 'displayAdminProductsExtra', 'Admin Product Extra Module Tab', 'This hook displays extra content in the Module tab on the product edit page', 1, 1),
(159, 'displayAdminProductsCombinationBottom', 'Display new elements in back office product page, Combination tab', 'This hook launches modules when the back office product page is displayed', 1, 1),
(160, 'displayDashboardToolbarTopMenu', 'Display new elements in back office page with a dashboard, on top Menu', 'This hook launches modules when a page with a dashboard is displayed', 1, 1),
(161, 'displayDashboardToolbarIcons', 'Display new elements in back office page with dashboard, on icons list', 'This hook launches modules when the back office with dashboard is displayed', 1, 1),
(162, 'actionBuildFrontEndObject', 'Manage elements added to the \"prestashop\" javascript object', 'This hook allows you to customize the \"prestashop\" javascript object that is included in all front office pages', 1, 1),
(163, 'actionFrontControllerInitAfter', 'Perform actions after front office controller initialization', 'This hook is launched after the initialization of all front office controllers', 1, 1),
(164, 'actionFrontControllerInitBefore', 'Perform actions before front office controller initialization', 'This hook is launched before the initialization of all front office controllers', 1, 1),
(165, 'actionAdminControllerInitAfter', 'Perform actions after admin controller initialization', 'This hook is launched after the initialization of all admin controllers', 1, 1),
(166, 'actionAdminControllerInitBefore', 'Perform actions before admin controller initialization', 'This hook is launched before the initialization of all admin controllers', 1, 1),
(167, 'actionControllerInitAfter', 'Perform actions after controller initialization', 'This hook is launched after the initialization of all controllers', 1, 1),
(168, 'actionControllerInitBefore', 'Perform actions before controller initialization', 'This hook is launched before the initialization of all controllers', 1, 1),
(169, 'actionAdminLoginControllerBefore', 'Perform actions before admin login controller initialization', 'This hook is launched before the initialization of the login controller', 1, 1),
(170, 'actionAdminLoginControllerLoginBefore', 'Perform actions before admin login controller login action initialization', 'This hook is launched before the initialization of the login action in login controller', 1, 1),
(171, 'actionAdminLoginControllerLoginAfter', 'Perform actions after admin login controller login action initialization', 'This hook is launched after the initialization of the login action in login controller', 1, 1),
(172, 'actionAdminLoginControllerForgotBefore', 'Perform actions before admin login controller forgot action initialization', 'This hook is launched before the initialization of the forgot action in login controller', 1, 1),
(173, 'actionAdminLoginControllerForgotAfter', 'Perform actions after admin login controller forgot action initialization', 'This hook is launched after the initialization of the forgot action in login controller', 1, 1),
(174, 'actionAdminLoginControllerResetBefore', 'Perform actions before admin login controller reset action initialization', 'This hook is launched before the initialization of the reset action in login controller', 1, 1),
(175, 'actionAdminLoginControllerResetAfter', 'Perform actions after admin login controller reset action initialization', 'This hook is launched after the initialization of the reset action in login controller', 1, 1),
(176, 'actionAdministrationPageForm', 'Manage Administration Page form fields', 'This hook adds, update or remove fields of the Administration Page form', 1, 1),
(177, 'actionPerformancePageForm', 'Manage Performance Page form fields', 'This hook adds, update or remove fields of the Performance Page form', 1, 1),
(178, 'actionMaintenancePageForm', 'Manage Maintenance Page form fields', 'This hook adds, update or remove fields of the Maintenance Page form', 1, 1),
(179, 'actionWebserviceKeyGridPresenterModifier', 'Modify Webservice grid view data', 'This hook allows to alter presented Webservice grid data', 1, 1),
(180, 'actionWebserviceKeyGridDefinitionModifier', 'Modifying Webservice grid definition', 'This hook allows to alter Webservice grid columns, actions and filters', 1, 1),
(181, 'actionWebserviceKeyGridQueryBuilderModifier', 'Modify Webservice grid query builder', 'This hook allows to alter Doctrine query builder for Webservice grid', 1, 1),
(182, 'actionWebserviceKeyGridFilterFormModifier', 'Modify filters form for Webservice grid', 'This hook allows to alter filters form used in Webservice', 1, 1),
(183, 'actionSqlRequestGridPresenterModifier', 'Modify SQL Manager grid view data', 'This hook allows to alter presented SQL Manager grid data', 1, 1),
(184, 'actionSqlRequestGridDefinitionModifier', 'Modifying SQL Manager grid definition', 'This hook allows to alter SQL Manager grid columns, actions and filters', 1, 1),
(185, 'actionSqlRequestGridQueryBuilderModifier', 'Modify SQL Manager grid query builder', 'This hook allows to alter Doctrine query builder for SQL Manager grid', 1, 1),
(186, 'actionSqlRequestGridFilterFormModifier', 'Modify filters form for SQL Manager grid', 'This hook allows to alter filters form used in SQL Manager', 1, 1),
(187, 'actionMetaGridPresenterModifier', 'Modify SEO and URLs grid view data', 'This hook allows to alter presented SEO and URLs grid data', 1, 1),
(188, 'actionMetaGridDefinitionModifier', 'Modifying SEO and URLs grid definition', 'This hook allows to alter SEO and URLs grid columns, actions and filters', 1, 1),
(189, 'actionMetaGridQueryBuilderModifier', 'Modify SEO and URLs grid query builder', 'This hook allows to alter Doctrine query builder for SEO and URLs grid', 1, 1),
(190, 'actionMetaGridFilterFormModifier', 'Modify filters form for SEO and URLs grid', 'This hook allows to alter filters form used in SEO and URLs', 1, 1),
(191, 'actionLogsGridPresenterModifier', 'Modify Logs grid view data', 'This hook allows to alter presented Logs grid data', 1, 1),
(192, 'actionLogsGridDefinitionModifier', 'Modifying Logs grid definition', 'This hook allows to alter Logs grid columns, actions and filters', 1, 1),
(193, 'actionLogsGridQueryBuilderModifier', 'Modify Logs grid query builder', 'This hook allows to alter Doctrine query builder for Logs grid', 1, 1),
(194, 'actionLogsGridFilterFormModifier', 'Modify filters form for Logs grid', 'This hook allows to alter filters form used in Logs', 1, 1),
(195, 'actionEmailLogsGridPresenterModifier', 'Modify E-mail grid view data', 'This hook allows to alter presented E-mail grid data', 1, 1),
(196, 'actionEmailLogsGridDefinitionModifier', 'Modifying E-mail grid definition', 'This hook allows to alter E-mail grid columns, actions and filters', 1, 1),
(197, 'actionEmailLogsGridQueryBuilderModifier', 'Modify E-mail grid query builder', 'This hook allows to alter Doctrine query builder for E-mail grid', 1, 1),
(198, 'actionEmailLogsGridFilterFormModifier', 'Modify filters form for E-mail grid', 'This hook allows to alter filters form used in E-mail', 1, 1),
(199, 'actionBackupGridPresenterModifier', 'Modify DB Backup grid view data', 'This hook allows to alter presented DB Backup grid data', 1, 1),
(200, 'actionBackupGridDefinitionModifier', 'Modifying DB Backup grid definition', 'This hook allows to alter DB Backup grid columns, actions and filters', 1, 1),
(201, 'actionBackupGridFilterFormModifier', 'Modify filters form for DB Backup grid', 'This hook allows to alter filters form used in DB Backup', 1, 1),
(202, 'actionProductFlagsModifier', 'Customize product labels displayed on the product list on FO', 'This hook allows to add and remove product labels displayed on top of product images', 1, 1),
(203, 'actionListMailThemes', 'List the available email themes and layouts', 'This hook allows to add/remove available email themes (ThemeInterface) and/or to add/remove their layouts (LayoutInterface)', 1, 1),
(204, 'actionGetMailThemeFolder', 'Define the folder of an email theme', 'This hook allows to change the folder of an email theme (useful if you theme is in a module for example)', 1, 1),
(205, 'actionBuildMailLayoutVariables', 'Build the variables used in email layout rendering', 'This hook allows to change the variables used when an email layout is rendered', 1, 1),
(206, 'actionGetMailLayoutTransformations', 'Define the transformation to apply on layout', 'This hook allows to add/remove TransformationInterface used to generate an email layout', 1, 1),
(207, 'displayProductActions', 'Display additional action button on the product page', 'This hook allow additional actions to be triggered, near the add to cart button.', 1, 1),
(208, 'displayPersonalInformationTop', 'Content in the checkout funnel, on top of the personal information panel', 'Display actions or additional content in the personal details tab of the checkout funnel.', 1, 1),
(209, 'actionSqlRequestFormBuilderModifier', 'Modify sql request identifiable object form', 'This hook allows to modify sql request identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(210, 'actionCustomerFormBuilderModifier', 'Modify customer identifiable object form', 'This hook allows to modify customer identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(211, 'actionLanguageFormBuilderModifier', 'Modify language identifiable object form', 'This hook allows to modify language identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(212, 'actionCurrencyFormBuilderModifier', 'Modify currency identifiable object form', 'This hook allows to modify currency identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(213, 'actionWebserviceKeyFormBuilderModifier', 'Modify webservice key identifiable object form', 'This hook allows to modify webservice key identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(214, 'actionMetaFormBuilderModifier', 'Modify meta identifiable object form', 'This hook allows to modify meta identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(215, 'actionCategoryFormBuilderModifier', 'Modify category identifiable object form', 'This hook allows to modify category identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(216, 'actionRootCategoryFormBuilderModifier', 'Modify root category identifiable object form', 'This hook allows to modify root category identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(217, 'actionContactFormBuilderModifier', 'Modify contact identifiable object form', 'This hook allows to modify contact identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(218, 'actionCmsPageCategoryFormBuilderModifier', 'Modify cms page category identifiable object form', 'This hook allows to modify cms page category identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(219, 'actionTaxFormBuilderModifier', 'Modify tax identifiable object form', 'This hook allows to modify tax identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(220, 'actionManufacturerFormBuilderModifier', 'Modify manufacturer identifiable object form', 'This hook allows to modify manufacturer identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(221, 'actionEmployeeFormBuilderModifier', 'Modify employee identifiable object form', 'This hook allows to modify employee identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(222, 'actionProfileFormBuilderModifier', 'Modify profile identifiable object form', 'This hook allows to modify profile identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(223, 'actionCmsPageFormBuilderModifier', 'Modify cms page identifiable object form', 'This hook allows to modify cms page identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(224, 'actionManufacturerAddressFormBuilderModifier', 'Modify manufacturer address identifiable object form', 'This hook allows to modify manufacturer address identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(225, 'actionBeforeUpdateSqlRequestFormHandler', 'Modify sql request identifiable object data before updating it', 'This hook allows to modify sql request identifiable object forms data before it was updated', 1, 1),
(226, 'actionBeforeUpdateCustomerFormHandler', 'Modify customer identifiable object data before updating it', 'This hook allows to modify customer identifiable object forms data before it was updated', 1, 1),
(227, 'actionBeforeUpdateLanguageFormHandler', 'Modify language identifiable object data before updating it', 'This hook allows to modify language identifiable object forms data before it was updated', 1, 1),
(228, 'actionBeforeUpdateCurrencyFormHandler', 'Modify currency identifiable object data before updating it', 'This hook allows to modify currency identifiable object forms data before it was updated', 1, 1),
(229, 'actionBeforeUpdateWebserviceKeyFormHandler', 'Modify webservice key identifiable object data before updating it', 'This hook allows to modify webservice key identifiable object forms data before it was updated', 1, 1),
(230, 'actionBeforeUpdateMetaFormHandler', 'Modify meta identifiable object data before updating it', 'This hook allows to modify meta identifiable object forms data before it was updated', 1, 1),
(231, 'actionBeforeUpdateCategoryFormHandler', 'Modify category identifiable object data before updating it', 'This hook allows to modify category identifiable object forms data before it was updated', 1, 1),
(232, 'actionBeforeUpdateRootCategoryFormHandler', 'Modify root category identifiable object data before updating it', 'This hook allows to modify root category identifiable object forms data before it was updated', 1, 1),
(233, 'actionBeforeUpdateContactFormHandler', 'Modify contact identifiable object data before updating it', 'This hook allows to modify contact identifiable object forms data before it was updated', 1, 1),
(234, 'actionBeforeUpdateCmsPageCategoryFormHandler', 'Modify cms page category identifiable object data before updating it', 'This hook allows to modify cms page category identifiable object forms data before it was updated', 1, 1),
(235, 'actionBeforeUpdateTaxFormHandler', 'Modify tax identifiable object data before updating it', 'This hook allows to modify tax identifiable object forms data before it was updated', 1, 1),
(236, 'actionBeforeUpdateManufacturerFormHandler', 'Modify manufacturer identifiable object data before updating it', 'This hook allows to modify manufacturer identifiable object forms data before it was updated', 1, 1),
(237, 'actionBeforeUpdateEmployeeFormHandler', 'Modify employee identifiable object data before updating it', 'This hook allows to modify employee identifiable object forms data before it was updated', 1, 1),
(238, 'actionBeforeUpdateProfileFormHandler', 'Modify profile identifiable object data before updating it', 'This hook allows to modify profile identifiable object forms data before it was updated', 1, 1),
(239, 'actionBeforeUpdateCmsPageFormHandler', 'Modify cms page identifiable object data before updating it', 'This hook allows to modify cms page identifiable object forms data before it was updated', 1, 1),
(240, 'actionBeforeUpdateManufacturerAddressFormHandler', 'Modify manufacturer address identifiable object data before updating it', 'This hook allows to modify manufacturer address identifiable object forms data before it was updated', 1, 1),
(241, 'actionAfterUpdateSqlRequestFormHandler', 'Modify sql request identifiable object data after updating it', 'This hook allows to modify sql request identifiable object forms data after it was updated', 1, 1),
(242, 'actionAfterUpdateCustomerFormHandler', 'Modify customer identifiable object data after updating it', 'This hook allows to modify customer identifiable object forms data after it was updated', 1, 1),
(243, 'actionAfterUpdateLanguageFormHandler', 'Modify language identifiable object data after updating it', 'This hook allows to modify language identifiable object forms data after it was updated', 1, 1),
(244, 'actionAfterUpdateCurrencyFormHandler', 'Modify currency identifiable object data after updating it', 'This hook allows to modify currency identifiable object forms data after it was updated', 1, 1),
(245, 'actionAfterUpdateWebserviceKeyFormHandler', 'Modify webservice key identifiable object data after updating it', 'This hook allows to modify webservice key identifiable object forms data after it was updated', 1, 1),
(246, 'actionAfterUpdateMetaFormHandler', 'Modify meta identifiable object data after updating it', 'This hook allows to modify meta identifiable object forms data after it was updated', 1, 1),
(247, 'actionAfterUpdateCategoryFormHandler', 'Modify category identifiable object data after updating it', 'This hook allows to modify category identifiable object forms data after it was updated', 1, 1),
(248, 'actionAfterUpdateRootCategoryFormHandler', 'Modify root category identifiable object data after updating it', 'This hook allows to modify root category identifiable object forms data after it was updated', 1, 1),
(249, 'actionAfterUpdateContactFormHandler', 'Modify contact identifiable object data after updating it', 'This hook allows to modify contact identifiable object forms data after it was updated', 1, 1),
(250, 'actionAfterUpdateCmsPageCategoryFormHandler', 'Modify cms page category identifiable object data after updating it', 'This hook allows to modify cms page category identifiable object forms data after it was updated', 1, 1),
(251, 'actionAfterUpdateTaxFormHandler', 'Modify tax identifiable object data after updating it', 'This hook allows to modify tax identifiable object forms data after it was updated', 1, 1),
(252, 'actionAfterUpdateManufacturerFormHandler', 'Modify manufacturer identifiable object data after updating it', 'This hook allows to modify manufacturer identifiable object forms data after it was updated', 1, 1),
(253, 'actionAfterUpdateEmployeeFormHandler', 'Modify employee identifiable object data after updating it', 'This hook allows to modify employee identifiable object forms data after it was updated', 1, 1),
(254, 'actionAfterUpdateProfileFormHandler', 'Modify profile identifiable object data after updating it', 'This hook allows to modify profile identifiable object forms data after it was updated', 1, 1),
(255, 'actionAfterUpdateCmsPageFormHandler', 'Modify cms page identifiable object data after updating it', 'This hook allows to modify cms page identifiable object forms data after it was updated', 1, 1),
(256, 'actionAfterUpdateManufacturerAddressFormHandler', 'Modify manufacturer address identifiable object data after updating it', 'This hook allows to modify manufacturer address identifiable object forms data after it was updated', 1, 1),
(257, 'actionBeforeCreateSqlRequestFormHandler', 'Modify sql request identifiable object data before creating it', 'This hook allows to modify sql request identifiable object forms data before it was created', 1, 1),
(258, 'actionBeforeCreateCustomerFormHandler', 'Modify customer identifiable object data before creating it', 'This hook allows to modify customer identifiable object forms data before it was created', 1, 1),
(259, 'actionBeforeCreateLanguageFormHandler', 'Modify language identifiable object data before creating it', 'This hook allows to modify language identifiable object forms data before it was created', 1, 1),
(260, 'actionBeforeCreateCurrencyFormHandler', 'Modify currency identifiable object data before creating it', 'This hook allows to modify currency identifiable object forms data before it was created', 1, 1),
(261, 'actionBeforeCreateWebserviceKeyFormHandler', 'Modify webservice key identifiable object data before creating it', 'This hook allows to modify webservice key identifiable object forms data before it was created', 1, 1),
(262, 'actionBeforeCreateMetaFormHandler', 'Modify meta identifiable object data before creating it', 'This hook allows to modify meta identifiable object forms data before it was created', 1, 1),
(263, 'actionBeforeCreateCategoryFormHandler', 'Modify category identifiable object data before creating it', 'This hook allows to modify category identifiable object forms data before it was created', 1, 1),
(264, 'actionBeforeCreateRootCategoryFormHandler', 'Modify root category identifiable object data before creating it', 'This hook allows to modify root category identifiable object forms data before it was created', 1, 1),
(265, 'actionBeforeCreateContactFormHandler', 'Modify contact identifiable object data before creating it', 'This hook allows to modify contact identifiable object forms data before it was created', 1, 1),
(266, 'actionBeforeCreateCmsPageCategoryFormHandler', 'Modify cms page category identifiable object data before creating it', 'This hook allows to modify cms page category identifiable object forms data before it was created', 1, 1),
(267, 'actionBeforeCreateTaxFormHandler', 'Modify tax identifiable object data before creating it', 'This hook allows to modify tax identifiable object forms data before it was created', 1, 1),
(268, 'actionBeforeCreateManufacturerFormHandler', 'Modify manufacturer identifiable object data before creating it', 'This hook allows to modify manufacturer identifiable object forms data before it was created', 1, 1),
(269, 'actionBeforeCreateEmployeeFormHandler', 'Modify employee identifiable object data before creating it', 'This hook allows to modify employee identifiable object forms data before it was created', 1, 1),
(270, 'actionBeforeCreateProfileFormHandler', 'Modify profile identifiable object data before creating it', 'This hook allows to modify profile identifiable object forms data before it was created', 1, 1),
(271, 'actionBeforeCreateCmsPageFormHandler', 'Modify cms page identifiable object data before creating it', 'This hook allows to modify cms page identifiable object forms data before it was created', 1, 1),
(272, 'actionBeforeCreateManufacturerAddressFormHandler', 'Modify manufacturer address identifiable object data before creating it', 'This hook allows to modify manufacturer address identifiable object forms data before it was created', 1, 1),
(273, 'actionAfterCreateSqlRequestFormHandler', 'Modify sql request identifiable object data after creating it', 'This hook allows to modify sql request identifiable object forms data after it was created', 1, 1),
(274, 'actionAfterCreateCustomerFormHandler', 'Modify customer identifiable object data after creating it', 'This hook allows to modify customer identifiable object forms data after it was created', 1, 1),
(275, 'actionAfterCreateLanguageFormHandler', 'Modify language identifiable object data after creating it', 'This hook allows to modify language identifiable object forms data after it was created', 1, 1),
(276, 'actionAfterCreateCurrencyFormHandler', 'Modify currency identifiable object data after creating it', 'This hook allows to modify currency identifiable object forms data after it was created', 1, 1),
(277, 'actionAfterCreateWebserviceKeyFormHandler', 'Modify webservice key identifiable object data after creating it', 'This hook allows to modify webservice key identifiable object forms data after it was created', 1, 1),
(278, 'actionAfterCreateMetaFormHandler', 'Modify meta identifiable object data after creating it', 'This hook allows to modify meta identifiable object forms data after it was created', 1, 1),
(279, 'actionAfterCreateCategoryFormHandler', 'Modify category identifiable object data after creating it', 'This hook allows to modify category identifiable object forms data after it was created', 1, 1),
(280, 'actionAfterCreateRootCategoryFormHandler', 'Modify root category identifiable object data after creating it', 'This hook allows to modify root category identifiable object forms data after it was created', 1, 1),
(281, 'actionAfterCreateContactFormHandler', 'Modify contact identifiable object data after creating it', 'This hook allows to modify contact identifiable object forms data after it was created', 1, 1),
(282, 'actionAfterCreateCmsPageCategoryFormHandler', 'Modify cms page category identifiable object data after creating it', 'This hook allows to modify cms page category identifiable object forms data after it was created', 1, 1),
(283, 'actionAfterCreateTaxFormHandler', 'Modify tax identifiable object data after creating it', 'This hook allows to modify tax identifiable object forms data after it was created', 1, 1),
(284, 'actionAfterCreateManufacturerFormHandler', 'Modify manufacturer identifiable object data after creating it', 'This hook allows to modify manufacturer identifiable object forms data after it was created', 1, 1),
(285, 'actionAfterCreateEmployeeFormHandler', 'Modify employee identifiable object data after creating it', 'This hook allows to modify employee identifiable object forms data after it was created', 1, 1),
(286, 'actionAfterCreateProfileFormHandler', 'Modify profile identifiable object data after creating it', 'This hook allows to modify profile identifiable object forms data after it was created', 1, 1),
(287, 'actionAfterCreateCmsPageFormHandler', 'Modify cms page identifiable object data after creating it', 'This hook allows to modify cms page identifiable object forms data after it was created', 1, 1),
(288, 'actionAfterCreateManufacturerAddressFormHandler', 'Modify manufacturer address identifiable object data after creating it', 'This hook allows to modify manufacturer address identifiable object forms data after it was created', 1, 1),
(289, 'actionShippingPreferencesPageForm', 'Modify shipping preferences page options form content', 'This hook allows to modify shipping preferences page options form FormBuilder', 1, 1),
(290, 'actionOrdersInvoicesByDateForm', 'Modify orders invoices by date options form content', 'This hook allows to modify orders invoices by date options form FormBuilder', 1, 1),
(291, 'actionOrdersInvoicesByStatusForm', 'Modify orders invoices by status options form content', 'This hook allows to modify orders invoices by status options form FormBuilder', 1, 1),
(292, 'actionOrdersInvoicesOptionsForm', 'Modify orders invoices options options form content', 'This hook allows to modify orders invoices options options form FormBuilder', 1, 1),
(293, 'actionCustomerPreferencesPageForm', 'Modify customer preferences page options form content', 'This hook allows to modify customer preferences page options form FormBuilder', 1, 1),
(294, 'actionOrderPreferencesPageForm', 'Modify order preferences page options form content', 'This hook allows to modify order preferences page options form FormBuilder', 1, 1),
(295, 'actionProductPreferencesPageForm', 'Modify product preferences page options form content', 'This hook allows to modify product preferences page options form FormBuilder', 1, 1),
(296, 'actionGeneralPageForm', 'Modify general page options form content', 'This hook allows to modify general page options form FormBuilder', 1, 1),
(297, 'actionLogsPageForm', 'Modify logs page options form content', 'This hook allows to modify logs page options form FormBuilder', 1, 1),
(298, 'actionOrderDeliverySlipOptionsForm', 'Modify order delivery slip options options form content', 'This hook allows to modify order delivery slip options options form FormBuilder', 1, 1),
(299, 'actionOrderDeliverySlipPdfForm', 'Modify order delivery slip pdf options form content', 'This hook allows to modify order delivery slip pdf options form FormBuilder', 1, 1),
(300, 'actionGeolocationPageForm', 'Modify geolocation page options form content', 'This hook allows to modify geolocation page options form FormBuilder', 1, 1),
(301, 'actionLocalizationPageForm', 'Modify localization page options form content', 'This hook allows to modify localization page options form FormBuilder', 1, 1),
(302, 'actionPaymentPreferencesForm', 'Modify payment preferences options form content', 'This hook allows to modify payment preferences options form FormBuilder', 1, 1),
(303, 'actionEmailConfigurationForm', 'Modify email configuration options form content', 'This hook allows to modify email configuration options form FormBuilder', 1, 1);
INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `active`, `position`) VALUES
(304, 'actionRequestSqlForm', 'Modify request sql options form content', 'This hook allows to modify request sql options form FormBuilder', 1, 1),
(305, 'actionBackupForm', 'Modify backup options form content', 'This hook allows to modify backup options form FormBuilder', 1, 1),
(306, 'actionWebservicePageForm', 'Modify webservice page options form content', 'This hook allows to modify webservice page options form FormBuilder', 1, 1),
(307, 'actionMetaPageForm', 'Modify meta page options form content', 'This hook allows to modify meta page options form FormBuilder', 1, 1),
(308, 'actionEmployeeForm', 'Modify employee options form content', 'This hook allows to modify employee options form FormBuilder', 1, 1),
(309, 'actionCurrencyForm', 'Modify currency options form content', 'This hook allows to modify currency options form FormBuilder', 1, 1),
(310, 'actionShopLogoForm', 'Modify shop logo options form content', 'This hook allows to modify shop logo options form FormBuilder', 1, 1),
(311, 'actionTaxForm', 'Modify tax options form content', 'This hook allows to modify tax options form FormBuilder', 1, 1),
(312, 'actionMailThemeForm', 'Modify mail theme options form content', 'This hook allows to modify mail theme options form FormBuilder', 1, 1),
(313, 'actionPerformancePageSave', 'Modify performance page options form saved data', 'This hook allows to modify data of performance page options form after it was saved', 1, 1),
(314, 'actionMaintenancePageSave', 'Modify maintenance page options form saved data', 'This hook allows to modify data of maintenance page options form after it was saved', 1, 1),
(315, 'actionAdministrationPageSave', 'Modify administration page options form saved data', 'This hook allows to modify data of administration page options form after it was saved', 1, 1),
(316, 'actionShippingPreferencesPageSave', 'Modify shipping preferences page options form saved data', 'This hook allows to modify data of shipping preferences page options form after it was saved', 1, 1),
(317, 'actionOrdersInvoicesByDateSave', 'Modify orders invoices by date options form saved data', 'This hook allows to modify data of orders invoices by date options form after it was saved', 1, 1),
(318, 'actionOrdersInvoicesByStatusSave', 'Modify orders invoices by status options form saved data', 'This hook allows to modify data of orders invoices by status options form after it was saved', 1, 1),
(319, 'actionOrdersInvoicesOptionsSave', 'Modify orders invoices options options form saved data', 'This hook allows to modify data of orders invoices options options form after it was saved', 1, 1),
(320, 'actionCustomerPreferencesPageSave', 'Modify customer preferences page options form saved data', 'This hook allows to modify data of customer preferences page options form after it was saved', 1, 1),
(321, 'actionOrderPreferencesPageSave', 'Modify order preferences page options form saved data', 'This hook allows to modify data of order preferences page options form after it was saved', 1, 1),
(322, 'actionProductPreferencesPageSave', 'Modify product preferences page options form saved data', 'This hook allows to modify data of product preferences page options form after it was saved', 1, 1),
(323, 'actionGeneralPageSave', 'Modify general page options form saved data', 'This hook allows to modify data of general page options form after it was saved', 1, 1),
(324, 'actionLogsPageSave', 'Modify logs page options form saved data', 'This hook allows to modify data of logs page options form after it was saved', 1, 1),
(325, 'actionOrderDeliverySlipOptionsSave', 'Modify order delivery slip options options form saved data', 'This hook allows to modify data of order delivery slip options options form after it was saved', 1, 1),
(326, 'actionOrderDeliverySlipPdfSave', 'Modify order delivery slip pdf options form saved data', 'This hook allows to modify data of order delivery slip pdf options form after it was saved', 1, 1),
(327, 'actionGeolocationPageSave', 'Modify geolocation page options form saved data', 'This hook allows to modify data of geolocation page options form after it was saved', 1, 1),
(328, 'actionLocalizationPageSave', 'Modify localization page options form saved data', 'This hook allows to modify data of localization page options form after it was saved', 1, 1),
(329, 'actionPaymentPreferencesSave', 'Modify payment preferences options form saved data', 'This hook allows to modify data of payment preferences options form after it was saved', 1, 1),
(330, 'actionEmailConfigurationSave', 'Modify email configuration options form saved data', 'This hook allows to modify data of email configuration options form after it was saved', 1, 1),
(331, 'actionRequestSqlSave', 'Modify request sql options form saved data', 'This hook allows to modify data of request sql options form after it was saved', 1, 1),
(332, 'actionBackupSave', 'Modify backup options form saved data', 'This hook allows to modify data of backup options form after it was saved', 1, 1),
(333, 'actionWebservicePageSave', 'Modify webservice page options form saved data', 'This hook allows to modify data of webservice page options form after it was saved', 1, 1),
(334, 'actionMetaPageSave', 'Modify meta page options form saved data', 'This hook allows to modify data of meta page options form after it was saved', 1, 1),
(335, 'actionEmployeeSave', 'Modify employee options form saved data', 'This hook allows to modify data of employee options form after it was saved', 1, 1),
(336, 'actionCurrencySave', 'Modify currency options form saved data', 'This hook allows to modify data of currency options form after it was saved', 1, 1),
(337, 'actionShopLogoSave', 'Modify shop logo options form saved data', 'This hook allows to modify data of shop logo options form after it was saved', 1, 1),
(338, 'actionTaxSave', 'Modify tax options form saved data', 'This hook allows to modify data of tax options form after it was saved', 1, 1),
(339, 'actionMailThemeSave', 'Modify mail theme options form saved data', 'This hook allows to modify data of mail theme options form after it was saved', 1, 1),
(340, 'actionCategoryGridDefinitionModifier', 'Modify category grid definition', 'This hook allows to alter category grid columns, actions and filters', 1, 1),
(341, 'actionEmployeeGridDefinitionModifier', 'Modify employee grid definition', 'This hook allows to alter employee grid columns, actions and filters', 1, 1),
(342, 'actionContactGridDefinitionModifier', 'Modify contact grid definition', 'This hook allows to alter contact grid columns, actions and filters', 1, 1),
(343, 'actionCustomerGridDefinitionModifier', 'Modify customer grid definition', 'This hook allows to alter customer grid columns, actions and filters', 1, 1),
(344, 'actionLanguageGridDefinitionModifier', 'Modify language grid definition', 'This hook allows to alter language grid columns, actions and filters', 1, 1),
(345, 'actionCurrencyGridDefinitionModifier', 'Modify currency grid definition', 'This hook allows to alter currency grid columns, actions and filters', 1, 1),
(346, 'actionSupplierGridDefinitionModifier', 'Modify supplier grid definition', 'This hook allows to alter supplier grid columns, actions and filters', 1, 1),
(347, 'actionProfileGridDefinitionModifier', 'Modify profile grid definition', 'This hook allows to alter profile grid columns, actions and filters', 1, 1),
(348, 'actionCmsPageCategoryGridDefinitionModifier', 'Modify cms page category grid definition', 'This hook allows to alter cms page category grid columns, actions and filters', 1, 1),
(349, 'actionTaxGridDefinitionModifier', 'Modify tax grid definition', 'This hook allows to alter tax grid columns, actions and filters', 1, 1),
(350, 'actionManufacturerGridDefinitionModifier', 'Modify manufacturer grid definition', 'This hook allows to alter manufacturer grid columns, actions and filters', 1, 1),
(351, 'actionManufacturerAddressGridDefinitionModifier', 'Modify manufacturer address grid definition', 'This hook allows to alter manufacturer address grid columns, actions and filters', 1, 1),
(352, 'actionCmsPageGridDefinitionModifier', 'Modify cms page grid definition', 'This hook allows to alter cms page grid columns, actions and filters', 1, 1),
(353, 'actionBackupGridQueryBuilderModifier', 'Modify backup grid query builder', 'This hook allows to alter Doctrine query builder for backup grid', 1, 1),
(354, 'actionCategoryGridQueryBuilderModifier', 'Modify category grid query builder', 'This hook allows to alter Doctrine query builder for category grid', 1, 1),
(355, 'actionEmployeeGridQueryBuilderModifier', 'Modify employee grid query builder', 'This hook allows to alter Doctrine query builder for employee grid', 1, 1),
(356, 'actionContactGridQueryBuilderModifier', 'Modify contact grid query builder', 'This hook allows to alter Doctrine query builder for contact grid', 1, 1),
(357, 'actionCustomerGridQueryBuilderModifier', 'Modify customer grid query builder', 'This hook allows to alter Doctrine query builder for customer grid', 1, 1),
(358, 'actionLanguageGridQueryBuilderModifier', 'Modify language grid query builder', 'This hook allows to alter Doctrine query builder for language grid', 1, 1),
(359, 'actionCurrencyGridQueryBuilderModifier', 'Modify currency grid query builder', 'This hook allows to alter Doctrine query builder for currency grid', 1, 1),
(360, 'actionSupplierGridQueryBuilderModifier', 'Modify supplier grid query builder', 'This hook allows to alter Doctrine query builder for supplier grid', 1, 1),
(361, 'actionProfileGridQueryBuilderModifier', 'Modify profile grid query builder', 'This hook allows to alter Doctrine query builder for profile grid', 1, 1),
(362, 'actionCmsPageCategoryGridQueryBuilderModifier', 'Modify cms page category grid query builder', 'This hook allows to alter Doctrine query builder for cms page category grid', 1, 1),
(363, 'actionTaxGridQueryBuilderModifier', 'Modify tax grid query builder', 'This hook allows to alter Doctrine query builder for tax grid', 1, 1),
(364, 'actionManufacturerGridQueryBuilderModifier', 'Modify manufacturer grid query builder', 'This hook allows to alter Doctrine query builder for manufacturer grid', 1, 1),
(365, 'actionManufacturerAddressGridQueryBuilderModifier', 'Modify manufacturer address grid query builder', 'This hook allows to alter Doctrine query builder for manufacturer address grid', 1, 1),
(366, 'actionCmsPageGridQueryBuilderModifier', 'Modify cms page grid query builder', 'This hook allows to alter Doctrine query builder for cms page grid', 1, 1),
(367, 'actionLogsGridDataModifier', 'Modify logs grid data', 'This hook allows to modify logs grid data', 1, 1),
(368, 'actionEmailLogsGridDataModifier', 'Modify email logs grid data', 'This hook allows to modify email logs grid data', 1, 1),
(369, 'actionSqlRequestGridDataModifier', 'Modify sql request grid data', 'This hook allows to modify sql request grid data', 1, 1),
(370, 'actionBackupGridDataModifier', 'Modify backup grid data', 'This hook allows to modify backup grid data', 1, 1),
(371, 'actionWebserviceKeyGridDataModifier', 'Modify webservice key grid data', 'This hook allows to modify webservice key grid data', 1, 1),
(372, 'actionMetaGridDataModifier', 'Modify meta grid data', 'This hook allows to modify meta grid data', 1, 1),
(373, 'actionCategoryGridDataModifier', 'Modify category grid data', 'This hook allows to modify category grid data', 1, 1),
(374, 'actionEmployeeGridDataModifier', 'Modify employee grid data', 'This hook allows to modify employee grid data', 1, 1),
(375, 'actionContactGridDataModifier', 'Modify contact grid data', 'This hook allows to modify contact grid data', 1, 1),
(376, 'actionCustomerGridDataModifier', 'Modify customer grid data', 'This hook allows to modify customer grid data', 1, 1),
(377, 'actionLanguageGridDataModifier', 'Modify language grid data', 'This hook allows to modify language grid data', 1, 1),
(378, 'actionCurrencyGridDataModifier', 'Modify currency grid data', 'This hook allows to modify currency grid data', 1, 1),
(379, 'actionSupplierGridDataModifier', 'Modify supplier grid data', 'This hook allows to modify supplier grid data', 1, 1),
(380, 'actionProfileGridDataModifier', 'Modify profile grid data', 'This hook allows to modify profile grid data', 1, 1),
(381, 'actionCmsPageCategoryGridDataModifier', 'Modify cms page category grid data', 'This hook allows to modify cms page category grid data', 1, 1),
(382, 'actionTaxGridDataModifier', 'Modify tax grid data', 'This hook allows to modify tax grid data', 1, 1),
(383, 'actionManufacturerGridDataModifier', 'Modify manufacturer grid data', 'This hook allows to modify manufacturer grid data', 1, 1),
(384, 'actionManufacturerAddressGridDataModifier', 'Modify manufacturer address grid data', 'This hook allows to modify manufacturer address grid data', 1, 1),
(385, 'actionCmsPageGridDataModifier', 'Modify cms page grid data', 'This hook allows to modify cms page grid data', 1, 1),
(386, 'actionCategoryGridFilterFormModifier', 'Modify category grid filters', 'This hook allows to modify filters for category grid', 1, 1),
(387, 'actionEmployeeGridFilterFormModifier', 'Modify employee grid filters', 'This hook allows to modify filters for employee grid', 1, 1),
(388, 'actionContactGridFilterFormModifier', 'Modify contact grid filters', 'This hook allows to modify filters for contact grid', 1, 1),
(389, 'actionCustomerGridFilterFormModifier', 'Modify customer grid filters', 'This hook allows to modify filters for customer grid', 1, 1),
(390, 'actionLanguageGridFilterFormModifier', 'Modify language grid filters', 'This hook allows to modify filters for language grid', 1, 1),
(391, 'actionCurrencyGridFilterFormModifier', 'Modify currency grid filters', 'This hook allows to modify filters for currency grid', 1, 1),
(392, 'actionSupplierGridFilterFormModifier', 'Modify supplier grid filters', 'This hook allows to modify filters for supplier grid', 1, 1),
(393, 'actionProfileGridFilterFormModifier', 'Modify profile grid filters', 'This hook allows to modify filters for profile grid', 1, 1),
(394, 'actionCmsPageCategoryGridFilterFormModifier', 'Modify cms page category grid filters', 'This hook allows to modify filters for cms page category grid', 1, 1),
(395, 'actionTaxGridFilterFormModifier', 'Modify tax grid filters', 'This hook allows to modify filters for tax grid', 1, 1),
(396, 'actionManufacturerGridFilterFormModifier', 'Modify manufacturer grid filters', 'This hook allows to modify filters for manufacturer grid', 1, 1),
(397, 'actionManufacturerAddressGridFilterFormModifier', 'Modify manufacturer address grid filters', 'This hook allows to modify filters for manufacturer address grid', 1, 1),
(398, 'actionCmsPageGridFilterFormModifier', 'Modify cms page grid filters', 'This hook allows to modify filters for cms page grid', 1, 1),
(399, 'actionCategoryGridPresenterModifier', 'Modify category grid template data', 'This hook allows to modify data which is about to be used in template for category grid', 1, 1),
(400, 'actionEmployeeGridPresenterModifier', 'Modify employee grid template data', 'This hook allows to modify data which is about to be used in template for employee grid', 1, 1),
(401, 'actionContactGridPresenterModifier', 'Modify contact grid template data', 'This hook allows to modify data which is about to be used in template for contact grid', 1, 1),
(402, 'actionCustomerGridPresenterModifier', 'Modify customer grid template data', 'This hook allows to modify data which is about to be used in template for customer grid', 1, 1),
(403, 'actionLanguageGridPresenterModifier', 'Modify language grid template data', 'This hook allows to modify data which is about to be used in template for language grid', 1, 1),
(404, 'actionCurrencyGridPresenterModifier', 'Modify currency grid template data', 'This hook allows to modify data which is about to be used in template for currency grid', 1, 1),
(405, 'actionSupplierGridPresenterModifier', 'Modify supplier grid template data', 'This hook allows to modify data which is about to be used in template for supplier grid', 1, 1),
(406, 'actionProfileGridPresenterModifier', 'Modify profile grid template data', 'This hook allows to modify data which is about to be used in template for profile grid', 1, 1),
(407, 'actionCmsPageCategoryGridPresenterModifier', 'Modify cms page category grid template data', 'This hook allows to modify data which is about to be used in template for cms page category grid', 1, 1),
(408, 'actionTaxGridPresenterModifier', 'Modify tax grid template data', 'This hook allows to modify data which is about to be used in template for tax grid', 1, 1),
(409, 'actionManufacturerGridPresenterModifier', 'Modify manufacturer grid template data', 'This hook allows to modify data which is about to be used in template for manufacturer grid', 1, 1),
(410, 'actionManufacturerAddressGridPresenterModifier', 'Modify manufacturer address grid template data', 'This hook allows to modify data which is about to be used in template for manufacturer address grid', 1, 1),
(411, 'actionCmsPageGridPresenterModifier', 'Modify cms page grid template data', 'This hook allows to modify data which is about to be used in template for cms page grid', 1, 1),
(412, 'displayAdminOrderTop', 'Admin Order Top', 'This hook displays content at the top of the order view page', 1, 1),
(413, 'displayBackOfficeOrderActions', 'Admin Order Actions', 'This hook displays content in the order view page after action buttons (or aliased to side column in migrated page)', 1, 1),
(414, 'displayAdminOrderSide', 'Admin Order Side Column', 'This hook displays content in the order view page in the side column under the customer view', 1, 1),
(415, 'displayAdminOrderBottom', 'Admin Order Side Column Bottom', 'This hook displays content in the order view page at the bottom of the side column', 1, 1),
(416, 'displayAdminOrderMain', 'Admin Order Main Column', 'This hook displays content in the order view page in the main column under the details view', 1, 1),
(417, 'displayAdminOrderMainBottom', 'Admin Order Main Column Bottom', 'This hook displays content in the order view page at the bottom of the main column', 1, 1),
(418, 'displayAdminOrderTabLink', 'Admin Order Tab Link', 'This hook displays new tab links on the order view page', 1, 1),
(419, 'displayAdminOrderTabContent', 'Admin Order Tab Content', 'This hook displays new tab contents on the order view page', 1, 1),
(420, 'actionGetAdminOrderButtons', 'Admin Order Buttons', 'This hook is used to generate the buttons collection on the order view page (see ActionsBarButtonsCollection)', 1, 1),
(421, 'actionPresentCart', 'Cart Presenter', 'This hook is called before a cart is presented', 1, 1),
(422, 'actionPresentOrder', 'Order Presenter', 'This hook is called before an order is presented', 1, 1),
(423, 'actionPresentOrderReturn', 'Order Return Presenter', 'This hook is called before an order return is presented', 1, 1),
(424, 'actionPresentProduct', 'Product Presenter', 'This hook is called before a product is presented', 1, 1),
(425, 'actionAdminAdminPreferencesControllerPostProcessBefore', 'On post-process in Admin Preferences', 'This hook is called on Admin Preferences post-process before processing the form', 1, 1),
(426, 'actionFeatureFormBuilderModifier', 'Modify feature identifiable object form', 'This hook allows to modify feature identifiable object forms content by modifying form builder data\n      or FormBuilder itself', 1, 1),
(427, 'actionOrderMessageFormBuilderModifier', 'Modify order message identifiable object form', 'This hook allows to modify order message identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(428, 'actionCatalogPriceRuleFormBuilderModifier', 'Modify catalog price rule identifiable object form', 'This hook allows to modify catalog price rule identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(429, 'actionAttachmentFormBuilderModifier', 'Modify attachment identifiable object form', 'This hook allows to modify attachment identifiable object forms content by modifying form builder data or FormBuilder itself', 1, 1),
(430, 'actionBeforeUpdateFeatureFormHandler', 'Modify feature identifiable object data before updating it', 'This hook allows to modify feature identifiable object forms data before it was updated', 1, 1),
(431, 'actionBeforeUpdateOrderMessageFormHandler', 'Modify order message identifiable object data before updating it', 'This hook allows to modify order message identifiable object forms data before it was updated', 1, 1),
(432, 'actionBeforeUpdateCatalogPriceRuleFormHandler', 'Modify catalog price rule identifiable object data before updating it', 'This hook allows to modify catalog price rule identifiable object forms data before it was updated', 1, 1),
(433, 'actionBeforeUpdateAttachmentFormHandler', 'Modify attachment identifiable object data before updating it', 'This hook allows to modify attachment identifiable object forms data before it was updated', 1, 1),
(434, 'actionAfterUpdateFeatureFormHandler', 'Modify feature identifiable object data after updating it', 'This hook allows to modify feature identifiable object forms data after it was updated', 1, 1),
(435, 'actionAfterUpdateOrderMessageFormHandler', 'Modify order message identifiable object data after updating it', 'This hook allows to modify order message identifiable object forms data after it was updated', 1, 1),
(436, 'actionAfterUpdateCatalogPriceRuleFormHandler', 'Modify catalog price rule identifiable object data after updating it', 'This hook allows to modify catalog price rule identifiable object forms data after it was updated', 1, 1),
(437, 'actionAfterUpdateAttachmentFormHandler', 'Modify attachment identifiable object data after updating it', 'This hook allows to modify attachment identifiable object forms data after it was updated', 1, 1),
(438, 'actionBeforeCreateFeatureFormHandler', 'Modify feature identifiable object data before creating it', 'This hook allows to modify feature identifiable object forms data before it was created', 1, 1),
(439, 'actionBeforeCreateOrderMessageFormHandler', 'Modify order message identifiable object data before creating it', 'This hook allows to modify order message identifiable object forms data before it was created', 1, 1),
(440, 'actionBeforeCreateCatalogPriceRuleFormHandler', 'Modify catalog price rule identifiable object data before creating it', 'This hook allows to modify catalog price rule identifiable object forms data before it was created', 1, 1),
(441, 'actionBeforeCreateAttachmentFormHandler', 'Modify attachment identifiable object data before creating it', 'This hook allows to modify attachment identifiable object forms data before it was created', 1, 1),
(442, 'actionAfterCreateFeatureFormHandler', 'Modify feature identifiable object data after creating it', 'This hook allows to modify feature identifiable object forms data after it was created', 1, 1),
(443, 'actionAfterCreateOrderMessageFormHandler', 'Modify order message identifiable object data after creating it', 'This hook allows to modify order message identifiable object forms data after it was created', 1, 1),
(444, 'actionAfterCreateCatalogPriceRuleFormHandler', 'Modify catalog price rule identifiable object data after creating it', 'This hook allows to modify catalog price rule identifiable object forms data after it was created', 1, 1),
(445, 'actionAfterCreateAttachmentFormHandler', 'Modify attachment identifiable object data after creating it', 'This hook allows to modify attachment identifiable object forms data after it was created', 1, 1),
(446, 'actionMerchandiseReturnForm', 'Modify merchandise return options form content', 'This hook allows to modify merchandise return options form FormBuilder', 1, 1),
(447, 'actionCreditSlipForm', 'Modify credit slip options form content', 'This hook allows to modify credit slip options form FormBuilder', 1, 1),
(448, 'actionMerchandiseReturnSave', 'Modify merchandise return options form saved data', 'This hook allows to modify data of merchandise return options form after it was saved', 1, 1),
(449, 'actionCreditSlipSave', 'Modify credit slip options form saved data', 'This hook allows to modify data of credit slip options form after it was saved', 1, 1),
(450, 'actionEmptyCategoryGridDefinitionModifier', 'Modify empty category grid definition', 'This hook allows to alter empty category grid columns, actions and filters', 1, 1),
(451, 'actionNoQtyProductWithCombinationGridDefinitionModifier', 'Modify no qty product with combination grid definition', 'This hook allows to alter no qty product with combination grid columns, actions and filters\n      ', 1, 1),
(452, 'actionNoQtyProductWithoutCombinationGridDefinitionModifier', 'Modify no qty product without combination grid definition', 'This hook allows to alter no qty product without combination grid columns, actions and filters\n      ', 1, 1),
(453, 'actionDisabledProductGridDefinitionModifier', 'Modify disabled product grid definition', 'This hook allows to alter disabled product grid columns, actions and filters', 1, 1),
(454, 'actionProductWithoutImageGridDefinitionModifier', 'Modify product without image grid definition', 'This hook allows to alter product without image grid columns, actions and filters', 1, 1),
(455, 'actionProductWithoutDescriptionGridDefinitionModifier', 'Modify product without description grid definition', 'This hook allows to alter product without description grid columns, actions and filters', 1, 1),
(456, 'actionProductWithoutPriceGridDefinitionModifier', 'Modify product without price grid definition', 'This hook allows to alter product without price grid columns, actions and filters', 1, 1),
(457, 'actionOrderGridDefinitionModifier', 'Modify order grid definition', 'This hook allows to alter order grid columns, actions and filters', 1, 1),
(458, 'actionCatalogPriceRuleGridDefinitionModifier', 'Modify catalog price rule grid definition', 'This hook allows to alter catalog price rule grid columns, actions and filters', 1, 1),
(459, 'actionOrderMessageGridDefinitionModifier', 'Modify order message grid definition', 'This hook allows to alter order message grid columns, actions and filters', 1, 1),
(460, 'actionAttachmentGridDefinitionModifier', 'Modify attachment grid definition', 'This hook allows to alter attachment grid columns, actions and filters', 1, 1),
(461, 'actionAttributeGroupGridDefinitionModifier', 'Modify attribute group grid definition', 'This hook allows to alter attribute group grid columns, actions and filters', 1, 1),
(462, 'actionMerchandiseReturnGridDefinitionModifier', 'Modify merchandise return grid definition', 'This hook allows to alter merchandise return grid columns, actions and filters', 1, 1),
(463, 'actionTaxRulesGroupGridDefinitionModifier', 'Modify tax rules group grid definition', 'This hook allows to alter tax rules group grid columns, actions and filters', 1, 1),
(464, 'actionAddressGridDefinitionModifier', 'Modify address grid definition', 'This hook allows to alter address grid columns, actions and filters', 1, 1),
(465, 'actionCreditSlipGridDefinitionModifier', 'Modify credit slip grid definition', 'This hook allows to alter credit slip grid columns, actions and filters', 1, 1),
(466, 'actionEmptyCategoryGridQueryBuilderModifier', 'Modify empty category grid query builder', 'This hook allows to alter Doctrine query builder for empty category grid', 1, 1),
(467, 'actionNoQtyProductWithCombinationGridQueryBuilderModifier', 'Modify no qty product with combination grid query builder', 'This hook allows to alter Doctrine query builder for no qty product with combination grid', 1, 1),
(468, 'actionNoQtyProductWithoutCombinationGridQueryBuilderModifier', 'Modify no qty product without combination grid query builder', 'This hook allows to alter Doctrine query builder for no qty product without combination grid', 1, 1),
(469, 'actionDisabledProductGridQueryBuilderModifier', 'Modify disabled product grid query builder', 'This hook allows to alter Doctrine query builder for disabled product grid', 1, 1),
(470, 'actionProductWithoutImageGridQueryBuilderModifier', 'Modify product without image grid query builder', 'This hook allows to alter Doctrine query builder for product without image grid', 1, 1),
(471, 'actionProductWithoutDescriptionGridQueryBuilderModifier', 'Modify product without description grid query builder', 'This hook allows to alter Doctrine query builder for product without description grid', 1, 1),
(472, 'actionProductWithoutPriceGridQueryBuilderModifier', 'Modify product without price grid query builder', 'This hook allows to alter Doctrine query builder for product without price grid', 1, 1),
(473, 'actionOrderGridQueryBuilderModifier', 'Modify order grid query builder', 'This hook allows to alter Doctrine query builder for order grid', 1, 1),
(474, 'actionCatalogPriceRuleGridQueryBuilderModifier', 'Modify catalog price rule grid query builder', 'This hook allows to alter Doctrine query builder for catalog price rule grid', 1, 1),
(475, 'actionOrderMessageGridQueryBuilderModifier', 'Modify order message grid query builder', 'This hook allows to alter Doctrine query builder for order message grid', 1, 1),
(476, 'actionAttachmentGridQueryBuilderModifier', 'Modify attachment grid query builder', 'This hook allows to alter Doctrine query builder for attachment grid', 1, 1),
(477, 'actionAttributeGroupGridQueryBuilderModifier', 'Modify attribute group grid query builder', 'This hook allows to alter Doctrine query builder for attribute group grid', 1, 1),
(478, 'actionMerchandiseReturnGridQueryBuilderModifier', 'Modify merchandise return grid query builder', 'This hook allows to alter Doctrine query builder for merchandise return grid', 1, 1),
(479, 'actionTaxRulesGroupGridQueryBuilderModifier', 'Modify tax rules group grid query builder', 'This hook allows to alter Doctrine query builder for tax rules group grid', 1, 1),
(480, 'actionAddressGridQueryBuilderModifier', 'Modify address grid query builder', 'This hook allows to alter Doctrine query builder for address grid', 1, 1),
(481, 'actionCreditSlipGridQueryBuilderModifier', 'Modify credit slip grid query builder', 'This hook allows to alter Doctrine query builder for credit slip grid', 1, 1),
(482, 'actionEmptyCategoryGridDataModifier', 'Modify empty category grid data', 'This hook allows to modify empty category grid data', 1, 1),
(483, 'actionNoQtyProductWithCombinationGridDataModifier', 'Modify no qty product with combination grid data', 'This hook allows to modify no qty product with combination grid data', 1, 1),
(484, 'actionNoQtyProductWithoutCombinationGridDataModifier', 'Modify no qty product without combination grid data', 'This hook allows to modify no qty product without combination grid data', 1, 1),
(485, 'actionDisabledProductGridDataModifier', 'Modify disabled product grid data', 'This hook allows to modify disabled product grid data', 1, 1),
(486, 'actionProductWithoutImageGridDataModifier', 'Modify product without image grid data', 'This hook allows to modify product without image grid data', 1, 1),
(487, 'actionProductWithoutDescriptionGridDataModifier', 'Modify product without description grid data', 'This hook allows to modify product without description grid data', 1, 1),
(488, 'actionProductWithoutPriceGridDataModifier', 'Modify product without price grid data', 'This hook allows to modify product without price grid data', 1, 1),
(489, 'actionOrderGridDataModifier', 'Modify order grid data', 'This hook allows to modify order grid data', 1, 1),
(490, 'actionCatalogPriceRuleGridDataModifier', 'Modify catalog price rule grid data', 'This hook allows to modify catalog price rule grid data', 1, 1),
(491, 'actionOrderMessageGridDataModifier', 'Modify order message grid data', 'This hook allows to modify order message grid data', 1, 1),
(492, 'actionAttachmentGridDataModifier', 'Modify attachment grid data', 'This hook allows to modify attachment grid data', 1, 1),
(493, 'actionAttributeGroupGridDataModifier', 'Modify attribute group grid data', 'This hook allows to modify attribute group grid data', 1, 1),
(494, 'actionMerchandiseReturnGridDataModifier', 'Modify merchandise return grid data', 'This hook allows to modify merchandise return grid data', 1, 1),
(495, 'actionTaxRulesGroupGridDataModifier', 'Modify tax rules group grid data', 'This hook allows to modify tax rules group grid data', 1, 1),
(496, 'actionAddressGridDataModifier', 'Modify address grid data', 'This hook allows to modify address grid data', 1, 1),
(497, 'actionCreditSlipGridDataModifier', 'Modify credit slip grid data', 'This hook allows to modify credit slip grid data', 1, 1),
(498, 'actionEmptyCategoryGridFilterFormModifier', 'Modify empty category grid filters', 'This hook allows to modify filters for empty category grid', 1, 1),
(499, 'actionNoQtyProductWithCombinationGridFilterFormModifier', 'Modify no qty product with combination grid filters', 'This hook allows to modify filters for no qty product with combination grid', 1, 1),
(500, 'actionNoQtyProductWithoutCombinationGridFilterFormModifier', 'Modify no qty product without combination grid filters', 'This hook allows to modify filters for no qty product without combination grid', 1, 1),
(501, 'actionDisabledProductGridFilterFormModifier', 'Modify disabled product grid filters', 'This hook allows to modify filters for disabled product grid', 1, 1),
(502, 'actionProductWithoutImageGridFilterFormModifier', 'Modify product without image grid filters', 'This hook allows to modify filters for product without image grid', 1, 1),
(503, 'actionProductWithoutDescriptionGridFilterFormModifier', 'Modify product without description grid filters', 'This hook allows to modify filters for product without description grid', 1, 1),
(504, 'actionProductWithoutPriceGridFilterFormModifier', 'Modify product without price grid filters', 'This hook allows to modify filters for product without price grid', 1, 1),
(505, 'actionOrderGridFilterFormModifier', 'Modify order grid filters', 'This hook allows to modify filters for order grid', 1, 1),
(506, 'actionCatalogPriceRuleGridFilterFormModifier', 'Modify catalog price rule grid filters', 'This hook allows to modify filters for catalog price rule grid', 1, 1),
(507, 'actionOrderMessageGridFilterFormModifier', 'Modify order message grid filters', 'This hook allows to modify filters for order message grid', 1, 1),
(508, 'actionAttachmentGridFilterFormModifier', 'Modify attachment grid filters', 'This hook allows to modify filters for attachment grid', 1, 1),
(509, 'actionAttributeGroupGridFilterFormModifier', 'Modify attribute group grid filters', 'This hook allows to modify filters for attribute group grid', 1, 1),
(510, 'actionMerchandiseReturnGridFilterFormModifier', 'Modify merchandise return grid filters', 'This hook allows to modify filters for merchandise return grid', 1, 1),
(511, 'actionTaxRulesGroupGridFilterFormModifier', 'Modify tax rules group grid filters', 'This hook allows to modify filters for tax rules group grid', 1, 1),
(512, 'actionAddressGridFilterFormModifier', 'Modify address grid filters', 'This hook allows to modify filters for address grid', 1, 1),
(513, 'actionCreditSlipGridFilterFormModifier', 'Modify credit slip grid filters', 'This hook allows to modify filters for credit slip grid', 1, 1),
(514, 'actionEmptyCategoryGridPresenterModifier', 'Modify empty category grid template data', 'This hook allows to modify data which is about to be used in template for empty category grid', 1, 1),
(515, 'actionNoQtyProductWithCombinationGridPresenterModifier', 'Modify no qty product with combination grid template data', 'This hook allows to modify data which is about to be used in template for no qty product with combination grid', 1, 1),
(516, 'actionNoQtyProductWithoutCombinationGridPresenterModifier', 'Modify no qty product without combination grid template data', 'This hook allows to modify data which is about to be used in template for no qty product without combination grid', 1, 1),
(517, 'actionDisabledProductGridPresenterModifier', 'Modify disabled product grid template data', 'This hook allows to modify data which is about to be used in template for disabled product grid', 1, 1),
(518, 'actionProductWithoutImageGridPresenterModifier', 'Modify product without image grid template data', 'This hook allows to modify data which is about to be used in template for product without image grid', 1, 1),
(519, 'actionProductWithoutDescriptionGridPresenterModifier', 'Modify product without description grid template data', 'This hook allows to modify data which is about to be used in template for product without description grid', 1, 1),
(520, 'actionProductWithoutPriceGridPresenterModifier', 'Modify product without price grid template data', 'This hook allows to modify data which is about to be used in template for product without price grid', 1, 1),
(521, 'actionOrderGridPresenterModifier', 'Modify order grid template data', 'This hook allows to modify data which is about to be used in template for order grid', 1, 1),
(522, 'actionCatalogPriceRuleGridPresenterModifier', 'Modify catalog price rule grid template data', 'This hook allows to modify data which is about to be used in template for catalog price rule grid', 1, 1),
(523, 'actionOrderMessageGridPresenterModifier', 'Modify order message grid template data', 'This hook allows to modify data which is about to be used in template for order message grid', 1, 1),
(524, 'actionAttachmentGridPresenterModifier', 'Modify attachment grid template data', 'This hook allows to modify data which is about to be used in template for attachment grid', 1, 1),
(525, 'actionAttributeGroupGridPresenterModifier', 'Modify attribute group grid template data', 'This hook allows to modify data which is about to be used in template for attribute group grid', 1, 1),
(526, 'actionMerchandiseReturnGridPresenterModifier', 'Modify merchandise return grid template data', 'This hook allows to modify data which is about to be used in template for merchandise return grid', 1, 1),
(527, 'actionTaxRulesGroupGridPresenterModifier', 'Modify tax rules group grid template data', 'This hook allows to modify data which is about to be used in template for tax rules group grid', 1, 1),
(528, 'actionAddressGridPresenterModifier', 'Modify address grid template data', 'This hook allows to modify data which is about to be used in template for address grid', 1, 1),
(529, 'actionCreditSlipGridPresenterModifier', 'Modify credit slip grid template data', 'This hook allows to modify data which is about to be used in template for credit slip grid', 1, 1),
(530, 'displayAdditionalCustomerAddressFields', 'Display additional customer address fields', 'This hook allows to display extra field values added in an address form using hook \'additionalCustomerAddressFields\'', 1, 1),
(531, 'displayFooterCategory', 'Category footer', 'This hook adds new blocks under the products listing in a category/search', 1, 1),
(532, 'displayHeaderCategory', 'Category header', 'This hook adds new blocks above the products listing in a category/search', 1, 1),
(533, 'actionAdminAdministrationControllerPostProcessBefore', 'On post-process in Admin Configure Advanced Parameters Administration Controller', 'This hook is called on Admin Configure Advanced Parameters Administration post-process before processing any form', 1, 1),
(534, 'actionAdminAdministrationControllerPostProcessGeneralBefore', 'On post-process in Admin Configure Advanced Parameters Administration Controller', 'This hook is called on Admin Configure Advanced Parameters Administration post-process before processing the General form', 1, 1),
(535, 'actionAdminAdministrationControllerPostProcessUploadQuotaBefore', 'On post-process in Admin Configure Advanced Parameters Administration Controller', 'This hook is called on Admin Configure Advanced Parameters Administration post-process before processing the Upload Quota form', 1, 1),
(536, 'actionAdminAdministrationControllerPostProcessNotificationsBefore', 'On post-process in Admin Configure Advanced Parameters Administration Controller', 'This hook is called on Admin Configure Advanced Parameters Administration post-process before processing the Notifications form', 1, 1),
(537, 'actionAdminAdvancedParametersPerformanceControllerPostProcessSmartyBefore', 'On post-process in Admin Configure Advanced Parameters Performance Controller', 'This hook is called on Admin Configure Advanced Parameters Performance post-process before processing the Smarty form', 1, 1),
(538, 'actionAdminAdvancedParametersPerformanceControllerPostProcessDebugModeBefore', 'On post-process in Admin Configure Advanced Parameters Performance Controller', 'This hook is called on Admin Configure Advanced Parameters Performance post-process before processing the Debug Mode form', 1, 1),
(539, 'actionAdminAdvancedParametersPerformanceControllerPostProcessOptionalFeaturesBefore', 'On post-process in Admin Configure Advanced Parameters Performance Controller', 'This hook is called on Admin Configure Advanced Parameters Performance post-process before processing the Optional Features form', 1, 1),
(540, 'actionAdminAdvancedParametersPerformanceControllerPostProcessCombineCompressCacheBefore', 'On post-process in Admin Configure Advanced Parameters Performance Controller', 'This hook is called on Admin Configure Advanced Parameters Performance post-process before processing the Combine Compress Cache form', 1, 1),
(541, 'actionAdminAdvancedParametersPerformanceControllerPostProcessMediaServersBefore', 'On post-process in Admin Configure Advanced Parameters Performance Controller', 'This hook is called on Admin Configure Advanced Parameters Performance post-process before processing the Media Servers form', 1, 1),
(542, 'actionAdminAdvancedParametersPerformanceControllerPostProcessCachingBefore', 'On post-process in Admin Configure Advanced Parameters Performance Controller', 'This hook is called on Admin Configure Advanced Parameters Performance post-process before processing the Caching form', 1, 1),
(543, 'actionAdminAdvancedParametersPerformanceControllerPostProcessBefore', 'On post-process in Admin Configure Advanced Parameters Performance Controller', 'This hook is called on Admin Configure Advanced Parameters Performance post-process before processing any form', 1, 1),
(544, 'actionAdminShopParametersMetaControllerPostProcessSetUpUrlsBefore', 'On post-process in Admin Configure Shop Parameters Meta Controller', 'This hook is called on Admin Configure Shop Parameters Meta post-process before processing the SetUp Urls form', 1, 1),
(545, 'actionAdminShopParametersMetaControllerPostProcessShopUrlsBefore', 'On post-process in Admin Configure Shop Parameters Meta Controller', 'This hook is called on Admin Configure Shop Parameters Meta post-process before processing the Shop Urls form', 1, 1),
(546, 'actionAdminShopParametersMetaControllerPostProcessUrlSchemaBefore', 'On post-process in Admin Configure Shop Parameters Meta Controller', 'This hook is called on Admin Configure Shop Parameters Meta post-process before processing the Url Schema form', 1, 1),
(547, 'actionAdminShopParametersMetaControllerPostProcessSeoOptionsBefore', 'On post-process in Admin Configure Shop Parameters Meta Controller', 'This hook is called on Admin Configure Shop Parameters Meta post-process before processing the Seo Options form', 1, 1),
(548, 'actionAdminAdminShopParametersMetaControllerPostProcessBefore', 'On post-process in Admin Configure Shop Parameters Meta Controller', 'This hook is called on Admin Configure Shop Parameters Meta post-process before processing any form', 1, 1),
(549, 'actionAdminShopParametersOrderPreferencesControllerPostProcessGeneralBefore', 'On post-process in Admin Configure Shop Parameters Order Preferences Controller', 'This hook is called on Admin Configure Shop Parameters Order Preferences post-process before processing the General form', 1, 1),
(550, 'actionAdminShopParametersOrderPreferencesControllerPostProcessGiftOptionsBefore', 'On post-process in Admin Configure Shop Parameters Order Preferences Controller', 'This hook is called on Admin Configure Shop Parameters Order Preferences post-process before processing the Gift Options form', 1, 1),
(551, 'actionAdminShopParametersOrderPreferencesControllerPostProcessBefore', 'On post-process in Admin Configure Shop Parameters Order Preferences Controller', 'This hook is called on Admin Configure Shop Parameters Order Preferences post-process before processing any form', 1, 1),
(552, 'actionAdminInternationalGeolocationControllerPostProcessByIpAddressBefore', 'On post-process in Admin Improve International Geolocation Controller', 'This hook is called on Admin Improve International Geolocation post-process before processing the By Ip Address form', 1, 1),
(553, 'actionAdminInternationalGeolocationControllerPostProcessWhitelistBefore', 'On post-process in Admin Improve International Geolocation Controller', 'This hook is called on Admin Improve International Geolocation post-process before processing the Whitelist form', 1, 1),
(554, 'actionAdminInternationalGeolocationControllerPostProcessOptionsBefore', 'On post-process in Admin Improve International Geolocation Controller', 'This hook is called on Admin Improve International Geolocation post-process before processing the Options form', 1, 1),
(555, 'actionAdminInternationalGeolocationControllerPostProcessBefore', 'On post-process in Admin Improve International Geolocation Controller', 'This hook is called on Admin Improve International Geolocation post-process before processing any form', 1, 1),
(556, 'actionAdminInternationalLocalizationControllerPostProcessConfigurationBefore', 'On post-process in Admin Improve International Localization Controller', 'This hook is called on Admin Improve International Localization post-process before processing the Configuration form', 1, 1),
(557, 'actionAdminInternationalLocalizationControllerPostProcessLocalUnitsBefore', 'On post-process in Admin Improve International Localization Controller', 'This hook is called on Admin Improve International Localization post-process before processing the Local Units form', 1, 1),
(558, 'actionAdminInternationalLocalizationControllerPostProcessAdvancedBefore', 'On post-process in Admin Improve International Localization Controller', 'This hook is called on Admin Improve International Localization post-process before processing the Advanced form', 1, 1),
(559, 'actionAdminInternationalLocalizationControllerPostProcessBefore', 'On post-process in Admin Improve International Localization Controller', 'This hook is called on Admin Improve International Localization post-process before processing any form', 1, 1),
(560, 'actionAdminShippingPreferencesControllerPostProcessHandlingBefore', 'On post-process in Admin Improve Shipping Preferences Controller', 'This hook is called on Admin Improve Shipping Preferences post-process before processing the Handling form', 1, 1),
(561, 'actionAdminShippingPreferencesControllerPostProcessCarrierOptionsBefore', 'On post-process in Admin Improve Shipping Preferences Controller', 'This hook is called on Admin Improve Shipping Preferences post-process before processing the Carrier Options form', 1, 1),
(562, 'actionAdminShippingPreferencesControllerPostProcessBefore', 'On post-process in Admin Improve Shipping Preferences Controller', 'This hook is called on Admin Improve Shipping Preferences post-process before processing any form', 1, 1),
(563, 'actionCheckoutRender', 'Modify checkout process', 'This hook is called when constructing the checkout process', 1, 1),
(564, 'actionPresentProductListing', 'Product Listing Presenter', 'This hook is called before a product listing is presented', 1, 1),
(565, 'actionGetProductPropertiesAfterUnitPrice', 'Product Properties', 'This hook is called after defining the properties of a product', 1, 1),
(566, 'actionOverrideEmployeeImage', 'Get Employee Image', 'This hook is used to get the employee image', 1, 1),
(567, 'actionProductSearchProviderRunQueryBefore', 'Runs an action before ProductSearchProviderInterface::RunQuery()', 'Required to modify an SQL query before executing it', 1, 1),
(568, 'actionProductSearchProviderRunQueryAfter', 'Runs an action after ProductSearchProviderInterface::RunQuery()', 'Required to return a previous state of an SQL query or/and to change a result of the SQL query after executing it', 1, 1),
(569, 'actionFrontControllerSetVariables', 'Add variables in JavaScript object and Smarty templates', 'Add variables to javascript object that is available in Front Office. These are also available in smarty templates in modules.your_module_name.', 1, 1),
(570, 'displayAdminOrderCreateExtraButtons', 'Add buttons on the create order page dropdown', 'Add buttons on the create order page dropdown', 1, 1),
(573, 'actionProductFormBuilderModifier', 'Modify product identifiable object form', 'This hook allows to modify product identifiable object form content by modifying form builder data or FormBuilder itself', 1, 1),
(574, 'actionBeforeCreateProductFormHandler', 'Modify product identifiable object data before creating it', 'This hook allows to modify product identifiable object form data before it was created', 1, 1),
(576, 'actionBeforeUpdateProductFormHandler', 'Modify product identifiable object data before updating it', 'This hook allows to modify product identifiable object form data before it was updated', 1, 1),
(577, 'actionAfterUpdateProductFormHandler', 'Modify product identifiable object data after updating it', 'This hook allows to modify product identifiable object form data after it was updated', 1, 1),
(578, 'actionCustomerDiscountGridDefinitionModifier', 'Modify customer discount grid definition', 'This hook allows to alter customer discount grid columns, actions and filters', 1, 1),
(579, 'actionCustomerAddressGridDefinitionModifier', 'Modify customer address grid definition', 'This hook allows to alter customer address grid columns, actions and filters', 1, 1);
INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `active`, `position`) VALUES
(580, 'actionCartRuleGridDefinitionModifier', 'Modify cart rule grid definition', 'This hook allows to alter cart rule grid columns, actions and filters', 1, 1),
(581, 'actionOrderStatesGridDefinitionModifier', 'Modify order states grid definition', 'This hook allows to alter order states grid columns, actions and filters', 1, 1),
(582, 'actionOrderReturnStatesGridDefinitionModifier', 'Modify order return states grid definition', 'This hook allows to alter order return states grid columns, actions and filters', 1, 1),
(583, 'actionOutstandingGridDefinitionModifier', 'Modify outstanding grid definition', 'This hook allows to alter outstanding grid columns, actions and filters', 1, 1),
(584, 'actionCarrierGridDefinitionModifier', 'Modify carrier grid definition', 'This hook allows to alter carrier grid columns, actions and filters', 1, 1),
(585, 'actionZoneGridDefinitionModifier', 'Modify zone grid definition', 'This hook allows to alter zone grid columns, actions and filters', 1, 1),
(586, 'actionCustomerDiscountGridQueryBuilderModifier', 'Modify customer discount grid query builder', 'This hook allows to alter Doctrine query builder for customer discount grid', 1, 1),
(587, 'actionCustomerAddressGridQueryBuilderModifier', 'Modify customer address grid query builder', 'This hook allows to alter Doctrine query builder for customer address grid', 1, 1),
(588, 'actionCartRuleGridQueryBuilderModifier', 'Modify cart rule grid query builder', 'This hook allows to alter Doctrine query builder for cart rule grid', 1, 1),
(589, 'actionOrderStatesGridQueryBuilderModifier', 'Modify order states grid query builder', 'This hook allows to alter Doctrine query builder for order states grid', 1, 1),
(590, 'actionOrderReturnStatesGridQueryBuilderModifier', 'Modify order return states grid query builder', 'This hook allows to alter Doctrine query builder for order return states grid', 1, 1),
(591, 'actionOutstandingGridQueryBuilderModifier', 'Modify outstanding grid query builder', 'This hook allows to alter Doctrine query builder for outstanding grid', 1, 1),
(592, 'actionCarrierGridQueryBuilderModifier', 'Modify carrier grid query builder', 'This hook allows to alter Doctrine query builder for carrier grid', 1, 1),
(593, 'actionZoneGridQueryBuilderModifier', 'Modify zone grid query builder', 'This hook allows to alter Doctrine query builder for zone grid', 1, 1),
(594, 'actionCustomerDiscountGridDataModifier', 'Modify customer discount grid data', 'This hook allows to modify customer discount grid data', 1, 1),
(595, 'actionCustomerAddressGridDataModifier', 'Modify customer address grid data', 'This hook allows to modify customer address grid data', 1, 1),
(596, 'actionCartRuleGridDataModifier', 'Modify cart rule grid data', 'This hook allows to modify cart rule grid data', 1, 1),
(597, 'actionOrderStatesGridDataModifier', 'Modify order states grid data', 'This hook allows to modify order states grid data', 1, 1),
(598, 'actionOrderReturnStatesGridDataModifier', 'Modify order return states grid data', 'This hook allows to modify order return states grid data', 1, 1),
(599, 'actionOutstandingGridDataModifier', 'Modify outstanding grid data', 'This hook allows to modify outstanding grid data', 1, 1),
(600, 'actionCarrierGridDataModifier', 'Modify carrier grid data', 'This hook allows to modify carrier grid data', 1, 1),
(601, 'actionZoneGridDataModifier', 'Modify zone grid data', 'This hook allows to modify zone grid data', 1, 1),
(602, 'actionCustomerDiscountGridFilterFormModifier', 'Modify customer discount grid filters', 'This hook allows to modify filters for customer discount grid', 1, 1),
(603, 'actionCustomerAddressGridFilterFormModifier', 'Modify customer address grid filters', 'This hook allows to modify filters for customer address grid', 1, 1),
(604, 'actionCartRuleGridFilterFormModifier', 'Modify cart rule grid filters', 'This hook allows to modify filters for cart rule grid', 1, 1),
(605, 'actionOrderStatesGridFilterFormModifier', 'Modify order states grid filters', 'This hook allows to modify filters for order states grid', 1, 1),
(606, 'actionOrderReturnStatesGridFilterFormModifier', 'Modify order return states grid filters', 'This hook allows to modify filters for order return states grid', 1, 1),
(607, 'actionOutstandingGridFilterFormModifier', 'Modify outstanding grid filters', 'This hook allows to modify filters for outstanding grid', 1, 1),
(608, 'actionCarrierGridFilterFormModifier', 'Modify carrier grid filters', 'This hook allows to modify filters for carrier grid', 1, 1),
(609, 'actionZoneGridFilterFormModifier', 'Modify zone grid filters', 'This hook allows to modify filters for zone grid', 1, 1),
(610, 'actionCustomerDiscountGridPresenterModifier', 'Modify customer discount grid template data', 'This hook allows to modify data which is about to be used in template for customer discount grid\n      ', 1, 1),
(611, 'actionCustomerAddressGridPresenterModifier', 'Modify customer address grid template data', 'This hook allows to modify data which is about to be used in template for customer address grid\n      ', 1, 1),
(612, 'actionCartRuleGridPresenterModifier', 'Modify cart rule grid template data', 'This hook allows to modify data which is about to be used in template for cart rule grid\n      ', 1, 1),
(613, 'actionOrderStatesGridPresenterModifier', 'Modify order states grid template data', 'This hook allows to modify data which is about to be used in template for order states grid\n      ', 1, 1),
(614, 'actionOrderReturnStatesGridPresenterModifier', 'Modify order return states grid template data', 'This hook allows to modify data which is about to be used in template for order return states grid\n      ', 1, 1),
(615, 'actionOutstandingGridPresenterModifier', 'Modify outstanding grid template data', 'This hook allows to modify data which is about to be used in template for outstanding grid\n      ', 1, 1),
(616, 'actionCarrierGridPresenterModifier', 'Modify carrier grid template data', 'This hook allows to modify data which is about to be used in template for carrier grid', 1, 1),
(617, 'actionZoneGridPresenterModifier', 'Modify zone grid template data', 'This hook allows to modify data which is about to be used in template for zone grid', 1, 1),
(618, 'actionPerformancePageSmartyForm', 'Modify performance page smarty options form content', 'This hook allows to modify performance page smarty options form FormBuilder', 1, 1),
(619, 'actionPerformancePageDebugModeForm', 'Modify performance page debug mode options form content', 'This hook allows to modify performance page debug mode options form FormBuilder', 1, 1),
(620, 'actionPerformancePageOptionalFeaturesForm', 'Modify performance page optional features options form content', 'This hook allows to modify performance page optional features options form FormBuilder', 1, 1),
(621, 'actionPerformancePageCombineCompressCacheForm', 'Modify performance page combine compress cache options form content', 'This hook allows to modify performance page combine compress cache options form FormBuilder\n      ', 1, 1),
(622, 'actionPerformancePageMediaServersForm', 'Modify performance page media servers options form content', 'This hook allows to modify performance page media servers options form FormBuilder', 1, 1),
(623, 'actionPerformancePagecachingForm', 'Modify performance pagecaching options form content', 'This hook allows to modify performance pagecaching options form FormBuilder', 1, 1),
(624, 'actionAdministrationPageGeneralForm', 'Modify administration page general options form content', 'This hook allows to modify administration page general options form FormBuilder', 1, 1),
(625, 'actionAdministrationPageUploadQuotaForm', 'Modify administration page upload quota options form content', 'This hook allows to modify administration page upload quota options form FormBuilder', 1, 1),
(626, 'actionAdministrationPageNotificationsForm', 'Modify administration page notifications options form content', 'This hook allows to modify administration page notifications options form FormBuilder', 1, 1),
(627, 'actionShippingPreferencesPageHandlingForm', 'Modify shipping preferences page handling options form content', 'This hook allows to modify shipping preferences page handling options form FormBuilder', 1, 1),
(628, 'actionShippingPreferencesPageCarrierOptionsForm', 'Modify shipping preferences page carrier options options form content', 'This hook allows to modify shipping preferences page carrier options options form FormBuilder\n      ', 1, 1),
(629, 'actionOrderPreferencesPageGeneralForm', 'Modify order preferences page general options form content', 'This hook allows to modify order preferences page general options form FormBuilder', 1, 1),
(630, 'actionOrderPreferencesPageGiftOptionsForm', 'Modify order preferences page gift options options form content', 'This hook allows to modify order preferences page gift options options form FormBuilder', 1, 1),
(631, 'actionProductPreferencesPageGeneralForm', 'Modify product preferences page general options form content', 'This hook allows to modify product preferences page general options form FormBuilder', 1, 1),
(632, 'actionProductPreferencesPagePaginationForm', 'Modify product preferences page pagination options form content', 'This hook allows to modify product preferences page pagination options form FormBuilder', 1, 1),
(633, 'actionProductPreferencesPagePageForm', 'Modify product preferences page page options form content', 'This hook allows to modify product preferences page page options form FormBuilder', 1, 1),
(634, 'actionProductPreferencesPageStockForm', 'Modify product preferences page stock options form content', 'This hook allows to modify product preferences page stock options form FormBuilder', 1, 1),
(635, 'actionGeolocationPageByAddressForm', 'Modify geolocation page by address options form content', 'This hook allows to modify geolocation page by address options form FormBuilder', 1, 1),
(636, 'actionGeolocationPageWhitelistForm', 'Modify geolocation page whitelist options form content', 'This hook allows to modify geolocation page whitelist options form FormBuilder', 1, 1),
(637, 'actionGeolocationPageOptionsForm', 'Modify geolocation page options options form content', 'This hook allows to modify geolocation page options options form FormBuilder', 1, 1),
(638, 'actionLocalizationPageConfigurationForm', 'Modify localization page configuration options form content', 'This hook allows to modify localization page configuration options form FormBuilder', 1, 1),
(639, 'actionLocalizationPageLocalUnitsForm', 'Modify localization page local units options form content', 'This hook allows to modify localization page local units options form FormBuilder', 1, 1),
(640, 'actionLocalizationPageAdvancedForm', 'Modify localization page advanced options form content', 'This hook allows to modify localization page advanced options form FormBuilder', 1, 1),
(641, 'actionFeatureFlagForm', 'Modify feature flag page form content', 'This hook allows to modify the Feature Flag page form\'s FormBuilder', 1, 1),
(642, 'actionPerformancePageSmartySave', 'Modify performance page smarty options form saved data', 'This hook allows to modify data of performance page smarty options form after it was saved\n      ', 1, 1),
(643, 'actionPerformancePageDebugModeSave', 'Modify performance page debug mode options form saved data', 'This hook allows to modify data of performance page debug mode options form after it was saved\n      ', 1, 1),
(644, 'actionPerformancePageOptionalFeaturesSave', 'Modify performance page optional features options form saved data', 'This hook allows to modify data of performance page optional features options form after it was\n        saved\n      ', 1, 1),
(645, 'actionPerformancePageCombineCompressCacheSave', 'Modify performance page combine compress cache options form saved data', 'This hook allows to modify data of performance page combine compress cache options form after it was\n        saved\n      ', 1, 1),
(646, 'actionPerformancePageMediaServersSave', 'Modify performance page media servers options form saved data', 'This hook allows to modify data of performance page media servers options form after it was saved\n      ', 1, 1),
(647, 'actionPerformancePagecachingSave', 'Modify performance pagecaching options form saved data', 'This hook allows to modify data of performance pagecaching options form after it was saved\n      ', 1, 1),
(648, 'actionAdministrationPageGeneralSave', 'Modify administration page general options form saved data', 'This hook allows to modify data of administration page general options form after it was saved\n      ', 1, 1),
(649, 'actionAdministrationPageUploadQuotaSave', 'Modify administration page upload quota options form saved data', 'This hook allows to modify data of administration page upload quota options form after it was saved\n      ', 1, 1),
(650, 'actionAdministrationPageNotificationsSave', 'Modify administration page notifications options form saved data', 'This hook allows to modify data of administration page notifications options form after it was\n        saved\n      ', 1, 1),
(651, 'actionShippingPreferencesPageHandlingSave', 'Modify shipping preferences page handling options form saved data', 'This hook allows to modify data of shipping preferences page handling options form after it was\n        saved\n      ', 1, 1),
(652, 'actionShippingPreferencesPageCarrierOptionsSave', 'Modify shipping preferences page carrier options options form saved data', 'This hook allows to modify data of shipping preferences page carrier options options form after it\n        was saved\n      ', 1, 1),
(653, 'actionOrderPreferencesPageGeneralSave', 'Modify order preferences page general options form saved data', 'This hook allows to modify data of order preferences page general options form after it was saved\n      ', 1, 1),
(654, 'actionOrderPreferencesPageGiftOptionsSave', 'Modify order preferences page gift options options form saved data', 'This hook allows to modify data of order preferences page gift options options form after it was\n        saved\n      ', 1, 1),
(655, 'actionProductPreferencesPageGeneralSave', 'Modify product preferences page general options form saved data', 'This hook allows to modify data of product preferences page general options form after it was saved\n      ', 1, 1),
(656, 'actionProductPreferencesPagePaginationSave', 'Modify product preferences page pagination options form saved data', 'This hook allows to modify data of product preferences page pagination options form after it was\n        saved\n      ', 1, 1),
(657, 'actionProductPreferencesPagePageSave', 'Modify product preferences page page options form saved data', 'This hook allows to modify data of product preferences page page options form after it was saved\n      ', 1, 1),
(658, 'actionProductPreferencesPageStockSave', 'Modify product preferences page stock options form saved data', 'This hook allows to modify data of product preferences page stock options form after it was saved\n      ', 1, 1),
(659, 'actionGeolocationPageByAddressSave', 'Modify geolocation page by address options form saved data', 'This hook allows to modify data of geolocation page by address options form after it was saved\n      ', 1, 1),
(660, 'actionGeolocationPageWhitelistSave', 'Modify geolocation page whitelist options form saved data', 'This hook allows to modify data of geolocation page whitelist options form after it was saved\n      ', 1, 1),
(661, 'actionGeolocationPageOptionsSave', 'Modify geolocation page options options form saved data', 'This hook allows to modify data of geolocation page options options form after it was saved\n      ', 1, 1),
(662, 'actionLocalizationPageConfigurationSave', 'Modify localization page configuration options form saved data', 'This hook allows to modify data of localization page configuration options form after it was saved\n      ', 1, 1),
(663, 'actionLocalizationPageLocalUnitsSave', 'Modify localization page local units options form saved data', 'This hook allows to modify data of localization page local units options form after it was saved\n      ', 1, 1),
(664, 'actionLocalizationPageAdvancedSave', 'Modify localization page advanced options form saved data', 'This hook allows to modify data of localization page advanced options form after it was saved\n      ', 1, 1),
(665, 'actionFeatureFlagSave', 'Modify feature flag form submitted data', 'This hook allows to modify the Feature Flag data being submitted through the form after it was\n        saved\n      ', 1, 1),
(666, 'actionOrderStateFormBuilderModifier', 'Modify order state identifiable object form', 'This hook allows to modify order state identifiable object forms content by modifying form builder\n        data or FormBuilder itself\n      ', 1, 1),
(667, 'actionOrderReturnStateFormBuilderModifier', 'Modify order return state identifiable object form', 'This hook allows to modify order return state identifiable object forms content by modifying form\n        builder data or FormBuilder itself\n      ', 1, 1),
(668, 'actionZoneFormBuilderModifier', 'Modify zone identifiable object form', 'This hook allows to modify zone identifiable object forms content by modifying form builder data or\n        FormBuilder itself\n      ', 1, 1),
(669, 'actionBeforeUpdateOrderStateFormHandler', 'Modify order state identifiable object data before updating it', 'This hook allows to modify order state identifiable object forms data before it was updated\n      ', 1, 1),
(670, 'actionBeforeUpdateOrderReturnStateFormHandler', 'Modify order return state identifiable object data before updating it', 'This hook allows to modify order return state identifiable object forms data before it was updated\n      ', 1, 1),
(671, 'actionBeforeUpdateZoneFormHandler', 'Modify zone identifiable object data before updating it', 'This hook allows to modify zone identifiable object forms data before it was updated', 1, 1),
(672, 'actionAfterUpdateOrderStateFormHandler', 'Modify order state identifiable object data after updating it', 'This hook allows to modify order state identifiable object forms data after it was updated\n      ', 1, 1),
(673, 'actionAfterUpdateOrderReturnStateFormHandler', 'Modify order return state identifiable object data after updating it', 'This hook allows to modify order return state identifiable object forms data after it was updated\n      ', 1, 1),
(674, 'actionAfterUpdateProductImageFormHandler', 'Modify product image identifiable object data after updating it', 'This hook allows to modify product image identifiable object forms data after it was updated\n      ', 1, 1),
(675, 'actionAfterUpdateZoneFormHandler', 'Modify zone identifiable object data after updating it', 'This hook allows to modify zone identifiable object forms data after it was updated', 1, 1),
(676, 'actionBeforeCreateOrderStateFormHandler', 'Modify order state identifiable object data before creating it', 'This hook allows to modify order state identifiable object forms data before it was created\n      ', 1, 1),
(677, 'actionBeforeCreateOrderReturnStateFormHandler', 'Modify order return state identifiable object data before creating it', 'This hook allows to modify order return state identifiable object forms data before it was created\n      ', 1, 1),
(678, 'actionBeforeCreateZoneFormHandler', 'Modify zone identifiable object data before creating it', 'This hook allows to modify zone identifiable object forms data before it was created', 1, 1),
(679, 'actionAfterCreateOrderStateFormHandler', 'Modify order state identifiable object data after creating it', 'This hook allows to modify order state identifiable object forms data after it was created\n      ', 1, 1),
(680, 'actionAfterCreateOrderReturnStateFormHandler', 'Modify order return state identifiable object data after creating it', 'This hook allows to modify order return state identifiable object forms data after it was created\n      ', 1, 1),
(681, 'actionAfterCreateZoneFormHandler', 'Modify zone identifiable object data after creating it', 'This hook allows to modify zone identifiable object forms data after it was created', 1, 1),
(682, 'actionAdminControllerSetMedia', 'actionAdminControllerSetMedia', '', 1, 1),
(683, 'actionFrontControllerSetMedia', 'actionFrontControllerSetMedia', '', 1, 1),
(684, 'deleteProductAttribute', 'deleteProductAttribute', '', 1, 1),
(685, 'registerGDPRConsent', 'registerGDPRConsent', '', 1, 1),
(686, 'dashboardZoneOne', 'dashboardZoneOne', '', 1, 1),
(687, 'dashboardData', 'dashboardData', '', 1, 1),
(688, 'actionObjectOrderAddAfter', 'actionObjectOrderAddAfter', '', 1, 1),
(689, 'actionObjectCustomerAddAfter', 'actionObjectCustomerAddAfter', '', 1, 1),
(690, 'actionObjectCustomerMessageAddAfter', 'actionObjectCustomerMessageAddAfter', '', 1, 1),
(691, 'actionObjectCustomerThreadAddAfter', 'actionObjectCustomerThreadAddAfter', '', 1, 1),
(692, 'actionObjectOrderReturnAddAfter', 'actionObjectOrderReturnAddAfter', '', 1, 1),
(693, 'dashboardZoneTwo', 'dashboardZoneTwo', '', 1, 1),
(694, 'actionSearch', 'actionSearch', '', 1, 1),
(695, 'GraphEngine', 'GraphEngine', '', 1, 1),
(696, 'GridEngine', 'GridEngine', '', 1, 1),
(697, 'gSitemapAppendUrls', 'GSitemap Append URLs', 'This hook allows a module to add URLs to a generated sitemap', 1, 1),
(698, 'displayProductListReviews', 'displayProductListReviews', '', 1, 1),
(699, 'actionDeleteGDPRCustomer', 'actionDeleteGDPRCustomer', '', 1, 1),
(700, 'actionExportGDPRData', 'actionExportGDPRData', '', 1, 1),
(701, 'actionObjectLanguageAddAfter', 'actionObjectLanguageAddAfter', '', 1, 1),
(702, 'paymentOptions', 'paymentOptions', '', 1, 1),
(703, 'paymentReturn', 'paymentReturn', '', 1, 1),
(704, 'displayNav1', 'displayNav1', '', 1, 1),
(705, 'actionAdminStoresControllerUpdate_optionsAfter', 'actionAdminStoresControllerUpdate_optionsAfter', '', 1, 1),
(706, 'actionAdminCurrenciesControllerSaveAfter', 'actionAdminCurrenciesControllerSaveAfter', '', 1, 1),
(707, 'actionModuleRegisterHookAfter', 'actionModuleRegisterHookAfter', '', 1, 1),
(708, 'actionModuleUnRegisterHookAfter', 'actionModuleUnRegisterHookAfter', '', 1, 1),
(709, 'actionShopDataDuplication', 'actionShopDataDuplication', '', 1, 1),
(710, 'displayFooterBefore', 'displayFooterBefore', '', 1, 1),
(711, 'actionObjectCustomerUpdateBefore', 'actionObjectCustomerUpdateBefore', '', 1, 1),
(712, 'displayAdminCustomersForm', 'displayAdminCustomersForm', '', 1, 1),
(713, 'productSearchProvider', 'productSearchProvider', '', 1, 1),
(714, 'actionObjectSpecificPriceRuleUpdateBefore', 'actionObjectSpecificPriceRuleUpdateBefore', '', 1, 1),
(715, 'actionAdminSpecificPriceRuleControllerSaveAfter', 'actionAdminSpecificPriceRuleControllerSaveAfter', '', 1, 1),
(716, 'displayOrderConfirmation2', 'displayOrderConfirmation2', '', 1, 1),
(717, 'displayCrossSellingShoppingCart', 'displayCrossSellingShoppingCart', '', 1, 1),
(718, 'actionAdminGroupsControllerSaveAfter', 'actionAdminGroupsControllerSaveAfter', '', 1, 1),
(719, 'actionObjectCategoryUpdateAfter', 'actionObjectCategoryUpdateAfter', '', 1, 1),
(720, 'actionObjectCategoryDeleteAfter', 'actionObjectCategoryDeleteAfter', '', 1, 1),
(721, 'actionObjectCategoryAddAfter', 'actionObjectCategoryAddAfter', '', 1, 1),
(722, 'actionObjectCmsUpdateAfter', 'actionObjectCmsUpdateAfter', '', 1, 1),
(723, 'actionObjectCmsDeleteAfter', 'actionObjectCmsDeleteAfter', '', 1, 1),
(724, 'actionObjectCmsAddAfter', 'actionObjectCmsAddAfter', '', 1, 1),
(725, 'actionObjectSupplierUpdateAfter', 'actionObjectSupplierUpdateAfter', '', 1, 1),
(726, 'actionObjectSupplierDeleteAfter', 'actionObjectSupplierDeleteAfter', '', 1, 1),
(727, 'actionObjectSupplierAddAfter', 'actionObjectSupplierAddAfter', '', 1, 1),
(728, 'actionObjectManufacturerUpdateAfter', 'actionObjectManufacturerUpdateAfter', '', 1, 1),
(729, 'actionObjectManufacturerDeleteAfter', 'actionObjectManufacturerDeleteAfter', '', 1, 1),
(730, 'actionObjectManufacturerAddAfter', 'actionObjectManufacturerAddAfter', '', 1, 1),
(731, 'actionObjectProductUpdateAfter', 'actionObjectProductUpdateAfter', '', 1, 1),
(732, 'actionObjectProductDeleteAfter', 'actionObjectProductDeleteAfter', '', 1, 1),
(733, 'actionObjectProductAddAfter', 'actionObjectProductAddAfter', '', 1, 1),
(734, 'displaySearch', 'displaySearch', '', 1, 1),
(735, 'displayProductButtons', 'displayProductButtons', '', 1, 1),
(736, 'displayNav2', 'displayNav2', '', 1, 1),
(737, 'AdminStatsModules', 'AdminStatsModules', '', 1, 1),
(738, 'authentication', 'authentication', '', 1, 1),
(739, 'createAccount', 'createAccount', '', 1, 1),
(740, 'displayAdminNavBarBeforeEnd', 'displayAdminNavBarBeforeEnd', '', 1, 1),
(741, 'displayAdminAfterHeader', 'displayAdminAfterHeader', '', 1, 1),
(742, 'displayGDPRConsent', 'displayGDPRConsent', '', 1, 1),
(743, 'displayAdminOrderLeft', 'displayAdminOrderLeft', '', 1, 1),
(744, 'actionObjectShopAddAfter', 'actionObjectShopAddAfter', '', 1, 1),
(745, 'displayProductPriceBlock', 'displayProductPriceBlock', '', 1, 1),
(746, 'header', 'header', '', 1, 1),
(747, 'actionObjectOrderPaymentAddAfter', 'actionObjectOrderPaymentAddAfter', '', 1, 1),
(748, 'actionObjectOrderPaymentUpdateAfter', 'actionObjectOrderPaymentUpdateAfter', '', 1, 1),
(749, 'displayExpressCheckout', 'displayExpressCheckout', '', 1, 1),
(750, 'actionCartUpdateQuantityBefore', 'actionCartUpdateQuantityBefore', '', 1, 1),
(751, 'actionAjaxDieProductControllerDisplayAjaxQuickviewAfter', 'actionAjaxDieProductControllerDisplayAjaxQuickviewAfter', '', 1, 1),
(752, 'actionNewsletterRegistrationAfter', 'actionNewsletterRegistrationAfter', '', 1, 1),
(753, 'actionFacebookCallPixel', 'actionFacebookCallPixel', '', 1, 1),
(754, 'displayFooterAfter', 'displayFooterAfter', '', 1, 1),
(755, 'displayReassurance', 'displayReassurance', '', 1, 1),
(756, 'displayNav3', 'displayNav3', 'Add a widget area above the Nav3', 1, 1),
(757, 'displayFooterBottom', 'displayFooterBottom', 'Add a widget area at the most bottom of the theme', 1, 1),
(758, 'displayPriceBottom', 'displayPriceBottom', 'Add a widget area at the bottom of the price in product single', 1, 1),
(759, 'actionCrazyBeforeInit', 'actionCrazyBeforeInit', '', 1, 1),
(760, 'actionCrazyAddCategory', 'actionCrazyAddCategory', '', 1, 1),
(761, 'backOfficeHeader', 'backOfficeHeader', '', 1, 1),
(762, 'backOfficeFooter', 'backOfficeFooter', '', 1, 1),
(763, 'actionObjectAddAfter', 'actionObjectAddAfter', '', 1, 1),
(764, 'actionObjectUpdateAfter', 'actionObjectUpdateAfter', '', 1, 1),
(765, 'overrideLayoutTemplate', 'overrideLayoutTemplate', '', 1, 1),
(766, 'displayAfterThemeInstallation', '', '', 1, 1),
(767, 'actionGenHookImportant', 'actionGenHookImportant', '', 1, 1),
(768, 'home', 'home', '', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_hook_alias`
--

DROP TABLE IF EXISTS `ps_hook_alias`;
CREATE TABLE IF NOT EXISTS `ps_hook_alias` (
  `id_hook_alias` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `alias` varchar(191) NOT NULL,
  `name` varchar(191) NOT NULL,
  PRIMARY KEY (`id_hook_alias`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_hook_alias`
--

INSERT INTO `ps_hook_alias` (`id_hook_alias`, `alias`, `name`) VALUES
(1, 'newOrder', 'actionValidateOrder'),
(2, 'paymentConfirm', 'actionPaymentConfirmation'),
(3, 'paymentReturn', 'displayPaymentReturn'),
(4, 'updateQuantity', 'actionUpdateQuantity'),
(5, 'rightColumn', 'displayRightColumn'),
(6, 'leftColumn', 'displayLeftColumn'),
(7, 'home', 'displayHome'),
(8, 'Header', 'displayHeader'),
(9, 'cart', 'actionCartSave'),
(10, 'authentication', 'actionAuthentication'),
(11, 'addproduct', 'actionProductAdd'),
(12, 'updateproduct', 'actionProductUpdate'),
(13, 'top', 'displayTop'),
(14, 'extraRight', 'displayRightColumnProduct'),
(15, 'deleteproduct', 'actionProductDelete'),
(16, 'productfooter', 'displayFooterProduct'),
(17, 'invoice', 'displayInvoice'),
(18, 'updateOrderStatus', 'actionOrderStatusUpdate'),
(19, 'adminOrder', 'displayAdminOrder'),
(20, 'footer', 'displayFooter'),
(21, 'PDFInvoice', 'displayPDFInvoice'),
(22, 'adminCustomers', 'displayAdminCustomers'),
(23, 'orderConfirmation', 'displayOrderConfirmation'),
(24, 'createAccount', 'actionCustomerAccountAdd'),
(25, 'customerAccount', 'displayCustomerAccount'),
(26, 'orderSlip', 'actionOrderSlipAdd'),
(27, 'shoppingCart', 'displayShoppingCartFooter'),
(28, 'createAccountForm', 'displayCustomerAccountForm'),
(29, 'AdminStatsModules', 'displayAdminStatsModules'),
(30, 'GraphEngine', 'displayAdminStatsGraphEngine'),
(31, 'orderReturn', 'actionOrderReturn'),
(32, 'productActions', 'displayProductAdditionalInfo'),
(33, 'displayProductButtons', 'displayProductAdditionalInfo'),
(34, 'backOfficeHome', 'displayBackOfficeHome'),
(35, 'GridEngine', 'displayAdminStatsGridEngine'),
(36, 'watermark', 'actionWatermark'),
(37, 'cancelProduct', 'actionProductCancel'),
(38, 'extraLeft', 'displayLeftColumnProduct'),
(39, 'productOutOfStock', 'actionProductOutOfStock'),
(40, 'updateProductAttribute', 'actionProductAttributeUpdate'),
(41, 'extraCarrier', 'displayCarrierList'),
(42, 'shoppingCartExtra', 'displayShoppingCart'),
(43, 'updateCarrier', 'actionCarrierUpdate'),
(44, 'postUpdateOrderStatus', 'actionOrderStatusPostUpdate'),
(45, 'createAccountTop', 'displayCustomerAccountFormTop'),
(46, 'backOfficeHeader', 'displayBackOfficeHeader'),
(47, 'backOfficeTop', 'displayBackOfficeTop'),
(48, 'backOfficeFooter', 'displayBackOfficeFooter'),
(49, 'deleteProductAttribute', 'actionProductAttributeDelete'),
(50, 'processCarrier', 'actionCarrierProcess'),
(51, 'beforeCarrier', 'displayBeforeCarrier'),
(52, 'orderDetailDisplayed', 'displayOrderDetail'),
(53, 'paymentCCAdded', 'actionPaymentCCAdd'),
(54, 'categoryAddition', 'actionCategoryAdd'),
(55, 'categoryUpdate', 'actionCategoryUpdate'),
(56, 'categoryDeletion', 'actionCategoryDelete'),
(57, 'paymentTop', 'displayPaymentTop'),
(58, 'afterCreateHtaccess', 'actionHtaccessCreate'),
(59, 'afterSaveAdminMeta', 'actionAdminMetaSave'),
(60, 'attributeGroupForm', 'displayAttributeGroupForm'),
(61, 'afterSaveAttributeGroup', 'actionAttributeGroupSave'),
(62, 'afterDeleteAttributeGroup', 'actionAttributeGroupDelete'),
(63, 'featureForm', 'displayFeatureForm'),
(64, 'afterSaveFeature', 'actionFeatureSave'),
(65, 'afterDeleteFeature', 'actionFeatureDelete'),
(66, 'afterSaveProduct', 'actionProductSave'),
(67, 'postProcessAttributeGroup', 'displayAttributeGroupPostProcess'),
(68, 'postProcessFeature', 'displayFeaturePostProcess'),
(69, 'featureValueForm', 'displayFeatureValueForm'),
(70, 'postProcessFeatureValue', 'displayFeatureValuePostProcess'),
(71, 'afterDeleteFeatureValue', 'actionFeatureValueDelete'),
(72, 'afterSaveFeatureValue', 'actionFeatureValueSave'),
(73, 'attributeForm', 'displayAttributeForm'),
(74, 'postProcessAttribute', 'actionAttributePostProcess'),
(75, 'afterDeleteAttribute', 'actionAttributeDelete'),
(76, 'afterSaveAttribute', 'actionAttributeSave'),
(77, 'taxManager', 'actionTaxManager'),
(78, 'myAccountBlock', 'displayMyAccountBlock'),
(79, 'actionBeforeCartUpdateQty', 'actionCartUpdateQuantityBefore'),
(80, 'actionBeforeAjaxDie', 'actionAjaxDieBefore'),
(81, 'actionBeforeAuthentication', 'actionAuthenticationBefore'),
(82, 'actionBeforeSubmitAccount', 'actionSubmitAccountBefore'),
(83, 'actionAfterDeleteProductInCart', 'actionDeleteProductInCartAfter'),
(84, 'displayInvoice', 'displayAdminOrderTop'),
(85, 'displayBackOfficeOrderActions', 'displayAdminOrderSide'),
(86, 'actionFrontControllerAfterInit', 'actionFrontControllerInitAfter'),
(87, 'displayAdminListBefore', 'displayAdminGridTableBefore'),
(88, 'displayAdminListAfter', 'displayAdminGridTableAfter');

-- --------------------------------------------------------

--
-- Structure de la table `ps_hook_module`
--

DROP TABLE IF EXISTS `ps_hook_module`;
CREATE TABLE IF NOT EXISTS `ps_hook_module` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_hook` int(10) UNSIGNED NOT NULL,
  `position` tinyint(2) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_module`,`id_hook`,`id_shop`),
  KEY `id_hook` (`id_hook`),
  KEY `id_module` (`id_module`),
  KEY `position` (`id_shop`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_hook_module`
--

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES
(60, 1, 693, 0),
(1, 1, 28, 1),
(1, 1, 75, 1),
(1, 1, 102, 1),
(1, 1, 682, 1),
(1, 1, 683, 1),
(1, 1, 684, 1),
(2, 1, 685, 1),
(3, 1, 687, 1),
(3, 1, 688, 1),
(3, 1, 689, 1),
(3, 1, 690, 1),
(3, 1, 691, 1),
(3, 1, 692, 1),
(4, 1, 69, 1),
(4, 1, 693, 1),
(6, 1, 694, 1),
(7, 1, 695, 1),
(8, 1, 696, 1),
(10, 1, 55, 1),
(11, 1, 31, 1),
(11, 1, 135, 1),
(11, 1, 698, 1),
(11, 1, 699, 1),
(11, 1, 700, 1),
(12, 1, 701, 1),
(13, 1, 14, 1),
(14, 1, 703, 1),
(15, 1, 704, 1),
(15, 1, 705, 1),
(17, 1, 706, 1),
(18, 1, 707, 1),
(18, 1, 708, 1),
(20, 1, 709, 1),
(21, 1, 122, 1),
(22, 1, 47, 1),
(22, 1, 48, 1),
(22, 1, 710, 1),
(22, 1, 711, 1),
(24, 1, 71, 1),
(25, 1, 19, 1),
(25, 1, 20, 1),
(25, 1, 82, 1),
(25, 1, 716, 1),
(25, 1, 717, 1),
(25, 1, 718, 1),
(26, 1, 15, 1),
(27, 1, 736, 1),
(28, 1, 41, 1),
(28, 1, 141, 1),
(29, 1, 25, 1),
(29, 1, 719, 1),
(29, 1, 720, 1),
(29, 1, 721, 1),
(29, 1, 722, 1),
(29, 1, 723, 1),
(29, 1, 724, 1),
(29, 1, 725, 1),
(29, 1, 726, 1),
(29, 1, 727, 1),
(29, 1, 728, 1),
(29, 1, 729, 1),
(29, 1, 730, 1),
(29, 1, 731, 1),
(29, 1, 732, 1),
(29, 1, 733, 1),
(30, 1, 734, 1),
(31, 1, 58, 1),
(35, 1, 7, 1),
(37, 1, 737, 1),
(44, 1, 24, 1),
(44, 1, 738, 1),
(44, 1, 739, 1),
(53, 1, 740, 1),
(53, 1, 741, 1),
(55, 1, 686, 1),
(56, 1, 742, 1),
(59, 1, 30, 1),
(59, 1, 43, 1),
(59, 1, 46, 1),
(59, 1, 84, 1),
(59, 1, 121, 1),
(59, 1, 208, 1),
(59, 1, 417, 1),
(59, 1, 702, 1),
(59, 1, 743, 1),
(59, 1, 744, 1),
(59, 1, 745, 1),
(59, 1, 747, 1),
(59, 1, 748, 1),
(59, 1, 749, 1),
(59, 1, 750, 1),
(61, 1, 17, 1),
(61, 1, 751, 1),
(61, 1, 752, 1),
(61, 1, 753, 1),
(63, 1, 755, 1),
(64, 1, 81, 1),
(64, 1, 83, 1),
(64, 1, 88, 1),
(64, 1, 89, 1),
(64, 1, 91, 1),
(64, 1, 92, 1),
(64, 1, 93, 1),
(64, 1, 98, 1),
(64, 1, 99, 1),
(64, 1, 101, 1),
(64, 1, 103, 1),
(64, 1, 426, 1),
(64, 1, 434, 1),
(64, 1, 442, 1),
(64, 1, 658, 1),
(64, 1, 713, 1),
(64, 1, 714, 1),
(64, 1, 715, 1),
(66, 1, 16, 1),
(66, 1, 140, 1),
(66, 1, 756, 1),
(66, 1, 757, 1),
(66, 1, 766, 1),
(66, 1, 767, 1),
(66, 1, 768, 1),
(67, 1, 110, 1),
(67, 1, 223, 1),
(67, 1, 746, 1),
(67, 1, 759, 1),
(67, 1, 760, 1),
(67, 1, 761, 1),
(67, 1, 762, 1),
(67, 1, 763, 1),
(67, 1, 764, 1),
(67, 1, 765, 1),
(3, 1, 682, 2),
(3, 1, 686, 2),
(4, 1, 687, 2),
(5, 1, 693, 2),
(6, 1, 688, 2),
(11, 1, 685, 2),
(14, 1, 702, 2),
(16, 1, 31, 2),
(16, 1, 69, 2),
(17, 1, 736, 2),
(18, 1, 41, 2),
(22, 1, 122, 2),
(22, 1, 683, 2),
(22, 1, 699, 2),
(22, 1, 700, 2),
(25, 1, 15, 2),
(25, 1, 28, 2),
(26, 1, 709, 2),
(29, 1, 82, 2),
(30, 1, 25, 2),
(33, 1, 710, 2),
(36, 1, 55, 2),
(39, 1, 737, 2),
(51, 1, 694, 2),
(53, 1, 71, 2),
(54, 1, 741, 2),
(56, 1, 47, 2),
(61, 1, 46, 2),
(61, 1, 690, 2),
(62, 1, 750, 2),
(64, 1, 14, 2),
(65, 1, 731, 2),
(65, 1, 732, 2),
(66, 1, 759, 2),
(66, 1, 760, 2),
(67, 1, 16, 2),
(67, 1, 140, 2),
(68, 1, 746, 2),
(68, 1, 761, 2),
(1, 1, 16, 3),
(4, 1, 682, 3),
(5, 1, 687, 3),
(6, 1, 693, 3),
(12, 1, 15, 3),
(15, 1, 41, 3),
(19, 1, 736, 3),
(22, 1, 685, 3),
(29, 1, 709, 3),
(35, 1, 702, 3),
(38, 1, 55, 3),
(42, 1, 737, 3),
(54, 1, 71, 3),
(56, 1, 122, 3),
(59, 1, 31, 3),
(59, 1, 683, 3),
(59, 1, 741, 3),
(61, 1, 47, 3),
(61, 1, 694, 3),
(62, 1, 46, 3),
(64, 1, 82, 3),
(5, 1, 682, 4),
(6, 1, 687, 4),
(20, 1, 15, 4),
(32, 1, 736, 4),
(40, 1, 55, 4),
(52, 1, 737, 4),
(60, 1, 741, 4),
(61, 1, 71, 4),
(61, 1, 683, 4),
(65, 1, 31, 4),
(7, 1, 682, 5),
(41, 1, 55, 5),
(62, 1, 71, 5),
(63, 1, 683, 5),
(43, 1, 55, 6),
(54, 1, 682, 6),
(45, 1, 55, 7),
(56, 1, 682, 7),
(46, 1, 55, 8),
(57, 1, 682, 8),
(47, 1, 55, 9),
(59, 1, 682, 9),
(48, 1, 55, 10),
(60, 1, 682, 10),
(49, 1, 55, 11),
(50, 1, 55, 12),
(51, 1, 55, 13);

-- --------------------------------------------------------

--
-- Structure de la table `ps_hook_module_exceptions`
--

DROP TABLE IF EXISTS `ps_hook_module_exceptions`;
CREATE TABLE IF NOT EXISTS `ps_hook_module_exceptions` (
  `id_hook_module_exceptions` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_hook` int(10) UNSIGNED NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_hook_module_exceptions`),
  KEY `id_module` (`id_module`),
  KEY `id_hook` (`id_hook`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_image`
--

DROP TABLE IF EXISTS `ps_image`;
CREATE TABLE IF NOT EXISTS `ps_image` (
  `id_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int(10) UNSIGNED NOT NULL,
  `position` smallint(2) UNSIGNED NOT NULL DEFAULT '0',
  `cover` tinyint(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id_image`),
  UNIQUE KEY `id_product_cover` (`id_product`,`cover`),
  UNIQUE KEY `idx_product_image` (`id_image`,`id_product`,`cover`),
  KEY `image_product` (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_image`
--

INSERT INTO `ps_image` (`id_image`, `id_product`, `position`, `cover`) VALUES
(24, 20, 1, 1),
(25, 20, 2, NULL),
(26, 23, 1, 1),
(27, 24, 1, 1),
(28, 24, 2, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ps_image_lang`
--

DROP TABLE IF EXISTS `ps_image_lang`;
CREATE TABLE IF NOT EXISTS `ps_image_lang` (
  `id_image` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `legend` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_image`,`id_lang`),
  KEY `id_image` (`id_image`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_image_lang`
--

INSERT INTO `ps_image_lang` (`id_image`, `id_lang`, `legend`) VALUES
(24, 1, ''),
(24, 2, ''),
(24, 3, ''),
(24, 4, ''),
(24, 5, ''),
(24, 6, ''),
(25, 1, ''),
(25, 2, ''),
(25, 3, ''),
(25, 4, ''),
(25, 5, ''),
(25, 6, ''),
(26, 1, ''),
(26, 2, ''),
(26, 3, ''),
(26, 4, ''),
(26, 5, ''),
(26, 6, ''),
(27, 1, ''),
(27, 2, ''),
(27, 3, ''),
(27, 4, ''),
(27, 5, ''),
(27, 6, ''),
(28, 1, ''),
(28, 2, ''),
(28, 3, ''),
(28, 4, ''),
(28, 5, ''),
(28, 6, '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_image_shop`
--

DROP TABLE IF EXISTS `ps_image_shop`;
CREATE TABLE IF NOT EXISTS `ps_image_shop` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_image` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `cover` tinyint(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id_image`,`id_shop`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`cover`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_image_shop`
--

INSERT INTO `ps_image_shop` (`id_product`, `id_image`, `id_shop`, `cover`) VALUES
(20, 25, 1, NULL),
(20, 24, 1, 1),
(23, 26, 1, 1),
(24, 28, 1, NULL),
(24, 27, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_image_type`
--

DROP TABLE IF EXISTS `ps_image_type`;
CREATE TABLE IF NOT EXISTS `ps_image_type` (
  `id_image_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `width` int(10) UNSIGNED NOT NULL,
  `height` int(10) UNSIGNED NOT NULL,
  `products` tinyint(1) NOT NULL DEFAULT '1',
  `categories` tinyint(1) NOT NULL DEFAULT '1',
  `manufacturers` tinyint(1) NOT NULL DEFAULT '1',
  `suppliers` tinyint(1) NOT NULL DEFAULT '1',
  `stores` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_image_type`),
  KEY `image_type_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_image_type`
--

INSERT INTO `ps_image_type` (`id_image_type`, `name`, `width`, `height`, `products`, `categories`, `manufacturers`, `suppliers`, `stores`) VALUES
(1, 'cart_default', 125, 125, 1, 0, 0, 0, 0),
(2, 'small_default', 98, 98, 1, 1, 1, 1, 0),
(3, 'medium_default', 452, 452, 1, 0, 1, 1, 0),
(4, 'home_default', 250, 250, 1, 0, 0, 0, 0),
(5, 'large_default', 800, 800, 1, 0, 1, 1, 0),
(6, 'category_default', 141, 180, 0, 1, 0, 0, 0),
(7, 'stores_default', 170, 115, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_import_match`
--

DROP TABLE IF EXISTS `ps_import_match`;
CREATE TABLE IF NOT EXISTS `ps_import_match` (
  `id_import_match` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `match` text NOT NULL,
  `skip` int(2) NOT NULL,
  PRIMARY KEY (`id_import_match`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_info`
--

DROP TABLE IF EXISTS `ps_info`;
CREATE TABLE IF NOT EXISTS `ps_info` (
  `id_info` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_info`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_info`
--

INSERT INTO `ps_info` (`id_info`) VALUES
(1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_info_lang`
--

DROP TABLE IF EXISTS `ps_info_lang`;
CREATE TABLE IF NOT EXISTS `ps_info_lang` (
  `id_info` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id_info`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_info_lang`
--

INSERT INTO `ps_info_lang` (`id_info`, `id_shop`, `id_lang`, `text`) VALUES
(1, 1, 1, '<h2>Bienvenue au Dédale</h2>\r\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetu</strong></p>\r\n<p>Sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>'),
(1, 1, 2, '<h2>Custom Text Block</h2>\r\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetu</strong></p>\r\n<p>Sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>'),
(1, 1, 3, '<h2>Custom Text Block</h2>\r\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetu</strong></p>\r\n<p>Sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>'),
(1, 1, 4, '<h2>Custom Text Block</h2>\r\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetu</strong></p>\r\n<p>Sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>'),
(1, 1, 5, '<h2>Custom Text Block</h2>\r\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetu</strong></p>\r\n<p>Sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>'),
(1, 1, 6, '<h2>Custom Text Block</h2>\r\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetu</strong></p>\r\n<p>Sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>');

-- --------------------------------------------------------

--
-- Structure de la table `ps_info_shop`
--

DROP TABLE IF EXISTS `ps_info_shop`;
CREATE TABLE IF NOT EXISTS `ps_info_shop` (
  `id_info` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_info`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_info_shop`
--

INSERT INTO `ps_info_shop` (`id_info`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_lang`
--

DROP TABLE IF EXISTS `ps_lang`;
CREATE TABLE IF NOT EXISTS `ps_lang` (
  `id_lang` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `iso_code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_format_lite` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_format_full` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_rtl` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_lang`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_lang`
--

INSERT INTO `ps_lang` (`id_lang`, `name`, `active`, `iso_code`, `language_code`, `locale`, `date_format_lite`, `date_format_full`, `is_rtl`) VALUES
(1, 'English (English)', 1, 'en', 'en-us', 'en-US', 'm/d/Y', 'm/d/Y H:i:s', 0),
(2, 'Français (French)', 1, 'fr', 'fr', 'fr-FR', 'd/m/Y', 'd/m/Y H:i:s', 0),
(3, 'Deutsch (German)', 1, 'de', 'de', 'de-DE', 'd.m.Y', 'd.m.Y H:i:s', 0),
(4, 'Nederlands (Dutch)', 1, 'nl', 'nl-nl', 'nl-NL', 'd-m-Y', 'd-m-Y H:i:s', 0),
(5, 'Italiano (Italian)', 1, 'it', 'it-it', 'it-IT', 'd/m/Y', 'd/m/Y H:i:s', 0),
(6, 'Español (Spanish)', 1, 'es', 'es-es', 'es-ES', 'd/m/Y', 'd/m/Y H:i:s', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_lang_shop`
--

DROP TABLE IF EXISTS `ps_lang_shop`;
CREATE TABLE IF NOT EXISTS `ps_lang_shop` (
  `id_lang` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_lang`,`id_shop`),
  KEY `IDX_2F43BFC7BA299860` (`id_lang`),
  KEY `IDX_2F43BFC7274A50A0` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_lang_shop`
--

INSERT INTO `ps_lang_shop` (`id_lang`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_category`
--

DROP TABLE IF EXISTS `ps_layered_category`;
CREATE TABLE IF NOT EXISTS `ps_layered_category` (
  `id_layered_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_value` int(10) UNSIGNED DEFAULT '0',
  `type` enum('category','id_feature','id_attribute_group','quantity','condition','manufacturer','weight','price') NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `filter_type` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `filter_show_limit` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_layered_category`),
  KEY `id_category_shop` (`id_category`,`id_shop`,`type`,`id_value`,`position`),
  KEY `id_category` (`id_category`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_layered_category`
--

INSERT INTO `ps_layered_category` (`id_layered_category`, `id_shop`, `id_category`, `id_value`, `type`, `position`, `filter_type`, `filter_show_limit`) VALUES
(1, 1, 2, NULL, 'category', 1, 0, 0),
(2, 1, 2, 1, 'id_attribute_group', 2, 0, 0),
(3, 1, 2, 2, 'id_attribute_group', 3, 0, 0),
(4, 1, 2, 1, 'id_feature', 4, 0, 0),
(5, 1, 2, 2, 'id_feature', 5, 0, 0),
(6, 1, 2, NULL, 'quantity', 6, 0, 0),
(7, 1, 2, NULL, 'manufacturer', 7, 0, 0),
(8, 1, 2, NULL, 'condition', 8, 0, 0),
(9, 1, 2, NULL, 'weight', 9, 0, 0),
(10, 1, 2, NULL, 'price', 10, 0, 0),
(11, 1, 2, 3, 'id_attribute_group', 11, 0, 0),
(12, 1, 2, 4, 'id_attribute_group', 12, 0, 0),
(13, 1, 4, NULL, 'category', 1, 0, 0),
(14, 1, 4, 1, 'id_attribute_group', 2, 0, 0),
(15, 1, 4, 2, 'id_attribute_group', 3, 0, 0),
(16, 1, 4, 1, 'id_feature', 4, 0, 0),
(17, 1, 4, 2, 'id_feature', 5, 0, 0),
(18, 1, 4, NULL, 'quantity', 6, 0, 0),
(19, 1, 4, NULL, 'manufacturer', 7, 0, 0),
(20, 1, 4, NULL, 'condition', 8, 0, 0),
(21, 1, 4, NULL, 'weight', 9, 0, 0),
(22, 1, 4, NULL, 'price', 10, 0, 0),
(23, 1, 4, 3, 'id_attribute_group', 11, 0, 0),
(24, 1, 4, 4, 'id_attribute_group', 12, 0, 0),
(25, 1, 5, NULL, 'category', 1, 0, 0),
(26, 1, 5, 1, 'id_attribute_group', 2, 0, 0),
(27, 1, 5, 2, 'id_attribute_group', 3, 0, 0),
(28, 1, 5, 1, 'id_feature', 4, 0, 0),
(29, 1, 5, 2, 'id_feature', 5, 0, 0),
(30, 1, 5, NULL, 'quantity', 6, 0, 0),
(31, 1, 5, NULL, 'manufacturer', 7, 0, 0),
(32, 1, 5, NULL, 'condition', 8, 0, 0),
(33, 1, 5, NULL, 'weight', 9, 0, 0),
(34, 1, 5, NULL, 'price', 10, 0, 0),
(35, 1, 5, 3, 'id_attribute_group', 11, 0, 0),
(36, 1, 5, 4, 'id_attribute_group', 12, 0, 0),
(37, 1, 8, NULL, 'category', 1, 0, 0),
(38, 1, 8, 1, 'id_attribute_group', 2, 0, 0),
(39, 1, 8, 2, 'id_attribute_group', 3, 0, 0),
(40, 1, 8, 1, 'id_feature', 4, 0, 0),
(41, 1, 8, 2, 'id_feature', 5, 0, 0),
(42, 1, 8, NULL, 'quantity', 6, 0, 0),
(43, 1, 8, NULL, 'manufacturer', 7, 0, 0),
(44, 1, 8, NULL, 'condition', 8, 0, 0),
(45, 1, 8, NULL, 'weight', 9, 0, 0),
(46, 1, 8, NULL, 'price', 10, 0, 0),
(47, 1, 8, 3, 'id_attribute_group', 11, 0, 0),
(48, 1, 8, 4, 'id_attribute_group', 12, 0, 0),
(49, 1, 7, NULL, 'category', 1, 0, 0),
(50, 1, 7, 1, 'id_attribute_group', 2, 0, 0),
(51, 1, 7, 2, 'id_attribute_group', 3, 0, 0),
(52, 1, 7, 1, 'id_feature', 4, 0, 0),
(53, 1, 7, 2, 'id_feature', 5, 0, 0),
(54, 1, 7, NULL, 'quantity', 6, 0, 0),
(55, 1, 7, NULL, 'manufacturer', 7, 0, 0),
(56, 1, 7, NULL, 'condition', 8, 0, 0),
(57, 1, 7, NULL, 'weight', 9, 0, 0),
(58, 1, 7, NULL, 'price', 10, 0, 0),
(59, 1, 7, 3, 'id_attribute_group', 11, 0, 0),
(60, 1, 7, 4, 'id_attribute_group', 12, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_filter`
--

DROP TABLE IF EXISTS `ps_layered_filter`;
CREATE TABLE IF NOT EXISTS `ps_layered_filter` (
  `id_layered_filter` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `filters` longtext,
  `n_categories` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_layered_filter`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_layered_filter`
--

INSERT INTO `ps_layered_filter` (`id_layered_filter`, `name`, `filters`, `n_categories`, `date_add`) VALUES
(1, 'My template 2022-11-21', 'a:14:{s:10:\"categories\";a:5:{i:0;i:2;i:2;i:4;i:3;i:5;i:6;i:8;i:7;i:7;}s:9:\"shop_list\";a:1:{i:1;i:1;}s:31:\"layered_selection_subcategories\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_1\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_2\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:24:\"layered_selection_feat_1\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:24:\"layered_selection_feat_2\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:23:\"layered_selection_stock\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:30:\"layered_selection_manufacturer\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:27:\"layered_selection_condition\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:31:\"layered_selection_weight_slider\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:30:\"layered_selection_price_slider\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_3\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_4\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}}', 8, '2022-11-21 11:07:32');

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_filter_block`
--

DROP TABLE IF EXISTS `ps_layered_filter_block`;
CREATE TABLE IF NOT EXISTS `ps_layered_filter_block` (
  `hash` char(32) NOT NULL DEFAULT '',
  `data` text,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_layered_filter_block`
--

INSERT INTO `ps_layered_filter_block` (`hash`, `data`) VALUES
('3e9d6a92f7ecccd45167c73eecb7147b', 'a:1:{s:7:\"filters\";a:0:{}}'),
('fcf3eabd32b038c79d6add70176b1f9f', 'a:1:{s:7:\"filters\";a:0:{}}');

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_filter_shop`
--

DROP TABLE IF EXISTS `ps_layered_filter_shop`;
CREATE TABLE IF NOT EXISTS `ps_layered_filter_shop` (
  `id_layered_filter` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_layered_filter`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_layered_filter_shop`
--

INSERT INTO `ps_layered_filter_shop` (`id_layered_filter`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_indexable_attribute_group`
--

DROP TABLE IF EXISTS `ps_layered_indexable_attribute_group`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_attribute_group` (
  `id_attribute_group` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_attribute_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_layered_indexable_attribute_group`
--

INSERT INTO `ps_layered_indexable_attribute_group` (`id_attribute_group`, `indexable`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_indexable_attribute_group_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_attribute_group_lang_value`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_attribute_group_lang_value` (
  `id_attribute_group` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_attribute_group`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_indexable_attribute_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_attribute_lang_value`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_attribute_lang_value` (
  `id_attribute` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_attribute`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_indexable_feature`
--

DROP TABLE IF EXISTS `ps_layered_indexable_feature`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_feature` (
  `id_feature` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_feature`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_layered_indexable_feature`
--

INSERT INTO `ps_layered_indexable_feature` (`id_feature`, `indexable`) VALUES
(1, 0),
(2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_indexable_feature_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_feature_lang_value`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_feature_lang_value` (
  `id_feature` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_feature`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_indexable_feature_value_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_feature_value_lang_value`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_feature_value_lang_value` (
  `id_feature_value` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_price_index`
--

DROP TABLE IF EXISTS `ps_layered_price_index`;
CREATE TABLE IF NOT EXISTS `ps_layered_price_index` (
  `id_product` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `price_min` decimal(20,6) NOT NULL,
  `price_max` decimal(20,6) NOT NULL,
  `id_country` int(11) NOT NULL,
  PRIMARY KEY (`id_product`,`id_currency`,`id_shop`,`id_country`),
  KEY `id_currency` (`id_currency`),
  KEY `price_min` (`price_min`),
  KEY `price_max` (`price_max`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_layered_price_index`
--

INSERT INTO `ps_layered_price_index` (`id_product`, `id_currency`, `id_shop`, `price_min`, `price_max`, `id_country`) VALUES
(1, 1, 1, '23.135200', '28.919000', 3),
(2, 1, 1, '34.751200', '43.439000', 3),
(3, 1, 1, '35.090000', '35.090000', 3),
(4, 1, 1, '35.090000', '35.090000', 3),
(5, 1, 1, '35.090000', '35.090000', 3),
(6, 1, 1, '14.399000', '14.399000', 3),
(7, 1, 1, '14.399000', '14.399000', 3),
(8, 1, 1, '14.399000', '14.399000', 3),
(9, 1, 1, '22.869000', '22.869000', 3),
(10, 1, 1, '22.869000', '22.869000', 3),
(11, 1, 1, '22.869000', '22.869000', 3),
(12, 1, 1, '10.890000', '10.890000', 3),
(13, 1, 1, '10.890000', '10.890000', 3),
(14, 1, 1, '10.890000', '10.890000', 3),
(15, 1, 1, '42.350000', '42.350000', 3),
(16, 1, 1, '15.609000', '15.609000', 3),
(17, 1, 1, '15.609000', '15.609000', 3),
(18, 1, 1, '15.609000', '15.609000', 3),
(19, 1, 1, '16.819000', '16.819000', 3),
(20, 1, 1, '23.292500', '23.292500', 3),
(20, 1, 1, '23.292500', '23.292500', 6),
(20, 1, 1, '23.292500', '23.292500', 10),
(20, 2, 1, '19.798625', '19.798625', 3),
(20, 2, 1, '19.798625', '19.798625', 6),
(20, 2, 1, '19.798625', '19.798625', 10),
(20, 3, 1, '22.826650', '22.826650', 3),
(20, 3, 1, '22.826650', '22.826650', 6),
(20, 3, 1, '22.826650', '22.826650', 10),
(20, 4, 1, '173.063275', '173.063275', 3),
(20, 4, 1, '173.063275', '173.063275', 6),
(20, 4, 1, '173.063275', '173.063275', 10),
(20, 5, 1, '253.189475', '253.189475', 3),
(20, 5, 1, '253.189475', '253.189475', 6),
(20, 5, 1, '253.189475', '253.189475', 10),
(20, 6, 1, '567.638225', '567.638225', 3),
(20, 6, 1, '567.638225', '567.638225', 6),
(20, 6, 1, '567.638225', '567.638225', 10),
(20, 7, 1, '108.775975', '108.775975', 3),
(20, 7, 1, '108.775975', '108.775975', 6),
(20, 7, 1, '108.775975', '108.775975', 10),
(20, 8, 1, '9540.375075', '9540.375075', 3),
(20, 8, 1, '9540.375075', '9540.375075', 6),
(20, 8, 1, '9540.375075', '9540.375075', 10),
(20, 9, 1, '114.599100', '114.599100', 3),
(20, 9, 1, '114.599100', '114.599100', 6),
(20, 9, 1, '114.599100', '114.599100', 10),
(20, 10, 1, '175.858375', '175.858375', 3),
(20, 10, 1, '175.858375', '175.858375', 6),
(20, 10, 1, '175.858375', '175.858375', 10),
(20, 11, 1, '45.886225', '45.886225', 3),
(20, 11, 1, '45.886225', '45.886225', 6),
(20, 11, 1, '45.886225', '45.886225', 10),
(20, 12, 1, '239.446900', '239.446900', 3),
(20, 12, 1, '239.446900', '239.446900', 6),
(20, 12, 1, '239.446900', '239.446900', 10),
(21, 1, 1, '0.000000', '0.000000', 3),
(22, 1, 1, '0.000000', '0.000000', 3),
(23, 1, 1, '62.920000', '62.920000', 3),
(23, 1, 1, '62.920000', '62.920000', 6),
(23, 1, 1, '62.920000', '62.920000', 10),
(23, 2, 1, '53.482000', '53.482000', 3),
(23, 2, 1, '53.482000', '53.482000', 6),
(23, 2, 1, '53.482000', '53.482000', 10),
(23, 3, 1, '61.661600', '61.661600', 3),
(23, 3, 1, '61.661600', '61.661600', 6),
(23, 3, 1, '61.661600', '61.661600', 10),
(23, 4, 1, '467.495600', '467.495600', 3),
(23, 4, 1, '467.495600', '467.495600', 6),
(23, 4, 1, '467.495600', '467.495600', 10),
(23, 5, 1, '683.940400', '683.940400', 3),
(23, 5, 1, '683.940400', '683.940400', 6),
(23, 5, 1, '683.940400', '683.940400', 10),
(23, 6, 1, '1533.360400', '1533.360400', 3),
(23, 6, 1, '1533.360400', '1533.360400', 6),
(23, 6, 1, '1533.360400', '1533.360400', 10),
(23, 7, 1, '293.836400', '293.836400', 3),
(23, 7, 1, '293.836400', '293.836400', 6),
(23, 7, 1, '293.836400', '293.836400', 10),
(23, 8, 1, '25771.402800', '25771.402800', 3),
(23, 8, 1, '25771.402800', '25771.402800', 6),
(23, 8, 1, '25771.402800', '25771.402800', 10),
(23, 9, 1, '309.566400', '309.566400', 3),
(23, 9, 1, '309.566400', '309.566400', 6),
(23, 9, 1, '309.566400', '309.566400', 10),
(23, 10, 1, '475.046000', '475.046000', 3),
(23, 10, 1, '475.046000', '475.046000', 6),
(23, 10, 1, '475.046000', '475.046000', 10),
(23, 11, 1, '123.952400', '123.952400', 3),
(23, 11, 1, '123.952400', '123.952400', 6),
(23, 11, 1, '123.952400', '123.952400', 10),
(23, 12, 1, '646.817600', '646.817600', 3),
(23, 12, 1, '646.817600', '646.817600', 6),
(23, 12, 1, '646.817600', '646.817600', 10),
(24, 1, 1, '59.895000', '59.895000', 3),
(24, 1, 1, '59.895000', '59.895000', 6),
(24, 1, 1, '59.895000', '59.895000', 10),
(24, 2, 1, '50.910750', '50.910750', 3),
(24, 2, 1, '50.910750', '50.910750', 6),
(24, 2, 1, '50.910750', '50.910750', 10),
(24, 3, 1, '58.697100', '58.697100', 3),
(24, 3, 1, '58.697100', '58.697100', 6),
(24, 3, 1, '58.697100', '58.697100', 10),
(24, 4, 1, '445.019850', '445.019850', 3),
(24, 4, 1, '445.019850', '445.019850', 6),
(24, 4, 1, '445.019850', '445.019850', 10),
(24, 5, 1, '651.058650', '651.058650', 3),
(24, 5, 1, '651.058650', '651.058650', 6),
(24, 5, 1, '651.058650', '651.058650', 10),
(24, 6, 1, '1459.641150', '1459.641150', 3),
(24, 6, 1, '1459.641150', '1459.641150', 6),
(24, 6, 1, '1459.641150', '1459.641150', 10),
(24, 7, 1, '279.709650', '279.709650', 3),
(24, 7, 1, '279.709650', '279.709650', 6),
(24, 7, 1, '279.709650', '279.709650', 10),
(24, 8, 1, '24532.393050', '24532.393050', 3),
(24, 8, 1, '24532.393050', '24532.393050', 6),
(24, 8, 1, '24532.393050', '24532.393050', 10),
(24, 9, 1, '294.683400', '294.683400', 3),
(24, 9, 1, '294.683400', '294.683400', 6),
(24, 9, 1, '294.683400', '294.683400', 10),
(24, 10, 1, '452.207250', '452.207250', 3),
(24, 10, 1, '452.207250', '452.207250', 6),
(24, 10, 1, '452.207250', '452.207250', 10),
(24, 11, 1, '117.993150', '117.993150', 3),
(24, 11, 1, '117.993150', '117.993150', 6),
(24, 11, 1, '117.993150', '117.993150', 10),
(24, 12, 1, '615.720600', '615.720600', 3),
(24, 12, 1, '615.720600', '615.720600', 6),
(24, 12, 1, '615.720600', '615.720600', 10),
(25, 1, 1, '0.000000', '0.000000', 3),
(25, 1, 1, '0.000000', '0.000000', 6),
(25, 1, 1, '0.000000', '0.000000', 10),
(25, 2, 1, '0.000000', '0.000000', 3),
(25, 2, 1, '0.000000', '0.000000', 6),
(25, 2, 1, '0.000000', '0.000000', 10),
(25, 3, 1, '0.000000', '0.000000', 3),
(25, 3, 1, '0.000000', '0.000000', 6),
(25, 3, 1, '0.000000', '0.000000', 10),
(25, 4, 1, '0.000000', '0.000000', 3),
(25, 4, 1, '0.000000', '0.000000', 6),
(25, 4, 1, '0.000000', '0.000000', 10),
(25, 5, 1, '0.000000', '0.000000', 3),
(25, 5, 1, '0.000000', '0.000000', 6),
(25, 5, 1, '0.000000', '0.000000', 10),
(25, 6, 1, '0.000000', '0.000000', 3),
(25, 6, 1, '0.000000', '0.000000', 6),
(25, 6, 1, '0.000000', '0.000000', 10),
(25, 7, 1, '0.000000', '0.000000', 3),
(25, 7, 1, '0.000000', '0.000000', 6),
(25, 7, 1, '0.000000', '0.000000', 10),
(25, 8, 1, '0.000000', '0.000000', 3),
(25, 8, 1, '0.000000', '0.000000', 6),
(25, 8, 1, '0.000000', '0.000000', 10),
(25, 9, 1, '0.000000', '0.000000', 3),
(25, 9, 1, '0.000000', '0.000000', 6),
(25, 9, 1, '0.000000', '0.000000', 10),
(25, 10, 1, '0.000000', '0.000000', 3),
(25, 10, 1, '0.000000', '0.000000', 6),
(25, 10, 1, '0.000000', '0.000000', 10),
(25, 11, 1, '0.000000', '0.000000', 3),
(25, 11, 1, '0.000000', '0.000000', 6),
(25, 11, 1, '0.000000', '0.000000', 10),
(25, 12, 1, '0.000000', '0.000000', 3),
(25, 12, 1, '0.000000', '0.000000', 6),
(25, 12, 1, '0.000000', '0.000000', 10);

-- --------------------------------------------------------

--
-- Structure de la table `ps_layered_product_attribute`
--

DROP TABLE IF EXISTS `ps_layered_product_attribute`;
CREATE TABLE IF NOT EXISTS `ps_layered_product_attribute` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_attribute_group` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_attribute`,`id_product`,`id_shop`),
  UNIQUE KEY `id_attribute_group` (`id_attribute_group`,`id_attribute`,`id_product`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_layered_product_attribute`
--

INSERT INTO `ps_layered_product_attribute` (`id_attribute`, `id_product`, `id_attribute_group`, `id_shop`) VALUES
(1, 1, 1, 1),
(1, 2, 1, 1),
(2, 1, 1, 1),
(2, 2, 1, 1),
(3, 1, 1, 1),
(3, 2, 1, 1),
(4, 1, 1, 1),
(4, 2, 1, 1),
(8, 1, 2, 1),
(8, 9, 2, 1),
(8, 10, 2, 1),
(8, 11, 2, 1),
(11, 1, 2, 1),
(11, 9, 2, 1),
(11, 10, 2, 1),
(11, 11, 2, 1),
(19, 3, 3, 1),
(19, 4, 3, 1),
(19, 5, 3, 1),
(20, 3, 3, 1),
(20, 4, 3, 1),
(20, 5, 3, 1),
(21, 3, 3, 1),
(21, 4, 3, 1),
(21, 5, 3, 1),
(22, 16, 4, 1),
(22, 17, 4, 1),
(22, 18, 4, 1),
(23, 16, 4, 1),
(23, 17, 4, 1),
(23, 18, 4, 1),
(24, 16, 4, 1),
(24, 17, 4, 1),
(24, 18, 4, 1),
(25, 16, 4, 1),
(25, 17, 4, 1),
(25, 18, 4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_linksmenutop`
--

DROP TABLE IF EXISTS `ps_linksmenutop`;
CREATE TABLE IF NOT EXISTS `ps_linksmenutop` (
  `id_linksmenutop` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `new_window` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_linksmenutop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_linksmenutop_lang`
--

DROP TABLE IF EXISTS `ps_linksmenutop_lang`;
CREATE TABLE IF NOT EXISTS `ps_linksmenutop_lang` (
  `id_linksmenutop` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `label` varchar(128) NOT NULL,
  `link` varchar(128) NOT NULL,
  KEY `id_linksmenutop` (`id_linksmenutop`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_link_block`
--

DROP TABLE IF EXISTS `ps_link_block`;
CREATE TABLE IF NOT EXISTS `ps_link_block` (
  `id_link_block` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_hook` int(1) UNSIGNED DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `content` text,
  PRIMARY KEY (`id_link_block`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_link_block`
--

INSERT INTO `ps_link_block` (`id_link_block`, `id_hook`, `position`, `content`) VALUES
(1, 41, 0, '{\"cms\":[false],\"product\":[\"prices-drop\",\"new-products\",\"best-sales\"],\"static\":[false]}'),
(2, 41, 1, '{\"cms\":[\"1\",\"2\",\"3\",\"4\",\"5\"],\"product\":[false],\"static\":[\"contact\",\"sitemap\",\"stores\"]}');

-- --------------------------------------------------------

--
-- Structure de la table `ps_link_block_lang`
--

DROP TABLE IF EXISTS `ps_link_block_lang`;
CREATE TABLE IF NOT EXISTS `ps_link_block_lang` (
  `id_link_block` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT '',
  `custom_content` text,
  PRIMARY KEY (`id_link_block`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_link_block_lang`
--

INSERT INTO `ps_link_block_lang` (`id_link_block`, `id_lang`, `name`, `custom_content`) VALUES
(1, 1, 'Products', NULL),
(1, 2, 'Products', NULL),
(1, 3, 'Products', NULL),
(1, 4, 'Products', NULL),
(1, 5, 'Prodotti', NULL),
(1, 6, 'Productos', NULL),
(2, 1, 'Our company', NULL),
(2, 2, 'Our company', NULL),
(2, 3, 'Our company', NULL),
(2, 4, 'Our company', NULL),
(2, 5, 'La nostra azienda', NULL),
(2, 6, 'Nuestra empresa', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ps_link_block_shop`
--

DROP TABLE IF EXISTS `ps_link_block_shop`;
CREATE TABLE IF NOT EXISTS `ps_link_block_shop` (
  `id_link_block` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_link_block`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_link_block_shop`
--

INSERT INTO `ps_link_block_shop` (`id_link_block`, `id_shop`, `position`) VALUES
(1, 1, 0),
(2, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_log`
--

DROP TABLE IF EXISTS `ps_log`;
CREATE TABLE IF NOT EXISTS `ps_log` (
  `id_log` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `severity` tinyint(1) NOT NULL,
  `error_code` int(11) DEFAULT NULL,
  `message` text NOT NULL,
  `object_type` varchar(32) DEFAULT NULL,
  `object_id` int(10) UNSIGNED DEFAULT NULL,
  `id_shop` int(10) UNSIGNED DEFAULT NULL,
  `id_shop_group` int(10) UNSIGNED DEFAULT NULL,
  `id_lang` int(10) UNSIGNED DEFAULT NULL,
  `in_all_shops` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=868 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_log`
--

INSERT INTO `ps_log` (`id_log`, `severity`, `error_code`, `message`, `object_type`, `object_id`, `id_shop`, `id_shop_group`, `id_lang`, `in_all_shops`, `id_employee`, `date_add`, `date_upd`) VALUES
(1, 1, 0, 'Exporting mail with theme modern for language English (English)', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:17', '2022-11-21 11:03:17'),
(2, 1, 0, 'Core output folder: C:\\\\wamp64\\\\www\\\\prestashop/mails', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:17', '2022-11-21 11:03:17'),
(3, 1, 0, 'Modules output folder: C:\\\\wamp64\\\\www\\\\prestashop/modules/', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:17', '2022-11-21 11:03:17'),
(4, 1, 0, 'Generate html template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\account.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:19', '2022-11-21 11:03:19'),
(5, 1, 0, 'Generate txt template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\account.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:19', '2022-11-21 11:03:19'),
(6, 1, 0, 'Generate html template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\backoffice_order.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:19', '2022-11-21 11:03:19'),
(7, 1, 0, 'Generate txt template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\backoffice_order.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:19', '2022-11-21 11:03:19'),
(8, 1, 0, 'Generate html template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\bankwire.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:19', '2022-11-21 11:03:19'),
(9, 1, 0, 'Generate txt template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\bankwire.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:19', '2022-11-21 11:03:19'),
(10, 1, 0, 'Generate html template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\cheque.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:19', '2022-11-21 11:03:19'),
(11, 1, 0, 'Generate txt template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\cheque.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:19', '2022-11-21 11:03:19'),
(12, 1, 0, 'Generate html template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\contact.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:19', '2022-11-21 11:03:19'),
(13, 1, 0, 'Generate txt template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\contact.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:19', '2022-11-21 11:03:19'),
(14, 1, 0, 'Generate html template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\contact_form.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(15, 1, 0, 'Generate txt template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\contact_form.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(16, 1, 0, 'Generate html template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\credit_slip.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(17, 1, 0, 'Generate txt template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\credit_slip.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(18, 1, 0, 'Generate html template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\download_product.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(19, 1, 0, 'Generate txt template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\download_product.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(20, 1, 0, 'Generate html template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\employee_password.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(21, 1, 0, 'Generate txt template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\employee_password.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(22, 1, 0, 'Generate html template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\forward_msg.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(23, 1, 0, 'Generate txt template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\forward_msg.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(24, 1, 0, 'Generate html template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\guest_to_customer.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(25, 1, 0, 'Generate txt template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\guest_to_customer.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(26, 1, 0, 'Generate html template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\import.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(27, 1, 0, 'Generate txt template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\import.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(28, 1, 0, 'Generate html template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\in_transit.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(29, 1, 0, 'Generate txt template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\in_transit.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(30, 1, 0, 'Generate html template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\log_alert.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(31, 1, 0, 'Generate txt template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\log_alert.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(32, 1, 0, 'Generate html template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\newsletter.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(33, 1, 0, 'Generate txt template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\newsletter.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(34, 1, 0, 'Generate html template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_canceled.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(35, 1, 0, 'Generate txt template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_canceled.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(36, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_changed.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(37, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_changed.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(38, 1, 0, 'Generate html template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_conf.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(39, 1, 0, 'Generate txt template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_conf.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(40, 1, 0, 'Generate html template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_customer_comment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(41, 1, 0, 'Generate txt template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_customer_comment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(42, 1, 0, 'Generate html template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_merchant_comment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:20', '2022-11-21 11:03:20'),
(43, 1, 0, 'Generate txt template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_merchant_comment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(44, 1, 0, 'Generate html template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_return_state.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(45, 1, 0, 'Generate txt template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\order_return_state.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(46, 1, 0, 'Generate html template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\outofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(47, 1, 0, 'Generate txt template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\outofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(48, 1, 0, 'Generate html template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\password.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(49, 1, 0, 'Generate txt template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\password.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(50, 1, 0, 'Generate html template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\password_query.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(51, 1, 0, 'Generate txt template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\password_query.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(52, 1, 0, 'Generate html template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\payment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(53, 1, 0, 'Generate txt template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\payment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(54, 1, 0, 'Generate html template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\payment_error.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(55, 1, 0, 'Generate txt template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\payment_error.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(56, 1, 0, 'Generate html template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\preparation.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(57, 1, 0, 'Generate txt template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\preparation.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(58, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\productoutofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(59, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\productoutofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(60, 1, 0, 'Generate html template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\refund.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(61, 1, 0, 'Generate txt template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\refund.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(62, 1, 0, 'Generate html template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\reply_msg.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(63, 1, 0, 'Generate txt template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\reply_msg.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(64, 1, 0, 'Generate html template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\shipped.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(65, 1, 0, 'Generate txt template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\shipped.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(66, 1, 0, 'Generate html template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\test.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(67, 1, 0, 'Generate txt template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\test.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(68, 1, 0, 'Generate html template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(69, 1, 0, 'Generate txt template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(70, 1, 0, 'Generate html template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\voucher_new.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(71, 1, 0, 'Generate txt template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\en\\\\voucher_new.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(72, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\en\\\\followup_1.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(73, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\en\\\\followup_1.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:21', '2022-11-21 11:03:21'),
(74, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\en\\\\followup_2.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(75, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\en\\\\followup_2.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(76, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\en\\\\followup_3.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(77, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\en\\\\followup_3.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(78, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\en\\\\followup_4.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(79, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\en\\\\followup_4.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(80, 1, 0, 'Generate html template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\customer_qty.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(81, 1, 0, 'Generate txt template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\customer_qty.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(82, 1, 0, 'Generate html template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\new_order.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(83, 1, 0, 'Generate txt template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\new_order.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(84, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\order_changed.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(85, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\order_changed.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(86, 1, 0, 'Generate html template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\productcoverage.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(87, 1, 0, 'Generate txt template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\productcoverage.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(88, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\productoutofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(89, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\productoutofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(90, 1, 0, 'Generate html template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\return_slip.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(91, 1, 0, 'Generate txt template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\en\\\\return_slip.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(92, 1, 0, 'Generate html template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\en\\\\newsletter_conf.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(93, 1, 0, 'Generate txt template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\en\\\\newsletter_conf.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(94, 1, 0, 'Generate html template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\en\\\\newsletter_verif.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(95, 1, 0, 'Generate txt template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\en\\\\newsletter_verif.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(96, 1, 0, 'Generate html template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\en\\\\newsletter_voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(97, 1, 0, 'Generate txt template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\en\\\\newsletter_voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(98, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\en\\\\followup_1.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(99, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\en\\\\followup_1.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(100, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\en\\\\followup_2.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(101, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\en\\\\followup_2.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(102, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\en\\\\followup_3.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(103, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\en\\\\followup_3.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:22', '2022-11-21 11:03:22'),
(104, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\en\\\\followup_4.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:23', '2022-11-21 11:03:23'),
(105, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\en\\\\followup_4.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:23', '2022-11-21 11:03:23'),
(106, 1, 0, 'Generate html template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\en\\\\referralprogram-congratulations.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:23', '2022-11-21 11:03:23'),
(107, 1, 0, 'Generate txt template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\en\\\\referralprogram-congratulations.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:23', '2022-11-21 11:03:23'),
(108, 1, 0, 'Generate html template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\en\\\\referralprogram-invitation.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:23', '2022-11-21 11:03:23'),
(109, 1, 0, 'Generate txt template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\en\\\\referralprogram-invitation.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:23', '2022-11-21 11:03:23'),
(110, 1, 0, 'Generate html template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\en\\\\referralprogram-voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:23', '2022-11-21 11:03:23'),
(111, 1, 0, 'Generate txt template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\en\\\\referralprogram-voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:23', '2022-11-21 11:03:23'),
(112, 1, 0, 'Exporting mail with theme modern for language Français (French)', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:24', '2022-11-21 11:03:24'),
(113, 1, 0, 'Core output folder: C:\\\\wamp64\\\\www\\\\prestashop/mails', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:24', '2022-11-21 11:03:24'),
(114, 1, 0, 'Modules output folder: C:\\\\wamp64\\\\www\\\\prestashop/modules/', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:24', '2022-11-21 11:03:24'),
(115, 1, 0, 'Generate html template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\account.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(116, 1, 0, 'Generate txt template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\account.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(117, 1, 0, 'Generate html template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\backoffice_order.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(118, 1, 0, 'Generate txt template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\backoffice_order.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(119, 1, 0, 'Generate html template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\bankwire.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(120, 1, 0, 'Generate txt template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\bankwire.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(121, 1, 0, 'Generate html template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\cheque.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(122, 1, 0, 'Generate txt template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\cheque.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(123, 1, 0, 'Generate html template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\contact.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(124, 1, 0, 'Generate txt template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\contact.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(125, 1, 0, 'Generate html template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\contact_form.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(126, 1, 0, 'Generate txt template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\contact_form.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(127, 1, 0, 'Generate html template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\credit_slip.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(128, 1, 0, 'Generate txt template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\credit_slip.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(129, 1, 0, 'Generate html template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\download_product.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(130, 1, 0, 'Generate txt template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\download_product.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:25', '2022-11-21 11:03:25'),
(131, 1, 0, 'Generate html template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\employee_password.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(132, 1, 0, 'Generate txt template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\employee_password.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(133, 1, 0, 'Generate html template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\forward_msg.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(134, 1, 0, 'Generate txt template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\forward_msg.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(135, 1, 0, 'Generate html template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\guest_to_customer.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(136, 1, 0, 'Generate txt template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\guest_to_customer.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(137, 1, 0, 'Generate html template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\import.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(138, 1, 0, 'Generate txt template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\import.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(139, 1, 0, 'Generate html template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\in_transit.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(140, 1, 0, 'Generate txt template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\in_transit.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(141, 1, 0, 'Generate html template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\log_alert.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(142, 1, 0, 'Generate txt template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\log_alert.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(143, 1, 0, 'Generate html template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\newsletter.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(144, 1, 0, 'Generate txt template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\newsletter.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(145, 1, 0, 'Generate html template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_canceled.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(146, 1, 0, 'Generate txt template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_canceled.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(147, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_changed.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(148, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_changed.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(149, 1, 0, 'Generate html template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_conf.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(150, 1, 0, 'Generate txt template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_conf.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(151, 1, 0, 'Generate html template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_customer_comment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(152, 1, 0, 'Generate txt template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_customer_comment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(153, 1, 0, 'Generate html template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_merchant_comment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(154, 1, 0, 'Generate txt template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_merchant_comment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(155, 1, 0, 'Generate html template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_return_state.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(156, 1, 0, 'Generate txt template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\order_return_state.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(157, 1, 0, 'Generate html template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\outofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(158, 1, 0, 'Generate txt template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\outofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(159, 1, 0, 'Generate html template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\password.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(160, 1, 0, 'Generate txt template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\password.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(161, 1, 0, 'Generate html template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\password_query.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(162, 1, 0, 'Generate txt template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\password_query.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(163, 1, 0, 'Generate html template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\payment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(164, 1, 0, 'Generate txt template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\payment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(165, 1, 0, 'Generate html template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\payment_error.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(166, 1, 0, 'Generate txt template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\payment_error.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(167, 1, 0, 'Generate html template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\preparation.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(168, 1, 0, 'Generate txt template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\preparation.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(169, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\productoutofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(170, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\productoutofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(171, 1, 0, 'Generate html template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\refund.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(172, 1, 0, 'Generate txt template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\refund.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(173, 1, 0, 'Generate html template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\reply_msg.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(174, 1, 0, 'Generate txt template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\reply_msg.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(175, 1, 0, 'Generate html template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\shipped.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(176, 1, 0, 'Generate txt template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\shipped.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(177, 1, 0, 'Generate html template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\test.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(178, 1, 0, 'Generate txt template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\test.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(179, 1, 0, 'Generate html template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(180, 1, 0, 'Generate txt template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(181, 1, 0, 'Generate html template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\voucher_new.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(182, 1, 0, 'Generate txt template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\fr\\\\voucher_new.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:26', '2022-11-21 11:03:26'),
(183, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\fr\\\\followup_1.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(184, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\fr\\\\followup_1.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(185, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\fr\\\\followup_2.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(186, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\fr\\\\followup_2.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(187, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\fr\\\\followup_3.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(188, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\fr\\\\followup_3.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(189, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\fr\\\\followup_4.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(190, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\fr\\\\followup_4.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(191, 1, 0, 'Generate html template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\customer_qty.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(192, 1, 0, 'Generate txt template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\customer_qty.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(193, 1, 0, 'Generate html template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\new_order.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(194, 1, 0, 'Generate txt template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\new_order.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(195, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\order_changed.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(196, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\order_changed.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(197, 1, 0, 'Generate html template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\productcoverage.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(198, 1, 0, 'Generate txt template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\productcoverage.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(199, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\productoutofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(200, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\productoutofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(201, 1, 0, 'Generate html template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\return_slip.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(202, 1, 0, 'Generate txt template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\fr\\\\return_slip.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(203, 1, 0, 'Generate html template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\fr\\\\newsletter_conf.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(204, 1, 0, 'Generate txt template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\fr\\\\newsletter_conf.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(205, 1, 0, 'Generate html template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\fr\\\\newsletter_verif.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(206, 1, 0, 'Generate txt template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\fr\\\\newsletter_verif.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(207, 1, 0, 'Generate html template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\fr\\\\newsletter_voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(208, 1, 0, 'Generate txt template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\fr\\\\newsletter_voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(209, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\fr\\\\followup_1.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(210, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\fr\\\\followup_1.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(211, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\fr\\\\followup_2.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(212, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\fr\\\\followup_2.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(213, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\fr\\\\followup_3.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(214, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\fr\\\\followup_3.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(215, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\fr\\\\followup_4.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(216, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\fr\\\\followup_4.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(217, 1, 0, 'Generate html template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\fr\\\\referralprogram-congratulations.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(218, 1, 0, 'Generate txt template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\fr\\\\referralprogram-congratulations.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(219, 1, 0, 'Generate html template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\fr\\\\referralprogram-invitation.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(220, 1, 0, 'Generate txt template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\fr\\\\referralprogram-invitation.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(221, 1, 0, 'Generate html template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\fr\\\\referralprogram-voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(222, 1, 0, 'Generate txt template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\fr\\\\referralprogram-voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:27', '2022-11-21 11:03:27'),
(223, 1, 0, 'Exporting mail with theme modern for language Deutsch (German)', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:28', '2022-11-21 11:03:28'),
(224, 1, 0, 'Core output folder: C:\\\\wamp64\\\\www\\\\prestashop/mails', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:28', '2022-11-21 11:03:28'),
(225, 1, 0, 'Modules output folder: C:\\\\wamp64\\\\www\\\\prestashop/modules/', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:28', '2022-11-21 11:03:28'),
(226, 1, 0, 'Generate html template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\account.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:28', '2022-11-21 11:03:28'),
(227, 1, 0, 'Generate txt template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\account.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:28', '2022-11-21 11:03:28'),
(228, 1, 0, 'Generate html template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\backoffice_order.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:28', '2022-11-21 11:03:28'),
(229, 1, 0, 'Generate txt template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\backoffice_order.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:28', '2022-11-21 11:03:28'),
(230, 1, 0, 'Generate html template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\bankwire.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(231, 1, 0, 'Generate txt template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\bankwire.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(232, 1, 0, 'Generate html template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\cheque.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(233, 1, 0, 'Generate txt template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\cheque.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(234, 1, 0, 'Generate html template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\contact.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(235, 1, 0, 'Generate txt template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\contact.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(236, 1, 0, 'Generate html template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\contact_form.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(237, 1, 0, 'Generate txt template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\contact_form.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(238, 1, 0, 'Generate html template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\credit_slip.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(239, 1, 0, 'Generate txt template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\credit_slip.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(240, 1, 0, 'Generate html template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\download_product.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(241, 1, 0, 'Generate txt template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\download_product.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(242, 1, 0, 'Generate html template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\employee_password.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(243, 1, 0, 'Generate txt template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\employee_password.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(244, 1, 0, 'Generate html template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\forward_msg.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(245, 1, 0, 'Generate txt template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\forward_msg.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(246, 1, 0, 'Generate html template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\guest_to_customer.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(247, 1, 0, 'Generate txt template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\guest_to_customer.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29');
INSERT INTO `ps_log` (`id_log`, `severity`, `error_code`, `message`, `object_type`, `object_id`, `id_shop`, `id_shop_group`, `id_lang`, `in_all_shops`, `id_employee`, `date_add`, `date_upd`) VALUES
(248, 1, 0, 'Generate html template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\import.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(249, 1, 0, 'Generate txt template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\import.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(250, 1, 0, 'Generate html template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\in_transit.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(251, 1, 0, 'Generate txt template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\in_transit.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(252, 1, 0, 'Generate html template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\log_alert.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(253, 1, 0, 'Generate txt template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\log_alert.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(254, 1, 0, 'Generate html template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\newsletter.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(255, 1, 0, 'Generate txt template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\newsletter.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(256, 1, 0, 'Generate html template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_canceled.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(257, 1, 0, 'Generate txt template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_canceled.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(258, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_changed.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(259, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_changed.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(260, 1, 0, 'Generate html template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_conf.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(261, 1, 0, 'Generate txt template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_conf.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(262, 1, 0, 'Generate html template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_customer_comment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(263, 1, 0, 'Generate txt template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_customer_comment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(264, 1, 0, 'Generate html template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_merchant_comment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(265, 1, 0, 'Generate txt template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_merchant_comment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(266, 1, 0, 'Generate html template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_return_state.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(267, 1, 0, 'Generate txt template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\order_return_state.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(268, 1, 0, 'Generate html template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\outofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(269, 1, 0, 'Generate txt template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\outofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(270, 1, 0, 'Generate html template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\password.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(271, 1, 0, 'Generate txt template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\password.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(272, 1, 0, 'Generate html template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\password_query.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(273, 1, 0, 'Generate txt template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\password_query.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(274, 1, 0, 'Generate html template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\payment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(275, 1, 0, 'Generate txt template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\payment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(276, 1, 0, 'Generate html template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\payment_error.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(277, 1, 0, 'Generate txt template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\payment_error.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(278, 1, 0, 'Generate html template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\preparation.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(279, 1, 0, 'Generate txt template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\preparation.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:29', '2022-11-21 11:03:29'),
(280, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\productoutofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(281, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\productoutofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(282, 1, 0, 'Generate html template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\refund.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(283, 1, 0, 'Generate txt template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\refund.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(284, 1, 0, 'Generate html template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\reply_msg.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(285, 1, 0, 'Generate txt template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\reply_msg.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(286, 1, 0, 'Generate html template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\shipped.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(287, 1, 0, 'Generate txt template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\shipped.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(288, 1, 0, 'Generate html template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\test.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(289, 1, 0, 'Generate txt template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\test.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(290, 1, 0, 'Generate html template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(291, 1, 0, 'Generate txt template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(292, 1, 0, 'Generate html template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\voucher_new.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(293, 1, 0, 'Generate txt template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\de\\\\voucher_new.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(294, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\de\\\\followup_1.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(295, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\de\\\\followup_1.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(296, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\de\\\\followup_2.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(297, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\de\\\\followup_2.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(298, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\de\\\\followup_3.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(299, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\de\\\\followup_3.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(300, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\de\\\\followup_4.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(301, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\de\\\\followup_4.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(302, 1, 0, 'Generate html template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\customer_qty.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(303, 1, 0, 'Generate txt template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\customer_qty.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(304, 1, 0, 'Generate html template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\new_order.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(305, 1, 0, 'Generate txt template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\new_order.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(306, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\order_changed.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(307, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\order_changed.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(308, 1, 0, 'Generate html template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\productcoverage.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(309, 1, 0, 'Generate txt template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\productcoverage.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(310, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\productoutofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(311, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\productoutofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(312, 1, 0, 'Generate html template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\return_slip.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(313, 1, 0, 'Generate txt template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\de\\\\return_slip.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(314, 1, 0, 'Generate html template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\de\\\\newsletter_conf.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(315, 1, 0, 'Generate txt template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\de\\\\newsletter_conf.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(316, 1, 0, 'Generate html template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\de\\\\newsletter_verif.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(317, 1, 0, 'Generate txt template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\de\\\\newsletter_verif.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(318, 1, 0, 'Generate html template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\de\\\\newsletter_voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(319, 1, 0, 'Generate txt template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\de\\\\newsletter_voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(320, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\de\\\\followup_1.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(321, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\de\\\\followup_1.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(322, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\de\\\\followup_2.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(323, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\de\\\\followup_2.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(324, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\de\\\\followup_3.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(325, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\de\\\\followup_3.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(326, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\de\\\\followup_4.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(327, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\de\\\\followup_4.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(328, 1, 0, 'Generate html template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\de\\\\referralprogram-congratulations.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(329, 1, 0, 'Generate txt template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\de\\\\referralprogram-congratulations.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(330, 1, 0, 'Generate html template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\de\\\\referralprogram-invitation.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:30', '2022-11-21 11:03:30'),
(331, 1, 0, 'Generate txt template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\de\\\\referralprogram-invitation.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:31', '2022-11-21 11:03:31'),
(332, 1, 0, 'Generate html template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\de\\\\referralprogram-voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:31', '2022-11-21 11:03:31'),
(333, 1, 0, 'Generate txt template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\de\\\\referralprogram-voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:31', '2022-11-21 11:03:31'),
(334, 1, 0, 'Exporting mail with theme modern for language Nederlands (Dutch)', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(335, 1, 0, 'Core output folder: C:\\\\wamp64\\\\www\\\\prestashop/mails', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(336, 1, 0, 'Modules output folder: C:\\\\wamp64\\\\www\\\\prestashop/modules/', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(337, 1, 0, 'Generate html template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\account.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(338, 1, 0, 'Generate txt template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\account.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(339, 1, 0, 'Generate html template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\backoffice_order.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(340, 1, 0, 'Generate txt template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\backoffice_order.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(341, 1, 0, 'Generate html template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\bankwire.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(342, 1, 0, 'Generate txt template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\bankwire.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(343, 1, 0, 'Generate html template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\cheque.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(344, 1, 0, 'Generate txt template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\cheque.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(345, 1, 0, 'Generate html template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\contact.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(346, 1, 0, 'Generate txt template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\contact.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(347, 1, 0, 'Generate html template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\contact_form.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(348, 1, 0, 'Generate txt template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\contact_form.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:32', '2022-11-21 11:03:32'),
(349, 1, 0, 'Generate html template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\credit_slip.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(350, 1, 0, 'Generate txt template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\credit_slip.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(351, 1, 0, 'Generate html template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\download_product.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(352, 1, 0, 'Generate txt template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\download_product.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(353, 1, 0, 'Generate html template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\employee_password.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(354, 1, 0, 'Generate txt template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\employee_password.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(355, 1, 0, 'Generate html template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\forward_msg.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(356, 1, 0, 'Generate txt template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\forward_msg.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(357, 1, 0, 'Generate html template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\guest_to_customer.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(358, 1, 0, 'Generate txt template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\guest_to_customer.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(359, 1, 0, 'Generate html template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\import.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(360, 1, 0, 'Generate txt template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\import.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(361, 1, 0, 'Generate html template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\in_transit.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(362, 1, 0, 'Generate txt template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\in_transit.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(363, 1, 0, 'Generate html template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\log_alert.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(364, 1, 0, 'Generate txt template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\log_alert.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(365, 1, 0, 'Generate html template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\newsletter.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(366, 1, 0, 'Generate txt template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\newsletter.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(367, 1, 0, 'Generate html template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_canceled.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(368, 1, 0, 'Generate txt template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_canceled.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(369, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_changed.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(370, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_changed.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(371, 1, 0, 'Generate html template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_conf.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(372, 1, 0, 'Generate txt template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_conf.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(373, 1, 0, 'Generate html template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_customer_comment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(374, 1, 0, 'Generate txt template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_customer_comment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(375, 1, 0, 'Generate html template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_merchant_comment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(376, 1, 0, 'Generate txt template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_merchant_comment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(377, 1, 0, 'Generate html template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_return_state.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(378, 1, 0, 'Generate txt template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\order_return_state.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(379, 1, 0, 'Generate html template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\outofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(380, 1, 0, 'Generate txt template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\outofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(381, 1, 0, 'Generate html template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\password.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(382, 1, 0, 'Generate txt template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\password.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(383, 1, 0, 'Generate html template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\password_query.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(384, 1, 0, 'Generate txt template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\password_query.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(385, 1, 0, 'Generate html template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\payment.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(386, 1, 0, 'Generate txt template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\payment.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(387, 1, 0, 'Generate html template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\payment_error.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(388, 1, 0, 'Generate txt template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\payment_error.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(389, 1, 0, 'Generate html template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\preparation.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(390, 1, 0, 'Generate txt template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\preparation.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(391, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\productoutofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(392, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\productoutofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(393, 1, 0, 'Generate html template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\refund.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(394, 1, 0, 'Generate txt template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\refund.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(395, 1, 0, 'Generate html template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\reply_msg.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(396, 1, 0, 'Generate txt template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\reply_msg.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(397, 1, 0, 'Generate html template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\shipped.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(398, 1, 0, 'Generate txt template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\shipped.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:33', '2022-11-21 11:03:33'),
(399, 1, 0, 'Generate html template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\test.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(400, 1, 0, 'Generate txt template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\test.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(401, 1, 0, 'Generate html template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(402, 1, 0, 'Generate txt template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(403, 1, 0, 'Generate html template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\voucher_new.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(404, 1, 0, 'Generate txt template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\nl\\\\voucher_new.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(405, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\nl\\\\followup_1.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(406, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\nl\\\\followup_1.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(407, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\nl\\\\followup_2.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(408, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\nl\\\\followup_2.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(409, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\nl\\\\followup_3.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(410, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\nl\\\\followup_3.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(411, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\nl\\\\followup_4.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(412, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\nl\\\\followup_4.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(413, 1, 0, 'Generate html template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\customer_qty.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(414, 1, 0, 'Generate txt template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\customer_qty.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(415, 1, 0, 'Generate html template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\new_order.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(416, 1, 0, 'Generate txt template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\new_order.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(417, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\order_changed.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(418, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\order_changed.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(419, 1, 0, 'Generate html template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\productcoverage.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(420, 1, 0, 'Generate txt template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\productcoverage.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(421, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\productoutofstock.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(422, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\productoutofstock.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(423, 1, 0, 'Generate html template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\return_slip.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(424, 1, 0, 'Generate txt template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\nl\\\\return_slip.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(425, 1, 0, 'Generate html template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\nl\\\\newsletter_conf.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(426, 1, 0, 'Generate txt template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\nl\\\\newsletter_conf.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(427, 1, 0, 'Generate html template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\nl\\\\newsletter_verif.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(428, 1, 0, 'Generate txt template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\nl\\\\newsletter_verif.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(429, 1, 0, 'Generate html template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\nl\\\\newsletter_voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(430, 1, 0, 'Generate txt template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\nl\\\\newsletter_voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(431, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\nl\\\\followup_1.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(432, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\nl\\\\followup_1.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(433, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\nl\\\\followup_2.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(434, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\nl\\\\followup_2.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(435, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\nl\\\\followup_3.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(436, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\nl\\\\followup_3.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(437, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\nl\\\\followup_4.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(438, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\nl\\\\followup_4.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(439, 1, 0, 'Generate html template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\nl\\\\referralprogram-congratulations.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(440, 1, 0, 'Generate txt template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\nl\\\\referralprogram-congratulations.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(441, 1, 0, 'Generate html template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\nl\\\\referralprogram-invitation.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(442, 1, 0, 'Generate txt template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\nl\\\\referralprogram-invitation.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(443, 1, 0, 'Generate html template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\nl\\\\referralprogram-voucher.html', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(444, 1, 0, 'Generate txt template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\nl\\\\referralprogram-voucher.txt', '', 0, NULL, NULL, 0, 0, 0, '2022-11-21 11:03:34', '2022-11-21 11:03:34'),
(445, 1, 0, 'Protect vendor folder in module blockwishlist', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:44', '2022-11-21 11:03:44'),
(446, 1, 0, 'Module blockwishlist has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:44', '2022-11-21 11:03:44'),
(447, 1, 0, 'Protect vendor folder in module contactform', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:44', '2022-11-21 11:03:44'),
(448, 1, 0, 'Module contactform has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:44', '2022-11-21 11:03:44'),
(449, 1, 0, 'Protect vendor folder in module dashactivity', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(450, 1, 0, 'Module dashactivity has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(451, 1, 0, 'Protect vendor folder in module dashtrends', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(452, 1, 0, 'Module dashtrends has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(453, 1, 0, 'Protect vendor folder in module dashgoals', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(454, 1, 0, 'Module dashgoals has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(455, 1, 0, 'Protect vendor folder in module dashproducts', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(456, 1, 0, 'Module dashproducts has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(457, 1, 0, 'Protect vendor folder in module graphnvd3', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(458, 1, 0, 'Module graphnvd3 has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(459, 1, 0, 'Protect vendor folder in module gridhtml', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(460, 1, 0, 'Module gridhtml has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(461, 1, 0, 'Protect vendor folder in module gsitemap', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(462, 1, 0, 'Module gsitemap has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(463, 1, 0, 'Protect vendor folder in module pagesnotfound', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(464, 1, 0, 'Module pagesnotfound has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:45', '2022-11-21 11:03:45'),
(465, 1, 0, 'Protect vendor folder in module productcomments', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(466, 1, 0, 'Module productcomments has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(467, 1, 0, 'Protect vendor folder in module ps_banner', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(468, 1, 0, 'Module ps_banner has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(469, 1, 0, 'Protect vendor folder in module ps_categorytree', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(470, 1, 0, 'Module ps_categorytree has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(471, 1, 0, 'Protect vendor folder in module ps_checkpayment', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(472, 1, 0, 'Module ps_checkpayment has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(473, 1, 0, 'Protect vendor folder in module ps_contactinfo', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(474, 1, 0, 'Module ps_contactinfo has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(475, 1, 0, 'Protect vendor folder in module ps_crossselling', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(476, 1, 0, 'Module ps_crossselling has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(477, 1, 0, 'Protect vendor folder in module ps_currencyselector', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(478, 1, 0, 'Module ps_currencyselector has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(479, 1, 0, 'Protect vendor folder in module ps_customeraccountlinks', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(480, 1, 0, 'Module ps_customeraccountlinks has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(481, 1, 0, 'Protect vendor folder in module ps_customersignin', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(482, 1, 0, 'Module ps_customersignin has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(483, 1, 0, 'Protect vendor folder in module ps_customtext', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(484, 1, 0, 'Module ps_customtext has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(485, 1, 0, 'Protect vendor folder in module ps_dataprivacy', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(486, 1, 0, 'Module ps_dataprivacy has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(487, 1, 0, 'Protect vendor folder in module ps_emailsubscription', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(488, 1, 0, 'Module ps_emailsubscription has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:46', '2022-11-21 11:03:46'),
(489, 1, 0, 'Protect vendor folder in module ps_facetedsearch', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(490, 1, 0, 'Module ps_facetedsearch has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(491, 1, 0, 'Protect vendor folder in module ps_faviconnotificationbo', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(492, 1, 0, 'Module ps_faviconnotificationbo has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(493, 1, 0, 'Protect vendor folder in module ps_featuredproducts', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(494, 1, 0, 'Module ps_featuredproducts has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(495, 1, 0, 'Protect vendor folder in module ps_imageslider', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(496, 1, 0, 'Module ps_imageslider has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(497, 1, 0, 'Protect vendor folder in module ps_languageselector', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(498, 1, 0, 'Module ps_languageselector has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:47', '2022-11-21 11:03:47'),
(499, 1, 0, 'Protect vendor folder in module ps_linklist', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(500, 1, 0, 'Module ps_linklist has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(501, 1, 0, 'Protect vendor folder in module ps_mainmenu', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(502, 1, 0, 'Module ps_mainmenu has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(503, 1, 0, 'Protect vendor folder in module ps_searchbar', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(504, 1, 0, 'Module ps_searchbar has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(505, 1, 0, 'Protect vendor folder in module ps_sharebuttons', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(506, 1, 0, 'Module ps_sharebuttons has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(507, 1, 0, 'Protect vendor folder in module ps_shoppingcart', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(508, 1, 0, 'Module ps_shoppingcart has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(509, 1, 0, 'Protect vendor folder in module ps_socialfollow', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(510, 1, 0, 'Module ps_socialfollow has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(511, 1, 0, 'Protect vendor folder in module ps_themecusto', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(512, 1, 0, 'Module ps_themecusto has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(513, 1, 0, 'Protect vendor folder in module ps_wirepayment', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(514, 1, 0, 'Module ps_wirepayment has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(515, 1, 0, 'Protect vendor folder in module statsbestcategories', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48');
INSERT INTO `ps_log` (`id_log`, `severity`, `error_code`, `message`, `object_type`, `object_id`, `id_shop`, `id_shop_group`, `id_lang`, `in_all_shops`, `id_employee`, `date_add`, `date_upd`) VALUES
(516, 1, 0, 'Module statsbestcategories has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(517, 1, 0, 'Protect vendor folder in module statsbestcustomers', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(518, 1, 0, 'Module statsbestcustomers has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(519, 1, 0, 'Protect vendor folder in module statsbestproducts', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(520, 1, 0, 'Module statsbestproducts has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:48', '2022-11-21 11:03:48'),
(521, 1, 0, 'Protect vendor folder in module statsbestsuppliers', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(522, 1, 0, 'Module statsbestsuppliers has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(523, 1, 0, 'Protect vendor folder in module statsbestvouchers', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(524, 1, 0, 'Module statsbestvouchers has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(525, 1, 0, 'Protect vendor folder in module statscarrier', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(526, 1, 0, 'Module statscarrier has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(527, 1, 0, 'Protect vendor folder in module statscatalog', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(528, 1, 0, 'Module statscatalog has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(529, 1, 0, 'Protect vendor folder in module statscheckup', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(530, 1, 0, 'Module statscheckup has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(531, 1, 0, 'Protect vendor folder in module statsdata', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(532, 1, 0, 'Module statsdata has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(533, 1, 0, 'Protect vendor folder in module statsforecast', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(534, 1, 0, 'Module statsforecast has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(535, 1, 0, 'Protect vendor folder in module statsnewsletter', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(536, 1, 0, 'Module statsnewsletter has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(537, 1, 0, 'Protect vendor folder in module statspersonalinfos', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(538, 1, 0, 'Module statspersonalinfos has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(539, 1, 0, 'Protect vendor folder in module statsproduct', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(540, 1, 0, 'Module statsproduct has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(541, 1, 0, 'Protect vendor folder in module statsregistrations', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(542, 1, 0, 'Module statsregistrations has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(543, 1, 0, 'Protect vendor folder in module statssales', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(544, 1, 0, 'Module statssales has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(545, 1, 0, 'Protect vendor folder in module statssearch', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(546, 1, 0, 'Module statssearch has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(547, 1, 0, 'Protect vendor folder in module statsstock', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(548, 1, 0, 'Module statsstock has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(549, 1, 0, 'Protect vendor folder in module welcome', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(550, 1, 0, 'Module welcome has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:03:49', '2022-11-21 11:03:49'),
(551, 1, 0, 'Protect vendor folder in module gamification', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:05:58', '2022-11-21 11:05:58'),
(552, 1, 0, 'Protect vendor folder in module psaddonsconnect', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:05:58', '2022-11-21 11:05:58'),
(553, 1, 0, 'Protect vendor folder in module psgdpr', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(554, 1, 0, 'Protect vendor folder in module ps_mbo', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(555, 1, 0, 'Protect vendor folder in module ps_buybuttonlite', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:05:59', '2022-11-21 11:05:59'),
(556, 1, 0, 'Protect vendor folder in module ps_checkout', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(557, 1, 0, 'Protect vendor folder in module ps_metrics', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(558, 1, 0, 'Protect vendor folder in module ps_facebook', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:06:00', '2022-11-21 11:06:00'),
(559, 1, 0, 'Protect vendor folder in module psxmarketingwithgoogle', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:06:01', '2022-11-21 11:06:01'),
(560, 1, 0, 'Protect vendor folder in module blockreassurance', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:06:43', '2022-11-21 11:06:43'),
(561, 1, 0, 'Module blockreassurance has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:06:43', '2022-11-21 11:06:43'),
(562, 1, 0, 'Protect vendor folder in module ps_facetedsearch', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(563, 1, 0, 'Module ps_facetedsearch has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-11-21 11:07:32', '2022-11-21 11:07:32'),
(564, 1, 0, 'Back office connection from ::1', '', 0, NULL, NULL, 1, 1, 1, '2022-11-21 11:17:04', '2022-11-21 11:17:04'),
(565, 1, 0, 'Back office connection from ::1', '', 0, NULL, NULL, 1, 1, 1, '2022-11-21 13:06:02', '2022-11-21 13:06:02'),
(566, 1, 0, 'Back office connection from ::1', '', 0, NULL, NULL, 1, 1, 1, '2022-12-05 09:04:27', '2022-12-05 09:04:27'),
(567, 1, 0, 'Product deactivated: 19', 'Product', 19, 1, NULL, 1, 0, 1, '2022-12-05 09:29:05', '2022-12-05 09:29:05'),
(568, 1, 0, 'Product deactivated: 18', 'Product', 18, 1, NULL, 1, 0, 1, '2022-12-05 09:29:06', '2022-12-05 09:29:06'),
(569, 1, 0, 'Product deactivated: 17', 'Product', 17, 1, NULL, 1, 0, 1, '2022-12-05 09:29:07', '2022-12-05 09:29:07'),
(570, 1, 0, 'Product deactivated: 16', 'Product', 16, 1, NULL, 1, 0, 1, '2022-12-05 09:29:11', '2022-12-05 09:29:11'),
(571, 1, 0, 'Product deactivated: 15', 'Product', 15, 1, NULL, 1, 0, 1, '2022-12-05 09:29:15', '2022-12-05 09:29:15'),
(572, 1, 0, 'Product deactivated: 14', 'Product', 14, 1, NULL, 1, 0, 1, '2022-12-05 09:29:20', '2022-12-05 09:29:20'),
(573, 1, 0, 'Product deactivated: 13', 'Product', 13, 1, NULL, 1, 0, 1, '2022-12-05 09:29:25', '2022-12-05 09:29:25'),
(574, 1, 0, 'Product deactivated: 1', 'Product', 1, 1, NULL, 1, 0, 1, '2022-12-05 09:29:33', '2022-12-05 09:29:33'),
(575, 1, 0, 'Product deactivated: 12', 'Product', 12, 1, NULL, 1, 0, 1, '2022-12-05 09:29:37', '2022-12-05 09:29:37'),
(576, 1, 0, 'Product deactivated: 11', 'Product', 11, 1, NULL, 1, 0, 1, '2022-12-05 09:29:42', '2022-12-05 09:29:42'),
(577, 1, 0, 'Product deactivated: 10', 'Product', 10, 1, NULL, 1, 0, 1, '2022-12-05 09:29:46', '2022-12-05 09:29:46'),
(578, 1, 0, 'Product deactivated: 9', 'Product', 9, 1, NULL, 1, 0, 1, '2022-12-05 09:29:51', '2022-12-05 09:29:51'),
(579, 1, 0, 'Product deactivated: 8', 'Product', 8, 1, NULL, 1, 0, 1, '2022-12-05 09:29:57', '2022-12-05 09:29:57'),
(580, 1, 0, 'Products deactivated: (19).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:10', '2022-12-05 09:35:10'),
(581, 1, 0, 'Products deactivated: (18).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:11', '2022-12-05 09:35:11'),
(582, 1, 0, 'Products deactivated: (17).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:11', '2022-12-05 09:35:11'),
(583, 1, 0, 'Products deactivated: (16).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:11', '2022-12-05 09:35:11'),
(584, 1, 0, 'Products deactivated: (15).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:11', '2022-12-05 09:35:11'),
(585, 1, 0, 'Products deactivated: (14).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:12', '2022-12-05 09:35:12'),
(586, 1, 0, 'Products deactivated: (13).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:12', '2022-12-05 09:35:12'),
(587, 1, 0, 'Products deactivated: (12).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:12', '2022-12-05 09:35:12'),
(588, 1, 0, 'Products deactivated: (11).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:12', '2022-12-05 09:35:12'),
(589, 1, 0, 'Products deactivated: (10).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:12', '2022-12-05 09:35:12'),
(590, 1, 0, 'Products deactivated: (9).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:13', '2022-12-05 09:35:13'),
(591, 1, 0, 'Products deactivated: (8).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:13', '2022-12-05 09:35:13'),
(592, 1, 0, 'Products deactivated: (7).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:13', '2022-12-05 09:35:13'),
(593, 1, 0, 'Products deactivated: (6).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:13', '2022-12-05 09:35:13'),
(594, 1, 0, 'Products deactivated: (5).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:14', '2022-12-05 09:35:14'),
(595, 1, 0, 'Products deactivated: (4).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:14', '2022-12-05 09:35:14'),
(596, 1, 0, 'Products deactivated: (3).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:14', '2022-12-05 09:35:14'),
(597, 1, 0, 'Products deactivated: (2).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:14', '2022-12-05 09:35:14'),
(598, 1, 0, 'Products deactivated: (1).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 09:35:14', '2022-12-05 09:35:14'),
(599, 1, 0, 'Product modification', 'Product', 20, 1, NULL, 1, 0, 1, '2022-12-05 09:45:50', '2022-12-05 09:45:50'),
(600, 1, 0, 'Product modification', 'Product', 20, 1, NULL, 1, 0, 1, '2022-12-05 09:46:10', '2022-12-05 09:46:10'),
(601, 1, 0, 'Product activated: 20', 'Product', 20, 1, NULL, 1, 0, 1, '2022-12-05 09:55:41', '2022-12-05 09:55:41'),
(602, 1, 0, 'Product modification', 'Product', 20, 1, NULL, 1, 0, 1, '2022-12-05 10:04:04', '2022-12-05 10:04:04'),
(603, 1, 0, 'Exporting mail with theme modern for language Italiano (Italian)', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(604, 1, 0, 'Core output folder: C:\\\\wamp64\\\\www\\\\prestashop/mails', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(605, 1, 0, 'Modules output folder: C:\\\\wamp64\\\\www\\\\prestashop/modules/', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(606, 1, 0, 'Generate html template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\account.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(607, 1, 0, 'Generate txt template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\account.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(608, 1, 0, 'Generate html template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\backoffice_order.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(609, 1, 0, 'Generate txt template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\backoffice_order.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(610, 1, 0, 'Generate html template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\bankwire.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(611, 1, 0, 'Generate txt template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\bankwire.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(612, 1, 0, 'Generate html template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\cheque.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(613, 1, 0, 'Generate txt template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\cheque.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(614, 1, 0, 'Generate html template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\contact.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(615, 1, 0, 'Generate txt template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\contact.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(616, 1, 0, 'Generate html template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\contact_form.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(617, 1, 0, 'Generate txt template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\contact_form.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:08', '2022-12-05 11:00:08'),
(618, 1, 0, 'Generate html template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\credit_slip.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(619, 1, 0, 'Generate txt template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\credit_slip.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(620, 1, 0, 'Generate html template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\download_product.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(621, 1, 0, 'Generate txt template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\download_product.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(622, 1, 0, 'Generate html template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\employee_password.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(623, 1, 0, 'Generate txt template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\employee_password.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(624, 1, 0, 'Generate html template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\forward_msg.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(625, 1, 0, 'Generate txt template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\forward_msg.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(626, 1, 0, 'Generate html template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\guest_to_customer.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(627, 1, 0, 'Generate txt template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\guest_to_customer.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(628, 1, 0, 'Generate html template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\import.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(629, 1, 0, 'Generate txt template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\import.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(630, 1, 0, 'Generate html template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\in_transit.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(631, 1, 0, 'Generate txt template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\in_transit.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(632, 1, 0, 'Generate html template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\log_alert.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(633, 1, 0, 'Generate txt template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\log_alert.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(634, 1, 0, 'Generate html template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\newsletter.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(635, 1, 0, 'Generate txt template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\newsletter.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(636, 1, 0, 'Generate html template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_canceled.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(637, 1, 0, 'Generate txt template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_canceled.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(638, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_changed.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(639, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_changed.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(640, 1, 0, 'Generate html template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_conf.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(641, 1, 0, 'Generate txt template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_conf.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(642, 1, 0, 'Generate html template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_customer_comment.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(643, 1, 0, 'Generate txt template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_customer_comment.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:09', '2022-12-05 11:00:09'),
(644, 1, 0, 'Generate html template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_merchant_comment.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(645, 1, 0, 'Generate txt template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_merchant_comment.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(646, 1, 0, 'Generate html template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_return_state.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(647, 1, 0, 'Generate txt template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\order_return_state.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(648, 1, 0, 'Generate html template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\outofstock.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(649, 1, 0, 'Generate txt template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\outofstock.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(650, 1, 0, 'Generate html template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\password.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(651, 1, 0, 'Generate txt template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\password.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(652, 1, 0, 'Generate html template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\password_query.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(653, 1, 0, 'Generate txt template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\password_query.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(654, 1, 0, 'Generate html template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\payment.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(655, 1, 0, 'Generate txt template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\payment.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(656, 1, 0, 'Generate html template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\payment_error.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(657, 1, 0, 'Generate txt template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\payment_error.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(658, 1, 0, 'Generate html template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\preparation.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(659, 1, 0, 'Generate txt template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\preparation.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(660, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\productoutofstock.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(661, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\productoutofstock.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(662, 1, 0, 'Generate html template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\refund.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(663, 1, 0, 'Generate txt template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\refund.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(664, 1, 0, 'Generate html template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\reply_msg.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(665, 1, 0, 'Generate txt template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\reply_msg.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(666, 1, 0, 'Generate html template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\shipped.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(667, 1, 0, 'Generate txt template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\shipped.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(668, 1, 0, 'Generate html template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\test.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(669, 1, 0, 'Generate txt template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\test.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(670, 1, 0, 'Generate html template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\voucher.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(671, 1, 0, 'Generate txt template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\voucher.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(672, 1, 0, 'Generate html template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\voucher_new.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:10', '2022-12-05 11:00:10'),
(673, 1, 0, 'Generate txt template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\it\\\\voucher_new.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(674, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\it\\\\followup_1.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(675, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\it\\\\followup_1.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(676, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\it\\\\followup_2.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(677, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\it\\\\followup_2.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(678, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\it\\\\followup_3.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(679, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\it\\\\followup_3.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(680, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\it\\\\followup_4.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(681, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\it\\\\followup_4.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(682, 1, 0, 'Generate html template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\customer_qty.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(683, 1, 0, 'Generate txt template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\customer_qty.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(684, 1, 0, 'Generate html template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\new_order.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(685, 1, 0, 'Generate txt template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\new_order.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(686, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\order_changed.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(687, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\order_changed.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(688, 1, 0, 'Generate html template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\productcoverage.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(689, 1, 0, 'Generate txt template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\productcoverage.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(690, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\productoutofstock.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(691, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\productoutofstock.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(692, 1, 0, 'Generate html template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\return_slip.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(693, 1, 0, 'Generate txt template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\it\\\\return_slip.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(694, 1, 0, 'Generate html template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\it\\\\newsletter_conf.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(695, 1, 0, 'Generate txt template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\it\\\\newsletter_conf.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(696, 1, 0, 'Generate html template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\it\\\\newsletter_verif.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(697, 1, 0, 'Generate txt template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\it\\\\newsletter_verif.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(698, 1, 0, 'Generate html template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\it\\\\newsletter_voucher.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(699, 1, 0, 'Generate txt template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\it\\\\newsletter_voucher.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:11', '2022-12-05 11:00:11'),
(700, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\it\\\\followup_1.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(701, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\it\\\\followup_1.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(702, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\it\\\\followup_2.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(703, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\it\\\\followup_2.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(704, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\it\\\\followup_3.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(705, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\it\\\\followup_3.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(706, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\it\\\\followup_4.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(707, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\it\\\\followup_4.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(708, 1, 0, 'Generate html template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\it\\\\referralprogram-congratulations.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(709, 1, 0, 'Generate txt template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\it\\\\referralprogram-congratulations.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(710, 1, 0, 'Generate html template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\it\\\\referralprogram-invitation.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(711, 1, 0, 'Generate txt template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\it\\\\referralprogram-invitation.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(712, 1, 0, 'Generate html template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\it\\\\referralprogram-voucher.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(713, 1, 0, 'Generate txt template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\it\\\\referralprogram-voucher.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 11:00:12', '2022-12-05 11:00:12'),
(714, 1, 0, 'Product modification', 'Product', 23, 1, NULL, 1, 0, 1, '2022-12-05 11:06:19', '2022-12-05 11:06:19'),
(715, 1, 0, 'Product modification', 'Product', 23, 1, NULL, 1, 0, 1, '2022-12-05 11:06:24', '2022-12-05 11:06:24'),
(716, 1, 0, 'Product modification', 'Product', 23, 1, NULL, 1, 0, 1, '2022-12-05 11:07:56', '2022-12-05 11:07:56'),
(717, 1, 0, 'Product modification', 'Product', 23, 1, NULL, 1, 0, 1, '2022-12-05 11:07:58', '2022-12-05 11:07:58'),
(718, 1, 0, 'Product activated: 23', 'Product', 23, 1, NULL, 1, 0, 1, '2022-12-05 11:08:05', '2022-12-05 11:08:05'),
(719, 1, 0, 'Products deleted: (19).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:10:59', '2022-12-05 12:10:59'),
(720, 1, 0, 'Products deleted: (18).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:10:59', '2022-12-05 12:10:59'),
(721, 1, 0, 'Products deleted: (17).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:10:59', '2022-12-05 12:10:59'),
(722, 1, 0, 'Products deleted: (16).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:10:59', '2022-12-05 12:10:59'),
(723, 1, 0, 'Products deleted: (15).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:00', '2022-12-05 12:11:00'),
(724, 1, 0, 'Products deleted: (14).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:00', '2022-12-05 12:11:00'),
(725, 1, 0, 'Products deleted: (13).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:00', '2022-12-05 12:11:00'),
(726, 1, 0, 'Products deleted: (12).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:00', '2022-12-05 12:11:00'),
(727, 1, 0, 'Products deleted: (11).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:01', '2022-12-05 12:11:01'),
(728, 1, 0, 'Products deleted: (10).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:01', '2022-12-05 12:11:01'),
(729, 1, 0, 'Products deleted: (9).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:01', '2022-12-05 12:11:01'),
(730, 1, 0, 'Products deleted: (8).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:01', '2022-12-05 12:11:01'),
(731, 1, 0, 'Products deleted: (7).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:02', '2022-12-05 12:11:02'),
(732, 1, 0, 'Products deleted: (6).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:02', '2022-12-05 12:11:02'),
(733, 1, 0, 'Products deleted: (5).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:02', '2022-12-05 12:11:02'),
(734, 1, 0, 'Products deleted: (4).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:02', '2022-12-05 12:11:02'),
(735, 1, 0, 'Products deleted: (3).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:02', '2022-12-05 12:11:02'),
(736, 1, 0, 'Products deleted: (2).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:03', '2022-12-05 12:11:03'),
(737, 1, 0, 'Products deleted: (1).', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 12:11:12', '2022-12-05 12:11:12'),
(738, 1, 0, 'Product modification', 'Product', 24, 1, NULL, 1, 0, 1, '2022-12-05 12:32:46', '2022-12-05 12:32:46'),
(739, 1, 0, 'Product modification', 'Product', 24, 1, NULL, 1, 0, 1, '2022-12-05 12:32:51', '2022-12-05 12:32:51'),
(740, 1, 0, 'Product modification', 'Product', 24, 1, NULL, 1, 0, 1, '2022-12-05 12:35:03', '2022-12-05 12:35:03'),
(741, 1, 0, 'Exporting mail with theme modern for language Español (Spanish)', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:54', '2022-12-05 13:24:54'),
(742, 1, 0, 'Core output folder: C:\\\\wamp64\\\\www\\\\prestashop/mails', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:54', '2022-12-05 13:24:54'),
(743, 1, 0, 'Modules output folder: C:\\\\wamp64\\\\www\\\\prestashop/modules/', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:54', '2022-12-05 13:24:54'),
(744, 1, 0, 'Generate html template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\account.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:54', '2022-12-05 13:24:54'),
(745, 1, 0, 'Generate txt template account at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\account.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:54', '2022-12-05 13:24:54'),
(746, 1, 0, 'Generate html template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\backoffice_order.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:54', '2022-12-05 13:24:54'),
(747, 1, 0, 'Generate txt template backoffice_order at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\backoffice_order.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:54', '2022-12-05 13:24:54'),
(748, 1, 0, 'Generate html template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\bankwire.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:54', '2022-12-05 13:24:54'),
(749, 1, 0, 'Generate txt template bankwire at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\bankwire.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:54', '2022-12-05 13:24:54'),
(750, 1, 0, 'Generate html template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\cheque.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(751, 1, 0, 'Generate txt template cheque at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\cheque.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(752, 1, 0, 'Generate html template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\contact.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(753, 1, 0, 'Generate txt template contact at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\contact.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(754, 1, 0, 'Generate html template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\contact_form.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(755, 1, 0, 'Generate txt template contact_form at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\contact_form.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(756, 1, 0, 'Generate html template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\credit_slip.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(757, 1, 0, 'Generate txt template credit_slip at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\credit_slip.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(758, 1, 0, 'Generate html template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\download_product.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(759, 1, 0, 'Generate txt template download_product at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\download_product.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(760, 1, 0, 'Generate html template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\employee_password.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(761, 1, 0, 'Generate txt template employee_password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\employee_password.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(762, 1, 0, 'Generate html template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\forward_msg.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(763, 1, 0, 'Generate txt template forward_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\forward_msg.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(764, 1, 0, 'Generate html template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\guest_to_customer.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(765, 1, 0, 'Generate txt template guest_to_customer at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\guest_to_customer.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(766, 1, 0, 'Generate html template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\import.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(767, 1, 0, 'Generate txt template import at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\import.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(768, 1, 0, 'Generate html template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\in_transit.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(769, 1, 0, 'Generate txt template in_transit at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\in_transit.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(770, 1, 0, 'Generate html template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\log_alert.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(771, 1, 0, 'Generate txt template log_alert at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\log_alert.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(772, 1, 0, 'Generate html template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\newsletter.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(773, 1, 0, 'Generate txt template newsletter at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\newsletter.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(774, 1, 0, 'Generate html template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_canceled.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(775, 1, 0, 'Generate txt template order_canceled at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_canceled.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(776, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_changed.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(777, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_changed.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(778, 1, 0, 'Generate html template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_conf.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(779, 1, 0, 'Generate txt template order_conf at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_conf.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(780, 1, 0, 'Generate html template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_customer_comment.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(781, 1, 0, 'Generate txt template order_customer_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_customer_comment.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(782, 1, 0, 'Generate html template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_merchant_comment.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(783, 1, 0, 'Generate txt template order_merchant_comment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_merchant_comment.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(784, 1, 0, 'Generate html template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_return_state.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(785, 1, 0, 'Generate txt template order_return_state at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\order_return_state.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(786, 1, 0, 'Generate html template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\outofstock.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(787, 1, 0, 'Generate txt template outofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\outofstock.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(788, 1, 0, 'Generate html template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\password.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(789, 1, 0, 'Generate txt template password at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\password.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(790, 1, 0, 'Generate html template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\password_query.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(791, 1, 0, 'Generate txt template password_query at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\password_query.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(792, 1, 0, 'Generate html template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\payment.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(793, 1, 0, 'Generate txt template payment at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\payment.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(794, 1, 0, 'Generate html template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\payment_error.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(795, 1, 0, 'Generate txt template payment_error at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\payment_error.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:55', '2022-12-05 13:24:55'),
(796, 1, 0, 'Generate html template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\preparation.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(797, 1, 0, 'Generate txt template preparation at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\preparation.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(798, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\productoutofstock.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(799, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\productoutofstock.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(800, 1, 0, 'Generate html template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\refund.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(801, 1, 0, 'Generate txt template refund at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\refund.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(802, 1, 0, 'Generate html template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\reply_msg.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(803, 1, 0, 'Generate txt template reply_msg at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\reply_msg.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(804, 1, 0, 'Generate html template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\shipped.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(805, 1, 0, 'Generate txt template shipped at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\shipped.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(806, 1, 0, 'Generate html template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\test.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(807, 1, 0, 'Generate txt template test at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\test.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(808, 1, 0, 'Generate html template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\voucher.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(809, 1, 0, 'Generate txt template voucher at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\voucher.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(810, 1, 0, 'Generate html template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\voucher_new.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(811, 1, 0, 'Generate txt template voucher_new at C:\\\\wamp64\\\\www\\\\prestashop/mails\\\\es\\\\voucher_new.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(812, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\es\\\\followup_1.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(813, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\es\\\\followup_1.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56');
INSERT INTO `ps_log` (`id_log`, `severity`, `error_code`, `message`, `object_type`, `object_id`, `id_shop`, `id_shop_group`, `id_lang`, `in_all_shops`, `id_employee`, `date_add`, `date_upd`) VALUES
(814, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\es\\\\followup_2.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(815, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\es\\\\followup_2.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(816, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\es\\\\followup_3.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(817, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\es\\\\followup_3.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(818, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\es\\\\followup_4.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(819, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\followup\\\\mails\\\\es\\\\followup_4.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(820, 1, 0, 'Generate html template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\customer_qty.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(821, 1, 0, 'Generate txt template customer_qty at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\customer_qty.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(822, 1, 0, 'Generate html template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\new_order.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(823, 1, 0, 'Generate txt template new_order at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\new_order.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(824, 1, 0, 'Generate html template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\order_changed.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(825, 1, 0, 'Generate txt template order_changed at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\order_changed.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(826, 1, 0, 'Generate html template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\productcoverage.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(827, 1, 0, 'Generate txt template productcoverage at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\productcoverage.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(828, 1, 0, 'Generate html template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\productoutofstock.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(829, 1, 0, 'Generate txt template productoutofstock at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\productoutofstock.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(830, 1, 0, 'Generate html template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\return_slip.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(831, 1, 0, 'Generate txt template return_slip at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailalerts\\\\mails\\\\es\\\\return_slip.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(832, 1, 0, 'Generate html template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\es\\\\newsletter_conf.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(833, 1, 0, 'Generate txt template newsletter_conf at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\es\\\\newsletter_conf.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(834, 1, 0, 'Generate html template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\es\\\\newsletter_verif.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(835, 1, 0, 'Generate txt template newsletter_verif at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\es\\\\newsletter_verif.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(836, 1, 0, 'Generate html template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\es\\\\newsletter_voucher.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(837, 1, 0, 'Generate txt template newsletter_voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_emailsubscription\\\\mails\\\\es\\\\newsletter_voucher.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(838, 1, 0, 'Generate html template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\es\\\\followup_1.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(839, 1, 0, 'Generate txt template followup_1 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\es\\\\followup_1.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(840, 1, 0, 'Generate html template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\es\\\\followup_2.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(841, 1, 0, 'Generate txt template followup_2 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\es\\\\followup_2.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:56', '2022-12-05 13:24:56'),
(842, 1, 0, 'Generate html template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\es\\\\followup_3.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:57', '2022-12-05 13:24:57'),
(843, 1, 0, 'Generate txt template followup_3 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\es\\\\followup_3.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:57', '2022-12-05 13:24:57'),
(844, 1, 0, 'Generate html template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\es\\\\followup_4.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:57', '2022-12-05 13:24:57'),
(845, 1, 0, 'Generate txt template followup_4 at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\ps_reminder\\\\mails\\\\es\\\\followup_4.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:57', '2022-12-05 13:24:57'),
(846, 1, 0, 'Generate html template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\es\\\\referralprogram-congratulations.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:57', '2022-12-05 13:24:57'),
(847, 1, 0, 'Generate txt template referralprogram-congratulations at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\es\\\\referralprogram-congratulations.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:57', '2022-12-05 13:24:57'),
(848, 1, 0, 'Generate html template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\es\\\\referralprogram-invitation.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:57', '2022-12-05 13:24:57'),
(849, 1, 0, 'Generate txt template referralprogram-invitation at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\es\\\\referralprogram-invitation.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:57', '2022-12-05 13:24:57'),
(850, 1, 0, 'Generate html template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\es\\\\referralprogram-voucher.html', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:57', '2022-12-05 13:24:57'),
(851, 1, 0, 'Generate txt template referralprogram-voucher at C:\\\\wamp64\\\\www\\\\prestashop/modules/\\\\referralprogram\\\\mails\\\\es\\\\referralprogram-voucher.txt', '', 0, 1, NULL, 1, 0, 1, '2022-12-05 13:24:57', '2022-12-05 13:24:57'),
(852, 1, 0, 'Product modification', 'Product', 24, 1, NULL, 1, 0, 1, '2022-12-05 14:06:52', '2022-12-05 14:06:52'),
(853, 1, 0, 'Product modification', 'Product', 23, 1, NULL, 1, 0, 1, '2022-12-05 14:17:52', '2022-12-05 14:17:52'),
(854, 1, 0, 'Product modification', 'Product', 23, 1, NULL, 1, 0, 1, '2022-12-05 14:20:11', '2022-12-05 14:20:11'),
(855, 1, 0, 'Product modification', 'Product', 23, 1, NULL, 1, 0, 1, '2022-12-05 14:20:24', '2022-12-05 14:20:24'),
(856, 1, 0, 'Back office connection from ::1', '', 0, NULL, NULL, 1, 1, 1, '2022-12-08 16:13:55', '2022-12-08 16:13:55'),
(857, 1, 0, 'Back office connection from ::1', '', 0, NULL, NULL, 1, 1, 1, '2022-12-11 15:48:32', '2022-12-11 15:48:32'),
(858, 1, 0, 'Product modification', 'Product', 20, 1, NULL, 1, 0, 1, '2022-12-11 15:52:06', '2022-12-11 15:52:06'),
(859, 1, 0, 'Protect vendor folder in module ps_viewedproduct', '', 0, 1, NULL, 1, 0, 1, '2022-12-11 16:01:44', '2022-12-11 16:01:44'),
(860, 1, 0, 'Protect vendor folder in module myshophelper', '', 0, 1, NULL, 1, 0, 1, '2022-12-11 16:04:15', '2022-12-11 16:04:15'),
(861, 1, 0, 'Module myshophelper has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-12-11 16:04:15', '2022-12-11 16:04:15'),
(862, 1, 0, 'Protect vendor folder in module crazyelements', '', 0, 1, NULL, 1, 0, 1, '2022-12-11 16:04:16', '2022-12-11 16:04:16'),
(863, 1, 0, 'Module crazyelements has no vendor folder', '', 0, 1, NULL, 1, 0, 1, '2022-12-11 16:04:16', '2022-12-11 16:04:16'),
(864, 1, 0, 'Protect vendor folder in module smartsupp', '', 0, 1, NULL, 1, 0, 1, '2022-12-11 16:14:00', '2022-12-11 16:14:00'),
(865, 1, 0, 'Product modification', 'Product', 23, 1, NULL, 1, 0, 1, '2022-12-11 16:39:06', '2022-12-11 16:39:06'),
(866, 1, 0, 'AdminCrazyContent addition', 'AdminCrazyContent', 1, 1, NULL, 1, 0, 1, '2022-12-11 16:46:13', '2022-12-11 16:46:13'),
(867, 1, 0, 'Protect vendor folder in module ps_banner', '', 0, 1, NULL, 1, 0, 1, '2022-12-11 16:48:59', '2022-12-11 16:48:59');

-- --------------------------------------------------------

--
-- Structure de la table `ps_mail`
--

DROP TABLE IF EXISTS `ps_mail`;
CREATE TABLE IF NOT EXISTS `ps_mail` (
  `id_mail` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `recipient` varchar(126) NOT NULL,
  `template` varchar(62) NOT NULL,
  `subject` varchar(254) NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_mail`),
  KEY `recipient` (`recipient`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_manufacturer`
--

DROP TABLE IF EXISTS `ps_manufacturer`;
CREATE TABLE IF NOT EXISTS `ps_manufacturer` (
  `id_manufacturer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_manufacturer`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_manufacturer`
--

INSERT INTO `ps_manufacturer` (`id_manufacturer`, `name`, `date_add`, `date_upd`, `active`) VALUES
(1, 'Studio Design', '2022-11-21 11:07:24', '2022-12-11 16:20:04', 0),
(2, 'Le Dédale', '2022-11-21 11:07:24', '2022-12-11 16:20:50', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_manufacturer_lang`
--

DROP TABLE IF EXISTS `ps_manufacturer_lang`;
CREATE TABLE IF NOT EXISTS `ps_manufacturer_lang` (
  `id_manufacturer` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text,
  `short_description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id_manufacturer`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_manufacturer_lang`
--

INSERT INTO `ps_manufacturer_lang` (`id_manufacturer`, `id_lang`, `description`, `short_description`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, '<p>Studio Design offers a range of items from ready-to-wear collections to contemporary objects. The brand has been presenting new ideas and trends since its creation in 2012.</p>', '', '', '', ''),
(1, 2, '<p>Studio Design propose une gamme de produits variée : prêt-à-porter, objets de décoration, accessoires de maison... Depuis sa création en 2012, la marque apporte des idées et des tendances nouvelles à travers ses collections. </p>', '', '', '', ''),
(1, 3, '<p>Studio Design offers a range of items from ready-to-wear collections to contemporary objects. The brand has been presenting new ideas and trends since its creation in 2012.</p>', '', '', '', ''),
(1, 4, '<p>Studio Design offers a range of items from ready-to-wear collections to contemporary objects. The brand has been presenting new ideas and trends since its creation in 2012.</p>', '', '', '', ''),
(1, 5, '<p>Studio Design offers a range of items from ready-to-wear collections to contemporary objects. The brand has been presenting new ideas and trends since its creation in 2012.</p>', '', '', '', ''),
(1, 6, '<p>Studio Design offers a range of items from ready-to-wear collections to contemporary objects. The brand has been presenting new ideas and trends since its creation in 2012.</p>', '', '', '', ''),
(2, 1, '', '', '', '', ''),
(2, 2, '<p>Graphic Corner propose depuis 2010 un large choix d\'affiches et de posters disponibles en version papier et sur de nombreux supports.</p>', '', '', '', ''),
(2, 3, '<p>Since 2010, Graphic Corner offers a large choice of quality posters, available on paper and many other formats.</p>', '', '', '', ''),
(2, 4, '<p>Since 2010, Graphic Corner offers a large choice of quality posters, available on paper and many other formats.</p>', '', '', '', ''),
(2, 5, '<p>Since 2010, Graphic Corner offers a large choice of quality posters, available on paper and many other formats.</p>', '', '', '', ''),
(2, 6, '<p>Since 2010, Graphic Corner offers a large choice of quality posters, available on paper and many other formats.</p>', '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_manufacturer_shop`
--

DROP TABLE IF EXISTS `ps_manufacturer_shop`;
CREATE TABLE IF NOT EXISTS `ps_manufacturer_shop` (
  `id_manufacturer` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_manufacturer`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_manufacturer_shop`
--

INSERT INTO `ps_manufacturer_shop` (`id_manufacturer`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_memcached_servers`
--

DROP TABLE IF EXISTS `ps_memcached_servers`;
CREATE TABLE IF NOT EXISTS `ps_memcached_servers` (
  `id_memcached_server` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip` varchar(254) NOT NULL,
  `port` int(11) UNSIGNED NOT NULL,
  `weight` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_memcached_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_message`
--

DROP TABLE IF EXISTS `ps_message`;
CREATE TABLE IF NOT EXISTS `ps_message` (
  `id_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cart` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `message` text NOT NULL,
  `private` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_message`),
  KEY `message_order` (`id_order`),
  KEY `id_cart` (`id_cart`),
  KEY `id_customer` (`id_customer`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_message_readed`
--

DROP TABLE IF EXISTS `ps_message_readed`;
CREATE TABLE IF NOT EXISTS `ps_message_readed` (
  `id_message` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_message`,`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_meta`
--

DROP TABLE IF EXISTS `ps_meta`;
CREATE TABLE IF NOT EXISTS `ps_meta` (
  `id_meta` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `page` varchar(64) NOT NULL,
  `configurable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_meta`),
  UNIQUE KEY `page` (`page`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_meta`
--

INSERT INTO `ps_meta` (`id_meta`, `page`, `configurable`) VALUES
(1, 'pagenotfound', 1),
(2, 'best-sales', 1),
(3, 'contact', 1),
(4, 'index', 1),
(5, 'manufacturer', 1),
(6, 'new-products', 1),
(7, 'password', 1),
(8, 'prices-drop', 1),
(9, 'sitemap', 1),
(10, 'supplier', 1),
(11, 'address', 1),
(12, 'addresses', 1),
(13, 'authentication', 1),
(14, 'cart', 1),
(15, 'discount', 1),
(16, 'history', 1),
(17, 'identity', 1),
(18, 'my-account', 1),
(19, 'order-follow', 1),
(20, 'order-slip', 1),
(21, 'order', 1),
(22, 'search', 1),
(23, 'stores', 1),
(24, 'guest-tracking', 1),
(25, 'order-confirmation', 1),
(26, 'product', 0),
(27, 'category', 0),
(28, 'cms', 0),
(29, 'module-cheque-payment', 0),
(30, 'module-cheque-validation', 0),
(31, 'module-bankwire-validation', 0),
(32, 'module-bankwire-payment', 0),
(33, 'module-cashondelivery-validation', 0),
(34, 'module-ps_checkpayment-payment', 1),
(35, 'module-ps_checkpayment-validation', 1),
(36, 'module-ps_emailsubscription-verification', 1),
(37, 'module-ps_emailsubscription-subscription', 1),
(38, 'module-ps_shoppingcart-ajax', 1),
(39, 'module-ps_wirepayment-payment', 1),
(40, 'module-ps_wirepayment-validation', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_meta_lang`
--

DROP TABLE IF EXISTS `ps_meta_lang`;
CREATE TABLE IF NOT EXISTS `ps_meta_lang` (
  `id_meta` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `url_rewrite` varchar(254) NOT NULL,
  PRIMARY KEY (`id_meta`,`id_shop`,`id_lang`),
  KEY `id_shop` (`id_shop`),
  KEY `id_lang` (`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_meta_lang`
--

INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
(1, 1, 1, '404 error', 'This page cannot be found', '', 'page-not-found'),
(1, 1, 2, 'Erreur 404', 'Impossible de trouver la page', '', 'page-introuvable'),
(1, 1, 3, 'Fehler 404', 'Seite wurde nicht gefunden', '', 'seite-nicht-gefunden'),
(1, 1, 4, '404 fout', 'We hebben de pagina niet gevonden', '', 'pagina-niet-gevonden'),
(1, 1, 5, 'errore 404', 'Impossibile trovare la pagina', '', 'pagina-non-trovata'),
(1, 1, 6, 'Error 404', 'Página no encontrada', '', 'pagina-no-encontrada'),
(2, 1, 1, 'Best sales', 'Our best sales', '', 'best-sales'),
(2, 1, 2, 'Meilleures ventes', 'Nos meilleures ventes', '', 'meilleures-ventes'),
(2, 1, 3, 'Verkaufshits', 'Verkaufshits', '', 'verkaufshits'),
(2, 1, 4, 'Best verkochte artikelen', 'Onze best verkochte artikelen', '', 'best-verkochte-artikelen'),
(2, 1, 5, 'Più venduti', 'I nostri prodotti più venduti', '', 'piu-venduti'),
(2, 1, 6, 'Los más vendidos', 'Los más vendidos', '', 'mas-vendidos'),
(3, 1, 1, 'Contact us', 'Use our form to contact us', '', 'contact-us'),
(3, 1, 2, 'Contactez-nous', 'Utiliser le formulaire pour nous contacter', '', 'nous-contacter'),
(3, 1, 3, 'Kontakt', 'Nutzen Sie unser Kontaktformular', '', 'kontakt'),
(3, 1, 4, 'Contacteer ons', 'Neem contact met ons op via ons formulier', '', 'contact-opnemen'),
(3, 1, 5, 'Contattaci', 'Si può usare il nostro modulo per contattarci', '', 'contattaci'),
(3, 1, 6, 'Contacte con nosotros', 'Contáctenos', '', 'contactenos'),
(4, 1, 1, '', 'Shop powered by PrestaShop', '', ''),
(4, 1, 2, '', 'Boutique propulsée par PrestaShop', '', ''),
(4, 1, 3, '', 'Powered by PrestaShop', '', ''),
(4, 1, 4, '', 'Winkel gerund met behulp van PrestaShop', '', ''),
(4, 1, 5, '', 'Negozio creato usando PrestaShop', '', ''),
(4, 1, 6, '', 'Tienda creada con PrestaShop', '', ''),
(5, 1, 1, 'Brands', 'Brands list', '', 'brands'),
(5, 1, 2, 'Brands', 'Brands list', '', 'brands'),
(5, 1, 3, 'Brands', 'Brands list', '', 'brands'),
(5, 1, 4, 'Brands', 'Brands list', '', 'brands'),
(5, 1, 5, 'Brands', 'Brands list', '', 'brands'),
(5, 1, 6, 'Brands', 'Brands list', '', 'brands'),
(6, 1, 1, 'New products', 'Our new products', '', 'new-products'),
(6, 1, 2, 'Nouveaux produits', 'Nos nouveaux produits', '', 'nouveaux-produits'),
(6, 1, 3, 'Neue Artikel', 'Neue Artikel', '', 'neue-artikel'),
(6, 1, 4, 'Nieuwe producten', 'Onze nieuwe producten', '', 'nieuwe-producten'),
(6, 1, 5, 'Nuovi prodotti', 'I nostri nuovi prodotti, gli ultimi arrivi', '', 'nuovi-prodotti'),
(6, 1, 6, 'Novedades', 'Novedades', '', 'novedades'),
(7, 1, 1, 'Forgot your password', 'Enter the e-mail address you use to sign in to receive an e-mail with a new password', '', 'password-recovery'),
(7, 1, 2, 'Mot de passe oublié', 'Entrez l\'adresse e-mail que vous utilisez pour vous connecter afin de recevoir un e-mail avec un nouveau mot de passe', '', 'recuperation-mot-de-passe'),
(7, 1, 3, 'Passwort vergessen?', 'Geben Sie hier die E-Mail ein, unter der Sie sich angemeldet haben. Sie erhalten dann ein neues Passwort.', '', 'passwort-zuruecksetzen'),
(7, 1, 4, 'Uw wachtwoord vergeten', 'Voer het e-mailadres in dat u heeft gebruikt voor uw aanmelding om een e-mail met een nieuw wachtwoord te ontvangen', '', 'wachtwoord-opvragen'),
(7, 1, 5, 'Hai dimenticato la password', 'Inserisci l\'indirizzo e-mail che usi per accedere, per ricevere un\'e-mail con una nuova password', '', 'recupero-password'),
(7, 1, 6, 'Ha olvidado su contraseña', 'Introduzca la dirección de correo electrónico que utiliza a la hora de iniciar sesión, para recibir un correo electrónico con una nueva contraseña', '', 'recuperar-contraseña'),
(8, 1, 1, 'Prices drop', 'Our special products', '', 'prices-drop'),
(8, 1, 2, 'Promotions', 'Our special products', '', 'promotions'),
(8, 1, 3, 'Angebote', 'Our special products', '', 'angebote'),
(8, 1, 4, 'Aanbiedingen', 'Our special products', '', 'aanbiedingen'),
(8, 1, 5, 'Offerte', 'Our special products', '', 'offerte'),
(8, 1, 6, 'Ofertas', 'Our special products', '', 'productos-rebajados'),
(9, 1, 1, 'Sitemap', 'Lost ? Find what your are looking for', '', 'sitemap'),
(9, 1, 2, 'Plan du site', 'Vous êtes perdu ? Trouvez ce que vous cherchez', '', 'plan-site'),
(9, 1, 3, 'Sitemap', 'Wissen Sie nicht weiter? Vielleicht finden Sie es hier', '', 'Sitemap'),
(9, 1, 4, 'Sitemap', 'De weg kwijt? Vinden wat u zoekt', '', 'sitemap'),
(9, 1, 5, 'Mappa del sito', 'Vi siete persi? Qui potete trovate quello che state cercando', '', 'mappa-del-sito'),
(9, 1, 6, 'Mapa del sitio', '¿Perdido? Encuentre lo que está buscando', '', 'mapa del sitio'),
(10, 1, 1, 'Suppliers', 'Suppliers list', '', 'supplier'),
(10, 1, 2, 'Fournisseurs', 'Liste des fournisseurs', '', 'fournisseur'),
(10, 1, 3, 'Lieferanten', 'Lieferanten-Liste', '', 'lieferant'),
(10, 1, 4, 'Leveranciers', 'Lijst met leveranciers', '', 'leverancier'),
(10, 1, 5, 'Fornitori', 'Elenco dei fornitori', '', 'fornitori'),
(10, 1, 6, 'Proveedores', 'Listado de proveedores', '', 'proveedor'),
(11, 1, 1, 'Address', '', '', 'address'),
(11, 1, 2, 'Adresse', '', '', 'adresse'),
(11, 1, 3, 'Straße', '', '', 'adresse'),
(11, 1, 4, 'Adres', '', '', 'adres'),
(11, 1, 5, 'Indirizzo', '', '', 'indirizzo'),
(11, 1, 6, 'Dirección', '', '', 'direccion'),
(12, 1, 1, 'Addresses', '', '', 'addresses'),
(12, 1, 2, 'Adresses', '', '', 'adresses'),
(12, 1, 3, 'Adressen', '', '', 'adressen'),
(12, 1, 4, 'Adressen', '', '', 'adressen'),
(12, 1, 5, 'Indirizzi', '', '', 'indirizzi'),
(12, 1, 6, 'Direcciones', '', '', 'direcciones'),
(13, 1, 1, 'Login', '', '', 'login'),
(13, 1, 2, 'Identifiant', '', '', 'connexion'),
(13, 1, 3, 'Anmelden', '', '', 'anmeldung'),
(13, 1, 4, 'Inloggen', '', '', 'aanmelden'),
(13, 1, 5, 'Entra', '', '', 'login'),
(13, 1, 6, 'Iniciar sesión', '', '', 'iniciar-sesion'),
(14, 1, 1, 'Cart', '', '', 'cart'),
(14, 1, 2, 'Panier', '', '', 'panier'),
(14, 1, 3, 'Warenkorb', '', '', 'warenkorb'),
(14, 1, 4, 'Winkelwagen', '', '', 'winkelmandje'),
(14, 1, 5, 'Carrello', '', '', 'carrello'),
(14, 1, 6, 'Carrito', '', '', 'carrito'),
(15, 1, 1, 'Discount', '', '', 'discount'),
(15, 1, 2, 'Remise', '', '', 'reduction'),
(15, 1, 3, 'Rabatt', '', '', 'Rabatt'),
(15, 1, 4, 'Korting', '', '', 'korting'),
(15, 1, 5, 'Sconto', '', '', 'buoni-sconto'),
(15, 1, 6, 'Descuento', '', '', 'descuento'),
(16, 1, 1, 'Order history', '', '', 'order-history'),
(16, 1, 2, 'Historique de vos commandes', '', '', 'historique-commandes'),
(16, 1, 3, 'Bestellverlauf', '', '', 'bestellungsverlauf'),
(16, 1, 4, 'Bestelgeschiedenis', '', '', 'besteloverzicht'),
(16, 1, 5, 'Storico ordini', '', '', 'cronologia-ordini'),
(16, 1, 6, 'Historial de pedidos', '', '', 'historial-compra'),
(17, 1, 1, 'Identity', '', '', 'identity'),
(17, 1, 2, 'Identité', '', '', 'identite'),
(17, 1, 3, 'Profil', '', '', 'profil'),
(17, 1, 4, 'Identiteit', '', '', 'identiteit'),
(17, 1, 5, 'Dati personali', '', '', 'dati-personali'),
(17, 1, 6, 'Datos personales', '', '', 'datos-personales'),
(18, 1, 1, 'My account', '', '', 'my-account'),
(18, 1, 2, 'Mon compte', '', '', 'mon-compte'),
(18, 1, 3, 'Ihr Kundenbereich', '', '', 'mein-Konto'),
(18, 1, 4, 'Mijn account', '', '', 'mijn-account'),
(18, 1, 5, 'Il mio account', '', '', 'account'),
(18, 1, 6, 'Mi cuenta', '', '', 'mi-cuenta'),
(19, 1, 1, 'Order follow', '', '', 'order-follow'),
(19, 1, 2, 'Suivi de commande', '', '', 'suivi-commande'),
(19, 1, 3, 'Bestellung verfolgen', '', '', 'bestellverfolgung'),
(19, 1, 4, 'Bestelling volgen', '', '', 'bestelling-volgen'),
(19, 1, 5, 'Segui l\'ordine', '', '', 'segui-ordine'),
(19, 1, 6, 'Seguimiento del pedido', '', '', 'seguimiento-pedido'),
(20, 1, 1, 'Credit slip', '', '', 'credit-slip'),
(20, 1, 2, 'Avoir', '', '', 'avoirs'),
(20, 1, 3, 'Rechnungskorrektur', '', '', 'bestellschein'),
(20, 1, 4, 'Creditnota', '', '', 'bestel-bon'),
(20, 1, 5, 'Nota di credito', '', '', 'buono-ordine'),
(20, 1, 6, 'Factura por abono', '', '', 'facturas-abono'),
(21, 1, 1, 'Order', '', '', 'order'),
(21, 1, 2, 'Commande', '', '', 'commande'),
(21, 1, 3, 'Bestellung', '', '', 'Bestellung'),
(21, 1, 4, 'Bestelling', '', '', 'bestelling'),
(21, 1, 5, 'Ordine', '', '', 'ordine'),
(21, 1, 6, 'Pedido', '', '', 'pedido'),
(22, 1, 1, 'Search', '', '', 'search'),
(22, 1, 2, 'Rechercher', '', '', 'recherche'),
(22, 1, 3, 'Suche', '', '', 'suche'),
(22, 1, 4, 'Zoeken', '', '', 'zoeken'),
(22, 1, 5, 'Cerca', '', '', 'ricerca'),
(22, 1, 6, 'Buscar', '', '', 'busqueda'),
(23, 1, 1, 'Stores', '', '', 'stores'),
(23, 1, 2, 'Magasins', '', '', 'magasins'),
(23, 1, 3, 'Shops', '', '', 'shops'),
(23, 1, 4, 'Winkels', '', '', 'winkels'),
(23, 1, 5, 'Negozi', '', '', 'negozi'),
(23, 1, 6, 'Tiendas', '', '', 'tiendas'),
(24, 1, 1, 'Guest tracking', '', '', 'guest-tracking'),
(24, 1, 2, 'Suivi de commande invité', '', '', 'suivi-commande-invite'),
(24, 1, 3, 'Auftragsverfolgung Gast', '', '', 'auftragsverfolgung-gast'),
(24, 1, 4, 'Bestelling volgen als gast', '', '', 'bestelling-volgen-als-gast'),
(24, 1, 5, 'Tracciatura ospite', '', '', 'tracciatura-ospite'),
(24, 1, 6, 'Seguimiento de pedidos de clientes invitados', '', '', 'seguimiento-pedido-invitado'),
(25, 1, 1, 'Order confirmation', '', '', 'order-confirmation'),
(25, 1, 2, 'Confirmation de commande', '', '', 'confirmation-commande'),
(25, 1, 3, 'Bestätigung der Bestellung', '', '', 'bestellbestatigung'),
(25, 1, 4, 'Bestelling bevestigd', '', '', 'order-bevestiging'),
(25, 1, 5, 'Conferma ordine', '', '', 'conferma-ordine'),
(25, 1, 6, 'Confirmación de pedido', '', '', 'confirmacion-pedido'),
(34, 1, 1, '', '', '', ''),
(34, 1, 2, '', '', '', ''),
(34, 1, 3, '', '', '', ''),
(34, 1, 4, '', '', '', ''),
(34, 1, 5, '', '', '', ''),
(34, 1, 6, '', '', '', ''),
(35, 1, 1, '', '', '', ''),
(35, 1, 2, '', '', '', ''),
(35, 1, 3, '', '', '', ''),
(35, 1, 4, '', '', '', ''),
(35, 1, 5, '', '', '', ''),
(35, 1, 6, '', '', '', ''),
(36, 1, 1, '', '', '', ''),
(36, 1, 2, '', '', '', ''),
(36, 1, 3, '', '', '', ''),
(36, 1, 4, '', '', '', ''),
(36, 1, 5, '', '', '', ''),
(36, 1, 6, '', '', '', ''),
(37, 1, 1, '', '', '', ''),
(37, 1, 2, '', '', '', ''),
(37, 1, 3, '', '', '', ''),
(37, 1, 4, '', '', '', ''),
(37, 1, 5, '', '', '', ''),
(37, 1, 6, '', '', '', ''),
(38, 1, 1, '', '', '', ''),
(38, 1, 2, '', '', '', ''),
(38, 1, 3, '', '', '', ''),
(38, 1, 4, '', '', '', ''),
(38, 1, 5, '', '', '', ''),
(38, 1, 6, '', '', '', ''),
(39, 1, 1, '', '', '', ''),
(39, 1, 2, '', '', '', ''),
(39, 1, 3, '', '', '', ''),
(39, 1, 4, '', '', '', ''),
(39, 1, 5, '', '', '', ''),
(39, 1, 6, '', '', '', ''),
(40, 1, 1, '', '', '', ''),
(40, 1, 2, '', '', '', ''),
(40, 1, 3, '', '', '', ''),
(40, 1, 4, '', '', '', ''),
(40, 1, 5, '', '', '', ''),
(40, 1, 6, '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_module`
--

DROP TABLE IF EXISTS `ps_module`;
CREATE TABLE IF NOT EXISTS `ps_module` (
  `id_module` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `version` varchar(8) NOT NULL,
  PRIMARY KEY (`id_module`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_module`
--

INSERT INTO `ps_module` (`id_module`, `name`, `active`, `version`) VALUES
(1, 'blockwishlist', 1, '2.1.0'),
(2, 'contactform', 1, '4.3.0'),
(3, 'dashactivity', 1, '2.0.2'),
(4, 'dashtrends', 1, '2.0.3'),
(5, 'dashgoals', 1, '2.0.2'),
(6, 'dashproducts', 1, '2.1.1'),
(7, 'graphnvd3', 1, '2.0.2'),
(8, 'gridhtml', 1, '2.0.2'),
(9, 'gsitemap', 1, '4.2.0'),
(10, 'pagesnotfound', 1, '2.0.2'),
(11, 'productcomments', 1, '5.0.1'),
(12, 'ps_banner', 1, '2.1.2'),
(13, 'ps_categorytree', 1, '2.0.2'),
(14, 'ps_checkpayment', 1, '2.0.5'),
(15, 'ps_contactinfo', 1, '3.3.0'),
(16, 'ps_crossselling', 1, '2.0.1'),
(17, 'ps_currencyselector', 1, '2.0.1'),
(18, 'ps_customeraccountlinks', 1, '3.1.1'),
(19, 'ps_customersignin', 1, '2.0.4'),
(20, 'ps_customtext', 1, '4.2.0'),
(21, 'ps_dataprivacy', 1, '2.1.0'),
(22, 'ps_emailsubscription', 1, '2.7.0'),
(24, 'ps_faviconnotificationbo', 1, '2.1.1'),
(25, 'ps_featuredproducts', 1, '2.1.2'),
(26, 'ps_imageslider', 1, '3.1.1'),
(27, 'ps_languageselector', 1, '2.1.0'),
(28, 'ps_linklist', 1, '5.0.4'),
(29, 'ps_mainmenu', 1, '2.3.1'),
(30, 'ps_searchbar', 1, '2.1.3'),
(31, 'ps_sharebuttons', 1, '2.1.1'),
(32, 'ps_shoppingcart', 1, '2.0.5'),
(33, 'ps_socialfollow', 1, '2.2.0'),
(34, 'ps_themecusto', 1, '1.2.1'),
(35, 'ps_wirepayment', 1, '2.1.1'),
(36, 'statsbestcategories', 1, '2.0.1'),
(37, 'statsbestcustomers', 1, '2.0.3'),
(38, 'statsbestproducts', 1, '2.0.1'),
(39, 'statsbestsuppliers', 1, '2.0.0'),
(40, 'statsbestvouchers', 1, '2.0.1'),
(41, 'statscarrier', 1, '2.0.1'),
(42, 'statscatalog', 1, '2.0.2'),
(43, 'statscheckup', 1, '2.0.2'),
(44, 'statsdata', 1, '2.1.0'),
(45, 'statsforecast', 1, '2.0.4'),
(46, 'statsnewsletter', 1, '2.0.3'),
(47, 'statspersonalinfos', 1, '2.0.4'),
(48, 'statsproduct', 1, '2.1.1'),
(49, 'statsregistrations', 1, '2.0.1'),
(50, 'statssales', 1, '2.1.0'),
(51, 'statssearch', 1, '2.0.2'),
(52, 'statsstock', 1, '2.0.0'),
(53, 'welcome', 1, '6.0.7'),
(54, 'gamification', 1, '2.5.0'),
(55, 'psaddonsconnect', 1, '2.1.0'),
(56, 'psgdpr', 1, '1.4.2'),
(57, 'ps_mbo', 1, '2.0.1'),
(58, 'ps_buybuttonlite', 1, '1.0.1'),
(59, 'ps_checkout', 1, '2.20.2'),
(60, 'ps_metrics', 1, '3.5.4'),
(61, 'ps_facebook', 1, '1.19.0'),
(62, 'psxmarketingwithgoogle', 1, '1.30.0'),
(63, 'blockreassurance', 1, '5.1.0'),
(64, 'ps_facetedsearch', 1, '3.8.0'),
(65, 'ps_viewedproduct', 1, '1.2.2'),
(66, 'myshophelper', 1, '1.1.3'),
(67, 'crazyelements', 1, '1.0.4'),
(68, 'smartsupp', 1, '2.1.9');

-- --------------------------------------------------------

--
-- Structure de la table `ps_module_access`
--

DROP TABLE IF EXISTS `ps_module_access`;
CREATE TABLE IF NOT EXISTS `ps_module_access` (
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_authorization_role` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_profile`,`id_authorization_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_module_access`
--

INSERT INTO `ps_module_access` (`id_profile`, `id_authorization_role`) VALUES
(1, 497),
(1, 498),
(1, 499),
(1, 500),
(1, 501),
(1, 502),
(1, 503),
(1, 504),
(1, 505),
(1, 506),
(1, 507),
(1, 508),
(1, 509),
(1, 510),
(1, 511),
(1, 512),
(1, 517),
(1, 518),
(1, 519),
(1, 520),
(1, 521),
(1, 522),
(1, 523),
(1, 524),
(1, 525),
(1, 526),
(1, 527),
(1, 528),
(1, 529),
(1, 530),
(1, 531),
(1, 532),
(1, 533),
(1, 534),
(1, 535),
(1, 536),
(1, 537),
(1, 538),
(1, 539),
(1, 540),
(1, 541),
(1, 542),
(1, 543),
(1, 544),
(1, 545),
(1, 546),
(1, 547),
(1, 548),
(1, 549),
(1, 550),
(1, 551),
(1, 552),
(1, 553),
(1, 554),
(1, 555),
(1, 556),
(1, 557),
(1, 558),
(1, 559),
(1, 560),
(1, 561),
(1, 562),
(1, 563),
(1, 564),
(1, 565),
(1, 566),
(1, 567),
(1, 568),
(1, 569),
(1, 570),
(1, 571),
(1, 572),
(1, 573),
(1, 574),
(1, 575),
(1, 576),
(1, 577),
(1, 578),
(1, 579),
(1, 580),
(1, 581),
(1, 582),
(1, 583),
(1, 584),
(1, 585),
(1, 586),
(1, 587),
(1, 588),
(1, 593),
(1, 594),
(1, 595),
(1, 596),
(1, 601),
(1, 602),
(1, 603),
(1, 604),
(1, 605),
(1, 606),
(1, 607),
(1, 608),
(1, 609),
(1, 610),
(1, 611),
(1, 612),
(1, 613),
(1, 614),
(1, 615),
(1, 616),
(1, 617),
(1, 618),
(1, 619),
(1, 620),
(1, 621),
(1, 622),
(1, 623),
(1, 624),
(1, 625),
(1, 626),
(1, 627),
(1, 628),
(1, 629),
(1, 630),
(1, 631),
(1, 632),
(1, 633),
(1, 634),
(1, 635),
(1, 636),
(1, 637),
(1, 638),
(1, 639),
(1, 640),
(1, 653),
(1, 654),
(1, 655),
(1, 656),
(1, 657),
(1, 658),
(1, 659),
(1, 660),
(1, 661),
(1, 662),
(1, 663),
(1, 664),
(1, 665),
(1, 666),
(1, 667),
(1, 668),
(1, 669),
(1, 670),
(1, 671),
(1, 672),
(1, 673),
(1, 674),
(1, 675),
(1, 676),
(1, 677),
(1, 678),
(1, 679),
(1, 680),
(1, 681),
(1, 682),
(1, 683),
(1, 684),
(1, 685),
(1, 686),
(1, 687),
(1, 688),
(1, 689),
(1, 690),
(1, 691),
(1, 692),
(1, 693),
(1, 694),
(1, 695),
(1, 696),
(1, 697),
(1, 698),
(1, 699),
(1, 700),
(1, 701),
(1, 702),
(1, 703),
(1, 704),
(1, 705),
(1, 706),
(1, 707),
(1, 708),
(1, 709),
(1, 710),
(1, 711),
(1, 712),
(1, 713),
(1, 714),
(1, 715),
(1, 716),
(1, 717),
(1, 718),
(1, 719),
(1, 720),
(1, 721),
(1, 722),
(1, 723),
(1, 724),
(1, 725),
(1, 726),
(1, 727),
(1, 728),
(1, 737),
(1, 738),
(1, 739),
(1, 740),
(1, 741),
(1, 742),
(1, 743),
(1, 744),
(1, 745),
(1, 746),
(1, 747),
(1, 748),
(1, 757),
(1, 758),
(1, 759),
(1, 760),
(1, 777),
(1, 778),
(1, 779),
(1, 780),
(1, 785),
(1, 786),
(1, 787),
(1, 788),
(1, 797),
(1, 798),
(1, 799),
(1, 800),
(1, 821),
(1, 822),
(1, 823),
(1, 824),
(1, 833),
(1, 834),
(1, 835),
(1, 836),
(1, 837),
(1, 838),
(1, 839),
(1, 840),
(1, 845),
(1, 846),
(1, 847),
(1, 848),
(1, 849),
(1, 850),
(1, 851),
(1, 852),
(1, 857),
(1, 858),
(1, 859),
(1, 860),
(1, 921),
(1, 922),
(1, 923),
(1, 924),
(1, 933),
(1, 934),
(1, 935),
(1, 936);

-- --------------------------------------------------------

--
-- Structure de la table `ps_module_carrier`
--

DROP TABLE IF EXISTS `ps_module_carrier`;
CREATE TABLE IF NOT EXISTS `ps_module_carrier` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_reference` int(11) NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_module_carrier`
--

INSERT INTO `ps_module_carrier` (`id_module`, `id_shop`, `id_reference`) VALUES
(14, 1, 1),
(14, 1, 2),
(14, 1, 3),
(14, 1, 4),
(35, 1, 1),
(35, 1, 2),
(35, 1, 3),
(35, 1, 4),
(59, 1, 1),
(59, 1, 2),
(59, 1, 3),
(59, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `ps_module_country`
--

DROP TABLE IF EXISTS `ps_module_country`;
CREATE TABLE IF NOT EXISTS `ps_module_country` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_country` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_module_country`
--

INSERT INTO `ps_module_country` (`id_module`, `id_shop`, `id_country`) VALUES
(14, 1, 3),
(35, 1, 3),
(59, 1, 1),
(59, 1, 2),
(59, 1, 3),
(59, 1, 4),
(59, 1, 5),
(59, 1, 6),
(59, 1, 7),
(59, 1, 8),
(59, 1, 9),
(59, 1, 10),
(59, 1, 11),
(59, 1, 12),
(59, 1, 13),
(59, 1, 14),
(59, 1, 15),
(59, 1, 16),
(59, 1, 17),
(59, 1, 18),
(59, 1, 19),
(59, 1, 20),
(59, 1, 21),
(59, 1, 22),
(59, 1, 23),
(59, 1, 24),
(59, 1, 25),
(59, 1, 26),
(59, 1, 27),
(59, 1, 28),
(59, 1, 29),
(59, 1, 30),
(59, 1, 31),
(59, 1, 32),
(59, 1, 33),
(59, 1, 34),
(59, 1, 35),
(59, 1, 36),
(59, 1, 37),
(59, 1, 38),
(59, 1, 40),
(59, 1, 41),
(59, 1, 42),
(59, 1, 43),
(59, 1, 44),
(59, 1, 45),
(59, 1, 46),
(59, 1, 47),
(59, 1, 48),
(59, 1, 49),
(59, 1, 51),
(59, 1, 52),
(59, 1, 53),
(59, 1, 54),
(59, 1, 55),
(59, 1, 56),
(59, 1, 57),
(59, 1, 58),
(59, 1, 59),
(59, 1, 60),
(59, 1, 62),
(59, 1, 63),
(59, 1, 64),
(59, 1, 65),
(59, 1, 67),
(59, 1, 68),
(59, 1, 69),
(59, 1, 70),
(59, 1, 71),
(59, 1, 72),
(59, 1, 73),
(59, 1, 74),
(59, 1, 76),
(59, 1, 77),
(59, 1, 78),
(59, 1, 79),
(59, 1, 81),
(59, 1, 82),
(59, 1, 83),
(59, 1, 85),
(59, 1, 86),
(59, 1, 87),
(59, 1, 88),
(59, 1, 89),
(59, 1, 90),
(59, 1, 91),
(59, 1, 92),
(59, 1, 93),
(59, 1, 95),
(59, 1, 96),
(59, 1, 97),
(59, 1, 98),
(59, 1, 100),
(59, 1, 102),
(59, 1, 103),
(59, 1, 104),
(59, 1, 106),
(59, 1, 107),
(59, 1, 108),
(59, 1, 109),
(59, 1, 110),
(59, 1, 114),
(59, 1, 116),
(59, 1, 117),
(59, 1, 118),
(59, 1, 119),
(59, 1, 121),
(59, 1, 122),
(59, 1, 123),
(59, 1, 124),
(59, 1, 126),
(59, 1, 129),
(59, 1, 130),
(59, 1, 132),
(59, 1, 133),
(59, 1, 134),
(59, 1, 135),
(59, 1, 136),
(59, 1, 137),
(59, 1, 138),
(59, 1, 139),
(59, 1, 140),
(59, 1, 141),
(59, 1, 142),
(59, 1, 143),
(59, 1, 144),
(59, 1, 145),
(59, 1, 146),
(59, 1, 147),
(59, 1, 148),
(59, 1, 149),
(59, 1, 150),
(59, 1, 151),
(59, 1, 152),
(59, 1, 153),
(59, 1, 154),
(59, 1, 155),
(59, 1, 156),
(59, 1, 157),
(59, 1, 158),
(59, 1, 159),
(59, 1, 160),
(59, 1, 162),
(59, 1, 164),
(59, 1, 166),
(59, 1, 167),
(59, 1, 168),
(59, 1, 169),
(59, 1, 170),
(59, 1, 171),
(59, 1, 173),
(59, 1, 174),
(59, 1, 175),
(59, 1, 176),
(59, 1, 178),
(59, 1, 179),
(59, 1, 181),
(59, 1, 182),
(59, 1, 183),
(59, 1, 184),
(59, 1, 185),
(59, 1, 186),
(59, 1, 187),
(59, 1, 188),
(59, 1, 189),
(59, 1, 190),
(59, 1, 191),
(59, 1, 192),
(59, 1, 193),
(59, 1, 195),
(59, 1, 197),
(59, 1, 198),
(59, 1, 199),
(59, 1, 201),
(59, 1, 202),
(59, 1, 203),
(59, 1, 204),
(59, 1, 206),
(59, 1, 207),
(59, 1, 208),
(59, 1, 210),
(59, 1, 211),
(59, 1, 212),
(59, 1, 213),
(59, 1, 214),
(59, 1, 215),
(59, 1, 216),
(59, 1, 218),
(59, 1, 219),
(59, 1, 220),
(59, 1, 221),
(59, 1, 223),
(59, 1, 225),
(59, 1, 226),
(59, 1, 227),
(59, 1, 228),
(59, 1, 231),
(59, 1, 233),
(59, 1, 234),
(59, 1, 237),
(59, 1, 238),
(59, 1, 239);

-- --------------------------------------------------------

--
-- Structure de la table `ps_module_currency`
--

DROP TABLE IF EXISTS `ps_module_currency`;
CREATE TABLE IF NOT EXISTS `ps_module_currency` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_currency` int(11) NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_currency`),
  KEY `id_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_module_currency`
--

INSERT INTO `ps_module_currency` (`id_module`, `id_shop`, `id_currency`) VALUES
(14, 1, 1),
(35, 1, 1),
(59, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_module_group`
--

DROP TABLE IF EXISTS `ps_module_group`;
CREATE TABLE IF NOT EXISTS `ps_module_group` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_group` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_module_group`
--

INSERT INTO `ps_module_group` (`id_module`, `id_shop`, `id_group`) VALUES
(1, 1, 1),
(1, 1, 2),
(1, 1, 3),
(2, 1, 1),
(2, 1, 2),
(2, 1, 3),
(3, 1, 1),
(3, 1, 2),
(3, 1, 3),
(4, 1, 1),
(4, 1, 2),
(4, 1, 3),
(5, 1, 1),
(5, 1, 2),
(5, 1, 3),
(6, 1, 1),
(6, 1, 2),
(6, 1, 3),
(7, 1, 1),
(7, 1, 2),
(7, 1, 3),
(8, 1, 1),
(8, 1, 2),
(8, 1, 3),
(9, 1, 1),
(9, 1, 2),
(9, 1, 3),
(10, 1, 1),
(10, 1, 2),
(10, 1, 3),
(11, 1, 1),
(11, 1, 2),
(11, 1, 3),
(12, 1, 1),
(12, 1, 2),
(12, 1, 3),
(13, 1, 1),
(13, 1, 2),
(13, 1, 3),
(14, 1, 1),
(14, 1, 2),
(14, 1, 3),
(15, 1, 1),
(15, 1, 2),
(15, 1, 3),
(16, 1, 1),
(16, 1, 2),
(16, 1, 3),
(17, 1, 1),
(17, 1, 2),
(17, 1, 3),
(18, 1, 1),
(18, 1, 2),
(18, 1, 3),
(19, 1, 1),
(19, 1, 2),
(19, 1, 3),
(20, 1, 1),
(20, 1, 2),
(20, 1, 3),
(21, 1, 1),
(21, 1, 2),
(21, 1, 3),
(22, 1, 1),
(22, 1, 2),
(22, 1, 3),
(24, 1, 1),
(24, 1, 2),
(24, 1, 3),
(25, 1, 1),
(25, 1, 2),
(25, 1, 3),
(26, 1, 1),
(26, 1, 2),
(26, 1, 3),
(27, 1, 1),
(27, 1, 2),
(27, 1, 3),
(28, 1, 1),
(28, 1, 2),
(28, 1, 3),
(29, 1, 1),
(29, 1, 2),
(29, 1, 3),
(30, 1, 1),
(30, 1, 2),
(30, 1, 3),
(31, 1, 1),
(31, 1, 2),
(31, 1, 3),
(32, 1, 1),
(32, 1, 2),
(32, 1, 3),
(33, 1, 1),
(33, 1, 2),
(33, 1, 3),
(34, 1, 1),
(34, 1, 2),
(34, 1, 3),
(35, 1, 1),
(35, 1, 2),
(35, 1, 3),
(36, 1, 1),
(36, 1, 2),
(36, 1, 3),
(37, 1, 1),
(37, 1, 2),
(37, 1, 3),
(38, 1, 1),
(38, 1, 2),
(38, 1, 3),
(39, 1, 1),
(39, 1, 2),
(39, 1, 3),
(40, 1, 1),
(40, 1, 2),
(40, 1, 3),
(41, 1, 1),
(41, 1, 2),
(41, 1, 3),
(42, 1, 1),
(42, 1, 2),
(42, 1, 3),
(43, 1, 1),
(43, 1, 2),
(43, 1, 3),
(44, 1, 1),
(44, 1, 2),
(44, 1, 3),
(45, 1, 1),
(45, 1, 2),
(45, 1, 3),
(46, 1, 1),
(46, 1, 2),
(46, 1, 3),
(47, 1, 1),
(47, 1, 2),
(47, 1, 3),
(48, 1, 1),
(48, 1, 2),
(48, 1, 3),
(49, 1, 1),
(49, 1, 2),
(49, 1, 3),
(50, 1, 1),
(50, 1, 2),
(50, 1, 3),
(51, 1, 1),
(51, 1, 2),
(51, 1, 3),
(52, 1, 1),
(52, 1, 2),
(52, 1, 3),
(53, 1, 1),
(53, 1, 2),
(53, 1, 3),
(54, 1, 1),
(54, 1, 2),
(54, 1, 3),
(55, 1, 1),
(55, 1, 2),
(55, 1, 3),
(56, 1, 1),
(56, 1, 2),
(56, 1, 3),
(57, 1, 1),
(57, 1, 2),
(57, 1, 3),
(58, 1, 1),
(58, 1, 2),
(58, 1, 3),
(59, 1, 1),
(59, 1, 2),
(59, 1, 3),
(60, 1, 1),
(60, 1, 2),
(60, 1, 3),
(61, 1, 1),
(61, 1, 2),
(61, 1, 3),
(62, 1, 1),
(62, 1, 2),
(62, 1, 3),
(63, 1, 1),
(63, 1, 2),
(63, 1, 3),
(64, 1, 1),
(64, 1, 2),
(64, 1, 3),
(65, 1, 1),
(65, 1, 2),
(65, 1, 3),
(66, 1, 1),
(66, 1, 2),
(66, 1, 3),
(67, 1, 1),
(67, 1, 2),
(67, 1, 3),
(68, 1, 1),
(68, 1, 2),
(68, 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `ps_module_history`
--

DROP TABLE IF EXISTS `ps_module_history`;
CREATE TABLE IF NOT EXISTS `ps_module_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_employee` int(11) NOT NULL,
  `id_module` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_module_history`
--

INSERT INTO `ps_module_history` (`id`, `id_employee`, `id_module`, `date_add`, `date_upd`) VALUES
(1, 1, 14, '2022-12-05 14:25:06', '2022-12-05 14:25:06'),
(2, 1, 68, '2022-12-11 16:19:10', '2022-12-11 16:19:10'),
(3, 1, 12, '2022-12-11 16:49:36', '2022-12-11 16:49:36');

-- --------------------------------------------------------

--
-- Structure de la table `ps_module_preference`
--

DROP TABLE IF EXISTS `ps_module_preference`;
CREATE TABLE IF NOT EXISTS `ps_module_preference` (
  `id_module_preference` int(11) NOT NULL AUTO_INCREMENT,
  `id_employee` int(11) NOT NULL,
  `module` varchar(191) NOT NULL,
  `interest` tinyint(1) DEFAULT NULL,
  `favorite` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_module_preference`),
  UNIQUE KEY `employee_module` (`id_employee`,`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_module_shop`
--

DROP TABLE IF EXISTS `ps_module_shop`;
CREATE TABLE IF NOT EXISTS `ps_module_shop` (
  `id_module` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `enable_device` tinyint(1) NOT NULL DEFAULT '7',
  PRIMARY KEY (`id_module`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_module_shop`
--

INSERT INTO `ps_module_shop` (`id_module`, `id_shop`, `enable_device`) VALUES
(1, 1, 7),
(2, 1, 7),
(3, 1, 7),
(4, 1, 7),
(5, 1, 7),
(6, 1, 7),
(7, 1, 7),
(8, 1, 7),
(9, 1, 7),
(10, 1, 7),
(11, 1, 7),
(12, 1, 3),
(13, 1, 7),
(14, 1, 7),
(15, 1, 7),
(16, 1, 7),
(17, 1, 7),
(18, 1, 7),
(19, 1, 7),
(20, 1, 7),
(21, 1, 7),
(22, 1, 7),
(24, 1, 7),
(25, 1, 7),
(26, 1, 3),
(27, 1, 7),
(28, 1, 7),
(29, 1, 7),
(30, 1, 7),
(31, 1, 7),
(32, 1, 7),
(33, 1, 7),
(34, 1, 7),
(35, 1, 7),
(36, 1, 7),
(37, 1, 7),
(38, 1, 7),
(39, 1, 7),
(40, 1, 7),
(41, 1, 7),
(42, 1, 7),
(43, 1, 7),
(44, 1, 7),
(45, 1, 7),
(46, 1, 7),
(47, 1, 7),
(48, 1, 7),
(49, 1, 7),
(50, 1, 7),
(51, 1, 7),
(52, 1, 7),
(53, 1, 7),
(54, 1, 7),
(55, 1, 7),
(56, 1, 7),
(57, 1, 7),
(58, 1, 7),
(59, 1, 7),
(60, 1, 7),
(61, 1, 7),
(62, 1, 7),
(63, 1, 7),
(64, 1, 7),
(65, 1, 7),
(66, 1, 7),
(67, 1, 7),
(68, 1, 7);

-- --------------------------------------------------------

--
-- Structure de la table `ps_operating_system`
--

DROP TABLE IF EXISTS `ps_operating_system`;
CREATE TABLE IF NOT EXISTS `ps_operating_system` (
  `id_operating_system` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_operating_system`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_operating_system`
--

INSERT INTO `ps_operating_system` (`id_operating_system`, `name`) VALUES
(1, 'Windows XP'),
(2, 'Windows Vista'),
(3, 'Windows 7'),
(4, 'Windows 8'),
(5, 'Windows 8.1'),
(6, 'Windows 10'),
(7, 'MacOsX'),
(8, 'Linux'),
(9, 'Android');

-- --------------------------------------------------------

--
-- Structure de la table `ps_orders`
--

DROP TABLE IF EXISTS `ps_orders`;
CREATE TABLE IF NOT EXISTS `ps_orders` (
  `id_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `reference` varchar(9) DEFAULT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL,
  `id_address_invoice` int(10) UNSIGNED NOT NULL,
  `current_state` int(10) UNSIGNED NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `payment` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `module` varchar(255) DEFAULT NULL,
  `recyclable` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_message` text,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_number` varchar(64) DEFAULT NULL,
  `total_discounts` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_real` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products_wt` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `carrier_tax_rate` decimal(10,3) NOT NULL DEFAULT '0.000',
  `total_wrapping` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `round_mode` tinyint(1) NOT NULL DEFAULT '2',
  `round_type` tinyint(1) NOT NULL DEFAULT '1',
  `invoice_number` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `delivery_number` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `invoice_date` datetime NOT NULL,
  `delivery_date` datetime NOT NULL,
  `valid` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `note` text,
  PRIMARY KEY (`id_order`),
  KEY `reference` (`reference`),
  KEY `id_customer` (`id_customer`),
  KEY `id_cart` (`id_cart`),
  KEY `invoice_number` (`invoice_number`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_lang` (`id_lang`),
  KEY `id_currency` (`id_currency`),
  KEY `id_address_delivery` (`id_address_delivery`),
  KEY `id_address_invoice` (`id_address_invoice`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `current_state` (`current_state`),
  KEY `id_shop` (`id_shop`),
  KEY `date_add` (`date_add`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_orders`
--

INSERT INTO `ps_orders` (`id_order`, `reference`, `id_shop_group`, `id_shop`, `id_carrier`, `id_lang`, `id_customer`, `id_cart`, `id_currency`, `id_address_delivery`, `id_address_invoice`, `current_state`, `secure_key`, `payment`, `conversion_rate`, `module`, `recyclable`, `gift`, `gift_message`, `mobile_theme`, `shipping_number`, `total_discounts`, `total_discounts_tax_incl`, `total_discounts_tax_excl`, `total_paid`, `total_paid_tax_incl`, `total_paid_tax_excl`, `total_paid_real`, `total_products`, `total_products_wt`, `total_shipping`, `total_shipping_tax_incl`, `total_shipping_tax_excl`, `carrier_tax_rate`, `total_wrapping`, `total_wrapping_tax_incl`, `total_wrapping_tax_excl`, `round_mode`, `round_type`, `invoice_number`, `delivery_number`, `invoice_date`, `delivery_date`, `valid`, `date_add`, `date_upd`, `note`) VALUES
(1, 'XKBKNABJK', 1, 1, 2, 1, 2, 1, 1, 5, 5, 6, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'ps_checkpayment', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '61.800000', '68.200000', '66.800000', '0.000000', '59.800000', '59.800000', '7.000000', '8.400000', '7.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2022-11-21 11:07:26', '2022-11-21 11:07:26', 'Test'),
(2, 'OHSATSERP', 1, 1, 2, 1, 2, 2, 1, 5, 5, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'ps_checkpayment', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '169.900000', '169.900000', '169.900000', '0.000000', '169.900000', '169.900000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2022-11-21 11:07:26', '2022-11-21 11:07:26', ''),
(3, 'UOYEVOLI', 1, 1, 2, 1, 2, 3, 1, 5, 5, 8, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'ps_checkpayment', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '14.900000', '21.300000', '19.900000', '0.000000', '12.900000', '12.900000', '7.000000', '8.400000', '7.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2022-11-21 11:07:26', '2022-11-21 11:07:26', ''),
(4, 'FFATNOMMJ', 1, 1, 2, 1, 2, 4, 1, 5, 5, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'ps_checkpayment', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '14.900000', '21.300000', '19.900000', '0.000000', '12.900000', '12.900000', '7.000000', '8.400000', '7.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2022-11-21 11:07:26', '2022-11-21 11:07:26', ''),
(5, 'KHWLILZLL', 1, 1, 2, 1, 2, 5, 1, 5, 5, 10, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Bank wire', '1.000000', 'ps_wirepayment', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '20.900000', '27.300000', '25.900000', '0.000000', '18.900000', '18.900000', '7.000000', '8.400000', '7.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2022-11-21 11:07:26', '2022-11-21 11:07:26', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_carrier`
--

DROP TABLE IF EXISTS `ps_order_carrier`;
CREATE TABLE IF NOT EXISTS `ps_order_carrier` (
  `id_order_carrier` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) UNSIGNED NOT NULL,
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_order_invoice` int(11) UNSIGNED DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  `shipping_cost_tax_excl` decimal(20,6) DEFAULT NULL,
  `shipping_cost_tax_incl` decimal(20,6) DEFAULT NULL,
  `tracking_number` varchar(64) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_carrier`),
  KEY `id_order` (`id_order`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_order_invoice` (`id_order_invoice`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_order_carrier`
--

INSERT INTO `ps_order_carrier` (`id_order_carrier`, `id_order`, `id_carrier`, `id_order_invoice`, `weight`, `shipping_cost_tax_excl`, `shipping_cost_tax_incl`, `tracking_number`, `date_add`) VALUES
(1, 1, 2, 0, '0.000000', '7.000000', '8.400000', '', '2022-11-21 11:07:26'),
(2, 2, 2, 0, '0.000000', '7.000000', '8.400000', '', '2022-11-21 11:07:26'),
(3, 3, 2, 0, '0.000000', '7.000000', '8.400000', '', '2022-11-21 11:07:26'),
(4, 4, 2, 0, '0.000000', '7.000000', '8.400000', '', '2022-11-21 11:07:26'),
(5, 5, 2, 0, '0.000000', '7.000000', '8.400000', '', '2022-11-21 11:07:26');

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_cart_rule`
--

DROP TABLE IF EXISTS `ps_order_cart_rule`;
CREATE TABLE IF NOT EXISTS `ps_order_cart_rule` (
  `id_order_cart_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_order_invoice` int(10) UNSIGNED DEFAULT '0',
  `name` varchar(254) NOT NULL,
  `value` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `value_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_cart_rule`),
  KEY `id_order` (`id_order`),
  KEY `id_cart_rule` (`id_cart_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_detail`
--

DROP TABLE IF EXISTS `ps_order_detail`;
CREATE TABLE IF NOT EXISTS `ps_order_detail` (
  `id_order_detail` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_order_invoice` int(11) DEFAULT NULL,
  `id_warehouse` int(10) UNSIGNED DEFAULT '0',
  `id_shop` int(11) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_attribute_id` int(10) UNSIGNED DEFAULT NULL,
  `id_customization` int(10) UNSIGNED DEFAULT '0',
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity_in_stock` int(10) NOT NULL DEFAULT '0',
  `product_quantity_refunded` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity_return` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity_reinjected` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_percent` decimal(5,2) NOT NULL DEFAULT '0.00',
  `reduction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_amount_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_amount_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `group_reduction` decimal(5,2) NOT NULL DEFAULT '0.00',
  `product_quantity_discount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `product_ean13` varchar(13) DEFAULT NULL,
  `product_isbn` varchar(32) DEFAULT NULL,
  `product_upc` varchar(12) DEFAULT NULL,
  `product_mpn` varchar(40) DEFAULT NULL,
  `product_reference` varchar(64) DEFAULT NULL,
  `product_supplier_reference` varchar(64) DEFAULT NULL,
  `product_weight` decimal(20,6) NOT NULL,
  `id_tax_rules_group` int(11) UNSIGNED DEFAULT '0',
  `tax_computation_method` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `tax_name` varchar(16) NOT NULL,
  `tax_rate` decimal(10,3) NOT NULL DEFAULT '0.000',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `ecotax_tax_rate` decimal(5,3) NOT NULL DEFAULT '0.000',
  `discount_quantity_applied` tinyint(1) NOT NULL DEFAULT '0',
  `download_hash` varchar(255) DEFAULT NULL,
  `download_nb` int(10) UNSIGNED DEFAULT '0',
  `download_deadline` datetime DEFAULT NULL,
  `total_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `purchase_supplier_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `original_product_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `original_wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_refunded_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_refunded_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id_order_detail`),
  KEY `order_detail_order` (`id_order`),
  KEY `product_id` (`product_id`,`product_attribute_id`),
  KEY `product_attribute_id` (`product_attribute_id`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `id_order_id_order_detail` (`id_order`,`id_order_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_order_detail`
--

INSERT INTO `ps_order_detail` (`id_order_detail`, `id_order`, `id_order_invoice`, `id_warehouse`, `id_shop`, `product_id`, `product_attribute_id`, `id_customization`, `product_name`, `product_quantity`, `product_quantity_in_stock`, `product_quantity_refunded`, `product_quantity_return`, `product_quantity_reinjected`, `product_price`, `reduction_percent`, `reduction_amount`, `reduction_amount_tax_incl`, `reduction_amount_tax_excl`, `group_reduction`, `product_quantity_discount`, `product_ean13`, `product_isbn`, `product_upc`, `product_mpn`, `product_reference`, `product_supplier_reference`, `product_weight`, `id_tax_rules_group`, `tax_computation_method`, `tax_name`, `tax_rate`, `ecotax`, `ecotax_tax_rate`, `discount_quantity_applied`, `download_hash`, `download_nb`, `download_deadline`, `total_price_tax_incl`, `total_price_tax_excl`, `unit_price_tax_incl`, `unit_price_tax_excl`, `total_shipping_price_tax_incl`, `total_shipping_price_tax_excl`, `purchase_supplier_price`, `original_product_price`, `original_wholesale_price`, `total_refunded_tax_excl`, `total_refunded_tax_incl`) VALUES
(1, 1, 0, 0, 1, 1, 1, 0, 'Hummingbird printed t-shirt - Color : White, Size : S', 1, 1, 0, 0, 0, '23.900000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', 'demo_1', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '23.900000', '23.900000', '23.900000', '23.900000', '0.000000', '0.000000', '0.000000', '23.900000', '0.000000', '0.000000', '0.000000'),
(2, 1, 0, 0, 1, 2, 9, 0, 'Hummingbird printed sweater - Color : White, Size : S', 1, 1, 0, 0, 0, '35.900000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', 'demo_3', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '35.900000', '35.900000', '35.900000', '35.900000', '0.000000', '0.000000', '0.000000', '35.900000', '0.000000', '0.000000', '0.000000'),
(3, 2, 0, 0, 1, 4, 18, 0, 'The adventure begins Framed poster - Size : 80x120cm', 2, 3, 0, 0, 0, '79.000000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', 'demo_5', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '158.000000', '79.000000', '79.000000', '79.000000', '0.000000', '0.000000', '0.000000', '79.000000', '0.000000', '0.000000', '0.000000'),
(4, 2, 0, 0, 1, 8, 0, 0, 'Mug Today is a good day', 1, 1, 0, 0, 0, '11.900000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', 'demo_13', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '11.900000', '11.900000', '11.900000', '11.900000', '0.000000', '0.000000', '0.000000', '11.900000', '0.000000', '0.000000', '0.000000'),
(5, 3, 0, 0, 1, 16, 28, 0, 'Mountain fox notebook Style : Ruled', 1, 1, 0, 0, 0, '12.900000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', 'demo_8', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '12.900000', '12.900000', '12.900000', '12.900000', '0.000000', '0.000000', '0.000000', '12.900000', '0.000000', '0.000000', '0.000000'),
(6, 4, 0, 0, 1, 16, 29, 0, 'Mountain fox notebook Style : Plain', 1, 1, 0, 0, 0, '12.900000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', 'demo_8', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '12.900000', '12.900000', '12.900000', '12.900000', '0.000000', '0.000000', '0.000000', '12.900000', '0.000000', '0.000000', '0.000000'),
(7, 5, 0, 0, 1, 10, 25, 0, 'Brown bear cushion Color : Black', 1, 1, 0, 0, 0, '18.900000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', 'demo_16', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '18.900000', '18.900000', '18.900000', '18.900000', '0.000000', '0.000000', '0.000000', '18.900000', '0.000000', '0.000000', '0.000000');

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_detail_tax`
--

DROP TABLE IF EXISTS `ps_order_detail_tax`;
CREATE TABLE IF NOT EXISTS `ps_order_detail_tax` (
  `id_order_detail` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `unit_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  KEY `id_order_detail` (`id_order_detail`),
  KEY `id_tax` (`id_tax`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_history`
--

DROP TABLE IF EXISTS `ps_order_history`;
CREATE TABLE IF NOT EXISTS `ps_order_history` (
  `id_order_history` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_history`),
  KEY `order_history_order` (`id_order`),
  KEY `id_employee` (`id_employee`),
  KEY `id_order_state` (`id_order_state`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_order_history`
--

INSERT INTO `ps_order_history` (`id_order_history`, `id_employee`, `id_order`, `id_order_state`, `date_add`) VALUES
(1, 0, 1, 1, '2022-11-21 11:07:26'),
(2, 0, 2, 1, '2022-11-21 11:07:26'),
(3, 0, 3, 1, '2022-11-21 11:07:26'),
(4, 0, 4, 1, '2022-11-21 11:07:26'),
(5, 0, 5, 10, '2022-11-21 11:07:26'),
(6, 1, 1, 6, '2022-11-21 11:07:26'),
(7, 1, 3, 8, '2022-11-21 11:07:26');

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_invoice`
--

DROP TABLE IF EXISTS `ps_order_invoice`;
CREATE TABLE IF NOT EXISTS `ps_order_invoice` (
  `id_order_invoice` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `delivery_number` int(11) NOT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `total_discount_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discount_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products_wt` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `shipping_tax_computation_method` int(10) UNSIGNED NOT NULL,
  `total_wrapping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `shop_address` text,
  `note` text,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_invoice`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_invoice_payment`
--

DROP TABLE IF EXISTS `ps_order_invoice_payment`;
CREATE TABLE IF NOT EXISTS `ps_order_invoice_payment` (
  `id_order_invoice` int(11) UNSIGNED NOT NULL,
  `id_order_payment` int(11) UNSIGNED NOT NULL,
  `id_order` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_order_invoice`,`id_order_payment`),
  KEY `order_payment` (`id_order_payment`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_invoice_tax`
--

DROP TABLE IF EXISTS `ps_order_invoice_tax`;
CREATE TABLE IF NOT EXISTS `ps_order_invoice_tax` (
  `id_order_invoice` int(11) NOT NULL,
  `type` varchar(15) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `amount` decimal(10,6) NOT NULL DEFAULT '0.000000',
  KEY `id_tax` (`id_tax`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_message`
--

DROP TABLE IF EXISTS `ps_order_message`;
CREATE TABLE IF NOT EXISTS `ps_order_message` (
  `id_order_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_message`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_order_message`
--

INSERT INTO `ps_order_message` (`id_order_message`, `date_add`) VALUES
(1, '2022-11-21 11:07:26');

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_message_lang`
--

DROP TABLE IF EXISTS `ps_order_message_lang`;
CREATE TABLE IF NOT EXISTS `ps_order_message_lang` (
  `id_order_message` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id_order_message`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_order_message_lang`
--

INSERT INTO `ps_order_message_lang` (`id_order_message`, `id_lang`, `name`, `message`) VALUES
(1, 1, 'Delay', 'Hi,\n\nUnfortunately, an item on your order is currently out of stock. This may cause a slight delay in delivery.\nPlease accept our apologies and rest assured that we are working hard to rectify this.\n\nBest regards,'),
(1, 2, 'Retard', 'Bonjour,\n\nMalheureusement, un article que vous avez commandé est actuellement en rupture de stock. Pour cette raison, il est possible que la livraison de votre commande soit légèrement retardée.\nNous vous prions de bien vouloir accepter nos excuses. Nous faisons tout notre possible pour remédier à cette situation.\n\nCordialement,'),
(1, 3, 'Lieferung', 'Leider sind ein oder mehrere Artikel Ihrer Bestellung derzeit nicht auf Lager. Dies kann zu einer leichten Verzögerung bei Auslieferung führen. Wir entschuldigen uns und versichern Ihnen, dass Sie Ihre Bestellung schnellstmöglich erhalten.'),
(1, 4, 'Vertraging', 'Hallo, Een van de door u bestelde artikelen is momenteel niet op voorraad. Hierdoor kan de levertijd iets uitlopen. Wij bieden u onze excuses aan en doen er alles aan om u zo snel mogelijk te leveren. Met vriendelijke groet,'),
(1, 5, 'Ritardo', 'Buongiorno,\n\npurtroppo un articolo che hai ordinato è momentaneamente esaurito. Potrebbe pertanto verificarsi un leggero ritardo nella consegna.\nScusandoci per l\'inconveniente, ti assicuriamo che stiamo facendo del nostro meglio per risolverlo.\n\nCordiali saluti,'),
(1, 6, 'Retraso', 'Hola:\n\nDesafortunadamente, un producto de su pedido está fuera de stock en este momento. Esto puede originar un pequeño retraso en el envío. Trabajaremos lo más rápido posible para solucionarlo. Por favor, acepte nuestras disculpas.\n\nSaludos');

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_payment`
--

DROP TABLE IF EXISTS `ps_order_payment`;
CREATE TABLE IF NOT EXISTS `ps_order_payment` (
  `id_order_payment` int(11) NOT NULL AUTO_INCREMENT,
  `order_reference` varchar(9) DEFAULT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `amount` decimal(20,6) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `transaction_id` varchar(254) DEFAULT NULL,
  `card_number` varchar(254) DEFAULT NULL,
  `card_brand` varchar(254) DEFAULT NULL,
  `card_expiration` char(7) DEFAULT NULL,
  `card_holder` varchar(254) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_payment`),
  KEY `order_reference` (`order_reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_return`
--

DROP TABLE IF EXISTS `ps_order_return`;
CREATE TABLE IF NOT EXISTS `ps_order_return` (
  `id_order_return` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `question` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_order_return`),
  KEY `order_return_customer` (`id_customer`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_return_detail`
--

DROP TABLE IF EXISTS `ps_order_return_detail`;
CREATE TABLE IF NOT EXISTS `ps_order_return_detail` (
  `id_order_return` int(10) UNSIGNED NOT NULL,
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `id_customization` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_return`,`id_order_detail`,`id_customization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_return_state`
--

DROP TABLE IF EXISTS `ps_order_return_state`;
CREATE TABLE IF NOT EXISTS `ps_order_return_state` (
  `id_order_return_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `color` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_order_return_state`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_order_return_state`
--

INSERT INTO `ps_order_return_state` (`id_order_return_state`, `color`) VALUES
(1, '#4169E1'),
(2, '#8A2BE2'),
(3, '#32CD32'),
(4, '#DC143C'),
(5, '#108510');

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_return_state_lang`
--

DROP TABLE IF EXISTS `ps_order_return_state_lang`;
CREATE TABLE IF NOT EXISTS `ps_order_return_state_lang` (
  `id_order_return_state` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_order_return_state`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_order_return_state_lang`
--

INSERT INTO `ps_order_return_state_lang` (`id_order_return_state`, `id_lang`, `name`) VALUES
(1, 1, 'Waiting for confirmation'),
(1, 2, 'En attente de confirmation'),
(1, 3, 'Warten auf Bestätigung'),
(1, 4, 'Wacht op bevestiging'),
(1, 5, 'In attesa di conferma'),
(1, 6, 'A la espera de confirmación'),
(2, 1, 'Waiting for package'),
(2, 2, 'En attente du colis'),
(2, 3, 'Sendung erwartet'),
(2, 4, 'Wacht op pakket'),
(2, 5, 'In attesa del pacco'),
(2, 6, 'A la espera del paquete'),
(3, 1, 'Package received'),
(3, 2, 'Colis reçu'),
(3, 3, 'Sendung erhalten'),
(3, 4, 'Pakket ontvangen'),
(3, 5, 'Pacco ricevuto'),
(3, 6, 'Paquete recibido'),
(4, 1, 'Return denied'),
(4, 2, 'Retour refusé'),
(4, 3, 'Rücksendung verweigert'),
(4, 4, 'Retour geweigerd'),
(4, 5, 'Reso rifiutato'),
(4, 6, 'Devolución denegada'),
(5, 1, 'Return completed'),
(5, 2, 'Retour terminé'),
(5, 3, 'Rücksendung abgeschlossen'),
(5, 4, 'Retour voltooid'),
(5, 5, 'Reso completato'),
(5, 6, 'Devolución completada');

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_slip`
--

DROP TABLE IF EXISTS `ps_order_slip`;
CREATE TABLE IF NOT EXISTS `ps_order_slip` (
  `id_order_slip` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `total_products_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_products_tax_incl` decimal(20,6) DEFAULT NULL,
  `total_shipping_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_shipping_tax_incl` decimal(20,6) DEFAULT NULL,
  `shipping_cost` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `shipping_cost_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `partial` tinyint(1) NOT NULL,
  `order_slip_type` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_order_slip`),
  KEY `order_slip_customer` (`id_customer`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_slip_detail`
--

DROP TABLE IF EXISTS `ps_order_slip_detail`;
CREATE TABLE IF NOT EXISTS `ps_order_slip_detail` (
  `id_order_slip` int(10) UNSIGNED NOT NULL,
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `unit_price_tax_excl` decimal(20,6) DEFAULT NULL,
  `unit_price_tax_incl` decimal(20,6) DEFAULT NULL,
  `total_price_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_price_tax_incl` decimal(20,6) DEFAULT NULL,
  `amount_tax_excl` decimal(20,6) DEFAULT NULL,
  `amount_tax_incl` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id_order_slip`,`id_order_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_state`
--

DROP TABLE IF EXISTS `ps_order_state`;
CREATE TABLE IF NOT EXISTS `ps_order_state` (
  `id_order_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice` tinyint(1) UNSIGNED DEFAULT '0',
  `send_email` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `module_name` varchar(255) DEFAULT NULL,
  `color` varchar(32) DEFAULT NULL,
  `unremovable` tinyint(1) UNSIGNED NOT NULL,
  `hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `logable` tinyint(1) NOT NULL DEFAULT '0',
  `delivery` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipped` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `paid` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `pdf_invoice` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `pdf_delivery` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_state`),
  KEY `module_name` (`module_name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_order_state`
--

INSERT INTO `ps_order_state` (`id_order_state`, `invoice`, `send_email`, `module_name`, `color`, `unremovable`, `hidden`, `logable`, `delivery`, `shipped`, `paid`, `pdf_invoice`, `pdf_delivery`, `deleted`) VALUES
(1, 0, 1, 'ps_checkpayment', '#34209E', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 1, 1, '', '#3498D8', 1, 0, 1, 0, 0, 1, 1, 0, 0),
(3, 1, 1, '', '#3498D8', 1, 0, 1, 1, 0, 1, 0, 0, 0),
(4, 1, 1, '', '#01B887', 1, 0, 1, 1, 1, 1, 0, 0, 0),
(5, 1, 0, '', '#01B887', 1, 0, 1, 1, 1, 1, 0, 0, 0),
(6, 0, 1, '', '#2C3E50', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 1, 1, '', '#01B887', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 0, 1, '', '#E74C3C', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 1, 1, '', '#3498D8', 1, 0, 0, 0, 0, 1, 0, 0, 0),
(10, 0, 1, 'ps_wirepayment', '#34209E', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 1, 1, '', '#3498D8', 1, 0, 1, 0, 0, 1, 0, 0, 0),
(12, 0, 1, '', '#34209E', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(13, 0, 0, 'ps_cashondelivery', '#34209E', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 0, 0, 'ps_checkout', '#34209E', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 0, 0, 'ps_checkout', '#34209E', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 0, 0, 'ps_checkout', '#34209E', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(17, 0, 0, 'ps_checkout', '#3498D8', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(18, 0, 0, 'ps_checkout', '#01B887', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(19, 0, 0, 'ps_checkout', '#3498D8', 1, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_order_state_lang`
--

DROP TABLE IF EXISTS `ps_order_state_lang`;
CREATE TABLE IF NOT EXISTS `ps_order_state_lang` (
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `template` varchar(64) NOT NULL,
  PRIMARY KEY (`id_order_state`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_order_state_lang`
--

INSERT INTO `ps_order_state_lang` (`id_order_state`, `id_lang`, `name`, `template`) VALUES
(1, 1, 'Awaiting check payment', 'cheque'),
(1, 2, 'En attente du paiement par chèque', 'cheque'),
(1, 3, 'Scheckzahlung wird erwartet', 'cheque'),
(1, 4, 'Wachtend op uw betaling', 'cheque'),
(1, 5, 'In attesa di assegno', 'cheque'),
(1, 6, 'En espera de pago por cheque', 'cheque'),
(2, 1, 'Payment accepted', 'payment'),
(2, 2, 'Paiement accepté', 'payment'),
(2, 3, 'Zahlung eingegangen', 'payment'),
(2, 4, 'Betaling aanvaard', 'payment'),
(2, 5, 'Pagamento accettato', 'payment'),
(2, 6, 'Pago aceptado', 'payment'),
(3, 1, 'Processing in progress', 'preparation'),
(3, 2, 'En cours de préparation', 'preparation'),
(3, 3, 'Bestellung in Bearbeitung', 'preparation'),
(3, 4, 'Wordt momenteel voorbereid', 'preparation'),
(3, 5, 'Preparazione in corso', 'preparation'),
(3, 6, 'Preparación en curso', 'preparation'),
(4, 1, 'Shipped', 'shipped'),
(4, 2, 'Expédié', 'shipped'),
(4, 3, 'Versand', 'shipped'),
(4, 4, 'Verzonden', 'shipped'),
(4, 5, 'Spedito', 'shipped'),
(4, 6, 'Enviado', 'shipped'),
(5, 1, 'Delivered', ''),
(5, 2, 'Livré', ''),
(5, 3, 'Bestellung ausgeliefert', ''),
(5, 4, 'Afgeleverd', ''),
(5, 5, 'Consegnato', ''),
(5, 6, 'Entregado', ''),
(6, 1, 'Canceled', 'order_canceled'),
(6, 2, 'Annulé', 'order_canceled'),
(6, 3, 'Bestellung storniert', 'order_canceled'),
(6, 4, 'Geannuleerd', 'order_canceled'),
(6, 5, 'Annullato', 'order_canceled'),
(6, 6, 'Cancelado', 'order_canceled'),
(7, 1, 'Refunded', 'refund'),
(7, 2, 'Remboursé', 'refund'),
(7, 3, 'Erstattet', 'refund'),
(7, 4, 'Terugbetaald', 'refund'),
(7, 5, 'Rimborsato', 'refund'),
(7, 6, 'Reembolsado', 'refund'),
(8, 1, 'Payment error', 'payment_error'),
(8, 2, 'Erreur de paiement', 'payment_error'),
(8, 3, 'Fehler bei der Bezahlung', 'payment_error'),
(8, 4, 'Betalingsfout', 'payment_error'),
(8, 5, 'Errore di pagamento', 'payment_error'),
(8, 6, 'Error en pago', 'payment_error'),
(9, 1, 'On backorder (paid)', 'outofstock'),
(9, 2, 'En attente de réapprovisionnement (payé)', 'outofstock'),
(9, 3, 'Artikel nicht auf Lager (bezahlt)', 'outofstock'),
(9, 4, 'Momenteel in backorder (betaald)', 'outofstock'),
(9, 5, 'In attesa di rifornimento (pagato)', 'outofstock'),
(9, 6, 'Pedido pendiente por falta de stock (pagado)', 'outofstock'),
(10, 1, 'Awaiting bank wire payment', 'bankwire'),
(10, 2, 'En attente de virement bancaire', 'bankwire'),
(10, 3, 'Warten auf Zahlungseingang Überweisung', 'bankwire'),
(10, 4, 'In afwachting van bankoverschrijving', 'bankwire'),
(10, 5, 'In attesa di pagamento con bonifico bancario', 'bankwire'),
(10, 6, 'En espera de pago por transferencia bancaria', 'bankwire'),
(11, 1, 'Remote payment accepted', 'payment'),
(11, 2, 'Paiement à distance accepté', 'payment'),
(11, 3, 'Zahlung ausserhalb von PrestaShop eingegangen', 'payment'),
(11, 4, 'Betaling op afstand aanvaard', 'payment'),
(11, 5, 'Pagamento remoto accettato', 'payment'),
(11, 6, 'Pago remoto aceptado', 'payment'),
(12, 1, 'On backorder (not paid)', 'outofstock'),
(12, 2, 'En attente de réapprovisionnement (non payé)', 'outofstock'),
(12, 3, 'Artikel nicht auf Lager', 'outofstock'),
(12, 4, 'Momenteel in backorder (niet betaald)', 'outofstock'),
(12, 5, 'In attesa di rifornimento (non pagato)', 'outofstock'),
(12, 6, 'Pedido pendiente por falta de stock (no pagado)', 'outofstock'),
(13, 1, 'Awaiting Cash On Delivery validation', 'cashondelivery'),
(13, 2, 'En attente de paiement à la livraison', 'cashondelivery'),
(13, 3, 'Warten auf Zahlungseingang Nachnahme', 'cashondelivery'),
(13, 4, 'Wachten op bevestiging (rembours)', 'cashondelivery'),
(13, 5, 'In attesa verifica contrassegno', 'cashondelivery'),
(13, 6, 'En espera de validación por contra reembolso.', 'cashondelivery'),
(14, 1, 'Waiting for PayPal payment', 'payment'),
(14, 2, 'En attente de paiement par PayPal', 'payment'),
(14, 3, 'Warten auf PayPal-Zahlung', 'payment'),
(14, 4, 'Wachten op PayPal-betaling', 'payment'),
(14, 5, 'Waiting for PayPal payment', 'payment'),
(14, 6, 'Waiting for PayPal payment', 'payment'),
(15, 1, 'Waiting for Credit Card Payment', 'payment'),
(15, 2, 'En attente de paiement par Carte de Crédit', 'payment'),
(15, 3, 'Warten auf Kreditkartenzahlung', 'payment'),
(15, 4, 'Wachten op creditcard-betaling', 'payment'),
(15, 5, 'Waiting for Credit Card Payment', 'payment'),
(15, 6, 'Waiting for Credit Card Payment', 'payment'),
(16, 1, 'Waiting for Local Payment Method Payment', 'payment'),
(16, 2, 'En attente de paiement par moyen de paiement local', 'payment'),
(16, 3, 'Warten auf Zahlung per lokaler Zahlungsmethode', 'payment'),
(16, 4, 'Wachten op nlaatselijke betaling', 'payment'),
(16, 5, 'Waiting for Local Payment Method Payment', 'payment'),
(16, 6, 'Waiting for Local Payment Method Payment', 'payment'),
(17, 1, 'Authorized. To be captured by merchant', 'payment'),
(17, 2, 'Autorisation. A capturer par le marchand', 'payment'),
(17, 3, 'Autorisiert. Wird von Händler erfasst.', 'payment'),
(17, 4, 'Goedgekeurd. Door retailer te registreren.', 'payment'),
(17, 5, 'Authorized. To be captured by merchant', 'payment'),
(17, 6, 'Authorized. To be captured by merchant', 'payment'),
(18, 1, 'Partial refund', 'payment'),
(18, 2, 'Remboursement partiel', 'payment'),
(18, 3, 'Teilweise Rückerstattung', 'payment'),
(18, 4, 'Gedeeltelijke terugbetaling', 'payment'),
(18, 5, 'Rimborso parziale', 'payment'),
(18, 6, 'Reembolso parcial', 'payment'),
(19, 1, 'Waiting capture', 'payment'),
(19, 2, 'En attente de capture', 'payment'),
(19, 3, 'Warten auf Erfassung', 'payment'),
(19, 4, 'Wachten op registratie', 'payment'),
(19, 5, 'Waiting capture', 'payment'),
(19, 6, 'Waiting capture', 'payment');

-- --------------------------------------------------------

--
-- Structure de la table `ps_pack`
--

DROP TABLE IF EXISTS `ps_pack`;
CREATE TABLE IF NOT EXISTS `ps_pack` (
  `id_product_pack` int(10) UNSIGNED NOT NULL,
  `id_product_item` int(10) UNSIGNED NOT NULL,
  `id_product_attribute_item` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_product_pack`,`id_product_item`,`id_product_attribute_item`),
  KEY `product_item` (`id_product_item`,`id_product_attribute_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_page`
--

DROP TABLE IF EXISTS `ps_page`;
CREATE TABLE IF NOT EXISTS `ps_page` (
  `id_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_page_type` int(10) UNSIGNED NOT NULL,
  `id_object` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id_page`),
  KEY `id_page_type` (`id_page_type`),
  KEY `id_object` (`id_object`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_page`
--

INSERT INTO `ps_page` (`id_page`, `id_page_type`, `id_object`) VALUES
(1, 1, NULL),
(2, 2, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ps_pagenotfound`
--

DROP TABLE IF EXISTS `ps_pagenotfound`;
CREATE TABLE IF NOT EXISTS `ps_pagenotfound` (
  `id_pagenotfound` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `request_uri` varchar(256) NOT NULL,
  `http_referer` varchar(256) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_pagenotfound`),
  KEY `date_add` (`date_add`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_page_type`
--

DROP TABLE IF EXISTS `ps_page_type`;
CREATE TABLE IF NOT EXISTS `ps_page_type` (
  `id_page_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_page_type`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_page_type`
--

INSERT INTO `ps_page_type` (`id_page_type`, `name`) VALUES
(1, 'index'),
(2, 'pagenotfound');

-- --------------------------------------------------------

--
-- Structure de la table `ps_page_viewed`
--

DROP TABLE IF EXISTS `ps_page_viewed`;
CREATE TABLE IF NOT EXISTS `ps_page_viewed` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_date_range` int(10) UNSIGNED NOT NULL,
  `counter` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_page`,`id_date_range`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product`
--

DROP TABLE IF EXISTS `ps_product`;
CREATE TABLE IF NOT EXISTS `ps_product` (
  `id_product` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_supplier` int(10) UNSIGNED DEFAULT NULL,
  `id_manufacturer` int(10) UNSIGNED DEFAULT NULL,
  `id_category_default` int(10) UNSIGNED DEFAULT NULL,
  `id_shop_default` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `on_sale` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `online_only` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ean13` varchar(13) DEFAULT NULL,
  `isbn` varchar(32) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `mpn` varchar(40) DEFAULT NULL,
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `quantity` int(10) NOT NULL DEFAULT '0',
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `low_stock_threshold` int(10) DEFAULT NULL,
  `low_stock_alert` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `additional_shipping_cost` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reference` varchar(64) DEFAULT NULL,
  `supplier_reference` varchar(64) DEFAULT NULL,
  `location` varchar(255) NOT NULL DEFAULT '',
  `width` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `height` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `depth` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `out_of_stock` int(10) UNSIGNED NOT NULL DEFAULT '2',
  `additional_delivery_times` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `quantity_discount` tinyint(1) DEFAULT '0',
  `customizable` tinyint(2) NOT NULL DEFAULT '0',
  `uploadable_files` tinyint(4) NOT NULL DEFAULT '0',
  `text_fields` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `redirect_type` enum('404','301-product','302-product','301-category','302-category') NOT NULL DEFAULT '404',
  `id_type_redirected` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `available_for_order` tinyint(1) NOT NULL DEFAULT '1',
  `available_date` date DEFAULT NULL,
  `show_condition` tinyint(1) NOT NULL DEFAULT '0',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT '1',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_is_pack` tinyint(1) NOT NULL DEFAULT '0',
  `cache_has_attachments` tinyint(1) NOT NULL DEFAULT '0',
  `is_virtual` tinyint(1) NOT NULL DEFAULT '0',
  `cache_default_attribute` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT '0',
  `pack_stock_type` int(11) UNSIGNED NOT NULL DEFAULT '3',
  `state` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `product_type` enum('standard','pack','virtual','combinations','') NOT NULL DEFAULT '',
  PRIMARY KEY (`id_product`),
  KEY `reference_idx` (`reference`),
  KEY `supplier_reference_idx` (`supplier_reference`),
  KEY `product_supplier` (`id_supplier`),
  KEY `product_manufacturer` (`id_manufacturer`,`id_product`),
  KEY `id_category_default` (`id_category_default`),
  KEY `indexed` (`indexed`),
  KEY `date_add` (`date_add`),
  KEY `state` (`state`,`date_upd`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_product`
--

INSERT INTO `ps_product` (`id_product`, `id_supplier`, `id_manufacturer`, `id_category_default`, `id_shop_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ean13`, `isbn`, `upc`, `mpn`, `ecotax`, `quantity`, `minimal_quantity`, `low_stock_threshold`, `low_stock_alert`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `reference`, `supplier_reference`, `location`, `width`, `height`, `depth`, `weight`, `out_of_stock`, `additional_delivery_times`, `quantity_discount`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_type_redirected`, `available_for_order`, `available_date`, `show_condition`, `condition`, `show_price`, `indexed`, `visibility`, `cache_is_pack`, `cache_has_attachments`, `is_virtual`, `cache_default_attribute`, `date_add`, `date_upd`, `advanced_stock_management`, `pack_stock_type`, `state`, `product_type`) VALUES
(20, 0, 0, 10, 1, 1, 0, 0, '', '', '', '', '0.000000', 0, 1, NULL, 0, '19.250000', '0.000000', '', '0.000000', '0.000000', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 1, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 0, 'new', 1, 1, 'both', 0, 0, 0, 0, '2022-12-05 09:35:22', '2022-12-11 15:52:06', 0, 3, 1, ''),
(23, 0, 0, 12, 1, 1, 0, 0, '', '', '', '', '0.000000', 0, 1, NULL, 0, '52.000000', '0.000000', '', '0.000000', '0.000000', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 1, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 0, 'new', 1, 1, 'both', 0, 0, 0, 0, '2022-12-05 11:00:55', '2022-12-11 16:39:05', 0, 3, 1, ''),
(24, 0, 0, 2, 1, 1, 0, 0, '', '', '', '', '0.000000', 0, 1, NULL, 0, '49.500000', '0.000000', '', '0.000000', '0.000000', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 1, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 0, 'new', 1, 1, 'both', 0, 0, 0, 0, '2022-12-05 12:15:28', '2022-12-11 15:49:05', 0, 3, 1, ''),
(25, 0, 0, 2, 1, 1, 0, 0, '', '', '', '', '0.000000', 0, 1, NULL, 0, '0.000000', '0.000000', '', '0.000000', '0.000000', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 1, 0, 0, 0, 0, 0, '404', 0, 1, '0000-00-00', 0, 'new', 1, 0, 'both', 0, 0, 0, 0, '2022-12-11 16:02:02', '2022-12-11 16:02:02', 0, 3, 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_attachment`
--

DROP TABLE IF EXISTS `ps_product_attachment`;
CREATE TABLE IF NOT EXISTS `ps_product_attachment` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_attachment` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product`,`id_attachment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_attribute`
--

DROP TABLE IF EXISTS `ps_product_attribute`;
CREATE TABLE IF NOT EXISTS `ps_product_attribute` (
  `id_product_attribute` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int(10) UNSIGNED NOT NULL,
  `reference` varchar(64) DEFAULT NULL,
  `supplier_reference` varchar(64) DEFAULT NULL,
  `location` varchar(255) NOT NULL DEFAULT '',
  `ean13` varchar(13) DEFAULT NULL,
  `isbn` varchar(32) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `mpn` varchar(40) DEFAULT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `quantity` int(10) NOT NULL DEFAULT '0',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_impact` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `default_on` tinyint(1) UNSIGNED DEFAULT NULL,
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `low_stock_threshold` int(10) DEFAULT NULL,
  `low_stock_alert` tinyint(1) NOT NULL DEFAULT '0',
  `available_date` date DEFAULT NULL,
  PRIMARY KEY (`id_product_attribute`),
  UNIQUE KEY `product_default` (`id_product`,`default_on`),
  KEY `product_attribute_product` (`id_product`),
  KEY `reference` (`reference`),
  KEY `supplier_reference` (`supplier_reference`),
  KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_attribute_combination`
--

DROP TABLE IF EXISTS `ps_product_attribute_combination`;
CREATE TABLE IF NOT EXISTS `ps_product_attribute_combination` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_attribute`,`id_product_attribute`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_attribute_image`
--

DROP TABLE IF EXISTS `ps_product_attribute_image`;
CREATE TABLE IF NOT EXISTS `ps_product_attribute_image` (
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `id_image` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product_attribute`,`id_image`),
  KEY `id_image` (`id_image`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_attribute_shop`
--

DROP TABLE IF EXISTS `ps_product_attribute_shop`;
CREATE TABLE IF NOT EXISTS `ps_product_attribute_shop` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_impact` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `default_on` tinyint(1) UNSIGNED DEFAULT NULL,
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `low_stock_threshold` int(10) DEFAULT NULL,
  `low_stock_alert` tinyint(1) NOT NULL DEFAULT '0',
  `available_date` date DEFAULT NULL,
  PRIMARY KEY (`id_product_attribute`,`id_shop`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`default_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_carrier`
--

DROP TABLE IF EXISTS `ps_product_carrier`;
CREATE TABLE IF NOT EXISTS `ps_product_carrier` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_carrier_reference` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product`,`id_carrier_reference`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_comment`
--

DROP TABLE IF EXISTS `ps_product_comment`;
CREATE TABLE IF NOT EXISTS `ps_product_comment` (
  `id_product_comment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_guest` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `content` text NOT NULL,
  `customer_name` varchar(64) DEFAULT NULL,
  `grade` float UNSIGNED NOT NULL,
  `validate` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_product_comment`),
  KEY `id_product` (`id_product`),
  KEY `id_customer` (`id_customer`),
  KEY `id_guest` (`id_guest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_comment_criterion`
--

DROP TABLE IF EXISTS `ps_product_comment_criterion`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_criterion` (
  `id_product_comment_criterion` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product_comment_criterion_type` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_product_comment_criterion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_product_comment_criterion`
--

INSERT INTO `ps_product_comment_criterion` (`id_product_comment_criterion`, `id_product_comment_criterion_type`, `active`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_comment_criterion_category`
--

DROP TABLE IF EXISTS `ps_product_comment_criterion_category`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_criterion_category` (
  `id_product_comment_criterion` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product_comment_criterion`,`id_category`),
  KEY `id_category` (`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_comment_criterion_lang`
--

DROP TABLE IF EXISTS `ps_product_comment_criterion_lang`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_criterion_lang` (
  `id_product_comment_criterion` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_product_comment_criterion`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_product_comment_criterion_lang`
--

INSERT INTO `ps_product_comment_criterion_lang` (`id_product_comment_criterion`, `id_lang`, `name`) VALUES
(1, 1, 'Quality'),
(1, 2, 'Quality'),
(1, 3, 'Quality'),
(1, 4, 'Quality'),
(1, 5, 'Quality'),
(1, 6, 'Quality');

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_comment_criterion_product`
--

DROP TABLE IF EXISTS `ps_product_comment_criterion_product`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_criterion_product` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_comment_criterion` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product`,`id_product_comment_criterion`),
  KEY `id_product_comment_criterion` (`id_product_comment_criterion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_comment_grade`
--

DROP TABLE IF EXISTS `ps_product_comment_grade`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_grade` (
  `id_product_comment` int(10) UNSIGNED NOT NULL,
  `id_product_comment_criterion` int(10) UNSIGNED NOT NULL,
  `grade` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product_comment`,`id_product_comment_criterion`),
  KEY `id_product_comment_criterion` (`id_product_comment_criterion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_comment_report`
--

DROP TABLE IF EXISTS `ps_product_comment_report`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_report` (
  `id_product_comment` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product_comment`,`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_comment_usefulness`
--

DROP TABLE IF EXISTS `ps_product_comment_usefulness`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_usefulness` (
  `id_product_comment` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `usefulness` tinyint(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product_comment`,`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_country_tax`
--

DROP TABLE IF EXISTS `ps_product_country_tax`;
CREATE TABLE IF NOT EXISTS `ps_product_country_tax` (
  `id_product` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL,
  PRIMARY KEY (`id_product`,`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_download`
--

DROP TABLE IF EXISTS `ps_product_download`;
CREATE TABLE IF NOT EXISTS `ps_product_download` (
  `id_product_download` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int(10) UNSIGNED NOT NULL,
  `display_filename` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_expiration` datetime DEFAULT NULL,
  `nb_days_accessible` int(10) UNSIGNED DEFAULT NULL,
  `nb_downloadable` int(10) UNSIGNED DEFAULT '1',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `is_shareable` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_product_download`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_group_reduction_cache`
--

DROP TABLE IF EXISTS `ps_product_group_reduction_cache`;
CREATE TABLE IF NOT EXISTS `ps_product_group_reduction_cache` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(5,4) NOT NULL,
  PRIMARY KEY (`id_product`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_lang`
--

DROP TABLE IF EXISTS `ps_product_lang`;
CREATE TABLE IF NOT EXISTS `ps_product_lang` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text,
  `description_short` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_description` varchar(512) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `available_now` varchar(255) DEFAULT NULL,
  `available_later` varchar(255) DEFAULT NULL,
  `delivery_in_stock` varchar(255) DEFAULT NULL,
  `delivery_out_stock` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_product`,`id_shop`,`id_lang`),
  KEY `id_lang` (`id_lang`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_product_lang`
--

INSERT INTO `ps_product_lang` (`id_product`, `id_shop`, `id_lang`, `description`, `description_short`, `link_rewrite`, `meta_description`, `meta_keywords`, `meta_title`, `name`, `available_now`, `available_later`, `delivery_in_stock`, `delivery_out_stock`) VALUES
(20, 1, 1, '<h3 class=\"a-spacing-mini\" style=\"padding:0px;font-size:17px;line-height:1.255;font-family:Arial, sans-serif;color:#0f1111;background-color:#ffffff;margin:0px 0px 6px 0px;\">Dungeons &amp; Dragons Starter Set - Version Anglaise</h3>\r\n<h5 class=\"a-spacing-mini a-color-secondary\" style=\"padding:0px;font-size:13px;line-height:19px;font-family:Arial, sans-serif;background-color:#ffffff;color:#565959;margin:0px 0px 6px 0px;\">Tout ce dont vous avez besoin</h5>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Contient tout ce dont vous avez besoin pour votre première partie de D&amp;D.</p>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Jusqu’à six joueurs peuvent profiter du Starter Set. L’un d’entre vous endossera le rôle du narrateur du jeu, le maître du donjon.</p>\r\n<h5 class=\"a-spacing-mini a-color-secondary\" style=\"padding:0px;font-size:13px;line-height:19px;font-family:Arial, sans-serif;background-color:#ffffff;color:#565959;margin:0px 0px 6px 0px;\">Rejoignez l’aventure</h5>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Si vous souhaitez en savoir plus sur D&amp;D, le Starter Set est la porte d’entrée idéale, qui vous mènera ensuite aux livres principaux de Dungeons &amp; Dragons : le Manuel des joueurs, le Manuel des monstres et le Guide du maître.</p>', '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding:0px;font-weight:400;color:#0f1111;font-family:Arial, sans-serif;background-color:#ffffff;font-size:24px;line-height:32px;margin:0px 0px 0px 0px;\"><span id=\"productTitle\" class=\"a-size-large product-title-word-break\" style=\"line-height:32px;\">Dungeons &amp; Dragons Starter Set</span></h1>\r\n<p><span class=\"a-size-large product-title-word-break\" style=\"line-height:32px;\"></span></p>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Contient tout ce dont vous avez besoin pour votre première partie de D&amp;D.</p>\r\n<p><span class=\"a-size-large product-title-word-break\" style=\"line-height:32px;\"></span></p>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Jusqu’à six joueurs peuvent profiter du Starter Set. L’un d’entre vous endossera le rôle du narrateur du jeu, le maître du donjon.</p>', 'dungeons-dragons-kit-d-initiation', '', '', '', 'Dungeons & Dragons : Kit d\'initiation', '', '', '', ''),
(20, 1, 2, '', '', 'dungeons-dragons-kit-d-initiation', '', '', '', 'Dungeons & Dragons : Kit d\'initiation', '', '', '', ''),
(20, 1, 3, '', '', 'dungeons-dragons-kit-d-initiation', '', '', '', 'Dungeons & Dragons : Kit d\'initiation', '', '', '', ''),
(20, 1, 4, '', '', 'dungeons-dragons-kit-d-initiation', '', '', '', 'Dungeons & Dragons : Kit d\'initiation', '', '', '', ''),
(20, 1, 5, '<h3 class=\"a-spacing-mini\" style=\"padding:0px;font-size:17px;line-height:1.255;font-family:Arial, sans-serif;color:#0f1111;background-color:#ffffff;margin:0px 0px 6px 0px;\">Dungeons &amp; Dragons Starter Set - Version Anglaise</h3>\r\n<h5 class=\"a-spacing-mini a-color-secondary\" style=\"padding:0px;font-size:13px;line-height:19px;font-family:Arial, sans-serif;background-color:#ffffff;color:#565959;margin:0px 0px 6px 0px;\">Tout ce dont vous avez besoin</h5>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Contient tout ce dont vous avez besoin pour votre première partie de D&amp;D.</p>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Jusqu’à six joueurs peuvent profiter du Starter Set. L’un d’entre vous endossera le rôle du narrateur du jeu, le maître du donjon.</p>\r\n<h5 class=\"a-spacing-mini a-color-secondary\" style=\"padding:0px;font-size:13px;line-height:19px;font-family:Arial, sans-serif;background-color:#ffffff;color:#565959;margin:0px 0px 6px 0px;\">Rejoignez l’aventure</h5>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Si vous souhaitez en savoir plus sur D&amp;D, le Starter Set est la porte d’entrée idéale, qui vous mènera ensuite aux livres principaux de Dungeons &amp; Dragons : le Manuel des joueurs, le Manuel des monstres et le Guide du maître.</p>', '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding:0px;font-weight:400;color:#0f1111;font-family:Arial, sans-serif;background-color:#ffffff;font-size:24px;line-height:32px;margin:0px 0px 0px 0px;\"><span id=\"productTitle\" class=\"a-size-large product-title-word-break\" style=\"line-height:32px;\">Dungeons &amp; Dragons Starter Set</span></h1>', 'dungeons-dragons-kit-d-initiation', '', '', '', 'Dungeons & Dragons : Kit d\'initiation', '', '', '', ''),
(20, 1, 6, '<h3 class=\"a-spacing-mini\" style=\"padding:0px;font-size:17px;line-height:1.255;font-family:Arial, sans-serif;color:#0f1111;background-color:#ffffff;margin:0px 0px 6px 0px;\">Dungeons &amp; Dragons Starter Set - Version Anglaise</h3>\r\n<h5 class=\"a-spacing-mini a-color-secondary\" style=\"padding:0px;font-size:13px;line-height:19px;font-family:Arial, sans-serif;background-color:#ffffff;color:#565959;margin:0px 0px 6px 0px;\">Tout ce dont vous avez besoin</h5>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Contient tout ce dont vous avez besoin pour votre première partie de D&amp;D.</p>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Jusqu’à six joueurs peuvent profiter du Starter Set. L’un d’entre vous endossera le rôle du narrateur du jeu, le maître du donjon.</p>\r\n<h5 class=\"a-spacing-mini a-color-secondary\" style=\"padding:0px;font-size:13px;line-height:19px;font-family:Arial, sans-serif;background-color:#ffffff;color:#565959;margin:0px 0px 6px 0px;\">Rejoignez l’aventure</h5>\r\n<p class=\"a-spacing-base\" style=\"padding:0px;color:#0f1111;font-family:Arial, sans-serif;font-size:14px;background-color:#ffffff;margin:0px 0px 14px 0px;\">Si vous souhaitez en savoir plus sur D&amp;D, le Starter Set est la porte d’entrée idéale, qui vous mènera ensuite aux livres principaux de Dungeons &amp; Dragons : le Manuel des joueurs, le Manuel des monstres et le Guide du maître.</p>', '<h1 id=\"title\" class=\"a-size-large a-spacing-none\" style=\"padding:0px;font-weight:400;color:#0f1111;font-family:Arial, sans-serif;background-color:#ffffff;font-size:24px;line-height:32px;margin:0px 0px 0px 0px;\"><span id=\"productTitle\" class=\"a-size-large product-title-word-break\" style=\"line-height:32px;\">Dungeons &amp; Dragons Starter Set</span></h1>', 'dungeons-dragons-kit-d-initiation', '', '', '', 'Dungeons & Dragons : Kit d\'initiation', '', '', '', ''),
(23, 1, 1, '<p><span style=\"color:#333333;font-family:\'Amazon Ember\', Arial, sans-serif;font-size:small;background-color:#ffffff;\">Les Tyrans de L\'Ombreterre Emparez- Vous de L\'Ombreterre! Prenez la tete d\'une maison drow et partez a la conquete de L\'Ombreterre. Construisez votre jeu tout au long de la partie pour recruter des drows, des dragons, des fanatiques et des demons puis utilisez-les pour assassiner les unites ennemies ou pour vous infiltrer dans la forteresse de vos adversaires et prendre le controle. Choisissez bien votre strategie car il ne peut y avoir qu\'une seule maison a la tete de L\'Ombreterre! Cette Boite Contient 1 Plateau de jeu, 1 livret de regles, 1 bloc de score, 1 casier de rangement, 1 plateau Marche, 4 plateaux Maison drow, 340 cartes Seide, 68 jetons predecoupes.</span></p>', '', 'dungeons-dragons-les-tyrans-de-l-ombreterre', '', '', '', 'Dungeons & Dragons : Les Tyrans de l\'ombreterre', '', '', '', ''),
(23, 1, 2, '', '', 'dungeons-dragons-les-tyrans-de-l-ombreterre', '', '', '', 'Dungeons & Dragons : Les Tyrans de l\'ombreterre', '', '', '', ''),
(23, 1, 3, '', '', 'dungeons-dragons-les-tyrans-de-l-ombreterre', '', '', '', 'Dungeons & Dragons : Les Tyrans de l\'ombreterre', '', '', '', ''),
(23, 1, 4, '', '', 'dungeons-dragons-les-tyrans-de-l-ombreterre', '', '', '', 'Dungeons & Dragons : Les Tyrans de l\'ombreterre', '', '', '', ''),
(23, 1, 5, '', '', 'dungeons-dragons-les-tyrans-de-l-ombreterre', '', '', '', 'Dungeons & Dragons : Les Tyrans de l\'ombreterre', '', '', '', ''),
(23, 1, 6, '<p><span style=\"color:#333333;font-family:\'Amazon Ember\', Arial, sans-serif;font-size:small;background-color:#ffffff;\">Les Tyrans de L\'Ombreterre Emparez- Vous de L\'Ombreterre! Prenez la tete d\'une maison drow et partez a la conquete de L\'Ombreterre. Construisez votre jeu tout au long de la partie pour recruter des drows, des dragons, des fanatiques et des demons puis utilisez-les pour assassiner les unites ennemies ou pour vous infiltrer dans la forteresse de vos adversaires et prendre le controle. Choisissez bien votre strategie car il ne peut y avoir qu\'une seule maison a la tete de L\'Ombreterre! Cette Boite Contient 1 Plateau de jeu, 1 livret de regles, 1 bloc de score, 1 casier de rangement, 1 plateau Marche, 4 plateaux Maison drow, 340 cartes Seide, 68 jetons predecoupes.</span></p>', '', 'dungeons-dragons-les-tyrans-de-l-ombreterre', '', '', '', 'Dungeons & Dragons : Les Tyrans de l\'ombreterre', '', '', '', ''),
(24, 1, 1, '<p><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">« Un magicien n’est jamais en retard, ni en avance d’ailleurs… »</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">Parfois, pour apprécier à sa juste valeur une histoire, il faut d’abord revenir à son point de départ. Le premier pas d’un voyage est aussi important que ses péripéties et les liens forts que nous créons en cours de route. Gardons ceci en tête et prenons le temps de retourner au début d’une des aventures les plus épiques de tous les Âges…</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">La boîte de base révisée du jeu de cartes évolutif du Seigneur des Anneaux contient du matériel amélioré, pour une expérience plus agréable de jeu, et idéal pour les personnes qui voudraient se lancer dans un périple mythique ! Tous les éléments sont disponibles pour partir jusqu’à 4 et former une communauté digne de ce nom. (Re)découvrez un des classiques du jeu de société, qui nous fait rêver depuis 10 ans maintenant !</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">Contenu :</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 1 Livret de règles</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 188 cartes joueur</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 12 cartes héros</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 84 cartes Rencontre</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 10 cartes quête</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 3 cartes Campagne</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 16 cartes Avantage/fardeau</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 57 Marqueurs de Ressource</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 66 Marqueurs de Dégâts</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 66 Marqueurs de Progression</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 4 Compteurs de Menace</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 1 marqueur Premier Joueur</span></p>', '', 'le-seigneur-des-anneaux-jce-edition-revisee', '', '', '', 'Le Seigneur des Anneaux JCE - Edition révisée', '', '', '', ''),
(24, 1, 2, '', '', 'le-seigneur-des-anneaux-jce-edition-revisee', '', '', '', 'Le Seigneur des Anneaux JCE - Edition révisée', '', '', '', ''),
(24, 1, 3, '', '', 'le-seigneur-des-anneaux-jce-edition-revisee', '', '', '', 'Le Seigneur des Anneaux JCE - Edition révisée', '', '', '', ''),
(24, 1, 4, '', '', 'le-seigneur-des-anneaux-jce-edition-revisee', '', '', '', 'Le Seigneur des Anneaux JCE - Edition révisée', '', '', '', ''),
(24, 1, 5, '', '', 'le-seigneur-des-anneaux-jce-edition-revisee', '', '', '', 'Le Seigneur des Anneaux JCE - Edition révisée', '', '', '', ''),
(24, 1, 6, '<p><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">« Un magicien n’est jamais en retard, ni en avance d’ailleurs… »</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">Parfois, pour apprécier à sa juste valeur une histoire, il faut d’abord revenir à son point de départ. Le premier pas d’un voyage est aussi important que ses péripéties et les liens forts que nous créons en cours de route. Gardons ceci en tête et prenons le temps de retourner au début d’une des aventures les plus épiques de tous les Âges…</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">La boîte de base révisée du jeu de cartes évolutif du Seigneur des Anneaux contient du matériel amélioré, pour une expérience plus agréable de jeu, et idéal pour les personnes qui voudraient se lancer dans un périple mythique ! Tous les éléments sont disponibles pour partir jusqu’à 4 et former une communauté digne de ce nom. (Re)découvrez un des classiques du jeu de société, qui nous fait rêver depuis 10 ans maintenant !</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">Contenu :</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 1 Livret de règles</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 188 cartes joueur</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 12 cartes héros</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 84 cartes Rencontre</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 10 cartes quête</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 3 cartes Campagne</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 16 cartes Avantage/fardeau</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 57 Marqueurs de Ressource</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 66 Marqueurs de Dégâts</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 66 Marqueurs de Progression</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 4 Compteurs de Menace</span><br style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;\" /><span style=\"font-family:\'Trebuchet MS\', \'Lucida Grande\', Verdana, Lucida, Geneva, Helvetica, Arial, sans-serif;background-color:#ffffff;\">- 1 marqueur Premier Joueur</span></p>', '', 'le-seigneur-des-anneaux-jce-edition-revisee', '', '', '', 'Le Seigneur des Anneaux JCE - Edition révisée', '', '', '', ''),
(25, 1, 1, '', '', '', '', '', '', '', '', '', '', ''),
(25, 1, 2, '', '', '', '', '', '', '', '', '', '', ''),
(25, 1, 3, '', '', '', '', '', '', '', '', '', '', ''),
(25, 1, 4, '', '', '', '', '', '', '', '', '', '', ''),
(25, 1, 5, '', '', '', '', '', '', '', '', '', '', ''),
(25, 1, 6, '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_sale`
--

DROP TABLE IF EXISTS `ps_product_sale`;
CREATE TABLE IF NOT EXISTS `ps_product_sale` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sale_nbr` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_upd` date DEFAULT NULL,
  PRIMARY KEY (`id_product`),
  KEY `quantity` (`quantity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_shop`
--

DROP TABLE IF EXISTS `ps_product_shop`;
CREATE TABLE IF NOT EXISTS `ps_product_shop` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `id_category_default` int(10) UNSIGNED DEFAULT NULL,
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `on_sale` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `online_only` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `low_stock_threshold` int(10) DEFAULT NULL,
  `low_stock_alert` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `additional_shipping_cost` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `customizable` tinyint(2) NOT NULL DEFAULT '0',
  `uploadable_files` tinyint(4) NOT NULL DEFAULT '0',
  `text_fields` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `redirect_type` enum('','404','301-product','302-product','301-category','302-category') NOT NULL DEFAULT '',
  `id_type_redirected` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `available_for_order` tinyint(1) NOT NULL DEFAULT '1',
  `available_date` date DEFAULT NULL,
  `show_condition` tinyint(1) NOT NULL DEFAULT '1',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT '1',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_default_attribute` int(10) UNSIGNED DEFAULT NULL,
  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `pack_stock_type` int(11) UNSIGNED NOT NULL DEFAULT '3',
  PRIMARY KEY (`id_product`,`id_shop`),
  KEY `id_category_default` (`id_category_default`),
  KEY `date_add` (`date_add`,`active`,`visibility`),
  KEY `indexed` (`indexed`,`active`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_product_shop`
--

INSERT INTO `ps_product_shop` (`id_product`, `id_shop`, `id_category_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ecotax`, `minimal_quantity`, `low_stock_threshold`, `low_stock_alert`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_type_redirected`, `available_for_order`, `available_date`, `show_condition`, `condition`, `show_price`, `indexed`, `visibility`, `cache_default_attribute`, `advanced_stock_management`, `date_add`, `date_upd`, `pack_stock_type`) VALUES
(20, 1, 10, 1, 0, 0, '0.000000', 1, NULL, 0, '19.250000', '0.000000', '', '0.000000', '0.000000', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 0, 'new', 1, 1, 'both', 0, 0, '2022-12-05 09:35:22', '2022-12-11 15:52:06', 3),
(23, 1, 12, 1, 0, 0, '0.000000', 1, NULL, 0, '52.000000', '0.000000', '', '0.000000', '0.000000', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 0, 'new', 1, 1, 'both', 0, 0, '2022-12-05 11:00:55', '2022-12-11 16:39:05', 3),
(24, 1, 2, 1, 0, 0, '0.000000', 1, NULL, 0, '49.500000', '0.000000', '', '0.000000', '0.000000', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 0, 'new', 1, 1, 'both', 0, 0, '2022-12-05 12:15:28', '2022-12-11 15:49:05', 3),
(25, 1, 2, 1, 0, 0, '0.000000', 1, NULL, 0, '0.000000', '0.000000', '', '0.000000', '0.000000', 0, 0, 0, 0, '404', 0, 1, '0000-00-00', 0, 'new', 1, 0, 'both', 0, 0, '2022-12-11 16:02:02', '2022-12-11 16:02:02', 3);

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_supplier`
--

DROP TABLE IF EXISTS `ps_product_supplier`;
CREATE TABLE IF NOT EXISTS `ps_product_supplier` (
  `id_product_supplier` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `id_supplier` int(11) UNSIGNED NOT NULL,
  `product_supplier_reference` varchar(64) DEFAULT NULL,
  `product_supplier_price_te` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `id_currency` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product_supplier`),
  UNIQUE KEY `id_product` (`id_product`,`id_product_attribute`,`id_supplier`),
  KEY `id_supplier` (`id_supplier`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_product_tag`
--

DROP TABLE IF EXISTS `ps_product_tag`;
CREATE TABLE IF NOT EXISTS `ps_product_tag` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_tag` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product`,`id_tag`),
  KEY `id_tag` (`id_tag`),
  KEY `id_lang` (`id_lang`,`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_profile`
--

DROP TABLE IF EXISTS `ps_profile`;
CREATE TABLE IF NOT EXISTS `ps_profile` (
  `id_profile` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_profile`
--

INSERT INTO `ps_profile` (`id_profile`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Structure de la table `ps_profile_lang`
--

DROP TABLE IF EXISTS `ps_profile_lang`;
CREATE TABLE IF NOT EXISTS `ps_profile_lang` (
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_profile` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id_profile`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_profile_lang`
--

INSERT INTO `ps_profile_lang` (`id_lang`, `id_profile`, `name`) VALUES
(1, 1, 'SuperAdmin'),
(2, 1, 'SuperAdmin'),
(3, 1, 'SuperAdmin'),
(4, 1, 'SuperAdmin'),
(5, 1, 'SuperAdmin'),
(6, 1, 'SuperAdmin'),
(1, 2, 'Logistician'),
(2, 2, 'Logisticien'),
(3, 2, 'Logistiker'),
(4, 2, 'Logistiek medewerker'),
(5, 2, 'Addetto alla spedizione'),
(6, 2, 'Encargado de logística'),
(1, 3, 'Translator'),
(2, 3, 'Traducteur'),
(3, 3, 'Übersetzer'),
(4, 3, 'Vertaler'),
(5, 3, 'Traduttore'),
(6, 3, 'Traductor'),
(1, 4, 'Salesman'),
(2, 4, 'Commercial'),
(3, 4, 'Verkäufer'),
(4, 4, 'Verkoper'),
(5, 4, 'Venditore'),
(6, 4, 'Vendedor');

-- --------------------------------------------------------

--
-- Structure de la table `ps_pscheckout_cart`
--

DROP TABLE IF EXISTS `ps_pscheckout_cart`;
CREATE TABLE IF NOT EXISTS `ps_pscheckout_cart` (
  `id_pscheckout_cart` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `paypal_intent` varchar(20) DEFAULT 'CAPTURE',
  `paypal_order` varchar(20) DEFAULT NULL,
  `paypal_status` varchar(20) DEFAULT NULL,
  `paypal_funding` varchar(20) DEFAULT NULL,
  `paypal_token` text,
  `paypal_token_expire` datetime DEFAULT NULL,
  `paypal_authorization_expire` datetime DEFAULT NULL,
  `isExpressCheckout` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `isHostedFields` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_pscheckout_cart`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_pscheckout_funding_source`
--

DROP TABLE IF EXISTS `ps_pscheckout_funding_source`;
CREATE TABLE IF NOT EXISTS `ps_pscheckout_funding_source` (
  `name` varchar(20) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `position` tinyint(2) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`name`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_pscheckout_order_matrice`
--

DROP TABLE IF EXISTS `ps_pscheckout_order_matrice`;
CREATE TABLE IF NOT EXISTS `ps_pscheckout_order_matrice` (
  `id_order_matrice` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_order_prestashop` int(10) UNSIGNED NOT NULL,
  `id_order_paypal` varchar(20) NOT NULL,
  PRIMARY KEY (`id_order_matrice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_psgdpr_consent`
--

DROP TABLE IF EXISTS `ps_psgdpr_consent`;
CREATE TABLE IF NOT EXISTS `ps_psgdpr_consent` (
  `id_gdpr_consent` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_module` int(10) UNSIGNED NOT NULL,
  `active` int(10) NOT NULL,
  `error` int(10) DEFAULT NULL,
  `error_message` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_gdpr_consent`,`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_psgdpr_consent_lang`
--

DROP TABLE IF EXISTS `ps_psgdpr_consent_lang`;
CREATE TABLE IF NOT EXISTS `ps_psgdpr_consent_lang` (
  `id_gdpr_consent` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `message` text,
  `id_shop` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_gdpr_consent`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_psgdpr_log`
--

DROP TABLE IF EXISTS `ps_psgdpr_log`;
CREATE TABLE IF NOT EXISTS `ps_psgdpr_log` (
  `id_gdpr_log` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) UNSIGNED DEFAULT NULL,
  `id_guest` int(10) UNSIGNED DEFAULT NULL,
  `client_name` varchar(250) DEFAULT NULL,
  `id_module` int(10) UNSIGNED NOT NULL,
  `request_type` int(10) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_gdpr_log`),
  KEY `id_customer` (`id_customer`),
  KEY `idx_id_customer` (`id_customer`,`id_guest`,`client_name`,`id_module`,`date_add`,`date_upd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_psreassurance`
--

DROP TABLE IF EXISTS `ps_psreassurance`;
CREATE TABLE IF NOT EXISTS `ps_psreassurance` (
  `id_psreassurance` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) DEFAULT NULL,
  `custom_icon` varchar(255) DEFAULT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `type_link` int(10) UNSIGNED DEFAULT NULL,
  `id_cms` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_psreassurance`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_psreassurance`
--

INSERT INTO `ps_psreassurance` (`id_psreassurance`, `icon`, `custom_icon`, `status`, `position`, `id_shop`, `type_link`, `id_cms`, `date_add`, `date_upd`) VALUES
(1, '/prestashop/modules/blockreassurance/views/img/reassurance/pack2/security.svg', NULL, 1, 1, 1, NULL, NULL, '2022-11-21 11:06:43', NULL),
(2, '/prestashop/modules/blockreassurance/views/img/reassurance/pack2/carrier.svg', NULL, 1, 2, 1, NULL, NULL, '2022-11-21 11:06:43', NULL),
(3, '/prestashop/modules/blockreassurance/views/img/reassurance/pack2/parcel.svg', NULL, 1, 3, 1, NULL, NULL, '2022-11-21 11:06:43', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ps_psreassurance_lang`
--

DROP TABLE IF EXISTS `ps_psreassurance_lang`;
CREATE TABLE IF NOT EXISTS `ps_psreassurance_lang` (
  `id_psreassurance` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id_psreassurance`,`id_shop`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_psreassurance_lang`
--

INSERT INTO `ps_psreassurance_lang` (`id_psreassurance`, `id_lang`, `id_shop`, `title`, `description`, `link`) VALUES
(1, 1, 1, 'Security policy', '(edit with the Customer Reassurance module)', ''),
(1, 2, 1, 'Security policy', '(edit with the Customer Reassurance module)', ''),
(1, 3, 1, 'Security policy', '(edit with the Customer Reassurance module)', ''),
(1, 4, 1, 'Security policy', '(edit with the Customer Reassurance module)', ''),
(1, 5, 1, 'Security policy', '(edit with the Customer Reassurance module)', ''),
(1, 6, 1, 'Security policy', '(edit with the Customer Reassurance module)', ''),
(2, 1, 1, 'Delivery policy', '(edit with the Customer Reassurance module)', ''),
(2, 2, 1, 'Delivery policy', '(edit with the Customer Reassurance module)', ''),
(2, 3, 1, 'Delivery policy', '(edit with the Customer Reassurance module)', ''),
(2, 4, 1, 'Delivery policy', '(edit with the Customer Reassurance module)', ''),
(2, 5, 1, 'Delivery policy', '(edit with the Customer Reassurance module)', ''),
(2, 6, 1, 'Delivery policy', '(edit with the Customer Reassurance module)', ''),
(3, 1, 1, 'Return policy', '(edit with the Customer Reassurance module)', ''),
(3, 2, 1, 'Return policy', '(edit with the Customer Reassurance module)', ''),
(3, 3, 1, 'Return policy', '(edit with the Customer Reassurance module)', ''),
(3, 4, 1, 'Return policy', '(edit with the Customer Reassurance module)', ''),
(3, 5, 1, 'Return policy', '(edit with the Customer Reassurance module)', ''),
(3, 6, 1, 'Return policy', '(edit with the Customer Reassurance module)', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_quick_access`
--

DROP TABLE IF EXISTS `ps_quick_access`;
CREATE TABLE IF NOT EXISTS `ps_quick_access` (
  `id_quick_access` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `new_window` tinyint(1) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id_quick_access`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_quick_access`
--

INSERT INTO `ps_quick_access` (`id_quick_access`, `new_window`, `link`) VALUES
(1, 0, 'index.php?controller=AdminOrders'),
(2, 0, 'index.php?controller=AdminCartRules&addcart_rule'),
(3, 0, 'index.php/sell/catalog/products/new'),
(4, 0, 'index.php/sell/catalog/categories/new'),
(5, 0, 'index.php/improve/modules/manage'),
(6, 0, 'index.php?controller=AdminStats&module=statscheckup');

-- --------------------------------------------------------

--
-- Structure de la table `ps_quick_access_lang`
--

DROP TABLE IF EXISTS `ps_quick_access_lang`;
CREATE TABLE IF NOT EXISTS `ps_quick_access_lang` (
  `id_quick_access` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_quick_access`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_quick_access_lang`
--

INSERT INTO `ps_quick_access_lang` (`id_quick_access`, `id_lang`, `name`) VALUES
(1, 1, 'Orders'),
(1, 2, 'Commandes'),
(1, 3, 'Bestellungen'),
(1, 4, 'Bestellingen'),
(1, 5, 'Ordini'),
(1, 6, 'Pedidos'),
(2, 1, 'New voucher'),
(2, 2, 'Nouveau bon de réduction'),
(2, 3, 'Neuer Ermäßigungsgutschein'),
(2, 4, 'Nieuwe voucher'),
(2, 5, 'Nuovo voucher'),
(2, 6, 'Nuevo cupón de descuento'),
(3, 1, 'New product'),
(3, 2, 'Nouveau produit'),
(3, 3, 'Neuer Artikel'),
(3, 4, 'Nieuw product'),
(3, 5, 'Nuovo prodotto'),
(3, 6, 'Nuevo'),
(4, 1, 'New category'),
(4, 2, 'Nouvelle catégorie'),
(4, 3, 'Neue Kategorie'),
(4, 4, 'Nieuwe categorie'),
(4, 5, 'Nuova categoria'),
(4, 6, 'Nueva categoría'),
(5, 1, 'Installed modules'),
(5, 2, 'Modules installés'),
(5, 3, 'Installierte Module'),
(5, 4, 'Geïnstalleerde modules'),
(5, 5, 'Moduli installati'),
(5, 6, 'Módulos instalados'),
(6, 1, 'Catalog evaluation'),
(6, 2, 'Évaluation du catalogue'),
(6, 3, 'Katalogauswertung'),
(6, 4, 'Winkel evaluatie'),
(6, 5, 'Valutazione catalogo'),
(6, 6, 'Evaluación del catálogo');

-- --------------------------------------------------------

--
-- Structure de la table `ps_range_price`
--

DROP TABLE IF EXISTS `ps_range_price`;
CREATE TABLE IF NOT EXISTS `ps_range_price` (
  `id_range_price` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL,
  PRIMARY KEY (`id_range_price`),
  UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_range_price`
--

INSERT INTO `ps_range_price` (`id_range_price`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
(1, 2, '0.000000', '10000.000000'),
(2, 3, '0.000000', '50.000000'),
(3, 3, '50.000000', '100.000000'),
(4, 3, '100.000000', '200.000000');

-- --------------------------------------------------------

--
-- Structure de la table `ps_range_weight`
--

DROP TABLE IF EXISTS `ps_range_weight`;
CREATE TABLE IF NOT EXISTS `ps_range_weight` (
  `id_range_weight` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL,
  PRIMARY KEY (`id_range_weight`),
  UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_range_weight`
--

INSERT INTO `ps_range_weight` (`id_range_weight`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
(1, 2, '0.000000', '10000.000000'),
(2, 4, '0.000000', '1.000000'),
(3, 4, '1.000000', '3.000000'),
(4, 4, '3.000000', '10000.000000');

-- --------------------------------------------------------

--
-- Structure de la table `ps_referrer`
--

DROP TABLE IF EXISTS `ps_referrer`;
CREATE TABLE IF NOT EXISTS `ps_referrer` (
  `id_referrer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `passwd` varchar(255) DEFAULT NULL,
  `http_referer_regexp` varchar(64) DEFAULT NULL,
  `http_referer_like` varchar(64) DEFAULT NULL,
  `request_uri_regexp` varchar(64) DEFAULT NULL,
  `request_uri_like` varchar(64) DEFAULT NULL,
  `http_referer_regexp_not` varchar(64) DEFAULT NULL,
  `http_referer_like_not` varchar(64) DEFAULT NULL,
  `request_uri_regexp_not` varchar(64) DEFAULT NULL,
  `request_uri_like_not` varchar(64) DEFAULT NULL,
  `base_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `percent_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `click_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_referrer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_referrer_cache`
--

DROP TABLE IF EXISTS `ps_referrer_cache`;
CREATE TABLE IF NOT EXISTS `ps_referrer_cache` (
  `id_connections_source` int(11) UNSIGNED NOT NULL,
  `id_referrer` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_connections_source`,`id_referrer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_referrer_shop`
--

DROP TABLE IF EXISTS `ps_referrer_shop`;
CREATE TABLE IF NOT EXISTS `ps_referrer_shop` (
  `id_referrer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cache_visitors` int(11) DEFAULT NULL,
  `cache_visits` int(11) DEFAULT NULL,
  `cache_pages` int(11) DEFAULT NULL,
  `cache_registrations` int(11) DEFAULT NULL,
  `cache_orders` int(11) DEFAULT NULL,
  `cache_sales` decimal(17,2) DEFAULT NULL,
  `cache_reg_rate` decimal(5,4) DEFAULT NULL,
  `cache_order_rate` decimal(5,4) DEFAULT NULL,
  PRIMARY KEY (`id_referrer`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_request_sql`
--

DROP TABLE IF EXISTS `ps_request_sql`;
CREATE TABLE IF NOT EXISTS `ps_request_sql` (
  `id_request_sql` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `sql` text NOT NULL,
  PRIMARY KEY (`id_request_sql`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_required_field`
--

DROP TABLE IF EXISTS `ps_required_field`;
CREATE TABLE IF NOT EXISTS `ps_required_field` (
  `id_required_field` int(11) NOT NULL AUTO_INCREMENT,
  `object_name` varchar(32) NOT NULL,
  `field_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_required_field`),
  KEY `object_name` (`object_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_risk`
--

DROP TABLE IF EXISTS `ps_risk`;
CREATE TABLE IF NOT EXISTS `ps_risk` (
  `id_risk` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `percent` tinyint(3) NOT NULL,
  `color` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_risk`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_risk`
--

INSERT INTO `ps_risk` (`id_risk`, `percent`, `color`) VALUES
(1, 0, '#32CD32'),
(2, 35, '#FF8C00'),
(3, 75, '#DC143C'),
(4, 100, '#ec2e15');

-- --------------------------------------------------------

--
-- Structure de la table `ps_risk_lang`
--

DROP TABLE IF EXISTS `ps_risk_lang`;
CREATE TABLE IF NOT EXISTS `ps_risk_lang` (
  `id_risk` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_risk`,`id_lang`),
  KEY `id_risk` (`id_risk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_risk_lang`
--

INSERT INTO `ps_risk_lang` (`id_risk`, `id_lang`, `name`) VALUES
(1, 1, 'None'),
(1, 2, 'Aucun'),
(1, 3, 'Keine'),
(1, 4, 'Geen'),
(1, 5, 'Nessuno'),
(1, 6, 'Ninguno'),
(2, 1, 'Low'),
(2, 2, 'Basse'),
(2, 3, 'Niedrig'),
(2, 4, 'Laag'),
(2, 5, 'Bassa'),
(2, 6, 'Baja'),
(3, 1, 'Medium'),
(3, 2, 'Moyenne'),
(3, 3, 'Mittel'),
(3, 4, 'Gemiddeld'),
(3, 5, 'Media'),
(3, 6, 'Medio'),
(4, 1, 'High'),
(4, 2, 'Haute'),
(4, 3, 'Hoch'),
(4, 4, 'Hoog'),
(4, 5, 'Alta'),
(4, 6, 'Alto');

-- --------------------------------------------------------

--
-- Structure de la table `ps_search_engine`
--

DROP TABLE IF EXISTS `ps_search_engine`;
CREATE TABLE IF NOT EXISTS `ps_search_engine` (
  `id_search_engine` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `server` varchar(64) NOT NULL,
  `getvar` varchar(16) NOT NULL,
  PRIMARY KEY (`id_search_engine`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_search_engine`
--

INSERT INTO `ps_search_engine` (`id_search_engine`, `server`, `getvar`) VALUES
(1, 'google', 'q'),
(2, 'aol', 'q'),
(3, 'yandex', 'text'),
(4, 'ask.com', 'q'),
(5, 'nhl.com', 'q'),
(6, 'yahoo', 'p'),
(7, 'baidu', 'wd'),
(8, 'lycos', 'query'),
(9, 'exalead', 'q'),
(10, 'search.live', 'q'),
(11, 'voila', 'rdata'),
(12, 'altavista', 'q'),
(13, 'bing', 'q'),
(14, 'daum', 'q'),
(15, 'eniro', 'search_word'),
(16, 'naver', 'query'),
(17, 'msn', 'q'),
(18, 'netscape', 'query'),
(19, 'cnn', 'query'),
(20, 'about', 'terms'),
(21, 'mamma', 'query'),
(22, 'alltheweb', 'q'),
(23, 'virgilio', 'qs'),
(24, 'alice', 'qs'),
(25, 'najdi', 'q'),
(26, 'mama', 'query'),
(27, 'seznam', 'q'),
(28, 'onet', 'qt'),
(29, 'szukacz', 'q'),
(30, 'yam', 'k'),
(31, 'pchome', 'q'),
(32, 'kvasir', 'q'),
(33, 'sesam', 'q'),
(34, 'ozu', 'q'),
(35, 'terra', 'query'),
(36, 'mynet', 'q'),
(37, 'ekolay', 'q'),
(38, 'rambler', 'words');

-- --------------------------------------------------------

--
-- Structure de la table `ps_search_index`
--

DROP TABLE IF EXISTS `ps_search_index`;
CREATE TABLE IF NOT EXISTS `ps_search_index` (
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_word` int(11) UNSIGNED NOT NULL,
  `weight` smallint(4) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_word`,`id_product`),
  KEY `id_product` (`id_product`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_search_index`
--

INSERT INTO `ps_search_index` (`id_product`, `id_word`, `weight`) VALUES
(20, 2903, 1),
(20, 2904, 1),
(20, 2927, 1),
(20, 2928, 1),
(20, 2929, 1),
(20, 2930, 1),
(20, 2931, 1),
(20, 2932, 1),
(20, 2933, 1),
(20, 2934, 1),
(20, 2935, 1),
(20, 2936, 1),
(20, 2937, 1),
(20, 2938, 1),
(20, 2939, 1),
(20, 2940, 1),
(20, 2941, 1),
(20, 2942, 1),
(20, 2943, 1),
(20, 2944, 1),
(20, 2945, 1),
(20, 2946, 1),
(20, 3267, 1),
(20, 3703, 1),
(20, 3709, 1),
(20, 3715, 1),
(20, 3748, 1),
(20, 3754, 1),
(20, 3757, 1),
(20, 3764, 1),
(20, 3774, 1),
(20, 3916, 1),
(20, 3923, 1),
(20, 3924, 1),
(20, 3926, 1),
(20, 4038, 1),
(20, 4039, 1),
(20, 4040, 1),
(20, 4041, 1),
(20, 4042, 1),
(20, 4043, 1),
(20, 4044, 1),
(20, 4045, 1),
(20, 4046, 1),
(20, 4047, 1),
(20, 4048, 1),
(20, 4049, 1),
(20, 4050, 1),
(20, 4051, 1),
(20, 4052, 1),
(20, 4053, 1),
(20, 4054, 1),
(20, 4055, 1),
(20, 4056, 1),
(20, 4058, 1),
(20, 4059, 1),
(20, 4060, 1),
(20, 4061, 1),
(20, 4062, 1),
(20, 4063, 1),
(20, 4064, 1),
(20, 4065, 1),
(20, 4066, 1),
(20, 4067, 1),
(20, 4068, 1),
(20, 4069, 1),
(20, 4070, 1),
(20, 4071, 1),
(20, 4072, 1),
(20, 4073, 1),
(20, 4074, 1),
(20, 4075, 1),
(20, 4076, 1),
(20, 4077, 1),
(20, 4078, 1),
(20, 4079, 1),
(20, 4088, 1),
(20, 4089, 1),
(20, 4090, 1),
(20, 4091, 1),
(20, 4092, 1),
(20, 4093, 1),
(20, 4094, 1),
(20, 4095, 1),
(20, 4096, 1),
(20, 4097, 1),
(20, 4098, 1),
(20, 4100, 1),
(20, 4101, 1),
(20, 4102, 1),
(20, 4103, 1),
(20, 4104, 1),
(20, 4105, 1),
(20, 4106, 1),
(20, 4107, 1),
(20, 4108, 1),
(20, 4109, 1),
(20, 4110, 1),
(20, 4111, 1),
(20, 4112, 1),
(20, 4113, 1),
(20, 4114, 1),
(20, 4115, 1),
(20, 4116, 1),
(20, 4117, 1),
(20, 4118, 1),
(20, 1089, 2),
(20, 2902, 2),
(20, 2905, 2),
(20, 2906, 2),
(20, 2907, 2),
(20, 2908, 2),
(20, 2909, 2),
(20, 2910, 2),
(20, 2911, 2),
(20, 2912, 2),
(20, 2913, 2),
(20, 2914, 2),
(20, 2915, 2),
(20, 2916, 2),
(20, 2917, 2),
(20, 2918, 2),
(20, 2919, 2),
(20, 2920, 2),
(20, 2921, 2),
(20, 2923, 2),
(20, 2924, 2),
(20, 2925, 2),
(20, 2926, 2),
(20, 4036, 2),
(20, 4037, 2),
(20, 4086, 2),
(20, 4087, 2),
(20, 2947, 3),
(20, 2953, 3),
(20, 2954, 3),
(20, 2959, 3),
(20, 2960, 3),
(20, 2965, 3),
(20, 2966, 3),
(20, 3128, 3),
(20, 3964, 3),
(20, 4057, 4),
(20, 4099, 4),
(20, 2922, 5),
(20, 2900, 6),
(20, 2901, 6),
(20, 2949, 6),
(20, 2950, 6),
(20, 2951, 6),
(20, 2952, 6),
(20, 2955, 6),
(20, 2956, 6),
(20, 2957, 6),
(20, 2958, 6),
(20, 2961, 6),
(20, 2962, 6),
(20, 2963, 6),
(20, 2964, 6),
(20, 4034, 6),
(20, 4035, 6),
(20, 4084, 6),
(20, 4085, 6),
(20, 2898, 8),
(20, 2899, 8),
(20, 3123, 8),
(20, 3124, 8),
(20, 3911, 8),
(20, 3912, 8),
(23, 15, 1),
(23, 2905, 1),
(23, 2907, 1),
(23, 2910, 1),
(23, 2911, 1),
(23, 2912, 1),
(23, 2914, 1),
(23, 2924, 1),
(23, 2944, 1),
(23, 3039, 1),
(23, 3040, 1),
(23, 3041, 1),
(23, 3042, 1),
(23, 3043, 1),
(23, 3044, 1),
(23, 3045, 1),
(23, 3046, 1),
(23, 3047, 1),
(23, 3048, 1),
(23, 3049, 1),
(23, 3050, 1),
(23, 3051, 1),
(23, 3052, 1),
(23, 3053, 1),
(23, 3054, 1),
(23, 3055, 1),
(23, 3056, 1),
(23, 3057, 1),
(23, 3058, 1),
(23, 3059, 1),
(23, 3060, 1),
(23, 3061, 1),
(23, 3062, 1),
(23, 3063, 1),
(23, 3064, 1),
(23, 3065, 1),
(23, 3066, 1),
(23, 3067, 1),
(23, 3068, 1),
(23, 3069, 1),
(23, 3070, 1),
(23, 3071, 1),
(23, 3072, 1),
(23, 3073, 1),
(23, 3074, 1),
(23, 3075, 1),
(23, 3076, 1),
(23, 3077, 1),
(23, 3078, 1),
(23, 3079, 1),
(23, 3080, 1),
(23, 3081, 1),
(23, 3082, 1),
(23, 3083, 1),
(23, 3084, 1),
(23, 3085, 1),
(23, 3086, 1),
(23, 3087, 1),
(23, 3088, 1),
(23, 3089, 1),
(23, 3703, 1),
(23, 3715, 1),
(23, 3719, 1),
(23, 3742, 1),
(23, 3752, 1),
(23, 3754, 1),
(23, 3755, 1),
(23, 3757, 1),
(23, 3767, 1),
(23, 3788, 1),
(23, 3789, 1),
(23, 3915, 1),
(23, 3916, 1),
(23, 3917, 1),
(23, 3918, 1),
(23, 3919, 1),
(23, 3920, 1),
(23, 3921, 1),
(23, 3922, 1),
(23, 3923, 1),
(23, 3924, 1),
(23, 3925, 1),
(23, 3926, 1),
(23, 3927, 1),
(23, 3928, 1),
(23, 3929, 1),
(23, 3930, 1),
(23, 3931, 1),
(23, 3932, 1),
(23, 3933, 1),
(23, 3934, 1),
(23, 3935, 1),
(23, 3936, 1),
(23, 3937, 1),
(23, 3938, 1),
(23, 3939, 1),
(23, 3940, 1),
(23, 3941, 1),
(23, 3942, 1),
(23, 3943, 1),
(23, 3944, 1),
(23, 3945, 1),
(23, 3946, 1),
(23, 3947, 1),
(23, 3948, 1),
(23, 3949, 1),
(23, 3950, 1),
(23, 3951, 1),
(23, 3952, 1),
(23, 3953, 1),
(23, 3954, 1),
(23, 3955, 1),
(23, 3956, 1),
(23, 3957, 1),
(23, 3958, 1),
(23, 3959, 1),
(23, 3960, 1),
(23, 3961, 1),
(23, 3962, 1),
(23, 3963, 1),
(23, 2947, 3),
(23, 2953, 3),
(23, 2959, 3),
(23, 2965, 3),
(23, 3090, 3),
(23, 3105, 3),
(23, 3112, 3),
(23, 3119, 3),
(23, 3128, 3),
(23, 3129, 3),
(23, 3781, 3),
(23, 3964, 3),
(23, 2898, 6),
(23, 2949, 6),
(23, 2950, 6),
(23, 2955, 6),
(23, 2956, 6),
(23, 2961, 6),
(23, 2962, 6),
(23, 3103, 6),
(23, 3104, 6),
(23, 3109, 6),
(23, 3110, 6),
(23, 3111, 6),
(23, 3116, 6),
(23, 3117, 6),
(23, 3118, 6),
(23, 3123, 6),
(23, 3124, 6),
(23, 3125, 6),
(23, 3126, 6),
(23, 3127, 6),
(23, 3911, 6),
(23, 2899, 7),
(23, 3036, 7),
(23, 3037, 7),
(23, 3038, 7),
(23, 3733, 7),
(23, 3912, 7),
(23, 3913, 7),
(23, 3914, 7),
(24, 2910, 1),
(24, 2911, 1),
(24, 2915, 1),
(24, 2924, 1),
(24, 2931, 1),
(24, 2933, 1),
(24, 2937, 1),
(24, 3036, 1),
(24, 3041, 1),
(24, 3042, 1),
(24, 3058, 1),
(24, 3072, 1),
(24, 3074, 1),
(24, 3075, 1),
(24, 3083, 1),
(24, 3090, 1),
(24, 3135, 1),
(24, 3136, 1),
(24, 3137, 1),
(24, 3138, 1),
(24, 3139, 1),
(24, 3140, 1),
(24, 3141, 1),
(24, 3142, 1),
(24, 3143, 1),
(24, 3144, 1),
(24, 3145, 1),
(24, 3146, 1),
(24, 3147, 1),
(24, 3148, 1),
(24, 3149, 1),
(24, 3150, 1),
(24, 3151, 1),
(24, 3152, 1),
(24, 3153, 1),
(24, 3154, 1),
(24, 3155, 1),
(24, 3156, 1),
(24, 3157, 1),
(24, 3158, 1),
(24, 3159, 1),
(24, 3160, 1),
(24, 3161, 1),
(24, 3162, 1),
(24, 3163, 1),
(24, 3164, 1),
(24, 3165, 1),
(24, 3166, 1),
(24, 3167, 1),
(24, 3168, 1),
(24, 3169, 1),
(24, 3170, 1),
(24, 3171, 1),
(24, 3172, 1),
(24, 3173, 1),
(24, 3174, 1),
(24, 3175, 1),
(24, 3176, 1),
(24, 3177, 1),
(24, 3178, 1),
(24, 3179, 1),
(24, 3180, 1),
(24, 3181, 1),
(24, 3182, 1),
(24, 3183, 1),
(24, 3184, 1),
(24, 3185, 1),
(24, 3186, 1),
(24, 3187, 1),
(24, 3188, 1),
(24, 3189, 1),
(24, 3190, 1),
(24, 3191, 1),
(24, 3192, 1),
(24, 3193, 1),
(24, 3194, 1),
(24, 3195, 1),
(24, 3196, 1),
(24, 3197, 1),
(24, 3198, 1),
(24, 3199, 1),
(24, 3200, 1),
(24, 3201, 1),
(24, 3202, 1),
(24, 3203, 1),
(24, 3204, 1),
(24, 3205, 1),
(24, 3206, 1),
(24, 3207, 1),
(24, 3208, 1),
(24, 3209, 1),
(24, 3210, 1),
(24, 3211, 1),
(24, 3212, 1),
(24, 3213, 1),
(24, 3214, 1),
(24, 3215, 1),
(24, 3216, 1),
(24, 3217, 1),
(24, 3218, 1),
(24, 3219, 1),
(24, 3220, 1),
(24, 3221, 1),
(24, 3222, 1),
(24, 3223, 1),
(24, 3224, 1),
(24, 3225, 1),
(24, 3226, 1),
(24, 3227, 1),
(24, 3228, 1),
(24, 3708, 1),
(24, 3709, 1),
(24, 3710, 1),
(24, 3711, 1),
(24, 3712, 1),
(24, 3713, 1),
(24, 3714, 1),
(24, 3715, 1),
(24, 3716, 1),
(24, 3717, 1),
(24, 3718, 1),
(24, 3719, 1),
(24, 3720, 1),
(24, 3721, 1),
(24, 3722, 1),
(24, 3723, 1),
(24, 3724, 1),
(24, 3725, 1),
(24, 3726, 1),
(24, 3727, 1),
(24, 3728, 1),
(24, 3729, 1),
(24, 3730, 1),
(24, 3731, 1),
(24, 3732, 1),
(24, 3733, 1),
(24, 3734, 1),
(24, 3735, 1),
(24, 3736, 1),
(24, 3737, 1),
(24, 3738, 1),
(24, 3739, 1),
(24, 3740, 1),
(24, 3741, 1),
(24, 3742, 1),
(24, 3743, 1),
(24, 3744, 1),
(24, 3745, 1),
(24, 3746, 1),
(24, 3747, 1),
(24, 3748, 1),
(24, 3749, 1),
(24, 3750, 1),
(24, 3751, 1),
(24, 3752, 1),
(24, 3753, 1),
(24, 3754, 1),
(24, 3755, 1),
(24, 3756, 1),
(24, 3757, 1),
(24, 3758, 1),
(24, 3759, 1),
(24, 3760, 1),
(24, 3761, 1),
(24, 3762, 1),
(24, 3763, 1),
(24, 3764, 1),
(24, 3765, 1),
(24, 3766, 1),
(24, 3767, 1),
(24, 3768, 1),
(24, 3769, 1),
(24, 3770, 1),
(24, 3771, 1),
(24, 3772, 1),
(24, 3773, 1),
(24, 3774, 1),
(24, 3775, 1),
(24, 3776, 1),
(24, 3777, 1),
(24, 3778, 1),
(24, 3779, 1),
(24, 3780, 1),
(24, 3781, 1),
(24, 3782, 1),
(24, 3783, 1),
(24, 3784, 1),
(24, 3785, 1),
(24, 3786, 1),
(24, 3787, 1),
(24, 3788, 1),
(24, 3789, 1),
(24, 3790, 1),
(24, 3791, 1),
(24, 3792, 1),
(24, 3793, 1),
(24, 3794, 1),
(24, 3795, 1),
(24, 3796, 1),
(24, 3797, 1),
(24, 3798, 1),
(24, 3799, 1),
(24, 3800, 1),
(24, 3801, 1),
(24, 3802, 1),
(24, 3803, 1),
(24, 3804, 1),
(24, 3805, 1),
(24, 3806, 1),
(24, 3807, 1),
(24, 3808, 1),
(24, 3809, 1),
(24, 3810, 1),
(24, 3811, 1),
(24, 3812, 1),
(24, 3813, 1),
(24, 3814, 1),
(24, 3815, 1),
(24, 3229, 3),
(24, 3252, 3),
(24, 3258, 3),
(24, 3265, 3),
(24, 3272, 3),
(24, 3816, 3),
(24, 3132, 6),
(24, 3133, 6),
(24, 3247, 6),
(24, 3248, 6),
(24, 3249, 6),
(24, 3250, 6),
(24, 3251, 6),
(24, 3253, 6),
(24, 3254, 6),
(24, 3255, 6),
(24, 3256, 6),
(24, 3257, 6),
(24, 3259, 6),
(24, 3260, 6),
(24, 3261, 6),
(24, 3262, 6),
(24, 3263, 6),
(24, 3264, 6),
(24, 3266, 6),
(24, 3267, 6),
(24, 3268, 6),
(24, 3269, 6),
(24, 3270, 6),
(24, 3271, 6),
(24, 3705, 6),
(24, 3706, 6),
(24, 2944, 7),
(24, 3130, 7),
(24, 3131, 7),
(24, 3134, 7),
(24, 3702, 7),
(24, 3703, 7),
(24, 3704, 7),
(24, 3707, 7);

-- --------------------------------------------------------

--
-- Structure de la table `ps_search_word`
--

DROP TABLE IF EXISTS `ps_search_word`;
CREATE TABLE IF NOT EXISTS `ps_search_word` (
  `id_word` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `word` varchar(30) NOT NULL,
  PRIMARY KEY (`id_word`),
  UNIQUE KEY `id_lang` (`id_lang`,`id_shop`,`word`)
) ENGINE=InnoDB AUTO_INCREMENT=4295 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_search_word`
--

INSERT INTO `ps_search_word` (`id_word`, `id_shop`, `id_lang`, `word`) VALUES
(3203, 1, 1, '188'),
(3082, 1, 1, '340'),
(3146, 1, 1, 'abord'),
(3061, 1, 1, 'adversaires'),
(3174, 1, 1, 'ages'),
(3180, 1, 1, 'agreable'),
(3139, 1, 1, 'ailleurs'),
(3178, 1, 1, 'ameliore'),
(2904, 1, 1, 'anglaise'),
(3131, 1, 1, 'anneaux'),
(3200, 1, 1, 'ans'),
(3141, 1, 1, 'apprecier'),
(3054, 1, 1, 'assassiner'),
(3154, 1, 1, 'aussi'),
(2940, 1, 1, 'aux'),
(3138, 1, 1, 'avance'),
(3209, 1, 1, 'avantage'),
(2928, 1, 1, 'aventure'),
(3171, 1, 1, 'aventures'),
(2908, 1, 1, 'avez'),
(3069, 1, 1, 'avoir'),
(3175, 1, 1, 'base'),
(2909, 1, 1, 'besoin'),
(3065, 1, 1, 'bien'),
(3076, 1, 1, 'bloc'),
(3072, 1, 1, 'boite'),
(3208, 1, 1, 'campagne'),
(3223, 1, 1, 'campagne-'),
(3067, 1, 1, 'car'),
(3083, 1, 1, 'cartes'),
(3078, 1, 1, 'casier'),
(3166, 1, 1, 'ceci'),
(3071, 1, 1, 'cette'),
(3064, 1, 1, 'choisissez'),
(3196, 1, 1, 'classiques'),
(3192, 1, 1, 'communaute'),
(3215, 1, 1, 'compteurs'),
(3046, 1, 1, 'conquete'),
(3047, 1, 1, 'construisez'),
(3202, 1, 1, 'contenu'),
(2910, 1, 1, 'contient'),
(3063, 1, 1, 'controle'),
(3163, 1, 1, 'cours'),
(3162, 1, 1, 'creons'),
(3058, 1, 1, 'dans'),
(3170, 1, 1, 'debut'),
(3195, 1, 1, 'decouvrez'),
(3213, 1, 1, 'degats'),
(3226, 1, 1, 'degats-'),
(3051, 1, 1, 'demons'),
(3150, 1, 1, 'depart'),
(3199, 1, 1, 'depuis'),
(2944, 1, 1, 'des'),
(3193, 1, 1, 'digne'),
(3189, 1, 1, 'disponibles'),
(2926, 1, 1, 'donjon'),
(2906, 1, 1, 'dont'),
(2899, 1, 1, 'dragons'),
(3044, 1, 1, 'drow'),
(3049, 1, 1, 'drows'),
(2898, 1, 1, 'dungeons'),
(3133, 1, 1, 'edition'),
(3187, 1, 1, 'elements'),
(3039, 1, 1, 'emparez'),
(3088, 1, 1, 'emparez-'),
(2921, 1, 1, 'endossera'),
(3056, 1, 1, 'ennemies'),
(2939, 1, 1, 'ensuite'),
(2920, 1, 1, 'entre'),
(2935, 1, 1, 'entree'),
(3172, 1, 1, 'epiques'),
(2933, 1, 1, 'est'),
(3176, 1, 1, 'evolutif'),
(3179, 1, 1, 'experience'),
(3197, 1, 1, 'fait'),
(3050, 1, 1, 'fanatiques'),
(3210, 1, 1, 'fardeau'),
(3224, 1, 1, 'fardeau-'),
(3145, 1, 1, 'faut'),
(3191, 1, 1, 'former'),
(3059, 1, 1, 'forteresse'),
(3160, 1, 1, 'forts'),
(3165, 1, 1, 'gardons'),
(2946, 1, 1, 'guide'),
(3205, 1, 1, 'heros'),
(3220, 1, 1, 'heros-'),
(3144, 1, 1, 'histoire'),
(3229, 1, 1, 'home'),
(3181, 1, 1, 'ideal'),
(2936, 1, 1, 'ideale'),
(3155, 1, 1, 'important'),
(3057, 1, 1, 'infiltrer'),
(2901, 1, 1, 'initiation'),
(3136, 1, 1, 'jamais'),
(3132, 1, 1, 'jce'),
(3085, 1, 1, 'jetons'),
(2924, 1, 1, 'jeu'),
(2947, 1, 1, 'jeux'),
(3204, 1, 1, 'joueur'),
(3219, 1, 1, 'joueur-'),
(2917, 1, 1, 'joueurs'),
(2915, 1, 1, 'jusqu'),
(3142, 1, 1, 'juste'),
(2900, 1, 1, 'kit'),
(3184, 1, 1, 'lancer'),
(3036, 1, 1, 'les'),
(3159, 1, 1, 'liens'),
(2941, 1, 1, 'livres'),
(3074, 1, 1, 'livret'),
(15, 1, 1, 'long'),
(3135, 1, 1, 'magicien'),
(3201, 1, 1, 'maintenant'),
(3043, 1, 1, 'maison'),
(2925, 1, 1, 'maitre'),
(2943, 1, 1, 'manuel'),
(3080, 1, 1, 'marche'),
(3217, 1, 1, 'marqueur'),
(3211, 1, 1, 'marqueurs'),
(3177, 1, 1, 'materiel'),
(3216, 1, 1, 'menace'),
(3228, 1, 1, 'menace-'),
(2938, 1, 1, 'menera'),
(2945, 1, 1, 'monstres'),
(3186, 1, 1, 'mythique'),
(2923, 1, 1, 'narrateur'),
(3194, 1, 1, 'nom'),
(3161, 1, 1, 'nous'),
(3038, 1, 1, 'ombreterre'),
(3140, 1, 1, 'parfois'),
(3045, 1, 1, 'partez'),
(2914, 1, 1, 'partie'),
(3190, 1, 1, 'partir'),
(3152, 1, 1, 'pas'),
(3158, 1, 1, 'peripeties'),
(3185, 1, 1, 'periple'),
(3182, 1, 1, 'personnes'),
(3068, 1, 1, 'peut'),
(2918, 1, 1, 'peuvent'),
(3073, 1, 1, 'plateau'),
(3081, 1, 1, 'plateaux'),
(2931, 1, 1, 'plus'),
(3149, 1, 1, 'point'),
(2934, 1, 1, 'porte'),
(2911, 1, 1, 'pour'),
(3086, 1, 1, 'predecoupes'),
(3151, 1, 1, 'premier'),
(2913, 1, 1, 'premiere'),
(3062, 1, 1, 'prendre'),
(3040, 1, 1, 'prenez'),
(3167, 1, 1, 'prenons'),
(2942, 1, 1, 'principaux'),
(2919, 1, 1, 'profiter'),
(3214, 1, 1, 'progression'),
(3227, 1, 1, 'progression-'),
(3052, 1, 1, 'puis'),
(3156, 1, 1, 'que'),
(3207, 1, 1, 'quete'),
(3222, 1, 1, 'quete-'),
(2937, 1, 1, 'qui'),
(3079, 1, 1, 'rangement'),
(3048, 1, 1, 'recruter'),
(3075, 1, 1, 'regles'),
(3218, 1, 1, 'regles-'),
(2927, 1, 1, 'rejoignez'),
(3206, 1, 1, 'rencontre'),
(3221, 1, 1, 'rencontre-'),
(3212, 1, 1, 'ressource'),
(3225, 1, 1, 'ressource-'),
(3137, 1, 1, 'retard'),
(3169, 1, 1, 'retourner'),
(3147, 1, 1, 'revenir'),
(3198, 1, 1, 'rever'),
(3134, 1, 1, 'revisee'),
(2922, 1, 1, 'role'),
(3164, 1, 1, 'route'),
(2930, 1, 1, 'savoir'),
(3077, 1, 1, 'score'),
(3084, 1, 1, 'seide'),
(3130, 1, 1, 'seigneur'),
(3157, 1, 1, 'ses'),
(1089, 1, 1, 'set'),
(3070, 1, 1, 'seule'),
(2916, 1, 1, 'six'),
(3090, 1, 1, 'societe'),
(3148, 1, 1, 'son'),
(3188, 1, 1, 'sont'),
(2929, 1, 1, 'souhaitez'),
(2902, 1, 1, 'starter'),
(3066, 1, 1, 'strategie'),
(2932, 1, 1, 'sur'),
(3168, 1, 1, 'temps'),
(3041, 1, 1, 'tete'),
(3173, 1, 1, 'tous'),
(2905, 1, 1, 'tout'),
(3037, 1, 1, 'tyrans'),
(3042, 1, 1, 'une'),
(3055, 1, 1, 'unites'),
(3053, 1, 1, 'utilisez'),
(3089, 1, 1, 'utilisez-les'),
(3087, 1, 1, 'utilisezles'),
(3143, 1, 1, 'valeur'),
(2903, 1, 1, 'version'),
(3060, 1, 1, 'vos'),
(2912, 1, 1, 'votre'),
(3183, 1, 1, 'voudraient'),
(2907, 1, 1, 'vous'),
(3153, 1, 1, 'voyage'),
(3252, 1, 2, 'accueil'),
(3248, 1, 2, 'anneaux'),
(2950, 1, 2, 'dragons'),
(2949, 1, 2, 'dungeons'),
(3250, 1, 2, 'edition'),
(2952, 1, 2, 'initiation'),
(3249, 1, 2, 'jce'),
(2953, 1, 2, 'jeux'),
(2951, 1, 2, 'kit'),
(3104, 1, 2, 'ombreterre'),
(3251, 1, 2, 'revisee'),
(2954, 1, 2, 'role'),
(3247, 1, 2, 'seigneur'),
(3105, 1, 2, 'societe'),
(3103, 1, 2, 'tyrans'),
(3254, 1, 3, 'anneaux'),
(2956, 1, 3, 'dragons'),
(2955, 1, 3, 'dungeons'),
(3256, 1, 3, 'edition'),
(2958, 1, 3, 'initiation'),
(3255, 1, 3, 'jce'),
(2959, 1, 3, 'jeux'),
(2957, 1, 3, 'kit'),
(3109, 1, 3, 'les'),
(3111, 1, 3, 'ombreterre'),
(3257, 1, 3, 'revisee'),
(2960, 1, 3, 'role'),
(3253, 1, 3, 'seigneur'),
(3112, 1, 3, 'societe'),
(3258, 1, 3, 'startseite'),
(3110, 1, 3, 'tyrans'),
(3261, 1, 4, 'anneaux'),
(3260, 1, 4, 'des'),
(2962, 1, 4, 'dragons'),
(2961, 1, 4, 'dungeons'),
(3263, 1, 4, 'edition'),
(3265, 1, 4, 'home'),
(2964, 1, 4, 'initiation'),
(3262, 1, 4, 'jce'),
(2965, 1, 4, 'jeux'),
(2963, 1, 4, 'kit'),
(3116, 1, 4, 'les'),
(3118, 1, 4, 'ombreterre'),
(3264, 1, 4, 'revisee'),
(2966, 1, 4, 'role'),
(3259, 1, 4, 'seigneur'),
(3119, 1, 4, 'societe'),
(3117, 1, 4, 'tyrans'),
(4039, 1, 5, 'anglaise'),
(3268, 1, 5, 'anneaux'),
(4074, 1, 5, 'aux'),
(4063, 1, 5, 'aventure'),
(4043, 1, 5, 'avez'),
(4044, 1, 5, 'besoin'),
(4045, 1, 5, 'contient'),
(3267, 1, 5, 'des'),
(4061, 1, 5, 'donjon'),
(4041, 1, 5, 'dont'),
(3124, 1, 5, 'dragons'),
(3123, 1, 5, 'dungeons'),
(3270, 1, 5, 'edition'),
(4056, 1, 5, 'endossera'),
(4073, 1, 5, 'ensuite'),
(4055, 1, 5, 'entre'),
(4070, 1, 5, 'entree'),
(4068, 1, 5, 'est'),
(4079, 1, 5, 'guide'),
(3272, 1, 5, 'home'),
(4071, 1, 5, 'ideale'),
(4035, 1, 5, 'initiation'),
(3269, 1, 5, 'jce'),
(4059, 1, 5, 'jeu'),
(3128, 1, 5, 'jeux'),
(4052, 1, 5, 'joueurs'),
(4050, 1, 5, 'jusqu'),
(4034, 1, 5, 'kit'),
(3125, 1, 5, 'les'),
(4075, 1, 5, 'livres'),
(4060, 1, 5, 'maitre'),
(4077, 1, 5, 'manuel'),
(4072, 1, 5, 'menera'),
(4078, 1, 5, 'monstres'),
(4058, 1, 5, 'narrateur'),
(3127, 1, 5, 'ombreterre'),
(4049, 1, 5, 'partie'),
(4053, 1, 5, 'peuvent'),
(4066, 1, 5, 'plus'),
(4069, 1, 5, 'porte'),
(4046, 1, 5, 'pour'),
(4048, 1, 5, 'premiere'),
(4076, 1, 5, 'principaux'),
(4054, 1, 5, 'profiter'),
(4062, 1, 5, 'rejoignez'),
(3271, 1, 5, 'revisee'),
(4057, 1, 5, 'role'),
(4065, 1, 5, 'savoir'),
(3266, 1, 5, 'seigneur'),
(4037, 1, 5, 'set'),
(4051, 1, 5, 'six'),
(3129, 1, 5, 'societe'),
(4064, 1, 5, 'souhaitez'),
(4036, 1, 5, 'starter'),
(4067, 1, 5, 'sur'),
(4040, 1, 5, 'tout'),
(3126, 1, 5, 'tyrans'),
(4038, 1, 5, 'version'),
(4047, 1, 5, 'votre'),
(4042, 1, 5, 'vous'),
(3790, 1, 6, '188'),
(3957, 1, 6, '340'),
(3722, 1, 6, 'abord'),
(3939, 1, 6, 'adversaires'),
(3751, 1, 6, 'ages'),
(3761, 1, 6, 'agreable'),
(3713, 1, 6, 'ailleurs'),
(3759, 1, 6, 'ameliore'),
(4089, 1, 6, 'anglaise'),
(3704, 1, 6, 'anneaux'),
(3785, 1, 6, 'ans'),
(3716, 1, 6, 'apprecier'),
(3933, 1, 6, 'assassiner'),
(3729, 1, 6, 'aussi'),
(4113, 1, 6, 'aux'),
(3712, 1, 6, 'avance'),
(3796, 1, 6, 'avantage'),
(4104, 1, 6, 'aventure'),
(3747, 1, 6, 'aventures'),
(4091, 1, 6, 'avez'),
(3947, 1, 6, 'avoir'),
(3753, 1, 6, 'base'),
(4092, 1, 6, 'besoin'),
(3943, 1, 6, 'bien'),
(3951, 1, 6, 'bloc'),
(3752, 1, 6, 'boite'),
(3795, 1, 6, 'campagne'),
(3810, 1, 6, 'campagne-'),
(3945, 1, 6, 'car'),
(3755, 1, 6, 'cartes'),
(3953, 1, 6, 'casier'),
(3741, 1, 6, 'ceci'),
(3949, 1, 6, 'cette'),
(3942, 1, 6, 'choisissez'),
(3780, 1, 6, 'classiques'),
(3776, 1, 6, 'communaute'),
(3802, 1, 6, 'compteurs'),
(3921, 1, 6, 'conquete'),
(3922, 1, 6, 'construisez'),
(3787, 1, 6, 'contenu'),
(3757, 1, 6, 'contient'),
(3941, 1, 6, 'controle'),
(3738, 1, 6, 'cours'),
(3737, 1, 6, 'creons'),
(3767, 1, 6, 'dans'),
(3746, 1, 6, 'debut'),
(3779, 1, 6, 'decouvrez'),
(3800, 1, 6, 'degats'),
(3813, 1, 6, 'degats-'),
(3930, 1, 6, 'demons'),
(3725, 1, 6, 'depart'),
(3784, 1, 6, 'depuis'),
(3703, 1, 6, 'des'),
(3777, 1, 6, 'digne'),
(3772, 1, 6, 'disponibles'),
(4102, 1, 6, 'donjon'),
(4090, 1, 6, 'dont'),
(3912, 1, 6, 'dragons'),
(3919, 1, 6, 'drow'),
(3928, 1, 6, 'drows'),
(3911, 1, 6, 'dungeons'),
(3706, 1, 6, 'edition'),
(3770, 1, 6, 'elements'),
(3915, 1, 6, 'emparez'),
(3962, 1, 6, 'emparez-'),
(4098, 1, 6, 'endossera'),
(3935, 1, 6, 'ennemies'),
(4112, 1, 6, 'ensuite'),
(4109, 1, 6, 'entree'),
(3749, 1, 6, 'epiques'),
(3709, 1, 6, 'est'),
(3756, 1, 6, 'evolutif'),
(3760, 1, 6, 'experience'),
(3782, 1, 6, 'fait'),
(3929, 1, 6, 'fanatiques'),
(3797, 1, 6, 'fardeau'),
(3811, 1, 6, 'fardeau-'),
(3721, 1, 6, 'faut'),
(3775, 1, 6, 'former'),
(3937, 1, 6, 'forteresse'),
(3735, 1, 6, 'forts'),
(3740, 1, 6, 'gardons'),
(4118, 1, 6, 'guide'),
(3792, 1, 6, 'heros'),
(3807, 1, 6, 'heros-'),
(3720, 1, 6, 'histoire'),
(3762, 1, 6, 'ideal'),
(4110, 1, 6, 'ideale'),
(3730, 1, 6, 'important'),
(3936, 1, 6, 'infiltrer'),
(3816, 1, 6, 'inicio'),
(4085, 1, 6, 'initiation'),
(3710, 1, 6, 'jamais'),
(3705, 1, 6, 'jce'),
(3959, 1, 6, 'jetons'),
(3754, 1, 6, 'jeu'),
(3964, 1, 6, 'jeux'),
(3791, 1, 6, 'joueur'),
(3806, 1, 6, 'joueur-'),
(4095, 1, 6, 'joueurs'),
(3774, 1, 6, 'jusqu'),
(3717, 1, 6, 'juste'),
(4084, 1, 6, 'kit'),
(3766, 1, 6, 'lancer'),
(3733, 1, 6, 'les'),
(3734, 1, 6, 'liens'),
(4114, 1, 6, 'livres'),
(3788, 1, 6, 'livret'),
(3925, 1, 6, 'long'),
(3708, 1, 6, 'magicien'),
(3786, 1, 6, 'maintenant'),
(3918, 1, 6, 'maison'),
(4101, 1, 6, 'maitre'),
(4116, 1, 6, 'manuel'),
(3955, 1, 6, 'marche'),
(3804, 1, 6, 'marqueur'),
(3798, 1, 6, 'marqueurs'),
(3758, 1, 6, 'materiel'),
(3803, 1, 6, 'menace'),
(3815, 1, 6, 'menace-'),
(4111, 1, 6, 'menera'),
(4117, 1, 6, 'monstres'),
(3769, 1, 6, 'mythique'),
(4100, 1, 6, 'narrateur'),
(3778, 1, 6, 'nom'),
(3736, 1, 6, 'nous'),
(3914, 1, 6, 'ombreterre'),
(3714, 1, 6, 'parfois'),
(3920, 1, 6, 'partez'),
(3926, 1, 6, 'partie'),
(3773, 1, 6, 'partir'),
(3727, 1, 6, 'pas'),
(3732, 1, 6, 'peripeties'),
(3768, 1, 6, 'periple'),
(3763, 1, 6, 'personnes'),
(3946, 1, 6, 'peut'),
(4096, 1, 6, 'peuvent'),
(3950, 1, 6, 'plateau'),
(3956, 1, 6, 'plateaux'),
(3748, 1, 6, 'plus'),
(3724, 1, 6, 'point'),
(4108, 1, 6, 'porte'),
(3715, 1, 6, 'pour'),
(3960, 1, 6, 'predecoupes'),
(3726, 1, 6, 'premier'),
(4093, 1, 6, 'premiere'),
(3940, 1, 6, 'prendre'),
(3917, 1, 6, 'prenez'),
(3743, 1, 6, 'prenons'),
(4115, 1, 6, 'principaux'),
(4097, 1, 6, 'profiter'),
(3801, 1, 6, 'progression'),
(3814, 1, 6, 'progression-'),
(3931, 1, 6, 'puis'),
(3794, 1, 6, 'quete'),
(3809, 1, 6, 'quete-'),
(3764, 1, 6, 'qui'),
(3954, 1, 6, 'rangement'),
(3927, 1, 6, 'recruter'),
(3789, 1, 6, 'regles'),
(3805, 1, 6, 'regles-'),
(4103, 1, 6, 'rejoignez'),
(3793, 1, 6, 'rencontre'),
(3808, 1, 6, 'rencontre-'),
(3799, 1, 6, 'ressource'),
(3812, 1, 6, 'ressource-'),
(3711, 1, 6, 'retard'),
(3745, 1, 6, 'retourner'),
(3723, 1, 6, 'revenir'),
(3783, 1, 6, 'rever'),
(3707, 1, 6, 'revisee'),
(4099, 1, 6, 'role'),
(3739, 1, 6, 'route'),
(4106, 1, 6, 'savoir'),
(3952, 1, 6, 'score'),
(3958, 1, 6, 'seide'),
(3702, 1, 6, 'seigneur'),
(3731, 1, 6, 'ses'),
(4087, 1, 6, 'set'),
(3948, 1, 6, 'seule'),
(4094, 1, 6, 'six'),
(3781, 1, 6, 'societe'),
(3771, 1, 6, 'sont'),
(4105, 1, 6, 'souhaitez'),
(4086, 1, 6, 'starter'),
(3944, 1, 6, 'strategie'),
(4107, 1, 6, 'sur'),
(3744, 1, 6, 'temps'),
(3742, 1, 6, 'tete'),
(3750, 1, 6, 'tous'),
(3924, 1, 6, 'tout'),
(3913, 1, 6, 'tyrans'),
(3719, 1, 6, 'une'),
(3934, 1, 6, 'unites'),
(3932, 1, 6, 'utilisez'),
(3963, 1, 6, 'utilisez-les'),
(3961, 1, 6, 'utilisezles'),
(3718, 1, 6, 'valeur'),
(4088, 1, 6, 'version'),
(3938, 1, 6, 'vos'),
(3923, 1, 6, 'votre'),
(3765, 1, 6, 'voudraient'),
(3916, 1, 6, 'vous'),
(3728, 1, 6, 'voyage');

-- --------------------------------------------------------

--
-- Structure de la table `ps_shop`
--

DROP TABLE IF EXISTS `ps_shop`;
CREATE TABLE IF NOT EXISTS `ps_shop` (
  `id_shop` int(11) NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_category` int(11) NOT NULL,
  `theme_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_shop`),
  KEY `IDX_CBDFBB9EF5C9E40` (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_shop`
--

INSERT INTO `ps_shop` (`id_shop`, `id_shop_group`, `name`, `color`, `id_category`, `theme_name`, `active`, `deleted`) VALUES
(1, 1, 'demo', '', 2, 'myshop', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_shop_group`
--

DROP TABLE IF EXISTS `ps_shop_group`;
CREATE TABLE IF NOT EXISTS `ps_shop_group` (
  `id_shop_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `share_customer` tinyint(1) NOT NULL,
  `share_order` tinyint(1) NOT NULL,
  `share_stock` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_shop_group`
--

INSERT INTO `ps_shop_group` (`id_shop_group`, `name`, `color`, `share_customer`, `share_order`, `share_stock`, `active`, `deleted`) VALUES
(1, 'Default', '', 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_shop_url`
--

DROP TABLE IF EXISTS `ps_shop_url`;
CREATE TABLE IF NOT EXISTS `ps_shop_url` (
  `id_shop_url` int(11) NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) NOT NULL,
  `domain` varchar(150) NOT NULL,
  `domain_ssl` varchar(150) NOT NULL,
  `physical_uri` varchar(64) NOT NULL,
  `virtual_uri` varchar(64) NOT NULL,
  `main` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_shop_url`),
  KEY `IDX_279F19DA274A50A0` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_shop_url`
--

INSERT INTO `ps_shop_url` (`id_shop_url`, `id_shop`, `domain`, `domain_ssl`, `physical_uri`, `virtual_uri`, `main`, `active`) VALUES
(1, 1, 'localhost', 'localhost', '/prestashop/', '', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_smarty_cache`
--

DROP TABLE IF EXISTS `ps_smarty_cache`;
CREATE TABLE IF NOT EXISTS `ps_smarty_cache` (
  `id_smarty_cache` char(40) NOT NULL,
  `name` char(40) NOT NULL,
  `cache_id` varchar(254) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id_smarty_cache`),
  KEY `name` (`name`),
  KEY `cache_id` (`cache_id`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_smarty_last_flush`
--

DROP TABLE IF EXISTS `ps_smarty_last_flush`;
CREATE TABLE IF NOT EXISTS `ps_smarty_last_flush` (
  `type` enum('compile','template') NOT NULL,
  `last_flush` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_smarty_lazy_cache`
--

DROP TABLE IF EXISTS `ps_smarty_lazy_cache`;
CREATE TABLE IF NOT EXISTS `ps_smarty_lazy_cache` (
  `template_hash` varchar(32) NOT NULL DEFAULT '',
  `cache_id` varchar(191) NOT NULL DEFAULT '',
  `compile_id` varchar(32) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`template_hash`,`cache_id`,`compile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_specific_price`
--

DROP TABLE IF EXISTS `ps_specific_price`;
CREATE TABLE IF NOT EXISTS `ps_specific_price` (
  `id_specific_price` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule` int(11) UNSIGNED NOT NULL,
  `id_cart` int(11) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `price` decimal(20,6) NOT NULL,
  `from_quantity` mediumint(8) UNSIGNED NOT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_tax` tinyint(1) NOT NULL DEFAULT '1',
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  PRIMARY KEY (`id_specific_price`),
  UNIQUE KEY `id_product_2` (`id_product`,`id_product_attribute`,`id_customer`,`id_cart`,`from`,`to`,`id_shop`,`id_shop_group`,`id_currency`,`id_country`,`id_group`,`from_quantity`,`id_specific_price_rule`),
  KEY `id_product` (`id_product`,`id_shop`,`id_currency`,`id_country`,`id_group`,`id_customer`,`from_quantity`,`from`,`to`),
  KEY `from_quantity` (`from_quantity`),
  KEY `id_specific_price_rule` (`id_specific_price_rule`),
  KEY `id_cart` (`id_cart`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_shop` (`id_shop`),
  KEY `id_customer` (`id_customer`),
  KEY `from` (`from`),
  KEY `to` (`to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_specific_price_priority`
--

DROP TABLE IF EXISTS `ps_specific_price_priority`;
CREATE TABLE IF NOT EXISTS `ps_specific_price_priority` (
  `id_specific_price_priority` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `priority` varchar(80) NOT NULL,
  PRIMARY KEY (`id_specific_price_priority`,`id_product`),
  UNIQUE KEY `id_product` (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_specific_price_priority`
--

INSERT INTO `ps_specific_price_priority` (`id_specific_price_priority`, `id_product`, `priority`) VALUES
(1, 20, 'id_shop;id_currency;id_country;id_group'),
(4, 23, 'id_shop;id_currency;id_country;id_group'),
(5, 24, 'id_shop;id_currency;id_country;id_group');

-- --------------------------------------------------------

--
-- Structure de la table `ps_specific_price_rule`
--

DROP TABLE IF EXISTS `ps_specific_price_rule`;
CREATE TABLE IF NOT EXISTS `ps_specific_price_rule` (
  `id_specific_price_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `from_quantity` mediumint(8) UNSIGNED NOT NULL,
  `price` decimal(20,6) DEFAULT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_tax` tinyint(1) NOT NULL DEFAULT '1',
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  PRIMARY KEY (`id_specific_price_rule`),
  KEY `id_product` (`id_shop`,`id_currency`,`id_country`,`id_group`,`from_quantity`,`from`,`to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_specific_price_rule_condition`
--

DROP TABLE IF EXISTS `ps_specific_price_rule_condition`;
CREATE TABLE IF NOT EXISTS `ps_specific_price_rule_condition` (
  `id_specific_price_rule_condition` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule_condition_group` int(11) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id_specific_price_rule_condition`),
  KEY `id_specific_price_rule_condition_group` (`id_specific_price_rule_condition_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_specific_price_rule_condition_group`
--

DROP TABLE IF EXISTS `ps_specific_price_rule_condition_group`;
CREATE TABLE IF NOT EXISTS `ps_specific_price_rule_condition_group` (
  `id_specific_price_rule_condition_group` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_specific_price_rule_condition_group`,`id_specific_price_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_state`
--

DROP TABLE IF EXISTS `ps_state`;
CREATE TABLE IF NOT EXISTS `ps_state` (
  `id_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_country` int(11) UNSIGNED NOT NULL,
  `id_zone` int(11) UNSIGNED NOT NULL,
  `name` varchar(80) NOT NULL,
  `iso_code` varchar(7) NOT NULL,
  `tax_behavior` smallint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_state`),
  KEY `id_country` (`id_country`),
  KEY `name` (`name`),
  KEY `id_zone` (`id_zone`)
) ENGINE=InnoDB AUTO_INCREMENT=405 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_state`
--

INSERT INTO `ps_state` (`id_state`, `id_country`, `id_zone`, `name`, `iso_code`, `tax_behavior`, `active`) VALUES
(1, 21, 2, 'AA', 'AA', 0, 1),
(2, 21, 2, 'AE', 'AE', 0, 1),
(3, 21, 2, 'AP', 'AP', 0, 1),
(4, 21, 2, 'Alabama', 'AL', 0, 1),
(5, 21, 2, 'Alaska', 'AK', 0, 1),
(6, 21, 2, 'Arizona', 'AZ', 0, 1),
(7, 21, 2, 'Arkansas', 'AR', 0, 1),
(8, 21, 2, 'California', 'CA', 0, 1),
(9, 21, 2, 'Colorado', 'CO', 0, 1),
(10, 21, 2, 'Connecticut', 'CT', 0, 1),
(11, 21, 2, 'Delaware', 'DE', 0, 1),
(12, 21, 2, 'Florida', 'FL', 0, 1),
(13, 21, 2, 'Georgia', 'GA', 0, 1),
(14, 21, 2, 'Hawaii', 'HI', 0, 1),
(15, 21, 2, 'Idaho', 'ID', 0, 1),
(16, 21, 2, 'Illinois', 'IL', 0, 1),
(17, 21, 2, 'Indiana', 'IN', 0, 1),
(18, 21, 2, 'Iowa', 'IA', 0, 1),
(19, 21, 2, 'Kansas', 'KS', 0, 1),
(20, 21, 2, 'Kentucky', 'KY', 0, 1),
(21, 21, 2, 'Louisiana', 'LA', 0, 1),
(22, 21, 2, 'Maine', 'ME', 0, 1),
(23, 21, 2, 'Maryland', 'MD', 0, 1),
(24, 21, 2, 'Massachusetts', 'MA', 0, 1),
(25, 21, 2, 'Michigan', 'MI', 0, 1),
(26, 21, 2, 'Minnesota', 'MN', 0, 1),
(27, 21, 2, 'Mississippi', 'MS', 0, 1),
(28, 21, 2, 'Missouri', 'MO', 0, 1),
(29, 21, 2, 'Montana', 'MT', 0, 1),
(30, 21, 2, 'Nebraska', 'NE', 0, 1),
(31, 21, 2, 'Nevada', 'NV', 0, 1),
(32, 21, 2, 'New Hampshire', 'NH', 0, 1),
(33, 21, 2, 'New Jersey', 'NJ', 0, 1),
(34, 21, 2, 'New Mexico', 'NM', 0, 1),
(35, 21, 2, 'New York', 'NY', 0, 1),
(36, 21, 2, 'North Carolina', 'NC', 0, 1),
(37, 21, 2, 'North Dakota', 'ND', 0, 1),
(38, 21, 2, 'Ohio', 'OH', 0, 1),
(39, 21, 2, 'Oklahoma', 'OK', 0, 1),
(40, 21, 2, 'Oregon', 'OR', 0, 1),
(41, 21, 2, 'Pennsylvania', 'PA', 0, 1),
(42, 21, 2, 'Rhode Island', 'RI', 0, 1),
(43, 21, 2, 'South Carolina', 'SC', 0, 1),
(44, 21, 2, 'South Dakota', 'SD', 0, 1),
(45, 21, 2, 'Tennessee', 'TN', 0, 1),
(46, 21, 2, 'Texas', 'TX', 0, 1),
(47, 21, 2, 'Utah', 'UT', 0, 1),
(48, 21, 2, 'Vermont', 'VT', 0, 1),
(49, 21, 2, 'Virginia', 'VA', 0, 1),
(50, 21, 2, 'Washington', 'WA', 0, 1),
(51, 21, 2, 'West Virginia', 'WV', 0, 1),
(52, 21, 2, 'Wisconsin', 'WI', 0, 1),
(53, 21, 2, 'Wyoming', 'WY', 0, 1),
(54, 21, 2, 'Puerto Rico', 'PR', 0, 1),
(55, 21, 2, 'US Virgin Islands', 'VI', 0, 1),
(56, 21, 2, 'District of Columbia', 'DC', 0, 1),
(57, 144, 2, 'Aguascalientes', 'AGS', 0, 1),
(58, 144, 2, 'Baja California', 'BCN', 0, 1),
(59, 144, 2, 'Baja California Sur', 'BCS', 0, 1),
(60, 144, 2, 'Campeche', 'CAM', 0, 1),
(61, 144, 2, 'Chiapas', 'CHP', 0, 1),
(62, 144, 2, 'Chihuahua', 'CHH', 0, 1),
(63, 144, 2, 'Coahuila', 'COA', 0, 1),
(64, 144, 2, 'Colima', 'COL', 0, 1),
(65, 144, 2, 'Ciudad de México', 'CMX', 0, 1),
(66, 144, 2, 'Durango', 'DUR', 0, 1),
(67, 144, 2, 'Guanajuato', 'GUA', 0, 1),
(68, 144, 2, 'Guerrero', 'GRO', 0, 1),
(69, 144, 2, 'Hidalgo', 'HID', 0, 1),
(70, 144, 2, 'Jalisco', 'JAL', 0, 1),
(71, 144, 2, 'Estado de México', 'MEX', 0, 1),
(72, 144, 2, 'Michoacán', 'MIC', 0, 1),
(73, 144, 2, 'Morelos', 'MOR', 0, 1),
(74, 144, 2, 'Nayarit', 'NAY', 0, 1),
(75, 144, 2, 'Nuevo León', 'NLE', 0, 1),
(76, 144, 2, 'Oaxaca', 'OAX', 0, 1),
(77, 144, 2, 'Puebla', 'PUE', 0, 1),
(78, 144, 2, 'Querétaro', 'QUE', 0, 1),
(79, 144, 2, 'Quintana Roo', 'ROO', 0, 1),
(80, 144, 2, 'San Luis Potosí', 'SLP', 0, 1),
(81, 144, 2, 'Sinaloa', 'SIN', 0, 1),
(82, 144, 2, 'Sonora', 'SON', 0, 1),
(83, 144, 2, 'Tabasco', 'TAB', 0, 1),
(84, 144, 2, 'Tamaulipas', 'TAM', 0, 1),
(85, 144, 2, 'Tlaxcala', 'TLA', 0, 1),
(86, 144, 2, 'Veracruz', 'VER', 0, 1),
(87, 144, 2, 'Yucatán', 'YUC', 0, 1),
(88, 144, 2, 'Zacatecas', 'ZAC', 0, 1),
(89, 4, 2, 'Ontario', 'ON', 0, 1),
(90, 4, 2, 'Quebec', 'QC', 0, 1),
(91, 4, 2, 'British Columbia', 'BC', 0, 1),
(92, 4, 2, 'Alberta', 'AB', 0, 1),
(93, 4, 2, 'Manitoba', 'MB', 0, 1),
(94, 4, 2, 'Saskatchewan', 'SK', 0, 1),
(95, 4, 2, 'Nova Scotia', 'NS', 0, 1),
(96, 4, 2, 'New Brunswick', 'NB', 0, 1),
(97, 4, 2, 'Newfoundland and Labrador', 'NL', 0, 1),
(98, 4, 2, 'Prince Edward Island', 'PE', 0, 1),
(99, 4, 2, 'Northwest Territories', 'NT', 0, 1),
(100, 4, 2, 'Yukon', 'YT', 0, 1),
(101, 4, 2, 'Nunavut', 'NU', 0, 1),
(102, 44, 6, 'Buenos Aires', 'B', 0, 1),
(103, 44, 6, 'Catamarca', 'K', 0, 1),
(104, 44, 6, 'Chaco', 'H', 0, 1),
(105, 44, 6, 'Chubut', 'U', 0, 1),
(106, 44, 6, 'Ciudad de Buenos Aires', 'C', 0, 1),
(107, 44, 6, 'Córdoba', 'X', 0, 1),
(108, 44, 6, 'Corrientes', 'W', 0, 1),
(109, 44, 6, 'Entre Ríos', 'E', 0, 1),
(110, 44, 6, 'Formosa', 'P', 0, 1),
(111, 44, 6, 'Jujuy', 'Y', 0, 1),
(112, 44, 6, 'La Pampa', 'L', 0, 1),
(113, 44, 6, 'La Rioja', 'F', 0, 1),
(114, 44, 6, 'Mendoza', 'M', 0, 1),
(115, 44, 6, 'Misiones', 'N', 0, 1),
(116, 44, 6, 'Neuquén', 'Q', 0, 1),
(117, 44, 6, 'Río Negro', 'R', 0, 1),
(118, 44, 6, 'Salta', 'A', 0, 1),
(119, 44, 6, 'San Juan', 'J', 0, 1),
(120, 44, 6, 'San Luis', 'D', 0, 1),
(121, 44, 6, 'Santa Cruz', 'Z', 0, 1),
(122, 44, 6, 'Santa Fe', 'S', 0, 1),
(123, 44, 6, 'Santiago del Estero', 'G', 0, 1),
(124, 44, 6, 'Tierra del Fuego', 'V', 0, 1),
(125, 44, 6, 'Tucumán', 'T', 0, 1),
(126, 10, 1, 'Agrigento', 'AG', 0, 1),
(127, 10, 1, 'Alessandria', 'AL', 0, 1),
(128, 10, 1, 'Ancona', 'AN', 0, 1),
(129, 10, 1, 'Aosta', 'AO', 0, 1),
(130, 10, 1, 'Arezzo', 'AR', 0, 1),
(131, 10, 1, 'Ascoli Piceno', 'AP', 0, 1),
(132, 10, 1, 'Asti', 'AT', 0, 1),
(133, 10, 1, 'Avellino', 'AV', 0, 1),
(134, 10, 1, 'Bari', 'BA', 0, 1),
(135, 10, 1, 'Barletta-Andria-Trani', 'BT', 0, 1),
(136, 10, 1, 'Belluno', 'BL', 0, 1),
(137, 10, 1, 'Benevento', 'BN', 0, 1),
(138, 10, 1, 'Bergamo', 'BG', 0, 1),
(139, 10, 1, 'Biella', 'BI', 0, 1),
(140, 10, 1, 'Bologna', 'BO', 0, 1),
(141, 10, 1, 'Bolzano', 'BZ', 0, 1),
(142, 10, 1, 'Brescia', 'BS', 0, 1),
(143, 10, 1, 'Brindisi', 'BR', 0, 1),
(144, 10, 1, 'Cagliari', 'CA', 0, 1),
(145, 10, 1, 'Caltanissetta', 'CL', 0, 1),
(146, 10, 1, 'Campobasso', 'CB', 0, 1),
(147, 10, 1, 'Carbonia-Iglesias', 'CI', 0, 1),
(148, 10, 1, 'Caserta', 'CE', 0, 1),
(149, 10, 1, 'Catania', 'CT', 0, 1),
(150, 10, 1, 'Catanzaro', 'CZ', 0, 1),
(151, 10, 1, 'Chieti', 'CH', 0, 1),
(152, 10, 1, 'Como', 'CO', 0, 1),
(153, 10, 1, 'Cosenza', 'CS', 0, 1),
(154, 10, 1, 'Cremona', 'CR', 0, 1),
(155, 10, 1, 'Crotone', 'KR', 0, 1),
(156, 10, 1, 'Cuneo', 'CN', 0, 1),
(157, 10, 1, 'Enna', 'EN', 0, 1),
(158, 10, 1, 'Fermo', 'FM', 0, 1),
(159, 10, 1, 'Ferrara', 'FE', 0, 1),
(160, 10, 1, 'Firenze', 'FI', 0, 1),
(161, 10, 1, 'Foggia', 'FG', 0, 1),
(162, 10, 1, 'Forlì-Cesena', 'FC', 0, 1),
(163, 10, 1, 'Frosinone', 'FR', 0, 1),
(164, 10, 1, 'Genova', 'GE', 0, 1),
(165, 10, 1, 'Gorizia', 'GO', 0, 1),
(166, 10, 1, 'Grosseto', 'GR', 0, 1),
(167, 10, 1, 'Imperia', 'IM', 0, 1),
(168, 10, 1, 'Isernia', 'IS', 0, 1),
(169, 10, 1, 'L\'Aquila', 'AQ', 0, 1),
(170, 10, 1, 'La Spezia', 'SP', 0, 1),
(171, 10, 1, 'Latina', 'LT', 0, 1),
(172, 10, 1, 'Lecce', 'LE', 0, 1),
(173, 10, 1, 'Lecco', 'LC', 0, 1),
(174, 10, 1, 'Livorno', 'LI', 0, 1),
(175, 10, 1, 'Lodi', 'LO', 0, 1),
(176, 10, 1, 'Lucca', 'LU', 0, 1),
(177, 10, 1, 'Macerata', 'MC', 0, 1),
(178, 10, 1, 'Mantova', 'MN', 0, 1),
(179, 10, 1, 'Massa', 'MS', 0, 1),
(180, 10, 1, 'Matera', 'MT', 0, 1),
(181, 10, 1, 'Medio Campidano', 'VS', 0, 1),
(182, 10, 1, 'Messina', 'ME', 0, 1),
(183, 10, 1, 'Milano', 'MI', 0, 1),
(184, 10, 1, 'Modena', 'MO', 0, 1),
(185, 10, 1, 'Monza e della Brianza', 'MB', 0, 1),
(186, 10, 1, 'Napoli', 'NA', 0, 1),
(187, 10, 1, 'Novara', 'NO', 0, 1),
(188, 10, 1, 'Nuoro', 'NU', 0, 1),
(189, 10, 1, 'Ogliastra', 'OG', 0, 1),
(190, 10, 1, 'Olbia-Tempio', 'OT', 0, 1),
(191, 10, 1, 'Oristano', 'OR', 0, 1),
(192, 10, 1, 'Padova', 'PD', 0, 1),
(193, 10, 1, 'Palermo', 'PA', 0, 1),
(194, 10, 1, 'Parma', 'PR', 0, 1),
(195, 10, 1, 'Pavia', 'PV', 0, 1),
(196, 10, 1, 'Perugia', 'PG', 0, 1),
(197, 10, 1, 'Pesaro-Urbino', 'PU', 0, 1),
(198, 10, 1, 'Pescara', 'PE', 0, 1),
(199, 10, 1, 'Piacenza', 'PC', 0, 1),
(200, 10, 1, 'Pisa', 'PI', 0, 1),
(201, 10, 1, 'Pistoia', 'PT', 0, 1),
(202, 10, 1, 'Pordenone', 'PN', 0, 1),
(203, 10, 1, 'Potenza', 'PZ', 0, 1),
(204, 10, 1, 'Prato', 'PO', 0, 1),
(205, 10, 1, 'Ragusa', 'RG', 0, 1),
(206, 10, 1, 'Ravenna', 'RA', 0, 1),
(207, 10, 1, 'Reggio Calabria', 'RC', 0, 1),
(208, 10, 1, 'Reggio Emilia', 'RE', 0, 1),
(209, 10, 1, 'Rieti', 'RI', 0, 1),
(210, 10, 1, 'Rimini', 'RN', 0, 1),
(211, 10, 1, 'Roma', 'RM', 0, 1),
(212, 10, 1, 'Rovigo', 'RO', 0, 1),
(213, 10, 1, 'Salerno', 'SA', 0, 1),
(214, 10, 1, 'Sassari', 'SS', 0, 1),
(215, 10, 1, 'Savona', 'SV', 0, 1),
(216, 10, 1, 'Siena', 'SI', 0, 1),
(217, 10, 1, 'Siracusa', 'SR', 0, 1),
(218, 10, 1, 'Sondrio', 'SO', 0, 1),
(219, 10, 1, 'Taranto', 'TA', 0, 1),
(220, 10, 1, 'Teramo', 'TE', 0, 1),
(221, 10, 1, 'Terni', 'TR', 0, 1),
(222, 10, 1, 'Torino', 'TO', 0, 1),
(223, 10, 1, 'Trapani', 'TP', 0, 1),
(224, 10, 1, 'Trento', 'TN', 0, 1),
(225, 10, 1, 'Treviso', 'TV', 0, 1),
(226, 10, 1, 'Trieste', 'TS', 0, 1),
(227, 10, 1, 'Udine', 'UD', 0, 1),
(228, 10, 1, 'Varese', 'VA', 0, 1),
(229, 10, 1, 'Venezia', 'VE', 0, 1),
(230, 10, 1, 'Verbano-Cusio-Ossola', 'VB', 0, 1),
(231, 10, 1, 'Vercelli', 'VC', 0, 1),
(232, 10, 1, 'Verona', 'VR', 0, 1),
(233, 10, 1, 'Vibo Valentia', 'VV', 0, 1),
(234, 10, 1, 'Vicenza', 'VI', 0, 1),
(235, 10, 1, 'Viterbo', 'VT', 0, 1),
(236, 110, 3, 'Aceh', 'ID-AC', 0, 1),
(237, 110, 3, 'Bali', 'ID-BA', 0, 1),
(238, 110, 3, 'Banten', 'ID-BT', 0, 1),
(239, 110, 3, 'Bengkulu', 'ID-BE', 0, 1),
(240, 110, 3, 'Gorontalo', 'ID-GO', 0, 1),
(241, 110, 3, 'Jakarta', 'ID-JK', 0, 1),
(242, 110, 3, 'Jambi', 'ID-JA', 0, 1),
(243, 110, 3, 'Jawa Barat', 'ID-JB', 0, 1),
(244, 110, 3, 'Jawa Tengah', 'ID-JT', 0, 1),
(245, 110, 3, 'Jawa Timur', 'ID-JI', 0, 1),
(246, 110, 3, 'Kalimantan Barat', 'ID-KB', 0, 1),
(247, 110, 3, 'Kalimantan Selatan', 'ID-KS', 0, 1),
(248, 110, 3, 'Kalimantan Tengah', 'ID-KT', 0, 1),
(249, 110, 3, 'Kalimantan Timur', 'ID-KI', 0, 1),
(250, 110, 3, 'Kalimantan Utara', 'ID-KU', 0, 1),
(251, 110, 3, 'Kepulauan Bangka Belitug', 'ID-BB', 0, 1),
(252, 110, 3, 'Kepulauan Riau', 'ID-KR', 0, 1),
(253, 110, 3, 'Lampung', 'ID-LA', 0, 1),
(254, 110, 3, 'Maluku', 'ID-MA', 0, 1),
(255, 110, 3, 'Maluku Utara', 'ID-MU', 0, 1),
(256, 110, 3, 'Nusa Tengara Barat', 'ID-NB', 0, 1),
(257, 110, 3, 'Nusa Tenggara Timur', 'ID-NT', 0, 1),
(258, 110, 3, 'Papua', 'ID-PA', 0, 1),
(259, 110, 3, 'Papua Barat', 'ID-PB', 0, 1),
(260, 110, 3, 'Riau', 'ID-RI', 0, 1),
(261, 110, 3, 'Sulawesi Barat', 'ID-SR', 0, 1),
(262, 110, 3, 'Sulawesi Selatan', 'ID-SN', 0, 1),
(263, 110, 3, 'Sulawesi Tengah', 'ID-ST', 0, 1),
(264, 110, 3, 'Sulawesi Tenggara', 'ID-SG', 0, 1),
(265, 110, 3, 'Sulawesi Utara', 'ID-SA', 0, 1),
(266, 110, 3, 'Sumatera Barat', 'ID-SB', 0, 1),
(267, 110, 3, 'Sumatera Selatan', 'ID-SS', 0, 1),
(268, 110, 3, 'Sumatera Utara', 'ID-SU', 0, 1),
(269, 110, 3, 'Yogyakarta', 'ID-YO', 0, 1),
(270, 11, 3, 'Aichi', '23', 0, 1),
(271, 11, 3, 'Akita', '05', 0, 1),
(272, 11, 3, 'Aomori', '02', 0, 1),
(273, 11, 3, 'Chiba', '12', 0, 1),
(274, 11, 3, 'Ehime', '38', 0, 1),
(275, 11, 3, 'Fukui', '18', 0, 1),
(276, 11, 3, 'Fukuoka', '40', 0, 1),
(277, 11, 3, 'Fukushima', '07', 0, 1),
(278, 11, 3, 'Gifu', '21', 0, 1),
(279, 11, 3, 'Gunma', '10', 0, 1),
(280, 11, 3, 'Hiroshima', '34', 0, 1),
(281, 11, 3, 'Hokkaido', '01', 0, 1),
(282, 11, 3, 'Hyogo', '28', 0, 1),
(283, 11, 3, 'Ibaraki', '08', 0, 1),
(284, 11, 3, 'Ishikawa', '17', 0, 1),
(285, 11, 3, 'Iwate', '03', 0, 1),
(286, 11, 3, 'Kagawa', '37', 0, 1),
(287, 11, 3, 'Kagoshima', '46', 0, 1),
(288, 11, 3, 'Kanagawa', '14', 0, 1),
(289, 11, 3, 'Kochi', '39', 0, 1),
(290, 11, 3, 'Kumamoto', '43', 0, 1),
(291, 11, 3, 'Kyoto', '26', 0, 1),
(292, 11, 3, 'Mie', '24', 0, 1),
(293, 11, 3, 'Miyagi', '04', 0, 1),
(294, 11, 3, 'Miyazaki', '45', 0, 1),
(295, 11, 3, 'Nagano', '20', 0, 1),
(296, 11, 3, 'Nagasaki', '42', 0, 1),
(297, 11, 3, 'Nara', '29', 0, 1),
(298, 11, 3, 'Niigata', '15', 0, 1),
(299, 11, 3, 'Oita', '44', 0, 1),
(300, 11, 3, 'Okayama', '33', 0, 1),
(301, 11, 3, 'Okinawa', '47', 0, 1),
(302, 11, 3, 'Osaka', '27', 0, 1),
(303, 11, 3, 'Saga', '41', 0, 1),
(304, 11, 3, 'Saitama', '11', 0, 1),
(305, 11, 3, 'Shiga', '25', 0, 1),
(306, 11, 3, 'Shimane', '32', 0, 1),
(307, 11, 3, 'Shizuoka', '22', 0, 1),
(308, 11, 3, 'Tochigi', '09', 0, 1),
(309, 11, 3, 'Tokushima', '36', 0, 1),
(310, 11, 3, 'Tokyo', '13', 0, 1),
(311, 11, 3, 'Tottori', '31', 0, 1),
(312, 11, 3, 'Toyama', '16', 0, 1),
(313, 11, 3, 'Wakayama', '30', 0, 1),
(314, 11, 3, 'Yamagata', '06', 0, 1),
(315, 11, 3, 'Yamaguchi', '35', 0, 1),
(316, 11, 3, 'Yamanashi', '19', 0, 1),
(317, 24, 5, 'Australian Capital Territory', 'ACT', 0, 1),
(318, 24, 5, 'New South Wales', 'NSW', 0, 1),
(319, 24, 5, 'Northern Territory', 'NT', 0, 1),
(320, 24, 5, 'Queensland', 'QLD', 0, 1),
(321, 24, 5, 'South Australia', 'SA', 0, 1),
(322, 24, 5, 'Tasmania', 'TAS', 0, 1),
(323, 24, 5, 'Victoria', 'VIC', 0, 1),
(324, 24, 5, 'Western Australia', 'WA', 0, 1),
(325, 109, 3, 'Andhra Pradesh', 'AP', 0, 1),
(326, 109, 3, 'Arunachal Pradesh', 'AR', 0, 1),
(327, 109, 3, 'Assam', 'AS', 0, 1),
(328, 109, 3, 'Bihar', 'BR', 0, 1),
(329, 109, 3, 'Chhattisgarh', 'CT', 0, 1),
(330, 109, 3, 'Goa', 'GA', 0, 1),
(331, 109, 3, 'Gujarat', 'GJ', 0, 1),
(332, 109, 3, 'Haryana', 'HR', 0, 1),
(333, 109, 3, 'Himachal Pradesh', 'HP', 0, 1),
(334, 109, 3, 'Jharkhand', 'JH', 0, 1),
(335, 109, 3, 'Karnataka', 'KA', 0, 1),
(336, 109, 3, 'Kerala', 'KL', 0, 1),
(337, 109, 3, 'Madhya Pradesh', 'MP', 0, 1),
(338, 109, 3, 'Maharashtra', 'MH', 0, 1),
(339, 109, 3, 'Manipur', 'MN', 0, 1),
(340, 109, 3, 'Meghalaya', 'ML', 0, 1),
(341, 109, 3, 'Mizoram', 'MZ', 0, 1),
(342, 109, 3, 'Nagaland', 'NL', 0, 1),
(343, 109, 3, 'Odisha', 'OR', 0, 1),
(344, 109, 3, 'Punjab', 'PB', 0, 1),
(345, 109, 3, 'Rajasthan', 'RJ', 0, 1),
(346, 109, 3, 'Sikkim', 'SK', 0, 1),
(347, 109, 3, 'Tamil Nadu', 'TN', 0, 1),
(348, 109, 3, 'Telangana', 'TG', 0, 1),
(349, 109, 3, 'Tripura', 'TR', 0, 1),
(350, 109, 3, 'Uttar Pradesh', 'UP', 0, 1),
(351, 109, 3, 'Uttarakhand', 'UT', 0, 1),
(352, 109, 3, 'West Bengal', 'WB', 0, 1),
(353, 6, 1, 'A Coruña', 'ES-C', 0, 1),
(354, 6, 1, 'Álava', 'ES-VI', 0, 1),
(355, 6, 1, 'Albacete', 'ES-AB', 0, 1),
(356, 6, 1, 'Alacant', 'ES-A', 0, 1),
(357, 6, 1, 'Almería', 'ES-AL', 0, 1),
(358, 6, 1, 'Asturias', 'ES-O', 0, 1),
(359, 6, 1, 'Ávila', 'ES-AV', 0, 1),
(360, 6, 1, 'Badajoz', 'ES-BA', 0, 1),
(361, 6, 1, 'Balears', 'ES-PM', 0, 1),
(362, 6, 1, 'Barcelona', 'ES-B', 0, 1),
(363, 6, 1, 'Burgos', 'ES-BU', 0, 1),
(364, 6, 1, 'Cáceres', 'ES-CC', 0, 1),
(365, 6, 1, 'Cádiz', 'ES-CA', 0, 1),
(366, 6, 1, 'Cantabria', 'ES-S', 0, 1),
(367, 6, 1, 'Castelló', 'ES-CS', 0, 1),
(368, 6, 1, 'Ciudad Real', 'ES-CR', 0, 1),
(369, 6, 1, 'Córdoba', 'ES-CO', 0, 1),
(370, 6, 1, 'Cuenca', 'ES-CU', 0, 1),
(371, 6, 1, 'Girona', 'ES-GI', 0, 1),
(372, 6, 1, 'Granada', 'ES-GR', 0, 1),
(373, 6, 1, 'Guadalajara', 'ES-GU', 0, 1),
(374, 6, 1, 'Gipuzkoa', 'ES-SS', 0, 1),
(375, 6, 1, 'Huelva', 'ES-H', 0, 1),
(376, 6, 1, 'Huesca', 'ES-HU', 0, 1),
(377, 6, 1, 'Jaén', 'ES-J', 0, 1),
(378, 6, 1, 'La Rioja', 'ES-LO', 0, 1),
(379, 6, 1, 'Las Palmas', 'ES-GC', 0, 1),
(380, 6, 1, 'León', 'ES-LE', 0, 1),
(381, 6, 1, 'Lleida', 'ES-L', 0, 1),
(382, 6, 1, 'Lugo', 'ES-LU', 0, 1),
(383, 6, 1, 'Madrid', 'ES-M', 0, 1),
(384, 6, 1, 'Málaga', 'ES-MA', 0, 1),
(385, 6, 1, 'Murcia', 'ES-MU', 0, 1),
(386, 6, 1, 'Nafarroa', 'ES-NA', 0, 1),
(387, 6, 1, 'Ourense', 'ES-OR', 0, 1),
(388, 6, 1, 'Palencia', 'ES-P', 0, 1),
(389, 6, 1, 'Pontevedra', 'ES-PO', 0, 1),
(390, 6, 1, 'Salamanca', 'ES-SA', 0, 1),
(391, 6, 1, 'Santa Cruz de Tenerife', 'ES-TF', 0, 1),
(392, 6, 1, 'Segovia', 'ES-SG', 0, 1),
(393, 6, 1, 'Sevilla', 'ES-SE', 0, 1),
(394, 6, 1, 'Soria', 'ES-SO', 0, 1),
(395, 6, 1, 'Tarragona', 'ES-T', 0, 1),
(396, 6, 1, 'Teruel', 'ES-TE', 0, 1),
(397, 6, 1, 'Toledo', 'ES-TO', 0, 1),
(398, 6, 1, 'València', 'ES-V', 0, 1),
(399, 6, 1, 'Valladolid', 'ES-VA', 0, 1),
(400, 6, 1, 'Bizkaia', 'ES-BI', 0, 1),
(401, 6, 1, 'Zamora', 'ES-ZA', 0, 1),
(402, 6, 1, 'Zaragoza', 'ES-Z', 0, 1),
(403, 6, 1, 'Ceuta', 'ES-CE', 0, 1),
(404, 6, 1, 'Melilla', 'ES-ML', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_statssearch`
--

DROP TABLE IF EXISTS `ps_statssearch`;
CREATE TABLE IF NOT EXISTS `ps_statssearch` (
  `id_statssearch` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `keywords` varchar(255) NOT NULL,
  `results` int(6) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_statssearch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_stock`
--

DROP TABLE IF EXISTS `ps_stock`;
CREATE TABLE IF NOT EXISTS `ps_stock` (
  `id_stock` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `reference` varchar(64) NOT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `isbn` varchar(32) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `mpn` varchar(40) DEFAULT NULL,
  `physical_quantity` int(11) UNSIGNED NOT NULL,
  `usable_quantity` int(11) UNSIGNED NOT NULL,
  `price_te` decimal(20,6) DEFAULT '0.000000',
  PRIMARY KEY (`id_stock`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_product` (`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_stock_available`
--

DROP TABLE IF EXISTS `ps_stock_available`;
CREATE TABLE IF NOT EXISTS `ps_stock_available` (
  `id_stock_available` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `quantity` int(10) NOT NULL DEFAULT '0',
  `physical_quantity` int(11) NOT NULL DEFAULT '0',
  `reserved_quantity` int(11) NOT NULL DEFAULT '0',
  `depends_on_stock` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `out_of_stock` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `location` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_stock_available`),
  UNIQUE KEY `product_sqlstock` (`id_product`,`id_product_attribute`,`id_shop`,`id_shop_group`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_product` (`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_stock_available`
--

INSERT INTO `ps_stock_available` (`id_stock_available`, `id_product`, `id_product_attribute`, `id_shop`, `id_shop_group`, `quantity`, `physical_quantity`, `reserved_quantity`, `depends_on_stock`, `out_of_stock`, `location`) VALUES
(59, 20, 0, 1, 0, 600, 600, 0, 0, 2, ''),
(62, 23, 0, 1, 0, 500, 500, 0, 0, 2, ''),
(63, 18, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(64, 17, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(65, 16, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(66, 11, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(67, 10, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(68, 9, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(69, 5, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(70, 4, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(71, 3, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(72, 2, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(73, 1, 0, 1, 0, 0, 0, 0, 0, 0, ''),
(74, 24, 0, 1, 0, 400, 400, 0, 0, 2, ''),
(75, 25, 0, 1, 0, 0, 0, 0, 0, 2, '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_stock_mvt`
--

DROP TABLE IF EXISTS `ps_stock_mvt`;
CREATE TABLE IF NOT EXISTS `ps_stock_mvt` (
  `id_stock_mvt` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_stock` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_supply_order` int(11) DEFAULT NULL,
  `id_stock_mvt_reason` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `employee_lastname` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_firstname` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `physical_quantity` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `sign` smallint(6) NOT NULL DEFAULT '1',
  `price_te` decimal(20,6) DEFAULT '0.000000',
  `last_wa` decimal(20,6) DEFAULT '0.000000',
  `current_wa` decimal(20,6) DEFAULT '0.000000',
  `referer` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_stock_mvt`),
  KEY `id_stock` (`id_stock`),
  KEY `id_stock_mvt_reason` (`id_stock_mvt_reason`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_stock_mvt`
--

INSERT INTO `ps_stock_mvt` (`id_stock_mvt`, `id_stock`, `id_order`, `id_supply_order`, `id_stock_mvt_reason`, `id_employee`, `employee_lastname`, `employee_firstname`, `physical_quantity`, `date_add`, `sign`, `price_te`, `last_wa`, `current_wa`, `referer`) VALUES
(1, 59, NULL, NULL, 11, 1, 'Noel', 'Maxime', 600, '2022-12-05 09:45:50', 1, '0.000000', '0.000000', '0.000000', NULL),
(2, 62, NULL, NULL, 11, 1, 'Noel', 'Maxime', 500, '2022-12-05 11:06:19', 1, '0.000000', '0.000000', '0.000000', NULL),
(3, 74, NULL, NULL, 11, 1, 'Noel', 'Maxime', 400, '2022-12-05 12:32:46', 1, '0.000000', '0.000000', '0.000000', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ps_stock_mvt_reason`
--

DROP TABLE IF EXISTS `ps_stock_mvt_reason`;
CREATE TABLE IF NOT EXISTS `ps_stock_mvt_reason` (
  `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sign` tinyint(1) NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_stock_mvt_reason`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_stock_mvt_reason`
--

INSERT INTO `ps_stock_mvt_reason` (`id_stock_mvt_reason`, `sign`, `date_add`, `date_upd`, `deleted`) VALUES
(1, 1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(2, -1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(3, -1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(4, -1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(5, 1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(6, -1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(7, 1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(8, 1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(9, 1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(10, 1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(11, 1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0),
(12, -1, '2022-11-21 11:03:36', '2022-11-21 11:03:36', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_stock_mvt_reason_lang`
--

DROP TABLE IF EXISTS `ps_stock_mvt_reason_lang`;
CREATE TABLE IF NOT EXISTS `ps_stock_mvt_reason_lang` (
  `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_stock_mvt_reason`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_stock_mvt_reason_lang`
--

INSERT INTO `ps_stock_mvt_reason_lang` (`id_stock_mvt_reason`, `id_lang`, `name`) VALUES
(1, 1, 'Increase'),
(1, 2, 'Augmentation'),
(1, 3, 'Erhöhung'),
(1, 4, 'Verhoog'),
(1, 5, 'Aumento'),
(1, 6, 'Incrementar'),
(2, 1, 'Decrease'),
(2, 2, 'Diminution'),
(2, 3, 'Reduzierung'),
(2, 4, 'Verlaag'),
(2, 5, 'Diminuisci'),
(2, 6, 'Decrementar'),
(3, 1, 'Customer Order'),
(3, 2, 'Commande client'),
(3, 3, 'Bestellung'),
(3, 4, 'Klantbestelling'),
(3, 5, 'Ordine Cliente'),
(3, 6, 'Pedido del cliente'),
(4, 1, 'Regulation following an inventory of stock'),
(4, 2, 'Regulation following an inventory of stock'),
(4, 3, 'Regulation following an inventory of stock'),
(4, 4, 'Regulation following an inventory of stock'),
(4, 5, 'Regulation following an inventory of stock'),
(4, 6, 'Regulation following an inventory of stock'),
(5, 1, 'Regulation following an inventory of stock'),
(5, 2, 'Regulation following an inventory of stock'),
(5, 3, 'Regulation following an inventory of stock'),
(5, 4, 'Regulation following an inventory of stock'),
(5, 5, 'Regulation following an inventory of stock'),
(5, 6, 'Regulation following an inventory of stock'),
(6, 1, 'Transfer to another warehouse'),
(6, 2, 'Transfert vers un autre entrepôt'),
(6, 3, 'Übertragung in anderes Lager'),
(6, 4, 'Naar een ander magazijn verplaatsen'),
(6, 5, 'Trasferimento a un altro magazzino'),
(6, 6, 'Transferir a otro almacén'),
(7, 1, 'Transfer from another warehouse'),
(7, 2, 'Transfert depuis un autre entrepôt'),
(7, 3, 'Übertragung von anderem Lager'),
(7, 4, 'Van een ander magazijn verplaatsen'),
(7, 5, 'Trasferimento da un altro magazzino'),
(7, 6, 'Transferir desde otro almacén'),
(8, 1, 'Supply Order'),
(8, 2, 'Commande fournisseur'),
(8, 3, 'Lieferbestellung'),
(8, 4, 'Bestelling'),
(8, 5, 'Ordine di Fornitura'),
(8, 6, 'Pedido de suministros'),
(9, 1, 'Customer Order'),
(9, 2, 'Commande client'),
(9, 3, 'Bestellung'),
(9, 4, 'Klantbestelling'),
(9, 5, 'Ordine Cliente'),
(9, 6, 'Pedido del cliente'),
(10, 1, 'Product return'),
(10, 2, 'Retour produit'),
(10, 3, 'Warenrücksendung'),
(10, 4, 'Productretour'),
(10, 5, 'Reso di un Prodotto'),
(10, 6, 'Devolver producto'),
(11, 1, 'Employee Edition'),
(11, 2, 'Employee Edition'),
(11, 3, 'Employee Edition'),
(11, 4, 'Employee Edition'),
(11, 5, 'Employee Edition'),
(11, 6, 'Employee Edition'),
(12, 1, 'Employee Edition'),
(12, 2, 'Employee Edition'),
(12, 3, 'Employee Edition'),
(12, 4, 'Employee Edition'),
(12, 5, 'Employee Edition'),
(12, 6, 'Employee Edition');

-- --------------------------------------------------------

--
-- Structure de la table `ps_store`
--

DROP TABLE IF EXISTS `ps_store`;
CREATE TABLE IF NOT EXISTS `ps_store` (
  `id_store` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_state` int(10) UNSIGNED DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `postcode` varchar(12) NOT NULL,
  `latitude` decimal(13,8) DEFAULT NULL,
  `longitude` decimal(13,8) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `fax` varchar(16) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_store`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_store`
--

INSERT INTO `ps_store` (`id_store`, `id_country`, `id_state`, `city`, `postcode`, `latitude`, `longitude`, `phone`, `fax`, `email`, `active`, `date_add`, `date_upd`) VALUES
(1, 21, 12, 'Miami', '33135', '25.76500500', '-80.24379700', '', '', '', 1, '2022-11-21 11:07:27', '2022-11-21 11:07:27'),
(2, 21, 12, 'Miami', '33304', '26.13793600', '-80.13943500', '', '', '', 1, '2022-11-21 11:07:27', '2022-11-21 11:07:27'),
(3, 21, 12, 'Miami', '33026', '26.00998700', '-80.29447200', '', '', '', 1, '2022-11-21 11:07:27', '2022-11-21 11:07:27'),
(4, 21, 12, 'Miami', '33133', '25.73629600', '-80.24479700', '', '', '', 1, '2022-11-21 11:07:27', '2022-11-21 11:07:27'),
(5, 21, 12, 'Miami', '33181', '25.88674000', '-80.16329200', '', '', '', 1, '2022-11-21 11:07:27', '2022-11-21 11:07:27');

-- --------------------------------------------------------

--
-- Structure de la table `ps_store_lang`
--

DROP TABLE IF EXISTS `ps_store_lang`;
CREATE TABLE IF NOT EXISTS `ps_store_lang` (
  `id_store` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `hours` text,
  `note` text,
  PRIMARY KEY (`id_store`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_store_lang`
--

INSERT INTO `ps_store_lang` (`id_store`, `id_lang`, `name`, `address1`, `address2`, `hours`, `note`) VALUES
(1, 1, 'Dade County', '3030 SW 8th St Miami', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(1, 2, 'Dade County', '3030 SW 8th St Miami', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(1, 3, 'Dade County', '3030 SW 8th St Miami', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(1, 4, 'Dade County', '3030 SW 8th St Miami', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(1, 5, 'Dade County', '3030 SW 8th St Miami', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(1, 6, 'Dade County', '3030 SW 8th St Miami', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(2, 1, 'E Fort Lauderdale', '1000 Northeast 4th Ave Fort Lauderdale', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(2, 2, 'E Fort Lauderdale', '1000 Northeast 4th Ave Fort Lauderdale', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(2, 3, 'E Fort Lauderdale', '1000 Northeast 4th Ave Fort Lauderdale', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(2, 4, 'E Fort Lauderdale', '1000 Northeast 4th Ave Fort Lauderdale', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(2, 5, 'E Fort Lauderdale', '1000 Northeast 4th Ave Fort Lauderdale', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(2, 6, 'E Fort Lauderdale', '1000 Northeast 4th Ave Fort Lauderdale', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(3, 1, 'Pembroke Pines', '11001 Pines Blvd Pembroke Pines', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(3, 2, 'Pembroke Pines', '11001 Pines Blvd Pembroke Pines', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(3, 3, 'Pembroke Pines', '11001 Pines Blvd Pembroke Pines', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(3, 4, 'Pembroke Pines', '11001 Pines Blvd Pembroke Pines', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(3, 5, 'Pembroke Pines', '11001 Pines Blvd Pembroke Pines', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(3, 6, 'Pembroke Pines', '11001 Pines Blvd Pembroke Pines', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(4, 1, 'Coconut Grove', '2999 SW 32nd Avenue', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(4, 2, 'Coconut Grove', '2999 SW 32nd Avenue', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(4, 3, 'Coconut Grove', '2999 SW 32nd Avenue', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(4, 4, 'Coconut Grove', '2999 SW 32nd Avenue', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(4, 5, 'Coconut Grove', '2999 SW 32nd Avenue', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(4, 6, 'Coconut Grove', '2999 SW 32nd Avenue', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(5, 1, 'N Miami/Biscayne', '12055 Biscayne Blvd', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(5, 2, 'N Miami/Biscayne', '12055 Biscayne Blvd', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(5, 3, 'N Miami/Biscayne', '12055 Biscayne Blvd', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(5, 4, 'N Miami/Biscayne', '12055 Biscayne Blvd', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(5, 5, 'N Miami/Biscayne', '12055 Biscayne Blvd', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', ''),
(5, 6, 'N Miami/Biscayne', '12055 Biscayne Blvd', '', ' [[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"09:00AM - 07:00PM\"],[\"10:00AM - 04:00PM\"],[\"10:00AM - 04:00PM\"]]', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_store_shop`
--

DROP TABLE IF EXISTS `ps_store_shop`;
CREATE TABLE IF NOT EXISTS `ps_store_shop` (
  `id_store` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_store`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_store_shop`
--

INSERT INTO `ps_store_shop` (`id_store`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_supplier`
--

DROP TABLE IF EXISTS `ps_supplier`;
CREATE TABLE IF NOT EXISTS `ps_supplier` (
  `id_supplier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_supplier`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_supplier`
--

INSERT INTO `ps_supplier` (`id_supplier`, `name`, `date_add`, `date_upd`, `active`) VALUES
(1, 'Fashion supplier', '2022-11-21 11:07:24', '2022-12-11 16:02:58', 0),
(2, 'Accessories supplier', '2022-11-21 11:07:24', '2022-12-11 16:02:54', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_supplier_lang`
--

DROP TABLE IF EXISTS `ps_supplier_lang`;
CREATE TABLE IF NOT EXISTS `ps_supplier_lang` (
  `id_supplier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id_supplier`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_supplier_lang`
--

INSERT INTO `ps_supplier_lang` (`id_supplier`, `id_lang`, `description`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, '', '', '', ''),
(1, 2, '', '', '', ''),
(1, 3, '', '', '', ''),
(1, 4, '', '', '', ''),
(1, 5, '', '', '', ''),
(1, 6, '', '', '', ''),
(2, 1, '', '', '', ''),
(2, 2, '', '', '', ''),
(2, 3, '', '', '', ''),
(2, 4, '', '', '', ''),
(2, 5, '', '', '', ''),
(2, 6, '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_supplier_shop`
--

DROP TABLE IF EXISTS `ps_supplier_shop`;
CREATE TABLE IF NOT EXISTS `ps_supplier_shop` (
  `id_supplier` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_supplier`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_supplier_shop`
--

INSERT INTO `ps_supplier_shop` (`id_supplier`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_supply_order`
--

DROP TABLE IF EXISTS `ps_supply_order`;
CREATE TABLE IF NOT EXISTS `ps_supply_order` (
  `id_supply_order` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_supplier` int(11) UNSIGNED NOT NULL,
  `supplier_name` varchar(64) NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_ref_currency` int(11) UNSIGNED NOT NULL,
  `reference` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `date_delivery_expected` datetime DEFAULT NULL,
  `total_te` decimal(20,6) DEFAULT '0.000000',
  `total_with_discount_te` decimal(20,6) DEFAULT '0.000000',
  `total_tax` decimal(20,6) DEFAULT '0.000000',
  `total_ti` decimal(20,6) DEFAULT '0.000000',
  `discount_rate` decimal(20,6) DEFAULT '0.000000',
  `discount_value_te` decimal(20,6) DEFAULT '0.000000',
  `is_template` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_supply_order`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `reference` (`reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_supply_order_detail`
--

DROP TABLE IF EXISTS `ps_supply_order_detail`;
CREATE TABLE IF NOT EXISTS `ps_supply_order_detail` (
  `id_supply_order_detail` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_supply_order` int(11) UNSIGNED NOT NULL,
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `reference` varchar(64) NOT NULL,
  `supplier_reference` varchar(64) NOT NULL,
  `name` varchar(128) NOT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `isbn` varchar(32) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `mpn` varchar(40) DEFAULT NULL,
  `exchange_rate` decimal(20,6) DEFAULT '0.000000',
  `unit_price_te` decimal(20,6) DEFAULT '0.000000',
  `quantity_expected` int(11) UNSIGNED NOT NULL,
  `quantity_received` int(11) UNSIGNED NOT NULL,
  `price_te` decimal(20,6) DEFAULT '0.000000',
  `discount_rate` decimal(20,6) DEFAULT '0.000000',
  `discount_value_te` decimal(20,6) DEFAULT '0.000000',
  `price_with_discount_te` decimal(20,6) DEFAULT '0.000000',
  `tax_rate` decimal(20,6) DEFAULT '0.000000',
  `tax_value` decimal(20,6) DEFAULT '0.000000',
  `price_ti` decimal(20,6) DEFAULT '0.000000',
  `tax_value_with_order_discount` decimal(20,6) DEFAULT '0.000000',
  `price_with_order_discount_te` decimal(20,6) DEFAULT '0.000000',
  PRIMARY KEY (`id_supply_order_detail`),
  KEY `id_supply_order` (`id_supply_order`,`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_product_product_attribute` (`id_product`,`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_supply_order_history`
--

DROP TABLE IF EXISTS `ps_supply_order_history`;
CREATE TABLE IF NOT EXISTS `ps_supply_order_history` (
  `id_supply_order_history` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_supply_order` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `employee_lastname` varchar(255) DEFAULT '',
  `employee_firstname` varchar(255) DEFAULT '',
  `id_state` int(11) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_supply_order_history`),
  KEY `id_supply_order` (`id_supply_order`),
  KEY `id_employee` (`id_employee`),
  KEY `id_state` (`id_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_supply_order_receipt_history`
--

DROP TABLE IF EXISTS `ps_supply_order_receipt_history`;
CREATE TABLE IF NOT EXISTS `ps_supply_order_receipt_history` (
  `id_supply_order_receipt_history` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_supply_order_detail` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `employee_lastname` varchar(255) DEFAULT '',
  `employee_firstname` varchar(255) DEFAULT '',
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `quantity` int(11) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_supply_order_receipt_history`),
  KEY `id_supply_order_detail` (`id_supply_order_detail`),
  KEY `id_supply_order_state` (`id_supply_order_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_supply_order_state`
--

DROP TABLE IF EXISTS `ps_supply_order_state`;
CREATE TABLE IF NOT EXISTS `ps_supply_order_state` (
  `id_supply_order_state` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `delivery_note` tinyint(1) NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '0',
  `receipt_state` tinyint(1) NOT NULL DEFAULT '0',
  `pending_receipt` tinyint(1) NOT NULL DEFAULT '0',
  `enclosed` tinyint(1) NOT NULL DEFAULT '0',
  `color` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_supply_order_state`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_supply_order_state`
--

INSERT INTO `ps_supply_order_state` (`id_supply_order_state`, `delivery_note`, `editable`, `receipt_state`, `pending_receipt`, `enclosed`, `color`) VALUES
(1, 0, 1, 0, 0, 0, '#faab00'),
(2, 1, 0, 0, 0, 0, '#273cff'),
(3, 0, 0, 0, 1, 0, '#ff37f5'),
(4, 0, 0, 1, 1, 0, '#ff3e33'),
(5, 0, 0, 1, 0, 1, '#00d60c'),
(6, 0, 0, 0, 0, 1, '#666666');

-- --------------------------------------------------------

--
-- Structure de la table `ps_supply_order_state_lang`
--

DROP TABLE IF EXISTS `ps_supply_order_state_lang`;
CREATE TABLE IF NOT EXISTS `ps_supply_order_state_lang` (
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_supply_order_state`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_supply_order_state_lang`
--

INSERT INTO `ps_supply_order_state_lang` (`id_supply_order_state`, `id_lang`, `name`) VALUES
(1, 1, '1 - Creation in progress'),
(1, 2, '1 - En cours de création'),
(1, 3, '1 - In Bearbeitung'),
(1, 4, '1 - Bezig met aanmaken'),
(1, 5, '1 - Creazione in corso'),
(1, 6, '1 - Creación en proceso'),
(2, 1, '2 - Order validated'),
(2, 2, '2 - Commande validée'),
(2, 3, '2 - Bestellung geprüft'),
(2, 4, '2 - Bestelling bevestigd'),
(2, 5, '2 - Ordine convalidato'),
(2, 6, '2 - Pedido validado'),
(3, 1, '3 - Pending receipt'),
(3, 2, '3 - En attente de réception'),
(3, 3, '3 - Warten auf Rechnung'),
(3, 4, '3 - In afwachting van ontvangst'),
(3, 5, '3 - In attesa di ricevimento'),
(3, 6, '3 - Pendiente de recepción'),
(4, 1, '4 - Order received in part'),
(4, 2, '4 - Commande reçue partiellement'),
(4, 3, '4 - Teillieferung erhalten'),
(4, 4, '4 - Bestelling gedeeltelijk ontvangen'),
(4, 5, '4 - Ordine ricevuto in parte'),
(4, 6, '4 - Pedido recibido parcialmente'),
(5, 1, '5 - Order received completely'),
(5, 2, '5 - Commande reçue intégralement'),
(5, 3, '5 - Lieferung erhalten'),
(5, 4, '5 - Bestelling geheel ontvangen'),
(5, 5, '5 - Ordine ricevuto completamente'),
(5, 6, '5 - Pedido recibido al completo'),
(6, 1, '6 - Order canceled'),
(6, 2, '6 - Commande annulée'),
(6, 3, '6 - Bestellung storniert'),
(6, 4, '6 - Bestelling geannuleerd'),
(6, 5, '6 - Ordine annullato'),
(6, 6, '6 - Pedido cancelado');

-- --------------------------------------------------------

--
-- Structure de la table `ps_tab`
--

DROP TABLE IF EXISTS `ps_tab`;
CREATE TABLE IF NOT EXISTS `ps_tab` (
  `id_tab` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `module` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `hide_host_mode` tinyint(1) NOT NULL,
  `icon` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wording` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wording_domain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_tab`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_tab`
--

INSERT INTO `ps_tab` (`id_tab`, `id_parent`, `position`, `module`, `class_name`, `route_name`, `active`, `enabled`, `hide_host_mode`, `icon`, `wording`, `wording_domain`) VALUES
(1, 0, 0, NULL, 'AdminDashboard', NULL, 1, 1, 0, 'trending_up', 'Dashboard', 'Admin.Navigation.Menu'),
(2, 0, 1, NULL, 'SELL', NULL, 1, 1, 0, '', 'Sell', 'Admin.Navigation.Menu'),
(3, 2, 0, NULL, 'AdminParentOrders', NULL, 1, 1, 0, 'shopping_basket', 'Orders', 'Admin.Navigation.Menu'),
(4, 3, 0, NULL, 'AdminOrders', NULL, 1, 1, 0, '', 'Orders', 'Admin.Navigation.Menu'),
(5, 3, 1, NULL, 'AdminInvoices', NULL, 1, 1, 0, '', 'Invoices', 'Admin.Navigation.Menu'),
(6, 3, 2, NULL, 'AdminSlip', NULL, 1, 1, 0, '', 'Credit Slips', 'Admin.Navigation.Menu'),
(7, 3, 3, NULL, 'AdminDeliverySlip', NULL, 1, 1, 0, '', 'Delivery Slips', 'Admin.Navigation.Menu'),
(8, 3, 4, NULL, 'AdminCarts', NULL, 1, 1, 0, '', 'Shopping Carts', 'Admin.Navigation.Menu'),
(9, 2, 1, NULL, 'AdminCatalog', NULL, 1, 1, 0, 'store', 'Catalog', 'Admin.Navigation.Menu'),
(10, 9, 0, NULL, 'AdminProducts', NULL, 1, 1, 0, '', 'Products', 'Admin.Navigation.Menu'),
(11, 9, 1, NULL, 'AdminCategories', NULL, 1, 1, 0, '', 'Categories', 'Admin.Navigation.Menu'),
(12, 9, 2, NULL, 'AdminTracking', NULL, 1, 1, 0, '', 'Monitoring', 'Admin.Navigation.Menu'),
(13, 9, 3, NULL, 'AdminParentAttributesGroups', NULL, 1, 1, 0, '', 'Attributes & Features', 'Admin.Navigation.Menu'),
(14, 13, 0, NULL, 'AdminAttributesGroups', NULL, 1, 1, 0, '', 'Attributes', 'Admin.Navigation.Menu'),
(15, 13, 1, NULL, 'AdminFeatures', NULL, 1, 1, 0, '', 'Features', 'Admin.Navigation.Menu'),
(16, 9, 4, NULL, 'AdminParentManufacturers', NULL, 1, 1, 0, '', 'Brands & Suppliers', 'Admin.Navigation.Menu'),
(17, 16, 0, NULL, 'AdminManufacturers', NULL, 1, 1, 0, '', 'Brands', 'Admin.Navigation.Menu'),
(18, 16, 1, NULL, 'AdminSuppliers', NULL, 1, 1, 0, '', 'Suppliers', 'Admin.Navigation.Menu'),
(19, 9, 5, NULL, 'AdminAttachments', NULL, 1, 1, 0, '', 'Files', 'Admin.Navigation.Menu'),
(20, 9, 6, NULL, 'AdminParentCartRules', NULL, 1, 1, 0, '', 'Discounts', 'Admin.Navigation.Menu'),
(21, 20, 0, NULL, 'AdminCartRules', NULL, 1, 1, 0, '', 'Cart Rules', 'Admin.Navigation.Menu'),
(22, 20, 1, NULL, 'AdminSpecificPriceRule', NULL, 1, 1, 0, '', 'Catalog Price Rules', 'Admin.Navigation.Menu'),
(23, 9, 7, NULL, 'AdminStockManagement', NULL, 1, 1, 0, '', 'Stock', 'Admin.Navigation.Menu'),
(24, 2, 2, NULL, 'AdminParentCustomer', NULL, 1, 1, 0, 'account_circle', 'Customers', 'Admin.Navigation.Menu'),
(25, 24, 0, NULL, 'AdminCustomers', NULL, 1, 1, 0, '', 'Customers', 'Admin.Navigation.Menu'),
(26, 24, 1, NULL, 'AdminAddresses', NULL, 1, 1, 0, '', 'Addresses', 'Admin.Navigation.Menu'),
(27, 24, 2, NULL, 'AdminOutstanding', NULL, 0, 1, 0, '', 'Outstanding', 'Admin.Navigation.Menu'),
(28, 2, 3, NULL, 'AdminParentCustomerThreads', NULL, 1, 1, 0, 'chat', 'Customer Service', 'Admin.Navigation.Menu'),
(29, 28, 0, NULL, 'AdminCustomerThreads', NULL, 1, 1, 0, '', 'Customer Service', 'Admin.Navigation.Menu'),
(30, 28, 1, NULL, 'AdminOrderMessage', NULL, 1, 1, 0, '', 'Order Messages', 'Admin.Navigation.Menu'),
(31, 28, 2, NULL, 'AdminReturn', NULL, 1, 1, 0, '', 'Merchandise Returns', 'Admin.Navigation.Menu'),
(32, 2, 4, '', 'AdminStats', '', 1, 1, 0, 'assessment', 'Stats', 'Admin.Navigation.Menu'),
(33, 2, 5, NULL, 'AdminStock', NULL, 1, 1, 0, 'store', '', ''),
(34, 33, 0, NULL, 'AdminWarehouses', NULL, 1, 1, 0, '', 'Warehouses', 'Admin.Navigation.Menu'),
(35, 33, 1, NULL, 'AdminParentStockManagement', NULL, 1, 1, 0, '', 'Stock Management', 'Admin.Navigation.Menu'),
(36, 35, 0, NULL, 'AdminStockManagement', NULL, 1, 1, 0, '', 'Stock Management', 'Admin.Navigation.Menu'),
(37, 36, 0, NULL, 'AdminStockMvt', NULL, 1, 1, 0, '', 'Stock Movement', 'Admin.Navigation.Menu'),
(38, 36, 1, NULL, 'AdminStockInstantState', NULL, 1, 1, 0, '', 'Instant Stock Status', 'Admin.Navigation.Menu'),
(39, 36, 2, NULL, 'AdminStockCover', NULL, 1, 1, 0, '', 'Stock Coverage', 'Admin.Navigation.Menu'),
(40, 33, 2, NULL, 'AdminSupplyOrders', NULL, 1, 1, 0, '', 'Supply orders', 'Admin.Navigation.Menu'),
(41, 33, 3, NULL, 'AdminStockConfiguration', NULL, 1, 1, 0, '', 'Configuration', 'Admin.Navigation.Menu'),
(42, 0, 2, NULL, 'IMPROVE', NULL, 1, 1, 0, '', 'Improve', 'Admin.Navigation.Menu'),
(43, 42, 0, NULL, 'AdminParentModulesSf', NULL, 1, 1, 0, 'extension', 'Modules', 'Admin.Navigation.Menu'),
(44, 43, 0, NULL, 'AdminModulesSf', NULL, 1, 1, 0, '', 'Module Manager', 'Admin.Navigation.Menu'),
(45, 44, 0, NULL, 'AdminModulesManage', NULL, 1, 1, 0, '', 'Modules', 'Admin.Navigation.Menu'),
(46, 44, 1, NULL, 'AdminModulesNotifications', NULL, 1, 1, 0, '', 'Alerts', 'Admin.Navigation.Menu'),
(47, 44, 2, NULL, 'AdminModulesUpdates', NULL, 1, 1, 0, '', 'Updates', 'Admin.Navigation.Menu'),
(48, 43, 1, NULL, 'AdminParentModulesCatalog', NULL, 1, 1, 0, '', 'Module Catalog', 'Admin.Navigation.Menu'),
(49, 48, 0, '', 'AdminModulesCatalog', '', 0, 1, 0, '', 'Module Catalog', 'Admin.Navigation.Menu'),
(50, 48, 1, '', 'AdminAddonsCatalog', '', 0, 1, 0, '', 'Module Selections', 'Admin.Navigation.Menu'),
(51, 43, 2, NULL, 'AdminModules', NULL, 0, 1, 0, '', '', ''),
(52, 42, 1, NULL, 'AdminParentThemes', NULL, 1, 1, 0, 'desktop_mac', 'Design', 'Admin.Navigation.Menu'),
(53, 130, 1, '', 'AdminThemes', '', 1, 1, 0, '', 'Theme & Logo', 'Admin.Navigation.Menu'),
(54, 52, 1, '', 'AdminThemesCatalog', '', 0, 1, 0, '', 'Theme Catalog', 'Admin.Navigation.Menu'),
(55, 52, 2, NULL, 'AdminParentMailTheme', NULL, 1, 1, 0, '', 'Email Theme', 'Admin.Navigation.Menu'),
(56, 55, 0, NULL, 'AdminMailTheme', NULL, 1, 1, 0, '', 'Email Theme', 'Admin.Navigation.Menu'),
(57, 52, 3, NULL, 'AdminCmsContent', NULL, 1, 1, 0, '', 'Pages', 'Admin.Navigation.Menu'),
(58, 52, 4, NULL, 'AdminModulesPositions', NULL, 1, 1, 0, '', 'Positions', 'Admin.Navigation.Menu'),
(59, 52, 5, NULL, 'AdminImages', NULL, 1, 1, 0, '', 'Image Settings', 'Admin.Navigation.Menu'),
(60, 42, 2, NULL, 'AdminParentShipping', NULL, 1, 1, 0, 'local_shipping', 'Shipping', 'Admin.Navigation.Menu'),
(61, 60, 0, NULL, 'AdminCarriers', NULL, 1, 1, 0, '', 'Carriers', 'Admin.Navigation.Menu'),
(62, 60, 1, NULL, 'AdminShipping', NULL, 1, 1, 0, '', 'Preferences', 'Admin.Navigation.Menu'),
(63, 42, 3, NULL, 'AdminParentPayment', NULL, 1, 1, 0, 'payment', 'Payment', 'Admin.Navigation.Menu'),
(64, 63, 0, NULL, 'AdminPayment', NULL, 1, 1, 0, '', 'Payment Methods', 'Admin.Navigation.Menu'),
(65, 63, 1, NULL, 'AdminPaymentPreferences', NULL, 1, 1, 0, '', 'Preferences', 'Admin.Navigation.Menu'),
(66, 42, 4, NULL, 'AdminInternational', NULL, 1, 1, 0, 'language', 'International', 'Admin.Navigation.Menu'),
(67, 66, 0, NULL, 'AdminParentLocalization', NULL, 1, 1, 0, '', 'Localization', 'Admin.Navigation.Menu'),
(68, 67, 0, NULL, 'AdminLocalization', NULL, 1, 1, 0, '', 'Localization', 'Admin.Navigation.Menu'),
(69, 67, 1, NULL, 'AdminLanguages', NULL, 1, 1, 0, '', 'Languages', 'Admin.Navigation.Menu'),
(70, 67, 2, NULL, 'AdminCurrencies', NULL, 1, 1, 0, '', 'Currencies', 'Admin.Navigation.Menu'),
(71, 67, 3, NULL, 'AdminGeolocation', NULL, 1, 1, 0, '', 'Geolocation', 'Admin.Navigation.Menu'),
(72, 66, 1, NULL, 'AdminParentCountries', NULL, 1, 1, 0, '', 'Locations', 'Admin.Navigation.Menu'),
(73, 72, 0, NULL, 'AdminZones', NULL, 1, 1, 0, '', 'Zones', 'Admin.Navigation.Menu'),
(74, 72, 1, NULL, 'AdminCountries', NULL, 1, 1, 0, '', 'Countries', 'Admin.Navigation.Menu'),
(75, 72, 2, NULL, 'AdminStates', NULL, 1, 1, 0, '', 'States', 'Admin.Navigation.Menu'),
(76, 66, 2, NULL, 'AdminParentTaxes', NULL, 1, 1, 0, '', 'Taxes', 'Admin.Navigation.Menu'),
(77, 76, 0, NULL, 'AdminTaxes', NULL, 1, 1, 0, '', 'Taxes', 'Admin.Navigation.Menu'),
(78, 76, 1, NULL, 'AdminTaxRulesGroup', NULL, 1, 1, 0, '', 'Tax Rules', 'Admin.Navigation.Menu'),
(79, 66, 3, NULL, 'AdminTranslations', NULL, 1, 1, 0, '', 'Translations', 'Admin.Navigation.Menu'),
(80, 0, 3, NULL, 'CONFIGURE', NULL, 1, 1, 0, '', 'Configure', 'Admin.Navigation.Menu'),
(81, 80, 0, NULL, 'ShopParameters', NULL, 1, 1, 0, 'settings', 'Shop Parameters', 'Admin.Navigation.Menu'),
(82, 81, 0, NULL, 'AdminParentPreferences', NULL, 1, 1, 0, '', 'General', 'Admin.Navigation.Menu'),
(83, 82, 0, NULL, 'AdminPreferences', NULL, 1, 1, 0, '', 'General', 'Admin.Navigation.Menu'),
(84, 82, 1, NULL, 'AdminMaintenance', NULL, 1, 1, 0, '', 'Maintenance', 'Admin.Navigation.Menu'),
(85, 81, 1, NULL, 'AdminParentOrderPreferences', NULL, 1, 1, 0, '', 'Order Settings', 'Admin.Navigation.Menu'),
(86, 85, 0, NULL, 'AdminOrderPreferences', NULL, 1, 1, 0, '', 'Order Settings', 'Admin.Navigation.Menu'),
(87, 85, 1, NULL, 'AdminStatuses', NULL, 1, 1, 0, '', 'Statuses', 'Admin.Navigation.Menu'),
(88, 81, 2, NULL, 'AdminPPreferences', NULL, 1, 1, 0, '', 'Product Settings', 'Admin.Navigation.Menu'),
(89, 81, 3, NULL, 'AdminParentCustomerPreferences', NULL, 1, 1, 0, '', 'Customer Settings', 'Admin.Navigation.Menu'),
(90, 89, 0, NULL, 'AdminCustomerPreferences', NULL, 1, 1, 0, '', 'Customer Settings', 'Admin.Navigation.Menu'),
(91, 89, 1, NULL, 'AdminGroups', NULL, 1, 1, 0, '', 'Groups', 'Admin.Navigation.Menu'),
(92, 89, 2, NULL, 'AdminGenders', NULL, 1, 1, 0, '', 'Titles', 'Admin.Navigation.Menu'),
(93, 81, 4, NULL, 'AdminParentStores', NULL, 1, 1, 0, '', 'Contact', 'Admin.Navigation.Menu'),
(94, 93, 0, NULL, 'AdminContacts', NULL, 1, 1, 0, '', 'Contacts', 'Admin.Navigation.Menu'),
(95, 93, 1, NULL, 'AdminStores', NULL, 1, 1, 0, '', 'Stores', 'Admin.Navigation.Menu'),
(96, 81, 5, NULL, 'AdminParentMeta', NULL, 1, 1, 0, '', 'Traffic & SEO', 'Admin.Navigation.Menu'),
(97, 96, 0, NULL, 'AdminMeta', NULL, 1, 1, 0, '', 'SEO & URLs', 'Admin.Navigation.Menu'),
(98, 96, 1, NULL, 'AdminSearchEngines', NULL, 1, 1, 0, '', 'Search Engines', 'Admin.Navigation.Menu'),
(99, 96, 2, NULL, 'AdminReferrers', NULL, 1, 1, 0, '', 'Referrers', 'Admin.Navigation.Menu'),
(100, 81, 6, NULL, 'AdminParentSearchConf', NULL, 1, 1, 0, '', 'Search', 'Admin.Navigation.Menu'),
(101, 100, 0, NULL, 'AdminSearchConf', NULL, 1, 1, 0, '', 'Search', 'Admin.Navigation.Menu'),
(102, 100, 1, NULL, 'AdminTags', NULL, 1, 1, 0, '', 'Tags', 'Admin.Navigation.Menu'),
(103, 80, 1, NULL, 'AdminAdvancedParameters', NULL, 1, 1, 0, 'settings_applications', 'Advanced Parameters', 'Admin.Navigation.Menu'),
(104, 103, 0, NULL, 'AdminInformation', NULL, 1, 1, 0, '', 'Information', 'Admin.Navigation.Menu'),
(105, 103, 1, NULL, 'AdminPerformance', NULL, 1, 1, 0, '', 'Performance', 'Admin.Navigation.Menu'),
(106, 103, 2, NULL, 'AdminAdminPreferences', NULL, 1, 1, 0, '', 'Administration', 'Admin.Navigation.Menu'),
(107, 103, 3, NULL, 'AdminEmails', NULL, 1, 1, 0, '', 'E-mail', 'Admin.Navigation.Menu'),
(108, 103, 4, NULL, 'AdminImport', NULL, 1, 1, 0, '', 'Import', 'Admin.Navigation.Menu'),
(109, 103, 5, NULL, 'AdminParentEmployees', NULL, 1, 1, 0, '', 'Team', 'Admin.Navigation.Menu'),
(110, 109, 0, NULL, 'AdminEmployees', NULL, 1, 1, 0, '', 'Employees', 'Admin.Navigation.Menu'),
(111, 109, 1, NULL, 'AdminProfiles', NULL, 1, 1, 0, '', 'Profiles', 'Admin.Navigation.Menu'),
(112, 109, 2, NULL, 'AdminAccess', NULL, 1, 1, 0, '', 'Permissions', 'Admin.Navigation.Menu'),
(113, 103, 6, NULL, 'AdminParentRequestSql', NULL, 1, 1, 0, '', 'Database', 'Admin.Navigation.Menu'),
(114, 113, 0, NULL, 'AdminRequestSql', NULL, 1, 1, 0, '', 'SQL Manager', 'Admin.Navigation.Menu'),
(115, 113, 1, NULL, 'AdminBackup', NULL, 1, 1, 0, '', 'DB Backup', 'Admin.Navigation.Menu'),
(116, 103, 7, NULL, 'AdminLogs', NULL, 1, 1, 0, '', 'Logs', 'Admin.Navigation.Menu'),
(117, 103, 8, NULL, 'AdminWebservice', NULL, 1, 1, 0, '', 'Webservice', 'Admin.Navigation.Menu'),
(118, 103, 9, NULL, 'AdminShopGroup', NULL, 0, 1, 0, '', 'Multistore', 'Admin.Navigation.Menu'),
(119, 103, 10, NULL, 'AdminShopUrl', NULL, 0, 1, 0, '', 'Multistore', 'Admin.Navigation.Menu'),
(120, 103, 11, NULL, 'AdminFeatureFlag', NULL, 1, 1, 0, '', 'Experimental Features', 'Admin.Navigation.Menu'),
(121, -1, 0, NULL, 'AdminQuickAccesses', NULL, 1, 1, 0, '', 'Quick Access', 'Admin.Navigation.Menu'),
(122, 0, 4, NULL, 'DEFAULT', NULL, 1, 1, 0, '', 'More', 'Admin.Navigation.Menu'),
(123, -1, 1, NULL, 'AdminPatterns', NULL, 1, 1, 0, '', '', ''),
(124, 43, 3, 'blockwishlist', 'WishlistConfigurationAdminParentController', '', 0, 1, 0, '', NULL, NULL),
(125, 124, 1, 'blockwishlist', 'WishlistConfigurationAdminController', '', 1, 1, 0, '', NULL, NULL),
(126, 124, 2, 'blockwishlist', 'WishlistStatisticsAdminController', '', 1, 1, 0, '', NULL, NULL),
(127, -1, 2, 'dashgoals', 'AdminDashgoals', '', 1, 1, 0, '', NULL, NULL),
(128, -1, 3, 'ps_faviconnotificationbo', 'AdminConfigureFaviconBo', '', 1, 1, 0, '', NULL, NULL),
(129, 52, 6, 'ps_linklist', 'AdminLinkWidget', 'admin_link_block_list', 1, 1, 0, '', 'Link List', 'Modules.Linklist.Admin'),
(130, 52, 0, '', 'AdminThemesParent', '', 1, 1, 0, '', 'Theme & Logo', 'Admin.Navigation.Menu'),
(131, 130, 2, 'ps_themecusto', 'AdminPsThemeCustoConfiguration', '', 1, 1, 0, '', NULL, NULL),
(132, 130, 3, 'ps_themecusto', 'AdminPsThemeCustoAdvanced', '', 1, 1, 0, '', NULL, NULL),
(133, 0, 5, 'welcome', 'AdminWelcome', '', 1, 1, 0, '', NULL, NULL),
(134, 81, 7, 'gamification', 'AdminGamification', '', 1, 1, 0, '', NULL, NULL),
(135, -1, 4, 'psgdpr', 'AdminAjaxPsgdpr', '', 1, 1, 0, '', NULL, NULL),
(136, -1, 5, 'psgdpr', 'AdminDownloadInvoicesPsgdpr', '', 1, 1, 0, '', NULL, NULL),
(137, 48, 0, 'ps_mbo', 'AdminPsMboModule', '', 1, 1, 0, '', NULL, NULL),
(138, 48, 1, 'ps_mbo', 'AdminPsMboAddons', '', 1, 1, 0, '', NULL, NULL),
(139, -1, 0, 'ps_mbo', 'AdminPsMboRecommended', '', 1, 1, 0, '', NULL, NULL),
(140, 52, 1, 'ps_mbo', 'AdminPsMboTheme', '', 1, 1, 0, '', NULL, NULL),
(141, -1, 6, 'ps_buybuttonlite', 'AdminAjaxPs_buybuttonlite', '', 1, 1, 0, '', NULL, NULL),
(142, -1, 7, 'ps_checkout', 'AdminAjaxPrestashopCheckout', '', 1, 1, 0, '', NULL, NULL),
(143, -1, 8, 'ps_checkout', 'AdminPaypalOnboardingPrestashopCheckout', '', 1, 1, 0, '', NULL, NULL),
(144, 32, 1, 'ps_metrics', 'AdminMetricsLegacyStatsController', '', 1, 1, 0, '', NULL, NULL),
(145, 32, 2, 'ps_metrics', 'AdminMetricsController', '', 1, 1, 0, '', NULL, NULL),
(146, 42, 5, '', 'Marketing', '', 1, 1, 0, 'campaign', NULL, NULL),
(147, 146, 1, 'ps_facebook', 'AdminPsfacebookModule', '', 1, 1, 0, '', NULL, NULL),
(148, -1, 9, 'ps_facebook', 'AdminAjaxPsfacebook', '', 1, 1, 0, '', NULL, NULL),
(149, 146, 2, 'psxmarketingwithgoogle', 'AdminPsxMktgWithGoogleModule', '', 1, 1, 0, '', NULL, NULL),
(150, -1, 10, 'psxmarketingwithgoogle', 'AdminAjaxPsxMktgWithGoogle', '', 1, 1, 0, '', NULL, NULL),
(151, 0, 6, 'blockreassurance', 'AdminBlockListing', '', 0, 1, 0, '', NULL, NULL),
(152, 42, 6, 'myshophelper', 'AdminMyshophelper', '', 1, 1, 0, 'shopping_basket', NULL, NULL),
(153, 0, 7, 'crazyelements', 'AdminCrazyMain', '', 1, 1, 0, '', NULL, NULL),
(154, 153, 1, 'crazyelements', 'AdminCrazyEditor', '', 1, 1, 0, 'brush', NULL, NULL),
(155, 153, 2, 'crazyelements', 'AdminCrazyFonts', '', 1, 1, 0, '', NULL, NULL),
(156, 153, 3, 'crazyelements', 'AdminCrazyPseIcon', '', 1, 1, 0, '', NULL, NULL),
(157, 153, 4, 'crazyelements', 'AdminCrazySetting', '', 1, 1, 0, 'settings', NULL, NULL),
(158, 153, 5, 'crazyelements', 'AdminCrazyExtendedmodules', '', 1, 1, 0, '', NULL, NULL),
(159, -1, 11, 'crazyelements', 'AdminCrazyFrontendEditor', '', 1, 1, 0, '', NULL, NULL),
(160, -1, 12, 'crazyelements', 'AdminCrazyAjaxUrl', '', 1, 1, 0, '', NULL, NULL),
(161, -1, 13, 'crazyelements', 'AdminCrazyImages', '', 1, 1, 0, '', NULL, NULL),
(162, 154, 1, 'crazyelements', 'AdminCrazyContent', '', 1, 1, 0, '', NULL, NULL),
(163, 154, 2, 'crazyelements', 'AdminCrazyPages', '', 1, 1, 0, '', NULL, NULL),
(164, 154, 3, 'crazyelements', 'AdminCrazyProducts', '', 1, 1, 0, '', NULL, NULL),
(165, 154, 4, 'crazyelements', 'AdminCrazyCategories', '', 1, 1, 0, '', NULL, NULL),
(166, 154, 5, 'crazyelements', 'AdminCrazySuppliers', '', 1, 1, 0, '', NULL, NULL),
(167, 154, 6, 'crazyelements', 'AdminCrazyBrands', '', 1, 1, 0, '', NULL, NULL),
(168, 0, 8, 'crazyelements', 'AdminCrazyPrdlayouts', '', 0, 1, 0, '', NULL, NULL),
(169, -1, 14, 'smartsupp', 'AdminSmartsuppAjax', '', 1, 1, 0, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ps_tab_advice`
--

DROP TABLE IF EXISTS `ps_tab_advice`;
CREATE TABLE IF NOT EXISTS `ps_tab_advice` (
  `id_tab` int(11) NOT NULL,
  `id_advice` int(11) NOT NULL,
  PRIMARY KEY (`id_tab`,`id_advice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_tab_lang`
--

DROP TABLE IF EXISTS `ps_tab_lang`;
CREATE TABLE IF NOT EXISTS `ps_tab_lang` (
  `id_tab` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_tab`,`id_lang`),
  KEY `IDX_CFD9262DED47AB56` (`id_tab`),
  KEY `IDX_CFD9262DBA299860` (`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ps_tab_lang`
--

INSERT INTO `ps_tab_lang` (`id_tab`, `id_lang`, `name`) VALUES
(1, 1, 'Dashboard'),
(1, 2, 'Tableau de bord'),
(1, 3, 'Übersicht'),
(1, 4, 'Dashboard'),
(1, 5, 'Pannello di controllo'),
(1, 6, 'Inicio'),
(2, 1, 'Sell'),
(2, 2, 'Vendre'),
(2, 3, 'Verkauf'),
(2, 4, 'Verkopen'),
(2, 5, 'Vendi'),
(2, 6, 'Vender'),
(3, 1, 'Orders'),
(3, 2, 'Commandes'),
(3, 3, 'Bestellungen'),
(3, 4, 'Bestellingen'),
(3, 5, 'Ordini'),
(3, 6, 'Pedidos'),
(4, 1, 'Orders'),
(4, 2, 'Commandes'),
(4, 3, 'Bestellungen'),
(4, 4, 'Bestellingen'),
(4, 5, 'Ordini'),
(4, 6, 'Pedidos'),
(5, 1, 'Invoices'),
(5, 2, 'Factures'),
(5, 3, 'Rechnungen'),
(5, 4, 'Facturen'),
(5, 5, 'Fatture'),
(5, 6, 'Facturas'),
(6, 1, 'Credit Slips'),
(6, 2, 'Avoirs'),
(6, 3, 'Rechnungskorrekturen'),
(6, 4, 'Creditnota\'s'),
(6, 5, 'Buoni sconto'),
(6, 6, 'Facturas por abono'),
(7, 1, 'Delivery Slips'),
(7, 2, 'Bons de livraison'),
(7, 3, 'Lieferscheine'),
(7, 4, 'Pakbonnen'),
(7, 5, 'Bolle di consegna'),
(7, 6, 'Albaranes de entrega'),
(8, 1, 'Shopping Carts'),
(8, 2, 'Paniers'),
(8, 3, 'Warenkörbe'),
(8, 4, 'Winkelwagens'),
(8, 5, 'Carrello della spesa'),
(8, 6, 'Carritos de compra'),
(9, 1, 'Catalog'),
(9, 2, 'Catalogue'),
(9, 3, 'Katalog'),
(9, 4, 'Catalogus'),
(9, 5, 'Catalogo'),
(9, 6, 'Catálogo'),
(10, 1, 'Products'),
(10, 2, 'Produits'),
(10, 3, 'Artikel'),
(10, 4, 'Producten'),
(10, 5, 'Prodotti'),
(10, 6, 'Productos'),
(11, 1, 'Categories'),
(11, 2, 'Catégories'),
(11, 3, 'Kategorien'),
(11, 4, 'Categorieën'),
(11, 5, 'Categorie'),
(11, 6, 'Categorías'),
(12, 1, 'Monitoring'),
(12, 2, 'Suivi'),
(12, 3, 'Kontrollübersicht'),
(12, 4, 'Monitoring'),
(12, 5, 'Monitoraggio'),
(12, 6, 'Monitoreo'),
(13, 1, 'Attributes & Features'),
(13, 2, 'Attributs & caractéristiques'),
(13, 3, 'Varianten & Eigenschaften'),
(13, 4, 'Kenmerken en functies'),
(13, 5, 'Attributi e Funzionalità'),
(13, 6, 'Atributos y Características'),
(14, 1, 'Attributes'),
(14, 2, 'Attributs'),
(14, 3, 'Varianten'),
(14, 4, 'Attributen'),
(14, 5, 'Attributi'),
(14, 6, 'Atributos'),
(15, 1, 'Features'),
(15, 2, 'Caractéristiques'),
(15, 3, 'Eigenschaften'),
(15, 4, 'Functies'),
(15, 5, 'Funzioni'),
(15, 6, 'Características'),
(16, 1, 'Brands & Suppliers'),
(16, 2, 'Marques et fournisseurs'),
(16, 3, 'Marken & Lieferanten'),
(16, 4, 'Merken en leveranciers'),
(16, 5, 'Marche & Fornitori'),
(16, 6, 'Marcas y Proveedores'),
(17, 1, 'Brands'),
(17, 2, 'Marques'),
(17, 3, 'Marken'),
(17, 4, 'Merken'),
(17, 5, 'Marchi'),
(17, 6, 'Marcas'),
(18, 1, 'Suppliers'),
(18, 2, 'Fournisseurs'),
(18, 3, 'Lieferanten'),
(18, 4, 'Leveranciers'),
(18, 5, 'Fornitori'),
(18, 6, 'Proveedores'),
(19, 1, 'Files'),
(19, 2, 'Fichiers'),
(19, 3, 'Dateien'),
(19, 4, 'Bestanden'),
(19, 5, 'File'),
(19, 6, 'Archivos'),
(20, 1, 'Discounts'),
(20, 2, 'Réductions'),
(20, 3, 'Rabatt'),
(20, 4, 'Kortingen'),
(20, 5, 'Buoni sconto'),
(20, 6, 'Descuentos'),
(21, 1, 'Cart Rules'),
(21, 2, 'Règles panier'),
(21, 3, 'Warenkorb Preisregeln'),
(21, 4, 'Winkelwagenregels'),
(21, 5, 'Regole Carrello'),
(21, 6, 'Reglas del carrito'),
(22, 1, 'Catalog Price Rules'),
(22, 2, 'Règles de prix catalogue'),
(22, 3, 'Katalog Preisregeln'),
(22, 4, 'Catalogusprijsregels'),
(22, 5, 'Regole Catalogo Prezzi'),
(22, 6, 'Reglas de precio del catálogo'),
(23, 1, 'Stock'),
(23, 2, 'Stock'),
(23, 3, 'Lager'),
(23, 4, 'Voorraad'),
(23, 5, 'Magazzino'),
(23, 6, 'Stock'),
(24, 1, 'Customers'),
(24, 2, 'Clients'),
(24, 3, 'Kunden'),
(24, 4, 'Klanten'),
(24, 5, 'Clienti'),
(24, 6, 'Clientes'),
(25, 1, 'Customers'),
(25, 2, 'Clients'),
(25, 3, 'Kunden'),
(25, 4, 'Klanten'),
(25, 5, 'Clienti'),
(25, 6, 'Clientes'),
(26, 1, 'Addresses'),
(26, 2, 'Adresses'),
(26, 3, 'Adressen'),
(26, 4, 'Adressen'),
(26, 5, 'Indirizzi'),
(26, 6, 'Direcciones'),
(27, 1, 'Outstanding'),
(27, 2, 'Encours autorisés'),
(27, 3, 'Offene Posten'),
(27, 4, 'Openstaand'),
(27, 5, 'In sospeso'),
(27, 6, 'Saldo pendiente por cobrar'),
(28, 1, 'Customer Service'),
(28, 2, 'SAV'),
(28, 3, 'Kundenservice'),
(28, 4, 'Klantenservice'),
(28, 5, 'Servizio clienti'),
(28, 6, 'Servicio al Cliente'),
(29, 1, 'Customer Service'),
(29, 2, 'SAV'),
(29, 3, 'Kundenservice'),
(29, 4, 'Klantenservice'),
(29, 5, 'Servizio clienti'),
(29, 6, 'Servicio al Cliente'),
(30, 1, 'Order Messages'),
(30, 2, 'Messages prédéfinis'),
(30, 3, 'Bestellnachrichten'),
(30, 4, 'Bestellingsberichten'),
(30, 5, 'Messaggi d\'ordine'),
(30, 6, 'Mensajes de Pedidos'),
(31, 1, 'Merchandise Returns'),
(31, 2, 'Retours produits'),
(31, 3, 'Warenrücksendungen'),
(31, 4, 'Retourzendingen'),
(31, 5, 'Restituzione Prodotto'),
(31, 6, 'Devoluciones de mercancía'),
(32, 1, 'Stats'),
(32, 2, 'Statistiques'),
(32, 3, 'Statistiken'),
(32, 4, 'Statistieken'),
(32, 5, 'Statistiche'),
(32, 6, 'Estadísticas'),
(34, 1, 'Warehouses'),
(34, 2, 'Entrepôts'),
(34, 3, 'Lager'),
(34, 4, 'Magazijnen'),
(34, 5, 'Magazzini'),
(34, 6, 'Almacenes'),
(35, 1, 'Stock Management'),
(35, 2, 'Gestion du stock'),
(35, 3, 'Lagerverwaltung'),
(35, 4, 'Voorraadbeheer'),
(35, 5, 'Gestione del Magazzino'),
(35, 6, 'Gestión de stock'),
(37, 1, 'Stock Movement'),
(37, 2, 'Mouvements de stock'),
(37, 3, 'Lagerbewegung'),
(37, 4, 'Voorraadverplaatsing'),
(37, 5, 'Movimenti di magazzino'),
(37, 6, 'Movimiento de Stock'),
(38, 1, 'Instant Stock Status'),
(38, 2, 'État instantané du stock'),
(38, 3, 'Aktueller Lagerbestand'),
(38, 4, 'Huidige voorraadstatus'),
(38, 5, 'Stato dello Stock Istantaneo'),
(38, 6, 'Estado actual del stock'),
(39, 1, 'Stock Coverage'),
(39, 2, 'Couverture du stock'),
(39, 3, 'Lagerbestand'),
(39, 4, 'Voorraaddekking'),
(39, 5, 'Disponibilità di magazzino'),
(39, 6, 'Cobertura de stock'),
(40, 1, 'Supply orders'),
(40, 2, 'Commandes fournisseurs'),
(40, 3, 'Bestellaufträge'),
(40, 4, 'Leveringsbestellingen'),
(40, 5, 'Ordini fornitura'),
(40, 6, 'Pedidos de suministros'),
(41, 1, 'Configuration'),
(41, 2, 'Paramètres'),
(41, 3, 'Einstellungen'),
(41, 4, 'Configuratie'),
(41, 5, 'Configurazione'),
(41, 6, 'Configuración'),
(42, 1, 'Improve'),
(42, 2, 'Personnaliser'),
(42, 3, 'Optimierung'),
(42, 4, 'Verbeteren'),
(42, 5, 'Migliora'),
(42, 6, 'Personalizar'),
(43, 1, 'Modules'),
(43, 2, 'Modules'),
(43, 3, 'Module'),
(43, 4, 'Modules'),
(43, 5, 'Moduli'),
(43, 6, 'Módulos'),
(44, 1, 'Module Manager'),
(44, 2, 'Gestionnaire de modules '),
(44, 3, 'Modul-Verwaltung'),
(44, 4, 'Module manager'),
(44, 5, 'Gestione Moduli'),
(44, 6, 'Gestor de módulo'),
(45, 1, 'Modules'),
(45, 2, 'Modules'),
(45, 3, 'Module'),
(45, 4, 'Modules'),
(45, 5, 'Moduli'),
(45, 6, 'Módulos'),
(46, 1, 'Alerts'),
(46, 2, 'Alertes'),
(46, 3, 'Meldungen'),
(46, 4, 'Waarschuwingen'),
(46, 5, 'Avvisi'),
(46, 6, 'Alertas'),
(47, 1, 'Updates'),
(47, 2, 'Mises à jour'),
(47, 3, 'Aktualisierungen'),
(47, 4, 'Updates'),
(47, 5, 'Aggiornamenti'),
(47, 6, 'Actualizaciones'),
(48, 1, 'Module Catalog'),
(48, 2, 'Catalogue de modules'),
(48, 3, 'Modul-Katalog'),
(48, 4, 'Module catalogus'),
(48, 5, 'Catalogo Moduli'),
(48, 6, 'Catálogo de Módulos'),
(49, 1, 'Module Catalog'),
(49, 2, 'Catalogue de modules'),
(49, 3, 'Modul-Katalog'),
(49, 4, 'Module catalogus'),
(49, 5, 'Catalogo Moduli'),
(49, 6, 'Catálogo de Módulos'),
(50, 1, 'Module Selections'),
(50, 2, 'Module Selections'),
(50, 3, 'Module Selections'),
(50, 4, 'Module Selections'),
(50, 5, 'Module Selections'),
(50, 6, 'Module Selections'),
(52, 1, 'Design'),
(52, 2, 'Apparence'),
(52, 3, 'Design'),
(52, 4, 'Design'),
(52, 5, 'Design'),
(52, 6, 'Diseño'),
(53, 1, 'Theme & Logo'),
(53, 2, 'Thème et logo'),
(53, 3, 'Template und Logo'),
(53, 4, 'Thema en logo'),
(53, 5, 'Tema & Logo'),
(53, 6, 'Tema y logotipo'),
(54, 1, 'Theme Catalog'),
(54, 2, 'Catalogue de thèmes'),
(54, 3, 'Templates'),
(54, 4, 'Themacatalogus'),
(54, 5, 'Catalogo dei Temi'),
(54, 6, 'Catálogo de Temas'),
(55, 1, 'Email Theme'),
(55, 2, 'Thème d\'email'),
(55, 3, 'E-Mail-Theme'),
(55, 4, 'Email thema'),
(55, 5, 'Tema Email'),
(55, 6, 'Tema Email'),
(56, 1, 'Email Theme'),
(56, 2, 'Thème d\'email'),
(56, 3, 'E-Mail-Theme'),
(56, 4, 'Email thema'),
(56, 5, 'Tema Email'),
(56, 6, 'Tema Email'),
(57, 1, 'Pages'),
(57, 2, 'Pages'),
(57, 3, 'Seiten'),
(57, 4, 'Pagina\'s'),
(57, 5, 'Pagine'),
(57, 6, 'Páginas'),
(58, 1, 'Positions'),
(58, 2, 'Positions'),
(58, 3, 'Positionen'),
(58, 4, 'Posities'),
(58, 5, 'Posizioni'),
(58, 6, 'Posiciones'),
(59, 1, 'Image Settings'),
(59, 2, 'Images'),
(59, 3, 'Bilder'),
(59, 4, 'Afbeeldingsinstellingen'),
(59, 5, 'Impostazioni immagine'),
(59, 6, 'Ajustes de imágenes'),
(60, 1, 'Shipping'),
(60, 2, 'Livraison'),
(60, 3, 'Versand'),
(60, 4, 'Verzending'),
(60, 5, 'Spedizione'),
(60, 6, 'Transporte'),
(61, 1, 'Carriers'),
(61, 2, 'Transporteurs'),
(61, 3, 'Versanddienste'),
(61, 4, 'Vervoerders'),
(61, 5, 'Mezzi di spedizione'),
(61, 6, 'Transportistas'),
(62, 1, 'Preferences'),
(62, 2, 'Préférences'),
(62, 3, 'Voreinstellungen'),
(62, 4, 'Instellingen'),
(62, 5, 'Impostazioni'),
(62, 6, 'Preferencias'),
(63, 1, 'Payment'),
(63, 2, 'Paiement'),
(63, 3, 'Zahlung'),
(63, 4, 'Betaling'),
(63, 5, 'Pagamento'),
(63, 6, 'Pago'),
(64, 1, 'Payment Methods'),
(64, 2, 'Modes de paiement'),
(64, 3, 'Zahlungsarten'),
(64, 4, 'Betaalmethoden'),
(64, 5, 'Metodi di Pagamento'),
(64, 6, 'Métodos de pago'),
(65, 1, 'Preferences'),
(65, 2, 'Préférences'),
(65, 3, 'Voreinstellungen'),
(65, 4, 'Instellingen'),
(65, 5, 'Impostazioni'),
(65, 6, 'Preferencias'),
(66, 1, 'International'),
(66, 2, 'International'),
(66, 3, 'International'),
(66, 4, 'Internationaal'),
(66, 5, 'Internazionale'),
(66, 6, 'Internacional'),
(67, 1, 'Localization'),
(67, 2, 'Localisation'),
(67, 3, 'Lokalisierung'),
(67, 4, 'Lokalisatie'),
(67, 5, 'Localizzazione'),
(67, 6, 'Localización'),
(68, 1, 'Localization'),
(68, 2, 'Localisation'),
(68, 3, 'Lokalisierung'),
(68, 4, 'Lokalisatie'),
(68, 5, 'Localizzazione'),
(68, 6, 'Localización'),
(69, 1, 'Languages'),
(69, 2, 'Langues'),
(69, 3, 'Sprachen'),
(69, 4, 'Talen'),
(69, 5, 'Lingue'),
(69, 6, 'Idiomas'),
(70, 1, 'Currencies'),
(70, 2, 'Devises'),
(70, 3, 'Währungen'),
(70, 4, 'Valuta\'s'),
(70, 5, 'Valute'),
(70, 6, 'Monedas'),
(71, 1, 'Geolocation'),
(71, 2, 'Géolocalisation'),
(71, 3, 'Geotargeting'),
(71, 4, 'Geolocatie'),
(71, 5, 'Geolocalizzazione'),
(71, 6, 'Geolocalización'),
(72, 1, 'Locations'),
(72, 2, 'Zones géographiques'),
(72, 3, 'Länder & Gebiete'),
(72, 4, 'Locaties'),
(72, 5, 'Località'),
(72, 6, 'Ubicaciones Geográficas'),
(73, 1, 'Zones'),
(73, 2, 'Zones'),
(73, 3, 'Gebiete'),
(73, 4, 'Zones'),
(73, 5, 'Zone'),
(73, 6, 'Zonas'),
(74, 1, 'Countries'),
(74, 2, 'Pays'),
(74, 3, 'Länder'),
(74, 4, 'Landen'),
(74, 5, 'Nazione'),
(74, 6, 'Países'),
(75, 1, 'States'),
(75, 2, 'États'),
(75, 3, 'Bundesländer'),
(75, 4, 'Provincies'),
(75, 5, 'Province'),
(75, 6, 'Provincias'),
(76, 1, 'Taxes'),
(76, 2, 'Taxes'),
(76, 3, 'Steuersätze'),
(76, 4, 'BTW'),
(76, 5, 'Tasse'),
(76, 6, 'Impuestos'),
(77, 1, 'Taxes'),
(77, 2, 'Taxes'),
(77, 3, 'Steuersätze'),
(77, 4, 'BTW'),
(77, 5, 'Tasse'),
(77, 6, 'Impuestos'),
(78, 1, 'Tax Rules'),
(78, 2, 'Règles de taxes'),
(78, 3, 'Steuerregeln'),
(78, 4, 'Belastingregel'),
(78, 5, 'Aliquote Iva'),
(78, 6, 'Reglas de impuestos'),
(79, 1, 'Translations'),
(79, 2, 'Traductions'),
(79, 3, 'Übersetzungen'),
(79, 4, 'Vertalingen'),
(79, 5, 'Traduzioni'),
(79, 6, 'Traducciones'),
(80, 1, 'Configure'),
(80, 2, 'Configurer'),
(80, 3, 'Einstellungen'),
(80, 4, 'Configureer'),
(80, 5, 'Configura'),
(80, 6, 'Configurar'),
(81, 1, 'Shop Parameters'),
(81, 2, 'Paramètres de la boutique'),
(81, 3, 'Shop-Einstellungen'),
(81, 4, 'Winkelinstellingen'),
(81, 5, 'Parametri Negozio'),
(81, 6, 'Parámetros de la tienda'),
(82, 1, 'General'),
(82, 2, 'Paramètres généraux'),
(82, 3, 'Allgemein'),
(82, 4, 'Algemeen'),
(82, 5, 'Generale'),
(82, 6, 'Configuración'),
(83, 1, 'General'),
(83, 2, 'Paramètres généraux'),
(83, 3, 'Allgemein'),
(83, 4, 'Algemeen'),
(83, 5, 'Generale'),
(83, 6, 'Configuración'),
(84, 1, 'Maintenance'),
(84, 2, 'Maintenance'),
(84, 3, 'Wartung'),
(84, 4, 'Onderhoud'),
(84, 5, 'Manutenzione'),
(84, 6, 'Mantenimiento'),
(85, 1, 'Order Settings'),
(85, 2, 'Commandes'),
(85, 3, 'Bestellungen'),
(85, 4, 'Bestellingsinstellingen'),
(85, 5, 'Impostazioni Ordine'),
(85, 6, 'Configuración de Pedidos'),
(86, 1, 'Order Settings'),
(86, 2, 'Commandes'),
(86, 3, 'Bestellungen'),
(86, 4, 'Bestellingsinstellingen'),
(86, 5, 'Impostazioni Ordine'),
(86, 6, 'Configuración de Pedidos'),
(87, 1, 'Statuses'),
(87, 2, 'États de commandes'),
(87, 3, 'Status'),
(87, 4, 'Statussen'),
(87, 5, 'Stati'),
(87, 6, 'Estados'),
(88, 1, 'Product Settings'),
(88, 2, 'Produits'),
(88, 3, 'Artikel'),
(88, 4, 'Producten'),
(88, 5, 'Prodotti'),
(88, 6, 'Configuración de Productos'),
(89, 1, 'Customer Settings'),
(89, 2, 'Clients'),
(89, 3, 'Benutzerdefinierte Einstellungen'),
(89, 4, 'Klantinstellingen'),
(89, 5, 'Impostazioni clienti'),
(89, 6, 'Ajustes sobre clientes'),
(90, 1, 'Customer Settings'),
(90, 2, 'Clients'),
(90, 3, 'Benutzerdefinierte Einstellungen'),
(90, 4, 'Klantinstellingen'),
(90, 5, 'Impostazioni clienti'),
(90, 6, 'Ajustes sobre clientes'),
(91, 1, 'Groups'),
(91, 2, 'Groupes'),
(91, 3, 'Gruppen'),
(91, 4, 'Groepen'),
(91, 5, 'Gruppi'),
(91, 6, 'Grupos'),
(92, 1, 'Titles'),
(92, 2, 'Titres de civilité'),
(92, 3, 'Bezeichnung'),
(92, 4, 'Sociale titels'),
(92, 5, 'Titoli'),
(92, 6, 'Tratamientos'),
(93, 1, 'Contact'),
(93, 2, 'Contact'),
(93, 3, 'Kontakt'),
(93, 4, 'Contact'),
(93, 5, 'Contatto'),
(93, 6, 'Contacto'),
(94, 1, 'Contacts'),
(94, 2, 'Contacts'),
(94, 3, 'Kontakte'),
(94, 4, 'Contactpersonen'),
(94, 5, 'Contatti'),
(94, 6, 'Contacto'),
(95, 1, 'Stores'),
(95, 2, 'Magasins'),
(95, 3, 'Shops'),
(95, 4, 'Winkels'),
(95, 5, 'Negozi'),
(95, 6, 'Tiendas'),
(96, 1, 'Traffic & SEO'),
(96, 2, 'Trafic et SEO'),
(96, 3, 'Traffic & SEO'),
(96, 4, 'Verkeer en SEO'),
(96, 5, 'Traffico & SEO'),
(96, 6, 'Tráfico & SEO'),
(97, 1, 'SEO & URLs'),
(97, 2, 'SEO & URL'),
(97, 3, 'SEO & URLs'),
(97, 4, 'SEO & URL\'s'),
(97, 5, 'SEO & URLs'),
(97, 6, 'SEO y URLs'),
(98, 1, 'Search Engines'),
(98, 2, 'Moteurs de recherche'),
(98, 3, 'Suchmaschinen'),
(98, 4, 'Zoekmachines'),
(98, 5, 'Motori di ricerca'),
(98, 6, 'Motores de búsqueda'),
(99, 1, 'Referrers'),
(99, 2, 'Affiliés'),
(99, 3, 'Referrer'),
(99, 4, 'Referrers'),
(99, 5, 'Affiliati'),
(99, 6, 'Afiliados'),
(100, 1, 'Search'),
(100, 2, 'Rechercher'),
(100, 3, 'Suche'),
(100, 4, 'Zoeken'),
(100, 5, 'Cerca'),
(100, 6, 'Buscar'),
(101, 1, 'Search'),
(101, 2, 'Rechercher'),
(101, 3, 'Suche'),
(101, 4, 'Zoeken'),
(101, 5, 'Cerca'),
(101, 6, 'Buscar'),
(102, 1, 'Tags'),
(102, 2, 'Mots-clés'),
(102, 3, 'Stichwörter'),
(102, 4, 'Tags'),
(102, 5, 'Tags'),
(102, 6, 'Etiquetas'),
(103, 1, 'Advanced Parameters'),
(103, 2, 'Paramètres avancés'),
(103, 3, 'Erweiterte Einstellungen'),
(103, 4, 'Geavanceerde instellingen'),
(103, 5, 'Parametri Avanzati'),
(103, 6, 'Parámetros Avanzados'),
(104, 1, 'Information'),
(104, 2, 'Informations'),
(104, 3, 'Informationen'),
(104, 4, 'Informatie'),
(104, 5, 'Informazioni'),
(104, 6, 'Información'),
(105, 1, 'Performance'),
(105, 2, 'Performances'),
(105, 3, 'Leistung'),
(105, 4, 'Prestaties'),
(105, 5, 'Prestazioni'),
(105, 6, 'Rendimiento'),
(106, 1, 'Administration'),
(106, 2, 'Administration'),
(106, 3, 'Verwaltung'),
(106, 4, 'Administratie'),
(106, 5, 'Amministrazione'),
(106, 6, 'Administración'),
(107, 1, 'E-mail'),
(107, 2, 'Email'),
(107, 3, 'E-Mail'),
(107, 4, 'E-mail'),
(107, 5, 'Email'),
(107, 6, 'Dirección de correo electrónico'),
(108, 1, 'Import'),
(108, 2, 'Importer'),
(108, 3, 'Importieren'),
(108, 4, 'Importeren'),
(108, 5, 'Importa'),
(108, 6, 'Importar'),
(109, 1, 'Team'),
(109, 2, 'Équipe'),
(109, 3, 'Benutzerrechte'),
(109, 4, 'Medewerkers'),
(109, 5, 'Dipendenti'),
(109, 6, 'Equipo'),
(110, 1, 'Employees'),
(110, 2, 'Employés'),
(110, 3, 'Mitarbeiter'),
(110, 4, 'Medewerkers'),
(110, 5, 'Dipendenti'),
(110, 6, 'Empleados'),
(111, 1, 'Profiles'),
(111, 2, 'Profils'),
(111, 3, 'Profile'),
(111, 4, 'Profielen'),
(111, 5, 'Profili'),
(111, 6, 'Perfiles'),
(112, 1, 'Permissions'),
(112, 2, 'Permissions'),
(112, 3, 'Berechtigungen'),
(112, 4, 'Permissies'),
(112, 5, 'Permessi'),
(112, 6, 'Permisos'),
(113, 1, 'Database'),
(113, 2, 'Base de données'),
(113, 3, 'Datenbank'),
(113, 4, 'Database'),
(113, 5, 'Database'),
(113, 6, 'Base de datos'),
(114, 1, 'SQL Manager'),
(114, 2, 'Gestionnaire SQL'),
(114, 3, 'SQL-Abfragen'),
(114, 4, 'SQL-beheer'),
(114, 5, 'Manager SQL'),
(114, 6, 'Gestor SQL'),
(115, 1, 'DB Backup'),
(115, 2, 'Sauvegarde BDD'),
(115, 3, 'Datenbank-Backup'),
(115, 4, 'DB-backup'),
(115, 5, 'Backup DB'),
(115, 6, 'Respaldar BD'),
(116, 1, 'Logs'),
(116, 2, 'Logs'),
(116, 3, 'Log-Dateien'),
(116, 4, 'Logboeken'),
(116, 5, 'Logs'),
(116, 6, 'Registros/Logs'),
(117, 1, 'Webservice'),
(117, 2, 'Webservice'),
(117, 3, 'Webservice'),
(117, 4, 'Webservice'),
(117, 5, 'Webservice'),
(117, 6, 'Webservice'),
(118, 1, 'Multistore'),
(118, 2, 'Multiboutique'),
(118, 3, 'Multishop'),
(118, 4, 'Multistore'),
(118, 5, 'Multinegozio'),
(118, 6, 'Multitienda'),
(119, 1, 'Multistore'),
(119, 2, 'Multiboutique'),
(119, 3, 'Multishop'),
(119, 4, 'Multistore'),
(119, 5, 'Multinegozio'),
(119, 6, 'Multitienda'),
(120, 1, 'Experimental Features'),
(120, 2, 'Fonctionnalités expérimentales'),
(120, 3, 'Experimentelle Funktionen'),
(120, 4, 'Experimentele Functies'),
(120, 5, 'Funzionalità Sperimentali'),
(120, 6, 'Características experimentales'),
(121, 1, 'Quick Access'),
(121, 2, 'Accès rapide'),
(121, 3, 'Schnellzugriff'),
(121, 4, 'Snelle toegang'),
(121, 5, 'Accesso Veloce'),
(121, 6, 'Acceso rápido'),
(122, 1, 'More'),
(122, 2, 'Détails'),
(122, 3, 'Mehr'),
(122, 4, 'Meer'),
(122, 5, 'Più'),
(122, 6, 'Más'),
(124, 1, 'Wishlist Module'),
(124, 2, 'Wishlist Module'),
(124, 3, 'Wishlist Module'),
(124, 4, 'Wishlist Module'),
(124, 5, 'Wishlist Module'),
(124, 6, 'Wishlist Module'),
(125, 1, 'Configuration'),
(125, 2, 'Paramètres'),
(125, 3, 'Einstellungen'),
(125, 4, 'Configuratie'),
(125, 5, 'Configurazione'),
(125, 6, 'Configuración'),
(126, 1, 'Statistics'),
(126, 2, 'Statistics'),
(126, 3, 'Statistics'),
(126, 4, 'Statistics'),
(126, 5, 'Statistics'),
(126, 6, 'Statistics'),
(127, 1, 'Dashgoals'),
(127, 2, 'Dashgoals'),
(127, 3, 'Dashgoals'),
(127, 4, 'Dashgoals'),
(127, 5, 'Dashgoals'),
(127, 6, 'Dashgoals'),
(128, 1, 'Order Notifications on the Favicon'),
(128, 2, 'Order Notifications on the Favicon'),
(128, 3, 'Order Notifications on the Favicon'),
(128, 4, 'Order Notifications on the Favicon'),
(128, 5, 'Order Notifications on the Favicon'),
(128, 6, 'Order Notifications on the Favicon'),
(129, 1, 'Link List'),
(129, 2, 'Liste de liens'),
(129, 3, 'Linkliste'),
(129, 4, 'Linklijst'),
(129, 5, 'Elenco Link'),
(129, 6, 'Lista de enlaces'),
(130, 1, 'Theme & Logo'),
(130, 2, 'Thème et logo'),
(130, 3, 'Template und Logo'),
(130, 4, 'Thema en logo'),
(130, 5, 'Tema & Logo'),
(130, 6, 'Tema y logotipo'),
(131, 1, 'Pages Configuration'),
(131, 2, 'Pages Configuration'),
(131, 3, 'Pages Configuration'),
(131, 4, 'Pages Configuration'),
(131, 5, 'Pages Configuration'),
(131, 6, 'Pages Configuration'),
(132, 1, 'Advanced Customization'),
(132, 2, 'Personnalisation avancée'),
(132, 3, 'Advanced Customization'),
(132, 4, 'Advanced Customization'),
(132, 5, 'Advanced Customization'),
(132, 6, 'Advanced Customization'),
(133, 1, 'Welcome'),
(133, 2, 'Welcome'),
(133, 3, 'Welcome'),
(133, 4, 'Welcome'),
(133, 5, 'Welcome'),
(133, 6, 'Welcome'),
(134, 1, 'Merchant Expertise'),
(134, 2, 'Merchant Expertise'),
(134, 3, 'Merchant Expertise'),
(134, 4, 'Merchant Expertise'),
(134, 5, 'Merchant Expertise'),
(134, 6, 'Merchant Expertise'),
(135, 1, 'Official GDPR compliance'),
(135, 2, 'Official GDPR compliance'),
(135, 3, 'Official GDPR compliance'),
(135, 4, 'Official GDPR compliance'),
(135, 5, 'Official GDPR compliance'),
(135, 6, 'Official GDPR compliance'),
(136, 1, 'Official GDPR compliance'),
(136, 2, 'Official GDPR compliance'),
(136, 3, 'Official GDPR compliance'),
(136, 4, 'Official GDPR compliance'),
(136, 5, 'Official GDPR compliance'),
(136, 6, 'Official GDPR compliance'),
(137, 1, 'Module Catalog'),
(137, 2, 'Catalogue de modules'),
(137, 3, 'Modul-Katalog'),
(137, 4, 'Module catalogus'),
(137, 5, 'Catalogo Moduli'),
(137, 6, 'Catálogo de Módulos'),
(138, 1, 'Module Selections'),
(138, 2, 'Module Selections'),
(138, 3, 'Module Selections'),
(138, 4, 'Module Selections'),
(138, 5, 'Module Selections'),
(138, 6, 'Module Selections'),
(139, 1, 'Module recommended'),
(139, 2, 'Module recommended'),
(139, 3, 'Module recommended'),
(139, 4, 'Module recommended'),
(139, 5, 'Module recommended'),
(139, 6, 'Module recommended'),
(140, 1, 'Theme Catalog'),
(140, 2, 'Catalogue de thèmes'),
(140, 3, 'Templates'),
(140, 4, 'Themacatalogus'),
(140, 5, 'Catalogo dei Temi'),
(140, 6, 'Catálogo de Temas'),
(141, 1, 'ps_buybuttonlite'),
(141, 2, 'ps_buybuttonlite'),
(141, 3, 'ps_buybuttonlite'),
(141, 4, 'ps_buybuttonlite'),
(141, 5, 'ps_buybuttonlite'),
(141, 6, 'ps_buybuttonlite'),
(142, 1, 'PrestaShop Checkout'),
(142, 2, 'PrestaShop Checkout'),
(142, 3, 'PrestaShop Checkout'),
(142, 4, 'PrestaShop Checkout'),
(142, 5, 'PrestaShop Checkout'),
(142, 6, 'PrestaShop Checkout'),
(143, 1, 'PrestaShop Checkout'),
(143, 2, 'PrestaShop Checkout'),
(143, 3, 'PrestaShop Checkout'),
(143, 4, 'PrestaShop Checkout'),
(143, 5, 'PrestaShop Checkout'),
(143, 6, 'PrestaShop Checkout'),
(144, 1, 'Stats'),
(144, 2, 'Statistiques'),
(144, 3, 'Statistiken'),
(144, 4, 'Statistieken'),
(144, 5, 'Statistiche'),
(144, 6, 'Estadísticas'),
(145, 1, 'PrestaShop Metrics'),
(145, 2, 'PrestaShop Metrics'),
(145, 3, 'PrestaShop Metrics'),
(145, 4, 'PrestaShop Metrics'),
(145, 5, 'PrestaShop Metrics'),
(145, 6, 'PrestaShop Metrics'),
(146, 1, 'Marketing'),
(146, 2, 'Marketing'),
(146, 3, 'Marketing-Addons'),
(146, 4, 'Marketing'),
(146, 5, 'Marketing'),
(146, 6, 'Marketing'),
(147, 1, 'Facebook'),
(147, 2, 'Facebook'),
(147, 3, 'Facebook'),
(147, 4, 'Facebook'),
(147, 5, 'Facebook'),
(147, 6, 'Facebook'),
(148, 1, 'ps_facebook'),
(148, 2, 'ps_facebook'),
(148, 3, 'ps_facebook'),
(148, 4, 'ps_facebook'),
(148, 5, 'ps_facebook'),
(148, 6, 'ps_facebook'),
(149, 1, 'Google'),
(149, 2, 'Google'),
(149, 3, 'Google'),
(149, 4, 'Google'),
(149, 5, 'Google'),
(149, 6, 'Google'),
(150, 1, 'psxmarketingwithgoogle'),
(150, 2, 'psxmarketingwithgoogle'),
(150, 3, 'psxmarketingwithgoogle'),
(150, 4, 'psxmarketingwithgoogle'),
(150, 5, 'psxmarketingwithgoogle'),
(150, 6, 'psxmarketingwithgoogle'),
(151, 1, 'AdminBlockListing'),
(151, 2, 'AdminBlockListing'),
(151, 3, 'AdminBlockListing'),
(151, 4, 'AdminBlockListing'),
(151, 5, 'AdminBlockListing'),
(151, 6, 'AdminBlockListing'),
(152, 1, 'MyShop Settings'),
(152, 2, 'MyShop Settings'),
(152, 3, 'MyShop Settings'),
(152, 4, 'MyShop Settings'),
(152, 5, 'MyShop Settings'),
(152, 6, 'MyShop Settings'),
(153, 1, 'Crazy Elements'),
(153, 2, 'Crazy Elements'),
(153, 3, 'Crazy Elements'),
(153, 4, 'Crazy Elements'),
(153, 5, 'Crazy Elements'),
(153, 6, 'Crazy Elements'),
(154, 1, 'Crazy Editors'),
(154, 2, 'Crazy Editors'),
(154, 3, 'Crazy Editors'),
(154, 4, 'Crazy Editors'),
(154, 5, 'Crazy Editors'),
(154, 6, 'Crazy Editors'),
(155, 1, 'Font Manager'),
(155, 2, 'Font Manager'),
(155, 3, 'Font Manager'),
(155, 4, 'Font Manager'),
(155, 5, 'Font Manager'),
(155, 6, 'Font Manager'),
(156, 1, 'Icon Manager'),
(156, 2, 'Icon Manager'),
(156, 3, 'Icon Manager'),
(156, 4, 'Icon Manager'),
(156, 5, 'Icon Manager'),
(156, 6, 'Icon Manager'),
(157, 1, 'Settings'),
(157, 2, 'Settings'),
(157, 3, 'Settings'),
(157, 4, 'Settings'),
(157, 5, 'Settings'),
(157, 6, 'Settings'),
(158, 1, 'Extend Third Party Modules'),
(158, 2, 'Extend Third Party Modules'),
(158, 3, 'Extend Third Party Modules'),
(158, 4, 'Extend Third Party Modules'),
(158, 5, 'Extend Third Party Modules'),
(158, 6, 'Extend Third Party Modules'),
(159, 1, 'AdminCrazyFrontendEditor'),
(159, 2, 'AdminCrazyFrontendEditor'),
(159, 3, 'AdminCrazyFrontendEditor'),
(159, 4, 'AdminCrazyFrontendEditor'),
(159, 5, 'AdminCrazyFrontendEditor'),
(159, 6, 'AdminCrazyFrontendEditor'),
(160, 1, 'AdminCrazyAjaxUrl'),
(160, 2, 'AdminCrazyAjaxUrl'),
(160, 3, 'AdminCrazyAjaxUrl'),
(160, 4, 'AdminCrazyAjaxUrl'),
(160, 5, 'AdminCrazyAjaxUrl'),
(160, 6, 'AdminCrazyAjaxUrl'),
(161, 1, 'Image Type'),
(161, 2, 'Image Type'),
(161, 3, 'Image Type'),
(161, 4, 'Image Type'),
(161, 5, 'Image Type'),
(161, 6, 'Image Type'),
(162, 1, 'Content Any Where'),
(162, 2, 'Content Any Where'),
(162, 3, 'Content Any Where'),
(162, 4, 'Content Any Where'),
(162, 5, 'Content Any Where'),
(162, 6, 'Content Any Where'),
(163, 1, 'Pages (cms)'),
(163, 2, 'Pages (cms)'),
(163, 3, 'Pages (cms)'),
(163, 4, 'Pages (cms)'),
(163, 5, 'Pages (cms)'),
(163, 6, 'Pages (cms)'),
(164, 1, 'Products Description (Pro)'),
(164, 2, 'Products Description (Pro)'),
(164, 3, 'Products Description (Pro)'),
(164, 4, 'Products Description (Pro)'),
(164, 5, 'Products Description (Pro)'),
(164, 6, 'Products Description (Pro)'),
(165, 1, 'Categories Page (Pro)'),
(165, 2, 'Categories Page (Pro)'),
(165, 3, 'Categories Page (Pro)'),
(165, 4, 'Categories Page (Pro)'),
(165, 5, 'Categories Page (Pro)'),
(165, 6, 'Categories Page (Pro)'),
(166, 1, 'Suppliers Page'),
(166, 2, 'Suppliers Page'),
(166, 3, 'Suppliers Page'),
(166, 4, 'Suppliers Page'),
(166, 5, 'Suppliers Page'),
(166, 6, 'Suppliers Page'),
(167, 1, 'Brands Page'),
(167, 2, 'Brands Page'),
(167, 3, 'Brands Page'),
(167, 4, 'Brands Page'),
(167, 5, 'Brands Page'),
(167, 6, 'Brands Page'),
(168, 1, 'AdminCrazyPrdlayouts'),
(168, 2, 'AdminCrazyPrdlayouts'),
(168, 3, 'AdminCrazyPrdlayouts'),
(168, 4, 'AdminCrazyPrdlayouts'),
(168, 5, 'AdminCrazyPrdlayouts'),
(168, 6, 'AdminCrazyPrdlayouts'),
(169, 1, 'Smartsupp'),
(169, 2, 'Smartsupp'),
(169, 3, 'Smartsupp'),
(169, 4, 'Smartsupp'),
(169, 5, 'Smartsupp'),
(169, 6, 'Smartsupp');

-- --------------------------------------------------------

--
-- Structure de la table `ps_tab_module_preference`
--

DROP TABLE IF EXISTS `ps_tab_module_preference`;
CREATE TABLE IF NOT EXISTS `ps_tab_module_preference` (
  `id_tab_module_preference` int(11) NOT NULL AUTO_INCREMENT,
  `id_employee` int(11) NOT NULL,
  `id_tab` int(11) NOT NULL,
  `module` varchar(191) NOT NULL,
  PRIMARY KEY (`id_tab_module_preference`),
  UNIQUE KEY `employee_module` (`id_employee`,`id_tab`,`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_tag`
--

DROP TABLE IF EXISTS `ps_tag`;
CREATE TABLE IF NOT EXISTS `ps_tag` (
  `id_tag` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_tag`),
  KEY `tag_name` (`name`),
  KEY `id_lang` (`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_tag_count`
--

DROP TABLE IF EXISTS `ps_tag_count`;
CREATE TABLE IF NOT EXISTS `ps_tag_count` (
  `id_group` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_tag` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_lang` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `counter` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_group`,`id_tag`),
  KEY `id_group` (`id_group`,`id_lang`,`id_shop`,`counter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_tax`
--

DROP TABLE IF EXISTS `ps_tax`;
CREATE TABLE IF NOT EXISTS `ps_tax` (
  `id_tax` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rate` decimal(10,3) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_tax`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_tax`
--

INSERT INTO `ps_tax` (`id_tax`, `rate`, `active`, `deleted`) VALUES
(1, '21.000', 1, 0),
(2, '12.000', 1, 0),
(3, '6.000', 1, 0),
(4, '20.000', 1, 0),
(5, '20.000', 1, 0),
(6, '19.000', 1, 0),
(7, '21.000', 1, 0),
(8, '19.000', 1, 0),
(9, '25.000', 1, 0),
(10, '20.000', 1, 0),
(11, '21.000', 1, 0),
(12, '24.000', 1, 0),
(13, '20.000', 1, 0),
(14, '20.000', 1, 0),
(15, '24.000', 1, 0),
(16, '25.000', 1, 0),
(17, '27.000', 1, 0),
(18, '23.000', 1, 0),
(19, '22.000', 1, 0),
(20, '21.000', 1, 0),
(21, '17.000', 1, 0),
(22, '21.000', 1, 0),
(23, '18.000', 1, 0),
(24, '21.000', 1, 0),
(25, '23.000', 1, 0),
(26, '23.000', 1, 0),
(27, '19.000', 1, 0),
(28, '25.000', 1, 0),
(29, '22.000', 1, 0),
(30, '20.000', 1, 0),
(31, '10.000', 1, 0),
(32, '5.000', 1, 0),
(33, '4.000', 1, 0),
(34, '0.000', 1, 0),
(35, '20.000', 1, 0),
(36, '10.000', 1, 0),
(37, '4.000', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ps_tax_lang`
--

DROP TABLE IF EXISTS `ps_tax_lang`;
CREATE TABLE IF NOT EXISTS `ps_tax_lang` (
  `id_tax` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_tax`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_tax_lang`
--

INSERT INTO `ps_tax_lang` (`id_tax`, `id_lang`, `name`) VALUES
(1, 1, 'TVA BE 21%'),
(1, 2, 'TVA BE 21%'),
(1, 3, 'TVA BE 21%'),
(1, 4, 'TVA BE 21%'),
(1, 5, 'TVA BE 21%'),
(1, 6, 'TVA BE 21%'),
(2, 1, 'TVA BE 12%'),
(2, 2, 'TVA BE 12%'),
(2, 3, 'TVA BE 12%'),
(2, 4, 'TVA BE 12%'),
(2, 5, 'TVA BE 12%'),
(2, 6, 'TVA BE 12%'),
(3, 1, 'TVA BE 6%'),
(3, 2, 'TVA BE 6%'),
(3, 3, 'TVA BE 6%'),
(3, 4, 'TVA BE 6%'),
(3, 5, 'TVA BE 6%'),
(3, 6, 'TVA BE 6%'),
(4, 1, 'USt. AT 20%'),
(4, 2, 'USt. AT 20%'),
(4, 3, 'USt. AT 20%'),
(4, 4, 'USt. AT 20%'),
(4, 5, 'USt. AT 20%'),
(4, 6, 'USt. AT 20%'),
(5, 1, 'ДДС BG 20%'),
(5, 2, 'ДДС BG 20%'),
(5, 3, 'ДДС BG 20%'),
(5, 4, 'ДДС BG 20%'),
(5, 5, 'ДДС BG 20%'),
(5, 6, 'ДДС BG 20%'),
(6, 1, 'ΦΠΑ CY 19%'),
(6, 2, 'ΦΠΑ CY 19%'),
(6, 3, 'ΦΠΑ CY 19%'),
(6, 4, 'ΦΠΑ CY 19%'),
(6, 5, 'ΦΠΑ CY 19%'),
(6, 6, 'ΦΠΑ CY 19%'),
(7, 1, 'DPH CZ 21%'),
(7, 2, 'DPH CZ 21%'),
(7, 3, 'DPH CZ 21%'),
(7, 4, 'DPH CZ 21%'),
(7, 5, 'DPH CZ 21%'),
(7, 6, 'DPH CZ 21%'),
(8, 1, 'MwSt. DE 19%'),
(8, 2, 'MwSt. DE 19%'),
(8, 3, 'MwSt. DE 19%'),
(8, 4, 'MwSt. DE 19%'),
(8, 5, 'MwSt. DE 19%'),
(8, 6, 'MwSt. DE 19%'),
(9, 1, 'moms DK 25%'),
(9, 2, 'moms DK 25%'),
(9, 3, 'moms DK 25%'),
(9, 4, 'moms DK 25%'),
(9, 5, 'moms DK 25%'),
(9, 6, 'moms DK 25%'),
(10, 1, 'km EE 20%'),
(10, 2, 'km EE 20%'),
(10, 3, 'km EE 20%'),
(10, 4, 'km EE 20%'),
(10, 5, 'km EE 20%'),
(10, 6, 'km EE 20%'),
(11, 1, 'IVA ES 21%'),
(11, 2, 'IVA ES 21%'),
(11, 3, 'IVA ES 21%'),
(11, 4, 'IVA ES 21%'),
(11, 5, 'IVA ES 21%'),
(11, 6, 'IVA ES 21%'),
(12, 1, 'ALV FI 24%'),
(12, 2, 'ALV FI 24%'),
(12, 3, 'ALV FI 24%'),
(12, 4, 'ALV FI 24%'),
(12, 5, 'ALV FI 24%'),
(12, 6, 'ALV FI 24%'),
(13, 1, 'TVA FR 20%'),
(13, 2, 'TVA FR 20%'),
(13, 3, 'TVA FR 20%'),
(13, 4, 'TVA FR 20%'),
(13, 5, 'TVA FR 20%'),
(13, 6, 'TVA FR 20%'),
(14, 1, 'VAT UK 20%'),
(14, 2, 'VAT UK 20%'),
(14, 3, 'VAT UK 20%'),
(14, 4, 'VAT UK 20%'),
(14, 5, 'VAT UK 20%'),
(14, 6, 'VAT UK 20%'),
(15, 1, 'ΦΠΑ GR 24%'),
(15, 2, 'ΦΠΑ GR 24%'),
(15, 3, 'ΦΠΑ GR 24%'),
(15, 4, 'ΦΠΑ GR 24%'),
(15, 5, 'ΦΠΑ GR 24%'),
(15, 6, 'ΦΠΑ GR 24%'),
(16, 1, 'Croatia PDV 25%'),
(16, 2, 'Croatia PDV 25%'),
(16, 3, 'Croatia PDV 25%'),
(16, 4, 'Croatia PDV 25%'),
(16, 5, 'Croatia PDV 25%'),
(16, 6, 'Croatia PDV 25%'),
(17, 1, 'ÁFA HU 27%'),
(17, 2, 'ÁFA HU 27%'),
(17, 3, 'ÁFA HU 27%'),
(17, 4, 'ÁFA HU 27%'),
(17, 5, 'ÁFA HU 27%'),
(17, 6, 'ÁFA HU 27%'),
(18, 1, 'VAT IE 23%'),
(18, 2, 'VAT IE 23%'),
(18, 3, 'VAT IE 23%'),
(18, 4, 'VAT IE 23%'),
(18, 5, 'VAT IE 23%'),
(18, 6, 'VAT IE 23%'),
(19, 1, 'IVA IT 22%'),
(19, 2, 'IVA IT 22%'),
(19, 3, 'IVA IT 22%'),
(19, 4, 'IVA IT 22%'),
(19, 5, 'IVA IT 22%'),
(19, 6, 'IVA IT 22%'),
(20, 1, 'PVM LT 21%'),
(20, 2, 'PVM LT 21%'),
(20, 3, 'PVM LT 21%'),
(20, 4, 'PVM LT 21%'),
(20, 5, 'PVM LT 21%'),
(20, 6, 'PVM LT 21%'),
(21, 1, 'TVA LU 17%'),
(21, 2, 'TVA LU 17%'),
(21, 3, 'TVA LU 17%'),
(21, 4, 'TVA LU 17%'),
(21, 5, 'TVA LU 17%'),
(21, 6, 'TVA LU 17%'),
(22, 1, 'PVN LV 21%'),
(22, 2, 'PVN LV 21%'),
(22, 3, 'PVN LV 21%'),
(22, 4, 'PVN LV 21%'),
(22, 5, 'PVN LV 21%'),
(22, 6, 'PVN LV 21%'),
(23, 1, 'VAT MT 18%'),
(23, 2, 'VAT MT 18%'),
(23, 3, 'VAT MT 18%'),
(23, 4, 'VAT MT 18%'),
(23, 5, 'VAT MT 18%'),
(23, 6, 'VAT MT 18%'),
(24, 1, 'BTW NL 21%'),
(24, 2, 'BTW NL 21%'),
(24, 3, 'BTW NL 21%'),
(24, 4, 'BTW NL 21%'),
(24, 5, 'BTW NL 21%'),
(24, 6, 'BTW NL 21%'),
(25, 1, 'PTU PL 23%'),
(25, 2, 'PTU PL 23%'),
(25, 3, 'PTU PL 23%'),
(25, 4, 'PTU PL 23%'),
(25, 5, 'PTU PL 23%'),
(25, 6, 'PTU PL 23%'),
(26, 1, 'IVA PT 23%'),
(26, 2, 'IVA PT 23%'),
(26, 3, 'IVA PT 23%'),
(26, 4, 'IVA PT 23%'),
(26, 5, 'IVA PT 23%'),
(26, 6, 'IVA PT 23%'),
(27, 1, 'TVA RO 19%'),
(27, 2, 'TVA RO 19%'),
(27, 3, 'TVA RO 19%'),
(27, 4, 'TVA RO 19%'),
(27, 5, 'TVA RO 19%'),
(27, 6, 'TVA RO 19%'),
(28, 1, 'Moms SE 25%'),
(28, 2, 'Moms SE 25%'),
(28, 3, 'Moms SE 25%'),
(28, 4, 'Moms SE 25%'),
(28, 5, 'Moms SE 25%'),
(28, 6, 'Moms SE 25%'),
(29, 1, 'DDV SI 22%'),
(29, 2, 'DDV SI 22%'),
(29, 3, 'DDV SI 22%'),
(29, 4, 'DDV SI 22%'),
(29, 5, 'DDV SI 22%'),
(29, 6, 'DDV SI 22%'),
(30, 1, 'DPH SK 20%'),
(30, 2, 'DPH SK 20%'),
(30, 3, 'DPH SK 20%'),
(30, 4, 'DPH SK 20%'),
(30, 5, 'DPH SK 20%'),
(30, 6, 'DPH SK 20%'),
(31, 1, 'IVA IT 10%'),
(31, 2, 'IVA IT 10%'),
(31, 3, 'IVA IT 10%'),
(31, 4, 'IVA IT 10%'),
(31, 5, 'IVA IT 10%'),
(31, 6, 'IVA IT 10%'),
(32, 1, 'IVA IT 5%'),
(32, 2, 'IVA IT 5%'),
(32, 3, 'IVA IT 5%'),
(32, 4, 'IVA IT 5%'),
(32, 5, 'IVA IT 5%'),
(32, 6, 'IVA IT 5%'),
(33, 1, 'IVA IT 4%'),
(33, 2, 'IVA IT 4%'),
(33, 3, 'IVA IT 4%'),
(33, 4, 'IVA IT 4%'),
(33, 5, 'IVA IT 4%'),
(33, 6, 'IVA IT 4%'),
(34, 1, 'IVA IT 0%'),
(34, 2, 'IVA IT 0%'),
(34, 3, 'IVA IT 0%'),
(34, 4, 'IVA IT 0%'),
(34, 5, 'IVA IT 0%'),
(34, 6, 'IVA IT 0%'),
(35, 1, 'TVA MC 20%'),
(35, 2, 'TVA MC 20%'),
(35, 3, 'TVA MC 20%'),
(35, 4, 'TVA MC 20%'),
(35, 5, 'TVA MC 20%'),
(35, 6, 'TVA MC 20%'),
(36, 1, 'IVA ES 10%'),
(36, 2, 'IVA ES 10%'),
(36, 3, 'IVA ES 10%'),
(36, 4, 'IVA ES 10%'),
(36, 5, 'IVA ES 10%'),
(36, 6, 'IVA ES 10%'),
(37, 1, 'IVA ES 4%'),
(37, 2, 'IVA ES 4%'),
(37, 3, 'IVA ES 4%'),
(37, 4, 'IVA ES 4%'),
(37, 5, 'IVA ES 4%'),
(37, 6, 'IVA ES 4%');

-- --------------------------------------------------------

--
-- Structure de la table `ps_tax_rule`
--

DROP TABLE IF EXISTS `ps_tax_rule`;
CREATE TABLE IF NOT EXISTS `ps_tax_rule` (
  `id_tax_rule` int(11) NOT NULL AUTO_INCREMENT,
  `id_tax_rules_group` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `zipcode_from` varchar(12) NOT NULL,
  `zipcode_to` varchar(12) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `behavior` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id_tax_rule`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `id_tax` (`id_tax`),
  KEY `category_getproducts` (`id_tax_rules_group`,`id_country`,`id_state`,`zipcode_from`)
) ENGINE=InnoDB AUTO_INCREMENT=459 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_tax_rule`
--

INSERT INTO `ps_tax_rule` (`id_tax_rule`, `id_tax_rules_group`, `id_country`, `id_state`, `zipcode_from`, `zipcode_to`, `id_tax`, `behavior`, `description`) VALUES
(1, 1, 3, 0, '0', '0', 1, 0, ''),
(2, 1, 233, 0, '0', '0', 1, 0, ''),
(3, 1, 16, 0, '0', '0', 1, 0, ''),
(4, 1, 20, 0, '0', '0', 1, 0, ''),
(5, 1, 1, 0, '0', '0', 1, 0, ''),
(6, 1, 86, 0, '0', '0', 1, 0, ''),
(7, 1, 9, 0, '0', '0', 1, 0, ''),
(8, 1, 74, 0, '0', '0', 1, 0, ''),
(9, 1, 6, 0, '0', '0', 1, 0, ''),
(10, 1, 8, 0, '0', '0', 1, 0, ''),
(11, 1, 26, 0, '0', '0', 1, 0, ''),
(12, 1, 10, 0, '0', '0', 1, 0, ''),
(13, 1, 76, 0, '0', '0', 1, 0, ''),
(14, 1, 124, 0, '0', '0', 1, 0, ''),
(15, 1, 130, 0, '0', '0', 1, 0, ''),
(16, 1, 12, 0, '0', '0', 1, 0, ''),
(17, 1, 142, 0, '0', '0', 1, 0, ''),
(18, 1, 138, 0, '0', '0', 1, 0, ''),
(19, 1, 13, 0, '0', '0', 1, 0, ''),
(20, 1, 2, 0, '0', '0', 1, 0, ''),
(21, 1, 14, 0, '0', '0', 1, 0, ''),
(22, 1, 15, 0, '0', '0', 1, 0, ''),
(23, 1, 36, 0, '0', '0', 1, 0, ''),
(24, 1, 191, 0, '0', '0', 1, 0, ''),
(25, 1, 37, 0, '0', '0', 1, 0, ''),
(26, 1, 7, 0, '0', '0', 1, 0, ''),
(27, 1, 18, 0, '0', '0', 1, 0, ''),
(28, 1, 17, 0, '0', '0', 1, 0, ''),
(29, 2, 3, 0, '0', '0', 2, 0, ''),
(30, 2, 233, 0, '0', '0', 2, 0, ''),
(31, 2, 16, 0, '0', '0', 2, 0, ''),
(32, 2, 20, 0, '0', '0', 2, 0, ''),
(33, 2, 1, 0, '0', '0', 2, 0, ''),
(34, 2, 86, 0, '0', '0', 2, 0, ''),
(35, 2, 9, 0, '0', '0', 2, 0, ''),
(36, 2, 74, 0, '0', '0', 2, 0, ''),
(37, 2, 6, 0, '0', '0', 2, 0, ''),
(38, 2, 8, 0, '0', '0', 2, 0, ''),
(39, 2, 26, 0, '0', '0', 2, 0, ''),
(40, 2, 10, 0, '0', '0', 2, 0, ''),
(41, 2, 76, 0, '0', '0', 2, 0, ''),
(42, 2, 124, 0, '0', '0', 2, 0, ''),
(43, 2, 130, 0, '0', '0', 2, 0, ''),
(44, 2, 12, 0, '0', '0', 2, 0, ''),
(45, 2, 142, 0, '0', '0', 2, 0, ''),
(46, 2, 138, 0, '0', '0', 2, 0, ''),
(47, 2, 13, 0, '0', '0', 2, 0, ''),
(48, 2, 2, 0, '0', '0', 2, 0, ''),
(49, 2, 14, 0, '0', '0', 2, 0, ''),
(50, 2, 15, 0, '0', '0', 2, 0, ''),
(51, 2, 36, 0, '0', '0', 2, 0, ''),
(52, 2, 191, 0, '0', '0', 2, 0, ''),
(53, 2, 37, 0, '0', '0', 2, 0, ''),
(54, 2, 7, 0, '0', '0', 2, 0, ''),
(55, 2, 18, 0, '0', '0', 2, 0, ''),
(56, 2, 17, 0, '0', '0', 2, 0, ''),
(57, 3, 3, 0, '0', '0', 3, 0, ''),
(58, 3, 233, 0, '0', '0', 3, 0, ''),
(59, 3, 16, 0, '0', '0', 3, 0, ''),
(60, 3, 20, 0, '0', '0', 3, 0, ''),
(61, 3, 1, 0, '0', '0', 3, 0, ''),
(62, 3, 86, 0, '0', '0', 3, 0, ''),
(63, 3, 9, 0, '0', '0', 3, 0, ''),
(64, 3, 74, 0, '0', '0', 3, 0, ''),
(65, 3, 6, 0, '0', '0', 3, 0, ''),
(66, 3, 8, 0, '0', '0', 3, 0, ''),
(67, 3, 26, 0, '0', '0', 3, 0, ''),
(68, 3, 10, 0, '0', '0', 3, 0, ''),
(69, 3, 76, 0, '0', '0', 3, 0, ''),
(70, 3, 124, 0, '0', '0', 3, 0, ''),
(71, 3, 130, 0, '0', '0', 3, 0, ''),
(72, 3, 12, 0, '0', '0', 3, 0, ''),
(73, 3, 142, 0, '0', '0', 3, 0, ''),
(74, 3, 138, 0, '0', '0', 3, 0, ''),
(75, 3, 13, 0, '0', '0', 3, 0, ''),
(76, 3, 2, 0, '0', '0', 3, 0, ''),
(77, 3, 14, 0, '0', '0', 3, 0, ''),
(78, 3, 15, 0, '0', '0', 3, 0, ''),
(79, 3, 36, 0, '0', '0', 3, 0, ''),
(80, 3, 191, 0, '0', '0', 3, 0, ''),
(81, 3, 37, 0, '0', '0', 3, 0, ''),
(82, 3, 7, 0, '0', '0', 3, 0, ''),
(83, 3, 18, 0, '0', '0', 3, 0, ''),
(84, 3, 17, 0, '0', '0', 3, 0, ''),
(85, 4, 3, 0, '0', '0', 3, 0, ''),
(86, 4, 233, 0, '0', '0', 3, 0, ''),
(87, 4, 16, 0, '0', '0', 3, 0, ''),
(88, 4, 20, 0, '0', '0', 3, 0, ''),
(89, 4, 1, 0, '0', '0', 3, 0, ''),
(90, 4, 86, 0, '0', '0', 3, 0, ''),
(91, 4, 9, 0, '0', '0', 3, 0, ''),
(92, 4, 74, 0, '0', '0', 3, 0, ''),
(93, 4, 6, 0, '0', '0', 3, 0, ''),
(94, 4, 8, 0, '0', '0', 3, 0, ''),
(95, 4, 26, 0, '0', '0', 3, 0, ''),
(96, 4, 10, 0, '0', '0', 3, 0, ''),
(97, 4, 76, 0, '0', '0', 3, 0, ''),
(98, 4, 124, 0, '0', '0', 3, 0, ''),
(99, 4, 130, 0, '0', '0', 3, 0, ''),
(100, 4, 12, 0, '0', '0', 3, 0, ''),
(101, 4, 142, 0, '0', '0', 3, 0, ''),
(102, 4, 138, 0, '0', '0', 3, 0, ''),
(103, 4, 13, 0, '0', '0', 3, 0, ''),
(104, 4, 2, 0, '0', '0', 3, 0, ''),
(105, 4, 14, 0, '0', '0', 3, 0, ''),
(106, 4, 15, 0, '0', '0', 3, 0, ''),
(107, 4, 36, 0, '0', '0', 3, 0, ''),
(108, 4, 191, 0, '0', '0', 3, 0, ''),
(109, 4, 37, 0, '0', '0', 3, 0, ''),
(110, 4, 7, 0, '0', '0', 3, 0, ''),
(111, 4, 18, 0, '0', '0', 3, 0, ''),
(112, 4, 17, 0, '0', '0', 3, 0, ''),
(113, 5, 3, 0, '0', '0', 3, 0, ''),
(114, 5, 233, 0, '0', '0', 3, 0, ''),
(115, 5, 16, 0, '0', '0', 3, 0, ''),
(116, 5, 20, 0, '0', '0', 3, 0, ''),
(117, 5, 1, 0, '0', '0', 3, 0, ''),
(118, 5, 86, 0, '0', '0', 3, 0, ''),
(119, 5, 9, 0, '0', '0', 3, 0, ''),
(120, 5, 74, 0, '0', '0', 3, 0, ''),
(121, 5, 6, 0, '0', '0', 3, 0, ''),
(122, 5, 8, 0, '0', '0', 3, 0, ''),
(123, 5, 26, 0, '0', '0', 3, 0, ''),
(124, 5, 10, 0, '0', '0', 3, 0, ''),
(125, 5, 76, 0, '0', '0', 3, 0, ''),
(126, 5, 124, 0, '0', '0', 3, 0, ''),
(127, 5, 130, 0, '0', '0', 3, 0, ''),
(128, 5, 12, 0, '0', '0', 3, 0, ''),
(129, 5, 142, 0, '0', '0', 3, 0, ''),
(130, 5, 138, 0, '0', '0', 3, 0, ''),
(131, 5, 13, 0, '0', '0', 3, 0, ''),
(132, 5, 2, 0, '0', '0', 3, 0, ''),
(133, 5, 14, 0, '0', '0', 3, 0, ''),
(134, 5, 15, 0, '0', '0', 3, 0, ''),
(135, 5, 36, 0, '0', '0', 3, 0, ''),
(136, 5, 191, 0, '0', '0', 3, 0, ''),
(137, 5, 37, 0, '0', '0', 3, 0, ''),
(138, 5, 7, 0, '0', '0', 3, 0, ''),
(139, 5, 18, 0, '0', '0', 3, 0, ''),
(140, 5, 17, 0, '0', '0', 3, 0, ''),
(141, 6, 3, 0, '0', '0', 1, 0, ''),
(142, 6, 2, 0, '0', '0', 4, 0, ''),
(143, 6, 233, 0, '0', '0', 5, 0, ''),
(144, 6, 76, 0, '0', '0', 6, 0, ''),
(145, 6, 16, 0, '0', '0', 7, 0, ''),
(146, 6, 1, 0, '0', '0', 8, 0, ''),
(147, 6, 20, 0, '0', '0', 9, 0, ''),
(148, 6, 86, 0, '0', '0', 10, 0, ''),
(149, 6, 6, 0, '0', '0', 11, 0, ''),
(150, 6, 7, 0, '0', '0', 12, 0, ''),
(151, 6, 8, 0, '0', '0', 13, 0, ''),
(152, 6, 17, 0, '0', '0', 14, 0, ''),
(153, 6, 9, 0, '0', '0', 15, 0, ''),
(154, 6, 74, 0, '0', '0', 16, 0, ''),
(155, 6, 142, 0, '0', '0', 17, 0, ''),
(156, 6, 26, 0, '0', '0', 18, 0, ''),
(157, 6, 10, 0, '0', '0', 19, 0, ''),
(158, 6, 130, 0, '0', '0', 20, 0, ''),
(159, 6, 12, 0, '0', '0', 21, 0, ''),
(160, 6, 124, 0, '0', '0', 22, 0, ''),
(161, 6, 138, 0, '0', '0', 23, 0, ''),
(162, 6, 13, 0, '0', '0', 24, 0, ''),
(163, 6, 14, 0, '0', '0', 25, 0, ''),
(164, 6, 15, 0, '0', '0', 26, 0, ''),
(165, 6, 36, 0, '0', '0', 27, 0, ''),
(166, 6, 18, 0, '0', '0', 28, 0, ''),
(167, 6, 191, 0, '0', '0', 29, 0, ''),
(168, 6, 37, 0, '0', '0', 30, 0, ''),
(169, 7, 3, 0, '0', '0', 19, 0, ''),
(170, 7, 233, 0, '0', '0', 19, 0, ''),
(171, 7, 16, 0, '0', '0', 19, 0, ''),
(172, 7, 20, 0, '0', '0', 19, 0, ''),
(173, 7, 1, 0, '0', '0', 19, 0, ''),
(174, 7, 86, 0, '0', '0', 19, 0, ''),
(175, 7, 9, 0, '0', '0', 19, 0, ''),
(176, 7, 74, 0, '0', '0', 19, 0, ''),
(177, 7, 6, 0, '0', '0', 19, 0, ''),
(178, 7, 8, 0, '0', '0', 19, 0, ''),
(179, 7, 147, 0, '0', '0', 19, 0, ''),
(180, 7, 26, 0, '0', '0', 19, 0, ''),
(181, 7, 10, 0, '0', '0', 19, 0, ''),
(182, 7, 76, 0, '0', '0', 19, 0, ''),
(183, 7, 124, 0, '0', '0', 19, 0, ''),
(184, 7, 130, 0, '0', '0', 19, 0, ''),
(185, 7, 12, 0, '0', '0', 19, 0, ''),
(186, 7, 142, 0, '0', '0', 19, 0, ''),
(187, 7, 138, 0, '0', '0', 19, 0, ''),
(188, 7, 13, 0, '0', '0', 19, 0, ''),
(189, 7, 2, 0, '0', '0', 19, 0, ''),
(190, 7, 14, 0, '0', '0', 19, 0, ''),
(191, 7, 15, 0, '0', '0', 19, 0, ''),
(192, 7, 36, 0, '0', '0', 19, 0, ''),
(193, 7, 191, 0, '0', '0', 19, 0, ''),
(194, 7, 37, 0, '0', '0', 19, 0, ''),
(195, 7, 7, 0, '0', '0', 19, 0, ''),
(196, 7, 18, 0, '0', '0', 19, 0, ''),
(197, 7, 17, 0, '0', '0', 19, 0, ''),
(198, 8, 3, 0, '0', '0', 31, 0, ''),
(199, 8, 233, 0, '0', '0', 31, 0, ''),
(200, 8, 16, 0, '0', '0', 31, 0, ''),
(201, 8, 20, 0, '0', '0', 31, 0, ''),
(202, 8, 1, 0, '0', '0', 31, 0, ''),
(203, 8, 86, 0, '0', '0', 31, 0, ''),
(204, 8, 9, 0, '0', '0', 31, 0, ''),
(205, 8, 74, 0, '0', '0', 31, 0, ''),
(206, 8, 6, 0, '0', '0', 31, 0, ''),
(207, 8, 8, 0, '0', '0', 31, 0, ''),
(208, 8, 147, 0, '0', '0', 31, 0, ''),
(209, 8, 26, 0, '0', '0', 31, 0, ''),
(210, 8, 10, 0, '0', '0', 31, 0, ''),
(211, 8, 76, 0, '0', '0', 31, 0, ''),
(212, 8, 124, 0, '0', '0', 31, 0, ''),
(213, 8, 130, 0, '0', '0', 31, 0, ''),
(214, 8, 12, 0, '0', '0', 31, 0, ''),
(215, 8, 142, 0, '0', '0', 31, 0, ''),
(216, 8, 138, 0, '0', '0', 31, 0, ''),
(217, 8, 13, 0, '0', '0', 31, 0, ''),
(218, 8, 2, 0, '0', '0', 31, 0, ''),
(219, 8, 14, 0, '0', '0', 31, 0, ''),
(220, 8, 15, 0, '0', '0', 31, 0, ''),
(221, 8, 36, 0, '0', '0', 31, 0, ''),
(222, 8, 191, 0, '0', '0', 31, 0, ''),
(223, 8, 37, 0, '0', '0', 31, 0, ''),
(224, 8, 7, 0, '0', '0', 31, 0, ''),
(225, 8, 18, 0, '0', '0', 31, 0, ''),
(226, 8, 17, 0, '0', '0', 31, 0, ''),
(227, 9, 3, 0, '0', '0', 32, 0, ''),
(228, 9, 233, 0, '0', '0', 32, 0, ''),
(229, 9, 16, 0, '0', '0', 32, 0, ''),
(230, 9, 20, 0, '0', '0', 32, 0, ''),
(231, 9, 1, 0, '0', '0', 32, 0, ''),
(232, 9, 86, 0, '0', '0', 32, 0, ''),
(233, 9, 9, 0, '0', '0', 32, 0, ''),
(234, 9, 74, 0, '0', '0', 32, 0, ''),
(235, 9, 6, 0, '0', '0', 32, 0, ''),
(236, 9, 8, 0, '0', '0', 32, 0, ''),
(237, 9, 147, 0, '0', '0', 32, 0, ''),
(238, 9, 26, 0, '0', '0', 32, 0, ''),
(239, 9, 10, 0, '0', '0', 32, 0, ''),
(240, 9, 76, 0, '0', '0', 32, 0, ''),
(241, 9, 124, 0, '0', '0', 32, 0, ''),
(242, 9, 130, 0, '0', '0', 32, 0, ''),
(243, 9, 12, 0, '0', '0', 32, 0, ''),
(244, 9, 142, 0, '0', '0', 32, 0, ''),
(245, 9, 138, 0, '0', '0', 32, 0, ''),
(246, 9, 13, 0, '0', '0', 32, 0, ''),
(247, 9, 2, 0, '0', '0', 32, 0, ''),
(248, 9, 14, 0, '0', '0', 32, 0, ''),
(249, 9, 15, 0, '0', '0', 32, 0, ''),
(250, 9, 36, 0, '0', '0', 32, 0, ''),
(251, 9, 191, 0, '0', '0', 32, 0, ''),
(252, 9, 37, 0, '0', '0', 32, 0, ''),
(253, 9, 7, 0, '0', '0', 32, 0, ''),
(254, 9, 18, 0, '0', '0', 32, 0, ''),
(255, 9, 17, 0, '0', '0', 32, 0, ''),
(256, 10, 3, 0, '0', '0', 33, 0, ''),
(257, 10, 233, 0, '0', '0', 33, 0, ''),
(258, 10, 16, 0, '0', '0', 33, 0, ''),
(259, 10, 20, 0, '0', '0', 33, 0, ''),
(260, 10, 1, 0, '0', '0', 33, 0, ''),
(261, 10, 86, 0, '0', '0', 33, 0, ''),
(262, 10, 9, 0, '0', '0', 33, 0, ''),
(263, 10, 74, 0, '0', '0', 33, 0, ''),
(264, 10, 6, 0, '0', '0', 33, 0, ''),
(265, 10, 8, 0, '0', '0', 33, 0, ''),
(266, 10, 147, 0, '0', '0', 33, 0, ''),
(267, 10, 26, 0, '0', '0', 33, 0, ''),
(268, 10, 10, 0, '0', '0', 33, 0, ''),
(269, 10, 76, 0, '0', '0', 33, 0, ''),
(270, 10, 124, 0, '0', '0', 33, 0, ''),
(271, 10, 130, 0, '0', '0', 33, 0, ''),
(272, 10, 12, 0, '0', '0', 33, 0, ''),
(273, 10, 142, 0, '0', '0', 33, 0, ''),
(274, 10, 138, 0, '0', '0', 33, 0, ''),
(275, 10, 13, 0, '0', '0', 33, 0, ''),
(276, 10, 2, 0, '0', '0', 33, 0, ''),
(277, 10, 14, 0, '0', '0', 33, 0, ''),
(278, 10, 15, 0, '0', '0', 33, 0, ''),
(279, 10, 36, 0, '0', '0', 33, 0, ''),
(280, 10, 191, 0, '0', '0', 33, 0, ''),
(281, 10, 37, 0, '0', '0', 33, 0, ''),
(282, 10, 7, 0, '0', '0', 33, 0, ''),
(283, 10, 18, 0, '0', '0', 33, 0, ''),
(284, 10, 17, 0, '0', '0', 33, 0, ''),
(285, 11, 3, 0, '0', '0', 34, 0, ''),
(286, 11, 233, 0, '0', '0', 34, 0, ''),
(287, 11, 16, 0, '0', '0', 34, 0, ''),
(288, 11, 20, 0, '0', '0', 34, 0, ''),
(289, 11, 1, 0, '0', '0', 34, 0, ''),
(290, 11, 86, 0, '0', '0', 34, 0, ''),
(291, 11, 9, 0, '0', '0', 34, 0, ''),
(292, 11, 74, 0, '0', '0', 34, 0, ''),
(293, 11, 6, 0, '0', '0', 34, 0, ''),
(294, 11, 8, 0, '0', '0', 34, 0, ''),
(295, 11, 147, 0, '0', '0', 34, 0, ''),
(296, 11, 26, 0, '0', '0', 34, 0, ''),
(297, 11, 10, 0, '0', '0', 34, 0, ''),
(298, 11, 76, 0, '0', '0', 34, 0, ''),
(299, 11, 124, 0, '0', '0', 34, 0, ''),
(300, 11, 130, 0, '0', '0', 34, 0, ''),
(301, 11, 12, 0, '0', '0', 34, 0, ''),
(302, 11, 142, 0, '0', '0', 34, 0, ''),
(303, 11, 138, 0, '0', '0', 34, 0, ''),
(304, 11, 13, 0, '0', '0', 34, 0, ''),
(305, 11, 2, 0, '0', '0', 34, 0, ''),
(306, 11, 14, 0, '0', '0', 34, 0, ''),
(307, 11, 15, 0, '0', '0', 34, 0, ''),
(308, 11, 36, 0, '0', '0', 34, 0, ''),
(309, 11, 191, 0, '0', '0', 34, 0, ''),
(310, 11, 37, 0, '0', '0', 34, 0, ''),
(311, 11, 7, 0, '0', '0', 34, 0, ''),
(312, 11, 18, 0, '0', '0', 34, 0, ''),
(313, 11, 17, 0, '0', '0', 34, 0, ''),
(314, 12, 3, 0, '0', '0', 11, 0, ''),
(315, 12, 233, 0, '0', '0', 11, 0, ''),
(316, 12, 16, 0, '0', '0', 11, 0, ''),
(317, 12, 20, 0, '0', '0', 11, 0, ''),
(318, 12, 1, 0, '0', '0', 11, 0, ''),
(319, 12, 86, 0, '0', '0', 11, 0, ''),
(320, 12, 9, 0, '0', '0', 11, 0, ''),
(321, 12, 74, 0, '0', '0', 11, 0, ''),
(322, 12, 6, 0, '0', '0', 11, 0, ''),
(323, 12, 8, 0, '0', '0', 11, 0, ''),
(324, 12, 147, 0, '0', '0', 11, 0, ''),
(325, 12, 26, 0, '0', '0', 11, 0, ''),
(326, 12, 10, 0, '0', '0', 11, 0, ''),
(327, 12, 76, 0, '0', '0', 11, 0, ''),
(328, 12, 124, 0, '0', '0', 11, 0, ''),
(329, 12, 130, 0, '0', '0', 11, 0, ''),
(330, 12, 12, 0, '0', '0', 11, 0, ''),
(331, 12, 142, 0, '0', '0', 11, 0, ''),
(332, 12, 138, 0, '0', '0', 11, 0, ''),
(333, 12, 13, 0, '0', '0', 11, 0, ''),
(334, 12, 2, 0, '0', '0', 11, 0, ''),
(335, 12, 14, 0, '0', '0', 11, 0, ''),
(336, 12, 15, 0, '0', '0', 11, 0, ''),
(337, 12, 36, 0, '0', '0', 11, 0, ''),
(338, 12, 191, 0, '0', '0', 11, 0, ''),
(339, 12, 37, 0, '0', '0', 11, 0, ''),
(340, 12, 7, 0, '0', '0', 11, 0, ''),
(341, 12, 18, 0, '0', '0', 11, 0, ''),
(342, 12, 17, 0, '0', '0', 11, 0, ''),
(343, 13, 3, 0, '0', '0', 36, 0, ''),
(344, 13, 233, 0, '0', '0', 36, 0, ''),
(345, 13, 16, 0, '0', '0', 36, 0, ''),
(346, 13, 20, 0, '0', '0', 36, 0, ''),
(347, 13, 1, 0, '0', '0', 36, 0, ''),
(348, 13, 86, 0, '0', '0', 36, 0, ''),
(349, 13, 9, 0, '0', '0', 36, 0, ''),
(350, 13, 74, 0, '0', '0', 36, 0, ''),
(351, 13, 6, 0, '0', '0', 36, 0, ''),
(352, 13, 8, 0, '0', '0', 36, 0, ''),
(353, 13, 147, 0, '0', '0', 36, 0, ''),
(354, 13, 26, 0, '0', '0', 36, 0, ''),
(355, 13, 10, 0, '0', '0', 36, 0, ''),
(356, 13, 76, 0, '0', '0', 36, 0, ''),
(357, 13, 124, 0, '0', '0', 36, 0, ''),
(358, 13, 130, 0, '0', '0', 36, 0, ''),
(359, 13, 12, 0, '0', '0', 36, 0, ''),
(360, 13, 142, 0, '0', '0', 36, 0, ''),
(361, 13, 138, 0, '0', '0', 36, 0, ''),
(362, 13, 13, 0, '0', '0', 36, 0, ''),
(363, 13, 2, 0, '0', '0', 36, 0, ''),
(364, 13, 14, 0, '0', '0', 36, 0, ''),
(365, 13, 15, 0, '0', '0', 36, 0, ''),
(366, 13, 36, 0, '0', '0', 36, 0, ''),
(367, 13, 191, 0, '0', '0', 36, 0, ''),
(368, 13, 37, 0, '0', '0', 36, 0, ''),
(369, 13, 7, 0, '0', '0', 36, 0, ''),
(370, 13, 18, 0, '0', '0', 36, 0, ''),
(371, 13, 17, 0, '0', '0', 36, 0, ''),
(372, 14, 3, 0, '0', '0', 37, 0, ''),
(373, 14, 233, 0, '0', '0', 37, 0, ''),
(374, 14, 16, 0, '0', '0', 37, 0, ''),
(375, 14, 20, 0, '0', '0', 37, 0, ''),
(376, 14, 1, 0, '0', '0', 37, 0, ''),
(377, 14, 86, 0, '0', '0', 37, 0, ''),
(378, 14, 9, 0, '0', '0', 37, 0, ''),
(379, 14, 74, 0, '0', '0', 37, 0, ''),
(380, 14, 6, 0, '0', '0', 37, 0, ''),
(381, 14, 8, 0, '0', '0', 37, 0, ''),
(382, 14, 147, 0, '0', '0', 37, 0, ''),
(383, 14, 26, 0, '0', '0', 37, 0, ''),
(384, 14, 10, 0, '0', '0', 37, 0, ''),
(385, 14, 76, 0, '0', '0', 37, 0, ''),
(386, 14, 124, 0, '0', '0', 37, 0, ''),
(387, 14, 130, 0, '0', '0', 37, 0, ''),
(388, 14, 12, 0, '0', '0', 37, 0, ''),
(389, 14, 142, 0, '0', '0', 37, 0, ''),
(390, 14, 138, 0, '0', '0', 37, 0, ''),
(391, 14, 13, 0, '0', '0', 37, 0, ''),
(392, 14, 2, 0, '0', '0', 37, 0, ''),
(393, 14, 14, 0, '0', '0', 37, 0, ''),
(394, 14, 15, 0, '0', '0', 37, 0, ''),
(395, 14, 36, 0, '0', '0', 37, 0, ''),
(396, 14, 191, 0, '0', '0', 37, 0, ''),
(397, 14, 37, 0, '0', '0', 37, 0, ''),
(398, 14, 7, 0, '0', '0', 37, 0, ''),
(399, 14, 18, 0, '0', '0', 37, 0, ''),
(400, 14, 17, 0, '0', '0', 37, 0, ''),
(401, 15, 3, 0, '0', '0', 37, 0, ''),
(402, 15, 233, 0, '0', '0', 37, 0, ''),
(403, 15, 16, 0, '0', '0', 37, 0, ''),
(404, 15, 20, 0, '0', '0', 37, 0, ''),
(405, 15, 1, 0, '0', '0', 37, 0, ''),
(406, 15, 86, 0, '0', '0', 37, 0, ''),
(407, 15, 9, 0, '0', '0', 37, 0, ''),
(408, 15, 74, 0, '0', '0', 37, 0, ''),
(409, 15, 6, 0, '0', '0', 37, 0, ''),
(410, 15, 8, 0, '0', '0', 37, 0, ''),
(411, 15, 147, 0, '0', '0', 37, 0, ''),
(412, 15, 26, 0, '0', '0', 37, 0, ''),
(413, 15, 10, 0, '0', '0', 37, 0, ''),
(414, 15, 76, 0, '0', '0', 37, 0, ''),
(415, 15, 124, 0, '0', '0', 37, 0, ''),
(416, 15, 130, 0, '0', '0', 37, 0, ''),
(417, 15, 12, 0, '0', '0', 37, 0, ''),
(418, 15, 142, 0, '0', '0', 37, 0, ''),
(419, 15, 138, 0, '0', '0', 37, 0, ''),
(420, 15, 13, 0, '0', '0', 37, 0, ''),
(421, 15, 2, 0, '0', '0', 37, 0, ''),
(422, 15, 14, 0, '0', '0', 37, 0, ''),
(423, 15, 15, 0, '0', '0', 37, 0, ''),
(424, 15, 36, 0, '0', '0', 37, 0, ''),
(425, 15, 191, 0, '0', '0', 37, 0, ''),
(426, 15, 37, 0, '0', '0', 37, 0, ''),
(427, 15, 7, 0, '0', '0', 37, 0, ''),
(428, 15, 18, 0, '0', '0', 37, 0, ''),
(429, 15, 17, 0, '0', '0', 37, 0, ''),
(430, 16, 3, 0, '0', '0', 37, 0, ''),
(431, 16, 233, 0, '0', '0', 37, 0, ''),
(432, 16, 16, 0, '0', '0', 37, 0, ''),
(433, 16, 20, 0, '0', '0', 37, 0, ''),
(434, 16, 1, 0, '0', '0', 37, 0, ''),
(435, 16, 86, 0, '0', '0', 37, 0, ''),
(436, 16, 9, 0, '0', '0', 37, 0, ''),
(437, 16, 74, 0, '0', '0', 37, 0, ''),
(438, 16, 6, 0, '0', '0', 37, 0, ''),
(439, 16, 8, 0, '0', '0', 37, 0, ''),
(440, 16, 147, 0, '0', '0', 37, 0, ''),
(441, 16, 26, 0, '0', '0', 37, 0, ''),
(442, 16, 10, 0, '0', '0', 37, 0, ''),
(443, 16, 76, 0, '0', '0', 37, 0, ''),
(444, 16, 124, 0, '0', '0', 37, 0, ''),
(445, 16, 130, 0, '0', '0', 37, 0, ''),
(446, 16, 12, 0, '0', '0', 37, 0, ''),
(447, 16, 142, 0, '0', '0', 37, 0, ''),
(448, 16, 138, 0, '0', '0', 37, 0, ''),
(449, 16, 13, 0, '0', '0', 37, 0, ''),
(450, 16, 2, 0, '0', '0', 37, 0, ''),
(451, 16, 14, 0, '0', '0', 37, 0, ''),
(452, 16, 15, 0, '0', '0', 37, 0, ''),
(453, 16, 36, 0, '0', '0', 37, 0, ''),
(454, 16, 191, 0, '0', '0', 37, 0, ''),
(455, 16, 37, 0, '0', '0', 37, 0, ''),
(456, 16, 7, 0, '0', '0', 37, 0, ''),
(457, 16, 18, 0, '0', '0', 37, 0, ''),
(458, 16, 17, 0, '0', '0', 37, 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `ps_tax_rules_group`
--

DROP TABLE IF EXISTS `ps_tax_rules_group`;
CREATE TABLE IF NOT EXISTS `ps_tax_rules_group` (
  `id_tax_rules_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `active` int(11) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_tax_rules_group`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_tax_rules_group`
--

INSERT INTO `ps_tax_rules_group` (`id_tax_rules_group`, `name`, `active`, `deleted`, `date_add`, `date_upd`) VALUES
(1, 'BE Standard Rate (21%)', 1, 0, '2022-11-21 11:03:36', '2022-11-21 11:03:36'),
(2, 'BE Reduced Rate (12%)', 1, 0, '2022-11-21 11:03:36', '2022-11-21 11:03:36'),
(3, 'BE Super Reduced Rate (6%)', 1, 0, '2022-11-21 11:03:37', '2022-11-21 11:03:37'),
(4, 'BE Foodstuff Rate (6%)', 1, 0, '2022-11-21 11:03:37', '2022-11-21 11:03:37'),
(5, 'BE Books Rate (6%)', 1, 0, '2022-11-21 11:03:37', '2022-11-21 11:03:37'),
(6, 'EU VAT For Virtual Products', 1, 0, '2022-11-21 11:03:37', '2022-11-21 11:03:37'),
(7, 'IT Standard Rate (22%)', 1, 0, '2022-12-05 11:00:03', '2022-12-05 11:00:03'),
(8, 'IT Reduced Rate (10%)', 1, 0, '2022-12-05 11:00:03', '2022-12-05 11:00:03'),
(9, 'IT Reduced Rate (5%)', 1, 0, '2022-12-05 11:00:03', '2022-12-05 11:00:03'),
(10, 'IT Reduced Rate (4%)', 1, 0, '2022-12-05 11:00:03', '2022-12-05 11:00:03'),
(11, 'IT Zero Rate (0%)', 1, 0, '2022-12-05 11:00:03', '2022-12-05 11:00:03'),
(12, 'ES Standard rate (21%)', 1, 0, '2022-12-05 13:24:50', '2022-12-05 13:24:50'),
(13, 'ES Reduced Rate (10%)', 1, 0, '2022-12-05 13:24:50', '2022-12-05 13:24:50'),
(14, 'ES Super Reduced Rate (4%)', 1, 0, '2022-12-05 13:24:50', '2022-12-05 13:24:50'),
(15, 'ES Foodstuff Rate (4%)', 1, 0, '2022-12-05 13:24:50', '2022-12-05 13:24:50'),
(16, 'ES Books Rate (4%)', 1, 0, '2022-12-05 13:24:50', '2022-12-05 13:24:50');

-- --------------------------------------------------------

--
-- Structure de la table `ps_tax_rules_group_shop`
--

DROP TABLE IF EXISTS `ps_tax_rules_group_shop`;
CREATE TABLE IF NOT EXISTS `ps_tax_rules_group_shop` (
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_tax_rules_group`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_tax_rules_group_shop`
--

INSERT INTO `ps_tax_rules_group_shop` (`id_tax_rules_group`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_timezone`
--

DROP TABLE IF EXISTS `ps_timezone`;
CREATE TABLE IF NOT EXISTS `ps_timezone` (
  `id_timezone` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_timezone`)
) ENGINE=InnoDB AUTO_INCREMENT=561 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_timezone`
--

INSERT INTO `ps_timezone` (`id_timezone`, `name`) VALUES
(1, 'Africa/Abidjan'),
(2, 'Africa/Accra'),
(3, 'Africa/Addis_Ababa'),
(4, 'Africa/Algiers'),
(5, 'Africa/Asmara'),
(6, 'Africa/Asmera'),
(7, 'Africa/Bamako'),
(8, 'Africa/Bangui'),
(9, 'Africa/Banjul'),
(10, 'Africa/Bissau'),
(11, 'Africa/Blantyre'),
(12, 'Africa/Brazzaville'),
(13, 'Africa/Bujumbura'),
(14, 'Africa/Cairo'),
(15, 'Africa/Casablanca'),
(16, 'Africa/Ceuta'),
(17, 'Africa/Conakry'),
(18, 'Africa/Dakar'),
(19, 'Africa/Dar_es_Salaam'),
(20, 'Africa/Djibouti'),
(21, 'Africa/Douala'),
(22, 'Africa/El_Aaiun'),
(23, 'Africa/Freetown'),
(24, 'Africa/Gaborone'),
(25, 'Africa/Harare'),
(26, 'Africa/Johannesburg'),
(27, 'Africa/Kampala'),
(28, 'Africa/Khartoum'),
(29, 'Africa/Kigali'),
(30, 'Africa/Kinshasa'),
(31, 'Africa/Lagos'),
(32, 'Africa/Libreville'),
(33, 'Africa/Lome'),
(34, 'Africa/Luanda'),
(35, 'Africa/Lubumbashi'),
(36, 'Africa/Lusaka'),
(37, 'Africa/Malabo'),
(38, 'Africa/Maputo'),
(39, 'Africa/Maseru'),
(40, 'Africa/Mbabane'),
(41, 'Africa/Mogadishu'),
(42, 'Africa/Monrovia'),
(43, 'Africa/Nairobi'),
(44, 'Africa/Ndjamena'),
(45, 'Africa/Niamey'),
(46, 'Africa/Nouakchott'),
(47, 'Africa/Ouagadougou'),
(48, 'Africa/Porto-Novo'),
(49, 'Africa/Sao_Tome'),
(50, 'Africa/Timbuktu'),
(51, 'Africa/Tripoli'),
(52, 'Africa/Tunis'),
(53, 'Africa/Windhoek'),
(54, 'America/Adak'),
(55, 'America/Anchorage '),
(56, 'America/Anguilla'),
(57, 'America/Antigua'),
(58, 'America/Araguaina'),
(59, 'America/Argentina/Buenos_Aires'),
(60, 'America/Argentina/Catamarca'),
(61, 'America/Argentina/ComodRivadavia'),
(62, 'America/Argentina/Cordoba'),
(63, 'America/Argentina/Jujuy'),
(64, 'America/Argentina/La_Rioja'),
(65, 'America/Argentina/Mendoza'),
(66, 'America/Argentina/Rio_Gallegos'),
(67, 'America/Argentina/Salta'),
(68, 'America/Argentina/San_Juan'),
(69, 'America/Argentina/San_Luis'),
(70, 'America/Argentina/Tucuman'),
(71, 'America/Argentina/Ushuaia'),
(72, 'America/Aruba'),
(73, 'America/Asuncion'),
(74, 'America/Atikokan'),
(75, 'America/Atka'),
(76, 'America/Bahia'),
(77, 'America/Barbados'),
(78, 'America/Belem'),
(79, 'America/Belize'),
(80, 'America/Blanc-Sablon'),
(81, 'America/Boa_Vista'),
(82, 'America/Bogota'),
(83, 'America/Boise'),
(84, 'America/Buenos_Aires'),
(85, 'America/Cambridge_Bay'),
(86, 'America/Campo_Grande'),
(87, 'America/Cancun'),
(88, 'America/Caracas'),
(89, 'America/Catamarca'),
(90, 'America/Cayenne'),
(91, 'America/Cayman'),
(92, 'America/Chicago'),
(93, 'America/Chihuahua'),
(94, 'America/Coral_Harbour'),
(95, 'America/Cordoba'),
(96, 'America/Costa_Rica'),
(97, 'America/Cuiaba'),
(98, 'America/Curacao'),
(99, 'America/Danmarkshavn'),
(100, 'America/Dawson'),
(101, 'America/Dawson_Creek'),
(102, 'America/Denver'),
(103, 'America/Detroit'),
(104, 'America/Dominica'),
(105, 'America/Edmonton'),
(106, 'America/Eirunepe'),
(107, 'America/El_Salvador'),
(108, 'America/Ensenada'),
(109, 'America/Fort_Wayne'),
(110, 'America/Fortaleza'),
(111, 'America/Glace_Bay'),
(112, 'America/Godthab'),
(113, 'America/Goose_Bay'),
(114, 'America/Grand_Turk'),
(115, 'America/Grenada'),
(116, 'America/Guadeloupe'),
(117, 'America/Guatemala'),
(118, 'America/Guayaquil'),
(119, 'America/Guyana'),
(120, 'America/Halifax'),
(121, 'America/Havana'),
(122, 'America/Hermosillo'),
(123, 'America/Indiana/Indianapolis'),
(124, 'America/Indiana/Knox'),
(125, 'America/Indiana/Marengo'),
(126, 'America/Indiana/Petersburg'),
(127, 'America/Indiana/Tell_City'),
(128, 'America/Indiana/Vevay'),
(129, 'America/Indiana/Vincennes'),
(130, 'America/Indiana/Winamac'),
(131, 'America/Indianapolis'),
(132, 'America/Inuvik'),
(133, 'America/Iqaluit'),
(134, 'America/Jamaica'),
(135, 'America/Jujuy'),
(136, 'America/Juneau'),
(137, 'America/Kentucky/Louisville'),
(138, 'America/Kentucky/Monticello'),
(139, 'America/Knox_IN'),
(140, 'America/La_Paz'),
(141, 'America/Lima'),
(142, 'America/Los_Angeles'),
(143, 'America/Louisville'),
(144, 'America/Maceio'),
(145, 'America/Managua'),
(146, 'America/Manaus'),
(147, 'America/Marigot'),
(148, 'America/Martinique'),
(149, 'America/Mazatlan'),
(150, 'America/Mendoza'),
(151, 'America/Menominee'),
(152, 'America/Merida'),
(153, 'America/Mexico_City'),
(154, 'America/Miquelon'),
(155, 'America/Moncton'),
(156, 'America/Monterrey'),
(157, 'America/Montevideo'),
(158, 'America/Montreal'),
(159, 'America/Montserrat'),
(160, 'America/Nassau'),
(161, 'America/New_York'),
(162, 'America/Nipigon'),
(163, 'America/Nome'),
(164, 'America/Noronha'),
(165, 'America/North_Dakota/Center'),
(166, 'America/North_Dakota/New_Salem'),
(167, 'America/Panama'),
(168, 'America/Pangnirtung'),
(169, 'America/Paramaribo'),
(170, 'America/Phoenix'),
(171, 'America/Port-au-Prince'),
(172, 'America/Port_of_Spain'),
(173, 'America/Porto_Acre'),
(174, 'America/Porto_Velho'),
(175, 'America/Puerto_Rico'),
(176, 'America/Rainy_River'),
(177, 'America/Rankin_Inlet'),
(178, 'America/Recife'),
(179, 'America/Regina'),
(180, 'America/Resolute'),
(181, 'America/Rio_Branco'),
(182, 'America/Rosario'),
(183, 'America/Santarem'),
(184, 'America/Santiago'),
(185, 'America/Santo_Domingo'),
(186, 'America/Sao_Paulo'),
(187, 'America/Scoresbysund'),
(188, 'America/Shiprock'),
(189, 'America/St_Barthelemy'),
(190, 'America/St_Johns'),
(191, 'America/St_Kitts'),
(192, 'America/St_Lucia'),
(193, 'America/St_Thomas'),
(194, 'America/St_Vincent'),
(195, 'America/Swift_Current'),
(196, 'America/Tegucigalpa'),
(197, 'America/Thule'),
(198, 'America/Thunder_Bay'),
(199, 'America/Tijuana'),
(200, 'America/Toronto'),
(201, 'America/Tortola'),
(202, 'America/Vancouver'),
(203, 'America/Virgin'),
(204, 'America/Whitehorse'),
(205, 'America/Winnipeg'),
(206, 'America/Yakutat'),
(207, 'America/Yellowknife'),
(208, 'Antarctica/Casey'),
(209, 'Antarctica/Davis'),
(210, 'Antarctica/DumontDUrville'),
(211, 'Antarctica/Mawson'),
(212, 'Antarctica/McMurdo'),
(213, 'Antarctica/Palmer'),
(214, 'Antarctica/Rothera'),
(215, 'Antarctica/South_Pole'),
(216, 'Antarctica/Syowa'),
(217, 'Antarctica/Vostok'),
(218, 'Arctic/Longyearbyen'),
(219, 'Asia/Aden'),
(220, 'Asia/Almaty'),
(221, 'Asia/Amman'),
(222, 'Asia/Anadyr'),
(223, 'Asia/Aqtau'),
(224, 'Asia/Aqtobe'),
(225, 'Asia/Ashgabat'),
(226, 'Asia/Ashkhabad'),
(227, 'Asia/Baghdad'),
(228, 'Asia/Bahrain'),
(229, 'Asia/Baku'),
(230, 'Asia/Bangkok'),
(231, 'Asia/Beirut'),
(232, 'Asia/Bishkek'),
(233, 'Asia/Brunei'),
(234, 'Asia/Calcutta'),
(235, 'Asia/Choibalsan'),
(236, 'Asia/Chongqing'),
(237, 'Asia/Chungking'),
(238, 'Asia/Colombo'),
(239, 'Asia/Dacca'),
(240, 'Asia/Damascus'),
(241, 'Asia/Dhaka'),
(242, 'Asia/Dili'),
(243, 'Asia/Dubai'),
(244, 'Asia/Dushanbe'),
(245, 'Asia/Gaza'),
(246, 'Asia/Harbin'),
(247, 'Asia/Ho_Chi_Minh'),
(248, 'Asia/Hong_Kong'),
(249, 'Asia/Hovd'),
(250, 'Asia/Irkutsk'),
(251, 'Asia/Istanbul'),
(252, 'Asia/Jakarta'),
(253, 'Asia/Jayapura'),
(254, 'Asia/Jerusalem'),
(255, 'Asia/Kabul'),
(256, 'Asia/Kamchatka'),
(257, 'Asia/Karachi'),
(258, 'Asia/Kashgar'),
(259, 'Asia/Kathmandu'),
(260, 'Asia/Katmandu'),
(261, 'Asia/Kolkata'),
(262, 'Asia/Krasnoyarsk'),
(263, 'Asia/Kuala_Lumpur'),
(264, 'Asia/Kuching'),
(265, 'Asia/Kuwait'),
(266, 'Asia/Macao'),
(267, 'Asia/Macau'),
(268, 'Asia/Magadan'),
(269, 'Asia/Makassar'),
(270, 'Asia/Manila'),
(271, 'Asia/Muscat'),
(272, 'Asia/Nicosia'),
(273, 'Asia/Novosibirsk'),
(274, 'Asia/Omsk'),
(275, 'Asia/Oral'),
(276, 'Asia/Phnom_Penh'),
(277, 'Asia/Pontianak'),
(278, 'Asia/Pyongyang'),
(279, 'Asia/Qatar'),
(280, 'Asia/Qyzylorda'),
(281, 'Asia/Rangoon'),
(282, 'Asia/Riyadh'),
(283, 'Asia/Saigon'),
(284, 'Asia/Sakhalin'),
(285, 'Asia/Samarkand'),
(286, 'Asia/Seoul'),
(287, 'Asia/Shanghai'),
(288, 'Asia/Singapore'),
(289, 'Asia/Taipei'),
(290, 'Asia/Tashkent'),
(291, 'Asia/Tbilisi'),
(292, 'Asia/Tehran'),
(293, 'Asia/Tel_Aviv'),
(294, 'Asia/Thimbu'),
(295, 'Asia/Thimphu'),
(296, 'Asia/Tokyo'),
(297, 'Asia/Ujung_Pandang'),
(298, 'Asia/Ulaanbaatar'),
(299, 'Asia/Ulan_Bator'),
(300, 'Asia/Urumqi'),
(301, 'Asia/Vientiane'),
(302, 'Asia/Vladivostok'),
(303, 'Asia/Yakutsk'),
(304, 'Asia/Yekaterinburg'),
(305, 'Asia/Yerevan'),
(306, 'Atlantic/Azores'),
(307, 'Atlantic/Bermuda'),
(308, 'Atlantic/Canary'),
(309, 'Atlantic/Cape_Verde'),
(310, 'Atlantic/Faeroe'),
(311, 'Atlantic/Faroe'),
(312, 'Atlantic/Jan_Mayen'),
(313, 'Atlantic/Madeira'),
(314, 'Atlantic/Reykjavik'),
(315, 'Atlantic/South_Georgia'),
(316, 'Atlantic/St_Helena'),
(317, 'Atlantic/Stanley'),
(318, 'Australia/ACT'),
(319, 'Australia/Adelaide'),
(320, 'Australia/Brisbane'),
(321, 'Australia/Broken_Hill'),
(322, 'Australia/Canberra'),
(323, 'Australia/Currie'),
(324, 'Australia/Darwin'),
(325, 'Australia/Eucla'),
(326, 'Australia/Hobart'),
(327, 'Australia/LHI'),
(328, 'Australia/Lindeman'),
(329, 'Australia/Lord_Howe'),
(330, 'Australia/Melbourne'),
(331, 'Australia/North'),
(332, 'Australia/NSW'),
(333, 'Australia/Perth'),
(334, 'Australia/Queensland'),
(335, 'Australia/South'),
(336, 'Australia/Sydney'),
(337, 'Australia/Tasmania'),
(338, 'Australia/Victoria'),
(339, 'Australia/West'),
(340, 'Australia/Yancowinna'),
(341, 'Europe/Amsterdam'),
(342, 'Europe/Andorra'),
(343, 'Europe/Athens'),
(344, 'Europe/Belfast'),
(345, 'Europe/Belgrade'),
(346, 'Europe/Berlin'),
(347, 'Europe/Bratislava'),
(348, 'Europe/Brussels'),
(349, 'Europe/Bucharest'),
(350, 'Europe/Budapest'),
(351, 'Europe/Chisinau'),
(352, 'Europe/Copenhagen'),
(353, 'Europe/Dublin'),
(354, 'Europe/Gibraltar'),
(355, 'Europe/Guernsey'),
(356, 'Europe/Helsinki'),
(357, 'Europe/Isle_of_Man'),
(358, 'Europe/Istanbul'),
(359, 'Europe/Jersey'),
(360, 'Europe/Kaliningrad'),
(361, 'Europe/Kiev'),
(362, 'Europe/Lisbon'),
(363, 'Europe/Ljubljana'),
(364, 'Europe/London'),
(365, 'Europe/Luxembourg'),
(366, 'Europe/Madrid'),
(367, 'Europe/Malta'),
(368, 'Europe/Mariehamn'),
(369, 'Europe/Minsk'),
(370, 'Europe/Monaco'),
(371, 'Europe/Moscow'),
(372, 'Europe/Nicosia'),
(373, 'Europe/Oslo'),
(374, 'Europe/Paris'),
(375, 'Europe/Podgorica'),
(376, 'Europe/Prague'),
(377, 'Europe/Riga'),
(378, 'Europe/Rome'),
(379, 'Europe/Samara'),
(380, 'Europe/San_Marino'),
(381, 'Europe/Sarajevo'),
(382, 'Europe/Simferopol'),
(383, 'Europe/Skopje'),
(384, 'Europe/Sofia'),
(385, 'Europe/Stockholm'),
(386, 'Europe/Tallinn'),
(387, 'Europe/Tirane'),
(388, 'Europe/Tiraspol'),
(389, 'Europe/Uzhgorod'),
(390, 'Europe/Vaduz'),
(391, 'Europe/Vatican'),
(392, 'Europe/Vienna'),
(393, 'Europe/Vilnius'),
(394, 'Europe/Volgograd'),
(395, 'Europe/Warsaw'),
(396, 'Europe/Zagreb'),
(397, 'Europe/Zaporozhye'),
(398, 'Europe/Zurich'),
(399, 'Indian/Antananarivo'),
(400, 'Indian/Chagos'),
(401, 'Indian/Christmas'),
(402, 'Indian/Cocos'),
(403, 'Indian/Comoro'),
(404, 'Indian/Kerguelen'),
(405, 'Indian/Mahe'),
(406, 'Indian/Maldives'),
(407, 'Indian/Mauritius'),
(408, 'Indian/Mayotte'),
(409, 'Indian/Reunion'),
(410, 'Pacific/Apia'),
(411, 'Pacific/Auckland'),
(412, 'Pacific/Chatham'),
(413, 'Pacific/Easter'),
(414, 'Pacific/Efate'),
(415, 'Pacific/Enderbury'),
(416, 'Pacific/Fakaofo'),
(417, 'Pacific/Fiji'),
(418, 'Pacific/Funafuti'),
(419, 'Pacific/Galapagos'),
(420, 'Pacific/Gambier'),
(421, 'Pacific/Guadalcanal'),
(422, 'Pacific/Guam'),
(423, 'Pacific/Honolulu'),
(424, 'Pacific/Johnston'),
(425, 'Pacific/Kiritimati'),
(426, 'Pacific/Kosrae'),
(427, 'Pacific/Kwajalein'),
(428, 'Pacific/Majuro'),
(429, 'Pacific/Marquesas'),
(430, 'Pacific/Midway'),
(431, 'Pacific/Nauru'),
(432, 'Pacific/Niue'),
(433, 'Pacific/Norfolk'),
(434, 'Pacific/Noumea'),
(435, 'Pacific/Pago_Pago'),
(436, 'Pacific/Palau'),
(437, 'Pacific/Pitcairn'),
(438, 'Pacific/Ponape'),
(439, 'Pacific/Port_Moresby'),
(440, 'Pacific/Rarotonga'),
(441, 'Pacific/Saipan'),
(442, 'Pacific/Samoa'),
(443, 'Pacific/Tahiti'),
(444, 'Pacific/Tarawa'),
(445, 'Pacific/Tongatapu'),
(446, 'Pacific/Truk'),
(447, 'Pacific/Wake'),
(448, 'Pacific/Wallis'),
(449, 'Pacific/Yap'),
(450, 'Brazil/Acre'),
(451, 'Brazil/DeNoronha'),
(452, 'Brazil/East'),
(453, 'Brazil/West'),
(454, 'Canada/Atlantic'),
(455, 'Canada/Central'),
(456, 'Canada/East-Saskatchewan'),
(457, 'Canada/Eastern'),
(458, 'Canada/Mountain'),
(459, 'Canada/Newfoundland'),
(460, 'Canada/Pacific'),
(461, 'Canada/Saskatchewan'),
(462, 'Canada/Yukon'),
(463, 'CET'),
(464, 'Chile/Continental'),
(465, 'Chile/EasterIsland'),
(466, 'CST6CDT'),
(467, 'Cuba'),
(468, 'EET'),
(469, 'Egypt'),
(470, 'Eire'),
(471, 'EST'),
(472, 'EST5EDT'),
(473, 'Etc/GMT'),
(474, 'Etc/GMT+0'),
(475, 'Etc/GMT+1'),
(476, 'Etc/GMT+10'),
(477, 'Etc/GMT+11'),
(478, 'Etc/GMT+12'),
(479, 'Etc/GMT+2'),
(480, 'Etc/GMT+3'),
(481, 'Etc/GMT+4'),
(482, 'Etc/GMT+5'),
(483, 'Etc/GMT+6'),
(484, 'Etc/GMT+7'),
(485, 'Etc/GMT+8'),
(486, 'Etc/GMT+9'),
(487, 'Etc/GMT-0'),
(488, 'Etc/GMT-1'),
(489, 'Etc/GMT-10'),
(490, 'Etc/GMT-11'),
(491, 'Etc/GMT-12'),
(492, 'Etc/GMT-13'),
(493, 'Etc/GMT-14'),
(494, 'Etc/GMT-2'),
(495, 'Etc/GMT-3'),
(496, 'Etc/GMT-4'),
(497, 'Etc/GMT-5'),
(498, 'Etc/GMT-6'),
(499, 'Etc/GMT-7'),
(500, 'Etc/GMT-8'),
(501, 'Etc/GMT-9'),
(502, 'Etc/GMT0'),
(503, 'Etc/Greenwich'),
(504, 'Etc/UCT'),
(505, 'Etc/Universal'),
(506, 'Etc/UTC'),
(507, 'Etc/Zulu'),
(508, 'Factory'),
(509, 'GB'),
(510, 'GB-Eire'),
(511, 'GMT'),
(512, 'GMT+0'),
(513, 'GMT-0'),
(514, 'GMT0'),
(515, 'Greenwich'),
(516, 'Hongkong'),
(517, 'HST'),
(518, 'Iceland'),
(519, 'Iran'),
(520, 'Israel'),
(521, 'Jamaica'),
(522, 'Japan'),
(523, 'Kwajalein'),
(524, 'Libya'),
(525, 'MET'),
(526, 'Mexico/BajaNorte'),
(527, 'Mexico/BajaSur'),
(528, 'Mexico/General'),
(529, 'MST'),
(530, 'MST7MDT'),
(531, 'Navajo'),
(532, 'NZ'),
(533, 'NZ-CHAT'),
(534, 'Poland'),
(535, 'Portugal'),
(536, 'PRC'),
(537, 'PST8PDT'),
(538, 'ROC'),
(539, 'ROK'),
(540, 'Singapore'),
(541, 'Turkey'),
(542, 'UCT'),
(543, 'Universal'),
(544, 'US/Alaska'),
(545, 'US/Aleutian'),
(546, 'US/Arizona'),
(547, 'US/Central'),
(548, 'US/East-Indiana'),
(549, 'US/Eastern'),
(550, 'US/Hawaii'),
(551, 'US/Indiana-Starke'),
(552, 'US/Michigan'),
(553, 'US/Mountain'),
(554, 'US/Pacific'),
(555, 'US/Pacific-New'),
(556, 'US/Samoa'),
(557, 'UTC'),
(558, 'W-SU'),
(559, 'WET'),
(560, 'Zulu');

-- --------------------------------------------------------

--
-- Structure de la table `ps_translation`
--

DROP TABLE IF EXISTS `ps_translation`;
CREATE TABLE IF NOT EXISTS `ps_translation` (
  `id_translation` int(11) NOT NULL AUTO_INCREMENT,
  `id_lang` int(11) NOT NULL,
  `key` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `translation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `domain` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_translation`),
  KEY `IDX_ADEBEB36BA299860` (`id_lang`),
  KEY `key` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ps_warehouse`
--

DROP TABLE IF EXISTS `ps_warehouse`;
CREATE TABLE IF NOT EXISTS `ps_warehouse` (
  `id_warehouse` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_address` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `reference` varchar(64) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `management_type` enum('WA','FIFO','LIFO') NOT NULL DEFAULT 'WA',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_warehouse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_warehouse_carrier`
--

DROP TABLE IF EXISTS `ps_warehouse_carrier`;
CREATE TABLE IF NOT EXISTS `ps_warehouse_carrier` (
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_warehouse`,`id_carrier`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_carrier` (`id_carrier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_warehouse_product_location`
--

DROP TABLE IF EXISTS `ps_warehouse_product_location`;
CREATE TABLE IF NOT EXISTS `ps_warehouse_product_location` (
  `id_warehouse_product_location` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `location` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_warehouse_product_location`),
  UNIQUE KEY `id_product` (`id_product`,`id_product_attribute`,`id_warehouse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_warehouse_shop`
--

DROP TABLE IF EXISTS `ps_warehouse_shop`;
CREATE TABLE IF NOT EXISTS `ps_warehouse_shop` (
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_warehouse`,`id_shop`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_webservice_account`
--

DROP TABLE IF EXISTS `ps_webservice_account`;
CREATE TABLE IF NOT EXISTS `ps_webservice_account` (
  `id_webservice_account` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL,
  `description` text,
  `class_name` varchar(50) NOT NULL DEFAULT 'WebserviceRequest',
  `is_module` tinyint(2) NOT NULL DEFAULT '0',
  `module_name` varchar(50) DEFAULT NULL,
  `active` tinyint(2) NOT NULL,
  PRIMARY KEY (`id_webservice_account`),
  KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_webservice_account_shop`
--

DROP TABLE IF EXISTS `ps_webservice_account_shop`;
CREATE TABLE IF NOT EXISTS `ps_webservice_account_shop` (
  `id_webservice_account` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_webservice_account`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_webservice_permission`
--

DROP TABLE IF EXISTS `ps_webservice_permission`;
CREATE TABLE IF NOT EXISTS `ps_webservice_permission` (
  `id_webservice_permission` int(11) NOT NULL AUTO_INCREMENT,
  `resource` varchar(50) NOT NULL,
  `method` enum('GET','POST','PUT','DELETE','HEAD') NOT NULL,
  `id_webservice_account` int(11) NOT NULL,
  PRIMARY KEY (`id_webservice_permission`),
  UNIQUE KEY `resource_2` (`resource`,`method`,`id_webservice_account`),
  KEY `resource` (`resource`),
  KEY `method` (`method`),
  KEY `id_webservice_account` (`id_webservice_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ps_web_browser`
--

DROP TABLE IF EXISTS `ps_web_browser`;
CREATE TABLE IF NOT EXISTS `ps_web_browser` (
  `id_web_browser` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_web_browser`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_web_browser`
--

INSERT INTO `ps_web_browser` (`id_web_browser`, `name`) VALUES
(1, 'Safari'),
(2, 'Safari iPad'),
(3, 'Firefox'),
(4, 'Opera'),
(5, 'IE 6'),
(6, 'IE 7'),
(7, 'IE 8'),
(8, 'IE 9'),
(9, 'IE 10'),
(10, 'IE 11'),
(11, 'Chrome');

-- --------------------------------------------------------

--
-- Structure de la table `ps_wishlist`
--

DROP TABLE IF EXISTS `ps_wishlist`;
CREATE TABLE IF NOT EXISTS `ps_wishlist` (
  `id_wishlist` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED DEFAULT '1',
  `id_shop_group` int(10) UNSIGNED DEFAULT '1',
  `token` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `counter` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `default` int(10) UNSIGNED DEFAULT '0',
  PRIMARY KEY (`id_wishlist`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_wishlist_product`
--

DROP TABLE IF EXISTS `ps_wishlist_product`;
CREATE TABLE IF NOT EXISTS `ps_wishlist_product` (
  `id_wishlist_product` int(10) NOT NULL AUTO_INCREMENT,
  `id_wishlist` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `priority` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_wishlist_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_wishlist_product_cart`
--

DROP TABLE IF EXISTS `ps_wishlist_product_cart`;
CREATE TABLE IF NOT EXISTS `ps_wishlist_product_cart` (
  `id_wishlist_product` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_zone`
--

DROP TABLE IF EXISTS `ps_zone`;
CREATE TABLE IF NOT EXISTS `ps_zone` (
  `id_zone` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_zone`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_zone`
--

INSERT INTO `ps_zone` (`id_zone`, `name`, `active`) VALUES
(1, 'Europe', 1),
(2, 'North America', 1),
(3, 'Asia', 1),
(4, 'Africa', 1),
(5, 'Oceania', 1),
(6, 'South America', 1),
(7, 'Europe (non-EU)', 1),
(8, 'Central America/Antilla', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ps_zone_shop`
--

DROP TABLE IF EXISTS `ps_zone_shop`;
CREATE TABLE IF NOT EXISTS `ps_zone_shop` (
  `id_zone` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_zone`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ps_zone_shop`
--

INSERT INTO `ps_zone_shop` (`id_zone`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
